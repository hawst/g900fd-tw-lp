.class Lcom/google/android/finsky/billing/iab/PendingNotificationsService$3;
.super Ljava/lang/Object;
.source "PendingNotificationsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/PendingNotificationsService;->restartAlarmsAfterBoot()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/PendingNotificationsService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/PendingNotificationsService;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/PendingNotificationsService$3;->this$0:Lcom/google/android/finsky/billing/iab/PendingNotificationsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/iab/PendingNotificationsService$3;->this$0:Lcom/google/android/finsky/billing/iab/PendingNotificationsService;

    invoke-static {v2}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v9

    .line 125
    .local v9, "accounts":[Landroid/accounts/Account;
    move-object v10, v9

    .local v10, "arr$":[Landroid/accounts/Account;
    array-length v0, v10

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v11, v0, :cond_6

    aget-object v8, v10, v11

    .line 126
    .local v8, "account":Landroid/accounts/Account;
    iget-object v3, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 127
    .local v3, "accountName":Ljava/lang/String;
    sget-boolean v2, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 128
    const-string v2, "Checking for pending alarms for account=%s"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v20, 0x0

    aput-object v3, v17, v20

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    :cond_0
    invoke-static {v3}, Lcom/google/android/finsky/utils/VendingPreferences;->getMarketAlarmStartTime(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 131
    .local v18, "startTime":J
    const-wide/16 v20, 0x0

    cmp-long v2, v18, v20

    if-nez v2, :cond_2

    .line 132
    sget-boolean v2, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 133
    const-string v2, "No pending alarm."

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    :cond_1
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 137
    :cond_2
    invoke-static {v3}, Lcom/google/android/finsky/utils/VendingPreferences;->getMarketAlarmTimeout(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 142
    .local v14, "interval":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 143
    .local v6, "now":J
    sub-long v12, v6, v18

    .line 144
    .local v12, "elapsed":J
    const-wide/16 v20, 0x0

    cmp-long v2, v12, v20

    if-gez v2, :cond_3

    .line 146
    const-string v2, "Current time is wrong? current time=%d, alarm start time=%d"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v20, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v17, v20

    const/16 v20, 0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v17, v20

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    const-wide/16 v12, 0x0

    .line 150
    :cond_3
    sub-long v4, v14, v12

    .line 153
    .local v4, "remaining":J
    const-wide/16 v20, 0x4e20

    cmp-long v2, v4, v20

    if-gez v2, :cond_5

    .line 154
    sget-boolean v2, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v2, :cond_4

    .line 155
    const-string v2, "remaining=%d, delaying alarm for a while."

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v20, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v17, v20

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    :cond_4
    const-wide/16 v4, 0x4e20

    .line 159
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/iab/PendingNotificationsService$3;->this$0:Lcom/google/android/finsky/billing/iab/PendingNotificationsService;

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/billing/iab/PendingNotificationsService;->setMarketAlarm(Landroid/content/Context;Ljava/lang/String;JJ)V

    goto :goto_1

    .line 161
    .end local v3    # "accountName":Ljava/lang/String;
    .end local v4    # "remaining":J
    .end local v6    # "now":J
    .end local v8    # "account":Landroid/accounts/Account;
    .end local v12    # "elapsed":J
    .end local v14    # "interval":J
    .end local v18    # "startTime":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/iab/PendingNotificationsService$3;->this$0:Lcom/google/android/finsky/billing/iab/PendingNotificationsService;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/PendingNotificationsService;->stopSelf()V

    .line 162
    return-void
.end method
