.class public Lcom/google/android/finsky/activities/MainActivity;
.super Lcom/google/android/finsky/activities/AuthenticatedActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/fragments/PageFragmentHost;
.implements Lcom/google/android/finsky/layout/actionbar/ActionBarController;


# static fields
.field private static IS_HC_OR_ABOVE:Z

.field private static sBillingInitialized:Z


# instance fields
.field private mBitmapSequenceNumberToDrainFrom:I

.field public mContentFrame:Landroid/view/ViewGroup;

.field private mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

.field private final mHandler:Landroid/os/Handler;

.field private mLastShownErrorHash:I

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mNotificationListener:Lcom/google/android/finsky/utils/NotificationListener;

.field private mPageNeedsRefresh:Z

.field private mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSequenceNumberToDrainFrom:I

.field private final mStopPreviewsRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->IS_HC_OR_ABOVE:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 83
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;-><init>()V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPageNeedsRefresh:Z

    .line 216
    iput v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    .line 218
    iput v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    .line 220
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mHandler:Landroid/os/Handler;

    .line 231
    new-instance v0, Lcom/google/android/finsky/activities/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/MainActivity$1;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNotificationListener:Lcom/google/android/finsky/utils/NotificationListener;

    .line 270
    new-instance v0, Lcom/google/android/finsky/activities/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/MainActivity$2;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mStopPreviewsRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/MainActivity;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/MainActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/activities/MainActivity;->showErrorDialogForCode(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideErrorMessage()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/MainActivity;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSavedInstanceState:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/activities/MainActivity;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/finsky/activities/MainActivity;->mSavedInstanceState:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->handleIntent()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/MainActivity;)Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->initializeBilling()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->initializeBillingCountries()V

    return-void
.end method

.method private buildAnalyticsUrl(Ljava/lang/String;Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 527
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkHasPromoOffers(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 653
    new-instance v0, Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;-><init>(Lcom/google/android/finsky/activities/MainActivity;Lcom/google/android/finsky/api/DfeApi;)V

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->run(Ljava/lang/Runnable;)V

    .line 654
    return-void
.end method

.method public static getMyDownloadsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 278
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.finsky.VIEW_MY_DOWNLOADS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getReferringPackage()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 481
    :try_start_0
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 482
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v2

    .line 483
    .local v2, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 484
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 490
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v2    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    :goto_0
    return-object v3

    .line 486
    :catch_0
    move-exception v1

    .line 487
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Exception while getting package."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 490
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private handleIntent()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 390
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideLoadingIndicator()V

    .line 391
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideErrorMessage()V

    .line 393
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 395
    .local v6, "currentIntent":Landroid/content/Intent;
    const-string v0, "authAccount"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 396
    .local v10, "newAccountName":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 397
    const-string v0, "authAccount"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 398
    const-string v0, "b/5160617: Switching account to %s due to intent"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v10}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    invoke-virtual {p0, v10, v6}, Lcom/google/android/finsky/activities/MainActivity;->switchAccount(Ljava/lang/String;Landroid/content/Intent;)V

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->maybeShowDownloadManagerDisabledDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->maybeShowGmsCoreDisabledDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    invoke-direct {p0, v6}, Lcom/google/android/finsky/activities/MainActivity;->maybeShowErrorDialog(Landroid/content/Intent;)V

    .line 413
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->closeDrawer()V

    .line 417
    :cond_2
    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    .line 418
    .local v9, "intentAction":Ljava/lang/String;
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 419
    invoke-direct {p0, v6}, Lcom/google/android/finsky/activities/MainActivity;->handleSearchIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 420
    :cond_3
    const-string v0, "com.google.android.finsky.NAVIGATIONAL_SUGGESTION"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 423
    invoke-static {v6}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->getDocIdFromNavigationalQuery(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v8

    .line 424
    .local v8, "docId":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 425
    invoke-direct {p0, v6, v8}, Lcom/google/android/finsky/activities/MainActivity;->sendSuggestionsReport(Landroid/content/Intent;Ljava/lang/String;)V

    .line 427
    :cond_4
    invoke-direct {p0, v6}, Lcom/google/android/finsky/activities/MainActivity;->handleViewIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 428
    .end local v8    # "docId":Ljava/lang/String;
    :cond_5
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 430
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 431
    invoke-direct {p0, v6}, Lcom/google/android/finsky/activities/MainActivity;->handleViewIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 432
    :cond_7
    const-string v0, "com.google.android.finsky.DETAILS"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 433
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v6}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    goto :goto_0

    .line 434
    :cond_8
    const-string v0, "com.google.android.finsky.VIEW_MY_DOWNLOADS"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 435
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 436
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToMyDownloads(Lcom/google/android/finsky/api/model/DfeToc;)V

    goto/16 :goto_0

    .line 437
    :cond_9
    const-string v0, "com.google.android.finsky.CORPUS_HOME"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 438
    const-string v0, "backend_id"

    invoke-virtual {v6, v0, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 440
    .local v3, "backendId":I
    const-string v0, "title"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 442
    .local v2, "title":Ljava/lang/String;
    if-nez v3, :cond_a

    .line 443
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V

    goto/16 :goto_0

    .line 445
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v6}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    invoke-virtual {v0, v4, v2, v3, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToCorpusHome(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;)V

    goto/16 :goto_0

    .line 448
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "backendId":I
    :cond_b
    const-string v0, "com.google.android.finsky.VIEW_BROWSE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 449
    const-string v0, "backend_id"

    invoke-virtual {v6, v0, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 451
    .restart local v3    # "backendId":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v7

    .line 452
    .local v7, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    invoke-virtual {v7, v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    if-nez v0, :cond_c

    .line 455
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V

    goto/16 :goto_0

    .line 458
    :cond_c
    const-string v0, "title"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 459
    .restart local v2    # "title":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    .line 461
    .local v1, "listUrl":Ljava/lang/String;
    const-string v0, "clear_back_stack"

    invoke-virtual {v6, v0, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 463
    .local v11, "shouldClearBackStack":Z
    if-eqz v11, :cond_d

    .line 464
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 466
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0

    .line 471
    .end local v1    # "listUrl":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "backendId":I
    .end local v7    # "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    .end local v11    # "shouldClearBackStack":Z
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isBackStackEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V

    goto/16 :goto_0
.end method

.method private handleSearchIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 543
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->isTosAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    :goto_0
    return-void

    .line 547
    :cond_0
    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 550
    .local v0, "query":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/activities/MainActivity;->sendSuggestionsReport(Landroid/content/Intent;Ljava/lang/String;)V

    .line 553
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRecentSuggestions()Landroid/provider/SearchRecentSuggestions;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getCurrentBackend()I

    move-result v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSearch(Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method private handleViewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "currentIntent"    # Landroid/content/Intent;

    .prologue
    .line 495
    const-string v1, "deepLink"

    invoke-direct {p0, v1, p1}, Lcom/google/android/finsky/activities/MainActivity;->buildAnalyticsUrl(Ljava/lang/String;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 496
    .local v0, "analyticsUrl":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/analytics/Analytics;->logAdMobPageView(Ljava/lang/String;)V

    .line 497
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "dont_resolve_again"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/MainActivity;->openInBrowser(Landroid/content/Intent;)V

    .line 503
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->getReferringPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->handleDeepLink(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private hideErrorMessage()V
    .locals 2

    .prologue
    .line 1317
    const v0, 0x7f0a00f6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1318
    return-void
.end method

.method private initializeBilling()V
    .locals 2

    .prologue
    .line 624
    sget-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->sBillingInitialized:Z

    if-eqz v0, :cond_0

    .line 650
    :goto_0
    return-void

    .line 627
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->sBillingInitialized:Z

    .line 628
    const-string v0, "Optimistically initializing billing parameters."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 631
    new-instance v0, Lcom/google/android/finsky/activities/MainActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/MainActivity$7;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->initializeCarrierBillingStorage(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private initializeBillingCountries()V
    .locals 3

    .prologue
    .line 658
    new-instance v0, Lcom/google/android/finsky/billing/GetBillingCountriesAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;-><init>()V

    .line 659
    .local v0, "gbca":Lcom/google/android/finsky/billing/GetBillingCountriesAction;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;->run(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 660
    return-void
.end method

.method private maybeShowDownloadManagerDisabledDialog()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1174
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v5

    const-string v6, "com.android.providers.downloads"

    invoke-interface {v5, v6}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v3

    .line 1176
    .local v3, "state":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v3, :cond_1

    .line 1177
    const-string v5, "Cannot find com.android.providers.downloads"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1197
    :cond_0
    :goto_0
    return v4

    .line 1179
    :cond_1
    iget-boolean v5, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-nez v5, :cond_2

    iget-boolean v5, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabledByUser:Z

    if-eqz v5, :cond_0

    .line 1181
    :cond_2
    const-string v5, "Detected disabled com.android.providers.downloads"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1183
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1185
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v5, "download_manager_disabled"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_3

    .line 1186
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 1187
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const/16 v5, 0x28

    invoke-virtual {v0, v7, v5, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c01c1

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c02a0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c0134

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 1191
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 1192
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "download_manager_disabled"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1194
    .end local v0    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v1    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private maybeShowErrorDialog(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1256
    const-string v5, "error_html_message"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1257
    const/4 v4, 0x0

    .line 1258
    .local v4, "title":Ljava/lang/String;
    const-string v5, "error_title"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1259
    const-string v5, "error_title"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1261
    :cond_0
    const-string v5, "error_html_message"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1262
    .local v2, "message":Ljava/lang/String;
    const-string v5, "error_doc_id"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1263
    .local v0, "docId":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 1264
    .local v1, "errorHash":I
    iget v5, p0, Lcom/google/android/finsky/activities/MainActivity;->mLastShownErrorHash:I

    if-eq v5, v1, :cond_1

    .line 1265
    const-string v5, "error_return_code"

    const/4 v6, -0x1

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1267
    .local v3, "returnCode":I
    invoke-direct {p0, v4, v2, v3, v0}, Lcom/google/android/finsky/activities/MainActivity;->showErrorDialogForCode(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z

    .line 1268
    iput v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mLastShownErrorHash:I

    .line 1271
    .end local v0    # "docId":Ljava/lang/String;
    .end local v1    # "errorHash":I
    .end local v2    # "message":Ljava/lang/String;
    .end local v3    # "returnCode":I
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private maybeShowGmsCoreDisabledDialog()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1214
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-ge v5, v6, :cond_1

    .line 1241
    :cond_0
    :goto_0
    return v4

    .line 1218
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v5

    const-string v6, "com.google.android.gms"

    invoke-interface {v5, v6}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v3

    .line 1220
    .local v3, "state":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v3, :cond_2

    .line 1221
    const-string v5, "Cannot find com.google.android.gms"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1223
    :cond_2
    iget-boolean v5, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-nez v5, :cond_3

    iget-boolean v5, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabledByUser:Z

    if-eqz v5, :cond_0

    .line 1225
    :cond_3
    const-string v5, "Detected disabled com.google.android.gms"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1227
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1229
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v5, "gms_core_disabled"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_4

    .line 1230
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 1231
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const/16 v5, 0x2a

    invoke-virtual {v0, v7, v5, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c01c2

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c02a0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0c0134

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 1235
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 1236
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "gms_core_disabled"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1238
    .end local v0    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v1    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :cond_4
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private openInBrowser(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 508
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 509
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 510
    .local v1, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 511
    .local v0, "N":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 512
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v2, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 513
    .local v2, "ai":Landroid/content/pm/ActivityInfo;
    iget-object v5, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 518
    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v7, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 519
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 520
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->finish()V

    .line 524
    .end local v2    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_0
    return-void

    .line 511
    .restart local v2    # "ai":Landroid/content/pm/ActivityInfo;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private resetCurrentBackendId()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 700
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    if-eqz v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->updateCurrentBackendId(I)V

    .line 707
    :cond_1
    return-void
.end method

.method private sendSuggestionsReport(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "suggestedQuery"    # Ljava/lang/String;

    .prologue
    .line 567
    const-string v1, "intent_extra_data_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 568
    .local v0, "suggestionData":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 569
    invoke-static {v0, p2}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->sendSuggestionClickedLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_0
    return-void
.end method

.method private showErrorDialogForCode(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "htmlMessage"    # Ljava/lang/String;
    .param p3, "returnCode"    # I
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1146
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v4

    invoke-interface {v4, p4}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 1147
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    packed-switch p3, :pswitch_data_0

    .line 1157
    :cond_0
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/finsky/activities/MainActivity;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1158
    :goto_0
    return v2

    .line 1149
    :pswitch_0
    if-eqz v1, :cond_1

    iget-boolean v4, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    if-eqz v4, :cond_1

    move v0, v2

    .line 1150
    .local v0, "isSystemApp":Z
    :goto_1
    if-nez v0, :cond_0

    .line 1151
    invoke-direct {p0, p4}, Lcom/google/android/finsky/activities/MainActivity;->showMismatchedCertificatesDialog(Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "isSystemApp":Z
    :cond_1
    move v0, v3

    .line 1149
    goto :goto_1

    .line 1147
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private showErrorMessage(Lcom/android/volley/VolleyError;)V
    .locals 5
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 1321
    iget-boolean v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mStateSaved:Z

    if-eqz v3, :cond_0

    .line 1322
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 1323
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_0

    .line 1324
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->refreshOnResume()V

    .line 1362
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    :goto_0
    return-void

    .line 1329
    :cond_0
    instance-of v3, p1, Lcom/google/android/finsky/utils/BgDataDisabledError;

    if-eqz v3, :cond_1

    .line 1330
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->showBackgroundDataDialog()V

    goto :goto_0

    .line 1334
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isBackStackEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1335
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 1338
    :cond_2
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    .line 1339
    .local v1, "errorMessageHtml":Ljava/lang/String;
    const v3, 0x7f0a00f6

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1340
    .local v2, "errorUi":Landroid/view/View;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1341
    const v3, 0x7f0a00f4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1342
    const v3, 0x7f0a01ce

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/activities/MainActivity$13;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/MainActivity$13;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private showMismatchedCertificatesDialog(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1370
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 1371
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v3, 0x7f0c017a

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c02a0

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c01e7

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 1374
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1375
    .local v1, "data":Landroid/os/Bundle;
    const-string v3, "error_package_name"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    const/4 v3, 0x0

    const/16 v4, 0x20

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 1377
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v2

    .line 1378
    .local v2, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "mismatched_certificates"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1379
    return-void
.end method

.method private showRestartRequiredDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1246
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 1247
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const/16 v2, 0x29

    invoke-virtual {v0, v3, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c01c3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 1251
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 1252
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "restart_required"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1253
    return-void
.end method


# virtual methods
.method public disableActionBarOverlay()V
    .locals 7

    .prologue
    .line 735
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->clearAnimation()V

    .line 736
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->cancelCurrentActionBarAlphaAnimation()V

    .line 738
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->setActionBarOverlayEnabledForCurrent(Z)V

    .line 739
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    .line 741
    .local v2, "currentTopPadding":I
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/app/ActionBar;->getHeight()I

    move-result v0

    .line 744
    .local v0, "actionBarHeight":I
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 746
    .local v1, "contentFrameOriginalTopPadding":I
    if-ne v2, v1, :cond_0

    .line 753
    :goto_0
    return-void

    .line 749
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0
.end method

.method public enableActionBarOverlay()V
    .locals 6

    .prologue
    .line 722
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->setActionBarOverlayEnabledForCurrent(Z)V

    .line 723
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v0

    .line 724
    .local v0, "currentTopPadding":I
    if-nez v0, :cond_0

    .line 730
    :goto_0
    return-void

    .line 727
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0
.end method

.method public enterActionBarSearchMode()V
    .locals 3

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->enterActionBarSearchMode(Landroid/view/View;)V

    .line 774
    sget-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 779
    :cond_0
    return-void
.end method

.method public enterActionBarSectionExpandedMode(Ljava/lang/CharSequence;Lcom/google/android/finsky/activities/TextSectionTranslatable;)V
    .locals 2
    .param p1, "expandedModeTitle"    # Ljava/lang/CharSequence;
    .param p2, "expandedModeTranslatable"    # Lcom/google/android/finsky/activities/TextSectionTranslatable;

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->enterActionBarSectionExpandedMode(Landroid/view/View;Ljava/lang/CharSequence;Lcom/google/android/finsky/activities/TextSectionTranslatable;)V

    .line 797
    return-void
.end method

.method public enterDrawerOpenMode(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "drawerOpenModeTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->enterDrawerOpenMode(Ljava/lang/CharSequence;)V

    .line 808
    sget-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 813
    :cond_0
    return-void
.end method

.method public exitActionBarSearchMode()V
    .locals 3

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitActionBarSearchMode(Landroid/view/View;)V

    .line 785
    sget-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 790
    :cond_0
    return-void
.end method

.method public exitActionBarSectionExpandedMode()V
    .locals 2

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitActionBarSectionExpandedMode(Landroid/view/View;)V

    .line 802
    return-void
.end method

.method public exitDrawerOpenMode()V
    .locals 3

    .prologue
    .line 818
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitDrawerOpenMode()V

    .line 819
    sget-boolean v0, Lcom/google/android/finsky/activities/MainActivity;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    .line 822
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setLayerType(ILandroid/graphics/Paint;)V

    .line 824
    :cond_0
    return-void
.end method

.method public getActionBarAlpha()I
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getActionBarAlpha()I

    move-result v0

    return v0
.end method

.method public getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .locals 0

    .prologue
    .line 717
    return-object p0
.end method

.method public getActionBarHelper()Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    return-object v0
.end method

.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 870
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentBackend()I
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getCurrentBackendId()I

    move-result v0

    return v0
.end method

.method public getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p1, "dfeAccount"    # Ljava/lang/String;

    .prologue
    .line 865
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method public getPeopleClient()Lcom/google/android/gms/people/PeopleClient;
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    return-object v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->onBackPressed()V

    .line 831
    return-void
.end method

.method protected handleAuthenticationError(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 1301
    instance-of v2, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1302
    check-cast v0, Lcom/android/volley/AuthFailureError;

    .line 1303
    .local v0, "authFailure":Lcom/android/volley/AuthFailureError;
    invoke-virtual {v0}, Lcom/android/volley/AuthFailureError;->getResolutionIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1304
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 1305
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->handleUserAuthentication(Landroid/content/Intent;)V

    .line 1314
    .end local v0    # "authFailure":Lcom/android/volley/AuthFailureError;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1310
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideLoadingIndicator()V

    .line 1311
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/MainActivity;->showErrorMessage(Lcom/android/volley/VolleyError;)V

    .line 1313
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->openDrawer()V

    goto :goto_0
.end method

.method public isActionBarInOpaqueMode()Z
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isActionBarInOpaqueMode()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    .line 940
    const/16 v5, 0x1f

    if-ne p1, v5, :cond_0

    const/16 v5, 0x28

    if-ne p2, v5, :cond_0

    .line 943
    const-string v4, "b/5160617: Reinitialize with null accountafter user changed content level"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 945
    new-instance v4, Lcom/google/android/finsky/activities/MainActivity$8;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/MainActivity$8;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/MainActivity;->runOrScheduleActiveStateRunnable(Ljava/lang/Runnable;)V

    .line 1041
    :goto_0
    return-void

    .line 952
    :cond_0
    const/16 v5, 0x21

    if-ne p1, v5, :cond_5

    .line 953
    const/4 v0, -0x1

    .line 954
    .local v0, "backendId":I
    const/4 v1, 0x1

    .line 955
    .local v1, "hasShownHeavyDialog":Z
    const/4 v2, 0x0

    .line 956
    .local v2, "isGame":Z
    if-eqz p3, :cond_1

    .line 957
    const-string v5, "backend"

    invoke-virtual {p3, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 958
    const-string v5, "involved_heavy_dialogs"

    invoke-virtual {p3, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 960
    invoke-static {p3}, Lcom/google/android/finsky/activities/PlayGamesInstallHelper;->isGameIntent(Landroid/content/Intent;)Z

    move-result v2

    .line 961
    const/4 p3, 0x0

    .line 966
    :cond_1
    if-ne p2, v6, :cond_2

    const/4 v4, 0x3

    if-ne v0, v4, :cond_2

    if-nez v1, :cond_2

    .line 972
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->shouldShowGaiaRecoveryDialog()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 976
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/google/android/finsky/activities/MainActivity$9;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/MainActivity$9;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1040
    .end local v0    # "backendId":I
    .end local v1    # "hasShownHeavyDialog":Z
    .end local v2    # "isGame":Z
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 985
    .restart local v0    # "backendId":I
    .restart local v1    # "hasShownHeavyDialog":Z
    .restart local v2    # "isGame":Z
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/activities/AutoUpdateMigrationHelper;->shouldShowAutoUpdateMigrationDialog()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 990
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/google/android/finsky/activities/MainActivity$10;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/MainActivity$10;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 1001
    :cond_4
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/finsky/activities/PlayGamesInstallHelper;->shouldSuggestPlayGames()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1005
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/google/android/finsky/activities/MainActivity$11;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/MainActivity$11;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 1018
    .end local v0    # "backendId":I
    .end local v1    # "hasShownHeavyDialog":Z
    .end local v2    # "isGame":Z
    :cond_5
    const/16 v5, 0x22

    if-ne p1, v5, :cond_7

    .line 1019
    if-ne p2, v6, :cond_6

    move v3, v4

    .line 1020
    .local v3, "success":Z
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    const/16 v6, 0x1f7

    invoke-virtual {v5, v6, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logOperationSuccessBackgroundEvent(IZ)V

    .line 1022
    if-eqz v3, :cond_2

    .line 1025
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApi()Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/google/android/play/dfe/api/PlayDfeApi;->invalidatePlusProfile(Z)V

    .line 1028
    iget-object v4, p0, Lcom/google/android/finsky/activities/MainActivity;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/finsky/activities/MainActivity$12;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/MainActivity$12;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 1037
    .end local v3    # "success":Z
    :cond_7
    const/16 v4, 0x27

    if-ne p1, v4, :cond_2

    .line 1038
    invoke-static {p2, p3}, Lcom/google/android/finsky/utils/GPlusUtils;->circlePickerOnActivityResult(ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1082
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->closeDrawer()V

    .line 1094
    :cond_0
    :goto_0
    return-void

    .line 1086
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 1087
    .local v0, "currPage":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->onBackPressed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1091
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBack()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1092
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCleanup()V
    .locals 10

    .prologue
    .line 879
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v8

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getBitmapRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/google/android/finsky/FinskyApp;->drainAllRequests(II)V

    .line 883
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/FinskyApp;->clearCacheAsync(Ljava/lang/Runnable;)V

    .line 885
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mStateSaved:Z

    if-eqz v7, :cond_0

    .line 886
    const-string v7, "Should not be here after state was saved"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 919
    :goto_0
    return-void

    .line 894
    :cond_0
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    if-eqz v7, :cond_1

    .line 895
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 900
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->flush()Z

    .line 903
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    if-eqz v7, :cond_4

    .line 904
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 905
    .local v1, "childCount":I
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 906
    .local v4, "childrenToRemove":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v1, :cond_3

    .line 907
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v7, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 908
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    .line 909
    .local v2, "childId":I
    const v7, 0x7f0a00f5

    if-eq v2, v7, :cond_2

    const v7, 0x7f0a00f6

    if-eq v2, v7, :cond_2

    .line 911
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 906
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 914
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childId":I
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 915
    .local v3, "childToRemove":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 918
    .end local v1    # "childCount":I
    .end local v3    # "childToRemove":Landroid/view/View;
    .end local v4    # "childrenToRemove":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->showLoadingIndicator()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 385
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 387
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 284
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 286
    const v1, 0x7f0400d4

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->setContentView(I)V

    .line 288
    const v1, 0x7f0a00a6

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 289
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/MainActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 291
    iput-object p1, p0, Lcom/google/android/finsky/activities/MainActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 293
    const v1, 0x7f0a00c4

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mContentFrame:Landroid/view/ViewGroup;

    .line 295
    new-instance v1, Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 296
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->init(Lcom/google/android/finsky/activities/MainActivity;)V

    .line 299
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    new-instance v2, Lcom/google/android/finsky/activities/MainActivity$3;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/MainActivity$3;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 323
    if-eqz p1, :cond_0

    .line 324
    const-string v1, "last_shown_error_hash"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mLastShownErrorHash:I

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isBackStackEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 331
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideLoadingIndicator()V

    .line 332
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->hideErrorMessage()V

    .line 335
    :cond_1
    const v1, 0x7f0a025c

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .line 337
    new-instance v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-direct {v1, v2, p0, p0, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Landroid/support/v7/app/ActionBarActivity;Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .line 341
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v1, p0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->configure(Lcom/google/android/finsky/activities/MainActivity;Landroid/os/Bundle;)V

    .line 344
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MainActivity;->setDefaultKeyMode(I)V

    .line 348
    new-instance v1, Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/activities/MainActivity$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/MainActivity$4;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    new-instance v4, Lcom/google/android/finsky/activities/MainActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/MainActivity$5;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    const/16 v5, 0x79

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/people/PeopleClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;I)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    .line 364
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1098
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1100
    .local v0, "inflater":Landroid/view/MenuInflater;
    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1101
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v1, p0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->configureMenu(Landroid/app/Activity;Landroid/view/Menu;)V

    .line 1103
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->terminate()V

    .line 1046
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onDestroy()V

    .line 1047
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 1432
    sparse-switch p1, :sswitch_data_0

    .line 1455
    :goto_0
    return-void

    .line 1434
    :sswitch_0
    const-string v1, "error_package_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1435
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    goto :goto_0

    .line 1438
    .end local v0    # "packageName":Ljava/lang/String;
    :sswitch_1
    invoke-static {}, Lcom/google/android/finsky/activities/AutoUpdateMigrationHelper;->handleNegativeClick()V

    goto :goto_0

    .line 1441
    :sswitch_2
    const-string v1, "Shutting down because download manager remains disabled"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1442
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 1445
    :sswitch_3
    const-string v1, "Shutting down because gms core remains disabled"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1446
    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 1432
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x24 -> :sswitch_1
        0x28 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 923
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 924
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mStateSaved:Z

    if-nez v0, :cond_0

    .line 927
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->handleIntent()V

    .line 930
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/MainActivity;->onNewIntentDirect(Landroid/content/Intent;)V

    .line 936
    :goto_0
    return-void

    .line 933
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mStateSaved:Z

    .line 934
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 1110
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1140
    :cond_0
    :goto_0
    return v1

    .line 1114
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1140
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0

    .line 1117
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->closeDrawer()V

    .line 1119
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 1120
    .local v0, "currPage":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->onBackPressed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1125
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goUp()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1126
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onBackPressed()V

    goto :goto_0

    .line 1130
    .end local v0    # "currPage":Lcom/google/android/finsky/fragments/PageFragment;
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->translateButtonClicked()V

    goto :goto_0

    .line 1133
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->autoUpdateButtonClicked(Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0

    .line 1136
    :sswitch_3
    const-string v2, "Environment indicator (not visible externally)"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1114
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0a0188 -> :sswitch_1
        0x7f0a03cb -> :sswitch_2
        0x7f0a03cc -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 850
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onPause()V

    .line 851
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/Notifier;->setNotificationListener(Lcom/google/android/finsky/utils/NotificationListener;)V

    .line 852
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    .line 853
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    .line 855
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1386
    packed-switch p1, :pswitch_data_0

    .line 1421
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->onPositiveClick(ILandroid/os/Bundle;)V

    .line 1425
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 1391
    :pswitch_2
    invoke-static {}, Lcom/google/android/finsky/activities/AutoUpdateMigrationHelper;->handlePositiveClick()V

    goto :goto_0

    .line 1394
    :pswitch_3
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->onPositiveGaiaRecoveryDialogResponse()V

    goto :goto_0

    .line 1397
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {v0}, Lcom/google/android/finsky/activities/PlayGamesInstallHelper;->handlePositiveClick(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    goto :goto_0

    .line 1400
    :pswitch_5
    const-string v0, "Attempting to enable download manager"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1401
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.android.providers.downloads"

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 1405
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->showRestartRequiredDialog()V

    goto :goto_0

    .line 1408
    :pswitch_6
    const-string v0, "Attempting to enable gms core"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1409
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 1413
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->showRestartRequiredDialog()V

    goto :goto_0

    .line 1416
    :pswitch_7
    const-string v0, "Shutting down after download manager or gms core re-enabled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1417
    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 1386
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 378
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->syncDrawerIndicator()V

    .line 381
    return-void
.end method

.method protected onReady(Z)V
    .locals 1
    .param p1, "shouldHandleIntent"    # Z

    .prologue
    .line 580
    invoke-static {p0}, Lcom/google/android/finsky/utils/SessionStatsLogger;->logSessionStatsIfNecessary(Landroid/content/Context;)V

    .line 582
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->prefetchAndCacheGaiaAuthRecoveryIntent(Landroid/content/Context;Ljava/lang/String;)V

    .line 585
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->refreshHasFopCacheIfNecessary(Lcom/google/android/finsky/api/DfeApi;)V

    .line 586
    new-instance v0, Lcom/google/android/finsky/activities/MainActivity$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/activities/MainActivity$6;-><init>(Lcom/google/android/finsky/activities/MainActivity;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/MainActivity;->checkHasPromoOffers(Ljava/lang/Runnable;)V

    .line 611
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->refresh()V

    .line 612
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 859
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onRestart()V

    .line 860
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPageNeedsRefresh:Z

    .line 861
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 835
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onResume()V

    .line 836
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPageNeedsRefresh:Z

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->refreshPage()V

    .line 838
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPageNeedsRefresh:Z

    .line 840
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNotificationListener:Lcom/google/android/finsky/utils/NotificationListener;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/Notifier;->setNotificationListener(Lcom/google/android/finsky/utils/NotificationListener;)V

    .line 843
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->syncDrawerIndicator()V

    .line 846
    :cond_1
    return-void
.end method

.method public onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mStopPreviewsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 373
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onRetainCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 671
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 672
    const-string v0, "last_shown_error_hash"

    iget v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mLastShownErrorHash:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 673
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 674
    return-void

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->serialize(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1459
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->isTosAccepted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1460
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->searchButtonClicked()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onSearchRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1462
    :cond_1
    return v0
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1051
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onStop()V

    .line 1056
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mStopPreviewsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1060
    iget v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    if-ne v0, v3, :cond_0

    .line 1061
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    .line 1063
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    if-ne v0, v3, :cond_1

    .line 1064
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    .line 1068
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    iget v2, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/FinskyApp;->drainAllRequests(II)V

    .line 1070
    iput v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mSequenceNumberToDrainFrom:I

    .line 1071
    iput v3, p0, Lcom/google/android/finsky/activities/MainActivity;->mBitmapSequenceNumberToDrainFrom:I

    .line 1074
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0}, Lcom/google/android/gms/people/PeopleClient;->disconnect()V

    .line 1075
    return-void
.end method

.method public openSettings()V
    .locals 2

    .prologue
    .line 1466
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1467
    .local v0, "settingsIntent":Landroid/content/Intent;
    const/16 v1, 0x1f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/activities/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1468
    return-void
.end method

.method public setActionBarAlpha(IZ)V
    .locals 1
    .param p1, "alpha"    # I
    .param p2, "isTransient"    # Z

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setActionBarAlpha(IZ)V

    .line 758
    return-void
.end method

.method public setStatusBarBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->setStatusBarBackgroundColor(I)V

    .line 711
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "goBack"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1281
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1282
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mStateSaved:Z

    if-eqz v1, :cond_1

    .line 1286
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1287
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 1288
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_0

    .line 1289
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->refreshOnResume()V

    .line 1297
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    :cond_0
    :goto_0
    return-void

    .line 1292
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, p2, p3}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    goto :goto_0

    .line 1295
    :cond_2
    const-string v1, "Unknown error with empty error message."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected switchAccount(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "intentToReuse"    # Landroid/content/Intent;

    .prologue
    .line 1480
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->switchAccount(Ljava/lang/String;Landroid/content/Intent;)V

    .line 1485
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MainActivity;->resetCurrentBackendId()V

    .line 1486
    return-void
.end method

.method public updateBreadcrumb(Ljava/lang/String;)V
    .locals 1
    .param p1, "breadcrumb"    # Ljava/lang/String;

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateDefaultTitle(Ljava/lang/String;)V

    .line 684
    return-void
.end method

.method public updateCurrentBackendId(IZ)V
    .locals 1
    .param p1, "backend"    # I
    .param p2, "ignoreActionBarBackground"    # Z

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 689
    iget-object v0, p0, Lcom/google/android/finsky/activities/MainActivity;->mDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->updateCurrentBackendId(I)V

    .line 690
    return-void
.end method
