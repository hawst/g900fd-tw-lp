.class public Lcom/google/android/finsky/setup/PlaySetupService;
.super Landroid/app/Service;
.source "PlaySetupService.java"

# interfaces
.implements Lcom/google/android/finsky/installer/InstallerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/PlaySetupService$5;
    }
.end annotation


# instance fields
.field private mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

.field private mEarlyUpdatePackage:Ljava/lang/String;

.field private mEarlyUpdatePackageIsCritical:Z

.field private mLastEarlyUpdateResponse:Landroid/os/Bundle;

.field private mListenerAdded:Z

.field private mServiceStartId:I

.field private mStartupRefCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/setup/PlaySetupService;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doGetEarlyUpdate()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doStartEarlyUpdate()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/setup/PlaySetupService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/setup/PlaySetupService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doCancelEarlyUpdate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/setup/PlaySetupService;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/finsky/setup/PlaySetupService;->doGetRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doStartVpa()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/setup/PlaySetupService;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doGetFinalHoldFlow()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->doStartDownloads()V

    return-void
.end method

.method static synthetic access$710(Lcom/google/android/finsky/setup/PlaySetupService;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/setup/PlaySetupService;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/finsky/setup/PlaySetupService;->handleIntent(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/PlaySetupService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->stopServiceIfDone()V

    return-void
.end method

.method private doCancelEarlyUpdate()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 443
    sget-object v3, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateEnable:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 444
    const-string v3, "Canceled early-update when disabled"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v3, v4

    .line 476
    :goto_0
    return v3

    .line 449
    :cond_0
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v3, Lcom/google/android/finsky/setup/PlaySetupService$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/setup/PlaySetupService$3;-><init>(Lcom/google/android/finsky/setup/PlaySetupService;)V

    invoke-direct {v0, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 467
    .local v0, "canceler":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Boolean;>;"
    new-instance v3, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 469
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 470
    .local v2, "result":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    goto :goto_0

    .line 471
    .end local v2    # "result":Ljava/lang/Boolean;
    :catch_0
    move-exception v1

    .line 472
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "Canceler interrupted"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v3, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v3, v4

    .line 473
    goto :goto_0

    .line 474
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 475
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "Canceler crashed"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v3, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v3, v4

    .line 476
    goto :goto_0
.end method

.method private doGetEarlyUpdate()Landroid/os/Bundle;
    .locals 24

    .prologue
    .line 271
    sget-object v20, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateEnable:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    if-nez v20, :cond_0

    .line 272
    const/16 v19, 0x0

    .line 367
    :goto_0
    return-object v19

    .line 274
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 277
    sget-object v20, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Long;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-nez v20, :cond_1

    .line 278
    const-string v20, "Unexpected android-id = 0"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    move-object/from16 v21, v0

    sget-object v20, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateDeadlockMaxMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Long;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    sget-object v20, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_1
    monitor-enter p0

    .line 296
    const/16 v20, 0x0

    :try_start_1
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/setup/PlaySetupService;->mLastEarlyUpdateResponse:Landroid/os/Bundle;

    .line 297
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    const/4 v5, 0x0

    .line 303
    .local v5, "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :try_start_2
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getDeviceConfiguration()Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    .line 309
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/FinskyApp;->getDfeApiNonAuthenticated()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v6

    .line 310
    .local v6, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v10

    .line 311
    .local v10, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;>;"
    invoke-interface {v6, v5, v10, v10}, Lcom/google/android/finsky/api/DfeApi;->earlyUpdate(Lcom/google/android/finsky/protos/DeviceConfigurationProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 314
    :try_start_3
    invoke-virtual {v10}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_3

    .line 324
    .local v18, "response":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;
    const-string v20, "Received EarlyUpdate with %d entries"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;->earlyDocumentInfo:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    const/16 v17, 0x0

    .line 330
    .local v17, "pendingPackageCount":I
    const/16 v19, 0x0

    .line 331
    .local v19, "result":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 332
    .local v15, "packageManager":Landroid/content/pm/PackageManager;
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;->earlyDocumentInfo:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    .local v4, "arr$":[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_3
    if-ge v11, v13, :cond_4

    aget-object v7, v4, v11

    .line 333
    .local v7, "documentInfo":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    iget-object v0, v7, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 335
    .local v16, "packageName":Ljava/lang/String;
    sget-object v20, Lcom/google/android/finsky/utils/FinskyPreferences;->earlyUpdateSkipPackage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 332
    :cond_2
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 290
    .end local v4    # "arr$":[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .end local v5    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .end local v6    # "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    .end local v7    # "documentInfo":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .end local v10    # "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;>;"
    .end local v11    # "i$":I
    .end local v13    # "len$":I
    .end local v15    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v16    # "packageName":Ljava/lang/String;
    .end local v17    # "pendingPackageCount":I
    .end local v18    # "response":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;
    .end local v19    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v8

    .line 291
    .local v8, "e":Ljava/lang/InterruptedException;
    const-string v20, "Deadlocked - race condition longer than expected?"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 297
    .end local v8    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v20

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v20

    .line 304
    .restart local v5    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :catch_1
    move-exception v8

    .line 305
    .local v8, "e":Ljava/lang/Exception;
    const-string v20, "Exception while getting device configuration."

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v8, v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 315
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v6    # "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    .restart local v10    # "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;>;"
    :catch_2
    move-exception v8

    .line 316
    .local v8, "e":Ljava/lang/InterruptedException;
    const-string v20, "Interrupted while loading EarlyUpdate"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v8, v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 318
    .end local v8    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v8

    .line 320
    .local v8, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v8}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    .line 321
    .local v9, "error":Ljava/lang/Throwable;
    const-string v20, "Error while loading EarlyUpdate: %s"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 339
    .end local v8    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v9    # "error":Ljava/lang/Throwable;
    .restart local v4    # "arr$":[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .restart local v7    # "documentInfo":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .restart local v11    # "i$":I
    .restart local v13    # "len$":I
    .restart local v15    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v16    # "packageName":Ljava/lang/String;
    .restart local v17    # "pendingPackageCount":I
    .restart local v18    # "response":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;
    .restart local v19    # "result":Landroid/os/Bundle;
    :cond_3
    const/4 v12, 0x0

    .line 341
    .local v12, "installedVersion":I
    const/16 v20, 0x0

    :try_start_5
    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v14

    .line 342
    .local v14, "packageInfo":Landroid/content/pm/PackageInfo;
    iget v12, v14, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_4

    .line 346
    .end local v14    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_5
    iget v0, v7, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_2

    .line 350
    add-int/lit8 v17, v17, 0x1

    .line 352
    if-nez v19, :cond_2

    .line 354
    new-instance v19, Landroid/os/Bundle;

    .end local v19    # "result":Landroid/os/Bundle;
    invoke-direct/range {v19 .. v19}, Landroid/os/Bundle;-><init>()V

    .line 355
    .restart local v19    # "result":Landroid/os/Bundle;
    const-string v20, "package_name"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v20, "version_code"

    iget v0, v7, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 357
    const-string v20, "title"

    iget-object v0, v7, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v20, "critical"

    iget-boolean v0, v7, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_4

    .line 361
    .end local v7    # "documentInfo":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .end local v12    # "installedVersion":I
    .end local v16    # "packageName":Ljava/lang/String;
    :cond_4
    if-eqz v19, :cond_5

    .line 362
    const-string v20, "package_count"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 364
    :cond_5
    monitor-enter p0

    .line 365
    :try_start_6
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/setup/PlaySetupService;->mLastEarlyUpdateResponse:Landroid/os/Bundle;

    .line 366
    monitor-exit p0

    goto/16 :goto_0

    :catchall_1
    move-exception v20

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v20

    .line 343
    .restart local v7    # "documentInfo":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .restart local v12    # "installedVersion":I
    .restart local v16    # "packageName":Ljava/lang/String;
    :catch_4
    move-exception v20

    goto :goto_5
.end method

.method private doGetFinalHoldFlow()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 654
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->startDeferredInstalls()V

    .line 656
    const-string v2, "Getting final hold flow"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 657
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 658
    .local v1, "result":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/services/VpaService;->shouldHold()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/android/finsky/services/RestoreService;->shouldHold()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 659
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->createIntent()Landroid/content/Intent;

    move-result-object v0

    .line 660
    .local v0, "restoreAppsIntent":Landroid/content/Intent;
    const-string v2, "final_hold_intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 662
    .end local v0    # "restoreAppsIntent":Landroid/content/Intent;
    :cond_1
    return-object v1
.end method

.method private doGetRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 556
    sget-object v4, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 557
    const-string v4, "Unexpected android-id = 0"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 560
    :cond_0
    invoke-static {p1, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 562
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_2

    .line 563
    const-string v4, "Received invalid account name: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576
    :cond_1
    :goto_0
    return-object v3

    .line 567
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/finsky/setup/PlaySetupService;->getBackupDevices(Lcom/google/android/finsky/api/DfeApi;)[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    move-result-object v1

    .line 569
    .local v1, "backupDevices":[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    if-eqz v1, :cond_1

    array-length v4, v1

    if-eqz v4, :cond_1

    .line 572
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->createIntent(Ljava/lang/String;[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)Landroid/content/Intent;

    move-result-object v2

    .line 574
    .local v2, "restoreAppsIntent":Landroid/content/Intent;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 575
    .local v3, "result":Landroid/os/Bundle;
    const-string v4, "available_restore_intent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method private doStartDownloads()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 667
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 668
    const-string v0, "Unexpected android-id = 0"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 671
    :cond_0
    const-string v0, "Setup Wizard reports OK to begin downloading apps"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->startDeferredInstalls()V

    .line 674
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->setupWizardStartDownloads:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 675
    return-void
.end method

.method private doStartEarlyUpdate()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 379
    sget-object v1, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateEnable:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 380
    const-string v1, "Started early-update when disabled"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    :goto_0
    return-void

    .line 384
    :cond_0
    monitor-enter p0

    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mLastEarlyUpdateResponse:Landroid/os/Bundle;

    .line 386
    .local v0, "lastResponse":Landroid/os/Bundle;
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    if-nez v0, :cond_1

    .line 388
    const-string v1, "Started early-update when no earlier response given"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 386
    .end local v0    # "lastResponse":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 393
    .restart local v0    # "lastResponse":Landroid/os/Bundle;
    :cond_1
    const-string v1, "package_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "version_code"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "critical"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/finsky/setup/PlaySetupService;->earlyUpdatePackageCommand(Ljava/lang/String;ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method private doStartVpa()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 642
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 643
    const-string v0, "Unexpected android-id = 0"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 646
    :cond_0
    const-string v0, "Starting VPA"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 647
    invoke-static {}, Lcom/google/android/finsky/services/VpaService;->startVpa()V

    .line 648
    return-void
.end method

.method private earlyUpdatePackageCommand(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "critical"    # Z

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 171
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/setup/PlaySetupService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "package"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v2, "version_code"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    const-string v2, "title"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    const-string v2, "critical"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 176
    const-string v2, "playsetupservice://earlyupdatepackage"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 178
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 179
    return-void
.end method

.method private getBackupDevices(Lcom/google/android/finsky/api/DfeApi;)[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    .locals 24
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 581
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->getBlocking(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v21

    .line 582
    .local v21, "consistencyToken":Ljava/lang/String;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v22

    .line 584
    .local v22, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 585
    .local v8, "startTimeMs":J
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v22

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/api/DfeApi;->getBackupDeviceChoices(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 587
    :try_start_0
    invoke-virtual/range {v22 .. v22}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    iget-object v0, v4, Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;->backupDeviceInfo:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    move-object/from16 v20, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 601
    .local v20, "backupDeviceInfos":[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v14, p0

    move-object/from16 v17, p1

    move-wide/from16 v18, v8

    invoke-direct/range {v14 .. v19}, Lcom/google/android/finsky/setup/PlaySetupService;->logBackgroundEvent(ILjava/lang/Throwable;Lcom/google/android/finsky/api/DfeApi;J)V

    .line 602
    const-string v4, "getBackupDeviceChoices returned with %d devices"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, v20

    array-length v10, v0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 603
    .end local v20    # "backupDeviceInfos":[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    :goto_0
    return-object v20

    .line 588
    :catch_0
    move-exception v6

    .line 589
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v4, "Unable to fetch backup devices, InterruptedException: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 591
    const/4 v5, 0x1

    move-object/from16 v4, p0

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/finsky/setup/PlaySetupService;->logBackgroundEvent(ILjava/lang/Throwable;Lcom/google/android/finsky/api/DfeApi;J)V

    .line 592
    const/16 v20, 0x0

    goto :goto_0

    .line 593
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v6

    .line 594
    .local v6, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v6}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v12

    .line 595
    .local v12, "volleyError":Ljava/lang/Throwable;
    instance-of v4, v12, Lcom/google/android/volley/DisplayMessageError;

    if-eqz v4, :cond_0

    move-object v4, v12

    check-cast v4, Lcom/google/android/volley/DisplayMessageError;

    invoke-virtual {v4}, Lcom/google/android/volley/DisplayMessageError;->getDisplayErrorHtml()Ljava/lang/String;

    move-result-object v23

    .line 597
    .local v23, "serverMessage":Ljava/lang/String;
    :goto_1
    const-string v4, "Unable to fetch backup devices: %s (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v12, v5, v7

    const/4 v7, 0x1

    aput-object v23, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 598
    const/4 v11, 0x1

    move-object/from16 v10, p0

    move-object/from16 v13, p1

    move-wide v14, v8

    invoke-direct/range {v10 .. v15}, Lcom/google/android/finsky/setup/PlaySetupService;->logBackgroundEvent(ILjava/lang/Throwable;Lcom/google/android/finsky/api/DfeApi;J)V

    .line 599
    const/16 v20, 0x0

    goto :goto_0

    .line 595
    .end local v23    # "serverMessage":Ljava/lang/String;
    :cond_0
    const/16 v23, 0x0

    goto :goto_1
.end method

.method private handleEarlyUpdatePackageCommand(Ljava/lang/String;ILjava/lang/String;Z)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "critical"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 408
    iget-object v1, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 409
    const-string v1, "Received command to early-update %s while already handling %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    iget-object v3, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    .line 414
    iput-boolean p4, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackageIsCritical:Z

    .line 416
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    .line 417
    .local v0, "installer":Lcom/google/android/finsky/receivers/Installer;
    iget-boolean v1, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    if-nez v1, :cond_1

    .line 418
    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 419
    iput-boolean v7, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    .line 422
    :cond_1
    invoke-interface {v0, p1}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 423
    invoke-interface {v0, p1, v5, v5, v5}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 424
    invoke-interface {v0, p1}, Lcom/google/android/finsky/receivers/Installer;->setEarlyUpdate(Ljava/lang/String;)V

    .line 425
    const/4 v3, 0x0

    const-string v6, "early-update"

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 427
    return v7
.end method

.method private handleIntent(Landroid/content/Intent;)Z
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 242
    const-string v5, "package"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "version_code"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "title"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 244
    const-string v5, "package"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "packageName":Ljava/lang/String;
    const-string v5, "version_code"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 246
    .local v3, "versionCode":I
    const-string v5, "title"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "title":Ljava/lang/String;
    const-string v5, "critical"

    invoke-virtual {p1, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 248
    .local v0, "critical":Z
    invoke-direct {p0, v1, v3, v2, v0}, Lcom/google/android/finsky/setup/PlaySetupService;->handleEarlyUpdatePackageCommand(Ljava/lang/String;ILjava/lang/String;Z)Z

    move-result v4

    .line 251
    .end local v0    # "critical":Z
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "versionCode":I
    :goto_0
    return v4

    .line 250
    :cond_0
    const-string v5, "Unknown command intent %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v4

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private logBackgroundEvent(ILjava/lang/Throwable;Lcom/google/android/finsky/api/DfeApi;J)V
    .locals 8
    .param p1, "resultCode"    # I
    .param p2, "exception"    # Ljava/lang/Throwable;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "startTimeMs"    # J

    .prologue
    const/4 v6, 0x1

    .line 616
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, p4

    .line 617
    .local v2, "latencyMs":J
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    .line 619
    .local v0, "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    const/16 v4, 0x7d

    iput v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    .line 620
    iput-boolean v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    .line 622
    const/4 v4, -0x1

    if-eq p1, v4, :cond_0

    .line 623
    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    .line 624
    iput-boolean v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    .line 626
    :cond_0
    if-eqz p2, :cond_1

    .line 627
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    .line 628
    iput-boolean v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    .line 630
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-eqz v4, :cond_2

    .line 631
    iput-wide v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    .line 632
    iput-boolean v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    .line 634
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-interface {p3}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 635
    .local v1, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 636
    return-void
.end method

.method private startDeferredInstalls()V
    .locals 2

    .prologue
    .line 679
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/finsky/setup/PlaySetupService$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/setup/PlaySetupService$4;-><init>(Lcom/google/android/finsky/setup/PlaySetupService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 685
    return-void
.end method

.method private stopServiceIfDone()V
    .locals 1

    .prologue
    .line 210
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 211
    iget v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 212
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    .line 216
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mServiceStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/PlaySetupService;->stopSelf(I)V

    .line 218
    :cond_1
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/finsky/setup/PlaySetupService$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/setup/PlaySetupService$1;-><init>(Lcom/google/android/finsky/setup/PlaySetupService;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    .line 223
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    if-eqz v0, :cond_0

    .line 228
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mListenerAdded:Z

    .line 231
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    .line 232
    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 492
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    const-string v2, "EarlyUpdate %s: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v5

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 497
    const/4 v1, 0x0

    .line 498
    .local v1, "stopService":Z
    const/4 v0, 0x0

    .line 499
    .local v0, "skipPackage":Z
    sget-object v2, Lcom/google/android/finsky/setup/PlaySetupService$5;->$SwitchMap$com$google$android$finsky$installer$InstallerListener$InstallerPackageEvent:[I

    invoke-virtual {p2}, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 542
    :cond_2
    :goto_1
    :pswitch_0
    if-eqz v0, :cond_3

    .line 543
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->earlyUpdateSkipPackage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 545
    :cond_3
    if-eqz v1, :cond_0

    .line 546
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;

    .line 547
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 548
    invoke-direct {p0}, Lcom/google/android/finsky/setup/PlaySetupService;->stopServiceIfDone()V

    goto :goto_0

    .line 504
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdateLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v2

    if-nez v2, :cond_2

    .line 505
    const-string v2, "Couldn\'t acquire mutex for pending %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 514
    :pswitch_2
    const/4 v1, 0x1

    .line 515
    goto :goto_1

    .line 519
    :pswitch_3
    monitor-enter p0

    .line 520
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackageIsCritical:Z

    if-nez v2, :cond_4

    .line 521
    const/4 v0, 0x1

    .line 523
    :cond_4
    monitor-exit p0

    .line 524
    const/4 v1, 0x1

    .line 525
    goto :goto_1

    .line 523
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 528
    :pswitch_4
    const/4 v0, 0x1

    .line 529
    const/4 v1, 0x1

    .line 530
    goto :goto_1

    .line 533
    :pswitch_5
    const/4 v1, 0x1

    .line 534
    goto :goto_1

    .line 538
    :pswitch_6
    const-string v2, "EarlyUpdate %s: unexpected %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v5

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 499
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 186
    iput p3, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mServiceStartId:I

    .line 187
    iget v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/setup/PlaySetupService;->mStartupRefCount:I

    .line 188
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/setup/PlaySetupService$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/setup/PlaySetupService$2;-><init>(Lcom/google/android/finsky/setup/PlaySetupService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 201
    const/4 v0, 0x3

    return v0
.end method
