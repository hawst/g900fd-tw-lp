.class Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;
.super Ljava/lang/Object;
.source "InlineAppDetailsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->updateDetailsSections()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/InlineAppDetailsFragment;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;->this$0:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 262
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "market://details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;->this$0:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    # getter for: Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v2}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->access$000(Lcom/google/android/finsky/activities/InlineAppDetailsFragment;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 265
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;->this$0:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->startActivity(Landroid/content/Intent;)V

    .line 266
    return-void
.end method
