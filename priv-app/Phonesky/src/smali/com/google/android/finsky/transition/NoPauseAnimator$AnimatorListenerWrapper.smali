.class Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;
.super Ljava/lang/Object;
.source "NoPauseAnimator.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/transition/NoPauseAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimatorListenerWrapper"
.end annotation


# instance fields
.field private final mAnimator:Landroid/animation/Animator;

.field private final mListener:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(Landroid/animation/Animator;Landroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;
    .param p2, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mAnimator:Landroid/animation/Animator;

    .line 143
    iput-object p2, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mListener:Landroid/animation/Animator$AnimatorListener;

    .line 144
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mListener:Landroid/animation/Animator$AnimatorListener;

    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 159
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mListener:Landroid/animation/Animator$AnimatorListener;

    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 154
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mListener:Landroid/animation/Animator$AnimatorListener;

    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 164
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mListener:Landroid/animation/Animator$AnimatorListener;

    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 149
    return-void
.end method
