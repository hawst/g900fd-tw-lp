.class public Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;
.super Lcom/google/android/play/layout/PlayCardViewMini;
.source "PlayCardViewQuickSuggestionMini.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;


# instance fields
.field private final mCardInset:I

.field private mIsInPendingState:Z

.field private mPendingOverlay:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewMini;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mCardInset:I

    .line 38
    return-void
.end method


# virtual methods
.method public enterPendingStateIfNecessary(Z)V
    .locals 5
    .param p1, "animateChanges"    # Z

    .prologue
    const/4 v4, 0x0

    .line 78
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mLoadingIndicator:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    if-eqz v1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 82
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    .line 83
    if-eqz p1, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-static {v1, v2, v3, p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 86
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 89
    .end local v0    # "fadeInAnimation":Landroid/view/animation/Animation;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public exitPendingStateIfNecessary(Z)V
    .locals 4
    .param p1, "animateChanges"    # Z

    .prologue
    .line 95
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    .line 99
    if-eqz p1, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-static {v1, v2, v3, p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 102
    .local v0, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 104
    .end local v0    # "fadeOutAnimation":Landroid/view/animation/Animation;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 58
    return-void

    .line 57
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 53
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 49
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->onAttachedToWindow()V

    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    .line 66
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mIsInPendingState:Z

    .line 73
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->onDetachedFromWindow()V

    .line 74
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewMini;->onFinishInflate()V

    .line 44
    const v0, 0x7f0a02d1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    .line 45
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 126
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/PlayCardViewMini;->onLayout(ZIIII)V

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 136
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayCardThumbnail;->getTop()I

    move-result v0

    .line 133
    .local v0, "overlayTop":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mCardInset:I

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mCardInset:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 110
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewMini;->onMeasure(II)V

    .line 112
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mCardInset:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v1, v2, v3

    .line 117
    .local v1, "overlayWidth":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mCardInset:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    .line 119
    .local v0, "overlayHeight":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewQuickSuggestionMini;->mPendingOverlay:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method
