.class public final Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingProfileResponse"
.end annotation


# instance fields
.field public billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

.field public hasResult:Z

.field public hasUserMessageHtml:Z

.field public result:I

.field public userMessageHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1446
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1447
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .line 1448
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1451
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    .line 1452
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasResult:Z

    .line 1453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .line 1454
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    .line 1455
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasUserMessageHtml:Z

    .line 1456
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->cachedSize:I

    .line 1457
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1477
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1478
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasResult:Z

    if-eqz v1, :cond_1

    .line 1479
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1482
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    if-eqz v1, :cond_2

    .line 1483
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1486
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasUserMessageHtml:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1487
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1490
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1499
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1503
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1504
    :sswitch_0
    return-object p0

    .line 1509
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1510
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1514
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    .line 1515
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasResult:Z

    goto :goto_0

    .line 1521
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    if-nez v2, :cond_1

    .line 1522
    new-instance v2, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .line 1524
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1528
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    .line 1529
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasUserMessageHtml:Z

    goto :goto_0

    .line 1499
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 1510
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1413
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1463
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasResult:Z

    if-eqz v0, :cond_1

    .line 1464
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1466
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    if-eqz v0, :cond_2

    .line 1467
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1469
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->hasUserMessageHtml:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1470
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1472
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1473
    return-void
.end method
