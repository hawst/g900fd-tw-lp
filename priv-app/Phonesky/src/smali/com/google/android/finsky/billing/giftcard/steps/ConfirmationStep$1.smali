.class Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep$1;
.super Ljava/lang/Object;
.source "ConfirmationStep.java"

# interfaces
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep$1;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 114
    if-eqz p2, :cond_0

    .line 115
    invoke-virtual {p1}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 116
    .local v1, "imageLp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 117
    .local v0, "aspectRatio":F
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 118
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep$1;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;

    # getter for: Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->access$000(Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 120
    .end local v0    # "aspectRatio":F
    .end local v1    # "imageLp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 110
    return-void
.end method
