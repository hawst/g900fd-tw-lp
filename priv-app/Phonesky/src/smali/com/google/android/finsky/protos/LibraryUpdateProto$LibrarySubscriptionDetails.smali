.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibrarySubscriptionDetails"
.end annotation


# instance fields
.field public autoRenewing:Z

.field public deprecatedValidUntilTimestampMsec:J

.field public hasAutoRenewing:Z

.field public hasDeprecatedValidUntilTimestampMsec:Z

.field public hasInitiationTimestampMsec:Z

.field public hasSignature:Z

.field public hasSignedPurchaseData:Z

.field public hasTrialUntilTimestampMsec:Z

.field public initiationTimestampMsec:J

.field public signature:Ljava/lang/String;

.field public signedPurchaseData:Ljava/lang/String;

.field public trialUntilTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 671
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 672
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    .line 673
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 676
    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    .line 677
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    .line 678
    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    .line 679
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    .line 680
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    .line 681
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    .line 682
    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    .line 683
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    .line 684
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    .line 685
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    .line 686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    .line 687
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignature:Z

    .line 688
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->cachedSize:I

    .line 689
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 718
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 719
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 720
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 724
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 727
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    if-eqz v1, :cond_5

    .line 728
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 731
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 732
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 735
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 736
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 739
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignature:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 740
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 743
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 752
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 756
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 757
    :sswitch_0
    return-object p0

    .line 762
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    .line 763
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    goto :goto_0

    .line 767
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    .line 768
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    goto :goto_0

    .line 772
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    .line 773
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    goto :goto_0

    .line 777
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    .line 778
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    goto :goto_0

    .line 782
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    .line 783
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    goto :goto_0

    .line 787
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    .line 788
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignature:Z

    goto :goto_0

    .line 752
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 630
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 695
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasInitiationTimestampMsec:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 696
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 698
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasDeprecatedValidUntilTimestampMsec:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 699
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 701
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasAutoRenewing:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    if-eqz v0, :cond_5

    .line 702
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 704
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 705
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 707
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 708
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 710
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignature:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 711
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 713
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 714
    return-void
.end method
