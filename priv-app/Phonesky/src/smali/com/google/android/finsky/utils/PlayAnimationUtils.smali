.class public final Lcom/google/android/finsky/utils/PlayAnimationUtils;
.super Ljava/lang/Object;
.source "PlayAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;,
        Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
    }
.end annotation


# direct methods
.method public static animateFadeIn(Landroid/view/View;J)V
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "durationMs"    # J

    .prologue
    const/4 v6, 0x0

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 75
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    move-object v0, p0

    move-wide v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2, v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "alphaFrom"    # F
    .param p2, "alphaTo"    # F
    .param p3, "initialDelayMs"    # I
    .param p4, "durationMs"    # J
    .param p6, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 146
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    aput p2, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 147
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 148
    invoke-virtual {v0, p4, p5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 149
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 150
    if-eqz p6, :cond_0

    .line 151
    invoke-virtual {v0, p6}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 153
    :cond_0
    return-object v0
.end method

.method public static getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "initialDelayMs"    # J
    .param p3, "durationMs"    # J
    .param p5, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .prologue
    .line 88
    const v1, 0x7f05000e

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 89
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 90
    invoke-virtual {v0, p3, p4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 91
    if-eqz p5, :cond_0

    .line 92
    invoke-virtual {v0, p5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 94
    :cond_0
    return-object v0
.end method

.method public static getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "durationMs"    # J
    .param p3, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .prologue
    .line 83
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static getFadeOutAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "initialDelayMs"    # J
    .param p3, "durationMs"    # J
    .param p5, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .prologue
    .line 104
    const v1, 0x7f05000c

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 105
    .local v0, "fadeOutAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 106
    invoke-virtual {v0, p3, p4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 107
    if-eqz p5, :cond_0

    .line 108
    invoke-virtual {v0, p5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 110
    :cond_0
    return-object v0
.end method

.method public static getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "durationMs"    # J
    .param p3, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .prologue
    .line 99
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method public static getVerticalScrollByAnimation(Landroid/widget/ScrollView;JJILandroid/view/animation/Animation$AnimationListener;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    .locals 7
    .param p0, "scrollView"    # Landroid/widget/ScrollView;
    .param p1, "initialDelayMs"    # J
    .param p3, "durationMs"    # J
    .param p5, "verticalScrollAmount"    # I
    .param p6, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .prologue
    const/4 v1, 0x0

    .line 118
    new-instance v0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;

    int-to-float v4, p5

    move v2, v1

    move v3, v1

    move v5, p5

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;-><init>(FFFFILandroid/widget/ScrollView;)V

    .line 134
    .local v0, "scrollAnimation":Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->setStartOffset(J)V

    .line 135
    invoke-virtual {v0, p3, p4}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->setDuration(J)V

    .line 136
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 137
    if-eqz p6, :cond_0

    .line 138
    invoke-virtual {v0, p6}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 140
    :cond_0
    return-object v0
.end method
