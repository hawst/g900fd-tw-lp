.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryInAppDetails"
.end annotation


# instance fields
.field public hasSignature:Z

.field public hasSignedPurchaseData:Z

.field public signature:Ljava/lang/String;

.field public signedPurchaseData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 832
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 833
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    .line 834
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 837
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    .line 838
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignedPurchaseData:Z

    .line 839
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    .line 840
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignature:Z

    .line 841
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->cachedSize:I

    .line 842
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 859
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 860
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignedPurchaseData:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 861
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 864
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignature:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 865
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 868
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 876
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 877
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 881
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 882
    :sswitch_0
    return-object p0

    .line 887
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    .line 888
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignedPurchaseData:Z

    goto :goto_0

    .line 892
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    .line 893
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignature:Z

    goto :goto_0

    .line 877
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 807
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 848
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignedPurchaseData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 849
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 851
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignature:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 852
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 854
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 855
    return-void
.end method
