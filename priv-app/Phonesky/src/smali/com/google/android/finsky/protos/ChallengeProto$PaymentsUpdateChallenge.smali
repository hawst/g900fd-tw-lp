.class public final Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentsUpdateChallenge"
.end annotation


# instance fields
.field public hasUseClientCart:Z

.field public paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

.field public useClientCart:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2156
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2157
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    .line 2158
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 2162
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    .line 2163
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->hasUseClientCart:Z

    .line 2164
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->cachedSize:I

    .line 2165
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2182
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2183
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-eqz v1, :cond_0

    .line 2184
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2187
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->hasUseClientCart:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    if-eqz v1, :cond_2

    .line 2188
    :cond_1
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2191
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2200
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2204
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2205
    :sswitch_0
    return-object p0

    .line 2210
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-nez v1, :cond_1

    .line 2211
    new-instance v1, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 2213
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2217
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    .line 2218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->hasUseClientCart:Z

    goto :goto_0

    .line 2200
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2132
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-eqz v0, :cond_0

    .line 2172
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2174
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->hasUseClientCart:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    if-eqz v0, :cond_2

    .line 2175
    :cond_1
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2177
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2178
    return-void
.end method
