.class Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "GestureScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/GestureScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/GestureScrollView;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v5, 0x0

    .line 52
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v5

    .line 57
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->getScrollY()I

    move-result v1

    .line 58
    .local v1, "scrollY":I
    const/4 v2, 0x0

    cmpl-float v2, p4, v2

    if-lez v2, :cond_2

    .line 61
    if-gtz v1, :cond_0

    .line 62
    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mFlingToDismissListener:Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;
    invoke-static {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->access$000(Lcom/google/android/finsky/layout/scroll/GestureScrollView;)Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;->onFlingToDismiss()V

    goto :goto_0

    .line 67
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 68
    .local v0, "childHeight":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    if-lt v2, v0, :cond_0

    .line 70
    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mFlingToDismissListener:Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;
    invoke-static {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->access$000(Lcom/google/android/finsky/layout/scroll/GestureScrollView;)Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;->onFlingToDismiss()V

    goto :goto_0
.end method
