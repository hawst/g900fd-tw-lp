.class Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;
.super Ljava/lang/Object;
.source "PackageInstallerLegacyImpl.java"

# interfaces
.implements Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;->install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;

.field final synthetic val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;->this$0:Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;

    iput-object p2, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public installBeginning()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-interface {v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installBeginning()V

    .line 62
    return-void
.end method

.method public installFailed(ILjava/lang/String;)V
    .locals 1
    .param p1, "errorCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    .line 72
    return-void
.end method

.method public installSucceeded()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-interface {v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installSucceeded()V

    .line 67
    return-void
.end method
