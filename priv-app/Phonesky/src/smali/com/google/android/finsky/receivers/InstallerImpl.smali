.class public Lcom/google/android/finsky/receivers/InstallerImpl;
.super Ljava/lang/Object;
.source "InstallerImpl.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadQueueListener;
.implements Lcom/google/android/finsky/receivers/Installer;
.implements Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;
    }
.end annotation


# static fields
.field private static PROGRESS_DOWNLOAD_PENDING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

.field private static PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

.field private static PROGRESS_UNINSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mContext:Landroid/content/Context;

.field private mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

.field private final mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private final mHandler:Landroid/os/Handler;

.field private final mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private final mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

.field private mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field mListener:Lcom/google/android/finsky/installer/IMultiUserCoordinatorServiceListener;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/installer/InstallerListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotifier:Lcom/google/android/finsky/utils/Notifier;

.field private final mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

.field private final mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

.field private mPriorityPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRunning:Z

.field private mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

.field private mServiceConnectionCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstallingPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUsers:Lcom/google/android/finsky/utils/Users;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 786
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    .line 788
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->DOWNLOAD_PENDING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_DOWNLOAD_PENDING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    .line 790
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->UNINSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_UNINSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;Lcom/google/android/finsky/utils/Users;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p4, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueue;
    .param p5, "notifier"    # Lcom/google/android/finsky/utils/Notifier;
    .param p6, "installPolicies"    # Lcom/google/android/finsky/installer/InstallPolicies;
    .param p7, "packageMonitorReceiver"    # Lcom/google/android/finsky/receivers/PackageMonitorReceiver;
    .param p8, "users"    # Lcom/google/android/finsky/utils/Users;
    .param p9, "packageInstaller"    # Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    .line 1243
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;

    .line 1335
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerImpl$6;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/receivers/InstallerImpl$6;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mListener:Lcom/google/android/finsky/installer/IMultiUserCoordinatorServiceListener;

    .line 128
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    .line 129
    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 130
    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 131
    iput-object p4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 132
    iput-object p5, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    .line 133
    iput-object p6, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    .line 134
    iput-object p7, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    .line 135
    iput-object p8, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUsers:Lcom/google/android/finsky/utils/Users;

    .line 136
    iput-object p9, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .line 138
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mListeners:Ljava/util/List;

    .line 139
    invoke-virtual {p2}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mHandler:Landroid/os/Handler;

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z

    .line 143
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUninstallingPackages:Ljava/util/Set;

    .line 144
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->removeAllFilesFromCacheDir(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/appstate/AppStates;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/receivers/InstallerImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/receivers/InstallerImpl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/download/DownloadQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->recoverPriorityList(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/receivers/InstallerImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->recoverRunningDownloads(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/receivers/InstallerImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->pruneSessions()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/receivers/InstallerImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/finsky/receivers/InstallerImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/finsky/receivers/InstallerImpl;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->bindToMultiUserCoordinator(Ljava/lang/Runnable;)V

    return-void
.end method

.method private bindToMultiUserCoordinator(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "runAfter"    # Ljava/lang/Runnable;

    .prologue
    .line 1292
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 1293
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    if-eqz v2, :cond_1

    .line 1294
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1308
    :cond_0
    :goto_0
    return-void

    .line 1297
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1298
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    if-nez v2, :cond_0

    .line 1300
    new-instance v2, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    iput-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    .line 1301
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/finsky/installer/MultiUserCoordinatorService;->createBindIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 1302
    .local v0, "bindIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    const/4 v4, 0x5

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 1304
    .local v1, "result":Z
    if-nez v1, :cond_0

    .line 1305
    const-string v2, "Couldn\'t start service for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private cancelPendingInstall(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 4
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v3, 0x0

    .line 590
    if-eqz p1, :cond_0

    .line 591
    iget-object v0, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    .line 592
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "Cancel pending install of %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 594
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    .line 595
    iget-object v1, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v1, :cond_0

    .line 596
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->clearInstallerState(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 597
    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_CANCELLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 600
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private failPendingForegroundInstalls(Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "apps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    const/4 v5, 0x0

    const/16 v4, 0x3d2

    .line 1118
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 1119
    .local v7, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerImpl;->clearInstallerState(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 1120
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x6f

    iget-object v2, v7, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    const-string v3, "foreground"

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1124
    iget-object v0, v7, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALL_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    goto :goto_0

    .line 1128
    .end local v7    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_0
    return-void
.end method

.method private getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;
    .locals 5
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    .line 1205
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1206
    .local v1, "packageName":Ljava/lang/String;
    if-nez v1, :cond_1

    move-object v2, v3

    .line 1221
    :cond_0
    :goto_0
    return-object v2

    .line 1209
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v2

    .line 1210
    .local v2, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-nez v2, :cond_2

    .line 1212
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, p1}, Lcom/google/android/finsky/download/DownloadQueue;->cancel(Lcom/google/android/finsky/download/Download;)V

    move-object v2, v3

    .line 1213
    goto :goto_0

    .line 1215
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4, v1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 1216
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_3

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-nez v4, :cond_0

    .line 1218
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, p1}, Lcom/google/android/finsky/download/DownloadQueue;->cancel(Lcom/google/android/finsky/download/Download;)V

    move-object v2, v3

    .line 1219
    goto :goto_0
.end method

.method private getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    .line 1197
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private kick(Z)V
    .locals 9
    .param p1, "postToHandler"    # Z

    .prologue
    const/4 v3, 0x0

    .line 951
    if-eqz p1, :cond_1

    .line 952
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/receivers/InstallerImpl$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/receivers/InstallerImpl$3;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 960
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z

    if-nez v0, :cond_2

    .line 961
    const-string v0, "Installer kick - no action, not running yet"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 964
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-nez v0, :cond_0

    .line 967
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 968
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->kickMultiUser()V

    goto :goto_0

    .line 972
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->getAppsToInstall()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->selectNextTask(Ljava/util/List;Ljava/util/List;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v8

    .line 973
    .local v8, "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v8, :cond_0

    .line 974
    const-string v0, "Installer kick - starting %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v8, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 975
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v1, v8, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/receivers/InstallerTask;-><init>(Ljava/lang/String;Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    .line 977
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->start()V

    goto :goto_0
.end method

.method private kickMultiUser()V
    .locals 10

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->getAppsToInstall()Ljava/util/List;

    move-result-object v9

    .line 1143
    .local v9, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1145
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->unbindMultiUserCoordinator()V

    .line 1172
    :cond_0
    :goto_0
    return-void

    .line 1150
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    if-nez v0, :cond_2

    .line 1151
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerImpl$4;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/receivers/InstallerImpl$4;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->bindToMultiUserCoordinator(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1159
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    invoke-virtual {p0, v9, v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->selectNextTaskMultiUser(Ljava/util/List;Ljava/util/List;Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v8

    .line 1161
    .local v8, "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v8, :cond_0

    .line 1168
    const-string v0, "Installer kick - starting %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, v8, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1169
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v1, v8, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/receivers/InstallerTask;-><init>(Ljava/lang/String;Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    .line 1171
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->start()V

    goto :goto_0
.end method

.method private multiUserMode()Z
    .locals 1

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUsers:Lcom/google/android/finsky/utils/Users;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/Users;->hasMultipleUsers()Z

    move-result v0

    return v0
.end method

.method private pruneSessions()V
    .locals 5

    .prologue
    .line 287
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getAppsToInstall()Ljava/util/List;

    move-result-object v2

    .line 288
    .local v2, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 289
    .local v0, "activePackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 290
    .local v1, "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v4, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 292
    .end local v1    # "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v4, v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->pruneSessions(Ljava/util/List;)V

    .line 293
    return-void
.end method

.method private recoverDownload(Landroid/net/Uri;I)Z
    .locals 16
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "status"    # I

    .prologue
    .line 626
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    .line 627
    .local v15, "uriString":Ljava/lang/String;
    :goto_0
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 628
    const/4 v4, 0x0

    .line 679
    :goto_1
    return v4

    .line 626
    .end local v15    # "uriString":Ljava/lang/String;
    :cond_0
    const/4 v15, 0x0

    goto :goto_0

    .line 630
    .restart local v15    # "uriString":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-eqz v4, :cond_2

    .line 631
    const-string v4, "tried recovery while already handling %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v7, v7, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 632
    const/4 v4, 0x0

    goto :goto_1

    .line 634
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore;->getAll()Ljava/util/Collection;

    move-result-object v10

    .line 635
    .local v10, "allApps":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;>;"
    const/4 v14, 0x0

    .line 636
    .local v14, "recoverInstallerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 637
    .local v13, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v13}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 638
    move-object v14, v13

    .line 642
    .end local v13    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_4
    if-nez v14, :cond_5

    .line 643
    const/4 v4, 0x0

    goto :goto_1

    .line 645
    :cond_5
    invoke-virtual {v14}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 646
    .local v3, "packageName":Ljava/lang/String;
    const-string v4, "Recovering download for running %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 650
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 652
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    invoke-interface {v4, v3}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->acquirePackage(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 653
    const-string v4, "Can\'t recover %s *** cannot acquire"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    const/4 v4, 0x0

    goto :goto_1

    .line 656
    :catch_0
    move-exception v11

    .line 657
    .local v11, "e":Landroid/os/RemoteException;
    const-string v4, "Acquiring %s *** received %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object v11, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 662
    .end local v11    # "e":Landroid/os/RemoteException;
    :cond_6
    new-instance v2, Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object/from16 v4, p0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/finsky/receivers/InstallerTask;-><init>(Ljava/lang/String;Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V

    .line 664
    .local v2, "recoverTask":Lcom/google/android/finsky/receivers/InstallerTask;
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->recover(Landroid/net/Uri;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 665
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    .line 666
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 670
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 672
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    invoke-interface {v4, v3}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->releasePackage(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 679
    :cond_8
    :goto_2
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 673
    :catch_1
    move-exception v11

    .line 674
    .restart local v11    # "e":Landroid/os/RemoteException;
    const-string v4, "Releasing %s *** received %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object v11, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private recoverPriorityList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 249
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v1

    .line 250
    .local v1, "flags":I
    and-int/lit16 v3, v1, 0x4000

    if-eqz v3, :cond_1

    .line 252
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 253
    :cond_1
    const v3, 0x8000

    and-int/2addr v3, v1

    if-eqz v3, :cond_0

    .line 255
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    .end local v0    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v1    # "flags":I
    :cond_2
    return-void
.end method

.method private recoverRunningDownloads(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "runningDownloads":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 267
    const/4 v1, 0x0

    .line 268
    .local v1, "oneRecovered":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/download/DownloadProgress;

    .line 269
    .local v2, "record":Lcom/google/android/finsky/download/DownloadProgress;
    const-string v3, "Attempt recovery of %s %d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v2, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    aput-object v5, v4, v6

    iget v5, v2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    if-nez v1, :cond_0

    iget-object v3, v2, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    iget v4, v2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    invoke-direct {p0, v3, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->recoverDownload(Landroid/net/Uri;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 272
    :cond_0
    const-string v3, "Releasing %s %d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v2, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    aput-object v5, v4, v6

    iget v5, v2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    iget-object v4, v2, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    invoke-interface {v3, v4}, Lcom/google/android/finsky/download/DownloadQueue;->release(Landroid/net/Uri;)V

    goto :goto_0

    .line 276
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 279
    .end local v2    # "record":Lcom/google/android/finsky/download/DownloadProgress;
    :cond_2
    return-void
.end method

.method private releaseMultiUserPackage(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1178
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerImpl$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl$5;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->bindToMultiUserCoordinator(Ljava/lang/Runnable;)V

    .line 1188
    return-void
.end method

.method private removeAllFilesFromCacheDir(Ljava/lang/String;)V
    .locals 7
    .param p1, "dirName"    # Ljava/lang/String;

    .prologue
    .line 227
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-direct {v1, v6, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 228
    .local v1, "cacheDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 229
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 230
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_0

    array-length v6, v3

    if-eqz v6, :cond_0

    .line 231
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 232
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 231
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 236
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    return-void
.end method

.method private unbindMultiUserCoordinator()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1315
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 1316
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    if-nez v1, :cond_0

    .line 1331
    :goto_0
    return-void

    .line 1321
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    if-eqz v1, :cond_1

    .line 1322
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->registerListener(Lcom/google/android/finsky/installer/IMultiUserCoordinatorServiceListener;)V

    .line 1323
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    invoke-interface {v1}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->releaseAllPackages()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1328
    :cond_1
    :goto_1
    iput-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    .line 1329
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1330
    iput-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    goto :goto_0

    .line 1325
    :catch_0
    move-exception v0

    .line 1326
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Couldn\'t sign out from coordinator *** received %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public addListener(Lcom/google/android/finsky/installer/InstallerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/installer/InstallerListener;

    .prologue
    .line 823
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 824
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 825
    return-void
.end method

.method public cancel(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 559
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v1

    .line 560
    .local v1, "runningTask":Lcom/google/android/finsky/receivers/InstallerTask;
    if-eqz v1, :cond_0

    .line 562
    invoke-virtual {v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 568
    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    .line 569
    return-void

    .line 565
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 566
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->cancelPendingInstall(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto :goto_0
.end method

.method public cancelAll()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 574
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-eqz v3, :cond_0

    .line 575
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 578
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/AppStates;->getAppsToInstall()Ljava/util/List;

    move-result-object v1

    .line 579
    .local v1, "appsToInstall":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 581
    .local v0, "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->cancelPendingInstall(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto :goto_0

    .line 583
    .end local v0    # "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    .line 584
    return-void
.end method

.method clearInstallerState(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 5
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 606
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-nez v1, :cond_1

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    .line 610
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 611
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v1, v0, v3, v4}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setInstallerState(Ljava/lang/String;ILjava/lang/String;)V

    .line 613
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v1, v0, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 614
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v1, v0, v4}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getProgress(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 796
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v2

    .line 797
    .local v2, "runningTask":Lcom/google/android/finsky/receivers/InstallerTask;
    if-eqz v2, :cond_0

    .line 798
    invoke-virtual {v2}, Lcom/google/android/finsky/receivers/InstallerTask;->getProgress()Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    move-result-object v3

    .line 815
    :goto_0
    return-object v3

    .line 800
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUninstallingPackages:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 801
    sget-object v3, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_UNINSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    goto :goto_0

    .line 804
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 805
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_3

    .line 806
    const/4 v1, -0x1

    .line 807
    .local v1, "installedVersion":I
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v3, :cond_2

    .line 808
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v1, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 810
    :cond_2
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v3

    if-le v3, v1, :cond_3

    .line 812
    sget-object v3, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_DOWNLOAD_PENDING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    goto :goto_0

    .line 815
    .end local v1    # "installedVersion":I
    :cond_3
    sget-object v3, Lcom/google/android/finsky/receivers/InstallerImpl;->PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    goto :goto_0
.end method

.method public getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 764
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v2

    .line 765
    .local v2, "runningTask":Lcom/google/android/finsky/receivers/InstallerTask;
    if-eqz v2, :cond_0

    .line 766
    invoke-virtual {v2}, Lcom/google/android/finsky/receivers/InstallerTask;->getState()Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v3

    .line 783
    :goto_0
    return-object v3

    .line 768
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUninstallingPackages:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 769
    sget-object v3, Lcom/google/android/finsky/receivers/Installer$InstallerState;->UNINSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0

    .line 772
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 773
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_3

    .line 774
    const/4 v1, -0x1

    .line 775
    .local v1, "installedVersion":I
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v3, :cond_2

    .line 776
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v1, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 778
    :cond_2
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v3

    if-le v3, v1, :cond_3

    .line 780
    sget-object v3, Lcom/google/android/finsky/receivers/Installer$InstallerState;->DOWNLOAD_PENDING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0

    .line 783
    .end local v1    # "installedVersion":I
    :cond_3
    sget-object v3, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 690
    const/4 v0, 0x0

    return v0
.end method

.method notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/receivers/InstallerImpl$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/receivers/InstallerImpl$2;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 862
    return-void
.end method

.method public onCancel(Lcom/google/android/finsky/download/Download;)V
    .locals 8
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    .line 893
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v7

    .line 894
    .local v7, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-nez v7, :cond_1

    move-object v6, v3

    .line 895
    .local v6, "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 898
    if-eqz v7, :cond_0

    .line 899
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 901
    :cond_0
    return-void

    .line 894
    .end local v6    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/finsky/receivers/InstallerTask;->getAppData()Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    goto :goto_0
.end method

.method public onComplete(Lcom/google/android/finsky/download/Download;)V
    .locals 8
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    .line 880
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v7

    .line 881
    .local v7, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-nez v7, :cond_1

    move-object v6, v3

    .line 882
    .local v6, "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 885
    if-eqz v7, :cond_0

    .line 886
    invoke-virtual {v7, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->onComplete(Lcom/google/android/finsky/download/Download;)V

    .line 888
    :cond_0
    return-void

    .line 881
    .end local v6    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/finsky/receivers/InstallerTask;->getAppData()Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    goto :goto_0
.end method

.method public onError(Lcom/google/android/finsky/download/Download;I)V
    .locals 8
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "httpStatus"    # I

    .prologue
    const/4 v3, 0x0

    .line 907
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v7

    .line 908
    .local v7, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-nez v7, :cond_1

    move-object v6, v3

    .line 909
    .local v6, "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x68

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move v4, p2

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 912
    if-eqz v7, :cond_0

    .line 913
    invoke-virtual {v7, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->onError(Lcom/google/android/finsky/download/Download;I)V

    .line 915
    :cond_0
    return-void

    .line 908
    .end local v6    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/finsky/receivers/InstallerTask;->getAppData()Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    goto :goto_0
.end method

.method public onNotificationClicked(Lcom/google/android/finsky/download/Download;)V
    .locals 6
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v4, 0x0

    .line 929
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 930
    .local v1, "packageName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 931
    const-string v2, "Discarding notification click, no packageName for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 940
    :goto_0
    return-void

    .line 936
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v4, v4, v3}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 938
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 939
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onPackageAdded(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/utils/Notifier;->hidePackageRemovedMessage(Ljava/lang/String;)V

    .line 1364
    return-void
.end method

.method public onPackageAvailabilityChanged([Ljava/lang/String;Z)V
    .locals 0
    .param p1, "packageNames"    # [Ljava/lang/String;
    .param p2, "available"    # Z

    .prologue
    .line 1426
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1419
    return-void
.end method

.method public onPackageFirstLaunch(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1431
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "replacing"    # Z

    .prologue
    const/4 v5, -0x1

    .line 1379
    if-eqz p2, :cond_2

    .line 1380
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v4}, Lcom/google/android/finsky/utils/Notifier;->hideInstallingMessage()V

    .line 1387
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUninstallingPackages:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1390
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 1391
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v4, :cond_1

    .line 1392
    const/4 v1, 0x0

    .line 1393
    .local v1, "clearDesiredVersion":Z
    iget-object v3, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1394
    .local v3, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v4

    if-ne v4, v5, :cond_3

    .line 1408
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 1409
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v4, p1, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 1413
    .end local v1    # "clearDesiredVersion":Z
    .end local v3    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_1
    sget-object v4, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->UNINSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1414
    return-void

    .line 1382
    .end local v0    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v4, p1}, Lcom/google/android/finsky/utils/Notifier;->hideAllMessagesForPackage(Ljava/lang/String;)V

    .line 1383
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v4, p1}, Lcom/google/android/finsky/utils/Notifier;->hidePackageRemoveRequestMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 1396
    .restart local v0    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .restart local v1    # "clearDesiredVersion":Z
    .restart local v3    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_3
    if-eqz p2, :cond_4

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v4, :cond_4

    .line 1399
    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v2

    .line 1400
    .local v2, "desiredVersion":I
    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v4, v4, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    if-ge v4, v2, :cond_0

    .line 1401
    const/4 v1, 0x1

    goto :goto_1

    .line 1405
    .end local v2    # "desiredVersion":I
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 1
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    .line 920
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v0

    .line 921
    .local v0, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-eqz v0, :cond_0

    .line 922
    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V

    .line 924
    :cond_0
    return-void
.end method

.method public onStart(Lcom/google/android/finsky/download/Download;)V
    .locals 8
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    .line 867
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Lcom/google/android/finsky/download/Download;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v7

    .line 868
    .local v7, "task":Lcom/google/android/finsky/receivers/InstallerTask;
    if-nez v7, :cond_1

    move-object v6, v3

    .line 869
    .local v6, "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x65

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 872
    if-eqz v7, :cond_0

    .line 873
    invoke-virtual {v7, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->onStart(Lcom/google/android/finsky/download/Download;)V

    .line 875
    :cond_0
    return-void

    .line 868
    .end local v6    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/finsky/receivers/InstallerTask;->getAppData()Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    goto :goto_0
.end method

.method public promiseInstall(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installSize"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->hasSession(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const-string v0, "Promising install for already-existing session for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->createSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 310
    return-void
.end method

.method public removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/installer/InstallerListener;

    .prologue
    .line 832
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 833
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 834
    return-void
.end method

.method public requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 16
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "packageVersion"    # I
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "deferred"    # Z
    .param p6, "reason"    # Ljava/lang/String;
    .param p7, "priority"    # I

    .prologue
    .line 333
    const-string v2, "requestInstall"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 336
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v2, v3, :cond_1

    .line 337
    const-string v2, "Dropping install request for %s because already installing"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v9

    .line 346
    .local v9, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    move/from16 v12, p2

    .line 347
    .local v12, "newVersionCode":I
    if-eqz v9, :cond_4

    iget-object v15, v9, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 348
    .local v15, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :goto_1
    if-eqz v15, :cond_5

    iget v14, v15, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 350
    .local v14, "oldVersionCode":I
    :goto_2
    new-instance v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v8}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 351
    .local v8, "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iput v12, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 352
    const/4 v2, 0x1

    iput-boolean v2, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 353
    const/4 v2, -0x1

    if-le v14, v2, :cond_2

    .line 354
    iput v14, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 355
    const/4 v2, 0x1

    iput-boolean v2, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 357
    :cond_2
    if-eqz v15, :cond_3

    .line 358
    iget-object v2, v9, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-boolean v2, v2, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    iput-boolean v2, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 359
    const/4 v2, 0x1

    iput-boolean v2, v8, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 362
    :cond_3
    if-gt v12, v14, :cond_7

    .line 363
    const-string v2, "Skipping attempt to download %s version %d over version %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    .line 366
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const/16 v3, 0x70

    const-string v5, "older-version"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 369
    iget-object v2, v9, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v2, :cond_6

    iget-object v2, v9, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v11

    .line 370
    .local v11, "flags":I
    :goto_3
    and-int/lit8 v2, v11, 0x1

    if-nez v2, :cond_0

    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    const v4, 0x7f0c016c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/finsky/utils/Notifier;->showInstallationFailureMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 347
    .end local v8    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .end local v11    # "flags":I
    .end local v14    # "oldVersionCode":I
    .end local v15    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_4
    const/4 v15, 0x0

    goto :goto_1

    .line 348
    .restart local v15    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_5
    const/4 v14, -0x1

    goto :goto_2

    .line 369
    .restart local v8    # "logAppData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .restart local v14    # "oldVersionCode":I
    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    .line 379
    :cond_7
    const-string v2, "Request install of %s v=%d for %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p6, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 380
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const/16 v3, 0x69

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v4, p1

    move-object/from16 v5, p6

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->hasSession(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->createSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 390
    :cond_8
    if-eqz v9, :cond_a

    iget-object v13, v9, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 391
    .local v13, "oldRow":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :goto_4
    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->buildUpon(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    move-result-object v10

    .line 393
    .local v10, "builder":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    move/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDesiredVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 394
    move/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setLastNotifiedVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 395
    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 396
    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 398
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual {v10, v2, v4, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDeliveryData(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 399
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setInstallerState(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 400
    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDownloadUri(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 402
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v11

    .line 403
    .restart local v11    # "flags":I
    :goto_5
    and-int/lit8 v11, v11, -0xd

    .line 404
    and-int/lit16 v11, v11, -0x601

    .line 405
    and-int/lit16 v11, v11, -0x3001

    .line 407
    const v2, -0xc001

    and-int/2addr v11, v2

    .line 408
    const/4 v2, 0x1

    move/from16 v0, p7

    if-ne v0, v2, :cond_c

    .line 409
    or-int/lit16 v11, v11, 0x4000

    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 415
    :cond_9
    :goto_6
    invoke-virtual {v10, v11}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setFlags(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-virtual {v10}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->build()Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->put(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 419
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_PENDING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 422
    if-nez p5, :cond_0

    .line 423
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    goto/16 :goto_0

    .line 390
    .end local v10    # "builder":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .end local v11    # "flags":I
    .end local v13    # "oldRow":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_a
    const/4 v13, 0x0

    goto :goto_4

    .line 402
    .restart local v10    # "builder":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .restart local v13    # "oldRow":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_b
    const/4 v11, 0x0

    goto :goto_5

    .line 411
    .restart local v11    # "flags":I
    :cond_c
    const/4 v2, 0x2

    move/from16 v0, p7

    if-ne v0, v2, :cond_9

    .line 412
    const v2, 0x8000

    or-int/2addr v11, v2

    .line 413
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPriorityPackages:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6
.end method

.method selectNextTask(Ljava/util/List;Ljava/util/List;)Lcom/google/android/finsky/appstate/AppStates$AppState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;"
        }
    .end annotation

    .prologue
    .local p1, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    .local p2, "priorityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 999
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v1, v6

    .line 1028
    :cond_0
    :goto_0
    return-object v1

    .line 1014
    .local v3, "i":I
    .local v5, "priorityPackage":Ljava/lang/String;
    :cond_1
    const-string v7, "Unexpected: Priority package %s no longer installable"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v9

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1003
    .end local v3    # "i":I
    .end local v5    # "priorityPackage":Ljava/lang/String;
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 1004
    invoke-interface {p2, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1005
    .restart local v5    # "priorityPackage":Ljava/lang/String;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 1006
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 1007
    .local v1, "candidate":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1005
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1019
    .end local v1    # "candidate":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v3    # "i":I
    .end local v5    # "priorityPackage":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/finsky/installer/InstallPolicies;->getForegroundPackages(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v2

    .line 1020
    .local v2, "foregroundPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 1021
    .local v0, "app":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v7, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    move-object v1, v0

    .line 1022
    goto :goto_0

    .line 1027
    .end local v0    # "app":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->failPendingForegroundInstalls(Ljava/util/Collection;)V

    move-object v1, v6

    .line 1028
    goto :goto_0
.end method

.method selectNextTaskMultiUser(Ljava/util/List;Ljava/util/List;Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;)Lcom/google/android/finsky/appstate/AppStates$AppState;
    .locals 14
    .param p3, "coordinatorService"    # Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;",
            ")",
            "Lcom/google/android/finsky/appstate/AppStates$AppState;"
        }
    .end annotation

    .prologue
    .line 1062
    .local p1, "appsToInstall":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    .local p2, "priorityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v6, v10, -0x1

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_3

    .line 1063
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1064
    .local v9, "priorityPackage":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1065
    .local v5, "found":Z
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v7, v10, :cond_0

    .line 1066
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/appstate/AppStates$AppState;

    iget-object v10, v10, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1068
    invoke-interface {p1, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 1069
    .local v2, "appToMove":Lcom/google/android/finsky/appstate/AppStates$AppState;
    const/4 v10, 0x0

    invoke-interface {p1, v10, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1070
    const/4 v5, 0x1

    .line 1074
    .end local v2    # "appToMove":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_0
    if-nez v5, :cond_1

    .line 1077
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1062
    :cond_1
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 1065
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1083
    .end local v5    # "found":Z
    .end local v7    # "j":I
    .end local v9    # "priorityPackage":Ljava/lang/String;
    :cond_3
    iget-object v10, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/finsky/installer/InstallPolicies;->getForegroundPackages(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v8

    .line 1084
    .local v8, "packagesToSkip":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p2

    invoke-interface {v8, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1086
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 1090
    .local v4, "foregroundAppsSkipped":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/finsky/appstate/AppStates$AppState;>;"
    const/4 v6, 0x0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v6, v10, :cond_6

    .line 1091
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/AppStates$AppState;

    .line 1095
    .local v1, "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :try_start_0
    iget-object v10, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    invoke-interface {v8, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1096
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1090
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1099
    :cond_4
    iget-object v10, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->acquirePackage(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1100
    iget-object v10, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1114
    .end local v1    # "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :goto_4
    return-object v1

    .line 1103
    .restart local v1    # "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    :cond_5
    const-string v10, "Skipping install of %s - not acquired"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1104
    :catch_0
    move-exception v3

    .line 1106
    .local v3, "e":Landroid/os/RemoteException;
    const-string v10, "Couldn\'t acquire %s (proceed anyway) received %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageName:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v3, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 1113
    .end local v1    # "appToInstall":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_6
    invoke-direct {p0, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->failPendingForegroundInstalls(Ljava/util/Collection;)V

    .line 1114
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "deliveryToken"    # Ljava/lang/String;

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method public setEarlyUpdate(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 534
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    .line 535
    .local v1, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-interface {v1, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v0

    .line 536
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v3, 0x0

    .line 537
    .local v3, "oldFlags":I
    if-eqz v0, :cond_0

    .line 538
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v3

    .line 541
    :cond_0
    const/high16 v4, 0x10000

    or-int v2, v3, v4

    .line 542
    .local v2, "newFlags":I
    if-eq v2, v3, :cond_1

    .line 543
    invoke-interface {v1, p1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 545
    :cond_1
    return-void
.end method

.method public setMobileDataAllowed(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 476
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    .line 477
    .local v1, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-interface {v1, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v0

    .line 478
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v3, 0x0

    .line 479
    .local v3, "oldFlags":I
    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v3

    .line 483
    :cond_0
    or-int/lit8 v2, v3, 0x2

    .line 484
    .local v2, "newFlags":I
    if-eq v2, v3, :cond_1

    .line 485
    invoke-interface {v1, p1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 487
    :cond_1
    return-void
.end method

.method public setMobileDataProhibited(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 491
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    .line 492
    .local v1, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-interface {v1, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v0

    .line 493
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v3, 0x0

    .line 494
    .local v3, "oldFlags":I
    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v3

    .line 498
    :cond_0
    or-int/lit16 v2, v3, 0x800

    .line 499
    .local v2, "newFlags":I
    and-int/lit8 v2, v2, -0x3

    .line 500
    if-eq v2, v3, :cond_1

    .line 501
    invoke-interface {v1, p1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 503
    :cond_1
    return-void
.end method

.method public setVisibility(Ljava/lang/String;ZZZ)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "showProgress"    # Z
    .param p3, "showErrors"    # Z
    .param p4, "showComplete"    # Z

    .prologue
    .line 508
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    .line 509
    .local v1, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-interface {v1, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v0

    .line 510
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v3, 0x0

    .line 511
    .local v3, "oldFlags":I
    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v3

    .line 515
    :cond_0
    and-int/lit16 v2, v3, -0x92

    .line 518
    .local v2, "newFlags":I
    if-nez p2, :cond_1

    .line 519
    or-int/lit8 v2, v2, 0x10

    .line 521
    :cond_1
    if-nez p3, :cond_2

    .line 522
    or-int/lit8 v2, v2, 0x1

    .line 524
    :cond_2
    if-nez p4, :cond_3

    .line 525
    or-int/lit16 v2, v2, 0x80

    .line 527
    :cond_3
    if-eq v2, v3, :cond_4

    .line 528
    invoke-interface {v1, p1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 530
    :cond_4
    return-void
.end method

.method public start(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "afterStartedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/Notifier;->hideInstallingMessage()V

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/download/DownloadQueue;->addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->attach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 165
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl$1;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 220
    return-void
.end method

.method public startDeferredInstalls()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    .line 555
    return-void
.end method

.method taskFinished(Lcom/google/android/finsky/receivers/InstallerTask;)V
    .locals 5
    .param p1, "installerTask"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    const/4 v4, 0x1

    .line 1229
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    if-eq p1, v0, :cond_0

    .line 1230
    const-string v0, "Unexpected (late?) finish of task for %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1232
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerTask:Lcom/google/android/finsky/receivers/InstallerTask;

    .line 1233
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1234
    iget-object v0, p1, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->releaseMultiUserPackage(Ljava/lang/String;)V

    .line 1236
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V

    .line 1237
    return-void
.end method

.method public uninstallAssetSilently(Ljava/lang/String;)V
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 695
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    const-string v0, "Unexpected empty package name"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 740
    :goto_0
    return-void

    .line 700
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->getInstallerTask(Ljava/lang/String;)Lcom/google/android/finsky/receivers/InstallerTask;

    move-result-object v9

    .line 701
    .local v9, "runningTask":Lcom/google/android/finsky/receivers/InstallerTask;
    if-eqz v9, :cond_1

    .line 702
    invoke-virtual {v9, v10}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 706
    :cond_1
    const/4 v8, 0x0

    .line 708
    .local v8, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 713
    :goto_1
    const/4 v6, 0x0

    .line 714
    .local v6, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    if-eqz v8, :cond_2

    .line 715
    new-instance v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .end local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-direct {v6}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 716
    .restart local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iget v0, v8, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 717
    iput-boolean v10, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 718
    iget-object v0, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v10

    :goto_2
    iput-boolean v0, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 720
    iput-boolean v10, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 722
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x72

    move-object v2, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 725
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 727
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    const/4 v1, -0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 732
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 737
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mUninstallingPackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 738
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->UNINSTALLING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-virtual {p0, p1, v0, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 739
    invoke-static {}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->uninstallPackage(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v4

    .line 718
    goto :goto_2

    .line 733
    :catch_0
    move-exception v7

    .line 734
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "Skipping uninstall of %s - already uninstalled."

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 709
    .end local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public uninstallPackagesByUid(Ljava/lang/String;)V
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 744
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 747
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v5, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 753
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    .line 755
    .local v6, "packages":[Ljava/lang/String;
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v7, v0, v2

    .line 756
    .local v7, "uidPackageName":Ljava/lang/String;
    const-string v8, "Removing package \'%s\' (child of \'%s\')"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v7, v9, v10

    aput-object p1, v9, v11

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerImpl;->uninstallAssetSilently(Ljava/lang/String;)V

    .line 755
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 748
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v6    # "packages":[Ljava/lang/String;
    .end local v7    # "uidPackageName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 749
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "Skipping uninstall of %s - already uninstalled."

    new-array v9, v11, [Ljava/lang/Object;

    aput-object p1, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 759
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    return-void
.end method

.method public updateInstalledApps(Ljava/util/List;Ljava/lang/String;ZZZI)V
    .locals 9
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "showErrors"    # Z
    .param p4, "deferred"    # Z
    .param p5, "mobileDataProhibited"    # Z
    .param p6, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            "ZZZI)V"
        }
    .end annotation

    .prologue
    .line 430
    .local p1, "documentsToUpdate":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 431
    .local v7, "document":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    .line 432
    .local v1, "packageName":Ljava/lang/String;
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, p3, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 433
    if-eqz p5, :cond_0

    .line 434
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->setMobileDataProhibited(Ljava/lang/String;)V

    .line 436
    :cond_0
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getVersionCode()I

    move-result v2

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, p2

    move v5, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/receivers/InstallerImpl;->updateSingleInstalledApp(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    .line 439
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v7    # "document":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    return-void
.end method

.method public updateSingleInstalledApp(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # I
    .param p3, "packageTitle"    # Ljava/lang/String;
    .param p4, "reason"    # Ljava/lang/String;
    .param p5, "deferred"    # Z
    .param p6, "priority"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 444
    const-string v0, "updateSingleInstalledApp"

    const/4 v1, 0x0

    invoke-static {v0, p1, p3, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v8

    .line 448
    .local v8, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v8, :cond_0

    iget-object v0, v8, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-nez v0, :cond_1

    .line 449
    :cond_0
    const-string v0, "Cannot update %s because not installed."

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    .line 472
    :goto_0
    return-void

    .line 455
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getAppDetailsAccount(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Ljava/lang/String;

    move-result-object v3

    .line 457
    .local v3, "updateAccountName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 458
    const-string v0, "Cannot update %s because cannot determine update account."

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p5

    move-object v6, p4

    move v7, p6

    .line 464
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/receivers/InstallerImpl;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method
