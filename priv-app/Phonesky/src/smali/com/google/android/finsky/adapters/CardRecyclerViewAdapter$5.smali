.class Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;
.super Ljava/lang/Object;
.source "CardRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSingleDocCluster(ILandroid/view/View;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

.field final synthetic val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$uiElementType:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;ILcom/google/android/finsky/api/model/Document;)V
    .locals 0

    .prologue
    .line 1646
    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iput p2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->val$uiElementType:I

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1650
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->val$uiElementType:I

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v3, v3, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1653
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;->val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    .line 1654
    return-void
.end method
