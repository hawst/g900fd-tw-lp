.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;
.super Ljava/lang/Object;
.source "CheckoutPurchaseSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommitListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;

    .prologue
    .line 562
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;)V
    .locals 8
    .param p1, "response"    # Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    .prologue
    .line 566
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$902(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;[B)[B

    .line 567
    iget-object v0, p1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 568
    .local v0, "status":Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/16 v2, 0x131

    iget v3, v0, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCommitServerLatencyMs()J
    invoke-static {v4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getTimeElapsedSinceCommitMs()J
    invoke-static {v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1900(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v6

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->log(IIJJ)V
    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;IIJJ)V

    .line 570
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    const-string v3, "CheckoutPurchaseSidecar.commit"

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;

    invoke-direct {v4, p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;)V

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->handleLibraryUpdates([Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;Ljava/lang/Runnable;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2700(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 610
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 562
    check-cast p1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->onResponse(Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;)V

    return-void
.end method
