.class public Lcom/google/android/finsky/layout/AppScreenshot;
.super Landroid/widget/FrameLayout;
.source "AppScreenshot.java"


# instance fields
.field private mScreenshot:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 31
    const v0, 0x7f0a00cf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AppScreenshot;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AppScreenshot;->mScreenshot:Landroid/widget/ImageView;

    .line 32
    return-void
.end method

.method public setScreenshotDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "screenshotDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppScreenshot;->mScreenshot:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    return-void
.end method
