.class Lcom/google/android/finsky/activities/RateReviewActivity$2;
.super Ljava/lang/Object;
.source "RateReviewActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/RateReviewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

.field final synthetic val$isEditingReview:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/RateReviewActivity;Z)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    iput-boolean p2, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->val$isEditingReview:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNegativeButtonClick()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 314
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$100(Lcom/google/android/finsky/activities/RateReviewActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z
    invoke-static {v1, v2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$102(Lcom/google/android/finsky/activities/RateReviewActivity;Z)Z

    .line 318
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$200(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x4b6

    iget-object v3, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v1, v2, v5, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 329
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$300(Lcom/google/android/finsky/activities/RateReviewActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$400(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$500(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocDetailsUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$600(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/utils/RateReviewHelper;->deleteReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V

    .line 334
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 335
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "doc_id"

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$500(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 337
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->finish()V

    goto :goto_0
.end method

.method public onPositiveButtonClick()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x1

    .line 271
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$100(Lcom/google/android/finsky/activities/RateReviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # setter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z
    invoke-static {v0, v12}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$102(Lcom/google/android/finsky/activities/RateReviewActivity;Z)Z

    .line 276
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->val$isEditingReview:Z

    if-eqz v0, :cond_3

    const/16 v11, 0x4b5

    .line 279
    .local v11, "type":I
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$200(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v0, v11, v8, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$300(Lcom/google/android/finsky/activities/RateReviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$400(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$500(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocDetailsUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$600(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserRating()I
    invoke-static {v3}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$700(Lcom/google/android/finsky/activities/RateReviewActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserTitle()Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$800(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserComment()Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$900(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v6}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1000(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    const/16 v9, 0x4b3

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/RateReviewHelper;->updateReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V

    .line 295
    :cond_1
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 296
    .local v10, "intent":Landroid/content/Intent;
    const-string v0, "doc_id"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$500(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const-string v0, "rating"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserRating()I
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$700(Lcom/google/android/finsky/activities/RateReviewActivity;)I

    move-result v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    const-string v0, "review_title"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserTitle()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$800(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v0, "review_comment"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->getUserComment()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$900(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$300(Lcom/google/android/finsky/activities/RateReviewActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 303
    const-string v0, "author"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1000(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 305
    :cond_2
    const-string v0, "author_title"

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1000(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const-string v1, "author_profile_image_url"

    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1000(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {v10, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v0, v12, v10}, Lcom/google/android/finsky/activities/RateReviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 309
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$2;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->finish()V

    goto/16 :goto_0

    .line 276
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v11    # "type":I
    :cond_3
    const/16 v11, 0x4b4

    goto/16 :goto_1
.end method
