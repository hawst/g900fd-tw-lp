.class public Lcom/google/android/finsky/services/PlayGearheadService;
.super Landroid/app/Service;
.source "PlayGearheadService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;
    }
.end annotation


# instance fields
.field private mPlayGearheadServiceImpl:Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/services/PlayGearheadService;->mPlayGearheadServiceImpl:Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 35
    new-instance v0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;-><init>(Lcom/google/android/finsky/services/PlayGearheadService;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/PackageStateRepository;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/PlayGearheadService;->mPlayGearheadServiceImpl:Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;

    .line 37
    return-void
.end method
