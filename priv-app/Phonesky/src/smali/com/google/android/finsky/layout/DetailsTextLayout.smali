.class public Lcom/google/android/finsky/layout/DetailsTextLayout;
.super Landroid/widget/LinearLayout;
.source "DetailsTextLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsTextLayout$MetricsListener;
    }
.end annotation


# instance fields
.field private mContentView:Landroid/widget/TextView;

.field private mCurrentMaxLines:I

.field private mDefaultMaxLines:I

.field private mMetricsListener:Lcom/google/android/finsky/layout/DetailsTextLayout$MetricsListener;

.field private mPrevWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 47
    const v0, 0x7f0a018a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    .line 48
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 66
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 67
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mMetricsListener:Lcom/google/android/finsky/layout/DetailsTextLayout$MetricsListener;

    if-nez v4, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    sub-int v2, p4, p2

    .line 72
    .local v2, "width":I
    if-lez v2, :cond_0

    iget v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mPrevWidth:I

    if-eq v4, v2, :cond_0

    .line 73
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    const v5, 0x7fffffff

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 74
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 76
    .local v3, "widthSpec":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 77
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 79
    .local v0, "fullHeight":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    iget v5, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mDefaultMaxLines:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 80
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 81
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 83
    .local v1, "truncatedHeight":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    iget v5, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mCurrentMaxLines:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 84
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v4, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 86
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mMetricsListener:Lcom/google/android/finsky/layout/DetailsTextLayout$MetricsListener;

    invoke-interface {v4, v0, v1}, Lcom/google/android/finsky/layout/DetailsTextLayout$MetricsListener;->metricsAvailable(II)V

    .line 88
    iput v2, p0, Lcom/google/android/finsky/layout/DetailsTextLayout;->mPrevWidth:I

    goto :goto_0
.end method
