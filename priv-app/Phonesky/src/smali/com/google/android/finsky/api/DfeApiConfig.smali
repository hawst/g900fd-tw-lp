.class public Lcom/google/android/finsky/api/DfeApiConfig;
.super Ljava/lang/Object;
.source "DfeApiConfig.java"


# static fields
.field public static final ageVerificationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final androidId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final authTokenType:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final backupDevicesBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final backupDevicesMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final backupDevicesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final bulkDetailsBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final bulkDetailsMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final bulkDetailsTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final clientId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeBackoffMultipler:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final earlyUpdateBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final earlyUpdateMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final earlyUpdateTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final loggingId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static maxVouchersInDetailsRequest:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgBackoffMult:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final protoLogUrlRegexp:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final purchaseStatusTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final replicateLibraryBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final replicateLibraryMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final replicateLibraryTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final sendAdIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final sendPublicAndroidIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final showStagingData:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final verifyAssociationMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final verifyAssociationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static vouchersInDetailsRequestsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x9c4

    const v7, 0x88b8

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    .line 17
    const-string v1, "finsky.mcc_mnc_override"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;

    .line 23
    const-string v0, "finsky.proto_log_url_regexp"

    const-string v1, ".*"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->protoLogUrlRegexp:Lcom/google/android/play/utils/config/GservicesValue;

    .line 32
    const-string v0, "finsky.send_public_android_id_in_requests_for_rads"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->sendPublicAndroidIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;

    .line 39
    const-string v0, "finsky.send_ad_id_in_requests_for_rads"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->sendAdIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;

    .line 45
    const-string v0, "finsky.dfe_request_timeout_ms"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->dfeRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 51
    const-string v0, "finsky.dfe_max_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->dfeMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 57
    const-string v0, "finsky.dfe_backoff_multiplier"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->dfeBackoffMultipler:Lcom/google/android/play/utils/config/GservicesValue;

    .line 63
    const-string v0, "finsky.plus_profile_bg_timeout_ms"

    const/16 v1, 0x1f40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->plusProfileBgTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 69
    const-string v0, "finsky.plus_profile_bg_max_retries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->plusProfileBgMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 75
    const-string v0, "finsky.plus_profile_bg_backoff_mult"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->plusProfileBgBackoffMult:Lcom/google/android/play/utils/config/GservicesValue;

    .line 81
    const-string v1, "finsky.ip_country_override"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;

    .line 84
    const-string v0, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 86
    const-string v0, "finsky.auth_token_type"

    const-string v1, "androidmarket"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->authTokenType:Lcom/google/android/play/utils/config/GservicesValue;

    .line 89
    const-string v0, "logging_id2"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->partnerSetting(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->loggingId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 92
    const-string v0, "market_client_id"

    const-string v1, "am-google"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->partnerSetting(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->clientId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 98
    const-string v0, "finsky.purchase_status_timeout_ms"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->purchaseStatusTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 104
    const-string v0, "finsky.age_verification_timeout_ms"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->ageVerificationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 110
    const-string v0, "finsky.backup_devices_timeout_ms"

    const/16 v1, 0x3a98

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 116
    const-string v0, "finsky.backup_devices_max_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 122
    const-string v0, "finsky.backup_devices_backoff_multiplier"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    .line 129
    const-string v0, "finsky.bulk_details_timeout_ms"

    const/16 v1, 0x7530

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 135
    const-string v0, "finsky.bulk_details_max_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 141
    const-string v0, "finsky.bulk_details_backoff_multiplier"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    .line 148
    const-string v0, "finsky.verify_association_timeout_ms"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->verifyAssociationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 154
    const-string v0, "finsky.verify_association_max_retries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->verifyAssociationMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 160
    const-string v0, "finsky.replicate_library_timeout_ms"

    const/16 v1, 0x7530

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 166
    const-string v0, "finsky.replicate_library_max_retries"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 172
    const-string v0, "finsky.replicate_library_backoff_multiplier"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    .line 178
    const-string v0, "finsky.early_update_timeout_ms"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 184
    const-string v0, "finsky.early_update_max_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 190
    const-string v0, "finsky.early_update_backoff_multiplier"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    .line 197
    const-string v0, "finsky.skip_all_caches"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    .line 201
    const-string v0, "finsky.show_staging_data"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    .line 205
    const-string v0, "finsky.prex_disabled"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 211
    const-string v0, "finsky.vouchers_in_details_requests_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->vouchersInDetailsRequestsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 220
    const-string v0, "finsky.max_vouchers_in_details_request"

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->maxVouchersInDetailsRequest:Lcom/google/android/play/utils/config/GservicesValue;

    return-void
.end method
