.class Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
.super Ljava/lang/Object;
.source "TabbedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/TabbedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TabData"
.end annotation


# instance fields
.field public final browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

.field public dfeList:Lcom/google/android/finsky/api/model/DfeList;

.field public elementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

.field private instanceState:Lcom/google/android/finsky/utils/ObjectMap;

.field public final isCategoryTab:Z

.field public quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field public viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/protos/Browse$BrowseTab;Z)V
    .locals 0
    .param p1, "browseTab"    # Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .param p2, "isCategoryTab"    # Z

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    .line 98
    iput-boolean p2, p0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->isCategoryTab:Z

    .line 99
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;)Lcom/google/android/finsky/utils/ObjectMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/utils/ObjectMap;)Lcom/google/android/finsky/utils/ObjectMap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    .param p1, "x1"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object p1
.end method
