.class public Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewListingSmall.java"


# instance fields
.field private mRatingBadge:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getCardType()I
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x4

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 34
    const v0, 0x7f0a02d8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    .line 35
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 36
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingLeft()I

    move-result v17

    .line 129
    .local v17, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingRight()I

    move-result v18

    .line 130
    .local v18, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingTop()I

    move-result v19

    .line 131
    .local v19, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingBottom()I

    move-result v16

    .line 133
    .local v16, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getWidth()I

    move-result v32

    .line 134
    .local v32, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getHeight()I

    move-result v5

    .line 136
    .local v5, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v27

    .line 137
    .local v27, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v34

    add-int v34, v34, v17

    add-int v35, v19, v27

    move-object/from16 v0, v33

    move/from16 v1, v17

    move/from16 v2, v19

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v28

    check-cast v28, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 142
    .local v28, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v30

    check-cast v30, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 144
    .local v30, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v24

    check-cast v24, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 146
    .local v24, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 148
    .local v21, "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 150
    .local v7, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 153
    .local v13, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v33, v0

    add-int v33, v33, v17

    move-object/from16 v0, v28

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v34, v0

    add-int v26, v33, v34

    .line 155
    .local v26, "textContentLeft":I
    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v31, v19, v33

    .line 156
    .local v31, "titleTop":I
    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v33, v0

    add-int v29, v26, v33

    .line 157
    .local v29, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v34

    add-int v34, v34, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v35

    add-int v35, v35, v31

    move-object/from16 v0, v33

    move/from16 v1, v29

    move/from16 v2, v31

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 160
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v15, v31, v33

    .line 161
    .local v15, "overflowTop":I
    sub-int v33, v32, v18

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v34, v0

    sub-int v14, v33, v34

    .line 162
    .local v14, "overflowRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v34

    sub-int v34, v14, v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v35

    add-int v35, v35, v15

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v15, v14, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v33

    add-int v33, v33, v31

    move-object/from16 v0, v30

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v34, v0

    add-int v33, v33, v34

    move-object/from16 v0, v24

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v34, v0

    add-int v25, v33, v34

    .line 167
    .local v25, "subtitleTop":I
    move-object/from16 v0, v24

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v33, v0

    add-int v23, v26, v33

    .line 168
    .local v23, "subtitleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v34

    add-int v34, v34, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v35

    add-int v35, v35, v25

    move-object/from16 v0, v33

    move/from16 v1, v23

    move/from16 v2, v25

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v33

    add-int v33, v33, v25

    move-object/from16 v0, v24

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v34, v0

    add-int v33, v33, v34

    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v34, v0

    add-int v22, v33, v34

    .line 174
    .local v22, "ratingBadgeTop":I
    move-object/from16 v0, v21

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v33, v0

    add-int v20, v26, v33

    .line 175
    .local v20, "ratingBadgeLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getMeasuredWidth()I

    move-result v34

    add-int v34, v34, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/View;->getMeasuredHeight()I

    move-result v35

    add-int v35, v35, v22

    move-object/from16 v0, v33

    move/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 179
    sub-int v33, v5, v16

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v34, v0

    sub-int v6, v33, v34

    .line 180
    .local v6, "labelBottom":I
    sub-int v33, v32, v18

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v34, v0

    sub-int v8, v33, v34

    .line 181
    .local v8, "labelRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v34

    sub-int v34, v8, v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v35

    sub-int v35, v6, v35

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2, v8, v6}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    .line 185
    .local v12, "loadingWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 186
    .local v9, "loadingHeight":I
    sub-int v33, v32, v17

    sub-int v33, v33, v18

    sub-int v33, v33, v12

    div-int/lit8 v33, v33, 0x2

    add-int v10, v17, v33

    .line 187
    .local v10, "loadingLeft":I
    sub-int v33, v5, v19

    sub-int v33, v33, v16

    sub-int v33, v33, v9

    div-int/lit8 v33, v33, 0x2

    add-int v11, v19, v33

    .line 188
    .local v11, "loadingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getMeasuredWidth()I

    move-result v34

    add-int v34, v34, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/View;->getMeasuredHeight()I

    move-result v35

    add-int v35, v35, v11

    move-object/from16 v0, v33

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->recomputeOverflowAreaIfNeeded()V

    .line 193
    return-void
.end method

.method protected onMeasure(II)V
    .locals 29
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 39
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->measureThumbnailSpanningHeight(I)V

    .line 41
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v25

    .line 42
    .local v25, "width":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 44
    .local v4, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 46
    .local v22, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 48
    .local v23, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 50
    .local v18, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 52
    .local v15, "ratingBadgeLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 55
    .local v6, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingLeft()I

    move-result v10

    .line 56
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingRight()I

    move-result v11

    .line 57
    .local v11, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingTop()I

    move-result v12

    .line 58
    .local v12, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingBottom()I

    move-result v9

    .line 60
    .local v9, "paddingBottom":I
    sub-int v26, v4, v12

    sub-int v2, v26, v9

    .line 61
    .local v2, "contentHeight":I
    const/high16 v26, 0x40000000    # 2.0f

    move/from16 v0, v26

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 63
    .local v3, "contentHeightSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v26, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v27, v0

    const/high16 v28, 0x40000000    # 2.0f

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v3}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 66
    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    move/from16 v26, v0

    add-int v26, v26, v10

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    add-int v20, v26, v27

    .line 67
    .local v20, "textContentLeft":I
    sub-int v26, v25, v20

    sub-int v21, v26, v11

    .line 70
    .local v21, "textContentWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/widget/ImageView;->measure(II)V

    .line 73
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v26, v0

    sub-int v26, v21, v26

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    sub-int v24, v26, v27

    .line 74
    .local v24, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v24

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/widget/TextView;->measure(II)V

    .line 78
    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v26, v0

    sub-int v26, v21, v26

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    sub-int v19, v26, v27

    .line 79
    .local v19, "subtitleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v26, v0

    const/high16 v27, -0x80000000

    move/from16 v0, v19

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 83
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v26, v0

    sub-int v27, v25, v10

    sub-int v27, v27, v11

    const/high16 v28, -0x80000000

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 88
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v26, v0

    sub-int v26, v21, v26

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    sub-int v17, v26, v27

    .line 90
    .local v17, "ratingBadgeWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v27

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/view/View;->measure(II)V

    .line 94
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v26, v0

    add-int v26, v26, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getMeasuredHeight()I

    move-result v27

    add-int v13, v26, v27

    .line 98
    .local v13, "ratingBadgeBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getMeasuredHeight()I

    move-result v26

    sub-int v26, v26, v9

    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v27

    sub-int v7, v26, v27

    .line 100
    .local v7, "labelTop":I
    if-le v13, v7, :cond_0

    .line 103
    sub-int v26, v25, v11

    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v27

    sub-int v26, v26, v27

    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    sub-int v5, v26, v27

    .line 106
    .local v5, "labelLeft":I
    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v26, v0

    add-int v26, v26, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    add-int v26, v26, v27

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v27, v0

    add-int v14, v26, v27

    .line 109
    .local v14, "ratingBadgeLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getMeasuredWidth()I

    move-result v26

    add-int v16, v14, v26

    .line 110
    .local v16, "ratingBadgeRight":I
    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v26, v0

    add-int v26, v26, v16

    move/from16 v0, v26

    if-le v0, v5, :cond_0

    .line 114
    sub-int v26, v5, v14

    iget v0, v15, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v27, v0

    sub-int v8, v26, v27

    .line 115
    .local v8, "newRatingBadgeWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadge:Landroid/view/View;

    move-object/from16 v26, v0

    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v27

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/view/View;->measure(II)V

    .line 121
    .end local v5    # "labelLeft":I
    .end local v8    # "newRatingBadgeWidth":I
    .end local v14    # "ratingBadgeLeft":I
    .end local v16    # "ratingBadgeRight":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Landroid/view/View;->measure(II)V

    .line 123
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->setMeasuredDimension(II)V

    .line 124
    return-void
.end method
