.class public final Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ContentFlagging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ContentFlagging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FlagContentResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;->clear()Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    .line 99
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;
    .locals 1

    .prologue
    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;->cachedSize:I

    .line 103
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 112
    .local v0, "tag":I
    packed-switch v0, :pswitch_data_0

    .line 116
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    :pswitch_0
    return-object p0

    .line 112
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    move-result-object v0

    return-object v0
.end method
