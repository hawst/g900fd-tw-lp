.class public final Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseDeclinedData"
.end annotation


# instance fields
.field public hasReason:Z

.field public hasShowNotification:Z

.field public reason:I

.field public showNotification:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->clear()Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    .line 234
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    iput v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    .line 238
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasReason:Z

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    .line 240
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasShowNotification:Z

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->cachedSize:I

    .line 242
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 259
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 260
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasReason:Z

    if-eqz v1, :cond_1

    .line 261
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasShowNotification:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    if-eq v1, v2, :cond_3

    .line 265
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 276
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 277
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 281
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 282
    :sswitch_0
    return-object p0

    .line 287
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 288
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 294
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    .line 295
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasReason:Z

    goto :goto_0

    .line 301
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    .line 302
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasShowNotification:Z

    goto :goto_0

    .line 277
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 248
    iget v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasReason:Z

    if-eqz v0, :cond_1

    .line 249
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 251
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->hasShowNotification:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    if-eq v0, v1, :cond_3

    .line 252
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->showNotification:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 254
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 255
    return-void
.end method
