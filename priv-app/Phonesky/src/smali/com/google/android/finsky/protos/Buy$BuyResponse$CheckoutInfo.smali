.class public final Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy$BuyResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckoutInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    }
.end annotation


# instance fields
.field public addInstrumentUrl:Ljava/lang/String;

.field public checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

.field public deprecatedCheckoutUrl:Ljava/lang/String;

.field public eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field public eligibleInstrumentFamily:[I

.field public footerHtml:[Ljava/lang/String;

.field public footnoteHtml:[Ljava/lang/String;

.field public hasAddInstrumentUrl:Z

.field public hasDeprecatedCheckoutUrl:Z

.field public item:Lcom/google/android/finsky/protos/Buy$LineItem;

.field public subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 747
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->clear()Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    .line 748
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 751
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 752
    invoke-static {}, Lcom/google/android/finsky/protos/Buy$LineItem;->emptyArray()[Lcom/google/android/finsky/protos/Buy$LineItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 753
    invoke-static {}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->emptyArray()[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    .line 754
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    .line 755
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasDeprecatedCheckoutUrl:Z

    .line 756
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    .line 757
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasAddInstrumentUrl:Z

    .line 758
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    .line 759
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    .line 760
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    .line 761
    invoke-static {}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 762
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->cachedSize:I

    .line 763
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 828
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 829
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_0

    .line 830
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 833
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 834
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-ge v3, v5, :cond_2

    .line 835
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v2, v5, v3

    .line 836
    .local v2, "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v2, :cond_1

    .line 837
    const/4 v5, 0x4

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 834
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 842
    .end local v2    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    array-length v5, v5

    if-lez v5, :cond_4

    .line 843
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 844
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    aget-object v2, v5, v3

    .line 845
    .local v2, "element":Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    if-eqz v2, :cond_3

    .line 846
    const/4 v5, 0x5

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 843
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 851
    .end local v2    # "element":Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    .end local v3    # "i":I
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasDeprecatedCheckoutUrl:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 852
    :cond_5
    const/16 v5, 0xa

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 855
    :cond_6
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasAddInstrumentUrl:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 856
    :cond_7
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 859
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_b

    .line 860
    const/4 v0, 0x0

    .line 861
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 862
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_a

    .line 863
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 864
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_9

    .line 865
    add-int/lit8 v0, v0, 0x1

    .line 866
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 862
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 870
    .end local v2    # "element":Ljava/lang/String;
    :cond_a
    add-int/2addr v4, v1

    .line 871
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 873
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v5, v5

    if-lez v5, :cond_d

    .line 874
    const/4 v1, 0x0

    .line 875
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v5, v5

    if-ge v3, v5, :cond_c

    .line 876
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    aget v2, v5, v3

    .line 877
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 875
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 880
    .end local v2    # "element":I
    :cond_c
    add-int/2addr v4, v1

    .line 881
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 883
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_d
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_10

    .line 884
    const/4 v0, 0x0

    .line 885
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 886
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_f

    .line 887
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 888
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_e

    .line 889
    add-int/lit8 v0, v0, 0x1

    .line 890
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 886
    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 894
    .end local v2    # "element":Ljava/lang/String;
    :cond_f
    add-int/2addr v4, v1

    .line 895
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 897
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v5, v5

    if-lez v5, :cond_12

    .line 898
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v5, v5

    if-ge v3, v5, :cond_12

    .line 899
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    aget-object v2, v5, v3

    .line 900
    .local v2, "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v2, :cond_11

    .line 901
    const/16 v5, 0x2c

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 898
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 906
    .end local v2    # "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v3    # "i":I
    :cond_12
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 914
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 915
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 919
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 920
    :sswitch_0
    return-object p0

    .line 925
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v14, :cond_1

    .line 926
    new-instance v14, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 928
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 932
    :sswitch_2
    const/16 v14, 0x22

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 934
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v14, :cond_3

    const/4 v3, 0x0

    .line 935
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 937
    .local v7, "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v3, :cond_2

    .line 938
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 940
    :cond_2
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_4

    .line 941
    new-instance v14, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v14, v7, v3

    .line 942
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 943
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 940
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 934
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v3, v14

    goto :goto_1

    .line 946
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_4
    new-instance v14, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v14, v7, v3

    .line 947
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 948
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    goto :goto_0

    .line 952
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :sswitch_3
    const/16 v14, 0x2b

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 954
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    if-nez v14, :cond_6

    const/4 v3, 0x0

    .line 955
    .restart local v3    # "i":I
    :goto_3
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    .line 957
    .local v7, "newArray":[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    if-eqz v3, :cond_5

    .line 958
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 960
    :cond_5
    :goto_4
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_7

    .line 961
    new-instance v14, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;-><init>()V

    aput-object v14, v7, v3

    .line 962
    aget-object v14, v7, v3

    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 963
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 960
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 954
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    array-length v3, v14

    goto :goto_3

    .line 966
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    :cond_7
    new-instance v14, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;-><init>()V

    aput-object v14, v7, v3

    .line 967
    aget-object v14, v7, v3

    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 968
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    goto/16 :goto_0

    .line 972
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    .line 973
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasDeprecatedCheckoutUrl:Z

    goto/16 :goto_0

    .line 977
    :sswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    .line 978
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasAddInstrumentUrl:Z

    goto/16 :goto_0

    .line 982
    :sswitch_6
    const/16 v14, 0xa2

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 984
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    if-nez v14, :cond_9

    const/4 v3, 0x0

    .line 985
    .restart local v3    # "i":I
    :goto_5
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 986
    .local v7, "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_8

    .line 987
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 989
    :cond_8
    :goto_6
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_a

    .line 990
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 991
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 989
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 984
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_5

    .line 994
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 995
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 999
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_7
    const/16 v14, 0xf8

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 1001
    .local v5, "length":I
    new-array v12, v5, [I

    .line 1002
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 1003
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_7
    if-ge v3, v5, :cond_c

    .line 1004
    if-eqz v3, :cond_b

    .line 1005
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1007
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 1008
    .local v13, "value":I
    sparse-switch v13, :sswitch_data_1

    move v10, v11

    .line 1003
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_8
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_7

    .line 1020
    :sswitch_8
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_8

    .line 1024
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_c
    if-eqz v11, :cond_0

    .line 1025
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    if-nez v14, :cond_d

    const/4 v3, 0x0

    .line 1026
    :goto_9
    if-nez v3, :cond_e

    array-length v14, v12

    if-ne v11, v14, :cond_e

    .line 1027
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    goto/16 :goto_0

    .line 1025
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v3, v14

    goto :goto_9

    .line 1029
    :cond_e
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 1030
    .local v7, "newArray":[I
    if-eqz v3, :cond_f

    .line 1031
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1033
    :cond_f
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1034
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    goto/16 :goto_0

    .line 1040
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 1041
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 1043
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 1044
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 1045
    .local v8, "startPos":I
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_10

    .line 1046
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    sparse-switch v14, :sswitch_data_2

    goto :goto_a

    .line 1058
    :sswitch_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1062
    :cond_10
    if-eqz v1, :cond_14

    .line 1063
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 1064
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    if-nez v14, :cond_12

    const/4 v3, 0x0

    .line 1065
    .restart local v3    # "i":I
    :goto_b
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 1066
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_11

    .line 1067
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1069
    :cond_11
    :goto_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_13

    .line 1070
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 1071
    .restart local v13    # "value":I
    sparse-switch v13, :sswitch_data_3

    goto :goto_c

    .line 1083
    :sswitch_b
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_c

    .line 1064
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_12
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v3, v14

    goto :goto_b

    .line 1087
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_13
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    .line 1089
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_14
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1093
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_c
    const/16 v14, 0x122

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 1095
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    if-nez v14, :cond_16

    const/4 v3, 0x0

    .line 1096
    .restart local v3    # "i":I
    :goto_d
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 1097
    .local v7, "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_15

    .line 1098
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1100
    :cond_15
    :goto_e
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_17

    .line 1101
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 1102
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1100
    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    .line 1095
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_d

    .line 1105
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_17
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 1106
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1110
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_d
    const/16 v14, 0x162

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 1112
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v14, :cond_19

    const/4 v3, 0x0

    .line 1113
    .restart local v3    # "i":I
    :goto_f
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 1115
    .local v7, "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v3, :cond_18

    .line 1116
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1118
    :cond_18
    :goto_10
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_1a

    .line 1119
    new-instance v14, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    aput-object v14, v7, v3

    .line 1120
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1121
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1118
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    .line 1112
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v3, v14

    goto :goto_f

    .line 1124
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_1a
    new-instance v14, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    aput-object v14, v7, v3

    .line 1125
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1126
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto/16 :goto_0

    .line 915
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2b -> :sswitch_3
        0x52 -> :sswitch_4
        0x5a -> :sswitch_5
        0xa2 -> :sswitch_6
        0xf8 -> :sswitch_7
        0xfa -> :sswitch_9
        0x122 -> :sswitch_c
        0x162 -> :sswitch_d
    .end sparse-switch

    .line 1008
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_8
        0x1 -> :sswitch_8
        0x2 -> :sswitch_8
        0x3 -> :sswitch_8
        0x4 -> :sswitch_8
        0x5 -> :sswitch_8
        0x6 -> :sswitch_8
        0x7 -> :sswitch_8
        0x8 -> :sswitch_8
        0x9 -> :sswitch_8
        0x64 -> :sswitch_8
    .end sparse-switch

    .line 1046
    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_a
        0x1 -> :sswitch_a
        0x2 -> :sswitch_a
        0x3 -> :sswitch_a
        0x4 -> :sswitch_a
        0x5 -> :sswitch_a
        0x6 -> :sswitch_a
        0x7 -> :sswitch_a
        0x8 -> :sswitch_a
        0x9 -> :sswitch_a
        0x64 -> :sswitch_a
    .end sparse-switch

    .line 1071
    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_b
        0x1 -> :sswitch_b
        0x2 -> :sswitch_b
        0x3 -> :sswitch_b
        0x4 -> :sswitch_b
        0x5 -> :sswitch_b
        0x6 -> :sswitch_b
        0x7 -> :sswitch_b
        0x8 -> :sswitch_b
        0x9 -> :sswitch_b
        0x64 -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 769
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_0

    .line 770
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->item:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 772
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 773
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 774
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v0, v2, v1

    .line 775
    .local v0, "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v0, :cond_1

    .line 776
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 773
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 780
    .end local v0    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 781
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 782
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->checkoutOption:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    aget-object v0, v2, v1

    .line 783
    .local v0, "element":Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    if-eqz v0, :cond_3

    .line 784
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 781
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 788
    .end local v0    # "element":Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    .end local v1    # "i":I
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasDeprecatedCheckoutUrl:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 789
    :cond_5
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->deprecatedCheckoutUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 791
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->hasAddInstrumentUrl:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 792
    :cond_7
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->addInstrumentUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 794
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 795
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 796
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footerHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 797
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 798
    const/16 v2, 0x14

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 795
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 802
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v2, v2

    if-lez v2, :cond_b

    .line 803
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 804
    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrumentFamily:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 803
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 807
    .end local v1    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 808
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 809
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->footnoteHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 810
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_c

    .line 811
    const/16 v2, 0x24

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 808
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 815
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 816
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 817
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;->eligibleInstrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    aget-object v0, v2, v1

    .line 818
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v0, :cond_e

    .line 819
    const/16 v2, 0x2c

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 816
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 823
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v1    # "i":I
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 824
    return-void
.end method
