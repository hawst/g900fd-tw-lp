.class public Lcom/google/android/finsky/config/G;
.super Ljava/lang/Object;
.source "G.java"


# static fields
.field public static final GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

.field public static final additionalExperiments:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static analyticsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static antiMalwareActivityEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreAppIconMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreDownloadMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreFailsafeMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreFetchListMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreHttpStatusBlacklist:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreRetryDownloadHoldoffHighPriorityMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreRetryDownloadHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreRetryFetchListHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static appsWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDeliveryHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogFirstBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogSecondBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogSubsequentBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateExcludeForegroundServices:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateExcludeRunningPackagesPre:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateJobSchedulerGearheadDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateJobSchedulerGearheadTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateJobSchedulerTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateWifiCheckIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static booksWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static checkoutPrivacyPolicyUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final clientMutationCacheCirclesModelCacheSize:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final consistencyTokenEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static consumptionAppDataFilter:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final consumptionAppImageTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final consumptionAppTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static contentFilterInfoUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final continueUrlExpirationMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final corpusAwarenessThresholdBooks:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final corpusAwarenessThresholdMagazines:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final corpusAwarenessThresholdMovies:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final corpusAwarenessThresholdMusic:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneBootDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneHoldoffDuringSelfUpdate:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneHoldoffIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneImmediateDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneImmediateRunVersionCodeHigh:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneImmediateRunVersionCodeLow:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneInitialJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneProvisionHoldoffMaxMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneRegularIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneRetryIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dcb2SetupDelayTosLoading:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3PassphraseKeyVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3PrepareSendDeviceRebooted:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3SetupWhileRoamingEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3VerifyAssociationRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static dcbDebugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugAlwaysPromptForGaiaRecovery:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugLimitedUserState:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final defaultPurchaseAuthentication:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final deliveryDataExpirationMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static dfeLogsAdMobEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadBytesOverMobileMaximum:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadBytesOverMobileRecommended:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadExternalFileSizeMinBytes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadExternalFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadExternalFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadFreeSpaceApkSizeFactor:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadPatchFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadSendBaseVersionCode:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final editPaymentMethodUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final eduDevicePolicyApp:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static enableCorpusWidgets:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableDcbReducedBillingAddress:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableGaiaRecovery:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enablePeopleSuggestWidget:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enablePlayLogs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableRemoveAppsFromLibrary:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableReviewComments:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableSensitiveLogging:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableSessionStatsLog:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableThirdPartyDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableThirdPartyDirectPurchases:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableThirdPartyInlineAppInstalls:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final externalReferrerLifespanMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final firstNewsstandAppVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static forcePostPurchaseCrossSell:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final gaiaAuthValidityMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final gaiaOptOutLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static gamesBrowseUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gelPackageName:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gmsCoreAutoUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static gtvNavigationalSuggestEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final hasFopCacheTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final helpCenterBackgroundDataUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final iabV3FirstPartyApiEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final iabV3FirstPartyApiUnrestrictedAccess:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static iabV3MaxPurchasesInResponse:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static iabV3NetworkTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static iabV3SkuDetailsMaxSize:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final iabV3SubscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final initializeBillingDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final instrumentCheckLifetimeMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final launchUrlsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static legalNoticeMenuTitleOverride:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static legalNoticeMenuUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final libraryReplicatorRescheduleDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final lightPurchaseAutoDismissMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lightPurchaseOptimisticProvisioning:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final logBillingEventsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final logInlineAppInstallReferrerEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static logsDispatchIntervalSeconds:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final maxAutoUpdateDialogShownCount:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static maxLogQueueSizeBeforeFlush:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumMusicVersionCode:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumNumberOfRecs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumVideosVersionCodeHoneycombPlus:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumVideosVersionCodePreHoneycomb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static moviesWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicAllAccessSignUpIntentUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicAllAccessSubscriptionBackendDocid:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicAppSubscriptionBackendDocidBlacklist:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicAppSubscriptionResignupEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicDmcaReportLink:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static musicWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgeMidtermScores:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetAlwaysIncludeMostRecentBackend:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetBackgroundImageLocation:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetHasContentScoreMap:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetImportantContributionFraction:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetInteractionClusterMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetMidtermDurationMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermDecay:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermDurationMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermScores:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupConfigurationId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupCookieName:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupCookieNameTestKeys:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupCookieValue:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupCookieValueTestKeys:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupDowngradeFlag:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupExpectedSignature:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupExpectedSignatureTestKeys:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupFlagsMask:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupFlagsSet:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupLogCommonStatuses:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupNlpVersionMax:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupNlpVersionMin:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupUrlTestKeys:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpReinstallSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpReinstallSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpSharedUserId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final outstandingNotificationTimeDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final passwordMaxFailedAttempts:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final passwordRecoveryUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final peoplePageWarmWelcomeButtonIconUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final peoplePageWarmWelcomeGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesApiBiasRadiusMeters:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesApiKey:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static platformAntiMalwareDialogDelayMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static platformAntiMalwareEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesDocId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesGmsCoreMinVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionFirstBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionMaxShownCount:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionOnlyWhenGameFeatures:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionSubsequentBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogImpressionSettleTimeMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogMaxStorageSize:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogMinDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogRecommendedFileSize:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogServerUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final positiveRateThreshold:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final postPurchaseSharingEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final prePurchaseSharingEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final preserveForwardLockApiMax:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final preserveForwardLockApiMin:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopSnoozeScheduleM:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static readBookUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final recentlyUpdatedWindowSizeMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationTtlMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationsFetchTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationsWidgetFlipIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static recsCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static redeemTermsAndConditionsUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final reducedBillingAddressRequiresPhonenumber:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final resetPinUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final selectAddinstrumentByDefault:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardEarlyUpdateDeadlockMaxMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardEarlyUpdateEnable:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static setupWizardForceResizeForKeyboardInFullscreen:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardPaymentHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardPaymentHeaderWideGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreFinalHoldHeaderLandscapeUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreFinalHoldHeaderTabletUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreFinalHoldHeaderUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreFinalHoldLongMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreFinalHoldShortMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final setupWizardRestoreWideHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final skipAutoUpdateCleanupClientVersionCheck:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final subscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final supportUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final supportedAccountTypes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final tabskyMinVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static userCheckForSelfUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAddressServerUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmExpirationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmMaxTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmMinTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlwaysSendConfig:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static vendingAuthTokenType:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingBillingCountriesRefreshMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierCredentialsBufferMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningRefreshFrequencyMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningRetryMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningUseTosVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingContentRating:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbConnectionType:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbMmsFallback:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbPollTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbProxyHost:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbProxyPort:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingHideContentRating:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static vendingSecureAuthTokenType:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static verifyInstalledPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static verifyInstalledPackagesMinimumWaitMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static verifyInstalledPackagesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static verifyInstalledPlayPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final videoAppTrailerPlaybackMinVersion:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final walletEscrowUrl:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final whitelistedPlacesApiCountries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/32 v8, 0x240c8400

    const/4 v7, -0x1

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android_id"

    aput-object v1, v0, v5

    const-string v1, "finsky."

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "vending_"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "logging_id2"

    aput-object v2, v0, v1

    const-string v1, "market_client_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/config/G;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    .line 35
    const-string v0, "finsky.main_cache_size_mb"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 38
    const-string v0, "finsky.image_cache_size_mb"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 41
    const-string v0, "finsky.recs_cache_size_mb"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recsCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 44
    const-string v0, "finsky.debug_options_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 51
    const-string v0, "finsky.user_check_for_self_update_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->userCheckForSelfUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 58
    const-string v0, "finsky.debug_limited_user_state"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugLimitedUserState:Lcom/google/android/play/utils/config/GservicesValue;

    .line 64
    const-string v0, "finsky.always_prompt_for_gaia_recovery"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugAlwaysPromptForGaiaRecovery:Lcom/google/android/play/utils/config/GservicesValue;

    .line 70
    const-string v0, "finsky.enable_gaia_recovery"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableGaiaRecovery:Lcom/google/android/play/utils/config/GservicesValue;

    .line 76
    const-string v0, "finsky.gtv_navigational_suggestions_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gtvNavigationalSuggestEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 82
    const-string v0, "finsky.extra_menu_item_title"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->legalNoticeMenuTitleOverride:Lcom/google/android/play/utils/config/GservicesValue;

    .line 89
    const-string v0, "finsky.extra_menu_item_url"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->legalNoticeMenuUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 92
    const-string v0, "finsky.games_browse_url"

    const-string v1, "home?c=3&cat=GAME"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gamesBrowseUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 99
    const-string v0, "finsky.dfe_logs_admob_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dfeLogsAdMobEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 103
    const-string v0, "finsky.logs_dispatch_interval_seconds"

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->logsDispatchIntervalSeconds:Lcom/google/android/play/utils/config/GservicesValue;

    .line 107
    const-string v0, "finsky.maximum_log_queue_size"

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->maxLogQueueSizeBeforeFlush:Lcom/google/android/play/utils/config/GservicesValue;

    .line 110
    const-string v0, "vending.auth_token_type"

    const-string v1, "android"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAuthTokenType:Lcom/google/android/play/utils/config/GservicesValue;

    .line 113
    const-string v0, "vending.secure_auth_token_type"

    const-string v1, "androidsecure"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingSecureAuthTokenType:Lcom/google/android/play/utils/config/GservicesValue;

    .line 116
    const-string v0, "finsky.checkout_privacy_policy_url"

    const-string v1, "https://wallet.google.com/files/privacy.html?hl=%lang%"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->checkoutPrivacyPolicyUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 120
    const-string v0, "finsky.redeem_terms_and_conditions_url"

    const-string v1, "https://play.google.com/intl/%locale%/about/redeem-terms.html"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->redeemTermsAndConditionsUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 124
    const-string v0, "finsky.enable_corpus_widgets"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableCorpusWidgets:Lcom/google/android/play/utils/config/GservicesValue;

    .line 127
    const-string v0, "finsky.widget_url"

    const-string v1, "promo?c=3"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appsWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 130
    const-string v0, "finsky.music_widget_url"

    const-string v1, "promo?c=2"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 133
    const-string v0, "finsky.books_widget_url"

    const-string v1, "promo?c=1"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->booksWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 136
    const-string v0, "finsky.movies_widget_url"

    const-string v1, "promo?c=4"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->moviesWidgetUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 143
    const-string v0, "finsky.consumption_app_data_filter"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppDataFilter:Lcom/google/android/play/utils/config/GservicesValue;

    .line 146
    const-string v0, "finsky.read_book_url"

    const-string v1, "http://books.google.com/ebooks/reader"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->readBookUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 150
    const-string v0, "finsky.content_filter_info_url"

    const-string v1, "http://www.google.com/support/androidmarket/bin/answer.py?answer=1075738"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->contentFilterInfoUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 154
    const-string v0, "finsky.analytics_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->analyticsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 157
    const-string v0, "finsky.wallet_escrow_url"

    const-string v1, "https://wallet.google.com/dehEfe"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->walletEscrowUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 160
    const-string v0, "finsky.music_dmca_report_link"

    const-string v1, "https://www.google.com/support/artists?p=DMCA"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicDmcaReportLink:Lcom/google/android/play/utils/config/GservicesValue;

    .line 163
    const-string v0, "finsky.permission_buckets_learn_more_link"

    const-string v1, "https://support.google.com/googleplay/?p=app_permissions"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    .line 167
    const-string v0, "finsky.gaia_opt_out_learn_more_link"

    const-string v1, "http://support.google.com/googleplay/bin/answer.py?&answer=2889951"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gaiaOptOutLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    .line 175
    const-string v0, "finsky.initialize_billing_delay_ms"

    const/16 v1, 0x2710

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->initializeBillingDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 182
    const-string v0, "finsky.has_fop_cache_timeout_ms"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->hasFopCacheTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 201
    const-string v0, "finsky.prompt_for_fop_snooze_schedule_m"

    const-string v1, "10080"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->promptForFopSnoozeScheduleM:Lcom/google/android/play/utils/config/GservicesValue;

    .line 207
    const-string v0, "finsky.log_billing_events_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->logBillingEventsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 213
    const-string v0, "finsky.enable_sensitive_logging"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableSensitiveLogging:Lcom/google/android/play/utils/config/GservicesValue;

    .line 217
    const-string v0, "finsky.enable_playlog"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enablePlayLogs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 221
    const-string v0, "finsky.force_post_purchase_cross_sell"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->forcePostPurchaseCrossSell:Lcom/google/android/play/utils/config/GservicesValue;

    .line 225
    const-string v0, "finsky.playlog_max_storage_size_bytes"

    const-wide/32 v2, 0x200000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogMaxStorageSize:Lcom/google/android/play/utils/config/GservicesValue;

    .line 229
    const-string v0, "finsky.playlog_recommended_file_size_bytes"

    const-wide/32 v2, 0x8000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogRecommendedFileSize:Lcom/google/android/play/utils/config/GservicesValue;

    .line 233
    const-string v0, "finsky.playlog_delay_between_uploads_ms"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 237
    const-string v0, "finsky.playlog_min_delay_between_uploads_ms"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogMinDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 246
    const-string v0, "finsky.play_log_server_url"

    const-string v1, "https://android.clients.google.com/play/log"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogServerUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 250
    const-string v0, "finsky.playlog_impression_settle_time_ms"

    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogImpressionSettleTimeMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 256
    const-string v0, "finsky.vending_request_timeout_ms"

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 262
    const-string v0, "finsky.tabsky_min_version"

    const/16 v1, 0x6e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->tabskyMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 268
    const-string v0, "finsky.volley_buffer_pool_size_kb"

    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 274
    const-string v0, "finsky.reduced_billing_address_requires_phonenumber"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->reducedBillingAddressRequiresPhonenumber:Lcom/google/android/play/utils/config/GservicesValue;

    .line 280
    const-string v0, "finsky.enable_dcb_reduced_billing_address"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableDcbReducedBillingAddress:Lcom/google/android/play/utils/config/GservicesValue;

    .line 287
    const-string v0, "vending_always_send_config"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlwaysSendConfig:Lcom/google/android/play/utils/config/GservicesValue;

    .line 291
    const-string v0, "vending_address_server_url"

    const-string v1, "http://i18napis.appspot.com/address"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAddressServerUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 298
    const-string v0, "vending_carrier_ref_freq_ms"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningRefreshFrequencyMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 305
    const-string v0, "vending_carrier_prov_retry_ms"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningRetryMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 311
    const-string v0, "vending_carrier_prov_use_tos_version"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningUseTosVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 317
    const-string v0, "vending_dcb_connection_type"

    const-string v1, "HIPRI"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbConnectionType:Lcom/google/android/play/utils/config/GservicesValue;

    .line 323
    const-string v1, "vending_dcb_proxy_host"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbProxyHost:Lcom/google/android/play/utils/config/GservicesValue;

    .line 329
    const-string v0, "vending_dcb_proxy_port"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbProxyPort:Lcom/google/android/play/utils/config/GservicesValue;

    .line 335
    const-string v0, "vending_dcb_poll_timeout_ms"

    const/16 v1, 0x1388

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbPollTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 341
    const-string v0, "vending_dcb_mms_fallback"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbMmsFallback:Lcom/google/android/play/utils/config/GservicesValue;

    .line 351
    const-string v0, "vending_billingcountries_refresh_ms"

    const-wide/32 v2, 0x19bfcc00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingBillingCountriesRefreshMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 359
    const-string v0, "vending_carrier_cred_buf_ms"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierCredentialsBufferMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 372
    const-string v0, "vending_content_rating"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingContentRating:Lcom/google/android/play/utils/config/GservicesValue;

    .line 381
    const-string v0, "vending_hide_content_rating"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingHideContentRating:Lcom/google/android/play/utils/config/GservicesValue;

    .line 388
    const-string v0, "finsky.dcb_debug_options_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcbDebugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 395
    const-string v0, "finsky.dcb3_setup_while_roaming_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3SetupWhileRoamingEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 402
    const-string v0, "finsky.dcb3_verify_association_retries"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3VerifyAssociationRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 408
    const-string v0, "finsky.dcb3_prepare_send_device_rebooted"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3PrepareSendDeviceRebooted:Lcom/google/android/play/utils/config/GservicesValue;

    .line 415
    const-string v0, "finsky.dcb3_passphrase_key_version"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3PassphraseKeyVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 423
    const-string v0, "vending_alarm_max_timeout_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmMaxTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 431
    const-string v0, "vending_alarm_expiration_timeout_ms"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmExpirationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 437
    const-string v0, "vending_alarm_min_timeout_ms"

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmMinTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 443
    const-string v0, "finsky.support_url"

    const-string v1, "https://support.google.com/googleplay/digital-content/#topic=3364261"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->supportUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 451
    const-string v0, "finsky.support_enable_background_data_url"

    const-string v1, "https://support.google.com/googleplay/answer/1663315"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->helpCenterBackgroundDataUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 458
    const-string v0, "finsky.support_edit_payment_method_url"

    const-string v1, "https://wallet.google.com/manage/paymentMethods"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->editPaymentMethodUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 465
    const-string v0, "finsky.instrument_check_lifetime_ms"

    const-wide/32 v2, 0x19bfcc00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->instrumentCheckLifetimeMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 472
    const-string v0, "finsky.select_add_instrument_by_default"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->selectAddinstrumentByDefault:Lcom/google/android/play/utils/config/GservicesValue;

    .line 478
    const-string v0, "finsky.post_purchase_sharing_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->postPurchaseSharingEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 484
    const-string v0, "finsky.pre_purchase_sharing_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->prePurchaseSharingEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 490
    const-string v0, "finsky.launch_urls_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->launchUrlsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 497
    const-string v0, "finsky.external_referrer_lifespan_ms"

    const-wide v2, 0x1cf7c5800L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->externalReferrerLifespanMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 503
    const-string v0, "finsky.subscriptions_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->subscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 511
    const-string v0, "finsky.enable_third_party_inline_app_installs"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableThirdPartyInlineAppInstalls:Lcom/google/android/play/utils/config/GservicesValue;

    .line 517
    const-string v0, "finsky.log_inline_app_install_referrer_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->logInlineAppInstallReferrerEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 525
    const-string v0, "finsky.enable_third_party_direct_purchases"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableThirdPartyDirectPurchases:Lcom/google/android/play/utils/config/GservicesValue;

    .line 533
    const-string v0, "finsky.enable_third_party_details_api"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableThirdPartyDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;

    .line 537
    const-string v0, "finsky.enable_details_api"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;

    .line 541
    const-string v0, "finsky.daily_hygiene_regular_interval_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRegularIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 545
    const-string v0, "finsky.daily_hygiene_retry_interval_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRetryIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 549
    const-string v0, "finsky.daily_hygiene_boot_delay_ms"

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneBootDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 553
    const-string v0, "finsky.daily_hygiene_immediate_delay_ms"

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 567
    const-string v0, "finsky.daily_hygiene_reschedule_jitter_factor"

    const v1, 0x3dcccccd    # 0.1f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;

    .line 576
    const-string v0, "finsky.daily_hygiene_initial_reschedule_jitter_factor"

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneInitialJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;

    .line 583
    const-string v0, "finsky.daily_hygiene_provision_holdoff_max_ms"

    const-wide/32 v2, 0x4ef6d80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneProvisionHoldoffMaxMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 587
    const-string v0, "finsky.daily_hygiene_holdoff_interval_ms"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneHoldoffIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 598
    const-string v0, "finsky.daily_hygiene_holdoff_during_self_update"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneHoldoffDuringSelfUpdate:Lcom/google/android/play/utils/config/GservicesValue;

    .line 609
    const-string v0, "finsky.daily_hygiene_emergency_version_code_low"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateRunVersionCodeLow:Lcom/google/android/play/utils/config/GservicesValue;

    .line 620
    const-string v0, "finsky.daily_hygiene_emergency_version_code_high"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateRunVersionCodeHigh:Lcom/google/android/play/utils/config/GservicesValue;

    .line 629
    const-string v0, "finsky.auto_update_delivery_holdoff_ms"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDeliveryHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 633
    const-string v0, "finsky.continue_url_expiration_ms"

    const-wide/32 v2, 0x124f80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->continueUrlExpirationMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 643
    const-string v0, "finsky.auto_update_exclude_foreground_services"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateExcludeForegroundServices:Lcom/google/android/play/utils/config/GservicesValue;

    .line 657
    const-string v0, "finsky.auto_update_exclude_running_packages_pre"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateExcludeRunningPackagesPre:Lcom/google/android/play/utils/config/GservicesValue;

    .line 667
    const-string v0, "finsky.play_services_auto_update_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gmsCoreAutoUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 675
    const-string v0, "finsky.consistency_token_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consistencyTokenEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 679
    const-string v0, "finsky.edu_device_policy_package_name"

    const-string v1, "com.google.android.apps.enterprise.dmagent"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->eduDevicePolicyApp:Lcom/google/android/play/utils/config/GservicesValue;

    .line 687
    const-string v0, "finsky.nlp_shared_user_id"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpSharedUserId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 691
    const-string v0, "finsky.nlp_package_name"

    const-string v1, "com.google.android.location"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    .line 695
    const-string v0, "finsky.nlp_reinstall_sdk_min"

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpReinstallSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    .line 700
    const-string v0, "finsky.nlp_reinstall_sdk_max"

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpReinstallSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    .line 710
    const-string v0, "finsky.nlp_cleanup_configuration_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupConfigurationId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 714
    const-string v0, "finsky.nlp_cleanup_flags_mask"

    const/16 v1, 0x81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupFlagsMask:Lcom/google/android/play/utils/config/GservicesValue;

    .line 719
    const-string v0, "finsky.nlp_cleanup_flags_set"

    const/16 v1, 0x81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupFlagsSet:Lcom/google/android/play/utils/config/GservicesValue;

    .line 724
    const-string v0, "finsky.nlp_cleanup_sdk_min"

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    .line 729
    const-string v0, "finsky.nlp_cleanup_sdk_max"

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    .line 734
    const-string v0, "finsky.nlp_cleanup_nlp_min"

    const/16 v1, 0x456

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupNlpVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    .line 738
    const-string v0, "finsky.nlp_cleanup_nlp_max"

    const/16 v1, 0x456

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupNlpVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    .line 742
    const-string v0, "finsky.nlp_cleanup_expected_signature"

    const-string v1, "OJGKRT0HGZNU-LGa8F7GViztV4g"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupExpectedSignature:Lcom/google/android/play/utils/config/GservicesValue;

    .line 747
    const-string v0, "finsky.nlp_cleanup_expected_signature_test_keys"

    const-string v1, "WOHEEz90Qew9LCcCcKFIAtpHug4"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupExpectedSignatureTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    .line 755
    const-string v0, "finsky.nlp_cleanup_url"

    const-string v1, "https://ssl.gstatic.com/android/market_images/apks/nlpfixer/nlpfixer-r01.apk"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 760
    const-string v0, "finsky.nlp_cleanup_cookie_name"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupCookieName:Lcom/google/android/play/utils/config/GservicesValue;

    .line 764
    const-string v0, "finsky.nlp_cleanup_cookie_value"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupCookieValue:Lcom/google/android/play/utils/config/GservicesValue;

    .line 771
    const-string v0, "finsky.nlp_cleanup_url_test_keys"

    const-string v1, "https://ssl.gstatic.com/android/market_images/apks/nlpfixer/nlpfixer-r01-test-keys.apk"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupUrlTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    .line 776
    const-string v0, "finsky.nlp_cleanup_cookie_name_test_keys"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupCookieNameTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    .line 780
    const-string v0, "finsky.nlp_cleanup_cookie_value_test_keys"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupCookieValueTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    .line 784
    const-string v0, "finsky.nlp_cleanup_downgrade_flag"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupDowngradeFlag:Lcom/google/android/play/utils/config/GservicesValue;

    .line 788
    const-string v0, "finsky.nlp_cleanup_log_common_statuses"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupLogCommonStatuses:Lcom/google/android/play/utils/config/GservicesValue;

    .line 792
    const-string v0, "finsky.download_bytes_mobile_recommended"

    const-wide/32 v2, 0x3200000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileRecommended:Lcom/google/android/play/utils/config/GservicesValue;

    .line 796
    const-string v0, "finsky.download_bytes_mobile_maximum"

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileMaximum:Lcom/google/android/play/utils/config/GservicesValue;

    .line 808
    const-string v0, "finsky.download_free_space_threshold_bytes"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;

    .line 816
    const-string v0, "finsky.download_free_space_apk_size_factor"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadFreeSpaceApkSizeFactor:Lcom/google/android/play/utils/config/GservicesValue;

    .line 820
    const-string v0, "finsky.download_send_base_version"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadSendBaseVersionCode:Lcom/google/android/play/utils/config/GservicesValue;

    .line 824
    const-string v0, "finsky.download_deliverydata_expiration"

    const-wide/32 v2, 0xdbba00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->deliveryDataExpirationMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 832
    const-string v0, "finsky.download_patch_free_space_factor"

    const/16 v1, 0xd2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadPatchFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;

    .line 840
    const-string v0, "finsky.download_external_file_size_min_bytes"

    const-wide/32 v2, 0x3200000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadExternalFileSizeMinBytes:Lcom/google/android/play/utils/config/GservicesValue;

    .line 848
    const-string v0, "finsky.download_external_free_space_factor"

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadExternalFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;

    .line 856
    const-string v0, "finsky.download_external_free_space_threshold_bytes"

    const-wide/32 v2, 0x4000000

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadExternalFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;

    .line 865
    const-string v0, "finsky.preserve_forward_lock_api_min"

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->preserveForwardLockApiMin:Lcom/google/android/play/utils/config/GservicesValue;

    .line 874
    const-string v0, "finsky.preserve_forward_lock_api_max"

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->preserveForwardLockApiMax:Lcom/google/android/play/utils/config/GservicesValue;

    .line 881
    const-string v0, "finsky.app_restore_holdoff_ms"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 888
    const-string v0, "finsky.app_restore_failsafe_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreFailsafeMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 895
    const-string v0, "finsky.app_restore_fetch_list_max_attempts"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreFetchListMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    .line 902
    const-string v0, "finsky.app_restore_retry_fetch_list_holdoff_ms"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreRetryFetchListHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 910
    const-string v0, "finsky.app_restore_download_max_attempts"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreDownloadMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    .line 917
    const-string v0, "finsky.app_restore_retry_download_holdoff_ms"

    const-wide/32 v2, 0x3a980

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreRetryDownloadHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 925
    const-string v0, "finsky.app_restore_retry_download_holdoff_high_priority_ms"

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreRetryDownloadHoldoffHighPriorityMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 938
    const-string v0, "finsky.app_restore_retry_download_blacklist"

    const-string v1, "198,498"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreHttpStatusBlacklist:Lcom/google/android/play/utils/config/GservicesValue;

    .line 945
    const-string v0, "finsky.app_restore_app_icon_max_attempts"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreAppIconMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    .line 949
    const-string v0, "finsky.my_library_widget_interactions_cluster_ms"

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetInteractionClusterMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 954
    const-string v0, "finsky.my_library_widget_shortterm_duration_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermDurationMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 958
    const-string v0, "finsky.my_library_widget_midterm_duration_ms"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetMidtermDurationMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 962
    const-string v0, "finsky.my_library_widget_important_contribution_fraction"

    const v1, 0x3e4ccccd    # 0.2f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetImportantContributionFraction:Lcom/google/android/play/utils/config/GservicesValue;

    .line 967
    const-string v0, "finsky.my_library_widget_shortterm_decay"

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermDecay:Lcom/google/android/play/utils/config/GservicesValue;

    .line 975
    const-string v0, "finsky.my_library_has_content_scores"

    const-string v1, "0:15"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetHasContentScoreMap:Lcom/google/android/play/utils/config/GservicesValue;

    .line 983
    const-string v0, "finsky.my_library_widget_shortterm_scores"

    const-string v1, "0:5"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermScores:Lcom/google/android/play/utils/config/GservicesValue;

    .line 991
    const-string v0, "finsky.my_library_widget_midterm_scores"

    const-string v1, "0:1"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgeMidtermScores:Lcom/google/android/play/utils/config/GservicesValue;

    .line 998
    const-string v0, "finsky.my_library_widget_always_include_most_recent"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetAlwaysIncludeMostRecentBackend:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1001
    const-string v0, "finsky.recs_widget_flip_interval_ms"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationsWidgetFlipIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1007
    const-string v0, "finsky.consumption_app_timeout_ms"

    const-wide/16 v2, 0x1b58

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1013
    const-string v0, "finsky.consumption_app_image_timeout_ms"

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppImageTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1019
    const-string v0, "finsky.recommendations_fetch_timeout_ms"

    const-wide/16 v2, 0x3a98

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationsFetchTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1023
    const-string v0, "finsky.minimum_videos_app_version_code_pre_honeycomb"

    const/16 v1, 0x7d9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodePreHoneycomb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1028
    const-string v0, "finsky.minimum_videos_app_version_code_post_honeycomb"

    const/16 v1, 0x4e7a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodeHoneycombPlus:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1036
    const-string v0, "finsky.minimum_music_app_version_code"

    const/16 v1, 0x581

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumMusicVersionCode:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1040
    const-string v0, "finsky.minimum_number_of_recommendations"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumNumberOfRecs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1044
    const-string v0, "finsky.recommendation_ttl_ms"

    const-wide/32 v2, 0xa4cb800

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationTtlMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1048
    const-string v0, "finsky.gaia_auth_validity_ms"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gaiaAuthValidityMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1052
    const-string v0, "finsky.platform_anti_malware_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1056
    const-string v0, "finsky.platform_anti_malware_dialog_delay_ms"

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareDialogDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1060
    const-string v0, "finsky.anti_malware_activity_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->antiMalwareActivityEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1064
    const-string v0, "finsky.verify_installed_packages_minimum_wait_time_ms"

    const-wide/32 v2, 0x1ee62800

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesMinimumWaitMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1070
    const-string v0, "finsky.verify_installed_packages_timeout_ms"

    const/16 v1, 0x3a98

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1076
    const-string v0, "finsky.verify_installed_packages_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1082
    const-string v0, "finsky.verify_installed_play_packages_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->verifyInstalledPlayPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1089
    const-string v0, "finsky.enable_review_comments"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableReviewComments:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1095
    const-string v0, "finsky.enable_remove_apps_from_library"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableRemoveAppsFromLibrary:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1099
    const-string v0, "finsky.iabv3_subscriptions_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3SubscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1107
    const-string v0, "finsky.iabv3_max_purchases_in_response"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3MaxPurchasesInResponse:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1115
    const-string v0, "finsky.iabv3_network_timeout_ms"

    const-wide/16 v2, 0x4e20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3NetworkTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1121
    const-string v0, "finsky.iabv3_sku_details_max_size"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3SkuDetailsMaxSize:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1127
    const-string v0, "finsky.iabv3_first_party_api_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3FirstPartyApiEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1138
    const-string v0, "finsky.iabv3_first_party_api_unrestricted_access"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3FirstPartyApiUnrestrictedAccess:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1145
    const-string v0, "finsky.places_api_bias_radius_meters"

    const v1, 0xc350

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->placesApiBiasRadiusMeters:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1158
    const-string v0, "finsky.whitelisted_places_api_countries"

    const-string v1, "0:CA,CH,US"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->whitelistedPlacesApiCountries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1162
    const-string v0, "finsky.places_api_key"

    const-string v1, "AIzaSyCsc46SHdb57jPJa9fo7euV_HBDgwdrXsE"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->placesApiKey:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1166
    const-string v0, "finsky.enable_people_suggest_widget"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enablePeopleSuggestWidget:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1174
    const-string v0, "finsky.light_purchase_auto_dismiss_ms"

    const-wide/16 v2, 0x9c4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseAutoDismissMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1183
    const-string v0, "finsky.light_purchase_optimistic_provisioning"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseOptimisticProvisioning:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1189
    const-string v0, "finsky.default_purchase_authentication"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->defaultPurchaseAuthentication:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1200
    const-string v0, "finsky.password_recovery_url"

    const-string v1, "https://accounts.google.com/RecoverAccount?fpOnly=1&Email=%email%"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->passwordRecoveryUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1208
    const-string v0, "finsky.reset_pin_url"

    const-string v1, "https://accounts.google.com/ResetPin?Email=%email%"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->resetPinUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1214
    const-string v0, "finsky.password_max_failed_attempts"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->passwordMaxFailedAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1221
    const-string v0, "finsky.max_times_auto_update_cleanup_shown"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->maxAutoUpdateDialogShownCount:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1228
    const-string v0, "finsky.auto_update_cleanup_first_backoff_days"

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogFirstBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1237
    const-string v0, "finsky.auto_update_cleanup_second_backoff_days"

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogSecondBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1245
    const-string v0, "finsky.auto_update_cleanup_subsequent_backoff_days"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogSubsequentBackoffDays:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1248
    const-string v0, "finsky.skip_auto_update_cleanup_client_version_check"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->skipAutoUpdateCleanupClientVersionCheck:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1255
    const-string v0, "finsky.play_games_install_suggestion_max_times_shown"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionMaxShownCount:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1261
    const-string v0, "finsky.play_games_gms_core_min_version"

    const v1, 0x3010b0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesGmsCoreMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1268
    const-string v0, "finsky.play_games_install_suggestion_only_when_game_features"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionOnlyWhenGameFeatures:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1276
    const-string v0, "finsky.play_games_install_suggestion_first_backoff_ms"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionFirstBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1284
    const-string v0, "finsky.play_games_install_suggestion_subsequent_backoff_ms"

    const-wide v2, 0x9a7ec800L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionSubsequentBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1291
    const-string v0, "finsky.play_games_doc_id"

    const-string v1, "com.google.android.play.games"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playGamesDocId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1297
    const-string v0, "finsky.enable_session_stats_log"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableSessionStatsLog:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1303
    const-string v0, "finsky.dcb2_setup_delay_tos_loading"

    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb2SetupDelayTosLoading:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1313
    const-string v0, "finsky.music_app_subscription_backend_docid_blacklist"

    const-string v1, "Saeaaaaaa"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicAppSubscriptionBackendDocidBlacklist:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1320
    const-string v0, "finsky.all_access_backend_docid"

    const-string v1, "Saeaaaaaa"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicAllAccessSubscriptionBackendDocid:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1326
    const-string v0, "finsky.music_app_subscription_resignup_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicAppSubscriptionResignupEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1332
    const-string v1, "finsky.additional_experiments"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->additionalExperiments:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1338
    const-string v0, "finsky.recently_updated_window_size_ms"

    const-wide/32 v2, 0xf731400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recentlyUpdatedWindowSizeMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1345
    const-string v0, "finsky.outstanding_notification_time_delay_ms"

    const-wide/32 v2, 0xf731400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->outstandingNotificationTimeDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1353
    const-string v0, "finsky.corpusAwarenessThreshold.books"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdBooks:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1361
    const-string v0, "finsky.corpusAwarenessThreshold.magazines"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMagazines:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1369
    const-string v0, "finsky.corpusAwarenessThreshold.movies"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMovies:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1377
    const-string v0, "finsky.corpusAwarenessThreshold.music"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMusic:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1383
    const-string v0, "finsky.auto_update_job_scheduler_gearhead_delay"

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerGearheadDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1390
    const-string v0, "finsky.auto_update_job_scheduler_gearhead_delay"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerGearheadTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1399
    const-string v0, "finsky.auto_update_wifi_check_interval"

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateWifiCheckIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1405
    const-string v0, "finsky.auto_update_job_scheduler_timeout_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1412
    const-string v0, "finsky.video_app_trailer_playback_min_version"

    const/16 v1, 0x6996

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->videoAppTrailerPlaybackMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1418
    const-string v0, "finsky.newsstand_app_first_version"

    const v1, 0x7e35bdf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->firstNewsstandAppVersion:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1424
    const-string v0, "finsky.gel_package_name"

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gelPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1432
    const-string v0, "finsky.library_replicator_reschedule_delay_ms"

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->libraryReplicatorRescheduleDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1439
    const-string v0, "finsky.positive_rate_threshold"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->positiveRateThreshold:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1445
    const-string v0, "finsky.peoplePageWarmWelcomeGraphicUrl"

    const-string v1, "https://lh6.ggpht.com/MBGKEdyM2S9-d0wZxNGZkdrTDHeT6iU3fANSMYh6R8cw6GNlRoEi_pMzxoZhJTLl4A"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->peoplePageWarmWelcomeGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1452
    const-string v0, "finsky.peoplePageWarmWelcomeButtonIconUrl"

    const-string v1, "https://lh3.ggpht.com/UFLxPwW24-zpdw50aPXy07XUncvFN7BT4LW0yIKB6uNWWljHiiWalKAddBUHl15cgmg"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->peoplePageWarmWelcomeButtonIconUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1460
    const-string v0, "finsky.setupWizardRestoreHeaderGraphicUrl"

    const-string v1, "https://lh3.ggpht.com/mVyozFkAy-ppNcs7MK9cGlbvH6Gzahl0x_XiOLIk-rUMd9TPtZUCVtivtA53EqcMcQ"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1468
    const-string v0, "finsky.setupWizardRestoreWideHeaderGraphicUrl"

    const-string v1, "https://lh6.ggpht.com/MJOZe02zps-3ks_-mD8e0H9XInCVD5Un-4aYvP1gOSUhouHrC4wvqwopxg0NsxUUpoY"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreWideHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1476
    const-string v0, "finsky.setupWizardRestoreFinalHoldHeaderUrl"

    const-string v1, "https://lh6.ggpht.com/-jHSfNHsSAhHV2mHP1moDA8BZHNEQFTTeLulxy327nlGm5eYE_c5hL9iwBgmMgS1b1k"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1484
    const-string v0, "finsky.setupWizardRestoreFinalHoldHeaderLandscapeUrl"

    const-string v1, "https://lh6.ggpht.com/TWBB8vzWLPJm4iYShK_-7jcoPoE5ea9E8kICksvYhim1XEB1Njc8m5JK1uc8Bephpg"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderLandscapeUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1492
    const-string v0, "finsky.setupWizardRestoreFinalHoldHeaderTabletUrl"

    const-string v1, "https://lh5.ggpht.com/oc1mRJCrLUcAdXhuVYW2ke0v78bTYzp5W6eSFoU8zCaldflOgmRpaMTBEqTDIJwazHc"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderTabletUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1500
    const-string v0, "finsky.setup_wizard_restore_final_hold_short_ms"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldShortMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1508
    const-string v0, "finsky.setup_wizard_restore_final_hold_long_ms"

    const-wide/32 v2, 0x2bf20

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldLongMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1516
    const-string v0, "finsky.setupWizardPaymentHeaderGraphicUrl"

    const-string v1, "https://lh3.ggpht.com/Abae-7-cb7aY274FnRM2l-PL9VVBIm_Gl0mESZf6JJQNjhe50ICjGUorkNLRiE4Q5Q"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardPaymentHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1524
    const-string v0, "finsky.setupWizardPaymentHeaderWideGraphicUrl"

    const-string v1, "https://lh4.ggpht.com/7Qt-tnJI4-P5n7PCvoyzL7oBu5NsbpbOVbO4H-evP3k_Pzom57o47T23QKhiLsRB1A"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardPaymentHeaderWideGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1532
    const-string v0, "finsky.setupWizardForceResizeForKeyboardInFullscreen"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardForceResizeForKeyboardInFullscreen:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1538
    const-string v0, "finsky.client_mutation_cache_circles_model_cache_size"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->clientMutationCacheCirclesModelCacheSize:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1545
    const-string v0, "finsky.myLibraryWidget.backgroundImageLocation"

    const-string v1, "/system/media/play_widget_background.png"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetBackgroundImageLocation:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1549
    const-string v0, "finsky.music_all_access_sign_up_intent_url"

    const-string v1, "http://play.google.com/music/listen?signup=1&utm_source=playstore&utm_medium=album_detail_page&utm_campaign=30daytrial"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicAllAccessSignUpIntentUrl:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1560
    const-string v0, "finsky.setup_wizard_early_update_enable"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateEnable:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1568
    const-string v0, "finsky.setup_wizard_early_update_deadlock_max_ms"

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->setupWizardEarlyUpdateDeadlockMaxMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 1575
    const-string v0, "finsky.supported_account_types"

    const-string v1, "com.google.work"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->supportedAccountTypes:Lcom/google/android/play/utils/config/GservicesValue;

    return-void
.end method
