.class public final Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedeemGiftCardResponse"
.end annotation


# instance fields
.field public addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field public balanceHtml:Ljava/lang/String;

.field public checkoutTokenRequired:Z

.field public hasBalanceHtml:Z

.field public hasCheckoutTokenRequired:Z

.field public hasResult:Z

.field public hasUserMessageHtml:Z

.field public result:I

.field public userMessageHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1274
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    .line 1275
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1278
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    .line 1279
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasResult:Z

    .line 1280
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    .line 1281
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasUserMessageHtml:Z

    .line 1282
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    .line 1283
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasBalanceHtml:Z

    .line 1284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 1285
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    .line 1286
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasCheckoutTokenRequired:Z

    .line 1287
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->cachedSize:I

    .line 1288
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1314
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1315
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasResult:Z

    if-eqz v1, :cond_1

    .line 1316
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1319
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasUserMessageHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1320
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1323
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasBalanceHtml:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1324
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1327
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v1, :cond_6

    .line 1328
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1331
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasCheckoutTokenRequired:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    if-eqz v1, :cond_8

    .line 1332
    :cond_7
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1335
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1344
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1348
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1349
    :sswitch_0
    return-object p0

    .line 1354
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1355
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1369
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    .line 1370
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasResult:Z

    goto :goto_0

    .line 1376
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    .line 1377
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasUserMessageHtml:Z

    goto :goto_0

    .line 1381
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    .line 1382
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasBalanceHtml:Z

    goto :goto_0

    .line 1386
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-nez v2, :cond_1

    .line 1387
    new-instance v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 1389
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1393
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    .line 1394
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasCheckoutTokenRequired:Z

    goto :goto_0

    .line 1344
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 1355
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1222
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1294
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasResult:Z

    if-eqz v0, :cond_1

    .line 1295
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->result:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1297
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasUserMessageHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1298
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1300
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasBalanceHtml:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1301
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->balanceHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1303
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v0, :cond_6

    .line 1304
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1306
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->hasCheckoutTokenRequired:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    if-eqz v0, :cond_8

    .line 1307
    :cond_7
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;->checkoutTokenRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1309
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1310
    return-void
.end method
