.class public final Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RestoreApplicationsRequestProto"
.end annotation


# instance fields
.field public backupAndroidId:Ljava/lang/String;

.field public deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

.field public hasBackupAndroidId:Z

.field public hasTosVersion:Z

.field public tosVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2885
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2886
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    .line 2887
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2890
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    .line 2891
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasBackupAndroidId:Z

    .line 2892
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    .line 2893
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasTosVersion:Z

    .line 2894
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 2895
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->cachedSize:I

    .line 2896
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2916
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2917
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasBackupAndroidId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2918
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2921
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasTosVersion:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2922
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2925
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-eqz v1, :cond_4

    .line 2926
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2929
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2937
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2938
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2942
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2943
    :sswitch_0
    return-object p0

    .line 2948
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    .line 2949
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasBackupAndroidId:Z

    goto :goto_0

    .line 2953
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    .line 2954
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasTosVersion:Z

    goto :goto_0

    .line 2958
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-nez v1, :cond_1

    .line 2959
    new-instance v1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DeviceConfigurationProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 2961
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2938
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2857
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2902
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasBackupAndroidId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2903
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->backupAndroidId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2905
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->hasTosVersion:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2906
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->tosVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2908
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-eqz v0, :cond_4

    .line 2909
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2911
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2912
    return-void
.end method
