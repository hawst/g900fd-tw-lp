.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;
.super Ljava/lang/Object;
.source "CheckoutPurchaseSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrepareErrorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V
    .locals 0

    .prologue
    .line 623
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;

    .prologue
    .line 623
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 9
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v8, 0x3

    .line 627
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mVolleyError:Lcom/android/volley/VolleyError;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$3102(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    .line 628
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/16 v2, 0x12f

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getPrepareServerLatencyMs()J
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getTimeElapsedSincePrepareMs()J
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1100(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v6

    move-object v3, p1

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->logVolleyError(ILcom/android/volley/VolleyError;JJ)V
    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$3200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;ILcom/android/volley/VolleyError;JJ)V

    .line 630
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v8, v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$3300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    .line 631
    return-void
.end method
