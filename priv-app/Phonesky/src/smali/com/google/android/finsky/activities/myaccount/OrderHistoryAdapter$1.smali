.class Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;
.super Ljava/lang/Object;
.source "OrderHistoryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindOrderHistoryRowView(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

.field final synthetic val$position:I

.field final synthetic val$rowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;Lcom/google/android/finsky/layout/OrderHistoryRowView;I)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$rowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    iput p3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 267
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0xa2c

    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$rowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    invoke-virtual {v1, v2, v5, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 271
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$rowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->toggleExpansion()Z

    move-result v0

    .line 272
    .local v0, "expanded":Z
    if-eqz v0, :cond_1

    .line 275
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)Lcom/google/android/finsky/layout/OrderHistoryRowView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)Lcom/google/android/finsky/layout/OrderHistoryRowView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->toggleExpansion()Z

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$position:I

    # setter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I
    invoke-static {v1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$102(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;I)I

    .line 279
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->val$rowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # setter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;
    invoke-static {v1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$202(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;Lcom/google/android/finsky/layout/OrderHistoryRowView;)Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .line 284
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # setter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I
    invoke-static {v1, v4}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$102(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;I)I

    .line 282
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    # setter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;
    invoke-static {v1, v5}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->access$202(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;Lcom/google/android/finsky/layout/OrderHistoryRowView;)Lcom/google/android/finsky/layout/OrderHistoryRowView;

    goto :goto_0
.end method
