.class public final Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BulkDetailsEntry"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;


# instance fields
.field public doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 618
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->clear()Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .line 619
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    .locals 2

    .prologue
    .line 603
    sget-object v0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->_emptyArray:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    if-nez v0, :cond_1

    .line 604
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 606
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->_emptyArray:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    if-nez v0, :cond_0

    .line 607
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    sput-object v0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->_emptyArray:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .line 609
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->_emptyArray:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    return-object v0

    .line 609
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 623
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->cachedSize:I

    .line 624
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 638
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 639
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_0

    .line 640
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 643
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 651
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 652
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 656
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 657
    :sswitch_0
    return-object p0

    .line 662
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_1

    .line 663
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 665
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 652
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 597
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    .line 631
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 633
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 634
    return-void
.end method
