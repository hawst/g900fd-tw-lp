.class public Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentUtils;
.super Ljava/lang/Object;
.source "GenericInstrumentUtils.java"


# direct methods
.method public static createTextFormInput(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;
    .locals 8
    .param p0, "editText"    # Landroid/widget/EditText;
    .param p1, "field"    # Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 72
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;-><init>()V

    .line 73
    .local v1, "input":Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 74
    .local v3, "text":Ljava/lang/CharSequence;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 75
    iget-object v2, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    .line 76
    .local v2, "responseFormat":Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
    iget v4, v2, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    packed-switch v4, :pswitch_data_0

    .line 88
    const-string v4, "Unknown ResponseFormat type: %d"

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v2, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    .end local v2    # "responseFormat":Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
    :cond_0
    :goto_0
    return-object v1

    .line 78
    .restart local v2    # "responseFormat":Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
    :pswitch_0
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    .line 79
    iput-boolean v5, v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->hasPlaintextResponse:Z

    goto :goto_0

    .line 82
    :pswitch_1
    new-instance v0, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;-><init>()V

    .line 83
    .local v0, "encryptor":Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;
    iget-object v4, v2, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->createPaypalAuthResponse([BLjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static updateEditText(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)V
    .locals 3
    .param p0, "editText"    # Landroid/widget/EditText;
    .param p1, "field"    # Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .prologue
    const/4 v2, 0x0

    .line 32
    iget v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    packed-switch v1, :pswitch_data_0

    .line 46
    :pswitch_0
    const/4 v0, 0x1

    .line 48
    .local v0, "inputType":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 50
    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 52
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    :goto_1
    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    :goto_2
    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_3
    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 59
    return-void

    .line 34
    .end local v0    # "inputType":I
    :pswitch_1
    const/16 v0, 0x21

    .line 35
    .restart local v0    # "inputType":I
    goto :goto_0

    .line 37
    .end local v0    # "inputType":I
    :pswitch_2
    const/4 v0, 0x2

    .line 38
    .restart local v0    # "inputType":I
    goto :goto_0

    .line 40
    .end local v0    # "inputType":I
    :pswitch_3
    const/4 v0, 0x3

    .line 41
    .restart local v0    # "inputType":I
    goto :goto_0

    .line 43
    .end local v0    # "inputType":I
    :pswitch_4
    const/16 v0, 0x81

    .line 44
    .restart local v0    # "inputType":I
    goto :goto_0

    .line 52
    :cond_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    goto :goto_1

    .line 54
    :cond_1
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    goto :goto_2

    .line 56
    :cond_2
    iget-object v2, p1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    goto :goto_3

    .line 32
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
