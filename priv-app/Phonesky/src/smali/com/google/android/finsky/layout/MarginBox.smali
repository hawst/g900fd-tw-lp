.class public Lcom/google/android/finsky/layout/MarginBox;
.super Landroid/widget/FrameLayout;
.source "MarginBox.java"


# instance fields
.field protected mEdgePadding:I

.field protected mMaxContentWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/MarginBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 30
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/MarginBox;->mMaxContentWidth:I

    .line 35
    const v1, 0x7f0b0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0084

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/finsky/layout/MarginBox;->mEdgePadding:I

    .line 37
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/MarginBox;->getWidth()I

    move-result v7

    .line 64
    .local v7, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/MarginBox;->getHeight()I

    move-result v5

    .line 65
    .local v5, "height":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/MarginBox;->getChildCount()I

    move-result v8

    if-ge v6, v8, :cond_0

    .line 66
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/MarginBox;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 68
    .local v4, "childWidth":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 69
    .local v1, "childHeight":I
    sub-int v8, v7, v4

    div-int/lit8 v2, v8, 0x2

    .line 70
    .local v2, "childLeft":I
    sub-int v8, v5, v1

    div-int/lit8 v3, v8, 0x2

    .line 71
    .local v3, "childTop":I
    add-int v8, v2, v4

    add-int v9, v3, v1

    invoke-virtual {v0, v2, v3, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 65
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childHeight":I
    .end local v2    # "childLeft":I
    .end local v3    # "childTop":I
    .end local v4    # "childWidth":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 41
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 42
    .local v6, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 44
    .local v4, "height":I
    iget v7, p0, Lcom/google/android/finsky/layout/MarginBox;->mMaxContentWidth:I

    iget v8, p0, Lcom/google/android/finsky/layout/MarginBox;->mEdgePadding:I

    mul-int/lit8 v8, v8, 0x2

    sub-int v8, v6, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 45
    .local v2, "childWidth":I
    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 46
    .local v3, "childWidthSpec":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/MarginBox;->getChildCount()I

    move-result v7

    if-ge v5, v7, :cond_1

    .line 47
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/MarginBox;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v1, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 49
    .local v1, "childHeightParam":I
    const/4 v7, -0x1

    if-ne v1, v7, :cond_0

    .line 50
    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v3, v7}, Landroid/view/View;->measure(II)V

    .line 46
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 53
    :cond_0
    const/high16 v7, -0x80000000

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v3, v7}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 58
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childHeightParam":I
    :cond_1
    invoke-virtual {p0, v6, v4}, Lcom/google/android/finsky/layout/MarginBox;->setMeasuredDimension(II)V

    .line 59
    return-void
.end method
