.class public Lcom/google/android/finsky/config/PurchaseAuth;
.super Ljava/lang/Object;
.source "PurchaseAuth.java"


# direct methods
.method static cleanupPreferences(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/experiments/Experiments;Lcom/google/android/finsky/analytics/FinskyEventLog;)V
    .locals 4
    .param p3, "experiments"    # Lcom/google/android/finsky/experiments/Experiments;
    .param p4, "eventLog"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/finsky/experiments/Experiments;",
            "Lcom/google/android/finsky/analytics/FinskyEventLog;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p0, "hasSeenPurchaseSessionMessage":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    .local p1, "purchaseAuthType":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    .local p2, "purchaseAuthVersionCode":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    const-string v0, "cl:billing.cleanup_auth_settings"

    invoke-interface {p3, v0}, Lcom/google/android/finsky/experiments/Experiments;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 150
    invoke-virtual {p1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 151
    const/16 v0, 0x190

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "cleanup-auth-settings"

    invoke-virtual {p4, v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSettingsBackgroundEvent(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 157
    :cond_0
    return-void
.end method

.method public static getPurchaseAuthType(Ljava/lang/String;)I
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 61
    .local v0, "purchaseAuth":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->defaultPurchaseAuthentication:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .end local v0    # "purchaseAuth":I
    :cond_0
    return v0
.end method

.method public static migrateAndCleanupPreferences([Landroid/accounts/Account;)V
    .locals 7
    .param p0, "accounts"    # [Landroid/accounts/Account;

    .prologue
    .line 90
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 91
    aget-object v2, p0, v1

    iget-object v0, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 93
    .local v0, "accountName":Ljava/lang/String;
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->isGaiaAuthOptedOut:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/config/PurchaseAuth;->migratePreferences(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)V

    .line 96
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v3

    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthVersionCode:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/finsky/config/PurchaseAuth;->cleanupPreferences(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/experiments/Experiments;Lcom/google/android/finsky/analytics/FinskyEventLog;)V

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method static migratePreferences(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "isGaiaAuthOptedOut":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    .local p1, "purchaseAuthType":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 114
    .local v0, "isOptedOut":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    .line 121
    .local v1, "newValue":I
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 125
    .end local v0    # "isOptedOut":Ljava/lang/Boolean;
    .end local v1    # "newValue":I
    :cond_0
    return-void

    .line 119
    .restart local v0    # "isOptedOut":Ljava/lang/Boolean;
    :cond_1
    sget-object v2, Lcom/google/android/finsky/config/G;->defaultPurchaseAuthentication:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static setAndLogPurchaseAuth(Ljava/lang/String;ILjava/lang/Integer;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)V
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "toSetting"    # I
    .param p2, "fromSetting"    # Ljava/lang/Integer;
    .param p3, "eventLog"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .param p4, "reason"    # Ljava/lang/String;

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthVersionCode:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/config/PurchaseAuth;->setPurchaseAuthType(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;I)V

    .line 72
    const/16 v0, 0x190

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v0, v1, p2, p4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSettingsBackgroundEvent(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method static setPurchaseAuthType(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;I)V
    .locals 1
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "purchaseAuthType":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    .local p1, "purchaseAuthVersionCode":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 81
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getVersionCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 82
    return-void
.end method
