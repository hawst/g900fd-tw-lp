.class public Lcom/google/android/finsky/layout/HistogramBar;
.super Landroid/view/View;
.source "HistogramBar.java"


# instance fields
.field private mHeight:I

.field private mMaxWidth:D

.field private mWidthPercentage:D


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/HistogramBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramBar;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 45
    iget-wide v2, p0, Lcom/google/android/finsky/layout/HistogramBar;->mWidthPercentage:D

    iget-wide v4, p0, Lcom/google/android/finsky/layout/HistogramBar;->mMaxWidth:D

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 46
    .local v0, "fill":I
    if-gez v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/layout/HistogramBar;->mHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/HistogramBar;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setBarHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/google/android/finsky/layout/HistogramBar;->mHeight:I

    .line 35
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "colorResourceId"    # I

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 39
    return-void
.end method

.method public setMaxBarWidth(D)V
    .locals 1
    .param p1, "maxWidth"    # D

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/google/android/finsky/layout/HistogramBar;->mMaxWidth:D

    .line 27
    return-void
.end method

.method public setWidthPercentage(D)V
    .locals 1
    .param p1, "widthPercentage"    # D

    .prologue
    .line 30
    iput-wide p1, p0, Lcom/google/android/finsky/layout/HistogramBar;->mWidthPercentage:D

    .line 31
    return-void
.end method
