.class public Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayEditorialNonAppCardView.java"

# interfaces
.implements Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;


# static fields
.field private static final IMAGE_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->IMAGE_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x0
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getCardType()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x6

    return v0
.end method

.method public getImageTypePreference()[I
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->IMAGE_TYPES:[I

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 49
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_0

    const/4 v1, 0x6

    .line 50
    .local v1, "docType":I
    :goto_0
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->mThumbnailAspectRatio:F

    .line 52
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 54
    .local v3, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v3, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 55
    .local v2, "thumbnailHeight":I
    int-to-float v5, v2

    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->mThumbnailAspectRatio:F

    div-float/2addr v5, v6

    float-to-int v4, v5

    .line 56
    .local v4, "thumbnailWidth":I
    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->onMeasure(II)V

    .line 59
    return-void

    .line 49
    .end local v1    # "docType":I
    .end local v2    # "thumbnailHeight":I
    .end local v3    # "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "thumbnailWidth":I
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    goto :goto_0
.end method
