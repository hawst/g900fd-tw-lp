.class public Lcom/google/android/finsky/utils/GetTocHelper;
.super Ljava/lang/Object;
.source "GetTocHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/GetTocHelper$Listener;
    }
.end annotation


# direct methods
.method static synthetic access$000(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    .prologue
    .line 29
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper;->doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    return-void
.end method

.method static synthetic access$100(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    .prologue
    .line 29
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    return-void
.end method

.method public static changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 0
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 213
    invoke-interface {p0}, Lcom/google/android/finsky/api/DfeApi;->invalidateTocCache()V

    .line 214
    return-void
.end method

.method private static doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 3
    .param p0, "allowRetry"    # Z
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "allowDouble"    # Z
    .param p3, "listener"    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    .prologue
    .line 167
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "deviceConfigToken":Ljava/lang/String;
    new-instance v1, Lcom/google/android/finsky/utils/GetTocHelper$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/finsky/utils/GetTocHelper$3;-><init>(ZLcom/google/android/finsky/utils/GetTocHelper$Listener;Lcom/google/android/finsky/api/DfeApi;Z)V

    new-instance v2, Lcom/google/android/finsky/utils/GetTocHelper$4;

    invoke-direct {v2, p3}, Lcom/google/android/finsky/utils/GetTocHelper$4;-><init>(Lcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    invoke-interface {p1, p2, v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getToc(ZLjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 206
    return-void
.end method

.method private static doRequestToken(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 1
    .param p0, "allowRetry"    # Z
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "allowDouble"    # Z
    .param p3, "listener"    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/finsky/utils/GetTocHelper$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/utils/GetTocHelper$2;-><init>(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->requestToken(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 152
    return-void
.end method

.method public static getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V
    .locals 2
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "allowDouble"    # Z
    .param p2, "listener"    # Lcom/google/android/finsky/utils/GetTocHelper$Listener;

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "deviceConfigToken":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->uploadIfNotRegistered(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;)V

    .line 85
    const/4 v1, 0x1

    invoke-static {v1, p0, p1, p2}, Lcom/google/android/finsky/utils/GetTocHelper;->doGetToc(ZLcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    .line 86
    return-void
.end method

.method public static getTocBlocking(Lcom/google/android/finsky/api/DfeApi;)Lcom/google/android/finsky/protos/Toc$TocResponse;
    .locals 1
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 93
    const/16 v0, 0x3e7

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/GetTocHelper;->getTocBlocking(Lcom/google/android/finsky/api/DfeApi;I)Lcom/google/android/finsky/protos/Toc$TocResponse;

    move-result-object v0

    return-object v0
.end method

.method public static getTocBlocking(Lcom/google/android/finsky/api/DfeApi;I)Lcom/google/android/finsky/protos/Toc$TocResponse;
    .locals 8
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "secondsToWait"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 102
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 104
    new-instance v2, Ljava/util/concurrent/Semaphore;

    invoke-direct {v2, v7}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 105
    .local v2, "semaphore":Ljava/util/concurrent/Semaphore;
    const/4 v4, 0x1

    new-array v1, v4, [Lcom/google/android/finsky/protos/Toc$TocResponse;

    .line 107
    .local v1, "result":[Lcom/google/android/finsky/protos/Toc$TocResponse;
    new-instance v4, Lcom/google/android/finsky/utils/GetTocHelper$1;

    invoke-direct {v4, v1, v2}, Lcom/google/android/finsky/utils/GetTocHelper$1;-><init>([Lcom/google/android/finsky/protos/Toc$TocResponse;Ljava/util/concurrent/Semaphore;)V

    invoke-static {p0, v7, v4}, Lcom/google/android/finsky/utils/GetTocHelper;->getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    .line 123
    int-to-long v4, p1

    :try_start_0
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 129
    :goto_0
    return-object v3

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    aget-object v3, v1, v7

    goto :goto_0
.end method
