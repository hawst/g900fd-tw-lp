.class public abstract Lcom/google/android/finsky/activities/DetailsDataBasedFragment;
.super Lcom/google/android/finsky/fragments/UrlBasedPageFragment;
.source "DetailsDataBasedFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/TextSectionStateListener;


# instance fields
.field protected final mCardItemsPerRow:I

.field protected mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

.field private mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field protected mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

.field protected mExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mNfcHandler:Lcom/google/android/finsky/utils/Nfc$NfcHandler;

.field private mPageCreationTime:J

.field protected mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field protected mSavedInstanceState:Landroid/os/Bundle;

.field protected mScrollListener:Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

.field private mSentImpression:Z

.field protected mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

.field protected final mUseWideLayout:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;-><init>()V

    .line 58
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getPlayStoreUiElementType()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 73
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .line 75
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSentImpression:Z

    .line 89
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 90
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0f0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mUseWideLayout:Z

    .line 91
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v0

    .line 92
    .local v0, "gridColumnCount":I
    const v2, 0x7f0f000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    const v2, 0x7f0e000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mCardItemsPerRow:I

    .line 100
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 101
    return-void

    .line 97
    :cond_0
    iput v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mCardItemsPerRow:I

    goto :goto_0
.end method


# virtual methods
.method protected bindPromoHeroImage(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 363
    return-void
.end method

.method protected configureContentView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 371
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mUseWideLayout:Z

    if-eqz v1, :cond_0

    .line 372
    new-instance v0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;-><init>(Lcom/google/android/finsky/activities/DetailsDataBasedFragment;)V

    .line 383
    .local v0, "wideHeroParallaxListener":Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->addOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 385
    .end local v0    # "wideHeroParallaxListener":Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    :cond_0
    return-void
.end method

.method protected configurePromoHeroImage(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "fragmentView"    # Landroid/view/View;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 321
    const v5, 0x7f0a00c8

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/HeroGraphicView;

    iput-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    .line 322
    const/4 v4, 0x0

    .line 323
    .local v4, "shouldEnableActionBarOverlay":Z
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    if-eqz v5, :cond_1

    .line 324
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->bindPromoHeroImage(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V

    .line 326
    iget-boolean v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mUseWideLayout:Z

    if-eqz v5, :cond_0

    .line 331
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 332
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0b00f9

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 333
    .local v1, "leadingGap":I
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setPlayIconVerticalSpan(I)V

    .line 337
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->isShowingNoImageFallbackFill()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 338
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    const v6, 0x7f0a014e

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 339
    .local v0, "dummyHeader":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    div-int/lit8 v6, v1, 0x2

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 344
    .end local v0    # "dummyHeader":Landroid/view/View;
    .end local v1    # "leadingGap":I
    .end local v3    # "res":Landroid/content/res/Resources;
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3

    const/4 v4, 0x1

    .line 347
    :cond_1
    :goto_0
    if-eqz v4, :cond_4

    .line 348
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mScrollListener:Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    if-eqz v5, :cond_2

    .line 349
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mScrollListener:Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->removeOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 351
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getLeadingHeroSectionId()I

    move-result v2

    .line 352
    .local v2, "leadingHeroSectionId":I
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-static {v5, v2, v6}, Lcom/google/android/finsky/utils/UiUtils;->enableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;ILcom/google/android/finsky/layout/actionbar/ActionBarController;)Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mScrollListener:Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    .line 354
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->enableActionBarOverlayScrolling()V

    .line 360
    .end local v2    # "leadingHeroSectionId":I
    :goto_1
    return-void

    .line 344
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 356
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mScrollListener:Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/utils/UiUtils;->disableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V

    .line 358
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->disableActionBarOverlayScrolling()V

    goto :goto_1
.end method

.method protected getDetailsData()Lcom/google/android/finsky/api/model/DfeDetails;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    return-object v0
.end method

.method public getDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method protected final getLibraries()Lcom/google/android/finsky/library/Libraries;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-object v0
.end method

.method public final getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected abstract getPlayStoreUiElementType()I
.end method

.method protected hasDetailsDataLoaded()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 212
    invoke-static {p0}, Lcom/google/android/finsky/utils/Nfc;->getHandler(Lcom/google/android/finsky/activities/DetailsDataBasedFragment;)Lcom/google/android/finsky/utils/Nfc$NfcHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNfcHandler:Lcom/google/android/finsky/utils/Nfc$NfcHandler;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.DetailsDataBasedFragment.document"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 217
    if-eqz p1, :cond_0

    .line 218
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->switchToBlank()V

    .line 223
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-nez v0, :cond_1

    .line 224
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->requestData()V

    .line 225
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->rebindActionBar()V

    .line 231
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onDataChanged()V

    .line 232
    return-void

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 391
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    if-nez v2, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v1

    .line 396
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getCurrentlyExpandedSectionId()I

    move-result v0

    .line 397
    .local v0, "currentlyExpandedSectionId":I
    if-eqz v0, :cond_0

    .line 401
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onSectionCollapsed(I)V

    .line 402
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPageCreationTime:J

    .line 187
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 188
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 193
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 195
    .local v0, "result":Landroid/view/View;
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .line 197
    const v1, 0x7f0a00c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .line 199
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    const v2, 0x7f0a0213

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .line 202
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->configure(Lcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 205
    :cond_0
    return-object v0
.end method

.method public onDataChanged()V
    .locals 4

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->hasDetailsDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0c01f1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/fragments/PageFragmentHost;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 297
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNfcHandler:Lcom/google/android/finsky/utils/Nfc$NfcHandler;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/Nfc$NfcHandler;->onDataChanged()V

    .line 299
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onDataChanged()V

    .line 301
    :cond_1
    return-void

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->updateDocument(Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 316
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onDestroyView()V

    .line 317
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onPause()V

    .line 241
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {v0}, Lcom/google/android/finsky/previews/PreviewController;->setupOnBackStackChangedListener(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNfcHandler:Lcom/google/android/finsky/utils/Nfc$NfcHandler;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/Nfc$NfcHandler;->onPause()V

    .line 244
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onResume()V

    .line 249
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNfcHandler:Lcom/google/android/finsky/utils/Nfc$NfcHandler;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/Nfc$NfcHandler;->onResume()V

    .line 250
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->recordState(Landroid/os/Bundle;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 258
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 259
    return-void
.end method

.method public onSectionCollapsed(I)V
    .locals 1
    .param p1, "sectionLayoutId"    # I

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSection()V

    .line 420
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->exitActionBarSectionExpandedMode()V

    .line 421
    return-void
.end method

.method public onSectionExpanded(I)V
    .locals 3
    .param p1, "sectionLayoutId"    # I

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->cancelScrollAnimationsIfRunning()V

    .line 410
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->expandSection(ILcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 412
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enterActionBarSectionExpandedMode(Ljava/lang/CharSequence;Lcom/google/android/finsky/activities/TextSectionTranslatable;)V

    .line 414
    return-void
.end method

.method protected final rebindViews()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeDetails;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0xd1

    invoke-direct {v0, v1, v2, v2, p0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;->setServerLogsCookie([B)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->hasDetailsDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSentImpression:Z

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 160
    iput-boolean v6, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSentImpression:Z

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->rebindViews(Landroid/os/Bundle;)V

    .line 167
    const-string v0, "Page [class=%s] loaded in [%s ms] (hasDetailsDataLoaded? %b)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPageCreationTime:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->hasDetailsDataLoaded()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method protected abstract rebindViews(Landroid/os/Bundle;)V
.end method

.method protected final recordState()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->recordState(Landroid/os/Bundle;)V

    .line 113
    return-void
.end method

.method protected abstract recordState(Landroid/os/Bundle;)V
.end method

.method protected requestData()V
    .locals 5

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 270
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 275
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getVoucherIds(Lcom/google/android/finsky/library/AccountLibrary;)Ljava/util/Collection;

    move-result-object v0

    .line 277
    .local v0, "voucherIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v1, Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mUrl:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;ZLjava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 278
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 279
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 280
    return-void
.end method

.method protected setInitialDocument(Lcom/google/android/finsky/api/model/Document;)V
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 122
    const-string v0, "finsky.DetailsDataBasedFragment.document"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->setArgument(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123
    return-void
.end method

.method protected updateDocument(Lcom/google/android/finsky/api/model/Document;)V
    .locals 3
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 129
    const/high16 v0, -0x80000000

    .line 130
    .local v0, "streamType":I
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 131
    const/4 v0, 0x3

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->setVolumeControlStream(I)V

    .line 134
    return-void
.end method
