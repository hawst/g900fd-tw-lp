.class public Lcom/google/android/finsky/activities/TabbedBrowseFragment;
.super Lcom/google/android/finsky/fragments/UrlBasedPageFragment;
.source "TabbedBrowseFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/TabbedBrowseFragment$PlayHeaderListConfigurator;
    }
.end annotation


# instance fields
.field private mBackendId:I

.field private mBackgroundType:I

.field private mBreadcrumb:Ljava/lang/String;

.field mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

.field private mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

.field private mFrameworkBundle:Landroid/os/Bundle;

.field private mRestorePrevSelectedTabIndex:I

.field private mRevealNextPageTransition:Ljava/lang/String;

.field mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;-><init>()V

    .line 58
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRestorePrevSelectedTabIndex:I

    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    .line 83
    new-instance v0, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    .line 458
    return-void
.end method

.method private clearState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 354
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 355
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 356
    .local v0, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 357
    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V

    .line 359
    .end local v0    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 361
    iput-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 363
    :cond_1
    iput-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    .line 364
    return-void
.end method

.method private getBreadcrumbText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 223
    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeBrowse;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "breadcrumb":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v1

    .line 226
    .local v1, "data":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-nez v1, :cond_1

    .line 228
    const-string v0, ""

    .line 238
    .end local v1    # "data":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_0
    :goto_0
    return-object v0

    .line 229
    .restart local v1    # "data":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canGoUp()Z

    move-result v2

    if-nez v2, :cond_2

    .line 232
    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0c01ae

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_2
    iget-object v0, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "backendId"    # I
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Landroid/util/Pair",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/activities/TabbedBrowseFragment;"
        }
    .end annotation

    .prologue
    .line 99
    .local p4, "sharedElement":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    new-instance v1, Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;-><init>()V

    .line 100
    .local v1, "fragment":Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    if-eqz p4, :cond_0

    iget-object v2, p4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 101
    iget-object v2, p4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRevealNextPageTransition:Ljava/lang/String;

    .line 104
    :cond_0
    if-ltz p2, :cond_1

    .line 105
    iput p2, v1, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    .line 108
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 109
    iput-object p1, v1, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    .line 113
    :cond_2
    invoke-static {p3, p0}, Lcom/google/android/finsky/api/model/DfeToc;->isAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 114
    const/4 v0, 0x1

    .line 119
    .local v0, "backgroundType":I
    :goto_0
    invoke-virtual {v1, p3, p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 120
    const-string v2, "TabbedBrowseFragment.BackgroundType"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->setArgument(Ljava/lang/String;I)V

    .line 121
    return-object v1

    .line 116
    .end local v0    # "backgroundType":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "backgroundType":I
    goto :goto_0
.end method

.method private recordState()V
    .locals 3

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.Breadcrumb"

    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.BackendId"

    iget v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 385
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.CurrentTab"

    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 390
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/TabbedAdapter;->onSaveInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V

    .line 393
    :cond_1
    return-void
.end method

.method private restoreFromFrameworkBundle()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.Breadcrumb"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.Breadcrumb"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    .line 183
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.BackendId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.BackendId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.CurrentTab"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "TabbedBrowseFragment.CurrentTab"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRestorePrevSelectedTabIndex:I

    .line 191
    :cond_1
    return-void

    .line 180
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;
    .locals 7
    .param p1, "frame"    # Lcom/google/android/finsky/layout/ContentFrame;

    .prologue
    .line 171
    new-instance v0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;

    const v2, 0x7f0a0219

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0109

    const/4 v6, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    return-object v0
.end method

.method public getBackendId()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    return v0
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 205
    const v0, 0x7f0400ae

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeBrowse;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 149
    if-eqz p1, :cond_0

    .line 150
    iput-object p1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->restoreFromFrameworkBundle()V

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->isDataReady()Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->switchToLoading()V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->requestData()V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->rebindActionBar()V

    .line 166
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 167
    return-void

    .line 163
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->rebindViews()V

    goto :goto_0
.end method

.method public onBeforeTabSelected(I)V
    .locals 1
    .param p1, "pagerIndex"    # I

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/TabbedAdapter;->onBeforeTabSelected(I)V

    .line 442
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 127
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->setRetainInstance(Z)V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "TabbedBrowseFragment.BackgroundType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackgroundType:I

    .line 129
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ContentFrame;

    .line 135
    .local v0, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v1, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 136
    .local v1, "headerListLayout":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    new-instance v2, Lcom/google/android/finsky/activities/TabbedBrowseFragment$PlayHeaderListConfigurator;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/finsky/activities/TabbedBrowseFragment$PlayHeaderListConfigurator;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 138
    const v2, 0x7f0a021c

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setContentViewId(I)V

    .line 139
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget v4, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 141
    return-object v0
.end method

.method public onDataChanged()V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeBrowse;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 248
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onDataChanged()V

    .line 250
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->recordState()V

    .line 398
    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->clearState()V

    .line 400
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detach()V

    .line 406
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onDestroyView()V

    .line 407
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "scrollState"    # I

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/TabbedAdapter;->onPageScrollStateChanged(I)V

    .line 418
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 422
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 426
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/activities/TabbedAdapter;->onPageSelected(I)V

    .line 429
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/activities/TabbedAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "selectedTabTitle":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 431
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0c03b2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 435
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->recordState()V

    .line 369
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFrameworkBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 370
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 371
    return-void
.end method

.method public onTabSelected(I)V
    .locals 0
    .param p1, "pagerIndex"    # I

    .prologue
    .line 446
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 339
    return-void
.end method

.method public rebindViews()V
    .locals 28

    .prologue
    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->switchToData()V

    .line 255
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    if-eqz v3, :cond_0

    .line 332
    :goto_0
    return-void

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeBrowse;->getBackendId()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    .line 266
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getBreadcrumbText()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->rebindActionBar()V

    .line 269
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 270
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 275
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeBrowse;->getBrowseTabs()[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    const/16 v26, 0x1

    .line 277
    .local v26, "shouldShowTabsHeader":Z
    :goto_1
    new-instance v3, Lcom/google/android/finsky/activities/TabbedAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/DfeBrowse;->getBrowseTabs()[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/DfeBrowse;->getQuickLinkList()[Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/DfeBrowse;->getQuickLinkTabIndex()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/DfeBrowse;->getQuickLinkFallbackTabIndex()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-object/from16 v18, v0

    if-nez v26, :cond_4

    const/16 v19, 0x1

    :goto_2
    move-object/from16 v17, p0

    invoke-direct/range {v3 .. v19}, Lcom/google/android/finsky/activities/TabbedAdapter;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/play/image/BitmapLoader;[Lcom/google/android/finsky/protos/Browse$BrowseTab;[Lcom/google/android/finsky/protos/Browse$QuickLink;IIILcom/google/android/finsky/utils/ObjectMap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    const v4, 0x7f0a021c

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 285
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0110

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDataView:Landroid/view/ViewGroup;

    move-object/from16 v24, v0

    check-cast v24, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 290
    .local v24, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->notifyPagerAdapterChanged()V

    .line 291
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/TabbedAdapter;->getCount()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_5

    const/16 v25, 0x0

    .line 293
    .local v25, "newTabMode":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move/from16 v0, v25

    invoke-static {v3, v0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v27

    .line 294
    .local v27, "tabHeight":I
    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setTabMode(II)V

    .line 296
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 297
    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V

    .line 299
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBackendId:I

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v22

    .line 300
    .local v22, "corpusPrimaryColor":I
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    move/from16 v0, v22

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 302
    const v3, 0x7f0a0039

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 305
    .local v20, "backgroundLayer":Landroid/view/View;
    if-eqz v20, :cond_2

    .line 306
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 307
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move/from16 v0, v25

    invoke-static {v3, v0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v21

    .line 309
    .local v21, "backgroundLayerHeight":I
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move/from16 v0, v21

    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 310
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->requestLayout()V

    .line 315
    .end local v21    # "backgroundLayerHeight":I
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRestorePrevSelectedTabIndex:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    .line 317
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRestorePrevSelectedTabIndex:I

    move/from16 v23, v0

    .line 318
    .local v23, "currentTabIndex":I
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mRestorePrevSelectedTabIndex:I

    .line 324
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    move/from16 v0, v23

    if-ne v3, v0, :cond_7

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/TabbedAdapter;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/activities/TabbedAdapter;->onPageSelected(I)V

    goto/16 :goto_0

    .line 275
    .end local v20    # "backgroundLayer":Landroid/view/View;
    .end local v22    # "corpusPrimaryColor":I
    .end local v23    # "currentTabIndex":I
    .end local v24    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .end local v25    # "newTabMode":I
    .end local v26    # "shouldShowTabsHeader":Z
    .end local v27    # "tabHeight":I
    :cond_3
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 277
    .restart local v26    # "shouldShowTabsHeader":Z
    :cond_4
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 291
    .restart local v24    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_5
    const/16 v25, 0x2

    goto/16 :goto_3

    .line 321
    .restart local v20    # "backgroundLayer":Landroid/view/View;
    .restart local v22    # "corpusPrimaryColor":I
    .restart local v25    # "newTabMode":I
    .restart local v27    # "tabHeight":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeBrowse;->getLandingTabIndex()I

    move-result v23

    .restart local v23    # "currentTabIndex":I
    goto :goto_4

    .line 330
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x0

    move/from16 v0, v23

    invoke-virtual {v3, v0, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto/16 :goto_0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->rebindViews()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->refresh()V

    goto :goto_0
.end method

.method protected requestData()V
    .locals 3

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->clearState()V

    .line 348
    new-instance v0, Lcom/google/android/finsky/api/model/DfeBrowse;

    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mUrl:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/api/model/DfeBrowse;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    .line 349
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeBrowse;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->mBrowseData:Lcom/google/android/finsky/api/model/DfeBrowse;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeBrowse;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 351
    return-void
.end method
