.class public Lcom/google/android/finsky/activities/RateReviewActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "RateReviewActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAuthor:Lcom/google/android/finsky/api/model/Document;

.field private mBackend:I

.field private mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

.field private mClickDebounce:Z

.field private mCommentView:Landroid/widget/TextView;

.field private mDocDetailsUrl:Ljava/lang/String;

.field private mDocId:Ljava/lang/String;

.field mEmptyTextWatcher:Landroid/text/TextWatcher;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mImpressionId:J

.field private mIsExternalRequest:Z

.field private mRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

.field private mRatingDescription:Landroid/widget/TextView;

.field private mReviewMode:I

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mTitleView:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 113
    new-instance v0, Lcom/google/android/finsky/activities/RateReviewActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/RateReviewActivity$1;-><init>(Lcom/google/android/finsky/activities/RateReviewActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    .line 129
    const/16 v0, 0x4b3

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/RateReviewActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->syncButtonEnabledState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/RateReviewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/activities/RateReviewActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mClickDebounce:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/activities/RateReviewActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/RateReviewActivity;->syncRatingDescription(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/activities/RateReviewActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/RateReviewActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/RateReviewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocDetailsUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/activities/RateReviewActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserRating()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/activities/RateReviewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/RateReviewActivity;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;IZ)Landroid/content/Intent;
    .locals 6
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "author"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "previousReview"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p4, "prefilledRating"    # I
    .param p5, "isExternalRequest"    # Z

    .prologue
    .line 151
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    const-class v4, Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "account_name"

    invoke-virtual {v0, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v3, "doc_id"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v3, "doc_details_url"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v3, "doc_title"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v3, "author"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 157
    const-string v3, "backend"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    const-string v3, "previous_rating"

    invoke-virtual {v0, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    if-eqz p3, :cond_0

    .line 160
    const-string v3, "previous_title"

    iget-object v4, p3, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const-string v3, "previous_comment"

    iget-object v4, p3, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v3, "previous_author"

    iget-object v4, p3, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v4}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165
    :cond_0
    const-string v3, "server_logs_cookie"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 166
    const-string v3, "impression_id"

    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getNextImpressionId()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 167
    const-string v3, "is_external_request"

    invoke-virtual {v0, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 168
    if-eqz p5, :cond_2

    .line 169
    const-string v3, "doc_creator"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 171
    .local v1, "res":Landroid/content/res/Resources;
    const/4 v3, 0x0

    const v4, 0x7f0b00d6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-static {p1, v3, v4, v5}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v2

    .line 174
    .local v2, "thumbnailImage":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v2, :cond_1

    .line 175
    const-string v3, "doc_thumbnail_url"

    iget-object v4, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    :cond_1
    const-string v3, "doc_thumbnail_is_fife"

    iget-boolean v4, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    .end local v1    # "res":Landroid/content/res/Resources;
    .end local v2    # "thumbnailImage":Lcom/google/android/finsky/protos/Common$Image;
    :cond_2
    return-object v0

    .line 171
    :array_0
    .array-data 4
        0x4
        0x0
    .end array-data
.end method

.method private getUserComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUserRating()I
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getRating()I

    move-result v0

    return v0
.end method

.method private getUserTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private syncButtonEnabledState()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 445
    const/4 v0, 0x0

    .line 446
    .local v0, "enabled":Z
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserRating()I

    move-result v6

    if-lez v6, :cond_0

    move v2, v4

    .line 448
    .local v2, "hasRatings":Z
    :goto_0
    iget v6, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mReviewMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    .line 451
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    move v3, v4

    .line 452
    .local v3, "hasTitle":Z
    :goto_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserComment()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move v1, v4

    .line 453
    .local v1, "hasComment":Z
    :goto_2
    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    move v0, v4

    .line 458
    .end local v1    # "hasComment":Z
    .end local v3    # "hasTitle":Z
    :goto_3
    iget-object v4, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 459
    return-void

    .end local v2    # "hasRatings":Z
    :cond_0
    move v2, v5

    .line 446
    goto :goto_0

    .restart local v2    # "hasRatings":Z
    :cond_1
    move v3, v5

    .line 451
    goto :goto_1

    .restart local v3    # "hasTitle":Z
    :cond_2
    move v1, v5

    .line 452
    goto :goto_2

    .restart local v1    # "hasComment":Z
    :cond_3
    move v0, v5

    .line 453
    goto :goto_3

    .line 456
    .end local v1    # "hasComment":Z
    .end local v3    # "hasTitle":Z
    :cond_4
    move v0, v2

    goto :goto_3
.end method

.method private syncRatingDescription(I)V
    .locals 4
    .param p1, "rating"    # I

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 433
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/finsky/utils/RateReviewHelper;->getRatingDescription(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 434
    if-nez p1, :cond_0

    .line 435
    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;

    const v3, 0x7f09005b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 441
    :goto_0
    return-void

    .line 437
    :cond_0
    iget v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mBackend:I

    invoke-static {p0, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 439
    .local v0, "colorStateList":Landroid/content/res/ColorStateList;
    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 495
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 463
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x4b7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 464
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 465
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "doc_id"

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 467
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 468
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 25
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 185
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 187
    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 189
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getWindow()Landroid/view/Window;

    move-result-object v19

    .line 192
    .local v19, "window":Landroid/view/Window;
    const/16 v20, 0x20

    const/16 v21, 0x20

    invoke-virtual/range {v19 .. v21}, Landroid/view/Window;->setFlags(II)V

    .line 193
    const/high16 v20, 0x40000

    const/high16 v21, 0x40000

    invoke-virtual/range {v19 .. v21}, Landroid/view/Window;->setFlags(II)V

    .line 196
    const v20, 0x7f04016e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->setContentView(I)V

    .line 200
    sget-object v20, Lcom/google/android/finsky/config/G;->enableReviewComments:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    if-eqz v20, :cond_1

    const/16 v20, 0x2

    :goto_0
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mReviewMode:I

    .line 203
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 206
    .local v7, "intent":Landroid/content/Intent;
    const-string v20, "account_name"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mAccountName:Ljava/lang/String;

    .line 207
    const-string v20, "is_external_request"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z

    .line 208
    const-string v20, "doc_id"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;

    .line 209
    const-string v20, "doc_details_url"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocDetailsUrl:Ljava/lang/String;

    .line 210
    const-string v20, "doc_title"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 211
    .local v5, "docTitle":Ljava/lang/String;
    const-string v20, "backend"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mBackend:I

    .line 212
    const-string v20, "previous_author"

    move-object/from16 v0, v20

    invoke-static {v7, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 214
    .local v11, "previousAuthorDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v11, :cond_2

    new-instance v10, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v10, v11}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 216
    .local v10, "previousAuthor":Lcom/google/android/finsky/api/model/Document;
    :goto_1
    const-string v20, "author"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    .line 223
    .local v4, "author":Lcom/google/android/finsky/api/model/Document;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v20, v0

    const-string v21, "previous_rating"

    invoke-virtual/range {v20 .. v21}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 225
    .local v13, "previousRating":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v20, v0

    const-string v21, "previous_title"

    invoke-virtual/range {v20 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 226
    .local v14, "previousTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v20, v0

    const-string v21, "previous_comment"

    invoke-virtual/range {v20 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 234
    .local v12, "previousComment":Ljava/lang/String;
    :goto_2
    const v20, 0x7f0a0325

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 235
    .local v16, "ratingSetterFrame":Landroid/view/View;
    const v20, 0x7f0a0328

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/EditText;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    .line 236
    const v20, 0x7f0a0329

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/EditText;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    .line 238
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mReviewMode:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 250
    const-string v20, "server_logs_cookie"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v18

    .line 251
    .local v18, "serverLogsCookie":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 252
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAccountName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 253
    const-string v20, "impression_id"

    const-wide/16 v22, 0x0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/finsky/activities/RateReviewActivity;->mImpressionId:J

    .line 254
    if-nez p1, :cond_0

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mImpressionId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-object/from16 v3, p0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 259
    :cond_0
    if-eqz v11, :cond_5

    const/4 v8, 0x1

    .line 260
    .local v8, "isEditingReview":Z
    :goto_4
    const v20, 0x7f0a0114

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v21, v0

    if-eqz v8, :cond_6

    const v20, 0x7f0c024c

    :goto_5
    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonVisible(Z)V

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v20, v0

    const v21, 0x7f0c024d

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    move-object/from16 v20, v0

    new-instance v21, Lcom/google/android/finsky/activities/RateReviewActivity$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8}, Lcom/google/android/finsky/activities/RateReviewActivity$2;-><init>(Lcom/google/android/finsky/activities/RateReviewActivity;Z)V

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 342
    const v20, 0x7f0a0326

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    .line 343
    const v20, 0x7f0a0327

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;

    .line 344
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/finsky/activities/RateReviewActivity;->syncRatingDescription(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mBackend:I

    move/from16 v21, v0

    new-instance v22, Lcom/google/android/finsky/activities/RateReviewActivity$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/activities/RateReviewActivity$3;-><init>(Lcom/google/android/finsky/activities/RateReviewActivity;)V

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v13, v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->configure(IILcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;)V

    .line 361
    const v20, 0x7f0a0235

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 362
    .local v9, "itemTitle":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mIsExternalRequest:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 363
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 364
    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    :goto_6
    if-eqz v10, :cond_8

    .line 377
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;

    .line 381
    :goto_7
    const v20, 0x7f0a034f

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 382
    .local v17, "reviewBy":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0c024a

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-virtual/range {v20 .. v22}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    const v20, 0x7f0a034e

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/play/image/FifeImageView;

    .line 384
    .local v15, "profilePic":Lcom/google/android/play/image/FifeImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v20

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/protos/Common$Image;

    .line 385
    .local v6, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v0, v6, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    iget-boolean v0, v6, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v21, v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v22

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v15, v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 387
    return-void

    .line 200
    .end local v4    # "author":Lcom/google/android/finsky/api/model/Document;
    .end local v5    # "docTitle":Ljava/lang/String;
    .end local v6    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "isEditingReview":Z
    .end local v9    # "itemTitle":Landroid/widget/TextView;
    .end local v10    # "previousAuthor":Lcom/google/android/finsky/api/model/Document;
    .end local v11    # "previousAuthorDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v12    # "previousComment":Ljava/lang/String;
    .end local v13    # "previousRating":I
    .end local v14    # "previousTitle":Ljava/lang/String;
    .end local v15    # "profilePic":Lcom/google/android/play/image/FifeImageView;
    .end local v16    # "ratingSetterFrame":Landroid/view/View;
    .end local v17    # "reviewBy":Landroid/widget/TextView;
    .end local v18    # "serverLogsCookie":[B
    :cond_1
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 214
    .restart local v5    # "docTitle":Ljava/lang/String;
    .restart local v7    # "intent":Landroid/content/Intent;
    .restart local v11    # "previousAuthorDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_2
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 228
    .restart local v4    # "author":Lcom/google/android/finsky/api/model/Document;
    .restart local v10    # "previousAuthor":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    const-string v20, "previous_rating"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 229
    .restart local v13    # "previousRating":I
    const-string v20, "previous_title"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 230
    .restart local v14    # "previousTitle":Ljava/lang/String;
    const-string v20, "previous_comment"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "previousComment":Ljava/lang/String;
    goto/16 :goto_2

    .line 242
    .restart local v16    # "ratingSetterFrame":Landroid/view/View;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 259
    .restart local v18    # "serverLogsCookie":[B
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 262
    .restart local v8    # "isEditingReview":Z
    :cond_6
    const v20, 0x7f0c024b

    goto/16 :goto_5

    .line 366
    .restart local v9    # "itemTitle":Landroid/widget/TextView;
    :cond_7
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 379
    :cond_8
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/finsky/activities/RateReviewActivity;->mAuthor:Lcom/google/android/finsky/api/model/Document;

    goto/16 :goto_7
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mCommentView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 394
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 395
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 476
    const-string v0, "previous_rating"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserRating()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 477
    const-string v0, "previous_title"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    const-string v0, "previous_comment"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->getUserComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 399
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 400
    invoke-direct {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->syncButtonEnabledState()V

    .line 401
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 405
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 407
    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x4b7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 408
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 409
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "doc_id"

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity;->mDocId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 412
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/RateReviewActivity;->finish()V

    .line 413
    const/4 v1, 0x1

    .line 416
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method
