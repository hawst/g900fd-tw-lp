.class public Lcom/google/android/finsky/billing/BillingEventRecorder;
.super Ljava/lang/Object;
.source "BillingEventRecorder.java"


# static fields
.field private static final LOGGING_ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

.field private static final NOP_LISTENER:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/finsky/billing/BillingEventRecorder$1;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/BillingEventRecorder$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/BillingEventRecorder;->NOP_LISTENER:Lcom/android/volley/Response$Listener;

    .line 36
    new-instance v0, Lcom/google/android/finsky/billing/BillingEventRecorder$2;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/BillingEventRecorder$2;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/BillingEventRecorder;->LOGGING_ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    return-void
.end method

.method public static enableBillingEventLogging(Z)V
    .locals 2
    .param p0, "enable"    # Z

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/finsky/config/G;->logBillingEventsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->override(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method public static recordError(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p0, "carrierId"    # Ljava/lang/String;
    .param p1, "eventType"    # I
    .param p2, "errorCode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 79
    sget-object v2, Lcom/google/android/finsky/config/G;->logBillingEventsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    :goto_0
    return-void

    .line 83
    :cond_0
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;-><init>()V

    .line 84
    .local v0, "request":Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
    iput-object p0, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    .line 85
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    .line 86
    iput p1, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    .line 87
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    .line 88
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    .line 89
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    .line 90
    iput-object p2, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    .line 91
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasClientMessage:Z

    .line 92
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getVendingApi()Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v1

    .line 93
    .local v1, "vendingApi":Lcom/google/android/vending/remoting/api/VendingApi;
    sget-object v2, Lcom/google/android/finsky/billing/BillingEventRecorder;->NOP_LISTENER:Lcom/android/volley/Response$Listener;

    sget-object v3, Lcom/google/android/finsky/billing/BillingEventRecorder;->LOGGING_ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/vending/remoting/api/VendingApi;->recordBillingEvent(Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method public static recordSuccess(Ljava/lang/String;IZ)V
    .locals 4
    .param p0, "carrierId"    # Ljava/lang/String;
    .param p1, "eventType"    # I
    .param p2, "resultSuccess"    # Z

    .prologue
    const/4 v3, 0x1

    .line 58
    sget-object v2, Lcom/google/android/finsky/config/G;->logBillingEventsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 71
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;-><init>()V

    .line 63
    .local v0, "request":Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
    iput-object p0, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    .line 64
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    .line 65
    iput p1, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    .line 66
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    .line 67
    iput-boolean p2, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    .line 68
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    .line 69
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getVendingApi()Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v1

    .line 70
    .local v1, "vendingApi":Lcom/google/android/vending/remoting/api/VendingApi;
    sget-object v2, Lcom/google/android/finsky/billing/BillingEventRecorder;->NOP_LISTENER:Lcom/android/volley/Response$Listener;

    sget-object v3, Lcom/google/android/finsky/billing/BillingEventRecorder;->LOGGING_ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/vending/remoting/api/VendingApi;->recordBillingEvent(Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method
