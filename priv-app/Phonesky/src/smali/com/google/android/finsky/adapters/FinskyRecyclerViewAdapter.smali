.class public abstract Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;
.super Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;
.source "FinskyRecyclerViewAdapter.java"


# instance fields
.field protected mContainerList:Lcom/google/android/finsky/api/model/ContainerList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/api/model/ContainerList",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/ContainerList;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "containerList"    # Lcom/google/android/finsky/api/model/ContainerList;

    .prologue
    .line 28
    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/ContainerList;->inErrorState()Z

    move-result v0

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/ContainerList;->isMoreAvailable()Z

    move-result v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZZ)V

    .line 29
    iput-object p3, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    .line 30
    iget-object v0, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/ContainerList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected getLastRequestError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected isMoreDataAvailable()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->isMoreAvailable()Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/ContainerList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 41
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "restoreBundle"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const-string v0, "ListTab.RecyclerViewParcelKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->restoreInstanceState(Landroid/os/Parcelable;)V

    .line 80
    return-void
.end method

.method public onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 50
    const-string v0, "ListTab.RecyclerViewParcelKey"

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->saveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 51
    return-void
.end method

.method protected retryLoadingItems()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->retryLoadItems()V

    .line 61
    return-void
.end method

.method public updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/ContainerList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "containerList":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<*>;"
    iput-object p1, p0, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 75
    return-void
.end method
