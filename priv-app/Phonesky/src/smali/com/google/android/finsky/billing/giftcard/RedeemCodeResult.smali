.class public Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
.super Ljava/lang/Object;
.source "RedeemCodeResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mExtraPurchaseData:Landroid/os/Bundle;

.field private final mIsInstallAppDeferred:Z

.field private final mRedeemedOfferHtml:Ljava/lang/String;

.field private final mStoredValueInstrumentId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult$1;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mIsInstallAppDeferred:Z

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 32
    return-void

    :cond_1
    move-object v0, v1

    .line 28
    goto :goto_0

    .line 30
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "redeemedOfferHtml"    # Ljava/lang/String;
    .param p2, "storedValueInstrumentId"    # Ljava/lang/String;
    .param p3, "isInstallAppDeferred"    # Z
    .param p4, "extraPurchaseData"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    .line 23
    iput-boolean p3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mIsInstallAppDeferred:Z

    .line 24
    iput-object p4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public getExtraPurchaseData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mExtraPurchaseData:Landroid/os/Bundle;

    return-object v0
.end method

.method public getRedeemedOfferHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getStoredValueInstrumentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    return-object v0
.end method

.method public isInstallAppDeferred()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mIsInstallAppDeferred:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mRedeemedOfferHtml:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mIsInstallAppDeferred:Z

    if-eqz v0, :cond_4

    :goto_2
    int-to-byte v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->mExtraPurchaseData:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 46
    return-void

    :cond_2
    move v0, v2

    .line 36
    goto :goto_0

    :cond_3
    move v0, v2

    .line 40
    goto :goto_1

    :cond_4
    move v2, v1

    .line 44
    goto :goto_2
.end method
