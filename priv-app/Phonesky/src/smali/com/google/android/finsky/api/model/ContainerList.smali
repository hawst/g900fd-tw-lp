.class public abstract Lcom/google/android/finsky/api/model/ContainerList;
.super Lcom/google/android/finsky/api/model/PaginatedList;
.source "ContainerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/finsky/api/model/PaginatedList",
        "<TT;",
        "Lcom/google/android/finsky/api/model/Document;",
        ">;"
    }
.end annotation


# instance fields
.field private mContainerDocument:Lcom/google/android/finsky/api/model/Document;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 26
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "autoLoadNextPage"    # Z

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/lang/String;Z)V

    .line 36
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;IZ)V
    .locals 0
    .param p2, "currentCount"    # I
    .param p3, "autoLoadNextPage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    .local p1, "urlList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/util/List;IZ)V

    .line 46
    return-void
.end method


# virtual methods
.method public getBackendId()I
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId(I)I

    move-result v0

    return v0
.end method

.method public getBackendId(I)I
    .locals 1
    .param p1, "defaultBackendId"    # I

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/ContainerList;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/api/model/ContainerList;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result p1

    .line 69
    .end local p1    # "defaultBackendId":I
    :cond_0
    return p1
.end method

.method public getContainerDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 1

    .prologue
    .line 52
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/ContainerList;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method protected updateContainerAndGetItems(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)[Lcom/google/android/finsky/api/model/Document;
    .locals 5
    .param p1, "containerDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<TT;>;"
    if-eqz p1, :cond_0

    .line 78
    new-instance v3, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v3, p1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    iput-object v3, p0, Lcom/google/android/finsky/api/model/ContainerList;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    .line 79
    iget-object v3, p1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v3

    .line 80
    .local v2, "numChildren":I
    new-array v1, v2, [Lcom/google/android/finsky/api/model/Document;

    .line 81
    .local v1, "items":[Lcom/google/android/finsky/api/model/Document;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 82
    new-instance v3, Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v4, v4, v0

    invoke-direct {v3, v4}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    aput-object v3, v1, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "i":I
    .end local v1    # "items":[Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "numChildren":I
    :cond_0
    const/4 v3, 0x0

    new-array v1, v3, [Lcom/google/android/finsky/api/model/Document;

    :cond_1
    return-object v1
.end method
