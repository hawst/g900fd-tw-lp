.class public abstract Lcom/google/android/finsky/billing/InstrumentFlow;
.super Lcom/google/android/finsky/billing/BillingFlow;
.source "InstrumentFlow.java"


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "billingFlowContext"    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2, "listener"    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3, "parameters"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/BillingFlow;-><init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    .local v0, "result":Landroid/os/Bundle;
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    if-eqz v1, :cond_0

    .line 42
    const-string v1, "instrument_id"

    iget-object v2, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v1, :cond_1

    .line 46
    const-string v1, "redeemed_offer_message_html"

    iget-object v2, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    iget-object v2, v2, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/InstrumentFlow;->finish(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method
