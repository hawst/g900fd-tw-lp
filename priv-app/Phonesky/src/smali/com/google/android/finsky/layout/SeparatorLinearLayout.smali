.class public Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SeparatorLinearLayout.java"


# instance fields
.field private mDrawSeparator:Z

.field private final mHalfSeparatorThickness:I

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private mSeparatorPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setWillNotDraw(Z)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    .local v0, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b0052

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 42
    .local v1, "separatorThickness":I
    add-int/lit8 v3, v1, 0x1

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mHalfSeparatorThickness:I

    .line 43
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    .line 44
    iget-object v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getSeparatorColor(Landroid/content/res/Resources;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 45
    iget-object v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    .line 48
    sget-object v3, Lcom/android/vending/R$styleable;->SeparatorLinearLayout:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 51
    .local v2, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v3, 0x2

    invoke-virtual {v2, v5, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPosition:I

    .line 54
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    return-void
.end method


# virtual methods
.method protected getSeparatorColor(Landroid/content/res/Resources;)I
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 58
    const v0, 0x7f090053

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public hideSeparator()V
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->invalidate()V

    .line 73
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    if-eqz v0, :cond_1

    .line 78
    iget v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPosition:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 79
    iget v7, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mHalfSeparatorThickness:I

    .line 80
    .local v7, "topY":I
    int-to-float v2, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 82
    .end local v7    # "topY":I
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPosition:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mHalfSeparatorThickness:I

    sub-int v6, v0, v2

    .line 84
    .local v6, "bottomY":I
    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 88
    .end local v6    # "bottomY":I
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 89
    return-void
.end method

.method public showSeparator()V
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->mDrawSeparator:Z

    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->invalidate()V

    .line 66
    :cond_0
    return-void
.end method
