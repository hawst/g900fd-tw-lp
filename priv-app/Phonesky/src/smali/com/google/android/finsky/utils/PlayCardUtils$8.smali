.class final Lcom/google/android/finsky/utils/PlayCardUtils$8;
.super Ljava/lang/Object;
.source "PlayCardUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PlayCardUtils;->configureOverflowView(Lcom/google/android/play/layout/PlayCardViewBase;Landroid/widget/ImageView;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$card:Lcom/google/android/play/layout/PlayCardViewBase;

.field final synthetic val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$dismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$overflowView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$overflowView:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p4, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p5, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p6, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$card:Lcom/google/android/play/layout/PlayCardViewBase;

    iput-object p7, p0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$dismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 24
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 826
    new-instance v3, Lcom/google/android/play/layout/PlayPopupMenu;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$overflowView:Landroid/widget/ImageView;

    invoke-direct {v3, v4, v5}, Lcom/google/android/play/layout/PlayPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 827
    .local v3, "popupMenu":Lcom/google/android/play/layout/PlayPopupMenu;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 828
    .local v20, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v15

    .line 829
    .local v15, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    const/16 v4, 0xee

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v15, v4, v5, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 831
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v10

    .line 832
    .local v10, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-interface {v10}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    .line 833
    .local v6, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    # invokes: Lcom/google/android/finsky/utils/PlayCardUtils;->configureActions(Lcom/google/android/play/layout/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/utils/PlayCardUtils;->access$500(Lcom/google/android/play/layout/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 835
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v4, v10}, Lcom/google/android/finsky/utils/WishlistHelper;->shouldHideWishlistAction(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 838
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 839
    const v22, 0x7f0c037c

    .line 840
    .local v22, "wishlistActionTitleId":I
    const/16 v23, 0xcd

    .line 845
    .local v23, "wishlistClickEventType":I
    :goto_0
    new-instance v21, Lcom/google/android/finsky/utils/PlayCardUtils$8$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move/from16 v2, v23

    invoke-direct {v0, v1, v15, v2, v10}, Lcom/google/android/finsky/utils/PlayCardUtils$8$1;-><init>(Lcom/google/android/finsky/utils/PlayCardUtils$8;Lcom/google/android/finsky/analytics/FinskyEventLog;ILcom/google/android/finsky/api/DfeApi;)V

    .line 853
    .local v21, "wishlistAction":Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v3, v4, v5, v0}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 857
    .end local v21    # "wishlistAction":Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    .end local v22    # "wishlistActionTitleId":I
    .end local v23    # "wishlistClickEventType":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$dismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->isDismissable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 858
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getNeutralDismissal()Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    move-result-object v4

    iget-object v14, v4, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;->descriptionHtml:Ljava/lang/String;

    .line 859
    .local v14, "dismissalText":Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 860
    const/4 v4, 0x1

    new-instance v7, Lcom/google/android/finsky/utils/PlayCardUtils$CardDismissalAction;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$card:Lcom/google/android/play/layout/PlayCardViewBase;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$dismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {v7 .. v12}, Lcom/google/android/finsky/utils/PlayCardUtils$CardDismissalAction;-><init>(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {v3, v14, v4, v7}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 868
    .end local v14    # "dismissalText":Ljava/lang/String;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getOverflowLinks()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    move-result-object v19

    .line 869
    .local v19, "overflowLinks":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v19, :cond_4

    move-object/from16 v0, v19

    array-length v4, v0

    if-lez v4, :cond_4

    .line 870
    move-object/from16 v13, v19

    .local v13, "arr$":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    array-length v0, v13

    move/from16 v17, v0

    .local v17, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_2
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_4

    aget-object v18, v13, v16

    .line 871
    .local v18, "overflowLink":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->title:Ljava/lang/String;

    const/4 v5, 0x1

    new-instance v7, Lcom/google/android/finsky/utils/PlayCardUtils$8$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v7, v0, v1}, Lcom/google/android/finsky/utils/PlayCardUtils$8$2;-><init>(Lcom/google/android/finsky/utils/PlayCardUtils$8;Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;)V

    invoke-virtual {v3, v4, v5, v7}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 870
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 842
    .end local v13    # "arr$":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v16    # "i$":I
    .end local v17    # "len$":I
    .end local v18    # "overflowLink":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v19    # "overflowLinks":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_2
    const v22, 0x7f0c037b

    .line 843
    .restart local v22    # "wishlistActionTitleId":I
    const/16 v23, 0xcc

    .restart local v23    # "wishlistClickEventType":I
    goto/16 :goto_0

    .line 863
    .end local v22    # "wishlistActionTitleId":I
    .end local v23    # "wishlistClickEventType":I
    .restart local v14    # "dismissalText":Ljava/lang/String;
    :cond_3
    const-string v4, "Empty dismissal text received from the server for doc %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 886
    .end local v14    # "dismissalText":Ljava/lang/String;
    .restart local v19    # "overflowLinks":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;->val$overflowView:Landroid/widget/ImageView;

    const v5, 0x7f020198

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 887
    new-instance v4, Lcom/google/android/finsky/utils/PlayCardUtils$8$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/finsky/utils/PlayCardUtils$8$3;-><init>(Lcom/google/android/finsky/utils/PlayCardUtils$8;)V

    invoke-virtual {v3, v4}, Lcom/google/android/play/layout/PlayPopupMenu;->setOnPopupDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 895
    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayPopupMenu;->show()V

    .line 896
    return-void
.end method
