.class Lcom/google/android/finsky/activities/DetailsFragment$1;
.super Ljava/lang/Object;
.source "DetailsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsFragment;->updateDetailsSections(Landroid/os/Bundle;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsFragment;

.field final synthetic val$detailsData:Lcom/google/android/finsky/api/model/DfeDetails;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$hasDetailsDataLoaded:Z

.field final synthetic val$savedInstanceState:Landroid/os/Bundle;

.field final synthetic val$socialDataDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$socialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field final synthetic val$socialDoc:Lcom/google/android/finsky/api/model/Document;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$savedInstanceState:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$detailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    iput-object p5, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDataDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p6, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iput-object p7, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDoc:Lcom/google/android/finsky/api/model/Document;

    iput-boolean p8, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$hasDetailsDataLoaded:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$savedInstanceState:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$detailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDataDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$socialDoc:Lcom/google/android/finsky/api/model/Document;

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/DetailsFragment$1;->val$hasDetailsDataLoaded:Z

    # invokes: Lcom/google/android/finsky/activities/DetailsFragment;->updateSecondaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/activities/DetailsFragment;->access$200(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V

    .line 475
    return-void
.end method
