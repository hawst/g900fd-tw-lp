.class abstract Lcom/google/android/finsky/providers/SuggestionFetcher;
.super Ljava/lang/Object;
.source "SuggestionFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected final mQuery:Ljava/lang/String;

.field protected mStartTimeMs:J

.field protected final mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "suggestionHandler"    # Lcom/google/android/finsky/providers/SuggestionHandler;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/finsky/providers/SuggestionFetcher;->mQuery:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/google/android/finsky/providers/SuggestionFetcher;->mContext:Landroid/content/Context;

    .line 40
    iput-object p3, p0, Lcom/google/android/finsky/providers/SuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    .line 41
    return-void
.end method


# virtual methods
.method public gatherSuggestions()V
    .locals 3

    .prologue
    .line 72
    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 74
    .local v1, "sem":Ljava/util/concurrent/Semaphore;
    new-instance v2, Lcom/google/android/finsky/providers/SuggestionFetcher$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/finsky/providers/SuggestionFetcher$1;-><init>(Lcom/google/android/finsky/providers/SuggestionFetcher;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/providers/SuggestionFetcher;->makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V

    .line 87
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0
.end method

.method protected getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
.end method

.method public startRequestLatencyTimer()V
    .locals 2

    .prologue
    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/providers/SuggestionFetcher;->mStartTimeMs:J

    .line 48
    return-void
.end method
