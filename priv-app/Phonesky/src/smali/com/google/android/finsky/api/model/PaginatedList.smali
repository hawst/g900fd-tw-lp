.class public abstract Lcom/google/android/finsky/api/model/PaginatedList;
.super Lcom/google/android/finsky/api/model/DfeModel;
.source "PaginatedList.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "D:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/finsky/api/model/DfeModel;",
        "Lcom/android/volley/Response$Listener",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final mAutoLoadNextPage:Z

.field private mCurrentOffset:I

.field private mCurrentRequest:Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end field

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TD;>;"
        }
    .end annotation
.end field

.field private mItemsRemoved:Z

.field private mItemsUntilEndCount:I

.field private mLastPositionRequested:I

.field protected mLastResponse:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mMoreAvailable:Z

.field protected mUrlOffsetList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;",
            ">;"
        }
    .end annotation
.end field

.field private mWindowDistance:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/lang/String;Z)V

    .line 72
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "autoLoadNextPage"    # Z

    .prologue
    .line 74
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 38
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mWindowDistance:I

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    .line 56
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsUntilEndCount:I

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    .line 77
    iput-boolean p2, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mAutoLoadNextPage:Z

    .line 78
    return-void
.end method

.method protected constructor <init>(Ljava/util/List;IZ)V
    .locals 3
    .param p2, "currentCount"    # I
    .param p3, "autoLoadNextPage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    .local p1, "urlMap":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;>;"
    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, v2, p3}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/lang/String;Z)V

    .line 84
    iput-object p1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    .line 87
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    return-void
.end method

.method private requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V
    .locals 2
    .param p1, "urlOffsetPair"    # Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .prologue
    .line 472
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->inErrorState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 478
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->cancel()V

    .line 485
    :cond_2
    iget v0, p1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    iput v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    .line 486
    iget-object v0, p1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/model/PaginatedList;->makeRequest(Ljava/lang/String;)Lcom/android/volley/Request;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    goto :goto_0
.end method

.method private updateItemsUntilEndCount(I)V
    .locals 2
    .param p1, "lastPageSize"    # I

    .prologue
    .line 379
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsUntilEndCount:I

    if-gtz v0, :cond_0

    .line 382
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsUntilEndCount:I

    .line 388
    :goto_0
    return-void

    .line 386
    :cond_0
    const/4 v0, 0x1

    div-int/lit8 v1, p1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsUntilEndCount:I

    goto :goto_0
.end method


# virtual methods
.method public clearDataAndReplaceInitialUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 93
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->resetItems()V

    .line 96
    return-void
.end method

.method protected abstract clearDiskCache()V
.end method

.method public clearTransientState()V
    .locals 1

    .prologue
    .line 116
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    .line 117
    return-void
.end method

.method public flushUnusedPages()V
    .locals 3

    .prologue
    .line 328
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastPositionRequested:I

    if-gez v1, :cond_1

    .line 339
    :cond_0
    return-void

    .line 332
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 333
    iget v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastPositionRequested:I

    iget v2, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mWindowDistance:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastPositionRequested:I

    iget v2, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mWindowDistance:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_2

    .line 332
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 337
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TD;"
        }
    .end annotation

    .prologue
    .line 261
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/api/model/PaginatedList;->getItem(IZ)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(IZ)Ljava/lang/Object;
    .locals 9
    .param p1, "position"    # I
    .param p2, "autoFetchNewItems"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)TD;"
        }
    .end annotation

    .prologue
    .line 183
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    if-eqz p2, :cond_0

    .line 184
    iput p1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastPositionRequested:I

    .line 186
    :cond_0
    const/4 v4, 0x0

    .line 187
    .local v4, "result":Ljava/lang/Object;, "TD;"
    if-gez p1, :cond_1

    .line 188
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Can\'t return an item with a negative index: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 191
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->getCount()I

    move-result v6

    if-ge p1, v6, :cond_7

    .line 192
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 193
    iget-boolean v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mAutoLoadNextPage:Z

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->getCount()I

    move-result v6

    iget v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsUntilEndCount:I

    sub-int/2addr v6, v7

    if-lt p1, v6, :cond_5

    .line 195
    iget-boolean v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsRemoved:Z

    if-eqz v6, :cond_4

    .line 211
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_5

    .line 213
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget v6, v6, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    iget-object v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v6, v7, :cond_3

    .line 216
    const/4 v6, 0x1

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 217
    .local v3, "newUrlListSize":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v3, :cond_2

    .line 218
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 221
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 222
    .local v5, "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    if-eqz p2, :cond_3

    .line 223
    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/model/PaginatedList;->requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V

    .line 211
    .end local v3    # "newUrlListSize":I
    .end local v5    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    .end local v1    # "i":I
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 229
    .restart local v5    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    if-eqz p2, :cond_5

    .line 230
    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/model/PaginatedList;->requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V

    .line 235
    .end local v5    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_5
    if-nez v4, :cond_7

    .line 236
    const/4 v5, 0x0

    .line 237
    .restart local v5    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget-object v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 238
    .local v0, "currentWrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget v6, v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    if-gt v6, p1, :cond_6

    .line 239
    move-object v5, v0

    .line 243
    goto :goto_2

    .line 246
    .end local v0    # "currentWrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_6
    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/model/PaginatedList;->requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V

    .line 249
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "result":Ljava/lang/Object;, "TD;"
    .end local v5    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_7
    return-object v4
.end method

.method protected abstract getItemsFromResponse(Ljava/lang/Object;)[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[TD;"
        }
    .end annotation
.end method

.method public getListPageUrls()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    .local v2, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 104
    .local v1, "pair":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget-object v3, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    .end local v1    # "pair":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_0
    return-object v2
.end method

.method protected abstract getNextPageUrl(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public isMoreAvailable()Z
    .locals 1

    .prologue
    .line 364
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    return v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 146
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastResponse:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract makeRequest(Ljava/lang/String;)Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 151
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearTransientState()V

    .line 152
    invoke-super {p0, p1}, Lcom/google/android/finsky/api/model/DfeModel;->onErrorResponse(Lcom/android/volley/VolleyError;)V

    .line 153
    return-void
.end method

.method public onResponse(Ljava/lang/Object;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    .local p1, "response":Ljava/lang/Object;, "TT;"
    const/4 v6, 0x0

    .line 398
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearErrors()V

    .line 399
    iput-object p1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mLastResponse:Ljava/lang/Object;

    .line 401
    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 403
    .local v4, "oldSize":I
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/PaginatedList;->getItemsFromResponse(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .line 405
    .local v2, "items":[Ljava/lang/Object;, "[TD;"
    array-length v5, v2

    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/model/PaginatedList;->updateItemsUntilEndCount(I)V

    .line 407
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v2

    if-ge v0, v5, :cond_1

    .line 408
    iget v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    add-int/2addr v5, v0

    iget-object v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_0

    .line 409
    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    iget v7, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    add-int/2addr v7, v0

    aget-object v8, v2, v0

    invoke-interface {v5, v7, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 407
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    aget-object v7, v2, v0

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 416
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/PaginatedList;->getNextPageUrl(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 418
    .local v3, "nextPageUrl":Ljava/lang/String;
    const/4 v1, 0x0

    .line 424
    .local v1, "isNextRequestAtEndOfListAvail":Z
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    if-eq v5, v4, :cond_2

    iget-boolean v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsRemoved:Z

    if-eqz v5, :cond_3

    .line 425
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    new-instance v7, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget-object v8, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v7, v8, v3}, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;-><init>(ILjava/lang/String;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsRemoved:Z

    if-eqz v5, :cond_4

    .line 429
    iput-boolean v6, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsRemoved:Z

    .line 434
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    iget-object v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget v5, v5, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    if-ne v7, v5, :cond_5

    array-length v5, v2

    if-lez v5, :cond_5

    .line 436
    const/4 v1, 0x1

    .line 438
    :cond_5
    if-eqz v1, :cond_6

    iget-boolean v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mAutoLoadNextPage:Z

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_2
    iput-boolean v5, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    .line 441
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearTransientState()V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->notifyDataSetChanged()V

    .line 443
    return-void

    :cond_6
    move v5, v6

    .line 438
    goto :goto_2
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 282
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItemsRemoved:Z

    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->cancel()V

    .line 292
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearDiskCache()V

    .line 293
    return-void
.end method

.method public resetItems()V
    .locals 1

    .prologue
    .line 354
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    .line 355
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 356
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->notifyDataSetChanged()V

    .line 357
    return-void
.end method

.method public retryLoadItems()V
    .locals 5

    .prologue
    .line 301
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->inErrorState()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 303
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearTransientState()V

    .line 305
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearErrors()V

    .line 308
    const/4 v2, 0x0

    .line 309
    .local v2, "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 310
    iget-object v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 311
    .local v0, "currentWrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mCurrentOffset:I

    iget v4, v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    if-ne v3, v4, :cond_0

    .line 312
    move-object v2, v0

    .line 317
    .end local v0    # "currentWrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    if-nez v2, :cond_2

    .line 318
    iget-object v3, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    check-cast v2, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 320
    .restart local v2    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/finsky/api/model/PaginatedList;->requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V

    .line 322
    .end local v2    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_3
    return-void
.end method

.method public setWindowDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 127
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iput p1, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mWindowDistance:I

    .line 128
    return-void
.end method

.method public startLoadItems()V
    .locals 2

    .prologue
    .line 166
    .local p0, "this":Lcom/google/android/finsky/api/model/PaginatedList;, "Lcom/google/android/finsky/api/model/PaginatedList<TT;TD;>;"
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mMoreAvailable:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/PaginatedList;->clearErrors()V

    .line 168
    iget-object v0, p0, Lcom/google/android/finsky/api/model/PaginatedList;->mUrlOffsetList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/model/PaginatedList;->requestMoreItemsIfNoRequestExists(Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;)V

    .line 170
    :cond_0
    return-void
.end method
