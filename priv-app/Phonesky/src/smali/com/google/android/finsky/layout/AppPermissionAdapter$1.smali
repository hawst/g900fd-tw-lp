.class Lcom/google/android/finsky/layout/AppPermissionAdapter$1;
.super Ljava/lang/Object;
.source "AppPermissionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/AppPermissionAdapter;->getExistingPermissionsView(Landroid/view/ViewGroup;Ljava/util/List;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

.field final synthetic val$detailedBuckets:Landroid/view/ViewGroup;

.field final synthetic val$expanderIcon:Landroid/widget/ImageView;

.field final synthetic val$headerRow:Landroid/view/View;

.field final synthetic val$shortDescription:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/AppPermissionAdapter;Landroid/view/View;Landroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$headerRow:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$expanderIcon:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$detailedBuckets:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$shortDescription:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 192
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$headerRow:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 193
    .local v0, "isExpanded":Z
    if-nez v0, :cond_0

    .line 194
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$expanderIcon:Landroid/widget/ImageView;

    const v3, 0x7f020101

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 195
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$expanderIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    # getter for: Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->access$000(Lcom/google/android/finsky/layout/AppPermissionAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c02e5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$detailedBuckets:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 198
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$shortDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 206
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$headerRow:Landroid/view/View;

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 207
    return-void

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$expanderIcon:Landroid/widget/ImageView;

    const v3, 0x7f0200fe

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 201
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$expanderIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    # getter for: Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->access$000(Lcom/google/android/finsky/layout/AppPermissionAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c02e4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$detailedBuckets:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 204
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;->val$shortDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 206
    goto :goto_1
.end method
