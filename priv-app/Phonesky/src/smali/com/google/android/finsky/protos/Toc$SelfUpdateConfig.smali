.class public final Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelfUpdateConfig"
.end annotation


# instance fields
.field public hasLatestClientVersionCode:Z

.field public latestClientVersionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 818
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 819
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->clear()Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    .line 820
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 823
    iput v0, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    .line 824
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode:Z

    .line 825
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->cachedSize:I

    .line 826
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 840
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 841
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    if-eqz v1, :cond_1

    .line 842
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 845
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 854
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 858
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 859
    :sswitch_0
    return-object p0

    .line 864
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    .line 865
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode:Z

    goto :goto_0

    .line 854
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 797
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 832
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    if-eqz v0, :cond_1

    .line 833
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 835
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 836
    return-void
.end method
