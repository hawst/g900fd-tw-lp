.class public final Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormCheckbox"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;


# instance fields
.field public checked:Z

.field public description:Ljava/lang/String;

.field public hasChecked:Z

.field public hasDescription:Z

.field public hasId:Z

.field public hasPostParam:Z

.field public hasRequired:Z

.field public id:Ljava/lang/String;

.field public postParam:Ljava/lang/String;

.field public required:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1165
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1166
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->clear()Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 1167
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .locals 2

    .prologue
    .line 1134
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v0, :cond_1

    .line 1135
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1137
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v0, :cond_0

    .line 1138
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    sput-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 1140
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1142
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    return-object v0

    .line 1140
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1170
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    .line 1171
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasDescription:Z

    .line 1172
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    .line 1173
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasChecked:Z

    .line 1174
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    .line 1175
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasRequired:Z

    .line 1176
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    .line 1177
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasId:Z

    .line 1178
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    .line 1179
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasPostParam:Z

    .line 1180
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->cachedSize:I

    .line 1181
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1207
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1208
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasDescription:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1209
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1212
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasChecked:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    if-eqz v1, :cond_3

    .line 1213
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1216
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasRequired:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    if-eqz v1, :cond_5

    .line 1217
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1220
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasId:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1221
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1224
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasPostParam:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1225
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1228
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1237
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1241
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1242
    :sswitch_0
    return-object p0

    .line 1247
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    .line 1248
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasDescription:Z

    goto :goto_0

    .line 1252
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    .line 1253
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasChecked:Z

    goto :goto_0

    .line 1257
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    .line 1258
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasRequired:Z

    goto :goto_0

    .line 1262
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    .line 1263
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasId:Z

    goto :goto_0

    .line 1267
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    .line 1268
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasPostParam:Z

    goto :goto_0

    .line 1237
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1128
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1187
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasDescription:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1188
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1190
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasChecked:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    if-eqz v0, :cond_3

    .line 1191
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1193
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasRequired:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    if-eqz v0, :cond_5

    .line 1194
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1196
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasId:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1197
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1199
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->hasPostParam:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1200
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1202
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1203
    return-void
.end method
