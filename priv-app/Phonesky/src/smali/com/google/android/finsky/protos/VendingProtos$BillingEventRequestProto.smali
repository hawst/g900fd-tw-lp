.class public final Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingEventRequestProto"
.end annotation


# instance fields
.field public billingParametersId:Ljava/lang/String;

.field public clientMessage:Ljava/lang/String;

.field public eventType:I

.field public hasBillingParametersId:Z

.field public hasClientMessage:Z

.field public hasEventType:Z

.field public hasResultSuccess:Z

.field public resultSuccess:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3127
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3128
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    .line 3129
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3132
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    .line 3133
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    .line 3134
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    .line 3135
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    .line 3136
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    .line 3137
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    .line 3138
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    .line 3139
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasClientMessage:Z

    .line 3140
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->cachedSize:I

    .line 3141
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3164
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3165
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    if-eqz v1, :cond_1

    .line 3166
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3169
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3170
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3173
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    if-eqz v1, :cond_5

    .line 3174
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3177
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasClientMessage:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3178
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3181
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 3189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3190
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3194
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3195
    :sswitch_0
    return-object p0

    .line 3200
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3201
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3204
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    .line 3205
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    goto :goto_0

    .line 3211
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    .line 3212
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    goto :goto_0

    .line 3216
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    .line 3217
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    goto :goto_0

    .line 3221
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    .line 3222
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasClientMessage:Z

    goto :goto_0

    .line 3190
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 3201
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3090
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3147
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasEventType:Z

    if-eqz v0, :cond_1

    .line 3148
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->eventType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3150
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasBillingParametersId:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3151
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->billingParametersId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3153
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasResultSuccess:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    if-eqz v0, :cond_5

    .line 3154
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->resultSuccess:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3156
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->hasClientMessage:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3157
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;->clientMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3159
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3160
    return-void
.end method
