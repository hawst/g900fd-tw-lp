.class public final Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AndroidAppDelivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/AndroidAppDelivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SplitDeliveryData"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;


# instance fields
.field public downloadSize:J

.field public downloadUrl:Ljava/lang/String;

.field public gzippedDownloadSize:J

.field public gzippedDownloadUrl:Ljava/lang/String;

.field public hasDownloadSize:Z

.field public hasDownloadUrl:Z

.field public hasGzippedDownloadSize:Z

.field public hasGzippedDownloadUrl:Z

.field public hasId:Z

.field public hasSignature:Z

.field public id:Ljava/lang/String;

.field public patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

.field public signature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1287
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1288
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    .line 1289
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    .locals 2

    .prologue
    .line 1249
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    if-nez v0, :cond_1

    .line 1250
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1252
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    if-nez v0, :cond_0

    .line 1253
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    sput-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    .line 1255
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1257
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    return-object v0

    .line 1255
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1292
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    .line 1293
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasId:Z

    .line 1294
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    .line 1295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadSize:Z

    .line 1296
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    .line 1297
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadSize:Z

    .line 1298
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    .line 1299
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasSignature:Z

    .line 1300
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    .line 1301
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadUrl:Z

    .line 1302
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    .line 1303
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadUrl:Z

    .line 1304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    .line 1305
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->cachedSize:I

    .line 1306
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1338
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1339
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1340
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1343
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadSize:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 1344
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1347
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadSize:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1348
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1351
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasSignature:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1352
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1355
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadUrl:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1356
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1359
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadUrl:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1360
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1363
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-eqz v1, :cond_c

    .line 1364
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1367
    :cond_c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1375
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1376
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1380
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1381
    :sswitch_0
    return-object p0

    .line 1386
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    .line 1387
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasId:Z

    goto :goto_0

    .line 1391
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    .line 1392
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadSize:Z

    goto :goto_0

    .line 1396
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    .line 1397
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadSize:Z

    goto :goto_0

    .line 1401
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    .line 1402
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasSignature:Z

    goto :goto_0

    .line 1406
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    .line 1407
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadUrl:Z

    goto :goto_0

    .line 1411
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    .line 1412
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadUrl:Z

    goto :goto_0

    .line 1416
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-nez v1, :cond_1

    .line 1417
    new-instance v1, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    .line 1419
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1376
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1243
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1312
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1313
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->id:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1315
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadSize:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 1316
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1318
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadSize:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 1319
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1321
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasSignature:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1322
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1324
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasDownloadUrl:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1325
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1327
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->hasGzippedDownloadUrl:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1328
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1330
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-eqz v0, :cond_c

    .line 1331
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1333
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1334
    return-void
.end method
