.class public Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
.super Ljava/lang/Object;
.source "SetupWizardUtils.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/SetupWizardUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SetupWizardParams"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCardHolderName:Ljava/lang/String;

.field private final mHasSetupCompleteScreen:Z

.field private final mIsLightTheme:Z

.field private final mOnInitialSetup:Z

.field private final mUseImmersiveMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 336
    new-instance v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams$1;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    const-string v0, "cardholder_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    .line 303
    const-string v0, "on_initial_setup"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mOnInitialSetup:Z

    .line 306
    const-string v0, "on_initial_setup"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mHasSetupCompleteScreen:Z

    .line 307
    const-string v0, "useImmersiveMode"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mUseImmersiveMode:Z

    .line 308
    invoke-static {p1}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->shouldUseLightTheme(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mIsLightTheme:Z

    .line 309
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mOnInitialSetup:Z

    .line 314
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mHasSetupCompleteScreen:Z

    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mUseImmersiveMode:Z

    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mIsLightTheme:Z

    .line 317
    return-void

    .line 312
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 313
    goto :goto_1

    :cond_2
    move v0, v2

    .line 314
    goto :goto_2

    :cond_3
    move v0, v2

    .line 315
    goto :goto_3

    :cond_4
    move v1, v2

    .line 316
    goto :goto_4
.end method

.method private static shouldUseLightTheme(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 399
    invoke-static {}, Lcom/google/android/finsky/utils/SetupWizardUtils;->shouldUseMaterialTheme()Z

    move-result v2

    if-nez v2, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v1

    .line 404
    :cond_1
    const-string v2, "theme"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "theme":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "material_light"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    return v0
.end method

.method public getCardholderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    return-object v0
.end method

.method public hasSetupCompleteScreen()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mHasSetupCompleteScreen:Z

    return v0
.end method

.method public isInitialSetup()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mOnInitialSetup:Z

    return v0
.end method

.method public isLightTheme()Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mIsLightTheme:Z

    return v0
.end method

.method public useImmersiveMode()Z
    .locals 1

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mUseImmersiveMode:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 322
    iget-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mCardHolderName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 325
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mOnInitialSetup:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 326
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mHasSetupCompleteScreen:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 327
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mUseImmersiveMode:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 328
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->mIsLightTheme:Z

    if-eqz v0, :cond_5

    :goto_4
    int-to-byte v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 329
    return-void

    :cond_1
    move v0, v2

    .line 321
    goto :goto_0

    :cond_2
    move v0, v1

    .line 325
    goto :goto_1

    :cond_3
    move v0, v1

    .line 326
    goto :goto_2

    :cond_4
    move v0, v1

    .line 327
    goto :goto_3

    :cond_5
    move v2, v1

    .line 328
    goto :goto_4
.end method
