.class public Lcom/google/android/finsky/layout/FlowLayout;
.super Landroid/view/ViewGroup;
.source "FlowLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/FlowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FlowLayout;->getWidth()I

    move-result v6

    .line 69
    .local v6, "width":I
    const/4 v8, 0x0

    .line 70
    .local v8, "y":I
    const/4 v7, 0x0

    .line 71
    .local v7, "x":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FlowLayout;->getChildCount()I

    move-result v0

    .line 73
    .local v0, "childCount":I
    const/4 v4, 0x0

    .line 75
    .local v4, "currLineHeight":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v0, :cond_1

    .line 76
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 77
    .local v3, "currChild":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 78
    .local v2, "childWidth":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 80
    .local v1, "childHeight":I
    add-int v9, v7, v2

    if-le v9, v6, :cond_0

    .line 82
    add-int/2addr v8, v4

    .line 83
    const/4 v4, 0x0

    .line 84
    const/4 v7, 0x0

    .line 87
    :cond_0
    add-int v9, v7, v2

    add-int v10, v8, v1

    invoke-virtual {v3, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 88
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 89
    add-int/2addr v7, v2

    .line 75
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "childHeight":I
    .end local v2    # "childWidth":I
    .end local v3    # "currChild":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 26
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    .line 27
    .local v13, "widthMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 28
    .local v14, "widthSize":I
    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    if-ne v13, v0, :cond_1

    const/4 v10, 0x1

    .line 30
    .local v10, "isFixedSize":Z
    :goto_0
    const/4 v8, 0x0

    .line 32
    .local v8, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/FlowLayout;->getChildCount()I

    move-result v1

    .line 33
    .local v1, "childCount":I
    const/high16 v16, -0x80000000

    move/from16 v0, v16

    invoke-static {v14, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 35
    .local v4, "childWidthSpec":I
    const/4 v15, 0x0

    .line 36
    .local v15, "x":I
    const/4 v7, 0x0

    .line 37
    .local v7, "currLineWidth":I
    const/4 v6, 0x0

    .line 38
    .local v6, "currLineHeight":I
    const/4 v11, 0x0

    .line 40
    .local v11, "maxLineWidth":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v1, :cond_2

    .line 41
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/finsky/layout/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 43
    .local v5, "currChild":Landroid/view/View;
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v4, v0}, Landroid/view/View;->measure(II)V

    .line 44
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 45
    .local v3, "childMeasuredWidth":I
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 47
    .local v2, "childMeasuredHeight":I
    add-int v16, v15, v3

    move/from16 v0, v16

    if-le v0, v14, :cond_0

    .line 49
    add-int/2addr v8, v6

    .line 50
    const/4 v6, 0x0

    .line 51
    const/4 v15, 0x0

    .line 52
    const/4 v7, 0x0

    .line 54
    :cond_0
    add-int/2addr v15, v3

    .line 55
    add-int/2addr v7, v3

    .line 56
    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 57
    invoke-static {v7, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 40
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 28
    .end local v1    # "childCount":I
    .end local v2    # "childMeasuredHeight":I
    .end local v3    # "childMeasuredWidth":I
    .end local v4    # "childWidthSpec":I
    .end local v5    # "currChild":Landroid/view/View;
    .end local v6    # "currLineHeight":I
    .end local v7    # "currLineWidth":I
    .end local v8    # "height":I
    .end local v9    # "i":I
    .end local v10    # "isFixedSize":Z
    .end local v11    # "maxLineWidth":I
    .end local v15    # "x":I
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 60
    .restart local v1    # "childCount":I
    .restart local v4    # "childWidthSpec":I
    .restart local v6    # "currLineHeight":I
    .restart local v7    # "currLineWidth":I
    .restart local v8    # "height":I
    .restart local v9    # "i":I
    .restart local v10    # "isFixedSize":Z
    .restart local v11    # "maxLineWidth":I
    .restart local v15    # "x":I
    :cond_2
    add-int/2addr v8, v6

    .line 62
    if-eqz v10, :cond_3

    move v12, v14

    .line 63
    .local v12, "width":I
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v8}, Lcom/google/android/finsky/layout/FlowLayout;->setMeasuredDimension(II)V

    .line 64
    return-void

    .end local v12    # "width":I
    :cond_3
    move v12, v11

    .line 62
    goto :goto_2
.end method
