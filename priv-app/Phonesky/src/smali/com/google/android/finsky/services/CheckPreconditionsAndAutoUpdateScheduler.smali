.class public Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;
.super Ljava/lang/Object;
.source "CheckPreconditionsAndAutoUpdateScheduler.java"


# direct methods
.method public static cancelCheck()V
    .locals 6

    .prologue
    .line 99
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 103
    .local v1, "context":Lcom/google/android/finsky/FinskyApp;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 104
    const-string v4, "jobscheduler"

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/job/JobScheduler;

    .line 106
    .local v2, "jobScheduler":Landroid/app/job/JobScheduler;
    const v4, 0x30fc68e6

    invoke-virtual {v2, v4}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 113
    .end local v2    # "jobScheduler":Landroid/app/job/JobScheduler;
    :cond_0
    const-string v4, "alarm"

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 114
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 117
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 118
    const-string v4, "Canceling auto-update wifi check."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method private static getPendingIntent()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const-class v3, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-static {v2, v4, v0, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 127
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    return-object v1
.end method

.method public static isJobSchedulerEnabled()Z
    .locals 4

    .prologue
    .line 134
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static scheduleCheck(I)V
    .locals 10
    .param p0, "scheduleReason"    # I

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 44
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->isJobSchedulerEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    invoke-static {v1, p0}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->scheduleJob(Landroid/content/Context;I)V

    .line 57
    :goto_0
    return-void

    .line 47
    :cond_0
    const-string v4, "alarm"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 48
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    .line 50
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v4, Lcom/google/android/finsky/config/G;->autoUpdateWifiCheckIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 51
    .local v3, "wifiCheckIntervalMs":Ljava/lang/Long;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 52
    const/4 v4, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 55
    :cond_1
    const-string v4, "Scheduling auto-update wifi check using AlarmManager."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static scheduleJob(Landroid/content/Context;I)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "scheduleReason"    # I

    .prologue
    const/4 v12, 0x1

    .line 61
    const-string v11, "jobscheduler"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/job/JobScheduler;

    .line 63
    .local v4, "jobScheduler":Landroid/app/job/JobScheduler;
    new-instance v5, Landroid/content/ComponentName;

    const-class v11, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;

    invoke-direct {v5, p0, v11}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    .local v5, "serviceComponent":Landroid/content/ComponentName;
    new-instance v0, Landroid/app/job/JobInfo$Builder;

    const v11, 0x30fc68e6

    invoke-direct {v0, v11, v5}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 70
    .local v0, "builder":Landroid/app/job/JobInfo$Builder;
    and-int/lit8 v11, p1, 0x2

    if-eqz v11, :cond_0

    .line 71
    sget-object v11, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerGearheadDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 72
    .local v8, "minDelayMs":J
    invoke-virtual {v0, v8, v9}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 73
    invoke-virtual {v0, v8, v9, v12}, Landroid/app/job/JobInfo$Builder;->setBackoffCriteria(JI)Landroid/app/job/JobInfo$Builder;

    .line 74
    sget-object v11, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerGearheadTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 75
    .local v2, "gearheadTimeout":J
    invoke-virtual {v0, v2, v3}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    .line 85
    .end local v2    # "gearheadTimeout":J
    .end local v8    # "minDelayMs":J
    :goto_0
    new-instance v1, Landroid/os/PersistableBundle;

    invoke-direct {v1}, Landroid/os/PersistableBundle;-><init>()V

    .line 86
    .local v1, "extras":Landroid/os/PersistableBundle;
    const-string v11, "Finksy.AutoUpdateRetryReason"

    invoke-virtual {v1, v11, p1}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 88
    invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    .line 89
    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v10

    .line 91
    .local v10, "uploadTask":Landroid/app/job/JobInfo;
    invoke-virtual {v4, v10}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 92
    const-string v11, "Scheduling auto-update check using JobScheduler."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    return-void

    .line 79
    .end local v1    # "extras":Landroid/os/PersistableBundle;
    .end local v10    # "uploadTask":Landroid/app/job/JobInfo;
    :cond_0
    sget-object v11, Lcom/google/android/finsky/config/G;->autoUpdateJobSchedulerTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 80
    .local v6, "maxDelayMs":J
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v11

    invoke-virtual {v11, v12}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    goto :goto_0
.end method
