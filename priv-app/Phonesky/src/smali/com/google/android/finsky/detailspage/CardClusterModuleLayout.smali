.class public Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;
.super Landroid/widget/LinearLayout;
.source "CardClusterModuleLayout.java"


# instance fields
.field private mBucketRow:Lcom/google/android/finsky/layout/BucketRow;

.field private mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field private final mMaxItemsPerRow:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 49
    .local v12, "res":Landroid/content/res/Resources;
    const v0, 0x7f0f0007

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v13

    .line 52
    .local v13, "useWideLayout":Z
    const v0, 0x7f090054

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 58
    .local v7, "mainBgColor":I
    if-eqz v13, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->getPaddingLeft()I

    move-result v9

    .line 60
    .local v9, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->getPaddingTop()I

    move-result v11

    .line 61
    .local v11, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->getPaddingRight()I

    move-result v10

    .line 62
    .local v10, "paddingRight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->getPaddingBottom()I

    move-result v8

    .line 67
    .local v8, "paddingBottom":I
    const v0, 0x7f0b0056

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 68
    .local v2, "extraInset":I
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1, v7}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    invoke-virtual {p0, v9, v11, v10, v8}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->setPadding(IIII)V

    .line 77
    .end local v2    # "extraInset":I
    .end local v8    # "paddingBottom":I
    .end local v9    # "paddingLeft":I
    .end local v10    # "paddingRight":I
    .end local v11    # "paddingTop":I
    :goto_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v12, v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v6

    .line 78
    .local v6, "gridColumnCount":I
    const v0, 0x7f0f000c

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    const v0, 0x7f0e000a

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mMaxItemsPerRow:I

    .line 89
    :goto_1
    return-void

    .line 73
    .end local v6    # "gridColumnCount":I
    :cond_0
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 82
    .restart local v6    # "gridColumnCount":I
    :cond_1
    if-eqz v13, :cond_2

    .line 85
    add-int/lit8 v0, v6, -0x1

    iput v0, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mMaxItemsPerRow:I

    goto :goto_1

    .line 87
    :cond_2
    iput v6, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mMaxItemsPerRow:I

    goto :goto_1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 6

    .prologue
    .line 93
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 95
    const v3, 0x7f0a013f

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v3, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 96
    const v3, 0x7f0a0116

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/BucketRow;

    iput-object v3, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mBucketRow:Lcom/google/android/finsky/layout/BucketRow;

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 100
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const/4 v1, 0x0

    .local v1, "colNum":I
    :goto_0
    iget v3, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mMaxItemsPerRow:I

    if-ge v1, v3, :cond_0

    .line 101
    const v3, 0x7f0401d9

    iget-object v4, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mBucketRow:Lcom/google/android/finsky/layout/BucketRow;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 103
    .local v0, "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    iget-object v3, p0, Lcom/google/android/finsky/detailspage/CardClusterModuleLayout;->mBucketRow:Lcom/google/android/finsky/layout/BucketRow;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    .end local v0    # "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_0
    return-void
.end method
