.class public Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "CheckoutPurchaseSidecar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowErrorListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitErrorListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;,
        Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    }
.end annotation


# instance fields
.field private mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

.field private mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

.field private mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

.field private mCommitRequest:Lcom/google/android/finsky/api/DfeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/api/DfeRequest",
            "<*>;"
        }
    .end annotation
.end field

.field private mCommitStartedMs:J

.field private mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field private mCvcEscrowHandle:Ljava/lang/String;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mExtraPurchaseData:Landroid/os/Bundle;

.field private mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field private mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

.field private mPrepareRequest:Lcom/google/android/finsky/api/DfeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/api/DfeRequest",
            "<*>;"
        }
    .end annotation
.end field

.field private mPrepareStartedMs:J

.field private mServerLogsCookie:[B

.field private mVolleyError:Lcom/android/volley/VolleyError;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    .line 235
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setRetainInstance(Z)V

    .line 236
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getPrepareServerLatencyMs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getTimeElapsedSincePrepareMs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;IIJJ)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # J
    .param p5, "x4"    # J

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->log(IIJJ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1402(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCommitServerLatencyMs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getTimeElapsedSinceCommitMs()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$2202(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/Runnable;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->handleLibraryUpdates([Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCvcEscrowHandle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$3102(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;ILcom/android/volley/VolleyError;JJ)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/android/volley/VolleyError;
    .param p3, "x3"    # J
    .param p5, "x4"    # J

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->logVolleyError(ILcom/android/volley/VolleyError;JJ)V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;)Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p1, "x1"    # [B

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    return-object p1
.end method

.method private static bundleToMap(Landroid/os/Bundle;)Ljava/util/Map;
    .locals 4
    .param p0, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 705
    if-nez p0, :cond_1

    .line 706
    const/4 v2, 0x0

    .line 712
    :cond_0
    return-object v2

    .line 708
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 709
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 710
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static extractExtraPurchaseData(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/google/android/finsky/protos/Common$Docid;)Landroid/os/Bundle;
    .locals 9
    .param p0, "libraryUpdate"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p1, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;

    .prologue
    const/4 v1, 0x0

    .line 662
    if-eqz p0, :cond_0

    iget-object v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-nez v7, :cond_1

    .line 697
    :cond_0
    :goto_0
    return-object v1

    .line 667
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 668
    .local v5, "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v7, v7, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/16 v8, 0xb

    if-ne v7, v8, :cond_2

    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v7, v7, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iget-object v8, p1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    if-eqz v7, :cond_2

    .line 672
    iget-object v3, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    .line 673
    .local v3, "inAppDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    iget-boolean v7, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignature:Z

    if-eqz v7, :cond_3

    iget-boolean v7, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->hasSignedPurchaseData:Z

    if-eqz v7, :cond_3

    .line 674
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 675
    .local v1, "extraParams":Landroid/os/Bundle;
    const-string v7, "inapp_signed_purchase_data"

    iget-object v8, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v7, "inapp_purchase_data_signature"

    iget-object v8, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 681
    .end local v1    # "extraParams":Landroid/os/Bundle;
    .end local v3    # "inAppDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    :cond_2
    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v7, v7, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/16 v8, 0xf

    if-ne v7, v8, :cond_3

    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v7, v7, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iget-object v8, p1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    if-eqz v7, :cond_3

    .line 685
    iget-object v6, v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    .line 687
    .local v6, "subsDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    iget-boolean v7, v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignature:Z

    if-eqz v7, :cond_3

    iget-boolean v7, v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasSignedPurchaseData:Z

    if-eqz v7, :cond_3

    .line 688
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 689
    .restart local v1    # "extraParams":Landroid/os/Bundle;
    const-string v7, "inapp_signed_purchase_data"

    iget-object v8, v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const-string v7, "inapp_purchase_data_signature"

    iget-object v8, v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 667
    .end local v1    # "extraParams":Landroid/os/Bundle;
    .end local v6    # "subsDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getCommitServerLatencyMs()J
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitRequest:Lcom/google/android/finsky/api/DfeRequest;

    if-nez v0, :cond_0

    .line 503
    const-string v0, "Unexpected null commit request."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 504
    const-wide/16 v0, -0x1

    .line 506
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitRequest:Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getServerLatencyMs()J

    move-result-wide v0

    goto :goto_0
.end method

.method private getPrepareServerLatencyMs()J
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareRequest:Lcom/google/android/finsky/api/DfeRequest;

    if-nez v0, :cond_0

    .line 487
    const-string v0, "Unexpected null prepare request."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 488
    const-wide/16 v0, -0x1

    .line 490
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareRequest:Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getServerLatencyMs()J

    move-result-wide v0

    goto :goto_0
.end method

.method private getTimeElapsedSinceCommitMs()J
    .locals 4

    .prologue
    .line 494
    iget-wide v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitStartedMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 495
    const-string v0, "Commit not started."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 496
    const-wide/16 v0, -0x1

    .line 498
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitStartedMs:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private getTimeElapsedSincePrepareMs()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 478
    iget-wide v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareStartedMs:J

    cmp-long v2, v2, v0

    if-gtz v2, :cond_0

    .line 479
    const-string v2, "Prepare not started."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 482
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareStartedMs:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private handleLibraryUpdates([Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "libraryUpdates"    # [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p2, "debugEventName"    # Ljava/lang/String;
    .param p3, "postUpdateCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 455
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v4

    .line 456
    .local v4, "replicators":Lcom/google/android/finsky/library/LibraryReplicators;
    move-object v0, p1

    .local v0, "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 457
    .local v3, "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v5}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-interface {v4, v5, v3, p2}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    .line 456
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 460
    .end local v3    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_0
    array-length v5, p1

    if-lez v5, :cond_2

    .line 461
    invoke-interface {v4, p3}, Lcom/google/android/finsky/library/LibraryReplicators;->flushLibraryUpdates(Ljava/lang/Runnable;)V

    .line 467
    :goto_1
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 468
    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 469
    .restart local v3    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v5, v5, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v3, v5}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->extractExtraPurchaseData(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/google/android/finsky/protos/Common$Docid;)Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 471
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    if-eqz v5, :cond_3

    .line 475
    .end local v3    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_1
    return-void

    .line 463
    :cond_2
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 468
    .restart local v3    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private log(IIJJ)V
    .locals 13
    .param p1, "tag"    # I
    .param p2, "errorCode"    # I
    .param p3, "serverLatencyMs"    # J
    .param p5, "clientLatencyMs"    # J

    .prologue
    .line 397
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    move v2, p1

    move v6, p2

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 400
    return-void
.end method

.method private logVolleyError(ILcom/android/volley/VolleyError;JJ)V
    .locals 13
    .param p1, "tag"    # I
    .param p2, "volleyError"    # Lcom/android/volley/VolleyError;
    .param p3, "serverLatencyMs"    # J
    .param p5, "clientLatencyMs"    # J

    .prologue
    .line 404
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    move v2, p1

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 407
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 227
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 228
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;-><init>()V

    .line 230
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 231
    return-object v1
.end method


# virtual methods
.method public commit(Landroid/os/Bundle;Ljava/util/Map;)V
    .locals 9
    .param p1, "commitChallengeResponses"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-wide/16 v4, -0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 326
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    .line 340
    :goto_0
    return-void

    .line 330
    :cond_0
    const/16 v2, 0x130

    move-object v1, p0

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->log(IIJJ)V

    .line 331
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->bundleToMap(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v2

    .line 332
    .local v2, "postParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_1

    .line 333
    invoke-interface {v2, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 335
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/billing/BillingUtils;->getRiskHeader()Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "riskHeader":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitStartedMs:J

    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    invoke-direct {v4, p0, v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    new-instance v5, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitErrorListener;

    invoke-direct {v5, p0, v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitErrorListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->commitPurchase(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/finsky/api/DfeRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitRequest:Lcom/google/android/finsky/api/DfeRequest;

    .line 339
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    goto :goto_0
.end method

.method public escrowCvcCode(Ljava/lang/String;)V
    .locals 5
    .param p1, "cvc"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 350
    const-string v1, "escrowCvcCode() called while RUNNING."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/creditcard/EscrowRequest;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;

    invoke-direct {v1, p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowErrorListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowErrorListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    invoke-direct {v0, v3, p1, v1, v2}, Lcom/google/android/finsky/billing/creditcard/EscrowRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 354
    .local v0, "escrowRequest":Lcom/google/android/finsky/billing/creditcard/EscrowRequest;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 355
    const/4 v1, 0x6

    invoke-virtual {p0, v4, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    .line 356
    return-void
.end method

.method public getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    return-object v0
.end method

.method public getChangeSubscription()Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    return-object v0
.end method

.method public getCheckoutPurchaseError()Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    return-object v0
.end method

.method public getCompleteChallenge()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    return-object v0
.end method

.method public getCvcEscrowHandle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCvcEscrowHandle:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraPurchaseData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    return-object v0
.end method

.method public getPrepareChallenge()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    return-object v0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    return-object v0
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public installApp()V
    .locals 8

    .prologue
    .line 383
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appVersionCode:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v3}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "single_install"

    const/4 v7, 0x2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 394
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 242
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 244
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 245
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 246
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 251
    const-string v0, "CheckoutPurchaseSidecar.cart"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 252
    const-string v0, "CheckoutPurchaseSidecar.changeSubscription"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 254
    const-string v0, "CheckoutPurchaseSidecar.prepareChallenge"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 255
    const-string v0, "CheckoutPurchaseSidecar.completeChallenge"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 257
    const-string v0, "CheckoutPurchaseSidecar.extraPurchaseData"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 258
    const-string v0, "CheckoutPurchaseSidecar.checkoutPurchaseError"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 259
    const-string v0, "CheckoutPurchaseSidecar.serverLogsCookie"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 263
    return-void
.end method

.method public prepare(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;Ljava/lang/String;Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;Ljava/util/Map;)V
    .locals 14
    .param p1, "purchaseParams"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .param p2, "instrumentId"    # Ljava/lang/String;
    .param p3, "voucherParams"    # Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;
    .param p4, "prepareChallengeResponses"    # Landroid/os/Bundle;
    .param p5, "gaiaAuthParams"    # Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;",
            "Landroid/os/Bundle;",
            "Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 293
    .local p6, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    .line 295
    const/16 v4, 0x12e

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->log(IIJJ)V

    .line 296
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v13, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    .line 297
    .local v13, "iabParams":Lcom/google/android/finsky/billing/IabParameters;
    invoke-static/range {p4 .. p4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->bundleToMap(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v10

    .line 298
    .local v10, "extraParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p6, :cond_0

    .line 299
    move-object/from16 v0, p6

    invoke-interface {v10, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 301
    :cond_0
    if-nez v13, :cond_1

    const/4 v5, 0x0

    .line 305
    .local v5, "dfeIabParams":Lcom/google/android/finsky/api/DfeApi$IabParameters;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareStartedMs:J

    .line 306
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v9, v6, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appVersionCode:I

    new-instance v11, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;

    const/4 v6, 0x0

    invoke-direct {v11, p0, v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    new-instance v12, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;

    const/4 v6, 0x0

    invoke-direct {v12, p0, v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareErrorListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V

    move-object/from16 v6, p5

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    invoke-interface/range {v2 .. v12}, Lcom/google/android/finsky/api/DfeApi;->preparePurchase(Ljava/lang/String;ILcom/google/android/finsky/api/DfeApi$IabParameters;Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;Ljava/lang/String;Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;ILjava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/finsky/api/DfeRequest;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareRequest:Lcom/google/android/finsky/api/DfeRequest;

    .line 312
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitStartedMs:J

    .line 313
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCommitRequest:Lcom/google/android/finsky/api/DfeRequest;

    .line 315
    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    .line 316
    return-void

    .line 301
    .end local v5    # "dfeIabParams":Lcom/google/android/finsky/api/DfeApi$IabParameters;
    :cond_1
    new-instance v2, Lcom/google/android/finsky/api/DfeApi$IabParameters;

    iget v3, v13, Lcom/google/android/finsky/billing/IabParameters;->billingApiVersion:I

    iget-object v4, v13, Lcom/google/android/finsky/billing/IabParameters;->packageName:Ljava/lang/String;

    iget-object v5, v13, Lcom/google/android/finsky/billing/IabParameters;->packageSignatureHash:Ljava/lang/String;

    iget v6, v13, Lcom/google/android/finsky/billing/IabParameters;->packageVersionCode:I

    iget-object v7, v13, Lcom/google/android/finsky/billing/IabParameters;->developerPayload:Ljava/lang/String;

    iget-object v8, v13, Lcom/google/android/finsky/billing/IabParameters;->oldSkusAsDocidStrings:Ljava/util/List;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/finsky/api/DfeApi$IabParameters;-><init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    move-object v5, v2

    goto :goto_0
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 267
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 268
    const-string v0, "CheckoutPurchaseSidecar.cart"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 269
    const-string v0, "CheckoutPurchaseSidecar.changeSubscription"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 271
    const-string v0, "CheckoutPurchaseSidecar.prepareChallenge"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 273
    const-string v0, "CheckoutPurchaseSidecar.completeChallenge"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 275
    const-string v0, "CheckoutPurchaseSidecar.extraPurchaseData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 276
    const-string v0, "CheckoutPurchaseSidecar.checkoutPurchaseError"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .line 277
    const-string v0, "CheckoutPurchaseSidecar.serverLogsCookie"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B

    .line 278
    return-void
.end method

.method public switchFromChangeSubscriptionToCart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 362
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 363
    const-string v0, "switchFromChangeSubscriptionToCart() called in state %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    .line 366
    return-void
.end method

.method public switchToInstrumentManager()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 373
    const-string v0, "switchToInstrumentManager() called in state %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    :cond_0
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V

    .line 376
    return-void
.end method
