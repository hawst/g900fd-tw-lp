.class public final Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AgeChallenge"
.end annotation


# instance fields
.field public birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

.field public carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

.field public citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

.field public descriptionHtml:Ljava/lang/String;

.field public fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

.field public genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

.field public hasDescriptionHtml:Z

.field public hasSubmitFooterHtml:Z

.field public hasTitle:Z

.field public phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

.field public submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

.field public submitFooterHtml:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1619
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1620
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .line 1621
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1624
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    .line 1625
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasTitle:Z

    .line 1626
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    .line 1627
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasDescriptionHtml:Z

    .line 1628
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1629
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1630
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1631
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    .line 1632
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    .line 1633
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 1634
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    .line 1635
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasSubmitFooterHtml:Z

    .line 1636
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1637
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->cachedSize:I

    .line 1638
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1679
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1680
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1681
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1684
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1685
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1688
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v1, :cond_4

    .line 1689
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1692
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v1, :cond_5

    .line 1693
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1696
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v1, :cond_6

    .line 1697
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1700
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v1, :cond_7

    .line 1701
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1704
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v1, :cond_8

    .line 1705
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1708
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v1, :cond_9

    .line 1709
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1712
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasSubmitFooterHtml:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1713
    :cond_a
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1716
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v1, :cond_c

    .line 1717
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1720
    :cond_c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1728
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1729
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1733
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1734
    :sswitch_0
    return-object p0

    .line 1739
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    .line 1740
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasTitle:Z

    goto :goto_0

    .line 1744
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    .line 1745
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasDescriptionHtml:Z

    goto :goto_0

    .line 1749
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-nez v1, :cond_1

    .line 1750
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1752
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1756
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-nez v1, :cond_2

    .line 1757
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1759
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1763
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-nez v1, :cond_3

    .line 1764
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1766
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1770
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-nez v1, :cond_4

    .line 1771
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    .line 1773
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1777
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-nez v1, :cond_5

    .line 1778
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    .line 1780
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1784
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v1, :cond_6

    .line 1785
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 1787
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1791
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    .line 1792
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasSubmitFooterHtml:Z

    goto/16 :goto_0

    .line 1796
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-nez v1, :cond_7

    .line 1797
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1799
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1729
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1569
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1644
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1645
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1647
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1648
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1650
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v0, :cond_4

    .line 1651
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1653
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v0, :cond_5

    .line 1654
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1656
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v0, :cond_6

    .line 1657
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1659
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v0, :cond_7

    .line 1660
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1662
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v0, :cond_8

    .line 1663
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1665
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v0, :cond_9

    .line 1666
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1668
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->hasSubmitFooterHtml:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1669
    :cond_a
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1671
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v0, :cond_c

    .line 1672
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1674
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1675
    return-void
.end method
