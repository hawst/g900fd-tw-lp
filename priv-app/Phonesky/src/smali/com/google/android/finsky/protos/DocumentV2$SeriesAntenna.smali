.class public final Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SeriesAntenna"
.end annotation


# instance fields
.field public colorThemeArgb:Ljava/lang/String;

.field public episodeSubtitle:Ljava/lang/String;

.field public episodeTitle:Ljava/lang/String;

.field public hasColorThemeArgb:Z

.field public hasEpisodeSubtitle:Z

.field public hasEpisodeTitle:Z

.field public hasSeriesSubtitle:Z

.field public hasSeriesTitle:Z

.field public sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public seriesSubtitle:Ljava/lang/String;

.field public seriesTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3476
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3477
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->clear()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3478
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3481
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    .line 3482
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    .line 3483
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    .line 3484
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    .line 3485
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    .line 3486
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    .line 3487
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    .line 3488
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    .line 3489
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    .line 3490
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    .line 3491
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3492
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3493
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->cachedSize:I

    .line 3494
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3526
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3527
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3528
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3531
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3532
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3535
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3536
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3539
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3540
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3543
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3544
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3547
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_a

    .line 3548
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3551
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_b

    .line 3552
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3555
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3563
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3564
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3568
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3569
    :sswitch_0
    return-object p0

    .line 3574
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    .line 3575
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    goto :goto_0

    .line 3579
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    .line 3580
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    goto :goto_0

    .line 3584
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    .line 3585
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    goto :goto_0

    .line 3589
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    .line 3590
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    goto :goto_0

    .line 3594
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    .line 3595
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    goto :goto_0

    .line 3599
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v1, :cond_1

    .line 3600
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3602
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3606
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v1, :cond_2

    .line 3607
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3609
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3564
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3433
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3500
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3501
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3503
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3504
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3506
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3507
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3509
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3510
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3512
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 3513
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3515
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v0, :cond_a

    .line 3516
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3518
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v0, :cond_b

    .line 3519
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3521
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3522
    return-void
.end method
