.class Lcom/google/android/finsky/receivers/InstallerTask$2;
.super Ljava/lang/Object;
.source "InstallerTask.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->requestDeliveryData(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 8
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1025
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I
    invoke-static {v0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->access$800(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/android/volley/VolleyError;)I

    move-result v4

    .line 1026
    .local v4, "volleyErrorCode":I
    instance-of v0, p1, Lcom/google/android/volley/DisplayMessageError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/volley/DisplayMessageError;

    .end local p1    # "volleyError":Lcom/android/volley/VolleyError;
    invoke-virtual {p1}, Lcom/google/android/volley/DisplayMessageError;->getDisplayErrorHtml()Ljava/lang/String;

    move-result-object v7

    .line 1028
    .local v7, "serverMessage":Ljava/lang/String;
    :goto_0
    const-string v0, "Received VolleyError %d (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x1

    aput-object v7, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1029
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1030
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x68

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->val$packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1033
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    invoke-static {v0, v1, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->access$600(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1034
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$2;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V
    invoke-static {v0, v4, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->access$700(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    .line 1035
    return-void

    .end local v7    # "serverMessage":Ljava/lang/String;
    .restart local p1    # "volleyError":Lcom/android/volley/VolleyError;
    :cond_0
    move-object v7, v3

    .line 1026
    goto :goto_0
.end method
