.class public Lcom/google/android/finsky/adapters/SearchAdapter;
.super Lcom/google/android/finsky/adapters/CardListAdapter;
.source "SearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/SearchAdapter$StringBasedSpinnerAdapter;
    }
.end annotation


# instance fields
.field private final mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

.field private final mHasSuggestedQuery:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeSearch;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p7, "dfeSearch"    # Lcom/google/android/finsky/api/model/DfeSearch;
    .param p8, "isRestoring"    # Z
    .param p9, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 55
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x2

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v11, p8

    move-object/from16 v14, p9

    invoke-direct/range {v1 .. v14}, Lcom/google/android/finsky/adapters/CardListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 58
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    .line 59
    iget-object v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mHasSuggestedQuery:Z

    .line 60
    return-void

    .line 59
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/SearchAdapter;)Lcom/google/android/finsky/api/model/DfeSearch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/SearchAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    return-object v0
.end method

.method private bindSuggestionHeader(Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;)V
    .locals 7
    .param p1, "headerView"    # Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;

    .prologue
    const/4 v6, 0x1

    .line 182
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 183
    .local v1, "originalQuery":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v4

    .line 184
    .local v4, "suggestionQuery":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeSearch;->isFullPageReplaced()Z

    move-result v0

    .line 185
    .local v0, "fullPageReplaced":Z
    invoke-virtual {p1, v1, v4, v0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->bind(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 187
    if-eqz v0, :cond_0

    .line 188
    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/SearchAdapter;->makeReplacedClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 190
    .local v2, "replacedClickHandler":Landroid/view/View$OnClickListener;
    invoke-virtual {p1, v2}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    invoke-virtual {p1, v6}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setClickable(Z)V

    .line 192
    invoke-virtual {p1, v6}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setFocusable(Z)V

    .line 201
    .end local v2    # "replacedClickHandler":Landroid/view/View$OnClickListener;
    :goto_0
    const-string v5, "suggestion_header"

    invoke-virtual {p1, v5}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setIdentifier(Ljava/lang/String;)V

    .line 202
    return-void

    .line 194
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/finsky/adapters/SearchAdapter;->makeSuggestionClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 196
    .local v3, "suggestionClickHandler":Landroid/view/View$OnClickListener;
    invoke-virtual {p1, v3}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    invoke-virtual {p1, v6}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setClickable(Z)V

    .line 198
    invoke-virtual {p1, v6}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->setFocusable(Z)V

    goto :goto_0
.end method

.method private getSuggestionHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 229
    if-nez p1, :cond_0

    const v1, 0x7f040197

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/SearchAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .local v0, "headerView":Landroid/view/View;
    :goto_0
    move-object v1, v0

    .line 231
    check-cast v1, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/SearchAdapter;->bindSuggestionHeader(Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;)V

    .line 232
    return-object v0

    .end local v0    # "headerView":Landroid/view/View;
    :cond_0
    move-object v0, p1

    .line 229
    goto :goto_0
.end method

.method private makeReplacedClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "originalString"    # Ljava/lang/String;

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/finsky/adapters/SearchAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/adapters/SearchAdapter$3;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;Ljava/lang/String;)V

    return-object v0
.end method

.method private makeSuggestionClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "suggestionString"    # Ljava/lang/String;

    .prologue
    .line 205
    new-instance v0, Lcom/google/android/finsky/adapters/SearchAdapter$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/adapters/SearchAdapter$2;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V
    .locals 10
    .param p1, "identifiable"    # Lcom/google/android/finsky/layout/play/Identifiable;
    .param p2, "spinner"    # Landroid/widget/Spinner;
    .param p3, "corpusHeaderStrip"    # Landroid/view/View;

    .prologue
    .line 117
    const-string v8, "corpus_selector"

    invoke-interface {p1, v8}, Lcom/google/android/finsky/layout/play/Identifiable;->setIdentifier(Ljava/lang/String;)V

    .line 119
    iget-object v8, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/DfeSearch;->getRelatedSearches()[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    move-result-object v3

    .line 120
    .local v3, "related":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    array-length v8, v3

    new-array v7, v8, [Ljava/lang/String;

    .line 121
    .local v7, "spinnerTextArray":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 122
    .local v5, "selectedIndex":I
    const/4 v4, 0x3

    .line 123
    .local v4, "selectedBackend":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, v7

    if-ge v2, v8, :cond_1

    .line 124
    aget-object v0, v3, v2

    .line 125
    .local v0, "entry":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    iget-object v8, v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    aput-object v8, v7, v2

    .line 126
    if-nez v5, :cond_0

    iget-boolean v8, v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    if-eqz v8, :cond_0

    .line 127
    move v5, v2

    .line 128
    iget v4, v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    .line 123
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 133
    .end local v0    # "entry":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    :cond_1
    invoke-static {v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusSpinnerDrawable(I)I

    move-result v8

    invoke-virtual {p2, v8}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 135
    iget-object v8, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v8

    invoke-virtual {p3, v8}, Landroid/view/View;->setBackgroundColor(I)V

    .line 139
    invoke-virtual {p2}, Landroid/widget/Spinner;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b012c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iget v9, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mCardContentPadding:I

    add-int v1, v8, v9

    .line 141
    .local v1, "horizontalMargin":I
    invoke-virtual {p2}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 143
    .local v6, "spinnerLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 144
    iput v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 145
    invoke-virtual {p2, v6}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    new-instance v8, Lcom/google/android/finsky/adapters/SearchAdapter$StringBasedSpinnerAdapter;

    iget-object v9, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9, v7}, Lcom/google/android/finsky/adapters/SearchAdapter$StringBasedSpinnerAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    invoke-virtual {p2, v8}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 148
    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 150
    new-instance v8, Lcom/google/android/finsky/adapters/SearchAdapter$1;

    invoke-direct {v8, p0, v3, p2}, Lcom/google/android/finsky/adapters/SearchAdapter$1;-><init>(Lcom/google/android/finsky/adapters/SearchAdapter;[Lcom/google/android/finsky/protos/Search$RelatedSearch;Landroid/widget/Spinner;)V

    invoke-virtual {p2, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 168
    return-void
.end method

.method protected getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 3
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 66
    invoke-static {p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Ljava/lang/String;I)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataRowsCount()I
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getDataRowsCount()I

    move-result v1

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mHasSuggestedQuery:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 90
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mHasSuggestedQuery:Z

    if-eqz v0, :cond_1

    .line 91
    if-ne p1, v1, :cond_0

    .line 92
    const/16 v0, 0x18

    .line 100
    :goto_0
    return v0

    .line 94
    :cond_0
    if-le p1, v1, :cond_1

    .line 97
    add-int/lit8 p1, p1, -0x1

    .line 100
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getPrependedRowsCount()I
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPrependedRowsCount()I

    move-result v1

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mHasSuggestedQuery:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v1, 0x1

    .line 75
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mHasSuggestedQuery:Z

    if-eqz v0, :cond_1

    .line 76
    if-ne p1, v1, :cond_0

    .line 77
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/SearchAdapter;->getSuggestionHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 79
    :cond_0
    if-le p1, v1, :cond_1

    .line 82
    add-int/lit8 p1, p1, -0x1

    .line 85
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected hasFilters()Z
    .locals 2

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/finsky/adapters/SearchAdapter;->mDfeSearch:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getRelatedSearches()[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    move-result-object v0

    .line 111
    .local v0, "related":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
