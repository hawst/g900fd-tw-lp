.class public Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
.super Ljava/lang/Object;
.source "PurchaseParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final appContinueUrl:Ljava/lang/String;

.field public final appTitle:Ljava/lang/String;

.field public final appVersionCode:I

.field public final docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public final docidStr:Ljava/lang/String;

.field public final iabParameters:Lcom/google/android/finsky/billing/IabParameters;

.field public final offerType:I

.field public final voucherId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromParcel(Landroid/os/Parcel;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Docid;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appVersionCode:I

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    .line 83
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    .line 88
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    .line 93
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    .line 94
    const-class v0, Lcom/google/android/finsky/billing/IabParameters;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/IabParameters;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    .line 98
    :goto_3
    return-void

    .line 81
    :cond_0
    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    goto :goto_0

    .line 86
    :cond_1
    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    goto :goto_1

    .line 91
    :cond_2
    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    goto :goto_2

    .line 96
    :cond_3
    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    goto :goto_3
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docid:Lcom/google/android/finsky/protos/Common$Docid;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "docid cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docidStr:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$100(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "docidStr cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_1
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->offerType:I
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$200(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    .line 65
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appVersionCode:I
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$300(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appVersionCode:I

    .line 66
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$400(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    .line 67
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appContinueUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$500(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    .line 68
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->voucherId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$600(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    .line 69
    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;
    invoke-static {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->access$700(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Lcom/google/android/finsky/billing/IabParameters;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)V

    return-void
.end method

.method public static builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appVersionCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->appContinueUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    if-nez v0, :cond_7

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 127
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 111
    goto :goto_0

    :cond_5
    move v0, v2

    .line 115
    goto :goto_1

    :cond_6
    move v0, v2

    .line 119
    goto :goto_2

    :cond_7
    move v1, v2

    .line 123
    goto :goto_3
.end method
