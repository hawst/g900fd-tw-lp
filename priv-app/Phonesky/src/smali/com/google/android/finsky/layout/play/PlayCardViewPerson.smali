.class public Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewPerson.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;


# instance fields
.field private mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

.field private mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

.field private mCirclesStatus:Landroid/widget/TextView;

.field private mCirclesStatusIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method private configure(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "circles":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    const/4 v1, 0x0

    .line 132
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 134
    .local v0, "isInCircles":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 135
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/android/finsky/utils/GPlusUtils;->getCirclesString(Ljava/util/List;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v2}, Lcom/google/android/finsky/model/CirclesModel;->getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->configure(Ljava/lang/String;Z)V

    .line 143
    return-void

    .end local v0    # "isInCircles":Z
    :cond_0
    move v0, v1

    .line 132
    goto :goto_0

    .line 138
    .restart local v0    # "isInCircles":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v2}, Lcom/google/android/finsky/model/CirclesModel;->getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public getCardType()I
    .locals 1

    .prologue
    .line 271
    const/16 v0, 0xc

    return v0
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->NO_CARD_BG_IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public onCirclesUpdate(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "circles":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->configure(Ljava/util/List;)V

    .line 115
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v4/app/FragmentActivity;

    if-nez v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    if-ne p1, v1, :cond_0

    .line 124
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x118

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/finsky/utils/PlayCardUtils;->getCardParentNode(Lcom/google/android/play/layout/PlayCardViewBase;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 126
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 127
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/model/CirclesModel;->launchCirclePicker(Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/model/CirclesModel;->setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V

    .line 280
    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    .line 282
    :cond_0
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onDetachedFromWindow()V

    .line 283
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 61
    const v0, 0x7f0a02b0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v0, 0x7f0a02cf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0a02d0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    .line 65
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 37
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingLeft()I

    move-result v21

    .line 197
    .local v21, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingRight()I

    move-result v22

    .line 198
    .local v22, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingTop()I

    move-result v23

    .line 199
    .local v23, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingBottom()I

    move-result v20

    .line 201
    .local v20, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getWidth()I

    move-result v31

    .line 202
    .local v31, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getHeight()I

    move-result v14

    .line 204
    .local v14, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 206
    .local v26, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v25

    .line 207
    .local v25, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v33, v0

    add-int v33, v33, v21

    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v34, v0

    add-int v34, v34, v23

    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v35, v0

    add-int v35, v35, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v36

    add-int v35, v35, v36

    move-object/from16 v0, v26

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v36, v0

    add-int v36, v36, v23

    add-int v36, v36, v25

    invoke-virtual/range {v32 .. v36}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v29

    check-cast v29, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 214
    .local v29, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 217
    .local v9, "circleStatusLp":Landroid/view/ViewGroup$MarginLayoutParams;
    add-int v32, v23, v25

    move-object/from16 v0, v29

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v30, v32, v33

    .line 218
    .local v30, "titleTop":I
    move-object/from16 v0, v29

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v32, v0

    add-int v28, v21, v32

    .line 219
    .local v28, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v27

    .line 220
    .local v27, "titleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v33

    add-int v33, v33, v28

    add-int v34, v30, v27

    move-object/from16 v0, v32

    move/from16 v1, v28

    move/from16 v2, v30

    move/from16 v3, v33

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getVisibility()I

    move-result v32

    const/16 v33, 0x8

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_1

    .line 224
    add-int v32, v30, v27

    move-object/from16 v0, v29

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v33, v0

    add-int v32, v32, v33

    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v33, v0

    add-int v10, v32, v33

    .line 227
    .local v10, "circleStatusTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getVisibility()I

    move-result v32

    if-nez v32, :cond_2

    const/4 v15, 0x1

    .line 228
    .local v15, "isShowingStatusIcon":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v11

    .line 229
    .local v11, "circleStatusWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v7

    .line 230
    .local v7, "circleStatusIconWidth":I
    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v32, v0

    add-int v32, v32, v11

    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v33, v0

    add-int v33, v33, v32

    if-eqz v15, :cond_3

    move/from16 v32, v7

    :goto_1
    add-int v5, v33, v32

    .line 235
    .local v5, "circleStatusBlockWidth":I
    sub-int v32, v31, v21

    sub-int v32, v32, v22

    sub-int v32, v32, v5

    div-int/lit8 v32, v32, 0x2

    add-int v8, v21, v32

    .line 237
    .local v8, "circleStatusLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getVisibility()I

    move-result v32

    if-nez v32, :cond_0

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v32

    div-int/lit8 v32, v32, 0x2

    add-int v32, v32, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v33

    div-int/lit8 v33, v33, 0x2

    sub-int v6, v32, v33

    .line 240
    .local v6, "circleStatusIconTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    add-int v33, v8, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v34

    add-int v34, v34, v6

    move-object/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v8, v6, v1, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v32

    add-int v8, v8, v32

    .line 246
    .end local v6    # "circleStatusIconTop":I
    :cond_0
    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v32, v0

    add-int v8, v8, v32

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v32, v0

    add-int v33, v8, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v34

    add-int v34, v34, v10

    move-object/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v8, v10, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 253
    .end local v5    # "circleStatusBlockWidth":I
    .end local v7    # "circleStatusIconWidth":I
    .end local v8    # "circleStatusLeft":I
    .end local v10    # "circleStatusTop":I
    .end local v11    # "circleStatusWidth":I
    .end local v15    # "isShowingStatusIcon":Z
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardThumbnail;->getCoverPadding()I

    move-result v24

    .line 254
    .local v24, "thumbnailCoverPadding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardThumbnail;->getBottom()I

    move-result v32

    sub-int v12, v32, v24

    .line 255
    .local v12, "gplusCircleStatusIconBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/play/layout/PlayCardThumbnail;->getRight()I

    move-result v32

    sub-int v13, v32, v24

    .line 256
    .local v13, "gplusCircleStatusIconRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getMeasuredWidth()I

    move-result v33

    sub-int v33, v13, v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getMeasuredHeight()I

    move-result v34

    sub-int v34, v12, v34

    move-object/from16 v0, v32

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2, v13, v12}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->layout(IIII)V

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getMeasuredWidth()I

    move-result v19

    .line 261
    .local v19, "loadingWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    .line 262
    .local v16, "loadingHeight":I
    sub-int v32, v31, v21

    sub-int v32, v32, v22

    sub-int v32, v32, v19

    div-int/lit8 v32, v32, 0x2

    add-int v17, v21, v32

    .line 263
    .local v17, "loadingLeft":I
    sub-int v32, v14, v23

    sub-int v32, v32, v20

    sub-int v32, v32, v16

    div-int/lit8 v32, v32, 0x2

    add-int v18, v23, v32

    .line 264
    .local v18, "loadingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getMeasuredWidth()I

    move-result v33

    add-int v33, v33, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getMeasuredHeight()I

    move-result v34

    add-int v34, v34, v18

    move-object/from16 v0, v32

    move/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v33

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 267
    return-void

    .line 227
    .end local v12    # "gplusCircleStatusIconBottom":I
    .end local v13    # "gplusCircleStatusIconRight":I
    .end local v16    # "loadingHeight":I
    .end local v17    # "loadingLeft":I
    .end local v18    # "loadingTop":I
    .end local v19    # "loadingWidth":I
    .end local v24    # "thumbnailCoverPadding":I
    .restart local v10    # "circleStatusTop":I
    :cond_2
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 230
    .restart local v7    # "circleStatusIconWidth":I
    .restart local v11    # "circleStatusWidth":I
    .restart local v15    # "isShowingStatusIcon":Z
    :cond_3
    const/16 v32, 0x0

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 18
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 147
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 148
    .local v14, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingLeft()I

    move-result v6

    .line 149
    .local v6, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingRight()I

    move-result v7

    .line 150
    .local v7, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingTop()I

    move-result v8

    .line 151
    .local v8, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getPaddingBottom()I

    move-result v5

    .line 152
    .local v5, "paddingBottom":I
    sub-int v15, v14, v6

    sub-int v4, v15, v7

    .line 154
    .local v4, "contentWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v15}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 157
    .local v9, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v15, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v15, v4, v15

    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v16, v0

    sub-int v10, v15, v16

    .line 158
    .local v10, "thumbnailSize":I
    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 159
    .local v11, "thumbnailSpec":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v15, v11, v11}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 161
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 163
    .local v12, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 167
    .local v2, "circleStatusLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v15, v12, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v15, v4, v15

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v16, v0

    sub-int v13, v15, v16

    .line 168
    .local v13, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v13, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->measure(II)V

    .line 171
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/widget/ImageView;->measure(II)V

    .line 172
    iget v15, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v15, v4, v15

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v16, v0

    sub-int v1, v15, v16

    .line 174
    .local v1, "circleStatusAvailableWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    invoke-virtual {v15}, Landroid/widget/ImageView;->getVisibility()I

    move-result v15

    if-nez v15, :cond_0

    .line 175
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatusIcon:Landroid/widget/ImageView;

    invoke-virtual {v15}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v15

    sub-int/2addr v1, v15

    .line 177
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    const/high16 v16, -0x80000000

    move/from16 v0, v16

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/widget/TextView;->measure(II)V

    .line 181
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->measure(II)V

    .line 183
    iget v15, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v16

    add-int v15, v15, v16

    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mTitle:Landroid/widget/TextView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    add-int v15, v15, v16

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesStatus:Landroid/widget/TextView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    add-int v15, v15, v16

    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v16, v0

    add-int v3, v15, v16

    .line 189
    .local v3, "contentHeight":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mLoadingIndicator:Landroid/view/View;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/view/View;->measure(II)V

    .line 191
    add-int v15, v8, v3

    add-int/2addr v15, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->setMeasuredDimension(II)V

    .line 192
    return-void
.end method

.method public setData(Ljava/lang/Object;I)V
    .locals 8
    .param p1, "data"    # Ljava/lang/Object;
    .param p2, "backendId"    # I

    .prologue
    const/4 v7, 0x0

    .line 74
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->setData(Ljava/lang/Object;I)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    .line 78
    .local v4, "plusDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    if-eqz v5, :cond_1

    .line 79
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5}, Lcom/google/android/finsky/model/CirclesModel;->getOwnerAccountName()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v6

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5}, Lcom/google/android/finsky/model/CirclesModel;->getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v6

    if-eq v5, v6, :cond_1

    .line 82
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/model/CirclesModel;->setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V

    .line 83
    iput-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    .line 87
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    if-nez v5, :cond_2

    .line 89
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v1

    .line 91
    .local v1, "cache":Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-virtual {v1, v4, v0}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedCirclesModel(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/model/CirclesModel;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    .line 93
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5, p0}, Lcom/google/android/finsky/model/CirclesModel;->setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V

    .line 95
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 96
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v5}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getPeopleClient()Lcom/google/android/gms/people/PeopleClient;

    move-result-object v3

    .line 97
    .local v3, "peopleClient":Lcom/google/android/gms/people/PeopleClient;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5, v2, v3}, Lcom/google/android/finsky/model/CirclesModel;->loadCircles(Landroid/content/Context;Lcom/google/android/gms/people/PeopleClient;)V

    .line 105
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "cache":Lcom/google/android/finsky/utils/ClientMutationCache;
    .end local v2    # "context":Landroid/content/Context;
    .end local v3    # "peopleClient":Lcom/google/android/gms/people/PeopleClient;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v5}, Lcom/google/android/finsky/model/CirclesModel;->getCircles()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->configure(Ljava/util/List;)V

    .line 106
    return-void
.end method

.method public showCirclesIcon(Z)V
    .locals 2
    .param p1, "showCirclesIcon"    # Z

    .prologue
    .line 109
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->mCirclesIcon:Lcom/google/android/finsky/layout/play/PlayCirclesIcon;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setVisibility(I)V

    .line 110
    return-void

    .line 109
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
