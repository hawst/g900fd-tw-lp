.class public final Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseNotificationResponse"
.end annotation


# instance fields
.field public debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

.field public hasLocalizedErrorMessage:Z

.field public hasPurchaseId:Z

.field public hasStatus:Z

.field public localizedErrorMessage:Ljava/lang/String;

.field public purchaseId:Ljava/lang/String;

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->clear()Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    .line 47
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    iput v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    .line 51
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasStatus:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    .line 53
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasPurchaseId:Z

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasLocalizedErrorMessage:Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->cachedSize:I

    .line 58
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 82
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasStatus:Z

    if-eqz v1, :cond_1

    .line 83
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    if-eqz v1, :cond_2

    .line 87
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasLocalizedErrorMessage:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 91
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasPurchaseId:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 95
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 106
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 107
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 111
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 112
    :sswitch_0
    return-object p0

    .line 117
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 118
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 122
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    .line 123
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasStatus:Z

    goto :goto_0

    .line 129
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    if-nez v2, :cond_1

    .line 130
    new-instance v2, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    .line 132
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 136
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    .line 137
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasLocalizedErrorMessage:Z

    goto :goto_0

    .line 141
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    .line 142
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasPurchaseId:Z

    goto :goto_0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasStatus:Z

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->debugInfo:Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 70
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasLocalizedErrorMessage:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 73
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->hasPurchaseId:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 74
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->purchaseId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 76
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 77
    return-void
.end method
