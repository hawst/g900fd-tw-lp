.class public final Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormRadioButton"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;


# instance fields
.field public hasLabel:Z

.field public hasSelected:Z

.field public hasValue:Z

.field public label:Ljava/lang/String;

.field public selected:Z

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2858
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2859
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->clear()Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 2860
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .locals 2

    .prologue
    .line 2835
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    if-nez v0, :cond_1

    .line 2836
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 2838
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    if-nez v0, :cond_0

    .line 2839
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    sput-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 2841
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2843
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    return-object v0

    .line 2841
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2863
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    .line 2864
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasLabel:Z

    .line 2865
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    .line 2866
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasValue:Z

    .line 2867
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    .line 2868
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasSelected:Z

    .line 2869
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->cachedSize:I

    .line 2870
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2890
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2891
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasLabel:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2892
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2895
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasValue:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2896
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2899
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasSelected:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    if-eqz v1, :cond_5

    .line 2900
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2903
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2911
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2912
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2916
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2917
    :sswitch_0
    return-object p0

    .line 2922
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    .line 2923
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasLabel:Z

    goto :goto_0

    .line 2927
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    .line 2928
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasValue:Z

    goto :goto_0

    .line 2932
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    .line 2933
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasSelected:Z

    goto :goto_0

    .line 2912
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2829
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2876
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasLabel:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2877
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2879
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasValue:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2880
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2882
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->hasSelected:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    if-eqz v0, :cond_5

    .line 2883
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2885
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2886
    return-void
.end method
