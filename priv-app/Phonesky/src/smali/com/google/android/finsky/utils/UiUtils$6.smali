.class final Lcom/google/android/finsky/utils/UiUtils$6;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->enableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;ILcom/google/android/finsky/layout/actionbar/ActionBarController;)Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field final synthetic val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

.field final synthetic val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    iput-object p2, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    iput-object p3, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(II)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    iget-object v1, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    iget-object v2, p0, Lcom/google/android/finsky/utils/UiUtils$6;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    # invokes: Lcom/google/android/finsky/utils/UiUtils;->syncActionBarAlpha(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
    invoke-static {v0, v1, v2, p2}, Lcom/google/android/finsky/utils/UiUtils;->access$000(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V

    .line 473
    return-void
.end method
