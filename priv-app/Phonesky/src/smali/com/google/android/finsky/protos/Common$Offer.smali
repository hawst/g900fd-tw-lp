.class public final Lcom/google/android/finsky/protos/Common$Offer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Offer"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;


# instance fields
.field public checkoutFlowRequired:Z

.field public convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

.field public currencyCode:Ljava/lang/String;

.field public formattedAmount:Ljava/lang/String;

.field public formattedDescription:Ljava/lang/String;

.field public formattedFullAmount:Ljava/lang/String;

.field public formattedName:Ljava/lang/String;

.field public fullPriceMicros:J

.field public hasCheckoutFlowRequired:Z

.field public hasCurrencyCode:Z

.field public hasFormattedAmount:Z

.field public hasFormattedDescription:Z

.field public hasFormattedFullAmount:Z

.field public hasFormattedName:Z

.field public hasFullPriceMicros:Z

.field public hasLicensedOfferType:Z

.field public hasMicros:Z

.field public hasOfferId:Z

.field public hasOfferType:Z

.field public hasOnSaleDate:Z

.field public hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

.field public hasPreorder:Z

.field public hasPreorderFulfillmentDisplayDate:Z

.field public licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

.field public licensedOfferType:I

.field public micros:J

.field public offerId:Ljava/lang/String;

.field public offerType:I

.field public onSaleDate:J

.field public onSaleDateDisplayTimeZoneOffsetMsec:I

.field public preorder:Z

.field public preorderFulfillmentDisplayDate:J

.field public promotionLabel:[Ljava/lang/String;

.field public rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

.field public subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

.field public subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 507
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 508
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Offer;->clear()Lcom/google/android/finsky/protos/Common$Offer;

    .line 509
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;
    .locals 2

    .prologue
    .line 418
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v0, :cond_1

    .line 419
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 421
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v0, :cond_0

    .line 422
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Offer;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 424
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    return-object v0

    .line 424
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Offer;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 512
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 513
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    .line 514
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    .line 515
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    .line 516
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 517
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    .line 518
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    .line 519
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    .line 520
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    .line 521
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    .line 522
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 523
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    .line 524
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 525
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    .line 526
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Offer;->emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 527
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 528
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    .line 529
    iput v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 530
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    .line 531
    iput v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    .line 532
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    .line 533
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    .line 534
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 535
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 536
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 537
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    .line 538
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    .line 539
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    .line 540
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    .line 541
    iput-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 542
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    .line 543
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    .line 544
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    .line 545
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    .line 546
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    .line 547
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    .line 548
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->cachedSize:I

    .line 549
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    .line 633
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 634
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    if-nez v5, :cond_0

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 635
    :cond_0
    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    invoke-static {v10, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 638
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 639
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 642
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 643
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 646
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 647
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 648
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v2, v5, v3

    .line 649
    .local v2, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_6

    .line 650
    const/4 v5, 0x4

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 647
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 655
    .end local v2    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v3    # "i":I
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    if-nez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v5, :cond_9

    .line 656
    :cond_8
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 659
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-nez v5, :cond_a

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_b

    .line 660
    :cond_a
    const/4 v5, 0x6

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 663
    :cond_b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 664
    :cond_c
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 667
    :cond_d
    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-ne v5, v10, :cond_e

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    if-eqz v5, :cond_f

    .line 668
    :cond_e
    const/16 v5, 0x8

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 671
    :cond_f
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v5, :cond_10

    .line 672
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 675
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    if-nez v5, :cond_11

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_12

    .line 676
    :cond_11
    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 679
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 680
    const/4 v0, 0x0

    .line 681
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 682
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 683
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 684
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 685
    add-int/lit8 v0, v0, 0x1

    .line 686
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 682
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 690
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 691
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 693
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-eqz v5, :cond_16

    .line 694
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 697
    :cond_16
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    if-nez v5, :cond_17

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 698
    :cond_17
    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 701
    :cond_18
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    if-nez v5, :cond_19

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 702
    :cond_19
    const/16 v5, 0xe

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 705
    :cond_1a
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    if-nez v5, :cond_1b

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    if-eqz v5, :cond_1c

    .line 706
    :cond_1b
    const/16 v5, 0xf

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 709
    :cond_1c
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    if-nez v5, :cond_1d

    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    if-eqz v5, :cond_1e

    .line 710
    :cond_1d
    const/16 v5, 0x10

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 713
    :cond_1e
    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    if-ne v5, v10, :cond_1f

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    if-eqz v5, :cond_20

    .line 714
    :cond_1f
    const/16 v5, 0x11

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 717
    :cond_20
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-eqz v5, :cond_21

    .line 718
    const/16 v5, 0x12

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 721
    :cond_21
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    if-nez v5, :cond_22

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_23

    .line 722
    :cond_22
    const/16 v5, 0x13

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 725
    :cond_23
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    if-nez v5, :cond_24

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_25

    .line 726
    :cond_24
    const/16 v5, 0x14

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 729
    :cond_25
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    if-eqz v5, :cond_26

    .line 730
    const/16 v5, 0x15

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 733
    :cond_26
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 741
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 742
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 746
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 747
    :sswitch_0
    return-object p0

    .line 752
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 753
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    goto :goto_0

    .line 757
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    .line 758
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    goto :goto_0

    .line 762
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 763
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    goto :goto_0

    .line 767
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 769
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v6, :cond_2

    move v1, v5

    .line 770
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 772
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v1, :cond_1

    .line 773
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 775
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 776
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 777
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 778
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 775
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 769
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v1, v6

    goto :goto_1

    .line 781
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 782
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 783
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    goto :goto_0

    .line 787
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 788
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    goto :goto_0

    .line 792
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 793
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    goto :goto_0

    .line 797
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 798
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    goto/16 :goto_0

    .line 802
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 803
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 816
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 817
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    goto/16 :goto_0

    .line 823
    .end local v4    # "value":I
    :sswitch_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-nez v6, :cond_4

    .line 824
    new-instance v6, Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$RentalTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 826
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 830
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 831
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    goto/16 :goto_0

    .line 835
    :sswitch_b
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 837
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-nez v6, :cond_6

    move v1, v5

    .line 838
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 839
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 840
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 842
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 843
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 844
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 842
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 837
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_3

    .line 847
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 848
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    goto/16 :goto_0

    .line 852
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-nez v6, :cond_8

    .line 853
    new-instance v6, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 855
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 859
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    .line 860
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    goto/16 :goto_0

    .line 864
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    .line 865
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    goto/16 :goto_0

    .line 869
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    .line 870
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    goto/16 :goto_0

    .line 874
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    .line 875
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    goto/16 :goto_0

    .line 879
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 880
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto/16 :goto_0

    .line 893
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    .line 894
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    goto/16 :goto_0

    .line 900
    .end local v4    # "value":I
    :sswitch_12
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-nez v6, :cond_9

    .line 901
    new-instance v6, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 903
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 907
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    .line 908
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    goto/16 :goto_0

    .line 912
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    .line 913
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    goto/16 :goto_0

    .line 917
    :sswitch_15
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    if-nez v6, :cond_a

    .line 918
    new-instance v6, Lcom/google/android/finsky/protos/Common$LicenseTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$LicenseTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    .line 920
    :cond_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 742
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
    .end sparse-switch

    .line 803
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 880
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 412
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Offer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 9
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 555
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 556
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    invoke-virtual {p1, v8, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 558
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 559
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 561
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 562
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 564
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 565
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 566
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v2, v1

    .line 567
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_6

    .line 568
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 565
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 572
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v2, :cond_9

    .line 573
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 575
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-nez v2, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_b

    .line 576
    :cond_a
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 578
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 579
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 581
    :cond_d
    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-ne v2, v8, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    if-eqz v2, :cond_f

    .line 582
    :cond_e
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 584
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v2, :cond_10

    .line 585
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 587
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    if-nez v2, :cond_11

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_12

    .line 588
    :cond_11
    const/16 v2, 0xa

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 590
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 591
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 592
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 593
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_13

    .line 594
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 591
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 598
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-eqz v2, :cond_15

    .line 599
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 601
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 602
    :cond_16
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 604
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    if-nez v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 605
    :cond_18
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 607
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    if-nez v2, :cond_1a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    if-eqz v2, :cond_1b

    .line 608
    :cond_1a
    const/16 v2, 0xf

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 610
    :cond_1b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    if-nez v2, :cond_1c

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    if-eqz v2, :cond_1d

    .line 611
    :cond_1c
    const/16 v2, 0x10

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 613
    :cond_1d
    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    if-ne v2, v8, :cond_1e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    if-eqz v2, :cond_1f

    .line 614
    :cond_1e
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 616
    :cond_1f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-eqz v2, :cond_20

    .line 617
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 619
    :cond_20
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    if-nez v2, :cond_21

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 620
    :cond_21
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 622
    :cond_22
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    if-nez v2, :cond_23

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_24

    .line 623
    :cond_23
    const/16 v2, 0x14

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 625
    :cond_24
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    if-eqz v2, :cond_25

    .line 626
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 628
    :cond_25
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 629
    return-void
.end method
