.class public abstract Lcom/google/android/finsky/billing/InstrumentFlowFragment;
.super Lcom/google/android/finsky/billing/BillingFlowFragment;
.source "InstrumentFlowFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32
    .local v0, "result":Landroid/os/Bundle;
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    if-eqz v1, :cond_0

    .line 33
    const-string v1, "instrument_id"

    iget-object v2, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :cond_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v1, :cond_1

    .line 37
    const-string v1, "redeemed_offer_message_html"

    iget-object v2, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    iget-object v2, v2, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->finish(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method
