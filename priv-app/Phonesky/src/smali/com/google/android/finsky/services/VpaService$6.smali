.class Lcom/google/android/finsky/services/VpaService$6;
.super Ljava/lang/Object;
.source "VpaService.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/VpaService;->vpaGetPreloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/VpaService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/VpaService;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/finsky/services/VpaService$6;->this$0:Lcom/google/android/finsky/services/VpaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x0

    .line 266
    const-string v0, "Failed to retrieve preloads: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService$6;->this$0:Lcom/google/android/finsky/services/VpaService;

    # setter for: Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/finsky/services/VpaService;->access$302(Lcom/google/android/finsky/services/VpaService;Z)Z

    .line 268
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService$6;->this$0:Lcom/google/android/finsky/services/VpaService;

    # invokes: Lcom/google/android/finsky/services/VpaService;->stopServiceIfDone()V
    invoke-static {v0}, Lcom/google/android/finsky/services/VpaService;->access$200(Lcom/google/android/finsky/services/VpaService;)V

    .line 269
    return-void
.end method
