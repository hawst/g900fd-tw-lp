.class public Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
.super Ljava/lang/Object;
.source "InstallPolicies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/installer/InstallPolicies;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InstallWarnings"
.end annotation


# instance fields
.field public autoUpdateDisabled:Z

.field public largeDownload:Z

.field public newPermissions:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->largeDownload:Z

    .line 93
    iput-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->newPermissions:Z

    .line 99
    iput-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->autoUpdateDisabled:Z

    return-void
.end method


# virtual methods
.method public warningRequired()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->autoUpdateDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->largeDownload:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->newPermissions:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
