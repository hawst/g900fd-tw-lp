.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "AgeChallengeFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;
    }
.end annotation


# instance fields
.field private mBackend:I

.field private mBirthday:Landroid/widget/EditText;

.field private mBirthdayDate:Ljava/util/Date;

.field private mCarrierSelection:Landroid/widget/RadioGroup;

.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

.field private final mCheckBoxWatcher:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCitizenship:Landroid/widget/CheckBox;

.field private mFullName:Landroid/widget/EditText;

.field private mGenderSelection:Landroid/widget/RadioGroup;

.field private mMainView:Landroid/view/ViewGroup;

.field private mPhoneNumber:Landroid/widget/EditText;

.field private final mRadioGroupWatcher:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

.field private final mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    .line 78
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 93
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mRadioGroupWatcher:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 101
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCheckBoxWatcher:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->syncSubmitButtonState()V

    return-void
.end method

.method private getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    .line 373
    :goto_0
    return-object v0

    .line 369
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    if-eqz v0, :cond_1

    .line 370
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    goto :goto_0

    .line 372
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    if-eqz v0, :cond_2

    .line 373
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    goto :goto_0

    .line 375
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener registered."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private hasValidInput()Z
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 308
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return v0

    .line 312
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    if-eqz v1, :cond_0

    .line 315
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 319
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 328
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    if-nez v1, :cond_0

    .line 332
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initInputFields(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f040020

    const/4 v10, 0x0

    const/16 v9, 0x8

    .line 200
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00bc

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    .line 201
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v7, :cond_5

    .line 202
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 203
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 205
    :cond_0
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 206
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 208
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 213
    :goto_0
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00bd

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    .line 214
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v7, :cond_7

    .line 215
    if-eqz p1, :cond_6

    .line 216
    const-string v7, "AgeChallengeFragment.birthday_date"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/Date;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    .line 221
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    if-eqz v7, :cond_3

    .line 222
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    invoke-static {v8}, Lcom/google/android/finsky/utils/DateUtils;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :cond_3
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 225
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 228
    :cond_4
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 229
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    invoke-virtual {v7, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    :goto_2
    const/4 v3, 0x1

    .line 236
    .local v3, "nextRadioButtonId":I
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00be

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioGroup;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    .line 237
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v7, :cond_b

    .line 239
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 240
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v6, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 241
    .local v6, "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    array-length v7, v6

    if-ge v1, v7, :cond_8

    .line 242
    aget-object v0, v6, v1

    .line 243
    .local v0, "formRadioButton":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v11, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 245
    .local v5, "radioButton":Landroid/widget/RadioButton;
    iget-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 246
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nextRadioButtonId":I
    .local v4, "nextRadioButtonId":I
    invoke-virtual {v5, v3}, Landroid/widget/RadioButton;->setId(I)V

    .line 247
    iget-boolean v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 248
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v7, v5, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 241
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .end local v4    # "nextRadioButtonId":I
    .restart local v3    # "nextRadioButtonId":I
    goto :goto_3

    .line 210
    .end local v0    # "formRadioButton":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .end local v1    # "i":I
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v3    # "nextRadioButtonId":I
    .end local v5    # "radioButton":Landroid/widget/RadioButton;
    .end local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_5
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    invoke-virtual {v7, v9}, Landroid/widget/EditText;->setVisibility(I)V

    goto/16 :goto_0

    .line 217
    :cond_6
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 218
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    const-string v8, "yyyyMMdd"

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/DateUtils;->parseDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    goto/16 :goto_1

    .line 231
    :cond_7
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    invoke-virtual {v7, v9}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_2

    .line 250
    .restart local v1    # "i":I
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v3    # "nextRadioButtonId":I
    .restart local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_8
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mRadioGroupWatcher:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 255
    .end local v1    # "i":I
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :goto_4
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00bf

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    .line 256
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v7, :cond_c

    .line 257
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 258
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 260
    :cond_9
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 261
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 263
    :cond_a
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 268
    :goto_5
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00c0

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioGroup;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    .line 269
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    if-eqz v7, :cond_e

    .line 271
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 272
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v6, v7, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 273
    .restart local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    array-length v7, v6

    if-ge v1, v7, :cond_d

    .line 274
    aget-object v0, v6, v1

    .line 275
    .restart local v0    # "formRadioButton":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v11, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 277
    .restart local v5    # "radioButton":Landroid/widget/RadioButton;
    iget-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->label:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 278
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nextRadioButtonId":I
    .restart local v4    # "nextRadioButtonId":I
    invoke-virtual {v5, v3}, Landroid/widget/RadioButton;->setId(I)V

    .line 279
    iget-boolean v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->selected:Z

    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 280
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v7, v5, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;I)V

    .line 273
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    .end local v4    # "nextRadioButtonId":I
    .restart local v3    # "nextRadioButtonId":I
    goto :goto_6

    .line 252
    .end local v0    # "formRadioButton":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .end local v1    # "i":I
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v5    # "radioButton":Landroid/widget/RadioButton;
    .end local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_b
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v7, v9}, Landroid/widget/RadioGroup;->setVisibility(I)V

    goto/16 :goto_4

    .line 265
    :cond_c
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v7, v9}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_5

    .line 282
    .restart local v1    # "i":I
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_d
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mRadioGroupWatcher:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 287
    .end local v1    # "i":I
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v6    # "radioButtons":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :goto_7
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v8, 0x7f0a00c1

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    .line 288
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v7, v7, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v7, :cond_f

    .line 289
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    iget-boolean v8, v8, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 291
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCheckBoxWatcher:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 295
    :goto_8
    return-void

    .line 284
    :cond_e
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v7, v9}, Landroid/widget/RadioGroup;->setVisibility(I)V

    goto :goto_7

    .line 293
    :cond_f
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    invoke-virtual {v7, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_8
.end method

.method public static newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .prologue
    .line 121
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "AgeChallengeFragment.backend"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    const-string v2, "AgeChallengeFragment.challenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 125
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;-><init>()V

    .line 126
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 127
    return-object v1
.end method

.method private serializeInputResult()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 337
    .local v1, "inputResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 338
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->fullName:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mFullName:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 341
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->birthdate:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    const-string v4, "yyyyMMdd"

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/DateUtils;->formatDate(Ljava/util/Date;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 345
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mGenderSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 347
    .local v0, "index":I
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->genderSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    .end local v0    # "index":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 351
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->phoneNumber:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 354
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCarrierSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 356
    .restart local v0    # "index":I
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->carrierSelection:Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->value:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    .end local v0    # "index":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mCitizenship:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 360
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->citizenship:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    :cond_5
    return-object v1
.end method

.method private syncSubmitButtonState()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->hasValidInput()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setEnabled(Z)V

    .line 305
    return-void
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 415
    const/16 v0, 0x579

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 391
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    if-ne p1, v3, :cond_3

    .line 393
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "AgeChallengeFragment.date_picker"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 397
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    if-eqz v3, :cond_2

    .line 398
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 400
    :cond_2
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->newInstance(Ljava/util/Calendar;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

    move-result-object v1

    .line 402
    .local v1, "datePickerDialog":Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;
    invoke-virtual {v1, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;)V

    .line 403
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "AgeChallengeFragment.date_picker"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 404
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "datePickerDialog":Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    if-ne p1, v3, :cond_0

    .line 405
    const/16 v3, 0x57a

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->logClickEvent(I)V

    .line 406
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->serializeInputResult()Ljava/util/Map;

    move-result-object v2

    .line 407
    .local v2, "postParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;->onSubmit(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AgeChallengeFragment.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBackend:I

    .line 134
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AgeChallengeFragment.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .line 135
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 140
    const v4, 0x7f04001d

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    .line 143
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a009c

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 144
    .local v3, "title":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 145
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00b9

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 152
    .local v0, "accountName":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "authAccount"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00bb

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 155
    .local v1, "description":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 156
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 158
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 164
    :goto_1
    invoke-direct {p0, p3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->initInputFields(Landroid/os/Bundle;)V

    .line 166
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00c2

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 167
    .local v2, "footer":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 168
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitFooterHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    :goto_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a0231

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/IconButtonGroup;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    .line 174
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBackend:I

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/IconButtonGroup;->setBackendForLabel(I)V

    .line 175
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 176
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/IconButtonGroup;->setText(Ljava/lang/String;)V

    .line 180
    :goto_3
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mSubmitButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-virtual {v4, p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->syncSubmitButtonState()V

    .line 183
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    return-object v4

    .line 147
    .end local v0    # "accountName":Landroid/widget/TextView;
    .end local v1    # "description":Landroid/widget/TextView;
    .end local v2    # "footer":Landroid/widget/TextView;
    :cond_0
    const-string v4, "Title is not returned."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    .restart local v0    # "accountName":Landroid/widget/TextView;
    .restart local v1    # "description":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 170
    .restart local v2    # "footer":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 178
    :cond_3
    const-string v4, "Submit button is not returned"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/DatePicker;
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I

    .prologue
    .line 382
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p2, p3, p4}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    .line 383
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthday:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    invoke-static {v1}, Lcom/google/android/finsky/utils/DateUtils;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 384
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->syncSubmitButtonState()V

    .line 385
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 188
    invoke-super {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;->onResume()V

    .line 192
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 193
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 194
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 299
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 300
    const-string v0, "AgeChallengeFragment.birthday_date"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->mBirthdayDate:Ljava/util/Date;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 301
    return-void
.end method
