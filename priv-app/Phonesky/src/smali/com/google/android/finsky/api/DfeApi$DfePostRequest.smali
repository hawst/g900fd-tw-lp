.class public Lcom/google/android/finsky/api/DfeApi$DfePostRequest;
.super Lcom/google/android/finsky/api/DfeRequest;
.source "DfeApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/api/DfeApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DfePostRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/google/android/finsky/api/DfeRequest",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mPostParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "apiContext"    # Lcom/google/android/finsky/api/DfeApiContext;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/api/DfeApiContext;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1116
    .local p0, "this":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<TT;>;"
    .local p3, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    const/4 v1, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeRequest;-><init>(ILjava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->mPostParams:Ljava/util/Map;

    .line 1118
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 1119
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setAvoidBulkCancel()V

    .line 1120
    return-void
.end method


# virtual methods
.method public addPostParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1123
    .local p0, "this":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->mPostParams:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1124
    return-void
.end method

.method public getParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1128
    .local p0, "this":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->mPostParams:Ljava/util/Map;

    return-object v0
.end method
