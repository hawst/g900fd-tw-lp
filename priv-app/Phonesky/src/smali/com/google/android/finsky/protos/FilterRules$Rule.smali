.class public final Lcom/google/android/finsky/protos/FilterRules$Rule;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rule"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;


# instance fields
.field public availabilityProblemType:I

.field public comment:Ljava/lang/String;

.field public constArg:[I

.field public doubleArg:[D

.field public hasAvailabilityProblemType:Z

.field public hasComment:Z

.field public hasIncludeMissingValues:Z

.field public hasKey:Z

.field public hasNegate:Z

.field public hasOperator:Z

.field public hasResponseCode:Z

.field public includeMissingValues:Z

.field public key:I

.field public longArg:[J

.field public negate:Z

.field public operator:I

.field public responseCode:I

.field public stringArg:[Ljava/lang/String;

.field public stringArgHash:[J

.field public subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$Rule;->clear()Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 132
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v0, :cond_1

    .line 74
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$Rule;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 79
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 135
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    .line 136
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    .line 137
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    .line 138
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    .line 139
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    .line 140
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    .line 141
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    .line 142
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    .line 143
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    .line 144
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_DOUBLE_ARRAY:[D

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    .line 145
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    .line 146
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$Rule;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Rule;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 147
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    .line 148
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    .line 149
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    .line 150
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    .line 151
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    .line 152
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    .line 153
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    .line 154
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    .line 155
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->cachedSize:I

    .line 156
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 224
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v5

    .line 225
    .local v5, "size":I
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    if-eqz v6, :cond_1

    .line 226
    :cond_0
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    invoke-static {v8, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v5, v6

    .line 229
    :cond_1
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    if-ne v6, v8, :cond_2

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    if-eqz v6, :cond_3

    .line 230
    :cond_2
    const/4 v6, 0x2

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 233
    :cond_3
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    if-ne v6, v8, :cond_4

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    if-eqz v6, :cond_5

    .line 234
    :cond_4
    const/4 v6, 0x3

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 237
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_8

    .line 238
    const/4 v0, 0x0

    .line 239
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 240
    .local v1, "dataSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_7

    .line 241
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    aget-object v2, v6, v4

    .line 242
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 243
    add-int/lit8 v0, v0, 0x1

    .line 244
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v1, v6

    .line 240
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 248
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v5, v1

    .line 249
    mul-int/lit8 v6, v0, 0x1

    add-int/2addr v5, v6

    .line 251
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    if-lez v6, :cond_a

    .line 252
    const/4 v1, 0x0

    .line 253
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    if-ge v4, v6, :cond_9

    .line 254
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    aget-wide v2, v6, v4

    .line 255
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v1, v6

    .line 253
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 258
    .end local v2    # "element":J
    :cond_9
    add-int/2addr v5, v1

    .line 259
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 261
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    if-lez v6, :cond_b

    .line 262
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 263
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 264
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 266
    .end local v1    # "dataSize":I
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v6, v6

    if-lez v6, :cond_d

    .line 267
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v6, v6

    if-ge v4, v6, :cond_d

    .line 268
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    aget-object v2, v6, v4

    .line 269
    .local v2, "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v2, :cond_c

    .line 270
    const/4 v6, 0x7

    invoke-static {v6, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v5, v6

    .line 267
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 275
    .end local v2    # "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    .end local v4    # "i":I
    :cond_d
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    if-ne v6, v8, :cond_e

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    if-eqz v6, :cond_f

    .line 276
    :cond_e
    const/16 v6, 0x8

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 279
    :cond_f
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    if-nez v6, :cond_10

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_11

    .line 280
    :cond_10
    const/16 v6, 0x9

    iget-object v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v5, v6

    .line 283
    :cond_11
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    if-lez v6, :cond_12

    .line 284
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 285
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 286
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 288
    .end local v1    # "dataSize":I
    :cond_12
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    if-lez v6, :cond_14

    .line 289
    const/4 v1, 0x0

    .line 290
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    if-ge v4, v6, :cond_13

    .line 291
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    aget v2, v6, v4

    .line 292
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v1, v6

    .line 290
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 295
    .end local v2    # "element":I
    :cond_13
    add-int/2addr v5, v1

    .line 296
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 298
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_14
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    if-ne v6, v8, :cond_15

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    if-eqz v6, :cond_16

    .line 299
    :cond_15
    const/16 v6, 0xc

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 302
    :cond_16
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    if-nez v6, :cond_17

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    if-eqz v6, :cond_18

    .line 303
    :cond_17
    const/16 v6, 0xd

    iget-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v5, v6

    .line 306
    :cond_18
    return v5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 18
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v10

    .line 315
    .local v10, "tag":I
    sparse-switch v10, :sswitch_data_0

    .line 319
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v15

    if-nez v15, :cond_0

    .line 320
    :sswitch_0
    return-object p0

    .line 325
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    .line 326
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    goto :goto_0

    .line 330
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 331
    .local v14, "value":I
    packed-switch v14, :pswitch_data_0

    goto :goto_0

    .line 348
    :pswitch_0
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    .line 349
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    goto :goto_0

    .line 355
    .end local v14    # "value":I
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 356
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 390
    :pswitch_2
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    .line 391
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    goto :goto_0

    .line 397
    .end local v14    # "value":I
    :sswitch_4
    const/16 v15, 0x22

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 399
    .local v2, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-nez v15, :cond_2

    const/4 v4, 0x0

    .line 400
    .local v4, "i":I
    :goto_1
    add-int v15, v4, v2

    new-array v8, v15, [Ljava/lang/String;

    .line 401
    .local v8, "newArray":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 402
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    :cond_1
    :goto_2
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_3

    .line 405
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v8, v4

    .line 406
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 404
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 399
    .end local v4    # "i":I
    .end local v8    # "newArray":[Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v4, v15

    goto :goto_1

    .line 409
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v8, v4

    .line 410
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    goto/16 :goto_0

    .line 414
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[Ljava/lang/String;
    :sswitch_5
    const/16 v15, 0x28

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 416
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-nez v15, :cond_5

    const/4 v4, 0x0

    .line 417
    .restart local v4    # "i":I
    :goto_3
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 418
    .local v8, "newArray":[J
    if-eqz v4, :cond_4

    .line 419
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 421
    :cond_4
    :goto_4
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_6

    .line 422
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 423
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 421
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 416
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v4, v15

    goto :goto_3

    .line 426
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 427
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    goto/16 :goto_0

    .line 431
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 432
    .local v6, "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 434
    .local v7, "limit":I
    const/4 v2, 0x0

    .line 435
    .restart local v2    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v9

    .line 436
    .local v9, "startPos":I
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_7

    .line 437
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    .line 438
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 440
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 441
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-nez v15, :cond_9

    const/4 v4, 0x0

    .line 442
    .restart local v4    # "i":I
    :goto_6
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 443
    .restart local v8    # "newArray":[J
    if-eqz v4, :cond_8

    .line 444
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 446
    :cond_8
    :goto_7
    array-length v15, v8

    if-ge v4, v15, :cond_a

    .line 447
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 446
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 441
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v4, v15

    goto :goto_6

    .line 449
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_a
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    .line 450
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 454
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[J
    .end local v9    # "startPos":I
    :sswitch_7
    const/16 v15, 0x31

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 456
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-nez v15, :cond_c

    const/4 v4, 0x0

    .line 457
    .restart local v4    # "i":I
    :goto_8
    add-int v15, v4, v2

    new-array v8, v15, [D

    .line 458
    .local v8, "newArray":[D
    if-eqz v4, :cond_b

    .line 459
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 461
    :cond_b
    :goto_9
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_d

    .line 462
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 463
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 461
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 456
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v4, v15

    goto :goto_8

    .line 466
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[D
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 467
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    goto/16 :goto_0

    .line 471
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :sswitch_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 472
    .restart local v6    # "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 473
    .restart local v7    # "limit":I
    div-int/lit8 v2, v6, 0x8

    .line 474
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-nez v15, :cond_f

    const/4 v4, 0x0

    .line 475
    .restart local v4    # "i":I
    :goto_a
    add-int v15, v4, v2

    new-array v8, v15, [D

    .line 476
    .restart local v8    # "newArray":[D
    if-eqz v4, :cond_e

    .line 477
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 479
    :cond_e
    :goto_b
    array-length v15, v8

    if-ge v4, v15, :cond_10

    .line 480
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 479
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 474
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v4, v15

    goto :goto_a

    .line 482
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[D
    :cond_10
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    .line 483
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 487
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[D
    :sswitch_9
    const/16 v15, 0x3a

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 489
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v15, :cond_12

    const/4 v4, 0x0

    .line 490
    .restart local v4    # "i":I
    :goto_c
    add-int v15, v4, v2

    new-array v8, v15, [Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 492
    .local v8, "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v4, :cond_11

    .line 493
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 495
    :cond_11
    :goto_d
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_13

    .line 496
    new-instance v15, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v15}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    aput-object v15, v8, v4

    .line 497
    aget-object v15, v8, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 498
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 495
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 489
    .end local v4    # "i":I
    .end local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v4, v15

    goto :goto_c

    .line 501
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :cond_13
    new-instance v15, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v15}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    aput-object v15, v8, v4

    .line 502
    aget-object v15, v8, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 503
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    goto/16 :goto_0

    .line 507
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 508
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_2

    :pswitch_3
    goto/16 :goto_0

    .line 527
    :pswitch_4
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    .line 528
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    goto/16 :goto_0

    .line 534
    .end local v14    # "value":I
    :sswitch_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    .line 535
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    goto/16 :goto_0

    .line 539
    :sswitch_c
    const/16 v15, 0x51

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v2

    .line 541
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-nez v15, :cond_15

    const/4 v4, 0x0

    .line 542
    .restart local v4    # "i":I
    :goto_e
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 543
    .local v8, "newArray":[J
    if-eqz v4, :cond_14

    .line 544
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 546
    :cond_14
    :goto_f
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_16

    .line 547
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 548
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 546
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 541
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v4, v15

    goto :goto_e

    .line 551
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 552
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    goto/16 :goto_0

    .line 556
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :sswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 557
    .restart local v6    # "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 558
    .restart local v7    # "limit":I
    div-int/lit8 v2, v6, 0x8

    .line 559
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-nez v15, :cond_18

    const/4 v4, 0x0

    .line 560
    .restart local v4    # "i":I
    :goto_10
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 561
    .restart local v8    # "newArray":[J
    if-eqz v4, :cond_17

    .line 562
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 564
    :cond_17
    :goto_11
    array-length v15, v8

    if-ge v4, v15, :cond_19

    .line 565
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 564
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    .line 559
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_18
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v4, v15

    goto :goto_10

    .line 567
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_19
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    .line 568
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 572
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[J
    :sswitch_e
    const/16 v15, 0x58

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v6

    .line 574
    .restart local v6    # "length":I
    new-array v13, v6, [I

    .line 575
    .local v13, "validValues":[I
    const/4 v11, 0x0

    .line 576
    .local v11, "validCount":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    move v12, v11

    .end local v11    # "validCount":I
    .local v12, "validCount":I
    :goto_12
    if-ge v4, v6, :cond_1b

    .line 577
    if-eqz v4, :cond_1a

    .line 578
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 580
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 581
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_3

    move v11, v12

    .line 576
    .end local v12    # "validCount":I
    .restart local v11    # "validCount":I
    :goto_13
    add-int/lit8 v4, v4, 0x1

    move v12, v11

    .end local v11    # "validCount":I
    .restart local v12    # "validCount":I
    goto :goto_12

    .line 586
    :pswitch_5
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "validCount":I
    .restart local v11    # "validCount":I
    aput v14, v13, v12

    goto :goto_13

    .line 590
    .end local v11    # "validCount":I
    .end local v14    # "value":I
    .restart local v12    # "validCount":I
    :cond_1b
    if-eqz v12, :cond_0

    .line 591
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-nez v15, :cond_1c

    const/4 v4, 0x0

    .line 592
    :goto_14
    if-nez v4, :cond_1d

    array-length v15, v13

    if-ne v12, v15, :cond_1d

    .line 593
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    goto/16 :goto_0

    .line 591
    :cond_1c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v4, v15

    goto :goto_14

    .line 595
    :cond_1d
    add-int v15, v4, v12

    new-array v8, v15, [I

    .line 596
    .local v8, "newArray":[I
    if-eqz v4, :cond_1e

    .line 597
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 599
    :cond_1e
    const/4 v15, 0x0

    invoke-static {v13, v15, v8, v4, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 600
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    goto/16 :goto_0

    .line 606
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v8    # "newArray":[I
    .end local v12    # "validCount":I
    .end local v13    # "validValues":[I
    :sswitch_f
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v3

    .line 607
    .local v3, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 609
    .restart local v7    # "limit":I
    const/4 v2, 0x0

    .line 610
    .restart local v2    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v9

    .line 611
    .restart local v9    # "startPos":I
    :goto_15
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_1f

    .line 612
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v15

    packed-switch v15, :pswitch_data_4

    goto :goto_15

    .line 617
    :pswitch_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 621
    :cond_1f
    if-eqz v2, :cond_23

    .line 622
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 623
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-nez v15, :cond_21

    const/4 v4, 0x0

    .line 624
    .restart local v4    # "i":I
    :goto_16
    add-int v15, v4, v2

    new-array v8, v15, [I

    .line 625
    .restart local v8    # "newArray":[I
    if-eqz v4, :cond_20

    .line 626
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 628
    :cond_20
    :goto_17
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_22

    .line 629
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 630
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_5

    goto :goto_17

    .line 635
    :pswitch_7
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aput v14, v8, v4

    move v4, v5

    .end local v5    # "i":I
    .restart local v4    # "i":I
    goto :goto_17

    .line 623
    .end local v4    # "i":I
    .end local v8    # "newArray":[I
    .end local v14    # "value":I
    :cond_21
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v4, v15

    goto :goto_16

    .line 639
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[I
    :cond_22
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    .line 641
    .end local v4    # "i":I
    .end local v8    # "newArray":[I
    :cond_23
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 645
    .end local v2    # "arrayLength":I
    .end local v3    # "bytes":I
    .end local v7    # "limit":I
    .end local v9    # "startPos":I
    :sswitch_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 646
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_6

    goto/16 :goto_0

    .line 655
    :pswitch_8
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    .line 656
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    goto/16 :goto_0

    .line 662
    .end local v14    # "value":I
    :sswitch_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    .line 663
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    goto/16 :goto_0

    .line 315
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
        0x31 -> :sswitch_7
        0x32 -> :sswitch_8
        0x3a -> :sswitch_9
        0x40 -> :sswitch_a
        0x4a -> :sswitch_b
        0x51 -> :sswitch_c
        0x52 -> :sswitch_d
        0x58 -> :sswitch_e
        0x5a -> :sswitch_f
        0x60 -> :sswitch_10
        0x68 -> :sswitch_11
    .end sparse-switch

    .line 331
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 356
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 508
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 581
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 612
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    .line 630
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 646
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$Rule;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Rule;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 162
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    if-eqz v2, :cond_1

    .line 163
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    invoke-virtual {p1, v6, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 165
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    if-ne v2, v6, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    if-eqz v2, :cond_3

    .line 166
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 168
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    if-ne v2, v6, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    if-eqz v2, :cond_5

    .line 169
    :cond_4
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 171
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 172
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 173
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 174
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 175
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 172
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v2, v2

    if-lez v2, :cond_8

    .line 180
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 181
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 184
    .end local v1    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v2, v2

    if-lez v2, :cond_9

    .line 185
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 186
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 185
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 189
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 190
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 191
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    aget-object v0, v2, v1

    .line 192
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v0, :cond_a

    .line 193
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 190
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 197
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    .end local v1    # "i":I
    :cond_b
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    if-ne v2, v6, :cond_c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    if-eqz v2, :cond_d

    .line 198
    :cond_c
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 200
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 201
    :cond_e
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 203
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v2, v2

    if-lez v2, :cond_10

    .line 204
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 205
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 208
    .end local v1    # "i":I
    :cond_10
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v2, v2

    if-lez v2, :cond_11

    .line 209
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 210
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 213
    .end local v1    # "i":I
    :cond_11
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    if-ne v2, v6, :cond_12

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    if-eqz v2, :cond_13

    .line 214
    :cond_12
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 216
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    if-nez v2, :cond_14

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    if-eqz v2, :cond_15

    .line 217
    :cond_14
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 219
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 220
    return-void
.end method
