.class Lcom/google/android/finsky/library/SQLiteLibrary$1;
.super Ljava/lang/Object;
.source "SQLiteLibrary.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/library/SQLiteLibrary;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/finsky/library/LibraryEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/library/SQLiteLibrary;

.field final synthetic val$all:Landroid/database/Cursor;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/library/SQLiteLibrary;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->this$0:Lcom/google/android/finsky/library/SQLiteLibrary;

    iput-object p2, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 419
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 420
    return-void
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 327
    iget-object v1, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isLast()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 328
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 331
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Lcom/google/android/finsky/library/LibraryEntry;
    .locals 45

    .prologue
    .line 336
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 337
    const/16 v31, 0x0

    .line 405
    :goto_0
    return-object v31

    .line 340
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x0

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    .line 341
    .local v4, "accountName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x1

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 342
    .local v16, "libraryId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x2

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 343
    .local v17, "backend":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x3

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 344
    .local v5, "docId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x4

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v36

    .line 345
    .local v36, "docType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x5

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 346
    .local v6, "offerType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x6

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 350
    .local v7, "documentHash":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x7

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/4 v14, 0x7

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 355
    .local v22, "validUntilTimestampMs":J
    :goto_1
    const-string v3, "u-wl"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 356
    const/4 v3, 0x1

    move/from16 v0, v36

    if-ne v0, v3, :cond_2

    .line 358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0x8

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "certificateHashList":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 361
    .local v9, "appCertificateHashes":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0x9

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 363
    .local v10, "appRefundPreDeliveryTimeoutMs":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xa

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 365
    .local v12, "appRefundPostDeliveryWindowMs":J
    new-instance v3, Lcom/google/android/finsky/library/LibraryAppEntry;

    invoke-direct/range {v3 .. v13}, Lcom/google/android/finsky/library/LibraryAppEntry;-><init>(Ljava/lang/String;Ljava/lang/String;IJ[Ljava/lang/String;JJ)V

    move-object/from16 v31, v3

    goto/16 :goto_0

    .line 353
    .end local v2    # "certificateHashList":Ljava/lang/String;
    .end local v9    # "appCertificateHashes":[Ljava/lang/String;
    .end local v10    # "appRefundPreDeliveryTimeoutMs":J
    .end local v12    # "appRefundPostDeliveryWindowMs":J
    .end local v22    # "validUntilTimestampMs":J
    :cond_1
    const-wide v22, 0x7fffffffffffffffL

    .restart local v22    # "validUntilTimestampMs":J
    goto :goto_1

    .line 368
    :cond_2
    const/16 v3, 0xf

    move/from16 v0, v36

    if-ne v0, v3, :cond_8

    .line 369
    const/4 v3, 0x3

    move/from16 v0, v17

    if-ne v0, v3, :cond_6

    .line 370
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xb

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_5

    const/16 v28, 0x1

    .line 371
    .local v28, "autoRenewing":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xc

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 372
    .local v24, "initiationTimestamp":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xd

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 373
    .local v26, "trialUntilTimestamp":J
    const-string v29, ""

    .line 374
    .local v29, "purchaseInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xe

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xe

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 377
    :cond_3
    const-string v30, ""

    .line 378
    .local v30, "signature":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xf

    invoke-interface {v3, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 379
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xf

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 381
    :cond_4
    new-instance v14, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    move-object v15, v4

    move-object/from16 v18, v5

    move/from16 v19, v6

    move-wide/from16 v20, v7

    invoke-direct/range {v14 .. v30}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJJJZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v31, v14

    goto/16 :goto_0

    .line 370
    .end local v24    # "initiationTimestamp":J
    .end local v26    # "trialUntilTimestamp":J
    .end local v28    # "autoRenewing":Z
    .end local v29    # "purchaseInfo":Ljava/lang/String;
    .end local v30    # "signature":Ljava/lang/String;
    :cond_5
    const/16 v28, 0x0

    goto :goto_2

    .line 386
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xb

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_7

    const/16 v28, 0x1

    .line 387
    .restart local v28    # "autoRenewing":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xc

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 388
    .restart local v24    # "initiationTimestamp":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xd

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 389
    .restart local v26    # "trialUntilTimestamp":J
    new-instance v31, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    move-object/from16 v32, v4

    move-object/from16 v33, v16

    move/from16 v34, v17

    move-object/from16 v35, v5

    move/from16 v36, v6

    move-wide/from16 v37, v7

    move-wide/from16 v40, v24

    move-wide/from16 v42, v26

    move/from16 v44, v28

    invoke-direct/range {v31 .. v44}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJLjava/lang/Long;JJZ)V

    goto/16 :goto_0

    .line 386
    .end local v24    # "initiationTimestamp":J
    .end local v26    # "trialUntilTimestamp":J
    .end local v28    # "autoRenewing":Z
    :cond_7
    const/16 v28, 0x0

    goto :goto_3

    .line 393
    :cond_8
    const/16 v3, 0xb

    move/from16 v0, v36

    if-ne v0, v3, :cond_9

    .line 394
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xe

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 395
    .restart local v29    # "purchaseInfo":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/SQLiteLibrary$1;->val$all:Landroid/database/Cursor;

    const/16 v14, 0xf

    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 399
    .restart local v30    # "signature":Ljava/lang/String;
    if-eqz v29, :cond_9

    if-eqz v30, :cond_9

    .line 400
    new-instance v31, Lcom/google/android/finsky/library/LibraryInAppEntry;

    move-object/from16 v32, v4

    move-object/from16 v33, v16

    move-object/from16 v34, v5

    move/from16 v35, v6

    move-object/from16 v36, v29

    move-object/from16 v37, v30

    move-wide/from16 v38, v7

    invoke-direct/range {v31 .. v39}, Lcom/google/android/finsky/library/LibraryInAppEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 405
    .end local v29    # "purchaseInfo":Ljava/lang/String;
    .end local v30    # "signature":Ljava/lang/String;
    :cond_9
    new-instance v31, Lcom/google/android/finsky/library/LibraryEntry;

    move-object/from16 v32, v4

    move-object/from16 v33, v16

    move/from16 v34, v17

    move-object/from16 v35, v5

    move/from16 v37, v6

    move-wide/from16 v38, v7

    move-wide/from16 v40, v22

    invoke-direct/range {v31 .. v41}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIJJ)V

    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/finsky/library/SQLiteLibrary$1;->next()Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 411
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
