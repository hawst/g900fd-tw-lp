.class Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;
.super Ljava/lang/Object;
.source "DetailsSummaryWishlistView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;

.field final synthetic val$currAccount:Landroid/accounts/Account;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->this$0:Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$currAccount:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    .line 57
    .local v2, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$currAccount:Landroid/accounts/Account;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v1, 0xcd

    .line 60
    .local v1, "clickType":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 61
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 63
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->this$0:Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v3, v4, v2}, Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V

    .line 65
    return-void

    .line 57
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    .end local v1    # "clickType":I
    :cond_0
    const/16 v1, 0xcc

    goto :goto_0
.end method
