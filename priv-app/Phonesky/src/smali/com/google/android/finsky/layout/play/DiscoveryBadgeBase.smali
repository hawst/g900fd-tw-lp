.class public abstract Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.super Landroid/widget/LinearLayout;
.source "DiscoveryBadgeBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mBadge:Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

.field protected mBadgeRadius:I

.field protected mCurrentFillColor:I

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mIcon:Lcom/google/android/play/image/FifeImageView;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field protected mTitle:Landroid/widget/TextView;

.field protected mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0178

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mBadgeRadius:I

    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->setWillNotDraw(Z)V

    .line 59
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "badge"    # Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mBadge:Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 78
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 79
    iput-object p5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 80
    iput-object p6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 81
    iput-object p4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 82
    iput-object p7, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 84
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasBackgroundColor:Z

    if-eqz v1, :cond_3

    .line 85
    iget v1, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mCurrentFillColor:I

    .line 90
    :goto_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mIcon:Lcom/google/android/play/image/FifeImageView;

    if-eqz v1, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onPreImageLoad()V

    .line 92
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mIcon:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v2, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v3, v3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v1, v2, v3, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mTitle:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mBadge:Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    iget-object v0, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    .line 97
    .local v0, "badgeLink":Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_2

    .line 99
    :cond_1
    invoke-virtual {p0, p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->getPlayStoreUiElementType()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 103
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 106
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v1, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v1, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 111
    return-void

    .line 87
    .end local v0    # "badgeLink":Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mCurrentFillColor:I

    goto :goto_0
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected abstract getPlayStoreUiElementType()I
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mBadge:Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    iget-object v0, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    .line 122
    .local v0, "badgeLink":Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    if-eqz v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAllReviews(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V

    .line 130
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 131
    return-void

    .line 124
    :cond_1
    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    if-eqz v1, :cond_2

    .line 125
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAllReviews(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V

    goto :goto_0

    .line 126
    :cond_2
    iget-object v1, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mBadge:Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 65
    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mIcon:Lcom/google/android/play/image/FifeImageView;

    .line 66
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->mTitle:Landroid/widget/TextView;

    .line 67
    return-void
.end method

.method protected onPreImageLoad()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method
