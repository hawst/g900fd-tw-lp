.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;
.super Ljava/lang/Object;
.source "CheckoutPurchaseSidecar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->onResponse(Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

.field final synthetic val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

.field final synthetic val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    iput-object p3, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 574
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v0, :cond_1

    .line 575
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 576
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/receivers/Installer;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->installApp()V

    .line 585
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    iget v0, v0, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    packed-switch v0, :pswitch_data_0

    .line 603
    const-string v0, "Unknown purchase status: %d"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    iget v2, v2, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 604
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1402(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .line 605
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v4, v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2600(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    .line 608
    :goto_1
    return-void

    .line 582
    :cond_1
    const-string v0, "missing delivery data for %s"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPreparePurchaseParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    invoke-static {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 588
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v4, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2100(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_1

    .line 591
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$response:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCompleteChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2202(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 592
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/4 v1, 0x7

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_1

    .line 595
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    iget v2, v2, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->permissionError:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->val$status:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->errorMessageHtml:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1402(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .line 597
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v4, v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2400(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_1

    .line 600
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener$1;->this$1:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CommitListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/4 v1, 0x2

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_1

    .line 585
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
