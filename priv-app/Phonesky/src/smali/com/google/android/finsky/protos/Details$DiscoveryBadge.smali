.class public final Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiscoveryBadge"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;


# instance fields
.field public aggregateRating:F

.field public backgroundColor:I

.field public contentDescription:Ljava/lang/String;

.field public discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

.field public downloadCount:Ljava/lang/String;

.field public downloadUnits:Ljava/lang/String;

.field public hasAggregateRating:Z

.field public hasBackgroundColor:Z

.field public hasContentDescription:Z

.field public hasDownloadCount:Z

.field public hasDownloadUnits:Z

.field public hasIsPlusOne:Z

.field public hasServerLogsCookie:Z

.field public hasTitle:Z

.field public hasUserStarRating:Z

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public isPlusOne:Z

.field public playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

.field public serverLogsCookie:[B

.field public title:Ljava/lang/String;

.field public userStarRating:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 746
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 747
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->clear()Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 748
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .locals 2

    .prologue
    .line 690
    sget-object v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->_emptyArray:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    if-nez v0, :cond_1

    .line 691
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 693
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->_emptyArray:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    if-nez v0, :cond_0

    .line 694
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    sput-object v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->_emptyArray:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 696
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->_emptyArray:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    return-object v0

    .line 696
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 751
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    .line 752
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasTitle:Z

    .line 753
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    .line 754
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasContentDescription:Z

    .line 755
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 756
    iput v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    .line 757
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasBackgroundColor:Z

    .line 758
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    .line 759
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    .line 760
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasServerLogsCookie:Z

    .line 761
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    .line 762
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasIsPlusOne:Z

    .line 763
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    .line 764
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasAggregateRating:Z

    .line 765
    iput v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    .line 766
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasUserStarRating:Z

    .line 767
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    .line 768
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadCount:Z

    .line 769
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    .line 770
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadUnits:Z

    .line 771
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    .line 772
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->cachedSize:I

    .line 773
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 821
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 822
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 823
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 826
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_2

    .line 827
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 830
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasBackgroundColor:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    if-eqz v1, :cond_4

    .line 831
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 834
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    if-eqz v1, :cond_5

    .line 835
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 838
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasServerLogsCookie:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 839
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 842
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasIsPlusOne:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    if-eqz v1, :cond_9

    .line 843
    :cond_8
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 846
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasAggregateRating:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_b

    .line 848
    :cond_a
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 851
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasUserStarRating:Z

    if-nez v1, :cond_c

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    if-eqz v1, :cond_d

    .line 852
    :cond_c
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 855
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadCount:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 856
    :cond_e
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 859
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadUnits:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 860
    :cond_10
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 863
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasContentDescription:Z

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 864
    :cond_12
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 867
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    if-eqz v1, :cond_14

    .line 868
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 871
    :cond_14
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 879
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 880
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 884
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 885
    :sswitch_0
    return-object p0

    .line 890
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    .line 891
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasTitle:Z

    goto :goto_0

    .line 895
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 896
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 898
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 902
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    .line 903
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasBackgroundColor:Z

    goto :goto_0

    .line 907
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    if-nez v1, :cond_2

    .line 908
    new-instance v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    .line 910
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 914
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    .line 915
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasServerLogsCookie:Z

    goto :goto_0

    .line 919
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    .line 920
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasIsPlusOne:Z

    goto :goto_0

    .line 924
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    .line 925
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasAggregateRating:Z

    goto :goto_0

    .line 929
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    .line 930
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasUserStarRating:Z

    goto :goto_0

    .line 934
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    .line 935
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadCount:Z

    goto :goto_0

    .line 939
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    .line 940
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadUnits:Z

    goto :goto_0

    .line 944
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    .line 945
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasContentDescription:Z

    goto :goto_0

    .line 949
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    if-nez v1, :cond_3

    .line 950
    new-instance v1, Lcom/google/android/finsky/protos/Details$PlayerBadge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Details$PlayerBadge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    .line 952
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 880
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 684
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 779
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 780
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 782
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_2

    .line 783
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 785
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasBackgroundColor:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    if-eqz v0, :cond_4

    .line 786
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->backgroundColor:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 788
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    if-eqz v0, :cond_5

    .line 789
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->discoveryBadgeLink:Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 791
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasServerLogsCookie:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 792
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 794
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasIsPlusOne:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    if-eqz v0, :cond_9

    .line 795
    :cond_8
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 797
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasAggregateRating:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_b

    .line 799
    :cond_a
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 801
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasUserStarRating:Z

    if-nez v0, :cond_c

    iget v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    if-eqz v0, :cond_d

    .line 802
    :cond_c
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 804
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadCount:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 805
    :cond_e
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 807
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadUnits:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 808
    :cond_10
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 810
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasContentDescription:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 811
    :cond_12
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->contentDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 813
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    if-eqz v0, :cond_14

    .line 814
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->playerBadge:Lcom/google/android/finsky/protos/Details$PlayerBadge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 816
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 817
    return-void
.end method
