.class Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;
.super Lcom/google/android/finsky/providers/SuggestionFetcher;
.source "RecentSearchSuggestionFetcher.java"


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private final mFinskyExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/database/Cursor;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;Lcom/google/android/finsky/experiments/FinskyExperiments;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "historyCursor"    # Landroid/database/Cursor;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "suggestionHandler"    # Lcom/google/android/finsky/providers/SuggestionHandler;
    .param p5, "finskyExperiments"    # Lcom/google/android/finsky/experiments/FinskyExperiments;

    .prologue
    .line 21
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/finsky/providers/SuggestionFetcher;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 22
    iput-object p2, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    .line 23
    iput-object p5, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mFinskyExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    .line 24
    return-void
.end method

.method private getMaxRecentSuggestions()I
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mFinskyExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    const-string v1, "cl:search.cap_local_suggestions_2"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x2

    .line 66
    :goto_0
    return v0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mFinskyExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    const-string v1, "cl:search.cap_local_suggestions_3"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const/4 v0, 0x3

    goto :goto_0

    .line 66
    :cond_1
    const v0, 0x7fffffff

    goto :goto_0
.end method


# virtual methods
.method protected makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
    .locals 14
    .param p1, "listener"    # Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    .prologue
    .line 28
    const/4 v1, 0x0

    .line 29
    .local v1, "col":I
    const/4 v4, 0x0

    .line 30
    .local v4, "idColumn":I
    const/4 v9, 0x0

    .line 32
    .local v9, "textColumn":I
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v2, v0, v3

    .line 33
    .local v2, "historyCol":Ljava/lang/String;
    const-string v10, "_id"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 34
    move v4, v1

    .line 38
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 32
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 35
    :cond_1
    const-string v10, "suggest_text_1"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 36
    move v9, v1

    goto :goto_1

    .line 40
    .end local v2    # "historyCol":Ljava/lang/String;
    :cond_2
    const/4 v7, 0x0

    .line 41
    .local v7, "resultCount":I
    invoke-direct {p0}, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->getMaxRecentSuggestions()I

    move-result v6

    .line 42
    .local v6, "maxResults":I
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 43
    :goto_2
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_4

    if-ge v7, v6, :cond_4

    .line 45
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 46
    .local v8, "suggestion":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mQuery:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 47
    add-int/lit8 v7, v7, 0x1

    .line 48
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    iget-object v11, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const v12, 0x108004a

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v8, v13}, Lcom/google/android/finsky/providers/SuggestionHandler;->addRow(ILjava/lang/Object;Ljava/lang/String;[B)V

    .line 52
    :cond_3
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 54
    .end local v8    # "suggestion":Ljava/lang/String;
    :cond_4
    iget-object v10, p0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;->mCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 55
    invoke-interface {p1}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    .line 56
    return-void
.end method
