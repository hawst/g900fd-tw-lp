.class public Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "OrderHistoryFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;


# instance fields
.field private mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

.field private mBreadcrumb:Ljava/lang/String;

.field private mDfeList:Lcom/google/android/finsky/api/model/DfeList;

.field private mIsAdapterSet:Z

.field private mLastRequestTimeMs:J

.field private mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

.field private mRecyclerViewRestoreBundle:Landroid/os/Bundle;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    .line 74
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private clearDfeList()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 261
    :cond_0
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;
    .locals 3
    .param p0, "listUrl"    # Ljava/lang/String;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 80
    new-instance v1, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;-><init>()V

    .line 81
    .local v1, "fragment":Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v2, "list_url"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {v1, p2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 86
    return-object v1
.end method

.method private rebindAdapter()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 269
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-nez v0, :cond_0

    .line 270
    const-string v0, "Recycler view null, ignoring."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    if-nez v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v8

    .line 276
    .local v8, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v8, :cond_1

    .line 277
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v9

    .line 278
    .local v9, "cookie":[B
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v0, v9}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 281
    .end local v9    # "cookie":[B
    :cond_1
    new-instance v0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v5, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .line 286
    .end local v8    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mIsAdapterSet:Z

    if-nez v0, :cond_3

    .line 287
    iput-boolean v6, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mIsAdapterSet:Z

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v1, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;-><init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 307
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    goto :goto_0
.end method


# virtual methods
.method protected createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;
    .locals 7
    .param p1, "frame"    # Lcom/google/android/finsky/layout/ContentFrame;

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;

    const v2, 0x7f0a0219

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0109

    const/4 v6, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    return-object v0
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 230
    const v0, 0x7f0400ae

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x0

    .line 238
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mBreadcrumb:Ljava/lang/String;

    .line 167
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    const v4, 0x7f0a029a

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iput-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 169
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 170
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v0

    .line 173
    .local v0, "horizontalPadding":I
    const v3, 0x7f0b0092

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 176
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v3, v0, v4, v0, v5}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setPadding(IIII)V

    .line 179
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 180
    .local v1, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v3, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 181
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v4, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;

    invoke-direct {v4}, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->rebindActionBar()V

    .line 185
    iget-wide v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mLastRequestTimeMs:J

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/OrderHistoryHelper;->hasMutationOccurredSince(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->clearDfeList()V

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->isDataReady()Z

    move-result v3

    if-nez v3, :cond_1

    .line 193
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->requestData()V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->switchToLoading()V

    .line 199
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 200
    return-void

    .line 196
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->rebindAdapter()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->setRetainInstance(Z)V

    .line 93
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ContentFrame;

    .line 100
    .local v1, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 101
    .local v2, "headerListLayout":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 102
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$1;-><init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 157
    const v3, 0x7f0a029a

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setContentViewId(I)V

    .line 159
    return-object v1
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 314
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 319
    :cond_0
    iput-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 320
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onDestroyView()V

    .line 322
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onDestroy()V

    .line 323
    iput-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .line 324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mIsAdapterSet:Z

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detach()V

    .line 333
    :cond_2
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 334
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 392
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 365
    if-ne p1, v4, :cond_0

    .line 366
    const-string v2, "package_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "account_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "appRefundAccount":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$3;-><init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)V

    invoke-static {v2, v1, v0, v4, v3}, Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    .line 387
    .end local v0    # "appRefundAccount":Ljava/lang/String;
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onRefundAction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appRefundAccount"    # Ljava/lang/String;

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 342
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v4, "refund_confirm"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 356
    :goto_0
    return-void

    .line 346
    :cond_0
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 347
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v4, 0x7f0c02a7

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b2

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 350
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 351
    .local v2, "extraArguments":Landroid/os/Bundle;
    const-string v4, "package_name"

    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v4, "account_name"

    invoke-virtual {v2, v4, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const/4 v4, 0x1

    invoke-virtual {v0, p0, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 354
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 355
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "refund_confirm"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public rebindActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 211
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 212
    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 213
    .local v2, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mContext:Landroid/content/Context;

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    .line 215
    .local v0, "actionBarColor":I
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 216
    const v3, 0x7f0a0039

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 219
    .local v1, "backgroundLayer":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 224
    .end local v0    # "actionBarColor":I
    .end local v1    # "backgroundLayer":Landroid/view/View;
    .end local v2    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 225
    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v4, 0x1

    invoke-interface {v3, v5, v4}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 226
    return-void
.end method

.method protected rebindViews()V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->rebindAdapter()V

    .line 266
    return-void
.end method

.method protected requestData()V
    .locals 4

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->clearDfeList()V

    .line 248
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "list_url"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 249
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 252
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mLastRequestTimeMs:J

    .line 253
    return-void
.end method
