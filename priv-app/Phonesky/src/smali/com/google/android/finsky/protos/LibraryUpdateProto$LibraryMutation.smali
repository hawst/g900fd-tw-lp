.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryMutation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;


# instance fields
.field public appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

.field public deleted:Z

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public documentHash:J

.field public hasDeleted:Z

.field public hasDocumentHash:Z

.field public hasOfferType:Z

.field public hasPreordered:Z

.field public hasValidUntilTimestampMsec:Z

.field public inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

.field public offerType:I

.field public preordered:Z

.field public subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

.field public validUntilTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .line 286
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .locals 2

    .prologue
    .line 241
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-nez v0, :cond_1

    .line 242
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-nez v0, :cond_0

    .line 245
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    sput-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .line 247
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    return-object v0

    .line 247
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 289
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 290
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    .line 291
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasOfferType:Z

    .line 292
    iput-wide v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    .line 293
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDocumentHash:Z

    .line 294
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    .line 295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDeleted:Z

    .line 296
    iput-wide v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    .line 297
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasValidUntilTimestampMsec:Z

    .line 298
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    .line 299
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasPreordered:Z

    .line 300
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    .line 301
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    .line 302
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    .line 303
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->cachedSize:I

    .line 304
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 342
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 343
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasOfferType:Z

    if-eqz v1, :cond_2

    .line 348
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDocumentHash:Z

    if-nez v1, :cond_3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 352
    :cond_3
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDeleted:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    if-eqz v1, :cond_6

    .line 356
    :cond_5
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    if-eqz v1, :cond_7

    .line 360
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    if-eqz v1, :cond_8

    .line 364
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    if-eqz v1, :cond_9

    .line 368
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasValidUntilTimestampMsec:Z

    if-nez v1, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_b

    .line 372
    :cond_a
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasPreordered:Z

    if-nez v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    if-eqz v1, :cond_d

    .line 376
    :cond_c
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_d
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 387
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 388
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 392
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 393
    :sswitch_0
    return-object p0

    .line 398
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v2, :cond_1

    .line 399
    new-instance v2, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 401
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 405
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 406
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 419
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    .line 420
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasOfferType:Z

    goto :goto_0

    .line 426
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    .line 427
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDocumentHash:Z

    goto :goto_0

    .line 431
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    .line 432
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDeleted:Z

    goto :goto_0

    .line 436
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    if-nez v2, :cond_2

    .line 437
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    .line 439
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 443
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    if-nez v2, :cond_3

    .line 444
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    .line 446
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 450
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    if-nez v2, :cond_4

    .line 451
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    .line 453
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 457
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    .line 458
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasValidUntilTimestampMsec:Z

    goto :goto_0

    .line 462
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    .line 463
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasPreordered:Z

    goto/16 :goto_0

    .line 388
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 313
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasOfferType:Z

    if-eqz v0, :cond_2

    .line 314
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 316
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDocumentHash:Z

    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 317
    :cond_3
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 319
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasDeleted:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    if-eqz v0, :cond_6

    .line 320
    :cond_5
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 322
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    if-eqz v0, :cond_7

    .line 323
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 325
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    if-eqz v0, :cond_8

    .line 326
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 328
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    if-eqz v0, :cond_9

    .line 329
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 331
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasValidUntilTimestampMsec:Z

    if-nez v0, :cond_a

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 332
    :cond_a
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 334
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasPreordered:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    if-eqz v0, :cond_d

    .line 335
    :cond_c
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->preordered:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 337
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 338
    return-void
.end method
