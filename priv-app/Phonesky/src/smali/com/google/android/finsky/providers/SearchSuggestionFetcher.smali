.class Lcom/google/android/finsky/providers/SearchSuggestionFetcher;
.super Lcom/google/android/finsky/providers/SuggestionFetcher;
.source "SearchSuggestionFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;
    }
.end annotation


# static fields
.field private static BACKEND_ID_TO_ICON_PACKAGE_NAME:[Ljava/lang/String;

.field private static final BASE_URI:Landroid/net/Uri;

.field private static mIsGoogleTV:Ljava/lang/Boolean;


# instance fields
.field private final mBackendId:I

.field private final mIconSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 31
    const-string v0, "https://market.android.com/suggest/SuggRequest"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->BASE_URI:Landroid/net/Uri;

    .line 38
    sput-object v3, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIsGoogleTV:Ljava/lang/Boolean;

    .line 78
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.google.android.apps.books"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.android.music"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.google.android.videos"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.google.android.apps.magazines"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.android.videos"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->BACKEND_ID_TO_ICON_PACKAGE_NAME:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/finsky/providers/SuggestionHandler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backendId"    # I
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "suggestionHandler"    # Lcom/google/android/finsky/providers/SuggestionHandler;

    .prologue
    .line 42
    invoke-direct {p0, p3, p1, p4}, Lcom/google/android/finsky/providers/SuggestionFetcher;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 43
    iput p2, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mBackendId:I

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b012f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIconSize:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/providers/SearchSuggestionFetcher;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    .param p2, "x2"    # Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->getIconUri(Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getBackendCanonicalIconUri(I)Landroid/net/Uri;
    .locals 7
    .param p1, "backendId"    # I

    .prologue
    .line 93
    :try_start_0
    sget-object v5, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->BACKEND_ID_TO_ICON_PACKAGE_NAME:[Ljava/lang/String;

    aget-object v2, v5, p1

    .line 94
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 95
    .local v1, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/high16 v6, 0x10000

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 97
    .local v4, "rinfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 98
    .local v3, "rinfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v5, Landroid/content/pm/ApplicationInfo;->icon:I

    .line 99
    .local v0, "icon":I
    if-eqz v0, :cond_0

    .line 100
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "android.resource"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 109
    .end local v0    # "icon":I
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pkgName":Ljava/lang/String;
    .end local v3    # "rinfo":Landroid/content/pm/ResolveInfo;
    .end local v4    # "rinfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_0
    return-object v5

    .line 106
    :catch_0
    move-exception v5

    .line 109
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private getIconUri(Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)Landroid/net/Uri;
    .locals 8
    .param p1, "navSuggestion"    # Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    .param p2, "listener"    # Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 114
    iget-object v1, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    .line 116
    .local v1, "docId":Ljava/lang/String;
    iget-boolean v7, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    if-nez v7, :cond_0

    iget-object v7, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v7, :cond_0

    move v3, v5

    .line 117
    .local v3, "noIcon":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 118
    invoke-static {v1}, Lcom/google/android/finsky/utils/DocUtils;->docidToBackend(Ljava/lang/String;)I

    move-result v0

    .line 119
    .local v0, "backendId":I
    invoke-direct {p0, v0}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->getBackendCanonicalIconUri(I)Landroid/net/Uri;

    move-result-object v5

    .line 148
    .end local v0    # "backendId":I
    :goto_1
    return-object v5

    .end local v3    # "noIcon":Z
    :cond_0
    move v3, v6

    .line 116
    goto :goto_0

    .line 123
    .restart local v3    # "noIcon":Z
    :cond_1
    new-instance v4, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$1;

    invoke-direct {v4, p0, p2}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$1;-><init>(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)V

    .line 131
    .local v4, "onComplete":Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;
    new-instance v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;

    iget-object v7, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7, v1}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 134
    .local v2, "iconLoader":Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->addProducer()V

    .line 137
    iget-boolean v7, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    if-eqz v7, :cond_2

    .line 138
    iget-object v5, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    invoke-virtual {v2, v5, v4}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromBlob([BLcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    .line 148
    :goto_2
    sget-object v5, Lcom/google/android/finsky/providers/AppIconProvider;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    goto :goto_1

    .line 139
    :cond_2
    iget-object v7, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v7, :cond_3

    .line 140
    iget-object v5, p1, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {v2, v5, v4}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromUrl(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    goto :goto_2

    .line 143
    :cond_3
    const-string v7, "Nav suggestion for %s has no blob, no image"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->onComplete()V

    .line 145
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private isGoogleTv()Z
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIsGoogleTV:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.tv"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIsGoogleTV:Ljava/lang/Boolean;

    .line 155
    :cond_0
    sget-object v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIsGoogleTV:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
    .locals 9
    .param p1, "listener"    # Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    .prologue
    const/4 v4, 0x1

    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-interface {p1}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    .line 217
    :goto_0
    return-void

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/finsky/config/G;->gtvNavigationalSuggestEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v5, v4

    .line 172
    .local v5, "requestNavigationalSuggestions":Z
    :goto_1
    new-instance v8, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    invoke-direct {v8, p1}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;-><init>(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V

    .line 175
    .local v8, "refCountedListener":Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mQuery:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mBackendId:I

    iget v3, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mIconSize:I

    new-instance v6, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;

    invoke-direct {v6, p0, v8}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;-><init>(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)V

    new-instance v7, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$3;

    invoke-direct {v7, p0, v8}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$3;-><init>(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)V

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/api/DfeApi;->searchSuggest(Ljava/lang/String;IIZZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->startRequestLatencyTimer()V

    goto :goto_0

    .line 168
    .end local v5    # "requestNavigationalSuggestions":Z
    .end local v8    # "refCountedListener":Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
