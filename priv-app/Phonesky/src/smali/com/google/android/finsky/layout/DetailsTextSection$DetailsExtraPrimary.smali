.class public Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
.super Ljava/lang/Object;
.source "DetailsTextSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailsTextSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetailsExtraPrimary"
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public skipInCollapsedMode:Z

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p5, "skipInCollapsedMode"    # Z

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->title:Ljava/lang/String;

    .line 132
    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->description:Ljava/lang/String;

    .line 133
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->url:Ljava/lang/String;

    .line 134
    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 135
    iput-boolean p5, p0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->skipInCollapsedMode:Z

    .line 136
    return-void
.end method
