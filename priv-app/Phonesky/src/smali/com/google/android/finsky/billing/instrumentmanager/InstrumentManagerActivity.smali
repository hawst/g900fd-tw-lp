.class public Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "InstrumentManagerActivity.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "commonToken"    # [B
    .param p2, "actionToken"    # [B

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->addAccountNameExtra(Landroid/content/Intent;Ljava/lang/String;)V

    .line 34
    const-string v1, "common_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 35
    const-string v1, "action_token"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 36
    return-object v0
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 95
    const/16 v0, 0x640

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    new-instance v3, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;

    iget-object v6, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-direct {v3, p0, v6}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;-><init>(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/FinskyEventLog;)V

    .line 45
    .local v3, "logger":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;
    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->setEventListener(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "instrument_manager_fragment"

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "common_token"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 50
    .local v1, "commonToken":[B
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "action_token"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 52
    .local v0, "actionToken":[B
    const v4, 0x7f0d01bd

    .line 53
    .local v4, "themeResourceId":I
    invoke-static {v1, v0, v4}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->newInstance([B[BI)Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    move-result-object v2

    .line 55
    .local v2, "fragment":Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    .line 56
    .local v5, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v6, 0x1020002

    const-string v7, "instrument_manager_fragment"

    invoke-virtual {v5, v6, v2, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 57
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 59
    .end local v0    # "actionToken":[B
    .end local v1    # "commonToken":[B
    .end local v2    # "fragment":Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
    .end local v4    # "themeResourceId":I
    .end local v5    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->setEventListener(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;)V

    .line 66
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onDestroy()V

    .line 67
    return-void
.end method

.method public onInstrumentManagerResult(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 71
    packed-switch p1, :pswitch_data_0

    .line 86
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected InstrumentManager resultCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 73
    :pswitch_0
    const-string v2, "com.google.android.wallet.instrumentmanager.INSTRUMENT_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "instrumentId":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 76
    .local v1, "result":Landroid/content/Intent;
    const-string v2, "instrument_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->setResult(ILandroid/content/Intent;)V

    .line 90
    .end local v0    # "instrumentId":Ljava/lang/String;
    .end local v1    # "result":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->finish()V

    .line 91
    return-void

    .line 83
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->setResult(I)V

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
