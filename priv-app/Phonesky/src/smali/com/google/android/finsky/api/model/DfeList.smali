.class public Lcom/google/android/finsky/api/model/DfeList;
.super Lcom/google/android/finsky/api/model/ContainerList;
.source "DfeList.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/ContainerList",
        "<",
        "Lcom/google/android/finsky/protos/DocList$ListResponse;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/api/model/DfeList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mFilteredDocId:Ljava/lang/String;

.field private mInitialListUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList$1;

    invoke-direct {v0}, Lcom/google/android/finsky/api/model/DfeList$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/api/model/DfeList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "autoLoadNextPage"    # Z

    .prologue
    .line 63
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/api/model/ContainerList;-><init>(Ljava/lang/String;Z)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/google/android/finsky/api/model/DfeList;->mInitialListUrl:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 66
    return-void
.end method

.method private constructor <init>(Ljava/util/List;IZLjava/lang/String;)V
    .locals 2
    .param p2, "currentCount"    # I
    .param p3, "autoLoadNextPage"    # Z
    .param p4, "filteredDocId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;",
            ">;IZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "urlList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;>;"
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/api/model/ContainerList;-><init>(Ljava/util/List;IZ)V

    .line 25
    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget-object v0, v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeList;->mInitialListUrl:Ljava/lang/String;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;IZLjava/lang/String;Lcom/google/android/finsky/api/model/DfeList$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Lcom/google/android/finsky/api/model/DfeList$1;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Ljava/util/List;IZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public clearDataAndReplaceInitialUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeList;->mInitialListUrl:Ljava/lang/String;

    .line 71
    invoke-super {p0, p1}, Lcom/google/android/finsky/api/model/ContainerList;->clearDataAndReplaceInitialUrl(Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method protected clearDiskCache()V
    .locals 4

    .prologue
    .line 178
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/api/model/DfeList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 179
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/api/model/DfeList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget-object v1, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public filterDocId(Ljava/lang/String;)V
    .locals 0
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public getInitialUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeList;->mInitialListUrl:Ljava/lang/String;

    return-object v0
.end method

.method protected getItemsFromResponse(Lcom/google/android/finsky/protos/DocList$ListResponse;)[Lcom/google/android/finsky/api/model/Document;
    .locals 5
    .param p1, "response"    # Lcom/google/android/finsky/protos/DocList$ListResponse;

    .prologue
    const/4 v4, 0x0

    .line 81
    iget-object v3, p1, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-nez v3, :cond_2

    .line 82
    :cond_0
    new-array v2, v4, [Lcom/google/android/finsky/api/model/Document;

    .line 105
    :cond_1
    :goto_0
    return-object v2

    .line 86
    :cond_2
    iget-object v3, p1, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/api/model/DfeList;->updateContainerAndGetItems(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)[Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    .line 88
    .local v2, "items":[Lcom/google/android/finsky/api/model/Document;
    iget-object v3, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 92
    const/4 v0, -0x1

    .line 93
    .local v0, "filterIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_3

    .line 94
    aget-object v3, v2, v1

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 95
    move v0, v1

    .line 100
    :cond_3
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 105
    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/ArrayUtils;->remove([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/finsky/api/model/Document;

    move-object v2, v3

    goto :goto_0

    .line 93
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected bridge synthetic getItemsFromResponse(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/finsky/protos/DocList$ListResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeList;->getItemsFromResponse(Lcom/google/android/finsky/protos/DocList$ListResponse;)[Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getNextPageUrl(Lcom/google/android/finsky/protos/DocList$ListResponse;)Ljava/lang/String;
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/DocList$ListResponse;

    .prologue
    .line 162
    const/4 v1, 0x0

    .line 164
    .local v1, "nextPageUrl":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 166
    iget-object v2, p1, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 167
    .local v0, "dfeDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v2, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-eqz v2, :cond_0

    .line 168
    iget-object v2, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    iget-object v1, v2, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    .line 171
    .end local v0    # "dfeDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic getNextPageUrl(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/finsky/protos/DocList$ListResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeList;->getNextPageUrl(Lcom/google/android/finsky/protos/DocList$ListResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected makeRequest(Ljava/lang/String;)Lcom/android/volley/Request;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, p1, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->getList(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    return-object v0
.end method

.method public setDfeApi(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 0
    .param p1, "api"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 113
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 130
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeList;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 132
    .local v1, "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget v2, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object v2, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    .end local v1    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    iget-boolean v2, p0, Lcom/google/android/finsky/api/model/DfeList;->mAutoLoadNextPage:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeList;->mFilteredDocId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    return-void

    .line 136
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
