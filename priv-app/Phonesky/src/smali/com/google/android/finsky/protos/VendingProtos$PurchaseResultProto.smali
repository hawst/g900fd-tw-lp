.class public final Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseResultProto"
.end annotation


# instance fields
.field public hasResultCode:Z

.field public hasResultCodeMessage:Z

.field public resultCode:I

.field public resultCodeMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 890
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 891
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    .line 892
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 895
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    .line 896
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCode:Z

    .line 897
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    .line 898
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCodeMessage:Z

    .line 899
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->cachedSize:I

    .line 900
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 917
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 918
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCode:Z

    if-eqz v1, :cond_1

    .line 919
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 922
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCodeMessage:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 923
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 926
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 934
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 935
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 939
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 940
    :sswitch_0
    return-object p0

    .line 945
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 946
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 957
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    .line 958
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCode:Z

    goto :goto_0

    .line 964
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    .line 965
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCodeMessage:Z

    goto :goto_0

    .line 935
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 946
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 906
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCode:Z

    if-eqz v0, :cond_1

    .line 907
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 909
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->hasResultCodeMessage:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 910
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;->resultCodeMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 912
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 913
    return-void
.end method
