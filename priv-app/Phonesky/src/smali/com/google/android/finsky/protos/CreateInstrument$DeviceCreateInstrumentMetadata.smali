.class public final Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceCreateInstrumentMetadata"
.end annotation


# instance fields
.field public flowType:I

.field public hasFlowType:Z

.field public hasInstrumentType:Z

.field public instrumentType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 522
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 523
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    .line 527
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasInstrumentType:Z

    .line 528
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    .line 529
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasFlowType:Z

    .line 530
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->cachedSize:I

    .line 531
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 548
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 549
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasInstrumentType:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 550
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 553
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasFlowType:Z

    if-eqz v1, :cond_3

    .line 554
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 557
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 566
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 570
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 571
    :sswitch_0
    return-object p0

    .line 576
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    .line 577
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasInstrumentType:Z

    goto :goto_0

    .line 581
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 582
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 585
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    .line 586
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasFlowType:Z

    goto :goto_0

    .line 566
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 582
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 492
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasInstrumentType:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 538
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 540
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->hasFlowType:Z

    if-eqz v0, :cond_3

    .line 541
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 543
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 544
    return-void
.end method
