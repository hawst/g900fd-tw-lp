.class public final Lcom/google/android/finsky/protos/Details$PlayerBadge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayerBadge"
.end annotation


# instance fields
.field public overlayIcon:Lcom/google/android/finsky/protos/Common$Image;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 991
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 992
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$PlayerBadge;->clear()Lcom/google/android/finsky/protos/Details$PlayerBadge;

    .line 993
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$PlayerBadge;
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    .line 997
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->cachedSize:I

    .line 998
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1012
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1013
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_0

    .line 1014
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$PlayerBadge;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1025
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1026
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1030
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1031
    :sswitch_0
    return-object p0

    .line 1036
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 1037
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    .line 1039
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1026
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 971
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$PlayerBadge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$PlayerBadge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_0

    .line 1005
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$PlayerBadge;->overlayIcon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1007
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1008
    return-void
.end method
