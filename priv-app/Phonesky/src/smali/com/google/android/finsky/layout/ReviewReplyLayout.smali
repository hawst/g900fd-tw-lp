.class public Lcom/google/android/finsky/layout/ReviewReplyLayout;
.super Landroid/widget/LinearLayout;
.source "ReviewReplyLayout.java"


# instance fields
.field private mIsExpanded:Z

.field mReplyHeader:Landroid/widget/TextView;

.field mReplyText:Landroid/widget/TextView;

.field mReplyToggle:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/ReviewReplyLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mIsExpanded:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/layout/ReviewReplyLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ReviewReplyLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mIsExpanded:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/ReviewReplyLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->showMoreIndicator()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/ReviewReplyLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->showLessIndicator()V

    return-void
.end method

.method private disableToggle()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyToggle:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void
.end method

.method private enableToggle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    iput-boolean v1, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mIsExpanded:Z

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyToggle:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    new-instance v0, Lcom/google/android/finsky/layout/ReviewReplyLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout$1;-><init>(Lcom/google/android/finsky/layout/ReviewReplyLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    return-void
.end method

.method private showLessIndicator()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyToggle:Landroid/widget/ImageView;

    const v1, 0x7f020101

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    return-void
.end method

.method private showMoreIndicator()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyToggle:Landroid/widget/ImageView;

    const v1, 0x7f0200fe

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 74
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    const v0, 0x7f0a0363

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyHeader:Landroid/widget/TextView;

    .line 38
    const v0, 0x7f0a0364

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyText:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0a0365

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyToggle:Landroid/widget/ImageView;

    .line 40
    return-void
.end method

.method public setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 43
    iget-boolean v3, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-nez v3, :cond_0

    .line 44
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setVisibility(I)V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "docCreator":Ljava/lang/String;
    iget-boolean v3, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    if-eqz v3, :cond_2

    .line 50
    iget-wide v4, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    .line 51
    .local v4, "replyTimestampMsec":J
    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "formattedReplyDate":Ljava/lang/String;
    iget-boolean v3, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    if-eqz v3, :cond_1

    iget-wide v6, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    cmp-long v3, v6, v4

    if-lez v3, :cond_1

    .line 53
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->enableToggle()V

    .line 54
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->showMoreIndicator()V

    .line 55
    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyHeader:Landroid/widget/TextView;

    const v6, 0x7f0c0246

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v1, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .end local v2    # "formattedReplyDate":Ljava/lang/String;
    .end local v4    # "replyTimestampMsec":J
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyText:Landroid/widget/TextView;

    iget-object v6, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setVisibility(I)V

    .line 70
    return-void

    .line 58
    .restart local v2    # "formattedReplyDate":Ljava/lang/String;
    .restart local v4    # "replyTimestampMsec":J
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->disableToggle()V

    .line 59
    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyHeader:Landroid/widget/TextView;

    const v6, 0x7f0c0244

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v1, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 63
    .end local v2    # "formattedReplyDate":Ljava/lang/String;
    .end local v4    # "replyTimestampMsec":J
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->disableToggle()V

    .line 64
    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewReplyLayout;->mReplyHeader:Landroid/widget/TextView;

    const v6, 0x7f0c0245

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
