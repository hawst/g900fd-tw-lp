.class Lcom/google/android/finsky/model/CirclesModel$1;
.super Ljava/lang/Object;
.source "CirclesModel.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/model/CirclesModel;->loadCircles(Landroid/content/Context;Lcom/google/android/gms/people/PeopleClient;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/model/CirclesModel;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/model/CirclesModel;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/finsky/model/CirclesModel$1;->this$0:Lcom/google/android/finsky/model/CirclesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCirclesLoaded(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "belongingCircles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel$1;->this$0:Lcom/google/android/finsky/model/CirclesModel;

    # setter for: Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;
    invoke-static {v0, p1}, Lcom/google/android/finsky/model/CirclesModel;->access$002(Lcom/google/android/finsky/model/CirclesModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel$1;->this$0:Lcom/google/android/finsky/model/CirclesModel;

    # invokes: Lcom/google/android/finsky/model/CirclesModel;->invokeListener()V
    invoke-static {v0}, Lcom/google/android/finsky/model/CirclesModel;->access$100(Lcom/google/android/finsky/model/CirclesModel;)V

    .line 126
    return-void
.end method

.method public onCirclesLoadedFailed()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel$1;->this$0:Lcom/google/android/finsky/model/CirclesModel;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    # setter for: Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/google/android/finsky/model/CirclesModel;->access$002(Lcom/google/android/finsky/model/CirclesModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel$1;->this$0:Lcom/google/android/finsky/model/CirclesModel;

    # invokes: Lcom/google/android/finsky/model/CirclesModel;->invokeListener()V
    invoke-static {v0}, Lcom/google/android/finsky/model/CirclesModel;->access$100(Lcom/google/android/finsky/model/CirclesModel;)V

    .line 136
    return-void
.end method
