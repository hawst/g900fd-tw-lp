.class public abstract Lcom/google/android/finsky/adapters/CardSimpleListAdapter;
.super Lcom/google/android/finsky/adapters/FinskyListAdapter;
.source "CardSimpleListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/CardSimpleListAdapter$2;
    }
.end annotation


# instance fields
.field protected mCellLayoutId:I

.field protected mColumnCount:I

.field protected final mLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected final mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mRowCount:I

.field private mSingleBucketNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mTitle:Ljava/lang/String;

.field protected final mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/DfeList;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p6, "title"    # Ljava/lang/String;
    .param p7, "rowCount"    # I
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/finsky/adapters/FinskyListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 86
    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 87
    iput-object p4, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 89
    iput-object p6, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mTitle:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->getRegularGridColumnCount(Landroid/content/res/Resources;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mColumnCount:I

    .line 91
    iput p7, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mRowCount:I

    .line 93
    iput-object p8, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 95
    const v0, 0x7f040124

    iput v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mCellLayoutId:I

    .line 96
    return-void
.end method

.method private bindRowEntry(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1, "bucket"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "itemIndex"    # I
    .param p4, "docEntry"    # Landroid/view/View;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v5, 0x0

    .line 187
    move-object v0, p4

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 189
    .local v0, "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-nez p2, :cond_1

    .line 191
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 192
    invoke-virtual {v0, v5}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 193
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 202
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->clearCardState()V

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->isOrdered()Z

    move-result v1

    if-eqz v1, :cond_2

    move v9, p3

    :goto_1
    move-object v1, p2

    move-object v7, p5

    move v8, v5

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    goto :goto_0

    :cond_2
    const/4 v9, -0x1

    goto :goto_1
.end method

.method private getBannerHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 205
    if-eqz p1, :cond_1

    move-object v3, p1

    :goto_0
    check-cast v3, Lcom/google/android/finsky/layout/DocImageView;

    move-object v0, v3

    check-cast v0, Lcom/google/android/finsky/layout/DocImageView;

    .line 208
    .local v0, "bannerImage":Lcom/google/android/finsky/layout/DocImageView;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 209
    .local v1, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getContainerWithBannerTemplate()Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    move-result-object v2

    .line 212
    .local v2, "template":Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 213
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DocImageView;->setBackgroundColor(I)V

    .line 216
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/16 v5, 0x9

    aput v5, v4, v6

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 219
    return-object v0

    .line 205
    .end local v0    # "bannerImage":Lcom/google/android/finsky/layout/DocImageView;
    .end local v1    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "template":Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    :cond_1
    const v3, 0x7f04004e

    invoke-virtual {p0, v3, p2, v6}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_0
.end method

.method private getListItemViewType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 274
    .local v0, "lastRow":I
    if-ne p1, v0, :cond_1

    .line 275
    sget-object v2, Lcom/google/android/finsky/adapters/CardSimpleListAdapter$2;->$SwitchMap$com$google$android$finsky$adapters$PaginatedListAdapter$FooterMode:[I

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 283
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No footer or item at row "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 277
    :pswitch_0
    const/4 v1, 0x1

    .line 292
    :cond_0
    :goto_0
    :pswitch_1
    return v1

    .line 279
    :pswitch_2
    const/4 v1, 0x2

    goto :goto_0

    .line 286
    :cond_1
    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasBannerHeader()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 287
    const/4 v1, 0x3

    goto :goto_0

    .line 289
    :cond_2
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasPlainHeader()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    const/4 v1, 0x4

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getPaginatedRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 238
    if-nez p2, :cond_0

    .line 241
    const v0, 0x7f040044

    invoke-virtual {p0, v0, p3, v2}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v6, p2

    .line 242
    check-cast v6, Lcom/google/android/finsky/layout/BucketRow;

    .line 243
    .local v6, "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->configureBucketRow(Lcom/google/android/finsky/layout/BucketRow;)V

    .line 244
    const/4 v7, 0x0

    .local v7, "column":I
    :goto_0
    iget v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mColumnCount:I

    if-ge v7, v0, :cond_0

    .line 245
    iget v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mCellLayoutId:I

    invoke-virtual {p0, v0, v6, v2}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 244
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 248
    .end local v6    # "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    .end local v7    # "column":I
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 250
    .local v1, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mSingleBucketNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-nez v0, :cond_1

    .line 251
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v2, 0x190

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {v0, v2, v4, v5, v8}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mSingleBucketNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 255
    :cond_1
    const/4 v7, 0x0

    .restart local v7    # "column":I
    :goto_1
    iget v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mColumnCount:I

    if-ge v7, v0, :cond_2

    .line 256
    iget v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mColumnCount:I

    mul-int/2addr v0, p1

    add-int v3, v0, v7

    .line 257
    .local v3, "itemIndex":I
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mSingleBucketNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->bindRowEntry(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 255
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 261
    .end local v3    # "itemIndex":I
    :cond_2
    return-object p2
.end method

.method private getPaginatedRowIndex(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "headerCount":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasBannerHeader()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    add-int/lit8 v0, v0, 0x1

    .line 149
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasPlainHeader()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    add-int/lit8 v0, v0, 0x1

    .line 153
    :cond_1
    sub-int v1, p1, v0

    return v1
.end method

.method private getPlainHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 223
    if-nez p1, :cond_0

    .line 224
    const v1, 0x7f04011a

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_0
    move-object v0, p1

    .line 227
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 229
    .local v0, "plainHeader":Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mTitle:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 231
    return-object p1
.end method

.method private hasBannerHeader()Z
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasContainerWithBannerTemplate()Z

    move-result v0

    return v0
.end method

.method private hasPlainHeader()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected configureBucketRow(Lcom/google/android/finsky/layout/BucketRow;)V
    .locals 0
    .param p1, "row"    # Lcom/google/android/finsky/layout/BucketRow;

    .prologue
    .line 265
    return-void
.end method

.method public getCount()I
    .locals 6

    .prologue
    .line 125
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    int-to-double v2, v1

    iget v1, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mColumnCount:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 127
    .local v0, "count":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasBannerHeader()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    add-int/lit8 v0, v0, 0x1

    .line 131
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->hasPlainHeader()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    add-int/lit8 v0, v0, 0x1

    .line 134
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v1, v2, :cond_2

    .line 135
    add-int/lit8 v0, v0, 0x1

    .line 137
    :cond_2
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getListItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getListItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type for getView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getListItemViewType(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getBannerHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 110
    :goto_0
    return-object v0

    .line 104
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getPlainHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 106
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getPaginatedRowIndex(I)I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getPaginatedRow(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 108
    :pswitch_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getLoadingFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_4
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getErrorFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x5

    return v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method
