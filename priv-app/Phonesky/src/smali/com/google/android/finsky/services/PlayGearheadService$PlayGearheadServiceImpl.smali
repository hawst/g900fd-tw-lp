.class Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;
.super Lcom/google/android/finsky/services/IPlayGearheadService$Stub;
.source "PlayGearheadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/PlayGearheadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayGearheadServiceImpl"
.end annotation


# instance fields
.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

.field final synthetic this$0:Lcom/google/android/finsky/services/PlayGearheadService;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/services/PlayGearheadService;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/PackageStateRepository;)V
    .locals 0
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "packageStateRepository"    # Lcom/google/android/finsky/appstate/PackageStateRepository;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->this$0:Lcom/google/android/finsky/services/PlayGearheadService;

    invoke-direct {p0}, Lcom/google/android/finsky/services/IPlayGearheadService$Stub;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 56
    iput-object p3, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    .line 57
    return-void
.end method


# virtual methods
.method public validatePackageAcquiredByPlay(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 63
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v2, "result":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    invoke-interface {v4, p1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 68
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v1, :cond_0

    .line 70
    const-string v4, "Finsky.IsValid"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 83
    :goto_0
    return-object v2

    .line 75
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v4}, Lcom/google/android/finsky/library/Libraries;->blockingLoad()V

    .line 78
    iget-object v4, p0, Lcom/google/android/finsky/services/PlayGearheadService$PlayGearheadServiceImpl;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v5, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    iget-object v6, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 82
    .local v0, "appOwners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    const-string v4, "Finsky.IsValid"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method
