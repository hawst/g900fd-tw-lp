.class public Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardRateAndSuggestClusterView.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# instance fields
.field private mClusterDoc:Lcom/google/android/finsky/api/model/Document;

.field private mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

.field private mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

.field private mEmptySadface:Landroid/widget/TextView;

.field private mIndexOfItemToRate:I

.field private mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

.field private mShouldScrollRateCardOnDataLoad:Z

.field private mState:I

.field private mSuggestionsDismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mState:I

    .line 104
    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncIndexOfItemToRate(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    return-object v0
.end method

.method private clearRecommendedItemsList()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 402
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 403
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    .line 405
    :cond_0
    return-void
.end method

.method private handleErrorOrNoSuggestionsResponse()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 451
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 452
    .local v0, "ratedDoc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 453
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 455
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncIndexOfItemToRate(Z)V

    .line 456
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V

    .line 457
    return-void
.end method

.method public static isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z
    .locals 5
    .param p0, "cache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 193
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 194
    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 196
    .local v0, "childDoc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v2

    .line 204
    .local v2, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v2, :cond_2

    iget v4, v2, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    sget-object v3, Lcom/google/android/finsky/config/G;->positiveRateThreshold:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v4, v3, :cond_0

    .line 205
    :cond_2
    const/4 v3, 0x0

    .line 208
    .end local v0    # "childDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    :goto_1
    return v3

    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private setState(IZ)V
    .locals 2
    .param p1, "newState"    # I
    .param p2, "isDuringInitialLoading"    # Z

    .prologue
    .line 130
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mState:I

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;

    .line 133
    .local v0, "content":Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mState:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->setState(IZ)V

    .line 134
    return-void
.end method

.method private syncIndexOfItemToRate(Z)V
    .locals 12
    .param p1, "isDuringInitialLoading"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 144
    if-nez p1, :cond_0

    move v8, v9

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mShouldScrollRateCardOnDataLoad:Z

    .line 145
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v5

    .line 146
    .local v5, "mTotalItemCount":I
    :goto_1
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    if-ge v8, v5, :cond_3

    .line 147
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v8, v11}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 148
    .local v0, "candidate":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_1

    .line 149
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Got a null document at index "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v11, v10, [Ljava/lang/Object;

    invoke-static {v8, v11}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    goto :goto_1

    .end local v0    # "candidate":Lcom/google/android/finsky/api/model/Document;
    .end local v5    # "mTotalItemCount":I
    :cond_0
    move v8, v10

    .line 144
    goto :goto_0

    .line 154
    .restart local v0    # "candidate":Lcom/google/android/finsky/api/model/Document;
    .restart local v5    # "mTotalItemCount":I
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "candidateId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v8, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v7

    .line 156
    .local v7, "wasSkipped":Z
    if-eqz v7, :cond_2

    .line 158
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    goto :goto_1

    .line 163
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    const/4 v11, 0x0

    invoke-virtual {v8, v1, v11}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v3

    .line 164
    .local v3, "candidateReview":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-nez v3, :cond_4

    .line 186
    .end local v0    # "candidate":Lcom/google/android/finsky/api/model/Document;
    .end local v1    # "candidateId":Ljava/lang/String;
    .end local v3    # "candidateReview":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v7    # "wasSkipped":Z
    :cond_3
    return-void

    .line 169
    .restart local v0    # "candidate":Lcom/google/android/finsky/api/model/Document;
    .restart local v1    # "candidateId":Ljava/lang/String;
    .restart local v3    # "candidateReview":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .restart local v7    # "wasSkipped":Z
    :cond_4
    iget v2, v3, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 170
    .local v2, "candidateRating":I
    sget-object v8, Lcom/google/android/finsky/config/G;->positiveRateThreshold:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ge v2, v8, :cond_5

    .line 172
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    goto :goto_1

    .line 175
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSuggestForRatingSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v6

    .line 176
    .local v6, "suggestions":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    if-eqz v6, :cond_6

    iget-object v8, v6, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    move v4, v9

    .line 178
    .local v4, "hasSuggestions":Z
    :goto_2
    if-nez v4, :cond_3

    .line 180
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    goto :goto_1

    .end local v4    # "hasSuggestions":Z
    :cond_6
    move v4, v10

    .line 176
    goto :goto_2
.end method

.method private syncState(Z)V
    .locals 12
    .param p1, "isDuringInitialLoading"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 223
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildCount()I

    move-result v9

    if-nez v9, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mState:I

    .line 231
    .local v2, "oldState":I
    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v10

    if-lt v9, v10, :cond_3

    .line 232
    const/4 v9, 0x3

    invoke-direct {p0, v9, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V

    .line 233
    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mState:I

    if-eq v2, v9, :cond_0

    .line 234
    if-nez p1, :cond_2

    :goto_1
    invoke-direct {p0, v7}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->transitionToEmptyState(Z)V

    goto :goto_0

    :cond_2
    move v7, v8

    goto :goto_1

    .line 239
    :cond_3
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 240
    .local v0, "currentItemToRate":Lcom/google/android/finsky/api/model/Document;
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v5

    .line 243
    .local v5, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v5, :cond_5

    .line 248
    const/4 v7, 0x2

    invoke-direct {p0, v7, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V

    .line 249
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v7, v9}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getSuggestForRatingSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v6

    .line 251
    .local v6, "suggestions":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    if-eqz p1, :cond_4

    .line 252
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->hideRateCard()V

    .line 256
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v9, v6, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    iget-object v10, v6, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v7, v9, v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->replaceTitles(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 259
    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->clearRecommendedItemsList()V

    .line 260
    new-instance v7, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v10, v6, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    invoke-direct {v7, v9, v10, v8}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    .line 261
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v7, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 262
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v7, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 263
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    goto :goto_0

    .line 268
    .end local v6    # "suggestions":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    :cond_5
    invoke-direct {p0, v8, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V

    .line 270
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    new-array v7, v7, [Lcom/google/android/finsky/api/model/Document;

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v10

    aput-object v10, v7, v8

    invoke-static {v7}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .line 275
    .local v3, "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v7

    invoke-static {v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v4

    .line 277
    .local v4, "rateContentAspectRatio":F
    if-nez p1, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-wide/16 v8, 0x96

    new-instance v10, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;

    invoke-direct {v10, p0, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;F)V

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 307
    .local v1, "fadeOutAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v3, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method private transitionToEmptyState(Z)V
    .locals 8
    .param p1, "toAnimate"    # Z

    .prologue
    const-wide/16 v6, 0xfa

    const/4 v4, 0x0

    .line 521
    if-nez p1, :cond_0

    .line 522
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->setVisibility(I)V

    .line 523
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    :goto_0
    return-void

    .line 528
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)V

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 541
    .local v1, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->startAnimation(Landroid/view/animation/Animation;)V

    .line 542
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 544
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 547
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 12
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p7, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 348
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 350
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncIndexOfItemToRate(Z)V

    .line 352
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 353
    .local v10, "passThrough":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 354
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)V

    .line 361
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mSuggestionsDismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object v7, p0

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 362
    invoke-super/range {v1 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 365
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V

    .line 367
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .line 368
    .local v11, "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)V

    invoke-virtual {v11, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setRateListener(Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;)V

    .line 386
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setState(I)V

    .line 387
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 109
    const/16 v0, 0x19d

    return v0
.end method

.method public onDataChanged()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    .line 461
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v4

    .line 462
    .local v4, "recommendedItemsCount":I
    if-nez v4, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->handleErrorOrNoSuggestionsResponse()V

    .line 518
    :goto_0
    return-void

    .line 468
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 469
    .local v1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 471
    .local v2, "extraDocCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 472
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mRecommendedItems:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6, v3}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 476
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)V

    .line 478
    const/4 v3, 0x1

    :goto_2
    if-gt v3, v2, :cond_2

    .line 479
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v6, v3, v3, p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->bindCardAt(IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    .line 478
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 483
    :cond_2
    add-int/lit8 v3, v2, 0x1

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildCount()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 484
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v6, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->hideCardAt(I)V

    .line 483
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 491
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mIndexOfItemToRate:I

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 492
    .local v0, "docToRate":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSuggestForRatingSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v5

    .line 493
    .local v5, "suggestions":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mShouldScrollRateCardOnDataLoad:Z

    if-eqz v6, :cond_5

    .line 498
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v6, v6, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getWidth()I

    move-result v6

    if-gt v6, v10, :cond_4

    .line 499
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->scrollAwayRateCard()V

    .line 500
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v7, v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    iget-object v8, v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->replaceTitles(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 517
    :goto_4
    const/4 v6, 0x0

    invoke-direct {p0, v10, v6}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V

    goto :goto_0

    .line 503
    :cond_4
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v7, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;

    invoke-direct {v7, p0, v5}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;)V

    const-wide/16 v8, 0x2ee

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4

    .line 514
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->hideRateCard()V

    .line 515
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v7, v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    iget-object v8, v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->replaceTitles(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->hasCards()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .line 393
    .local v0, "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setRateListener(Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;)V

    .line 395
    .end local v0    # "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->clearRecommendedItemsList()V

    .line 396
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onDetachedFromWindow()V

    .line 397
    return-void
.end method

.method public onDismissDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    const/4 v2, 0x0

    .line 312
    invoke-virtual {p2}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardType()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 315
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncIndexOfItemToRate(Z)V

    .line 316
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mSuggestionsDismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardDismissListener;->onDismissDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->handleErrorOrNoSuggestionsResponse()V

    .line 446
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onFinishInflate()V

    .line 116
    const v0, 0x7f0a02d6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    .line 118
    const v0, 0x7f0a02d5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    .line 119
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getWidth()I

    move-result v2

    .line 430
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getPaddingTop()I

    move-result v3

    .line 432
    .local v3, "y":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 433
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v4, v6, v3, v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    .line 434
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 437
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->getMeasuredHeight()I

    move-result v0

    .line 438
    .local v0, "contentScrollerHeight":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    add-int v5, v3, v0

    invoke-virtual {v4, v6, v3, v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->layout(IIII)V

    .line 439
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    .line 440
    .local v1, "sadfaceY":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v4, v6, v1, v2, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 441
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v4, 0x0

    .line 409
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 410
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getPaddingBottom()I

    move-result v3

    add-int v0, v2, v3

    .line 412
    .local v0, "height":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 413
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2, p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->measure(II)V

    .line 414
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 417
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v2, p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->measure(II)V

    .line 418
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 420
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mEmptySadface:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->getMeasuredHeight()I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 424
    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setMeasuredDimension(II)V

    .line 425
    return-void
.end method

.method public setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V
    .locals 0
    .param p1, "clusterFadeOutListener"    # Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .line 127
    return-void
.end method

.method public withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .locals 2
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    .line 333
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setClusterLoggingDocument(Lcom/google/android/finsky/api/model/Document;)V

    .line 334
    return-object p0
.end method

.method public withLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .locals 2
    .param p2, "parentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterView;"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "looseDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This cluster does not support loose data"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
