.class public Lcom/google/android/finsky/adapters/CategoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "CategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mBackendId:I

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mLeadingExtraSpacerHeight:I

.field private final mLeadingSpacerHeight:I

.field private final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mNumQuickLinksPerRow:I

.field private final mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mQuickLinkRowCount:I

.field private final mQuickLinkRowPadding:I

.field private final mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field private final mSubheaderTitle:Ljava/lang/String;

.field private final mTextColor:Landroid/content/res/ColorStateList;

.field private final mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Browse$BrowseLink;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categories"    # [Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "backendId"    # I
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p7, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p8, "subheaderTitle"    # Ljava/lang/String;
    .param p9, "tabMode"    # I
    .param p10, "parent"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 64
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 65
    iput-object p2, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 66
    iput-object p3, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 67
    iput p4, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mBackendId:I

    .line 68
    invoke-static {p1, p4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mTextColor:Landroid/content/res/ColorStateList;

    .line 69
    iput-object p5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 70
    iput-object p6, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 71
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mSubheaderTitle:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 76
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e001b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float v2, v5, v6

    .line 78
    .local v2, "categoryPageWidthInPercents":F
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    float-to-double v6, v2

    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNumQuickLinksPerRow:I

    .line 80
    iput-object p7, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 81
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    array-length v3, v5

    .line 82
    .local v3, "quickLinkCount":I
    :goto_0
    iget v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNumQuickLinksPerRow:I

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    .line 83
    const v5, 0x7f0b0092

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowPadding:I

    .line 85
    const/4 v5, 0x0

    move/from16 v0, p9

    invoke-static {p1, v0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLeadingSpacerHeight:I

    .line 86
    const v5, 0x7f0b0176

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLeadingExtraSpacerHeight:I

    .line 88
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 89
    return-void

    .line 81
    .end local v3    # "quickLinkCount":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/CategoryAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CategoryAdapter;

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mBackendId:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/CategoryAdapter;)Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CategoryAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/adapters/CategoryAdapter;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CategoryAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method private getCategoryRowView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 150
    .local v0, "category":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-nez p2, :cond_0

    .line 151
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04004b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 154
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;

    .line 156
    .local v2, "holder":Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;
    if-nez v2, :cond_1

    .line 157
    new-instance v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;

    .end local v2    # "holder":Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;
    invoke-direct {v2}, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;-><init>()V

    .line 158
    .restart local v2    # "holder":Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;
    const v3, 0x7f0a012c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 159
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 162
    :cond_1
    iget-object v3, v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 163
    iget-object v3, v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const/16 v3, 0x64

    iget-object v4, v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->reset(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 173
    invoke-virtual {v2}, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->reportImpression()V

    .line 175
    move-object v1, v2

    .line 176
    .local v1, "finalHolder":Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;
    new-instance v3, Lcom/google/android/finsky/adapters/CategoryAdapter$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/finsky/adapters/CategoryAdapter$1;-><init>(Lcom/google/android/finsky/adapters/CategoryAdapter;Lcom/google/android/finsky/protos/Browse$BrowseLink;Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v3, v2, Lcom/google/android/finsky/adapters/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    return-object p2
.end method

.method private getExtraLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 120
    if-nez p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04014d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 124
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLeadingExtraSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 125
    return-object p1
.end method

.method private getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 110
    if-nez p1, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04014d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 114
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 115
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 116
    return-object p1
.end method

.method private getQuickLinksRowView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 11
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object v4, p1

    check-cast v4, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    add-int/lit8 v7, p3, -0x2

    iget v8, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mNumQuickLinksPerRow:I

    iget-object v9, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v5, p2

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/adapters/QuickLinkHelper;->getQuickLinksRow(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/LayoutInflater;Lcom/google/android/play/image/BitmapLoader;Landroid/view/ViewGroup;Landroid/view/ViewGroup;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View;

    move-result-object v10

    .line 132
    .local v10, "quickLinksRow":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowPadding:I

    add-int/2addr v0, v1

    invoke-virtual {v10}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v10}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowPadding:I

    add-int/2addr v2, v3

    invoke-virtual {v10}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 136
    return-object v10
.end method

.method private getSubheaderRowView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04004c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_0
    move-object v0, p1

    .line 143
    check-cast v0, Landroid/widget/TextView;

    .line 144
    .local v0, "subheader":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mSubheaderTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    return-object v0
.end method

.method private isQuickLinkRow(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 240
    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSubheaderRow(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 244
    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    add-int/2addr v1, v0

    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 231
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 207
    :goto_0
    return v0

    .line 195
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 197
    if-nez p1, :cond_1

    .line 198
    const/4 v0, 0x1

    goto :goto_0

    .line 200
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 202
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->isQuickLinkRow(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    const/4 v0, 0x3

    goto :goto_0

    .line 204
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->isSubheaderRow(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    const/4 v0, 0x4

    goto :goto_0

    .line 207
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 106
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 97
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getExtraLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 99
    :pswitch_2
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getQuickLinksRowView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 101
    :pswitch_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getSubheaderRowView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 103
    :pswitch_4
    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    add-int/lit8 v1, v0, 0x2

    iget v0, p0, Lcom/google/android/finsky/adapters/CategoryAdapter;->mQuickLinkRowCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v1

    sub-int/2addr p1, v0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getCategoryRowView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x5

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 236
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CategoryAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
