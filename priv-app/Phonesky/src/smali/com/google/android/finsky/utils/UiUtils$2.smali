.class final Lcom/google/android/finsky/utils/UiUtils$2;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->playShakeAnimationIfPossible(Landroid/content/Context;Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$shakeDelta:F


# direct methods
.method constructor <init>(F)V
    .locals 0

    .prologue
    .line 264
    iput p1, p0, Lcom/google/android/finsky/utils/UiUtils$2;->val$shakeDelta:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 2
    .param p1, "input"    # F

    .prologue
    .line 267
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p1

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/finsky/utils/UiUtils$2;->val$shakeDelta:F

    mul-float/2addr v0, v1

    return v0
.end method
