.class Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardAvailablePromoOfferActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

.field final synthetic val$noActionMessage:Ljava/lang/String;

.field final synthetic val$radioGroup:Landroid/widget/RadioGroup;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Landroid/widget/RadioGroup;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->this$0:Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->val$radioGroup:Landroid/widget/RadioGroup;

    iput-object p3, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->val$noActionMessage:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->val$radioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0a0381

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->this$0:Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

    # invokes: Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->handleOfferContinued()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->access$000(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;)V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->this$0:Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;->val$noActionMessage:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->handleOfferSkipped(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->access$100(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
