.class Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;
.super Ljava/lang/Object;
.source "DownloadManagerLegacyImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->enqueue(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/ParameterizedRunnable;

.field final synthetic val$request:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;Landroid/content/ContentValues;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->this$0:Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;

    iput-object p2, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$request:Landroid/content/ContentValues;

    iput-object p3, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$listener:Lcom/google/android/finsky/utils/ParameterizedRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 177
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->this$0:Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->access$200(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;)Landroid/content/ContentResolver;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->CONTENT_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->access$100()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$request:Landroid/content/ContentValues;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 178
    .local v1, "uri":Landroid/net/Uri;
    # invokes: Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sniffDownloadManagerVersion(Landroid/net/Uri;)V
    invoke-static {v1}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->access$300(Landroid/net/Uri;)V

    .line 179
    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$listener:Lcom/google/android/finsky/utils/ParameterizedRunnable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$listener:Lcom/google/android/finsky/utils/ParameterizedRunnable;

    invoke-interface {v2, v1}, Lcom/google/android/finsky/utils/ParameterizedRunnable;->run(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "Unable to insert download request for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;->val$request:Landroid/content/ContentValues;

    invoke-virtual {v5}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
