.class Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;
.super Ljava/lang/Object;
.source "CardRecyclerViewAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

.field final synthetic val$containerViews:[Lcom/google/android/finsky/protos/Containers$ContainerView;

.field final synthetic val$spinner:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Landroid/widget/Spinner;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V
    .locals 0

    .prologue
    .line 1733
    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->val$spinner:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->val$containerViews:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x1

    .line 1736
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->val$spinner:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v5

    invoke-interface {v5, p3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 1738
    .local v3, "item":Lcom/google/android/finsky/protos/Containers$ContainerView;
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->val$spinner:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    iget-boolean v5, v3, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    if-nez v5, :cond_2

    .line 1740
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    const/16 v7, 0xf9

    iget-object v8, v3, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    iget-object v9, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v9, v9, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v5, v7, v8, v9}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1745
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v5, v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1747
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->val$containerViews:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    .line 1748
    .local v1, "containerView":Lcom/google/android/finsky/protos/Containers$ContainerView;
    if-ne v1, v3, :cond_0

    move v5, v6

    :goto_1
    iput-boolean v5, v1, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    .line 1749
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasSelected:Z

    .line 1747
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1748
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 1751
    .end local v1    # "containerView":Lcom/google/android/finsky/protos/Containers$ContainerView;
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v5, v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1752
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v5, v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    iget-object v6, v3, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/api/model/ContainerList;->clearDataAndReplaceInitialUrl(Ljava/lang/String;)V

    .line 1753
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v5, v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/ContainerList;->startLoadItems()V

    .line 1755
    .end local v0    # "arr$":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1759
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
