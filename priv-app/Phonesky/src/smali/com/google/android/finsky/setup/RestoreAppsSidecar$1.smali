.class Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;
.super Ljava/lang/Object;
.source "RestoreAppsSidecar.java"

# interfaces
.implements Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/RestoreAppsSidecar;->fetchBackupDocs(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

.field final synthetic val$androidId:J


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar;J)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    iput-wide p2, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->val$androidId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Ljava/lang/String;)V
    .locals 7
    .param p1, "checkinConsistencyToken"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    # getter for: Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;
    invoke-static {v0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->access$300(Lcom/google/android/finsky/setup/RestoreAppsSidecar;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->val$androidId:J

    new-instance v5, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;-><init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;)V

    new-instance v6, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;-><init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;)V

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Lcom/google/android/finsky/api/DfeApi;->getBackupDocumentChoices(JLjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 88
    return-void
.end method
