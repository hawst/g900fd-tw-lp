.class public final Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateInstrumentFlowOption"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;


# instance fields
.field public apiMaxVersion:I

.field public apiMinVersion:I

.field public hasApiMaxVersion:Z

.field public hasApiMinVersion:Z

.field public initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->clear()Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    .line 189
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->_emptyArray:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    if-nez v0, :cond_1

    .line 166
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->_emptyArray:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    sput-object v0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->_emptyArray:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    .line 171
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->_emptyArray:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    return-object v0

    .line 171
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    .line 193
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    .line 194
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMinVersion:Z

    .line 195
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    .line 196
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMaxVersion:Z

    .line 197
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->cachedSize:I

    .line 198
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 218
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 219
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    if-eqz v1, :cond_0

    .line 220
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMinVersion:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    if-eqz v1, :cond_2

    .line 224
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMaxVersion:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    if-eqz v1, :cond_4

    .line 228
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 240
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 244
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    :sswitch_0
    return-object p0

    .line 250
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    if-nez v1, :cond_1

    .line 251
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    .line 253
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 257
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    .line 258
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMinVersion:Z

    goto :goto_0

    .line 262
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    .line 263
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMaxVersion:Z

    goto :goto_0

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->initialFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 207
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMinVersion:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    if-eqz v0, :cond_2

    .line 208
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMinVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 210
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->hasApiMaxVersion:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    if-eqz v0, :cond_4

    .line 211
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->apiMaxVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 213
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 214
    return-void
.end method
