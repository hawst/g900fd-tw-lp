.class public Lcom/google/android/finsky/layout/play/PlayAvatarPack;
.super Landroid/view/ViewGroup;
.source "PlayAvatarPack.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;


# instance fields
.field private mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

.field private mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

.field private final mPrimarySize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    sget-object v1, Lcom/android/vending/R$styleable;->PlayAvatarPack:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0165

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    return-void
.end method

.method private makeFifeImageView()Lcom/google/android/play/image/FifeImageView;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 166
    new-instance v0, Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;)V

    .line 167
    .local v0, "result":Lcom/google/android/play/image/FifeImageView;
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setBitmapTransformation(Lcom/google/android/play/image/BitmapTransformation;)V

    .line 169
    invoke-virtual {v0, v2}, Lcom/google/android/play/image/FifeImageView;->setHasFixedBounds(Z)V

    .line 170
    invoke-virtual {v0, v2}, Lcom/google/android/play/image/FifeImageView;->setFocusable(Z)V

    .line 171
    return-object v0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 27
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getWidth()I

    move-result v23

    .line 212
    .local v23, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getHeight()I

    move-result v12

    .line 213
    .local v12, "height":I
    div-int/lit8 v5, v23, 0x2

    .line 214
    .local v5, "centerX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v20, v5, v24

    .line 215
    .local v20, "primaryLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    move/from16 v24, v0

    add-int v21, v20, v24

    .line 217
    .local v21, "primaryRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    move/from16 v26, v0

    move-object/from16 v0, v24

    move/from16 v1, v20

    move/from16 v2, v25

    move/from16 v3, v21

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 221
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    if-nez v24, :cond_1

    const/16 v22, 0x0

    .line 222
    .local v22, "secondaryCount":I
    :goto_0
    if-lez v22, :cond_2

    .line 228
    move/from16 v9, v20

    .line 229
    .local v9, "evenRightMarker":I
    move/from16 v17, v21

    .line 230
    .local v17, "oddLeftMarker":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    move/from16 v0, v22

    if-ge v13, v0, :cond_2

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    aget-object v6, v24, v13

    .line 232
    .local v6, "even":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v11

    .line 233
    .local v11, "evenWidth":I
    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v7

    .line 234
    .local v7, "evenHeight":I
    int-to-float v0, v11

    move/from16 v24, v0

    const v25, 0x3ea8f5c3    # 0.33f

    mul-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    add-int v8, v9, v24

    .line 235
    .local v8, "evenRight":I
    sub-int v24, v12, v7

    div-int/lit8 v10, v24, 0x2

    .line 236
    .local v10, "evenTop":I
    sub-int v24, v8, v11

    add-int v25, v10, v7

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v10, v8, v1}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 237
    sub-int v9, v8, v11

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    add-int/lit8 v25, v13, 0x1

    aget-object v14, v24, v25

    .line 240
    .local v14, "odd":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v14}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v19

    .line 241
    .local v19, "oddWidth":I
    invoke-virtual {v14}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v15

    .line 242
    .local v15, "oddHeight":I
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v24, v0

    const v25, 0x3ea8f5c3    # 0.33f

    mul-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    sub-int v16, v17, v24

    .line 243
    .local v16, "oddLeft":I
    sub-int v24, v12, v15

    div-int/lit8 v18, v24, 0x2

    .line 244
    .local v18, "oddTop":I
    add-int v24, v16, v19

    add-int v25, v18, v15

    move/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 245
    add-int v17, v16, v19

    .line 230
    add-int/lit8 v13, v13, 0x2

    goto :goto_1

    .line 221
    .end local v6    # "even":Lcom/google/android/play/image/FifeImageView;
    .end local v7    # "evenHeight":I
    .end local v8    # "evenRight":I
    .end local v9    # "evenRightMarker":I
    .end local v10    # "evenTop":I
    .end local v11    # "evenWidth":I
    .end local v13    # "i":I
    .end local v14    # "odd":Lcom/google/android/play/image/FifeImageView;
    .end local v15    # "oddHeight":I
    .end local v16    # "oddLeft":I
    .end local v17    # "oddLeftMarker":I
    .end local v18    # "oddTop":I
    .end local v19    # "oddWidth":I
    .end local v22    # "secondaryCount":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v22, v0

    goto/16 :goto_0

    .line 248
    .restart local v22    # "secondaryCount":I
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const v10, 0x3f2b851e    # 0.66999996f

    .line 176
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 178
    .local v7, "width":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    if-eqz v8, :cond_0

    .line 179
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 180
    .local v1, "primarySpec":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8, v1, v1}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 183
    .end local v1    # "primarySpec":I
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    if-nez v8, :cond_2

    const/4 v2, 0x0

    .line 184
    .local v2, "secondaryCount":I
    :goto_0
    if-lez v2, :cond_3

    .line 186
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    sub-int v8, v7, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getPaddingRight()I

    move-result v9

    sub-int v6, v8, v9

    .line 188
    .local v6, "spaceLeft":I
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    int-to-float v8, v8

    const v9, 0x3f333333    # 0.7f

    mul-float/2addr v8, v9

    float-to-int v3, v8

    .line 191
    .local v3, "secondarySize":I
    int-to-float v8, v3

    mul-float/2addr v8, v10

    float-to-int v4, v8

    .line 193
    .local v4, "secondarySizeNonOverlapping":I
    mul-int v8, v2, v4

    if-le v8, v6, :cond_1

    .line 195
    div-int v4, v6, v2

    .line 197
    int-to-float v8, v4

    div-float/2addr v8, v10

    float-to-int v3, v8

    .line 200
    :cond_1
    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 201
    .local v5, "secondarySpec":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 202
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v8, v8, v0

    invoke-virtual {v8, v5, v5}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 183
    .end local v0    # "i":I
    .end local v2    # "secondaryCount":I
    .end local v3    # "secondarySize":I
    .end local v4    # "secondarySizeNonOverlapping":I
    .end local v5    # "secondarySpec":I
    .end local v6    # "spaceLeft":I
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    array-length v2, v8

    goto :goto_0

    .line 206
    .restart local v2    # "secondaryCount":I
    :cond_3
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mPrimarySize:I

    invoke-virtual {p0, v7, v8}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->setMeasuredDimension(II)V

    .line 207
    return-void
.end method

.method public onRecycle()V
    .locals 2

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    if-eqz v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v1}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    if-eqz v1, :cond_2

    .line 255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 256
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 255
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method public setData(Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 14
    .param p1, "primaryPerson"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p2, "secondaryPersons"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    const/16 v9, 0x8

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->setVisibility(I)V

    .line 163
    :goto_0
    return-void

    .line 77
    :cond_0
    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->setVisibility(I)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->removeAllViews()V

    .line 84
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v1

    .line 85
    .local v1, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 89
    .local v7, "res":Landroid/content/res/Resources;
    if-nez p2, :cond_1

    const/4 v8, 0x0

    .line 91
    .local v8, "secondaryCount":I
    :goto_1
    new-array v9, v8, [Lcom/google/android/play/image/FifeImageView;

    iput-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    .line 92
    if-lez v8, :cond_3

    .line 94
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v8, :cond_2

    .line 95
    aget-object v2, p2, v3

    .line 96
    .local v2, "finalPerson":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    const/4 v9, 0x4

    invoke-static {v2, v9}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v4

    .line 98
    .local v4, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->makeFifeImageView()Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    aput-object v10, v9, v3

    .line 99
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v9, v9, v3

    iget-object v10, v4, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v11, v4, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v9, v10, v11, v1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 101
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v9, v9, v3

    const v10, 0x7f0c03b3

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 106
    new-instance v5, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v9, 0x117

    iget-object v10, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-direct {v5, v9, v10, v11, v0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 109
    .local v5, "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 111
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    aget-object v9, v9, v3

    new-instance v10, Lcom/google/android/finsky/layout/play/PlayAvatarPack$1;

    move-object/from16 v0, p3

    invoke-direct {v10, p0, v0, v2, v5}, Lcom/google/android/finsky/layout/play/PlayAvatarPack$1;-><init>(Lcom/google/android/finsky/layout/play/PlayAvatarPack;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {v9, v10}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 89
    .end local v2    # "finalPerson":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v3    # "i":I
    .end local v4    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v5    # "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .end local v8    # "secondaryCount":I
    :cond_1
    const/4 v9, 0x4

    move-object/from16 v0, p2

    array-length v10, v0

    const/4 v11, 0x2

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/IntMath;->floor(II)I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto :goto_1

    .line 126
    .restart local v3    # "i":I
    .restart local v8    # "secondaryCount":I
    :cond_2
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v8, :cond_3

    .line 127
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarsSecondary:[Lcom/google/android/play/image/FifeImageView;

    sub-int v10, v8, v3

    add-int/lit8 v10, v10, -0x1

    aget-object v9, v9, v10

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->addView(Landroid/view/View;)V

    .line 126
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 133
    .end local v3    # "i":I
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->makeFifeImageView()Lcom/google/android/play/image/FifeImageView;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    .line 134
    const/4 v9, 0x4

    invoke-static {p1, v9}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v4

    .line 135
    .restart local v4    # "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    iget-object v10, v4, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v11, v4, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v9, v10, v11, v1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 136
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    const v10, 0x7f0c03b3

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    new-instance v5, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v9, 0x117

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-direct {v5, v9, v10, v11, v0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 144
    .restart local v5    # "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 149
    new-instance v6, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v6, p1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 150
    .local v6, "primaryDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-static {v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 151
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :goto_4
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 154
    :cond_4
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->mAvatarPrimary:Lcom/google/android/play/image/FifeImageView;

    new-instance v10, Lcom/google/android/finsky/layout/play/PlayAvatarPack$2;

    move-object/from16 v0, p3

    invoke-direct {v10, p0, v0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack$2;-><init>(Lcom/google/android/finsky/layout/play/PlayAvatarPack;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    invoke-virtual {v9, v10}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method
