.class Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;
.super Ljava/lang/Object;
.source "SetupWizardFinalHoldActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RestoringTitleAnimator"
.end annotation


# static fields
.field private static final ELLIPSES:[I


# instance fields
.field private mCallback:Ljava/lang/Runnable;

.field private mEllipseView:Landroid/widget/TextView;

.field private mPosition:I

.field private mRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->ELLIPSES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0c03f3
        0x7f0c03f4
        0x7f0c03f5
    .end array-data
.end method

.method public constructor <init>(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "ellipseView"    # Landroid/widget/TextView;

    .prologue
    const/4 v0, 0x0

    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mPosition:I

    .line 359
    iput-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mRunning:Z

    .line 361
    new-instance v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator$1;-><init>(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;)V

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mCallback:Ljava/lang/Runnable;

    .line 372
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    .line 373
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->nextStep()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mRunning:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    return-object v0
.end method

.method private nextStep()V
    .locals 3

    .prologue
    .line 376
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mPosition:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->ELLIPSES:[I

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mPosition:I

    .line 377
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mPosition:I

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 383
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->ELLIPSES:[I

    iget v2, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mPosition:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public start()V
    .locals 4

    .prologue
    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mRunning:Z

    .line 387
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mCallback:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 389
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mRunning:Z

    .line 393
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mEllipseView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->mCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 394
    return-void
.end method
