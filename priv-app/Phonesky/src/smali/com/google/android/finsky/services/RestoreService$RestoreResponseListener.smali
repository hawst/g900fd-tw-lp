.class Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/RestoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RestoreResponseListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V
    .locals 0
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 1442
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1443
    iput-object p2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->mAccountName:Ljava/lang/String;

    .line 1444
    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;)V
    .locals 26
    .param p1, "response"    # Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .prologue
    .line 1449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I
    invoke-static {v2, v5}, Lcom/google/android/finsky/services/RestoreService;->access$2402(Lcom/google/android/finsky/services/RestoreService;I)I

    .line 1450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I
    invoke-static {v2, v5}, Lcom/google/android/finsky/services/RestoreService;->access$2502(Lcom/google/android/finsky/services/RestoreService;I)I

    .line 1451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I
    invoke-static {v2, v5}, Lcom/google/android/finsky/services/RestoreService;->access$2602(Lcom/google/android/finsky/services/RestoreService;I)I

    .line 1452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I
    invoke-static {v2, v5}, Lcom/google/android/finsky/services/RestoreService;->access$2702(Lcom/google/android/finsky/services/RestoreService;I)I

    .line 1453
    const/16 v25, 0x0

    .line 1455
    .local v25, "numDeferred":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v11

    .line 1457
    .local v11, "installer":Lcom/google/android/finsky/receivers/Installer;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-object/from16 v19, v0

    .local v19, "arr$":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_4

    aget-object v20, v19, v21

    .line 1459
    .local v20, "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v3, v2, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 1460
    .local v3, "packageName":Ljava/lang/String;
    move-object/from16 v0, v20

    iget v4, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    .line 1461
    .local v4, "packageVersion":I
    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    .line 1464
    .local v6, "packageTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->mAccountName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->shouldRestore(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z
    invoke-static {v2, v3, v4, v5, v11}, Lcom/google/android/finsky/services/RestoreService;->access$2800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1457
    :cond_0
    :goto_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 1469
    :cond_1
    const/4 v7, 0x3

    .line 1470
    .local v7, "priority":I
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, v20

    iget v2, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    const/16 v5, 0x64

    if-ge v2, v5, :cond_2

    .line 1471
    const/4 v7, 0x1

    .line 1474
    :cond_2
    const/4 v10, 0x0

    .line 1475
    .local v10, "appIconUrl":Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_3

    .line 1476
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v10, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1480
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->mAccountName:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->startPackage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    .line 1486
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-interface {v11, v3, v2, v5, v8}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 1487
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->mAccountName:Ljava/lang/String;

    const/16 v16, 0x1

    const-string v17, "restore"

    move-object v12, v3

    move v13, v4

    move-object v15, v6

    move/from16 v18, v7

    invoke-interface/range {v11 .. v18}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 1495
    add-int/lit8 v25, v25, 0x1

    .line 1498
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1499
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v10}, Lcom/google/android/finsky/services/RestoreService;->access$1700(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1503
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "packageVersion":I
    .end local v6    # "packageTitle":Ljava/lang/String;
    .end local v7    # "priority":I
    .end local v10    # "appIconUrl":Ljava/lang/String;
    .end local v20    # "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_4
    const-string v2, "Attempted to restore %d assets."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1504
    const-string v2, "  Skipped (already tracked): %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService;->access$2400(Lcom/google/android/finsky/services/RestoreService;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1505
    const-string v2, "  Skipped (other account): %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService;->access$2500(Lcom/google/android/finsky/services/RestoreService;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1506
    const-string v2, "  Skipped (attempts exceeded): %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService;->access$2700(Lcom/google/android/finsky/services/RestoreService;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1507
    const-string v2, "  Skipped (already installed): %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService;->access$2600(Lcom/google/android/finsky/services/RestoreService;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1523
    if-lez v25, :cond_5

    .line 1524
    const-string v2, "  Posted for deferred download/install: %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1525
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->setupWizardStartDownloads:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1528
    invoke-interface {v11}, Lcom/google/android/finsky/receivers/Installer;->startDeferredInstalls()V

    .line 1539
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->mAccountName:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v2, v5, v8, v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishAccount(Ljava/lang/String;ZI)V

    .line 1540
    return-void

    .line 1531
    :cond_6
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v2, v5, :cond_7

    .line 1532
    sget-object v2, Lcom/google/android/finsky/config/G;->appRestoreFailsafeMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .line 1536
    .local v22, "holdoff":J
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->this$0:Lcom/google/android/finsky/services/RestoreService;

    invoke-virtual {v5}, Lcom/google/android/finsky/services/RestoreService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    # invokes: Lcom/google/android/finsky/services/RestoreService;->getKickIntent(Landroid/content/Context;)Landroid/content/Intent;
    invoke-static {v5}, Lcom/google/android/finsky/services/RestoreService;->access$2900(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    move-wide/from16 v0, v22

    # invokes: Lcom/google/android/finsky/services/RestoreService;->setAlarm(Landroid/content/Intent;J)J
    invoke-static {v2, v5, v0, v1}, Lcom/google/android/finsky/services/RestoreService;->access$1600(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;J)J

    goto :goto_2

    .line 1534
    .end local v22    # "holdoff":J
    :cond_7
    sget-object v2, Lcom/google/android/finsky/config/G;->appRestoreHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .restart local v22    # "holdoff":J
    goto :goto_3
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1439
    check-cast p1, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;->onResponse(Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;)V

    return-void
.end method
