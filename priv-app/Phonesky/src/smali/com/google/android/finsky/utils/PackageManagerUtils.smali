.class public Lcom/google/android/finsky/utils/PackageManagerUtils;
.super Ljava/lang/Object;
.source "PackageManagerUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PackageManagerUtils$FreeSpaceListener;,
        Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;
    }
.end annotation


# direct methods
.method public static freeStorageAndNotify(Landroid/content/Context;JLcom/google/android/finsky/utils/PackageManagerUtils$FreeSpaceListener;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requiredSize"    # J
    .param p3, "listener"    # Lcom/google/android/finsky/utils/PackageManagerUtils$FreeSpaceListener;

    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 64
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v1, Lcom/google/android/finsky/utils/PackageManagerUtils$2;

    invoke-direct {v1, p3}, Lcom/google/android/finsky/utils/PackageManagerUtils$2;-><init>(Lcom/google/android/finsky/utils/PackageManagerUtils$FreeSpaceListener;)V

    invoke-virtual {v0, p1, p2, v1}, Landroid/content/pm/PackageManager;->freeStorageAndNotify(JLandroid/content/pm/IPackageDataObserver;)V

    .line 70
    return-void
.end method

.method public static installExistingPackage(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 45
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->installExistingPackage(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static installPackage(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "observer"    # Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;
    .param p3, "installFlags"    # I

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/finsky/utils/PackageManagerUtils$1;

    invoke-direct {v0, p2}, Lcom/google/android/finsky/utils/PackageManagerUtils$1;-><init>(Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;)V

    .line 34
    .local v0, "innerObserver":Landroid/content/pm/IPackageInstallObserver;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 35
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const-string v2, "com.android.vending"

    invoke-virtual {v1, p1, v0, p3, v2}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 36
    return-void
.end method

.method public static uninstallPackage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 86
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    .line 87
    return-void
.end method
