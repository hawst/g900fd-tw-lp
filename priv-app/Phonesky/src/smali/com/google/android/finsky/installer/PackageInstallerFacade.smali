.class public interface abstract Lcom/google/android/finsky/installer/PackageInstallerFacade;
.super Ljava/lang/Object;
.source "PackageInstallerFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;
    }
.end annotation


# virtual methods
.method public abstract cancelSession(Ljava/lang/String;)V
.end method

.method public abstract createSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
.end method

.method public abstract getAppIconSize()I
.end method

.method public abstract hasSession(Ljava/lang/String;)Z
.end method

.method public abstract install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
.end method

.method public abstract pruneSessions(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract reportProgress(Ljava/lang/String;JJ)V
.end method

.method public abstract setAppIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end method

.method public abstract uninstallPackage(Ljava/lang/String;)V
.end method
