.class Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;
.super Ljava/lang/Object;
.source "FinskyDrawerLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->makeMyCollectionAction(I)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

.field final synthetic val$backendId:I

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;ILandroid/content/Context;)V
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    iput p2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    iput-object p3, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 439
    iget v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    packed-switch v2, :pswitch_data_0

    .line 453
    :pswitch_0
    const/16 v0, 0x67

    .line 456
    .local v0, "clickType":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # invokes: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->logMenuClickEvent(I)V
    invoke-static {v2, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$500(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;I)V

    .line 459
    iget v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    packed-switch v2, :pswitch_data_1

    .line 474
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # getter for: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$400(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToMyDownloads(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 476
    :goto_1
    return-void

    .line 441
    .end local v0    # "clickType":I
    :pswitch_2
    const/16 v0, 0x6a

    .line 442
    .restart local v0    # "clickType":I
    goto :goto_0

    .line 444
    .end local v0    # "clickType":I
    :pswitch_3
    const/16 v0, 0x69

    .line 445
    .restart local v0    # "clickType":I
    goto :goto_0

    .line 447
    .end local v0    # "clickType":I
    :pswitch_4
    const/16 v0, 0x68

    .line 448
    .restart local v0    # "clickType":I
    goto :goto_0

    .line 450
    .end local v0    # "clickType":I
    :pswitch_5
    const/16 v0, 0x6b

    .line 451
    .restart local v0    # "clickType":I
    goto :goto_0

    .line 464
    :pswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # getter for: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$400(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showAppNeededDialog(I)V

    goto :goto_1

    .line 469
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$context:Landroid/content/Context;

    iget v3, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$backendId:I

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 471
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 459
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_1
        :pswitch_6
        :pswitch_1
        :pswitch_6
    .end packed-switch
.end method
