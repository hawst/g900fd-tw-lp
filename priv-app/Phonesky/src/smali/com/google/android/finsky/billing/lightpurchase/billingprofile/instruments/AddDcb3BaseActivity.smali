.class public abstract Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;
.source "AddDcb3BaseActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;-><init>()V

    return-void
.end method

.method protected static putIntentExtras(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;Landroid/content/Intent;)V
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "instrumentStatus"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    const-string v0, "authAccount"

    invoke-virtual {p2, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    const-string v0, "AddDcb3Instrument.instrument_status"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 28
    return-void
.end method


# virtual methods
.method protected getBillingFlow()Lcom/google/android/finsky/billing/BillingFlow;
    .locals 6

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "AddDcb3Instrument.instrument_status"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 41
    .local v4, "instrumentStatus":Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    .line 42
    .local v3, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->getBillingUiMode()I

    move-result v5

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;-><init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;I)V

    return-object v0
.end method

.method protected getBillingFlowFragment()Lcom/google/android/finsky/billing/BillingFlowFragment;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract getPlayStoreUiElementType()I
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->mSavedFlowState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->startOrResumeFlow(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method
