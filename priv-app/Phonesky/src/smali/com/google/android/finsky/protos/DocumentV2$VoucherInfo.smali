.class public final Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoucherInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;


# instance fields
.field public doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public offer:[Lcom/google/android/finsky/protos/Common$Offer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5773
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5774
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->clear()Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    .line 5775
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    .locals 2

    .prologue
    .line 5756
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-nez v0, :cond_1

    .line 5757
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 5759
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-nez v0, :cond_0

    .line 5760
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    .line 5762
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5764
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    return-object v0

    .line 5762
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    .locals 1

    .prologue
    .line 5778
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 5779
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Offer;->emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 5780
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->cachedSize:I

    .line 5781
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 5803
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 5804
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_0

    .line 5805
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5808
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 5809
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 5810
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v3, v1

    .line 5811
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_1

    .line 5812
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5809
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5817
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 5825
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 5826
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 5830
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5831
    :sswitch_0
    return-object p0

    .line 5836
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_1

    .line 5837
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 5839
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5843
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5845
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v5, :cond_3

    move v1, v4

    .line 5846
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 5848
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v1, :cond_2

    .line 5849
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5851
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 5852
    new-instance v5, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 5853
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 5854
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5851
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5845
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v1, v5

    goto :goto_1

    .line 5857
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 5858
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 5859
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    goto :goto_0

    .line 5826
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5750
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5787
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_0

    .line 5788
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5790
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 5791
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 5792
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v2, v1

    .line 5793
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_1

    .line 5794
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5791
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5798
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5799
    return-void
.end method
