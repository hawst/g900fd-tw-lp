.class Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;
.super Ljava/lang/Object;
.source "AppsPermissionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/AppsPermissionsActivity;->updateFromDoc()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

.field final synthetic val$learnMoreLink:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/AppsPermissionsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;->this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;->val$learnMoreLink:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 296
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;->val$learnMoreLink:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 298
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;->this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 299
    return-void
.end method
