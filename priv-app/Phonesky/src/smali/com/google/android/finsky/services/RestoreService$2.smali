.class Lcom/google/android/finsky/services/RestoreService$2;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/RestoreService;->restore(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$aid:Ljava/lang/String;

.field final synthetic val$finalAid:J


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iput-object p2, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$finalAid:J

    iput-object p5, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$aid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Ljava/lang/String;)V
    .locals 7
    .param p1, "consistencyToken"    # Ljava/lang/String;

    .prologue
    .line 1596
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$finalAid:J

    new-instance v5, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;

    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    invoke-direct {v5, v0, v4}, Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;-><init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/finsky/services/RestoreService$2$1;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/services/RestoreService$2$1;-><init>(Lcom/google/android/finsky/services/RestoreService$2;)V

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Lcom/google/android/finsky/api/DfeApi;->getBackupDocumentChoices(JLjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1616
    return-void
.end method
