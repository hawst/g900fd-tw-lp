.class public final Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceCreateInstrumentFlowHandle"
.end annotation


# instance fields
.field public hasToken:Z

.field public token:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 427
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 428
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;
    .locals 1

    .prologue
    .line 431
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    .line 432
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->hasToken:Z

    .line 433
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->cachedSize:I

    .line 434
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 448
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 449
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->hasToken:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 450
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 461
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 462
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 466
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 467
    :sswitch_0
    return-object p0

    .line 472
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    .line 473
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->hasToken:Z

    goto :goto_0

    .line 462
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->hasToken:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 441
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 443
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 444
    return-void
.end method
