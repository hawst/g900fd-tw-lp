.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreSessionData"
.end annotation


# instance fields
.field public allowUnknownSources:Z

.field public autoUpdateCleanupDialogNumTimesShown:I

.field public contentFilterLevel:I

.field public downloadDataDirSizeMb:I

.field public gaiaPasswordAuthOptedOut:Z

.field public globalAutoUpdateEnabled:Z

.field public globalAutoUpdateOverWifiOnly:Z

.field public hasAllowUnknownSources:Z

.field public hasAutoUpdateCleanupDialogNumTimesShown:Z

.field public hasContentFilterLevel:Z

.field public hasDownloadDataDirSizeMb:Z

.field public hasGaiaPasswordAuthOptedOut:Z

.field public hasGlobalAutoUpdateEnabled:Z

.field public hasGlobalAutoUpdateOverWifiOnly:Z

.field public hasNetworkSubType:Z

.field public hasNetworkType:Z

.field public hasNumAccountsOnDevice:Z

.field public hasNumAutoUpdatingInstalledApps:Z

.field public hasNumInstalledApps:Z

.field public hasNumInstalledAppsNotAutoUpdating:Z

.field public hasPurchaseAuthType:Z

.field public hasRecommendedMaxDownloadOverMobileBytes:Z

.field public networkSubType:I

.field public networkType:I

.field public numAccountsOnDevice:I

.field public numAutoUpdatingInstalledApps:I

.field public numInstalledApps:I

.field public numInstalledAppsNotAutoUpdating:I

.field public promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

.field public purchaseAuthType:I

.field public recommendedMaxDownloadOverMobileBytes:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3753
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3754
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .line 3755
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3758
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    .line 3759
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    .line 3760
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    .line 3761
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    .line 3762
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    .line 3763
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    .line 3764
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    .line 3765
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    .line 3766
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    .line 3767
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    .line 3768
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    .line 3769
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    .line 3770
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    .line 3771
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    .line 3772
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    .line 3773
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    .line 3774
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    .line 3775
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    .line 3776
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    .line 3777
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    .line 3778
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    .line 3779
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasContentFilterLevel:Z

    .line 3780
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    .line 3781
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAllowUnknownSources:Z

    .line 3782
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    .line 3783
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    .line 3784
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasPurchaseAuthType:Z

    .line 3785
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    .line 3786
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasDownloadDataDirSizeMb:Z

    .line 3787
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    .line 3788
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasRecommendedMaxDownloadOverMobileBytes:Z

    .line 3789
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->cachedSize:I

    .line 3790
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 3849
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3850
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    if-eqz v1, :cond_1

    .line 3851
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3854
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    if-eqz v1, :cond_3

    .line 3855
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3858
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    if-eqz v1, :cond_5

    .line 3859
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3862
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    if-eqz v1, :cond_7

    .line 3863
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3866
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    if-eqz v1, :cond_9

    .line 3867
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3870
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    if-eqz v1, :cond_b

    .line 3871
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3874
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    if-nez v1, :cond_c

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    if-eqz v1, :cond_d

    .line 3875
    :cond_c
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3878
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    if-nez v1, :cond_e

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    if-eqz v1, :cond_f

    .line 3879
    :cond_e
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3882
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    if-nez v1, :cond_10

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    if-eqz v1, :cond_11

    .line 3883
    :cond_10
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3886
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    if-nez v1, :cond_12

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    if-eqz v1, :cond_13

    .line 3887
    :cond_12
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3890
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasContentFilterLevel:Z

    if-nez v1, :cond_14

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    if-eqz v1, :cond_15

    .line 3891
    :cond_14
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3894
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAllowUnknownSources:Z

    if-nez v1, :cond_16

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    if-eqz v1, :cond_17

    .line 3895
    :cond_16
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3898
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    if-eqz v1, :cond_18

    .line 3899
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3902
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasPurchaseAuthType:Z

    if-nez v1, :cond_19

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    if-eqz v1, :cond_1a

    .line 3903
    :cond_19
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3906
    :cond_1a
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasDownloadDataDirSizeMb:Z

    if-nez v1, :cond_1b

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    if-eqz v1, :cond_1c

    .line 3907
    :cond_1b
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3910
    :cond_1c
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasRecommendedMaxDownloadOverMobileBytes:Z

    if-nez v1, :cond_1d

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1e

    .line 3911
    :cond_1d
    const/16 v1, 0x10

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3914
    :cond_1e
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3922
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3923
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3927
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3928
    :sswitch_0
    return-object p0

    .line 3933
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    .line 3934
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    goto :goto_0

    .line 3938
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    .line 3939
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    goto :goto_0

    .line 3943
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    .line 3944
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    goto :goto_0

    .line 3948
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    .line 3949
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    goto :goto_0

    .line 3953
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    .line 3954
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    goto :goto_0

    .line 3958
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    .line 3959
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    goto :goto_0

    .line 3963
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    .line 3964
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    goto :goto_0

    .line 3968
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    .line 3969
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    goto :goto_0

    .line 3973
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    .line 3974
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    goto :goto_0

    .line 3978
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    .line 3979
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    goto :goto_0

    .line 3983
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    .line 3984
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasContentFilterLevel:Z

    goto :goto_0

    .line 3988
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    .line 3989
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAllowUnknownSources:Z

    goto :goto_0

    .line 3993
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    if-nez v1, :cond_1

    .line 3994
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    .line 3996
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 4000
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    .line 4001
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasPurchaseAuthType:Z

    goto/16 :goto_0

    .line 4005
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    .line 4006
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasDownloadDataDirSizeMb:Z

    goto/16 :goto_0

    .line 4010
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    .line 4011
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasRecommendedMaxDownloadOverMobileBytes:Z

    goto/16 :goto_0

    .line 3923
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3673
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3796
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    if-eqz v0, :cond_1

    .line 3797
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3799
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    if-eqz v0, :cond_3

    .line 3800
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3802
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    if-eqz v0, :cond_5

    .line 3803
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3805
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    if-eqz v0, :cond_7

    .line 3806
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3808
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    if-eqz v0, :cond_9

    .line 3809
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3811
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    if-eqz v0, :cond_b

    .line 3812
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3814
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    if-nez v0, :cond_c

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    if-eqz v0, :cond_d

    .line 3815
    :cond_c
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3817
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    if-eqz v0, :cond_f

    .line 3818
    :cond_e
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3820
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    if-eqz v0, :cond_11

    .line 3821
    :cond_10
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3823
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    if-eqz v0, :cond_13

    .line 3824
    :cond_12
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3826
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasContentFilterLevel:Z

    if-nez v0, :cond_14

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    if-eqz v0, :cond_15

    .line 3827
    :cond_14
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3829
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAllowUnknownSources:Z

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    if-eqz v0, :cond_17

    .line 3830
    :cond_16
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3832
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    if-eqz v0, :cond_18

    .line 3833
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3835
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasPurchaseAuthType:Z

    if-nez v0, :cond_19

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    if-eqz v0, :cond_1a

    .line 3836
    :cond_19
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3838
    :cond_1a
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasDownloadDataDirSizeMb:Z

    if-nez v0, :cond_1b

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    if-eqz v0, :cond_1c

    .line 3839
    :cond_1b
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3841
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasRecommendedMaxDownloadOverMobileBytes:Z

    if-nez v0, :cond_1d

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1e

    .line 3842
    :cond_1d
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3844
    :cond_1e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3845
    return-void
.end method
