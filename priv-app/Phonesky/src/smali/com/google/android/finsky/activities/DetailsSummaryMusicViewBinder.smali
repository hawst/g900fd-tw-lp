.class public Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;
.super Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.source "DetailsSummaryMusicViewBinder.java"


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
    .locals 19
    .param p1, "launchButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p3, "buyButton2"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p4, "tryButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p5, "downloadButton"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 33
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z

    move-result v10

    .line 43
    .local v10, "isDisplayingActionButtons":Z
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isDetailsAlbumAllAccessEnabled()Z

    move-result v17

    if-nez v17, :cond_0

    move v11, v10

    .line 136
    .end local v10    # "isDisplayingActionButtons":Z
    .local v11, "isDisplayingActionButtons":Z
    :goto_0
    return v11

    .line 47
    .end local v11    # "isDisplayingActionButtons":Z
    .restart local v10    # "isDisplayingActionButtons":Z
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailableInMusicAllAccess(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v9

    .line 50
    .local v9, "isAvailableInAllAccess":Z
    if-nez v9, :cond_1

    move v11, v10

    .line 51
    .end local v10    # "isDisplayingActionButtons":Z
    .restart local v11    # "isDisplayingActionButtons":Z
    goto :goto_0

    .line 56
    .end local v11    # "isDisplayingActionButtons":Z
    .restart local v10    # "isDisplayingActionButtons":Z
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v13

    .line 57
    .local v13, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v3

    .line 58
    .local v3, "account":Landroid/accounts/Account;
    invoke-virtual {v13, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v14

    .line 59
    .local v14, "library":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-static {v14}, Lcom/google/android/finsky/utils/LibraryUtils;->isMusicAllAcessSubscriber(Lcom/google/android/finsky/library/Library;)Z

    move-result v8

    .line 60
    .local v8, "isAllAccessSubscriber":Z
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/PlayActionButton;->getVisibility()I

    move-result v17

    if-eqz v17, :cond_3

    const/4 v12, 0x1

    .line 62
    .local v12, "isOwned":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 63
    .local v16, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 64
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v17, 0x7f0a0183

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 66
    .local v4, "extraLabelsBottomContainer":Landroid/view/ViewGroup;
    const v17, 0x7f0a0184

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 68
    .local v5, "extraLabelsLeading":Landroid/view/ViewGroup;
    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 69
    const/4 v6, 0x0

    .line 71
    .local v6, "hasExtraLabelsBottom":Z
    if-nez v8, :cond_4

    .line 73
    const/16 v17, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 74
    const/16 v17, 0x1

    move-object/from16 v0, p5

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 75
    const/4 v10, 0x1

    .line 77
    new-instance v15, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;Landroid/accounts/Account;)V

    .line 96
    .local v15, "listener":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v17

    const v18, 0x7f0c0400

    move-object/from16 v0, p5

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2, v15}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 98
    const v17, 0x7f0c0401

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v7, v5, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->addExtraLabelBottom(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 100
    const/4 v6, 0x1

    .line 134
    .end local v15    # "listener":Landroid/view/View$OnClickListener;
    :cond_2
    :goto_2
    if-eqz v6, :cond_5

    const/16 v17, 0x0

    :goto_3
    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    move v11, v10

    .line 136
    .end local v10    # "isDisplayingActionButtons":Z
    .restart local v11    # "isDisplayingActionButtons":Z
    goto/16 :goto_0

    .line 60
    .end local v4    # "extraLabelsBottomContainer":Landroid/view/ViewGroup;
    .end local v5    # "extraLabelsLeading":Landroid/view/ViewGroup;
    .end local v6    # "hasExtraLabelsBottom":Z
    .end local v7    # "inflater":Landroid/view/LayoutInflater;
    .end local v11    # "isDisplayingActionButtons":Z
    .end local v12    # "isOwned":Z
    .end local v16    # "resources":Landroid/content/res/Resources;
    .restart local v10    # "isDisplayingActionButtons":Z
    :cond_3
    const/4 v12, 0x0

    goto :goto_1

    .line 101
    .restart local v4    # "extraLabelsBottomContainer":Landroid/view/ViewGroup;
    .restart local v5    # "extraLabelsLeading":Landroid/view/ViewGroup;
    .restart local v6    # "hasExtraLabelsBottom":Z
    .restart local v7    # "inflater":Landroid/view/LayoutInflater;
    .restart local v12    # "isOwned":Z
    .restart local v16    # "resources":Landroid/content/res/Resources;
    :cond_4
    if-nez v12, :cond_2

    .line 104
    const/16 v17, 0x0

    move-object/from16 v0, p5

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 105
    const/16 v17, 0x1

    move-object/from16 v0, p5

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 106
    const/4 v10, 0x1

    .line 108
    new-instance v15, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;Landroid/accounts/Account;)V

    .line 127
    .restart local v15    # "listener":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v17

    const v18, 0x7f0c020e

    move-object/from16 v0, p5

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2, v15}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 129
    const v17, 0x7f0c0402

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v7, v5, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->addExtraLabelBottom(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 131
    const/4 v6, 0x1

    goto :goto_2

    .line 134
    .end local v15    # "listener":Landroid/view/View$OnClickListener;
    :cond_5
    const/16 v17, 0x8

    goto :goto_3
.end method
