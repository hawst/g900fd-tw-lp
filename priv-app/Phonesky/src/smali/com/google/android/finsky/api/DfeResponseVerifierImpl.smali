.class public Lcom/google/android/finsky/api/DfeResponseVerifierImpl;
.super Ljava/lang/Object;
.source "DfeResponseVerifierImpl.java"

# interfaces
.implements Lcom/google/android/play/dfe/api/DfeResponseVerifier;


# static fields
.field private static final FALLBACK_KEYS_FILES_SUBDIR:Ljava/lang/String;

.field private static final PROD_KEYS_ASSETS_SUBDIR:Ljava/lang/String;

.field private static final SECURE_RANDOM:Ljava/security/SecureRandom;

.field private static sFallbackReader:Lorg/keyczar/interfaces/KeyczarReader;

.field private static sFallbackReaderInitialized:Z

.field private static sProdReader:Lorg/keyczar/interfaces/KeyczarReader;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mNonce:[B

.field private mNonceInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 30
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keys"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dfe-response-auth"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->PROD_KEYS_ASSETS_SUBDIR:Ljava/lang/String;

    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keys"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dfe-response-auth-dev"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->FALLBACK_KEYS_FILES_SUBDIR:Ljava/lang/String;

    .line 38
    :try_start_0
    const-string v2, "SHA1PRNG"

    invoke-static {v2}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 44
    .local v1, "random":Ljava/security/SecureRandom;
    :goto_0
    sput-object v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->SECURE_RANDOM:Ljava/security/SecureRandom;

    .line 45
    return-void

    .line 39
    .end local v1    # "random":Ljava/security/SecureRandom;
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v2, "Could not initialize SecureRandom, SHA1PRNG not supported. %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    const/4 v1, 0x0

    .restart local v1    # "random":Ljava/security/SecureRandom;
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonce:[B

    .line 58
    iput-object p1, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method private static extractResponseSignature(Ljava/lang/String;)[B
    .locals 8
    .param p0, "responseSignature"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 138
    const-string v5, "No signing response found."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    new-instance v5, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;

    const-string v6, "No signing response found."

    invoke-direct {v5, v6}, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 141
    :cond_0
    const-string v5, ";"

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 142
    .local v4, "part":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 143
    const-string v5, "signature="

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 146
    :cond_1
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "encodedSignature":Ljava/lang/String;
    const/16 v5, 0xb

    invoke-static {v1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v5

    return-object v5

    .line 150
    .end local v1    # "encodedSignature":Ljava/lang/String;
    .end local v4    # "part":Ljava/lang/String;
    :cond_2
    new-instance v5, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Signature not found in response: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private static declared-synchronized getFallbackReader(Landroid/content/Context;)Lorg/keyczar/interfaces/KeyczarReader;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 172
    const-class v2, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sFallbackReaderInitialized:Z

    if-nez v1, :cond_1

    .line 173
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    sget-object v3, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->FALLBACK_KEYS_FILES_SUBDIR:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 174
    .local v0, "fallbackDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    new-instance v1, Lorg/keyczar/KeyczarFileReader;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/keyczar/KeyczarFileReader;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sFallbackReader:Lorg/keyczar/interfaces/KeyczarReader;

    .line 177
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sFallbackReaderInitialized:Z

    .line 179
    .end local v0    # "fallbackDirectory":Ljava/io/File;
    :cond_1
    sget-object v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sFallbackReader:Lorg/keyczar/interfaces/KeyczarReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 172
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized getProdReader(Landroid/content/Context;)Lorg/keyczar/interfaces/KeyczarReader;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    const-class v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sProdReader:Lorg/keyczar/interfaces/KeyczarReader;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->PROD_KEYS_ASSETS_SUBDIR:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sProdReader:Lorg/keyczar/interfaces/KeyczarReader;

    .line 162
    :cond_0
    sget-object v0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->sProdReader:Lorg/keyczar/interfaces/KeyczarReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private internalVerify(Lorg/keyczar/interfaces/KeyczarReader;[B[BLjava/lang/String;)Z
    .locals 8
    .param p1, "reader"    # Lorg/keyczar/interfaces/KeyczarReader;
    .param p2, "nonce"    # [B
    .param p3, "response"    # [B
    .param p4, "responseSignature"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 119
    :try_start_0
    new-instance v3, Lorg/keyczar/Verifier;

    invoke-direct {v3, p1}, Lorg/keyczar/Verifier;-><init>(Lorg/keyczar/interfaces/KeyczarReader;)V

    .line 120
    .local v3, "verifier":Lorg/keyczar/Verifier;
    array-length v5, p2

    array-length v6, p3

    add-int/2addr v5, v6

    new-array v0, v5, [B

    .line 121
    .local v0, "allBytes":[B
    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, p2

    invoke-static {p2, v5, v0, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    const/4 v5, 0x0

    array-length v6, p2

    array-length v7, p3

    invoke-static {p3, v5, v0, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    invoke-static {p4}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->extractResponseSignature(Ljava/lang/String;)[B

    move-result-object v2

    .line 124
    .local v2, "signature":[B
    invoke-virtual {v3, v0, v2}, Lorg/keyczar/Verifier;->verify([B[B)Z
    :try_end_0
    .catch Lorg/keyczar/exceptions/KeyczarException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 131
    .end local v0    # "allBytes":[B
    .end local v2    # "signature":[B
    .end local v3    # "verifier":Lorg/keyczar/Verifier;
    :goto_0
    return v4

    .line 125
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Lorg/keyczar/exceptions/KeyczarException;
    const-string v5, "Keyczar exception during signature verification: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v4

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getSignatureRequest()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->SECURE_RANDOM:Ljava/security/SecureRandom;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;

    const-string v1, "Uninitialized SecureRandom."

    invoke-direct {v0, v1}, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 71
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonceInitialized:Z

    if-nez v0, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->SECURE_RANDOM:Ljava/security/SecureRandom;

    iget-object v1, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonce:[B

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonceInitialized:Z

    .line 75
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "nonce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonce:[B

    const/16 v2, 0xb

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public verify([BLjava/lang/String;)V
    .locals 6
    .param p1, "response"    # [B
    .param p2, "responseSignature"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->getProdReader(Landroid/content/Context;)Lorg/keyczar/interfaces/KeyczarReader;

    move-result-object v0

    .line 99
    .local v0, "reader":Lorg/keyczar/interfaces/KeyczarReader;
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonce:[B

    invoke-direct {p0, v0, v2, p1, p2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->internalVerify(Lorg/keyczar/interfaces/KeyczarReader;[B[BLjava/lang/String;)Z

    move-result v1

    .line 100
    .local v1, "verified":Z
    if-nez v1, :cond_0

    .line 102
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->getFallbackReader(Landroid/content/Context;)Lorg/keyczar/interfaces/KeyczarReader;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    const-string v2, "Retry verification using fallback keys."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->mNonce:[B

    invoke-direct {p0, v0, v2, p1, p2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;->internalVerify(Lorg/keyczar/interfaces/KeyczarReader;[B[BLjava/lang/String;)Z

    move-result v1

    .line 108
    :cond_0
    if-eqz v1, :cond_1

    sget-boolean v2, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 109
    :cond_1
    const-string v2, "Response signature verified: %b"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    :cond_2
    if-nez v1, :cond_3

    .line 112
    new-instance v2, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;

    const-string v3, "Response signature mismatch."

    invoke-direct {v2, v3}, Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 114
    :cond_3
    return-void
.end method
