.class public Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;
.super Landroid/widget/BaseAdapter;
.source "MyAppsSubscriptionsAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;
    }
.end annotation


# static fields
.field private static final sSubscriptionAbcCollator:Ljava/text/Collator;

.field private static final sSubscriptionAbcSorter:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLeadingSpacerHeight:I

.field private final mListener:Landroid/view/View$OnClickListener;

.field private final mSubscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->sSubscriptionAbcCollator:Ljava/text/Collator;

    .line 90
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$1;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->sSubscriptionAbcSorter:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 86
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    .line 120
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mContext:Landroid/content/Context;

    .line 121
    iput-object p2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 122
    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 123
    iput-object p4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mListener:Landroid/view/View$OnClickListener;

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mLeadingSpacerHeight:I

    .line 127
    return-void
.end method

.method static synthetic access$000()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->sSubscriptionAbcCollator:Ljava/text/Collator;

    return-object v0
.end method

.method private getLeadingSpacerView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04014d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 190
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 191
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 192
    return-object p1
.end method


# virtual methods
.method public addEntry(Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;)V
    .locals 2
    .param p1, "libEntry"    # Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "parent"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->notifyDataSetChanged()V

    .line 138
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->notifyDataSetChanged()V

    .line 294
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getDocument(I)Lcom/google/android/finsky/api/model/Document;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 281
    if-nez p1, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;->parentDoc:Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 162
    if-nez p1, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;->subscriptionDoc:Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 170
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 153
    if-nez p1, :cond_0

    .line 154
    const/4 v0, 0x1

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubscriptionView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 197
    if-nez p2, :cond_0

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v22, v0

    const v23, 0x7f04012a

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, p3

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 202
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    move-object/from16 v22, v0

    add-int/lit8 v23, p1, -0x1

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;

    .line 203
    .local v17, "subsEntry":Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;->subscriptionOwnership:Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    .line 204
    .local v7, "libEntry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    move-object/from16 v0, v17

    iget-object v15, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;->subscriptionDoc:Lcom/google/android/finsky/api/model/Document;

    .line 205
    .local v15, "subsDoc":Lcom/google/android/finsky/api/model/Document;
    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter$MyAppsSubscriptionEntry;->parentDoc:Lcom/google/android/finsky/api/model/Document;

    .line 206
    .local v11, "parentDoc":Lcom/google/android/finsky/api/model/Document;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .local v13, "res":Landroid/content/res/Resources;
    move-object/from16 v5, p2

    .line 208
    check-cast v5, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;

    .line 216
    .local v5, "entry":Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    .line 218
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getThumbnail()Lcom/google/android/play/layout/PlayCardThumbnail;

    move-result-object v18

    .line 219
    .local v18, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardThumbnail;->updateCoverPadding(I)V

    .line 220
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v19

    check-cast v19, Lcom/google/android/finsky/layout/DocImageView;

    .line 221
    .local v19, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v22, v0

    sget-object v23, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v11, v1, v2}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 223
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getTitle()Landroid/widget/TextView;

    move-result-object v20

    .line 224
    .local v20, "title":Landroid/widget/TextView;
    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getSubtitle()Lcom/google/android/play/layout/PlayTextView;

    move-result-object v4

    .line 227
    .local v4, "appTitle":Landroid/widget/TextView;
    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getLabel()Lcom/google/android/play/layout/PlayCardLabelView;

    move-result-object v14

    .line 230
    .local v14, "status":Lcom/google/android/play/layout/PlayCardLabelView;
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getItemBadge()Lcom/google/android/play/layout/PlayTextView;

    move-result-object v6

    .line 231
    .local v6, "extraInfo":Lcom/google/android/play/layout/PlayTextView;
    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v16

    .line 232
    .local v16, "subsDocBackendId":I
    iget-boolean v0, v7, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->isAutoRenewing:Z

    move/from16 v22, v0

    if-eqz v22, :cond_5

    .line 233
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 234
    .local v8, "now":J
    iget-wide v0, v7, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->trialUntilTimestampMs:J

    move-wide/from16 v22, v0

    cmp-long v22, v8, v22

    if-gez v22, :cond_2

    .line 235
    const v22, 0x7f0c0221

    move/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(II)V

    .line 239
    :goto_0
    if-eqz v6, :cond_1

    .line 240
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v10

    .line 241
    .local v10, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v10, :cond_4

    iget-object v0, v10, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    move-object/from16 v22, v0

    if-eqz v22, :cond_4

    .line 242
    iget-object v0, v10, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v12, v0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 244
    .local v12, "priceWithPeriod":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 245
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 246
    invoke-virtual {v6, v12}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    .end local v8    # "now":J
    .end local v10    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v12    # "priceWithPeriod":Ljava/lang/String;
    :cond_1
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mListener:Landroid/view/View$OnClickListener;

    move-object/from16 v22, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    const v22, 0x7f0a0159

    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setVisibility(I)V

    .line 276
    return-object p2

    .line 237
    .restart local v8    # "now":J
    :cond_2
    const v22, 0x7f0c0220

    move/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(II)V

    goto :goto_0

    .line 248
    .restart local v10    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v12    # "priceWithPeriod":Ljava/lang/String;
    :cond_3
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 249
    const-string v22, "Document for %s does not contain a formatted price."

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 253
    .end local v12    # "priceWithPeriod":Ljava/lang/String;
    :cond_4
    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 254
    const-string v22, "Document for %s does not contain a subscription offer or terms."

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 259
    .end local v8    # "now":J
    .end local v10    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_5
    const v22, 0x7f0c0222

    move/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(II)V

    .line 260
    if-eqz v6, :cond_1

    .line 261
    invoke-virtual {v7}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->getValidUntilTimestampMs()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v21

    .line 263
    .local v21, "validUntil":Ljava/lang/String;
    const v22, 0x7f0c021c

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v21, v23, v24

    move/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 181
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type for getView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->getLeadingSpacerView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->getSubscriptionView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x2

    return v0
.end method

.method sortDocs()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->mSubscriptions:Ljava/util/List;

    sget-object v1, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->sSubscriptionAbcSorter:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 289
    return-void
.end method
