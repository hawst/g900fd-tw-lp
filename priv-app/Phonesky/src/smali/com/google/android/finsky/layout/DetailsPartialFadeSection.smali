.class public interface abstract Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
.super Ljava/lang/Object;
.source "DetailsPartialFadeSection.java"


# virtual methods
.method public abstract addParticipatingChildViewIds(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addParticipatingChildViews(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation
.end method
