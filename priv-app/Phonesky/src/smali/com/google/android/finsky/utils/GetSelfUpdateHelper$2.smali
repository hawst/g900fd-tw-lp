.class final Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;
.super Ljava/lang/Object;
.source "GetSelfUpdateHelper.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$allowRetry:Z

.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;


# direct methods
.method constructor <init>(ZLcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;Lcom/google/android/finsky/api/DfeApi;)V
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$allowRetry:Z

    iput-object p2, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    iput-object p3, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;->requiresUploadDeviceConfig:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;->onResponse(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)V

    .line 156
    :goto_0
    return-void

    .line 145
    :cond_0
    const-string v0, "Server requests device config token"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->invalidateToken()V

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V

    .line 148
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$allowRetry:Z

    if-nez v0, :cond_1

    .line 150
    const-string v0, "Failed to converge on device config for selfUpdate"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    new-instance v1, Lcom/android/volley/ServerError;

    invoke-direct {v1}, Lcom/android/volley/ServerError;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;->onErrorResponse(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    # invokes: Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    invoke-static {v2, v0, v1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->access$100(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 127
    check-cast p1, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;->onResponse(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)V

    return-void
.end method
