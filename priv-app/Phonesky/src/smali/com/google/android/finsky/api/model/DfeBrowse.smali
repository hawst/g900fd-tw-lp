.class public Lcom/google/android/finsky/api/model/DfeBrowse;
.super Lcom/google/android/finsky/api/model/DfeModel;
.source "DfeBrowse.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/DfeModel;",
        "Landroid/os/Parcelable;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Browse$BrowseResponse;",
        ">;"
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/api/model/DfeBrowse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/finsky/api/model/DfeBrowse$1;

    invoke-direct {v0}, Lcom/google/android/finsky/api/model/DfeBrowse$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/api/model/DfeBrowse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V
    .locals 0
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "landingUrl"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 36
    invoke-interface {p1, p2, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->getBrowseLayout(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/protos/Browse$BrowseResponse;)V
    .locals 0
    .param p1, "browseResponse"    # Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .line 28
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public getBackendId()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    return v0
.end method

.method public getBrowseTabs()[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    return-object v0
.end method

.method public getLandingTabIndex()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    return v0
.end method

.method public getQuickLinkFallbackTabIndex()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    return v0
.end method

.method public getQuickLinkList()[Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    return-object v0
.end method

.method public getQuickLinkTabIndex()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    return v0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    array-length v0, v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/finsky/protos/Browse$BrowseResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeBrowse;->clearErrors()V

    .line 114
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeBrowse;->notifyDataSetChanged()V

    .line 116
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeBrowse;->onResponse(Lcom/google/android/finsky/protos/Browse$BrowseResponse;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBrowse;->mBrowseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 126
    return-void
.end method
