.class public Lcom/google/android/finsky/utils/MyPeoplePageHelper;
.super Ljava/lang/Object;
.source "MyPeoplePageHelper.java"


# static fields
.field private static sLastMutationTimeMs:J

.field private static sListUrls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sListUrls:Ljava/util/List;

    return-void
.end method

.method public static addPeoplePageListUrls(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "listUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sListUrls:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 32
    return-void
.end method

.method public static hasMutationOccurredSince(J)Z
    .locals 2
    .param p0, "timestampMs"    # J

    .prologue
    .line 35
    sget-wide v0, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sLastMutationTimeMs:J

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onMutationOccurred(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 4
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sLastMutationTimeMs:J

    .line 42
    sget-object v2, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sListUrls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    .local v1, "listUrl":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {p0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    goto :goto_0

    .line 45
    .end local v1    # "listUrl":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->sListUrls:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 46
    return-void
.end method
