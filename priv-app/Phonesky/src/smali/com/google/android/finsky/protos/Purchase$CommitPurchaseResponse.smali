.class public final Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommitPurchaseResponse"
.end annotation


# instance fields
.field public appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

.field public challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field public encodedDeliveryToken:Ljava/lang/String;

.field public hasEncodedDeliveryToken:Z

.field public hasServerLogsCookie:Z

.field public libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

.field public serverLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1204
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1205
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->clear()Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    .line 1206
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1209
    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 1210
    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 1211
    invoke-static {}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1212
    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 1213
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    .line 1214
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasServerLogsCookie:Z

    .line 1215
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    .line 1216
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasEncodedDeliveryToken:Z

    .line 1217
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->cachedSize:I

    .line 1218
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 1252
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1253
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-eqz v3, :cond_0

    .line 1254
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1257
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v3, :cond_1

    .line 1258
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1261
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 1262
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 1263
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v0, v3, v1

    .line 1264
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v0, :cond_2

    .line 1265
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1262
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1270
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v3, :cond_4

    .line 1271
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1274
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasServerLogsCookie:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1275
    :cond_5
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 1278
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasEncodedDeliveryToken:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1279
    :cond_7
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1282
    :cond_8
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1291
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1295
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1296
    :sswitch_0
    return-object p0

    .line 1301
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-nez v5, :cond_1

    .line 1302
    new-instance v5, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 1304
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1308
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v5, :cond_2

    .line 1309
    new-instance v5, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 1311
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1315
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1317
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v5, :cond_4

    move v1, v4

    .line 1318
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1320
    .local v2, "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v1, :cond_3

    .line 1321
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1323
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 1324
    new-instance v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v5, v2, v1

    .line 1325
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1326
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1323
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1317
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v1, v5

    goto :goto_1

    .line 1329
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_5
    new-instance v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v5, v2, v1

    .line 1330
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1331
    iput-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    goto :goto_0

    .line 1335
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v5, :cond_6

    .line 1336
    new-instance v5, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 1338
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1342
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    .line 1343
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 1347
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    .line 1348
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasEncodedDeliveryToken:Z

    goto/16 :goto_0

    .line 1291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1167
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1224
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-eqz v2, :cond_0

    .line 1225
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1227
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v2, :cond_1

    .line 1228
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1230
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1231
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 1232
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v0, v2, v1

    .line 1233
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v0, :cond_2

    .line 1234
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1231
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1238
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v2, :cond_4

    .line 1239
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1241
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1242
    :cond_5
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1244
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->hasEncodedDeliveryToken:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1245
    :cond_7
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1247
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1248
    return-void
.end method
