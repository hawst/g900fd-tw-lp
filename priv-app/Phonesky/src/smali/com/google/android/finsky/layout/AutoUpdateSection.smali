.class public Lcom/google/android/finsky/layout/AutoUpdateSection;
.super Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.source "AutoUpdateSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;
    }
.end annotation


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mLabel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AutoUpdateSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public static handleAutoUpdateButtonClick(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Z)V
    .locals 7
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "navMgr"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "showToast"    # Z

    .prologue
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    .line 124
    .local v6, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v6, :cond_0

    .line 125
    const-string v0, "tried to operate on a null doc"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 129
    const-string v0, "tried to operate on a non-apps doc."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    .line 134
    .local v2, "docId":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/layout/AutoUpdateSection;->isAutoUpdateEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v3, 0x1

    .line 135
    .local v3, "isAutoUpdateEnabledNewSetting":Z
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    move v4, p2

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/layout/AutoUpdateSection;->updateAutoUpdateForThisApp(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/appstate/AppStates;Ljava/lang/String;ZZLandroid/content/Context;)V

    goto :goto_0
.end method

.method public static isAutoUpdateEnabled(Ljava/lang/String;)Z
    .locals 4
    .param p0, "docId"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 106
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 107
    .local v1, "isAutoUpdateEnabled":Z
    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v2, :cond_0

    .line 108
    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    .line 111
    :cond_0
    :goto_0
    return v1

    .line 108
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isAutoUpdateVisible(Ljava/lang/String;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/receivers/Installer;)Z
    .locals 6
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "installer"    # Lcom/google/android/finsky/receivers/Installer;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 67
    if-nez p0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v4

    .line 72
    :cond_1
    invoke-static {p0}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 77
    invoke-virtual {p1, p0}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 83
    invoke-virtual {p2, p0}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 84
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_0

    .line 89
    iget-object v5, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v5, :cond_4

    move v2, v3

    .line 90
    .local v2, "isInstalled":Z
    :goto_1
    invoke-interface {p3, p0}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v1

    .line 91
    .local v1, "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    :cond_2
    if-eqz v2, :cond_3

    iget-object v5, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-boolean v5, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-nez v5, :cond_0

    :cond_3
    move v4, v3

    .line 100
    goto :goto_0

    .end local v1    # "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .end local v2    # "isInstalled":Z
    :cond_4
    move v2, v4

    .line 89
    goto :goto_1
.end method

.method private static updateAutoUpdateForThisApp(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/appstate/AppStates;Ljava/lang/String;ZZLandroid/content/Context;)V
    .locals 9
    .param p0, "fragmentMgr"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "isAutoUpdateEnabledNewSetting"    # Z
    .param p4, "showToast"    # Z
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 197
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 198
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v5, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v5}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v5

    sget-object v6, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v5, v6, :cond_1

    const/4 v3, 0x1

    .line 201
    .local v3, "oldEnabled":Z
    :goto_0
    if-eqz p3, :cond_2

    sget-object v2, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    .line 203
    .local v2, "newState":Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v5

    invoke-interface {v5, p2, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    .line 206
    if-eqz p3, :cond_3

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_3

    .line 207
    new-instance v1, Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;-><init>()V

    .line 208
    .local v1, "dialog":Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;
    const-string v5, "auto_update_dialog"

    invoke-virtual {v1, p0, v5}, Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 217
    .end local v1    # "dialog":Lcom/google/android/finsky/layout/AutoUpdateSection$AutoUpdateDialog;
    :cond_0
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v7

    const/16 v8, 0x193

    if-eqz p3, :cond_5

    const/4 v5, 0x1

    move v6, v5

    :goto_3
    if-eqz v3, :cond_6

    const/4 v5, 0x1

    :goto_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v8, v6, v5, p2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSettingsForPackageEvent(IILjava/lang/Integer;Ljava/lang/String;)V

    .line 220
    return-void

    .line 198
    .end local v2    # "newState":Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    .end local v3    # "oldEnabled":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 201
    .restart local v3    # "oldEnabled":Z
    :cond_2
    sget-object v2, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    goto :goto_1

    .line 209
    .restart local v2    # "newState":Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    :cond_3
    if-eqz p4, :cond_0

    .line 211
    if-eqz p3, :cond_4

    const v4, 0x7f0c035b

    .line 213
    .local v4, "toastString":I
    :goto_5
    const/4 v5, 0x1

    invoke-static {p5, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 211
    .end local v4    # "toastString":I
    :cond_4
    const v4, 0x7f0c035a

    goto :goto_5

    .line 217
    :cond_5
    const/4 v5, 0x0

    move v6, v5

    goto :goto_3

    :cond_6
    const/4 v5, 0x0

    goto :goto_4
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->onFinishInflate()V

    .line 60
    const v0, 0x7f0a00d2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AutoUpdateSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AutoUpdateSection;->mCheckBox:Landroid/widget/CheckBox;

    .line 61
    const v0, 0x7f0a00d3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AutoUpdateSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AutoUpdateSection;->mLabel:Landroid/widget/TextView;

    .line 62
    return-void
.end method
