.class public Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "RedeemScreenStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mBillingUiMode:I

.field private mCodeEntry:Landroid/widget/EditText;

.field private mErrorMessageHtml:Ljava/lang/String;

.field private mErrorMessageView:Landroid/widget/TextView;

.field private mMainView:Landroid/view/View;

.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 51
    const/16 v0, 0x371

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->syncContinueButtonState()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->logClickAndRedeemCode(Z)V

    return-void
.end method

.method private fireErrorEvents()V
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->playShakeAnimationIfPossible(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 166
    return-void
.end method

.method private logClickAndRedeemCode(Z)V
    .locals 4
    .param p1, "isImeAction"    # Z

    .prologue
    const/4 v2, 0x1

    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p1, :cond_0

    .line 215
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 216
    .restart local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 217
    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 219
    :cond_0
    const/16 v2, 0x372

    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->logClick(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 221
    .local v1, "code":Landroid/text/Editable;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->redeem(Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "prefillCode"    # Ljava/lang/String;
    .param p2, "errorMessageHtml"    # Ljava/lang/String;
    .param p3, "mode"    # I

    .prologue
    .line 63
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v2, "RedeemCodeActivity.prefill_code"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v2, "RedeemCodeFragment.error_message_html"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;-><init>()V

    .line 69
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->setArguments(Landroid/os/Bundle;)V

    .line 70
    return-object v1
.end method

.method private syncContinueButtonState()V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->setContinueButtonEnabled(Z)V

    .line 171
    return-void

    .line 170
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private syncErrorTextView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 196
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 197
    const-string v0, "Null mMainView."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 175
    const v0, 0x7f0c00fe

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->logClickAndRedeemCode(Z)V

    .line 181
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ui_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mBillingUiMode:I

    .line 77
    if-eqz p1, :cond_0

    .line 78
    const-string v0, "RedeemCodeFragment.error_message_html"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RedeemCodeFragment.error_message_html"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 93
    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mBillingUiMode:I

    if-ne v6, v9, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f0c0117

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mBillingUiMode:I

    if-nez v6, :cond_2

    const v3, 0x7f040184

    .line 98
    .local v3, "layoutId":I
    :goto_0
    invoke-virtual {p1, v3, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    .line 99
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    const v7, 0x7f0a0349

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "RedeemCodeActivity.prefill_code"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 101
    .local v4, "prefillCode":Ljava/lang/String;
    if-nez p3, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 102
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v6, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    const v7, 0x7f0a022b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    .local v0, "accountView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "authAccount"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    sget-object v6, Lcom/google/android/finsky/config/G;->redeemTermsAndConditionsUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/finsky/billing/BillingUtils;->replaceLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 108
    .local v5, "tosString":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    const v7, 0x7f0a00c2

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 109
    .local v2, "footerView":Landroid/widget/TextView;
    const v6, 0x7f0c0118

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v5, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 111
    invoke-virtual {v2}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 113
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 114
    .local v1, "codeEntryOriginalTextColors":Landroid/content/res/ColorStateList;
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    new-instance v7, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;

    invoke-direct {v7, p0, v1}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;Landroid/content/res/ColorStateList;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 132
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    new-instance v7, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$2;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$2;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 141
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    const v7, 0x7f0a00c6

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageView:Landroid/widget/TextView;

    .line 142
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->syncContinueButtonState()V

    .line 143
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->syncErrorTextView()V

    .line 145
    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mMainView:Landroid/view/View;

    return-object v6

    .line 96
    .end local v0    # "accountView":Landroid/widget/TextView;
    .end local v1    # "codeEntryOriginalTextColors":Landroid/content/res/ColorStateList;
    .end local v2    # "footerView":Landroid/widget/TextView;
    .end local v3    # "layoutId":I
    .end local v4    # "prefillCode":Ljava/lang/String;
    .end local v5    # "tosString":Ljava/lang/String;
    :cond_2
    const v3, 0x7f0401aa

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    const-string v0, "RedeemCodeFragment.error_message_html"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 150
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 151
    if-nez p2, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->fireErrorEvents()V

    .line 160
    :cond_0
    return-void
.end method

.method public showError(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mErrorMessageHtml:Ljava/lang/String;

    .line 190
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 191
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->syncErrorTextView()V

    .line 192
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->fireErrorEvents()V

    .line 193
    return-void
.end method
