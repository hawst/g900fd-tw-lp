.class Lcom/google/android/finsky/activities/TabbedBrowseFragment$PlayHeaderListConfigurator;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "TabbedBrowseFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/TabbedBrowseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlayHeaderListConfigurator"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 473
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    .line 474
    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 478
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 482
    const v0, 0x7f0400af

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 483
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 487
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x1

    return v0
.end method

.method protected getContentProtectionMode()I
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment$PlayHeaderListConfigurator;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x1

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 527
    const v0, 0x7f0a0118

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 512
    const/4 v0, 0x2

    return v0
.end method

.method protected getTabPaddingMode()I
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x1

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 507
    const v0, 0x7f0a021c

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x1

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    return v0
.end method
