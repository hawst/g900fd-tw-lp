.class Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;
.super Lcom/android/vending/billing/IMarketBillingService$Stub;
.source "MarketBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/MarketBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-direct {p0}, Lcom/android/vending/billing/IMarketBillingService$Stub;-><init>()V

    .line 622
    return-void
.end method

.method private argumentsMatch(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "requiredArguments"    # [Ljava/lang/String;
    .param p3, "optionalArguments"    # [Ljava/lang/String;

    .prologue
    .line 330
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 331
    .local v4, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v2

    .line 332
    .local v2, "expectedKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v6

    .line 333
    .local v6, "optionalKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v7, "BILLING_REQUEST"

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v7, "API_VERSION"

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    move-object v1, p2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v1, v3

    .line 337
    .local v0, "argument":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 339
    .end local v0    # "argument":Ljava/lang/String;
    :cond_0
    move-object v1, p3

    array-length v5, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_1

    aget-object v0, v1, v3

    .line 340
    .restart local v0    # "argument":Ljava/lang/String;
    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 339
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 342
    .end local v0    # "argument":Ljava/lang/String;
    :cond_1
    invoke-interface {v4, v6}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 343
    invoke-interface {v4, v2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v7

    return v7
.end method

.method private varargs argumentsMatchExactly(Landroid/os/Bundle;[Ljava/lang/String;)Z
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "requiredArguments"    # [Ljava/lang/String;

    .prologue
    .line 321
    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->EMPTY_STRING_ARRAY:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$000()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatch(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private computeSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 466
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    .line 467
    .local v0, "bytes":[B
    invoke-static {v0}, Lcom/google/android/finsky/utils/Md5Util;->secureHash([B)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getBillingRequest(Landroid/os/Bundle;)Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;
    .locals 6
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 291
    const-string v3, "BILLING_REQUEST"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, "billingRequestString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 293
    const-string v3, "Received bundle without billing request type"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :goto_0
    return-object v2

    .line 297
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 298
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "Unknown billing request type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getNextInAppRequestId()J
    .locals 4

    .prologue
    .line 647
    # getter for: Lcom/google/android/finsky/billing/iab/MarketBillingService;->sRandom:Ljava/util/Random;
    invoke-static {}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$300()Ljava/util/Random;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 662
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v1, v1, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 665
    :goto_0
    return-object v1

    .line 663
    :catch_0
    move-exception v0

    .line 664
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "cannot find package name: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 665
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleBillingRequest(Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 17
    .param p1, "billingRequest"    # Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 200
    const-string v2, "API_VERSION"

    const/4 v9, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 201
    .local v3, "billingApiVersion":I
    const-string v2, "PACKAGE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 202
    .local v4, "packageName":Ljava/lang/String;
    const-string v2, "DEVELOPER_PAYLOAD"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 203
    .local v7, "developerPayload":Ljava/lang/String;
    const-string v2, "ITEM_ID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 204
    .local v5, "itemId":Ljava/lang/String;
    const-string v2, "ITEM_TYPE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 205
    .local v6, "itemType":Ljava/lang/String;
    const-string v2, "NONCE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 206
    .local v12, "nonce":J
    const-string v2, "NOTIFY_IDS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 208
    .local v14, "notifyIds":[Ljava/lang/String;
    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    .line 209
    .local v15, "response":Landroid/os/Bundle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->checkBillingEnabled(ILjava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 210
    .local v16, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, v16

    if-eq v0, v2, :cond_0

    .line 211
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    .line 273
    :goto_0
    return-object v2

    .line 213
    :cond_0
    sget-object v2, Lcom/google/android/finsky/billing/iab/MarketBillingService$1;->$SwitchMap$com$google$android$finsky$billing$iab$MarketBillingService$BillingRequest:[I

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;->ordinal()I

    move-result v9

    aget v2, v2, v9

    packed-switch v2, :pswitch_data_0

    .line 270
    const-string v2, "enum %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    invoke-static {v2, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 215
    :pswitch_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "PACKAGE_NAME"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "ITEM_TYPE"

    aput-object v11, v9, v10

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatch(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 218
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 220
    :cond_1
    if-nez v6, :cond_2

    .line 222
    const-string v6, "inapp"

    .line 224
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->checkTypeSupported(ILjava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 273
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 227
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "PACKAGE_NAME"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string v10, "ITEM_ID"

    aput-object v10, v2, v9

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "DEVELOPER_PAYLOAD"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "ITEM_TYPE"

    aput-object v11, v9, v10

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatch(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 230
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto/16 :goto_0

    .line 232
    :cond_3
    if-nez v6, :cond_4

    .line 234
    const-string v6, "inapp"

    .line 236
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->checkTypeSupported(ILjava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 237
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, v16

    if-eq v0, v2, :cond_5

    .line 238
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto/16 :goto_0

    :cond_5
    move-object/from16 v2, p0

    .line 240
    invoke-virtual/range {v2 .. v7}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->requestPurchase(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v8

    .line 243
    .local v8, "requestIntentPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Landroid/app/PendingIntent;>;"
    const-string v9, "REQUEST_ID"

    iget-object v2, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v15, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 244
    const-string v9, "PURCHASE_INTENT"

    iget-object v2, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v15, v9, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 247
    .end local v8    # "requestIntentPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Landroid/app/PendingIntent;>;"
    :pswitch_2
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "PACKAGE_NAME"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string v10, "NONCE"

    aput-object v10, v2, v9

    const/4 v9, 0x2

    const-string v10, "NOTIFY_IDS"

    aput-object v10, v2, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatchExactly(Landroid/os/Bundle;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 249
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    move-object/from16 v9, p0

    move v10, v3

    move-object v11, v4

    .line 251
    invoke-virtual/range {v9 .. v14}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPurchaseInformation(ILjava/lang/String;J[Ljava/lang/String;)J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v10, v11}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->updateWithRequestId(Landroid/os/Bundle;J)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 254
    goto/16 :goto_1

    .line 256
    :pswitch_3
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "PACKAGE_NAME"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string v10, "NONCE"

    aput-object v10, v2, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatchExactly(Landroid/os/Bundle;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 257
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto/16 :goto_0

    .line 259
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v12, v13}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->restoreTransactions(ILjava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v10, v11}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->updateWithRequestId(Landroid/os/Bundle;J)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 261
    goto/16 :goto_1

    .line 263
    :pswitch_4
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "PACKAGE_NAME"

    aput-object v10, v2, v9

    const/4 v9, 0x1

    const-string v10, "NOTIFY_IDS"

    aput-object v10, v2, v9

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->argumentsMatchExactly(Landroid/os/Bundle;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 264
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto/16 :goto_0

    .line 266
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v14}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->confirmNotifications(ILjava/lang/String;[Ljava/lang/String;)J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v10, v11}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->updateWithRequestId(Landroid/os/Bundle;J)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v16

    .line 268
    goto/16 :goto_1

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private isBillingEnabledForAccount(Landroid/accounts/Account;I)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "billingApiVersion"    # I

    .prologue
    .line 698
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/finsky/billing/InAppBillingSetting;->isEnabled(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isBillingEnabledForPackage(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "billingApiVersion"    # I

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/MarketBillingService;->getPreferredAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100(Lcom/google/android/finsky/billing/iab/MarketBillingService;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForAccount(Landroid/accounts/Account;I)Z

    move-result v0

    return v0
.end method

.method private isRequestAllowed(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 707
    const/4 v0, 0x1

    return v0
.end method

.method private makeSignatureHash(Landroid/content/pm/PackageInfo;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;
    .locals 4
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    const/4 v3, 0x1

    .line 677
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;-><init>()V

    .line 678
    .local v1, "signatureHash":Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    .line 679
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasPackageName:Z

    .line 680
    iget v2, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    .line 681
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasVersionCode:Z

    .line 682
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    .line 683
    .local v0, "signature":[B
    invoke-static {v0}, Lcom/google/android/finsky/utils/Md5Util;->secureHashBytes([B)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    .line 684
    return-object v1
.end method

.method private setResponseCode(Landroid/os/Bundle;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/os/Bundle;
    .param p2, "code"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 281
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {p2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    return-object p1
.end method

.method private updateWithRequestId(Landroid/os/Bundle;J)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "requestId"    # J

    .prologue
    .line 309
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 310
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 313
    :goto_0
    return-object v0

    .line 312
    :cond_0
    const-string v0, "REQUEST_ID"

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 313
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method


# virtual methods
.method public checkBillingApiVersionSupport(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 4
    .param p1, "version"    # I

    .prologue
    const/4 v3, 0x0

    .line 406
    if-gtz p1, :cond_0

    .line 407
    const-string v0, "No billing API version given!"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 408
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 413
    :goto_0
    return-object v0

    .line 409
    :cond_0
    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 410
    const-string v0, "Unsupported (future) billing API version: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 413
    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method public checkBillingEnabled(ILjava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 5
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 354
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->checkBillingApiVersionSupport(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 355
    .local v1, "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 371
    .end local v1    # "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    :goto_0
    return-object v1

    .line 358
    .restart local v1    # "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    :cond_0
    if-nez p2, :cond_1

    .line 359
    const-string v2, "No packageName given!"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 362
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForPackage(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 363
    const-string v2, "Billing unavailable for this package."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 366
    :cond_2
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 367
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-nez v0, :cond_3

    .line 368
    const-string v2, "No package info for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 371
    :cond_3
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method public checkTypeSupported(ILjava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 4
    .param p1, "billingApiVersion"    # I
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 381
    const-string v0, "inapp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "subs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    const-string v0, "Unknown item type specified %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 399
    :goto_0
    return-object v0

    .line 387
    :cond_0
    if-ne p1, v3, :cond_1

    .line 388
    const-string v0, "inapp"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 389
    const-string v0, "Item type %s not supported in billing api version %d"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 393
    :cond_1
    if-ne p1, v1, :cond_2

    .line 394
    const-string v0, "subs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/finsky/config/G;->subscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 396
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 399
    :cond_2
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method public confirmNotifications(ILjava/lang/String;[Ljava/lang/String;)J
    .locals 8
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "notifyIds"    # [Ljava/lang/String;

    .prologue
    .line 539
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 540
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-nez v1, :cond_0

    .line 541
    const-wide/16 v4, -0x1

    .line 566
    :goto_0
    return-wide v4

    .line 543
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getNextInAppRequestId()J

    move-result-wide v4

    .line 544
    .local v4, "requestId":J
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/MarketBillingService;->getPreferredAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v3, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100(Lcom/google/android/finsky/billing/iab/MarketBillingService;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 545
    .local v0, "account":Landroid/accounts/Account;
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForAccount(Landroid/accounts/Account;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 546
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 549
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isRequestAllowed(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 550
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 554
    :cond_2
    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;-><init>()V

    .line 555
    .local v2, "request":Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->makeSignatureHash(Landroid/content/pm/PackageInfo;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 556
    iput-object p3, v2, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    .line 558
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/google/android/finsky/FinskyApp;->getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v3

    new-instance v6, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$2;

    invoke-direct {v6, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$2;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    new-instance v7, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;

    invoke-direct {v7, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/vending/remoting/api/VendingApi;->ackNotifications(Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method public getPurchaseInformation(ILjava/lang/String;J[Ljava/lang/String;)J
    .locals 9
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "nonce"    # J
    .param p5, "notifyIds"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 485
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 486
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-nez v1, :cond_0

    .line 487
    const-wide/16 v4, -0x1

    .line 524
    :goto_0
    return-wide v4

    .line 489
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getNextInAppRequestId()J

    move-result-wide v4

    .line 490
    .local v4, "requestId":J
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/MarketBillingService;->getPreferredAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v3, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100(Lcom/google/android/finsky/billing/iab/MarketBillingService;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 491
    .local v0, "account":Landroid/accounts/Account;
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForAccount(Landroid/accounts/Account;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 492
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 495
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isRequestAllowed(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 496
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 500
    :cond_2
    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;-><init>()V

    .line 502
    .local v2, "request":Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;
    iput p1, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    .line 503
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasBillingApiVersion:Z

    .line 504
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->makeSignatureHash(Landroid/content/pm/PackageInfo;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 505
    const-string v3, "SHA1withRSA"

    iput-object v3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 506
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasSignatureAlgorithm:Z

    .line 507
    iput-wide p3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    .line 508
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasNonce:Z

    .line 509
    iput-object p5, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    .line 511
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/google/android/finsky/FinskyApp;->getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v3

    new-instance v6, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$1;

    invoke-direct {v6, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$1;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    new-instance v7, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;

    invoke-direct {v7, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/vending/remoting/api/VendingApi;->getInAppPurchaseInformation(Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method public requestPurchase(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 20
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "itemType"    # Ljava/lang/String;
    .param p5, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    move-object/from16 v0, p2

    # invokes: Lcom/google/android/finsky/billing/iab/MarketBillingService;->getPreferredAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v5, v0}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100(Lcom/google/android/finsky/billing/iab/MarketBillingService;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v11

    .line 435
    .local v11, "account":Landroid/accounts/Account;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v14

    .line 440
    .local v14, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v11, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForAccount(Landroid/accounts/Account;I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 441
    :cond_0
    const/4 v5, 0x0

    .line 462
    :goto_0
    return-object v5

    .line 444
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getNextInAppRequestId()J

    move-result-wide v18

    .line 445
    .local v18, "requestId":J
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->computeSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v8

    .line 449
    .local v8, "signatureHash":Ljava/lang/String;
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocidStr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 450
    .local v12, "docidStr":Ljava/lang/String;
    new-instance v4, Lcom/google/android/finsky/billing/IabParameters;

    iget v7, v14, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v10, 0x0

    move/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v9, p5

    invoke-direct/range {v4 .. v10}, Lcom/google/android/finsky/billing/IabParameters;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 452
    .local v4, "iabParameters":Lcom/google/android/finsky/billing/IabParameters;
    invoke-static {}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-static {v12, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocid(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocid(Lcom/google/android/finsky/protos/Common$Docid;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocidStr(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setIabParameters(Lcom/google/android/finsky/billing/IabParameters;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v15

    .line 458
    .local v15, "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    move-wide/from16 v0, v18

    invoke-static {v11, v15, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/IabV2Activity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;J)Landroid/content/Intent;

    move-result-object v13

    .line 459
    .local v13, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    const/4 v6, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v6, v13, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 462
    .local v16, "pendingIntent":Landroid/app/PendingIntent;
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-static {v5, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_0
.end method

.method public restoreTransactions(ILjava/lang/String;J)J
    .locals 9
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "nonce"    # J

    .prologue
    const/4 v6, 0x1

    .line 580
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 581
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-nez v1, :cond_0

    .line 582
    const-wide/16 v4, -0x1

    .line 619
    :goto_0
    return-wide v4

    .line 584
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getNextInAppRequestId()J

    move-result-wide v4

    .line 585
    .local v4, "requestId":J
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/MarketBillingService;->getPreferredAccount(Ljava/lang/String;)Landroid/accounts/Account;
    invoke-static {v3, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->access$100(Lcom/google/android/finsky/billing/iab/MarketBillingService;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 586
    .local v0, "account":Landroid/accounts/Account;
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isBillingEnabledForAccount(Landroid/accounts/Account;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 587
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 590
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->isRequestAllowed(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 591
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v3, v3, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mNotifier:Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

    sget-object v6, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3, p2, v4, v5, v6}, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    goto :goto_0

    .line 595
    :cond_2
    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;-><init>()V

    .line 597
    .local v2, "request":Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;
    iput p1, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    .line 598
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasBillingApiVersion:Z

    .line 599
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->makeSignatureHash(Landroid/content/pm/PackageInfo;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 600
    const-string v3, "SHA1withRSA"

    iput-object v3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 601
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasSignatureAlgorithm:Z

    .line 602
    iput-wide p3, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    .line 603
    iput-boolean v6, v2, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasNonce:Z

    .line 604
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/google/android/finsky/FinskyApp;->getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v3

    new-instance v6, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$3;

    invoke-direct {v6, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$3;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    new-instance v7, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;

    invoke-direct {v7, p0, p2, v4, v5}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub$NotifyingErrorListener;-><init>(Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;Ljava/lang/String;J)V

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/vending/remoting/api/VendingApi;->restoreInAppTransactions(Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method public sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->getBillingRequest(Landroid/os/Bundle;)Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;

    move-result-object v0

    .line 179
    .local v0, "billingRequest":Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;
    if-nez v0, :cond_0

    .line 180
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 181
    .local v1, "response":Landroid/os/Bundle;
    const-string v2, "RESPONSE_CODE"

    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->stopSelf()V

    .line 188
    return-object v1

    .line 184
    .end local v1    # "response":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/billing/iab/MarketBillingService$Stub;->handleBillingRequest(Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingRequest;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .restart local v1    # "response":Landroid/os/Bundle;
    goto :goto_0
.end method
