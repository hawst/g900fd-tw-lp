.class Lcom/google/android/finsky/receivers/InstallerTask$7;
.super Landroid/os/AsyncTask;
.source "InstallerTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->startCopyFromExternalStorage(Lcom/google/android/finsky/appstate/AppStates$AppState;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$downloadUri:Landroid/net/Uri;

.field final synthetic val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2374
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$downloadUri:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/io/File;
    .locals 17
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2378
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v13}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v12

    .line 2379
    .local v12, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2380
    .local v5, "inputStream":Ljava/io/InputStream;
    const/4 v10, 0x0

    .line 2381
    .local v10, "outputStream":Ljava/io/OutputStream;
    const/4 v9, 0x0

    .line 2382
    .local v9, "outputFile":Ljava/io/File;
    const/4 v6, 0x0

    .line 2387
    .local v6, "keepOutputFile":Z
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2391
    .local v1, "appContext":Landroid/content/Context;
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$downloadUri:Landroid/net/Uri;

    invoke-virtual {v13, v14}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2403
    :try_start_2
    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    .line 2404
    .local v2, "cacheDir":Ljava/io/File;
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v2, v13, v14}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v13

    if-nez v13, :cond_1

    .line 2406
    const-string v13, "Could not make executable %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v2, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2407
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_0

    if-eqz v9, :cond_0

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .end local v2    # "cacheDir":Ljava/io/File;
    :cond_0
    :goto_0
    return-object v13

    .line 2392
    :catch_0
    move-exception v3

    .line 2393
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v14, "source-FileNotFoundException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v12, v14}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2394
    const-string v13, "FileNotFoundException %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$downloadUri:Landroid/net/Uri;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2395
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_0

    if-eqz v9, :cond_0

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2410
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "cacheDir":Ljava/io/File;
    :cond_1
    :try_start_4
    new-instance v8, Ljava/io/File;

    const-string v13, "copies"

    invoke-direct {v8, v2, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2411
    .local v8, "outputDirectory":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 2413
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v7, 0x1

    .line 2415
    .local v7, "madeReadable":Z
    :goto_1
    if-nez v7, :cond_3

    .line 2416
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v14, "out-dir-readable"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v12, v14}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2417
    const-string v13, "Could not make readable %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v8, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2418
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_0

    if-eqz v9, :cond_0

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2413
    .end local v7    # "madeReadable":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 2420
    .restart local v7    # "madeReadable":Z
    :cond_3
    :try_start_5
    const-string v13, ".apk"

    invoke-static {v12, v13, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    .line 2422
    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v9, v13, v14}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v13

    if-nez v13, :cond_4

    .line 2423
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v14, "out-file-readable"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v12, v14}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2424
    const-string v13, "Could not make readable %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v9, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2425
    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2426
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_0

    if-eqz v9, :cond_0

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 2428
    :cond_4
    :try_start_6
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2436
    .end local v10    # "outputStream":Ljava/io/OutputStream;
    .local v11, "outputStream":Ljava/io/OutputStream;
    :try_start_7
    invoke-static {v5, v11}, Lcom/google/android/finsky/utils/Utils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2442
    const/4 v6, 0x1

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_5

    if-eqz v9, :cond_5

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_5
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .restart local v10    # "outputStream":Ljava/io/OutputStream;
    move-object v13, v9

    goto/16 :goto_0

    .line 2429
    .end local v2    # "cacheDir":Ljava/io/File;
    .end local v7    # "madeReadable":Z
    .end local v8    # "outputDirectory":Ljava/io/File;
    :catch_1
    move-exception v4

    .line 2430
    .local v4, "e1":Ljava/io/IOException;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v14, "out-dir-IOException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v12, v14}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2431
    const-string v13, "IOException while opening: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2432
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_0

    if-eqz v9, :cond_0

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 2437
    .end local v4    # "e1":Ljava/io/IOException;
    .end local v10    # "outputStream":Ljava/io/OutputStream;
    .restart local v2    # "cacheDir":Ljava/io/File;
    .restart local v7    # "madeReadable":Z
    .restart local v8    # "outputDirectory":Ljava/io/File;
    .restart local v11    # "outputStream":Ljava/io/OutputStream;
    :catch_2
    move-exception v3

    .line 2438
    .local v3, "e":Ljava/io/IOException;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v14, "copy-IOException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v12, v14}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2439
    const-string v13, "IOException while copying: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2440
    const/4 v13, 0x0

    .line 2446
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_6

    if-eqz v9, :cond_6

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_6
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .restart local v10    # "outputStream":Ljava/io/OutputStream;
    goto/16 :goto_0

    .line 2446
    .end local v1    # "appContext":Landroid/content/Context;
    .end local v2    # "cacheDir":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "madeReadable":Z
    .end local v8    # "outputDirectory":Ljava/io/File;
    :catchall_0
    move-exception v13

    :goto_2
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2447
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2448
    if-nez v6, :cond_7

    if-eqz v9, :cond_7

    .line 2449
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_7
    throw v13

    .line 2446
    .end local v10    # "outputStream":Ljava/io/OutputStream;
    .restart local v1    # "appContext":Landroid/content/Context;
    .restart local v2    # "cacheDir":Ljava/io/File;
    .restart local v7    # "madeReadable":Z
    .restart local v8    # "outputDirectory":Ljava/io/File;
    .restart local v11    # "outputStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v13

    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .restart local v10    # "outputStream":Ljava/io/OutputStream;
    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2374
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$7;->doInBackground([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/io/File;)V
    .locals 7
    .param p1, "internalApk"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2462
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1900(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->val$downloadUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/download/DownloadQueue;->release(Landroid/net/Uri;)V

    .line 2465
    if-nez p1, :cond_0

    .line 2467
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2000(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 2469
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v0

    iput-boolean v4, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    .line 2470
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v0

    iput-boolean v4, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    .line 2471
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/16 v1, 0x1000

    const/16 v2, 0x2000

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2100(Lcom/google/android/finsky/receivers/InstallerTask;II)Z

    .line 2488
    :goto_0
    return-void

    .line 2477
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x7c

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2480
    const-string v0, "Successfully copied APK to update %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2483
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/16 v1, 0x3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1600(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    .line 2487
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$7;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2374
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$7;->onPostExecute(Ljava/io/File;)V

    return-void
.end method
