.class Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;
.super Ljava/lang/Object;
.source "PlayCardRateAndSuggestContentScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->onLayout(ZIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;)Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;)Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 136
    .local v0, "firstCard":Lcom/google/android/play/layout/PlayCardViewBase;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->scrollTo(II)V

    .line 137
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 139
    .end local v0    # "firstCard":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_0
    return-void
.end method
