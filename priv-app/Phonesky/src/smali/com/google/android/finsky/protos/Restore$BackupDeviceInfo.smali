.class public final Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Restore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Restore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackupDeviceInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;


# instance fields
.field public androidId:J

.field public hasAndroidId:Z

.field public hasLastCheckinTimeMs:Z

.field public hasName:Z

.field public hasNumDocuments:Z

.field public hasRestoreToken:Z

.field public lastCheckinTimeMs:J

.field public name:Ljava/lang/String;

.field public numDocuments:I

.field public restoreToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->clear()Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .line 47
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    sput-object v0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 50
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    .line 51
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasAndroidId:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    .line 53
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasName:Z

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasRestoreToken:Z

    .line 56
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    .line 57
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasLastCheckinTimeMs:Z

    .line 58
    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    .line 59
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasNumDocuments:Z

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->cachedSize:I

    .line 61
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 87
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 88
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasAndroidId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 89
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 93
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasRestoreToken:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 97
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasNumDocuments:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    if-eqz v1, :cond_7

    .line 101
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasLastCheckinTimeMs:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 105
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 116
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 117
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 121
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    :sswitch_0
    return-object p0

    .line 127
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    .line 128
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasAndroidId:Z

    goto :goto_0

    .line 132
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    .line 133
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasName:Z

    goto :goto_0

    .line 137
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    .line 138
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasRestoreToken:Z

    goto :goto_0

    .line 142
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    .line 143
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasNumDocuments:Z

    goto :goto_0

    .line 147
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    .line 148
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasLastCheckinTimeMs:Z

    goto :goto_0

    .line 117
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 67
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasAndroidId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 70
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 71
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 73
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasRestoreToken:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 74
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 76
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasNumDocuments:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    if-eqz v0, :cond_7

    .line 77
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->numDocuments:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 79
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->hasLastCheckinTimeMs:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 80
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 82
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 83
    return-void
.end method
