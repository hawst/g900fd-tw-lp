.class public abstract Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
.super Landroid/support/v4/app/Fragment;
.source "BillingProfileBaseFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;,
        Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;,
        Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;
    }
.end annotation


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field private final mBillingProfileListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;

.field protected mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

.field private final mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;

.field private mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

.field protected mErrorIndicator:Landroid/view/View;

.field protected mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mLastBillingProfileStateInstance:I

.field private mLastCarrierBillingStateInstance:I

.field protected mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

.field private mProfileDirty:Z

.field protected mProfileView:Landroid/view/View;

.field private mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field protected mProgressIndicator:Landroid/view/View;

.field protected mPurchaseContextToken:Ljava/lang/String;

.field private mStoredValueInstrumentId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 116
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;

    .line 118
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;

    .line 127
    iput v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    .line 129
    iput v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    .line 879
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/billing/ProgressDialogFragment;)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->addDcb2()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onDcbError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$BillingProfileListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->addGenericInstrument(Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->ensureProvisionedAndAddDcb2()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->showLoading()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->handleSuccess(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->handleError()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    return p1
.end method

.method private addDcb2()V
    .locals 2

    .prologue
    .line 563
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb2Activity;->createIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 564
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 565
    return-void
.end method

.method private addGenericInstrument(Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)V
    .locals 2
    .param p1, "instrument"    # Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .prologue
    .line 510
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddGenericInstrumentActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)Landroid/content/Intent;

    move-result-object v0

    .line 511
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 512
    return-void
.end method

.method private billingProfileOptionToActionEntry(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;[B)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .locals 9
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .param p2, "paymentsIntegratorCommonToken"    # [B

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 376
    iget v3, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    packed-switch v3, :pswitch_data_0

    .line 470
    const-string v3, "Skipping unknown option: type=%d, displayTitle=%s"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 472
    :goto_0
    return-object v2

    .line 378
    :pswitch_0
    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    new-instance v3, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$2;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 392
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->isDcb3SetupOption(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getDcb3Action(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getDcb2Action(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    move-result-object v2

    goto :goto_0

    .line 394
    :pswitch_2
    iget-object v0, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 395
    .local v0, "instrument":Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    iget-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 397
    .local v1, "metadata":Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->supportsGenericInstruments()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "PAYPAL"

    iget-object v4, v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    if-ne v3, v6, :cond_1

    .line 399
    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    new-instance v3, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$3;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 412
    :cond_1
    const-string v3, "Unsupported FlowType: flowType=%d, instrumentType=%s, title=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->flowType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 417
    .end local v0    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    .end local v1    # "metadata":Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;
    :pswitch_3
    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    new-instance v3, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$4;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 429
    :pswitch_4
    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    new-instance v3, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$5;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$5;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 441
    :pswitch_5
    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    new-instance v3, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;[B)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method private ensureProvisionedAndAddDcb2()V
    .locals 3

    .prologue
    .line 541
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->isProvisioned(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->addDcb2()V

    .line 560
    :goto_0
    return-void

    .line 545
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    .line 547
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_1

    .line 548
    const-string v0, "Not expected to have a carrier billing fragment."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 554
    :cond_1
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    .line 555
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 556
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    const-string v2, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private getDcb3Action(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .locals 2
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .prologue
    .line 497
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$8;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)V

    .line 506
    .local v0, "onClickListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    invoke-direct {v1, p1, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method private handleError()V
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getSubstate()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 289
    const-string v0, "Don\'t know how to handle error substate %d, cancel."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getSubstate()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    const v0, 0x7f0c01e0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onFatalError(Ljava/lang/String;)V

    .line 294
    :goto_0
    return-void

    .line 281
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getErrorMessageHtml()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onFatalError(Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onFatalError(Ljava/lang/String;)V

    goto :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSuccess(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;)V
    .locals 1
    .param p1, "billingProfile"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    .line 317
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->renderProfile()V

    .line 318
    return-void
.end method

.method private isDcb3SetupOption(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Z
    .locals 2
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .prologue
    .line 515
    iget-object v0, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    iget v0, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->apiVersion:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onDcbError(Ljava/lang/String;)V
    .locals 5
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->renderProfile()V

    .line 306
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 307
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 310
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 311
    .local v1, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "BillingProfileFragment.errorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 312
    return-void
.end method

.method private showLoading()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 266
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->logLoading()V

    .line 269
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mErrorIndicator:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mErrorIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_0
    return-void
.end method


# virtual methods
.method protected addCreditCard()V
    .locals 2

    .prologue
    .line 521
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddCreditCardActivity;->createIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 522
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 523
    return-void
.end method

.method protected addDcb3(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)V
    .locals 2
    .param p1, "instrumentStatus"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .prologue
    .line 532
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3Activity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)Landroid/content/Intent;

    move-result-object v0

    .line 533
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 534
    return-void
.end method

.method protected addInstrumentManager([B[BI)V
    .locals 2
    .param p1, "commonToken"    # [B
    .param p2, "actionToken"    # [B
    .param p3, "requestCode"    # I

    .prologue
    .line 526
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p1, p2}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerActivity;->createIntent(Ljava/lang/String;[B[B)Landroid/content/Intent;

    move-result-object v0

    .line 528
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0, p3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 529
    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 796
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 797
    return-void
.end method

.method protected createBillingProfileSidecar()V
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.billingProfileSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mPurchaseContextToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    const-string v2, "BillingProfileFragment.billingProfileSidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 223
    :cond_0
    return-void
.end method

.method protected abstract getBackgroundEventServerLogsCookie()[B
.end method

.method protected abstract getBillingProfileRequestEnum()I
.end method

.method protected abstract getCreditCardEventType()I
.end method

.method protected getDcb2Action(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .locals 2
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$7;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$7;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V

    .line 493
    .local v0, "onClickListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    invoke-direct {v1, p1, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method protected abstract getDcbEventType()I
.end method

.method protected abstract getGenericInstrumentEventType()I
.end method

.method protected abstract getRedeemEventType()I
.end method

.method protected abstract getTopupEventType()I
.end method

.method protected abstract logLoading()V
.end method

.method protected abstract logScreen()V
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x1

    .line 635
    packed-switch p1, :pswitch_data_0

    .line 659
    :pswitch_0
    const-string v6, "Unexpected requestCode=%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 660
    const/4 v0, -0x1

    .line 663
    .local v0, "backgroundEventType":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getBackgroundEventServerLogsCookie()[B

    move-result-object v4

    .line 665
    .local v4, "serverLogsCookie":[B
    packed-switch p1, :pswitch_data_1

    .line 721
    :cond_0
    :goto_1
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_1

    .line 723
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->logScreen()V

    .line 726
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 727
    return-void

    .line 637
    .end local v0    # "backgroundEventType":I
    .end local v4    # "serverLogsCookie":[B
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getCreditCardEventType()I

    move-result v0

    .line 638
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 640
    .end local v0    # "backgroundEventType":I
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getDcbEventType()I

    move-result v0

    .line 641
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 644
    .end local v0    # "backgroundEventType":I
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getGenericInstrumentEventType()I

    move-result v0

    .line 645
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 647
    .end local v0    # "backgroundEventType":I
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getRedeemEventType()I

    move-result v0

    .line 648
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 650
    .end local v0    # "backgroundEventType":I
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getTopupEventType()I

    move-result v0

    .line 651
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 653
    .end local v0    # "backgroundEventType":I
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getCreditCardEventType()I

    move-result v0

    .line 654
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 656
    .end local v0    # "backgroundEventType":I
    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getGenericInstrumentEventType()I

    move-result v0

    .line 657
    .restart local v0    # "backgroundEventType":I
    goto :goto_0

    .line 669
    .restart local v4    # "serverLogsCookie":[B
    :pswitch_9
    if-ne p2, v10, :cond_0

    .line 671
    const-string v6, "instrument_id"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 672
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v6, v0, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 673
    const-string v6, "instrument_id"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 675
    .local v1, "instrumentId":Ljava/lang/String;
    const-string v6, "redeemed_offer_message_html"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 677
    .local v3, "redeemedOfferHtml":Ljava/lang/String;
    invoke-virtual {p0, v1, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 682
    .end local v1    # "instrumentId":Ljava/lang/String;
    .end local v3    # "redeemedOfferHtml":Ljava/lang/String;
    :pswitch_a
    if-ne p2, v10, :cond_0

    .line 683
    const-string v6, "RedeemCodeBaseActivity.redeem_code_result"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .line 685
    .local v2, "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    if-eqz v2, :cond_0

    .line 686
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v6, v0, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 687
    invoke-virtual {v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getStoredValueInstrumentId()Ljava/lang/String;

    move-result-object v5

    .line 689
    .local v5, "storedValueInstrumentId":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 691
    iput-object v5, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    .line 692
    invoke-virtual {v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getRedeemedOfferHtml()Ljava/lang/String;

    move-result-object v3

    .line 693
    .restart local v3    # "redeemedOfferHtml":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p0, v6, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 696
    .end local v3    # "redeemedOfferHtml":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V

    goto :goto_1

    .line 702
    .end local v2    # "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    .end local v5    # "storedValueInstrumentId":Ljava/lang/String;
    :pswitch_b
    if-ne p2, v10, :cond_0

    .line 703
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v6, v0, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 704
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p0, v6, v11}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 710
    :pswitch_c
    if-ne p2, v10, :cond_0

    .line 711
    const-string v6, "instrument_id"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 713
    .restart local v1    # "instrumentId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 714
    iget-object v6, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v6, v0, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 715
    invoke-virtual {p0, v1, v11}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 635
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 665
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    .line 162
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileFragment.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    .line 164
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileFragment.purchaseContextToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mPurchaseContextToken:Ljava/lang/String;

    .line 165
    if-eqz p1, :cond_1

    .line 166
    const-string v0, "BillingProfileFragment.profile"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    .line 170
    :cond_0
    const-string v0, "BillingProfileFragment.lastBillingProfileStateInstance"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    .line 172
    const-string v0, "BillingProfileFragment.lastCarrierBillingStateInstance"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    .line 174
    const-string v0, "PurchaseFlowBillingProfileFragment.storedValueInstrumentId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    .line 176
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 177
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 254
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    .line 257
    iput v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    .line 258
    iput v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    .line 259
    return-void
.end method

.method protected abstract onFatalError(Ljava/lang/String;)V
.end method

.method protected abstract onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 810
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 813
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 249
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 250
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 803
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 806
    :cond_0
    return-void
.end method

.method protected abstract onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 227
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$1;-><init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->initializeStorageAndParams(Landroid/content/Context;ZLjava/lang/Runnable;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/BillingProfileBaseFragment$CarrierBillingProvisioningListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 241
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 182
    const-string v0, "BillingProfileFragment.profile"

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 183
    const-string v0, "BillingProfileFragment.lastBillingProfileStateInstance"

    iget v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastBillingProfileStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v0, "BillingProfileFragment.lastCarrierBillingStateInstance"

    iget v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mLastCarrierBillingStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 185
    const-string v0, "PurchaseFlowBillingProfileFragment.storedValueInstrumentId"

    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->createBillingProfileSidecar()V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    .line 206
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingProgressFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 208
    return-void
.end method

.method protected abstract onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 190
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mProfileView not set up."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressIndicator:Landroid/view/View;

    if-nez v0, :cond_1

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mProgressIndicator not set up."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->renderProfile()V

    .line 198
    return-void
.end method

.method protected abstract redeemCheckoutCode()V
.end method

.method protected abstract renderActions(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract renderInstruments([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V
.end method

.method protected renderProfile()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 321
    iget-boolean v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    if-nez v8, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iput-boolean v10, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileDirty:Z

    .line 325
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    if-eqz v8, :cond_0

    .line 326
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v8, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->shouldRender([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 330
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v2, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .local v2, "arr$":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v4, v2, v3

    .line 332
    .local v4, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget v8, v4, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    const/4 v9, 0x7

    if-ne v8, v9, :cond_2

    iget-object v8, v4, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    if-eqz v8, :cond_2

    .line 334
    iget-object v7, v4, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    .line 335
    .local v7, "storedValueInfo":Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
    iget v8, v7, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    const/16 v9, 0x21

    if-ne v8, v9, :cond_2

    .line 336
    iget-object v8, v4, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    .line 330
    .end local v7    # "storedValueInfo":Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 340
    .end local v4    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v8, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->renderInstruments([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V

    .line 341
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v8, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v8, v8

    invoke-static {v8}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 343
    .local v0, "actionEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;>;"
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v2, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .local v2, "arr$":[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    array-length v5, v2

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_5

    aget-object v6, v2, v3

    .line 344
    .local v6, "option":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v8, v8, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    invoke-direct {p0, v6, v8}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->billingProfileOptionToActionEntry(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;[B)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    move-result-object v1

    .line 346
    .local v1, "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    if-eqz v1, :cond_4

    .line 347
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 350
    .end local v1    # "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .end local v6    # "option":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    :cond_5
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->renderActions(Ljava/util/List;)V

    .line 351
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 352
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mErrorIndicator:Landroid/view/View;

    if-eqz v8, :cond_6

    .line 353
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mErrorIndicator:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    .line 355
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileView:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 356
    iget-object v8, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mProfileView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    .line 357
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->logScreen()V

    goto :goto_0
.end method

.method protected requestBillingProfile()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 785
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 786
    .local v0, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v1, v1, v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->addPrepareOrBillingProfileParams(ZZLjava/util/Map;)V

    .line 787
    const-string v1, "bpif"

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->getBillingProfileRequestEnum()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 789
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->start(Ljava/util/Map;)V

    .line 790
    return-void
.end method

.method protected shouldRender([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Z
    .locals 1
    .param p1, "instruments"    # [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    .line 578
    const/4 v0, 0x1

    return v0
.end method

.method protected supportsGenericInstruments()Z
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x1

    return v0
.end method

.method protected topup(Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)V
    .locals 2
    .param p1, "topupInfo"    # Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .prologue
    .line 573
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)Landroid/content/Intent;

    move-result-object v0

    .line 574
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 575
    return-void
.end method
