.class Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;
.super Ljava/lang/Object;
.source "DetailedPermissionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DetailedPermissionBucket"
.end annotation


# instance fields
.field public final mBucketIcon:I

.field public final mBucketTitle:I

.field public final mExistingPermissionDescriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mNewPermissionDescriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;)V
    .locals 1
    .param p1, "newPermissionBucket"    # Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .param p2, "existingBucket"    # Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    if-eqz p1, :cond_0

    .line 182
    iget v0, p1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketIcon:I

    iput v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketIcon:I

    .line 183
    iget v0, p1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    iput v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketTitle:I

    .line 189
    :goto_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    :goto_1
    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mNewPermissionDescriptions:Ljava/util/List;

    .line 191
    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    :goto_2
    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mExistingPermissionDescriptions:Ljava/util/List;

    .line 193
    return-void

    .line 185
    :cond_0
    iget v0, p2, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketIcon:I

    iput v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketIcon:I

    .line 186
    iget v0, p2, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    iput v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketTitle:I

    goto :goto_0

    .line 189
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 191
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method
