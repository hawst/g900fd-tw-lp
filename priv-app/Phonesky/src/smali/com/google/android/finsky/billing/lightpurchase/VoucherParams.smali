.class public Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;
.super Ljava/lang/Object;
.source "VoucherParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final autoApply:Z

.field public final hasVouchers:Z

.field public final selectedVoucherId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams$1;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->selectedVoucherId:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->autoApply:Z

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->hasVouchers:Z

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 38
    goto :goto_0

    :cond_1
    move v1, v2

    .line 39
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/finsky/billing/lightpurchase/VoucherParams$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/VoucherParams$1;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "selectedVoucherId"    # Ljava/lang/String;
    .param p2, "autoApply"    # Z
    .param p3, "hasVouchers"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->selectedVoucherId:Ljava/lang/String;

    .line 32
    iput-boolean p2, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->autoApply:Z

    .line 33
    iput-boolean p3, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->hasVouchers:Z

    .line 34
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->selectedVoucherId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->autoApply:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 51
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->hasVouchers:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 52
    return-void

    :cond_0
    move v0, v2

    .line 50
    goto :goto_0

    :cond_1
    move v1, v2

    .line 51
    goto :goto_1
.end method
