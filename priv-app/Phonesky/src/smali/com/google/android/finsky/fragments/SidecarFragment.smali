.class public Lcom/google/android/finsky/fragments/SidecarFragment;
.super Landroid/support/v4/app/Fragment;
.source "SidecarFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/fragments/SidecarFragment$Listener;
    }
.end annotation


# instance fields
.field private mCreated:Z

.field private mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

.field private mState:I

.field private mStateInstance:I

.field private mSubstate:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    .line 125
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    .line 130
    return-void
.end method

.method private notifyListener()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/fragments/SidecarFragment$Listener;->onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V

    .line 257
    :cond_0
    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    return v0
.end method

.method public getStateInstance()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    return v0
.end method

.method public getSubstate()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mSubstate:I

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 152
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/fragments/SidecarFragment;->setRetainInstance(Z)V

    .line 154
    if-eqz p1, :cond_0

    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    if-eqz v0, :cond_1

    .line 158
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;->notifyListener()V

    .line 160
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mCreated:Z

    .line 161
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mCreated:Z

    .line 166
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 167
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 171
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 172
    const-string v0, "SidecarFragment.state"

    iget v1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    const-string v0, "SidecarFragment.substate"

    iget v1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mSubstate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    const-string v0, "SidecarFragment.stateInstance"

    iget v1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 186
    const-string v0, "SidecarFragment.state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    .line 187
    const-string v0, "SidecarFragment.substate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mSubstate:I

    .line 188
    const-string v0, "SidecarFragment.stateInstance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    .line 189
    iget v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 190
    const-string v0, "Restoring after serialization in RUNNING, resetting to INIT."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    invoke-virtual {p0, v2, v2}, Lcom/google/android/finsky/fragments/SidecarFragment;->setState(II)V

    .line 193
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mListener:Lcom/google/android/finsky/fragments/SidecarFragment$Listener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mCreated:Z

    if-eqz v0, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;->notifyListener()V

    .line 210
    :cond_0
    return-void
.end method

.method protected setState(II)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "substate"    # I

    .prologue
    .line 246
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 247
    iput p1, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mState:I

    .line 248
    iput p2, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mSubstate:I

    .line 249
    iget v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/fragments/SidecarFragment;->mStateInstance:I

    .line 250
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;->notifyListener()V

    .line 251
    return-void
.end method
