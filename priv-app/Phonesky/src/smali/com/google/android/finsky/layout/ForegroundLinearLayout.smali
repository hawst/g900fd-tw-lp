.class public Lcom/google/android/finsky/layout/ForegroundLinearLayout;
.super Landroid/widget/LinearLayout;
.source "ForegroundLinearLayout.java"


# static fields
.field private static IS_HC_OR_ABOVE:Z

.field private static IS_JBMR1_OR_ABOVE:Z


# instance fields
.field private mForegroundBoundsChanged:Z

.field private mForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mForegroundPaddingBottom:I

.field private mForegroundPaddingLeft:I

.field private mForegroundPaddingRight:I

.field private mForegroundPaddingTop:I

.field private final mOverlayBounds:Landroid/graphics/Rect;

.field private final mSelfBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->IS_HC_OR_ABOVE:Z

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->IS_JBMR1_OR_ABOVE:Z

    return-void

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0

    :cond_1
    move v1, v2

    .line 25
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 30
    iput v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 31
    iput v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 32
    iput v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 34
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 35
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 36
    iput-boolean v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 43
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010109

    aput v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 45
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 46
    .local v1, "foregroundDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 49
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 50
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 157
    iget-object v6, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 159
    .local v6, "foreground":Landroid/graphics/drawable/Drawable;
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 161
    iget-object v3, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mSelfBounds:Landroid/graphics/Rect;

    .line 162
    .local v3, "selfBounds":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    .line 164
    .local v4, "overlayBounds":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->getWidth()I

    move-result v8

    .line 165
    .local v8, "w":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->getHeight()I

    move-result v7

    .line 167
    .local v7, "h":I
    iget v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    iget v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingTop:I

    iget v2, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingRight:I

    sub-int v2, v8, v2

    iget v9, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    sub-int v9, v7, v9

    invoke-virtual {v3, v0, v1, v2, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 171
    sget-boolean v0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->IS_JBMR1_OR_ABOVE:Z

    if-eqz v0, :cond_2

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->getLayoutDirection()I

    move-result v5

    .line 173
    .local v5, "layoutDirection":I
    const/16 v0, 0x77

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/GravityCompat;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 179
    .end local v5    # "layoutDirection":I
    :goto_0
    invoke-virtual {v6, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 182
    .end local v3    # "selfBounds":Landroid/graphics/Rect;
    .end local v4    # "overlayBounds":Landroid/graphics/Rect;
    .end local v7    # "h":I
    .end local v8    # "w":I
    :cond_0
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 184
    .end local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void

    .line 177
    .restart local v3    # "selfBounds":Landroid/graphics/Rect;
    .restart local v4    # "overlayBounds":Landroid/graphics/Rect;
    .restart local v6    # "foreground":Landroid/graphics/drawable/Drawable;
    .restart local v7    # "h":I
    .restart local v8    # "w":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public drawableHotspotChanged(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 122
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->drawableHotspotChanged(FF)V

    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 127
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 118
    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/widget/LinearLayout;->jumpDrawablesToCurrentState()V

    .line 107
    sget-boolean v0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->IS_HC_OR_ABOVE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 110
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 141
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 144
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 148
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundBoundsChanged:Z

    .line 151
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v3, 0x0

    .line 53
    iget-object v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v1, p1, :cond_3

    .line 54
    iget-object v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 56
    iget-object v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 60
    iput v3, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 61
    iput v3, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 62
    iput v3, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 63
    iput v3, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 65
    if-eqz p1, :cond_4

    .line 66
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->setWillNotDraw(Z)V

    .line 67
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 68
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 71
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 72
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingLeft:I

    .line 74
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingTop:I

    .line 75
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingRight:I

    .line 76
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundPaddingBottom:I

    .line 81
    .end local v0    # "padding":Landroid/graphics/Rect;
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->requestLayout()V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->invalidate()V

    .line 84
    :cond_3
    return-void

    .line 79
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->setWillNotDraw(Z)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    iget-object v2, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 134
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 135
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 137
    .end local v0    # "isVisible":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 134
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->mForegroundDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
