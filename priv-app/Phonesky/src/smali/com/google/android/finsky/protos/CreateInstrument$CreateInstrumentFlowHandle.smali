.class public final Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateInstrumentFlowHandle"
.end annotation


# instance fields
.field public apiVersion:I

.field public flowHandle:[B

.field public flowType:I

.field public hasApiVersion:Z

.field public hasFlowHandle:Z

.field public hasFlowType:Z

.field public hasInstrumentType:Z

.field public instrumentType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->clear()Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    .line 47
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    .line 51
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasInstrumentType:Z

    .line 52
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    .line 53
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowHandle:Z

    .line 54
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    .line 55
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowType:Z

    .line 56
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    .line 57
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasApiVersion:Z

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->cachedSize:I

    .line 59
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 83
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasInstrumentType:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowHandle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 88
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowType:Z

    if-eqz v1, :cond_5

    .line 92
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasApiVersion:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    if-eqz v1, :cond_7

    .line 96
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 108
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 112
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    :sswitch_0
    return-object p0

    .line 118
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    .line 119
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasInstrumentType:Z

    goto :goto_0

    .line 123
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    .line 124
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowHandle:Z

    goto :goto_0

    .line 128
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 129
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 132
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    .line 133
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowType:Z

    goto :goto_0

    .line 139
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    .line 140
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasApiVersion:Z

    goto :goto_0

    .line 108
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 129
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasInstrumentType:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->instrumentType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 68
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowHandle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 69
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowHandle:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 71
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasFlowType:Z

    if-eqz v0, :cond_5

    .line 72
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->flowType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 74
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->hasApiVersion:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    if-eqz v0, :cond_7

    .line 75
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowHandle;->apiVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 77
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 78
    return-void
.end method
