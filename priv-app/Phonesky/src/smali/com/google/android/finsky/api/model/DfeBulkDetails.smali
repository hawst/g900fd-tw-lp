.class public Lcom/google/android/finsky/api/model/DfeBulkDetails;
.super Lcom/google/android/finsky/api/model/DfeModel;
.source "DfeBulkDetails.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/DfeModel;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mDocids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/util/List;)V
    .locals 1
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeApi;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "docids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 38
    iput-object p2, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDocids:Ljava/util/List;

    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDocids:Ljava/util/List;

    invoke-interface {p1, v0, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 40
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/finsky/api/DfeApi;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V
    .locals 4
    .param p1, "mockDfe"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "responses"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 49
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDocids:Ljava/util/List;

    .line 50
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 52
    new-instance v2, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .line 53
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    array-length v3, p2

    new-array v3, v3, [Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    iput-object v3, v2, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;->entry:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .line 54
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 55
    new-instance v1, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;-><init>()V

    .line 56
    .local v1, "singleEntry":Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    aget-object v2, p2, v0

    iput-object v2, v1, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 57
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;->entry:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    aput-object v1, v2, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    .end local v1    # "singleEntry":Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    :cond_0
    return-void
.end method


# virtual methods
.method public getDfeApi()Lcom/google/android/finsky/api/DfeApi;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method public getDocuments()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v3, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    if-nez v3, :cond_1

    .line 67
    const/4 v2, 0x0

    .line 83
    :cond_0
    return-object v2

    .line 70
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;->entry:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 72
    iget-object v3, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;->entry:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    aget-object v3, v3, v1

    iget-object v0, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 73
    .local v0, "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-nez v0, :cond_3

    .line 76
    sget-boolean v3, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 77
    const-string v3, "Null document for requested docid: %s "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mDocids:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    :cond_3
    new-instance v3, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v3, v0}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeBulkDetails;->mBulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->notifyDataSetChanged()V

    .line 108
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->onResponse(Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;)V

    return-void
.end method
