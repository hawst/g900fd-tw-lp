.class public Lcom/google/android/finsky/activities/TabbedAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "TabbedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/TabbedAdapter$1;,
        Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;,
        Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    }
.end annotation


# instance fields
.field private final mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field private final mBackendId:I

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private final mContext:Landroid/content/Context;

.field private mCurrentlySelectedTab:I

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSupportsOverlappingActionBar:Z

.field mTabDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/TabbedAdapter$TabData;",
            ">;"
        }
    .end annotation
.end field

.field private final mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/play/image/BitmapLoader;[Lcom/google/android/finsky/protos/Browse$BrowseTab;[Lcom/google/android/finsky/protos/Browse$QuickLink;IIILcom/google/android/finsky/utils/ObjectMap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p7, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p8, "browseTabs"    # [Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .param p9, "quickLinks"    # [Lcom/google/android/finsky/protos/Browse$QuickLink;
    .param p10, "quickLinkTabIndex"    # I
    .param p11, "quickLinkFallbackTabIndex"    # I
    .param p12, "backendId"    # I
    .param p13, "tabRestoreState"    # Lcom/google/android/finsky/utils/ObjectMap;
    .param p14, "parent"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p15, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .param p16, "supportsOverlappingActionBar"    # Z

    .prologue
    .line 113
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    .line 57
    new-instance v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;-><init>(Lcom/google/android/finsky/activities/TabbedAdapter$1;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    .line 114
    iput-object p1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mContext:Landroid/content/Context;

    .line 115
    iput-object p2, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 116
    iput-object p3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 117
    iput-object p4, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 118
    iput-object p5, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 119
    iput-object p6, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 120
    iput-object p7, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 122
    iput p12, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBackendId:I

    .line 123
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 124
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mCurrentlySelectedTab:I

    .line 125
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .line 126
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mSupportsOverlappingActionBar:Z

    .line 128
    move-object/from16 v0, p13

    invoke-direct {p0, p8, v0}, Lcom/google/android/finsky/activities/TabbedAdapter;->generateTabList([Lcom/google/android/finsky/protos/Browse$BrowseTab;Lcom/google/android/finsky/utils/ObjectMap;)V

    .line 129
    invoke-direct {p0, p9, p10, p11}, Lcom/google/android/finsky/activities/TabbedAdapter;->generateQuickLinks([Lcom/google/android/finsky/protos/Browse$QuickLink;II)V

    .line 133
    invoke-direct {p0, p9}, Lcom/google/android/finsky/activities/TabbedAdapter;->showQuickLinksIfNoListsExist([Lcom/google/android/finsky/protos/Browse$QuickLink;)V

    .line 134
    return-void
.end method

.method private generateQuickLinks([Lcom/google/android/finsky/protos/Browse$QuickLink;II)V
    .locals 11
    .param p1, "quickLinks"    # [Lcom/google/android/finsky/protos/Browse$QuickLink;
    .param p2, "quickLinkTabIndex"    # I
    .param p3, "quickLinkFallbackTabIndex"    # I

    .prologue
    .line 158
    iget-object v9, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const/4 v6, 0x0

    .line 162
    .local v6, "quickLinkInfos":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    if-eqz p1, :cond_2

    array-length v9, p1

    if-lez v9, :cond_2

    .line 163
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 164
    move-object v0, p1

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 165
    .local v3, "link":Lcom/google/android/finsky/protos/Browse$QuickLink;
    new-instance v9, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iget v10, v3, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    invoke-direct {v9, v3, v10}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;-><init>(Lcom/google/android/finsky/protos/Browse$QuickLink;I)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 168
    .end local v0    # "arr$":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "link":Lcom/google/android/finsky/protos/Browse$QuickLink;
    :cond_2
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 170
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 171
    .local v8, "quickLinkTabOutput":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 172
    .local v5, "quickLinkFallbackTabOutput":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    iget-object v9, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9, v6, v8, v5}, Lcom/google/android/finsky/adapters/QuickLinkHelper;->getQuickLinksForStream(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 175
    iget-object v9, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v9, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 176
    .local v7, "quickLinkTab":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    const/4 v9, -0x1

    if-eq p3, v9, :cond_3

    iget-object v9, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v9, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    move-object v4, v9

    .line 179
    .local v4, "quickLinkFallbackTab":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    :goto_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    invoke-interface {v8, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iput-object v9, v7, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 181
    if-eqz v4, :cond_0

    .line 183
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    invoke-interface {v5, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iput-object v9, v4, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    goto :goto_0

    .line 176
    .end local v4    # "quickLinkFallbackTab":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private generateTabList([Lcom/google/android/finsky/protos/Browse$BrowseTab;Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 12
    .param p1, "browseTabs"    # [Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .param p2, "restoreState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 370
    iget-object v7, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 373
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, p1

    if-ge v1, v7, :cond_1

    .line 374
    aget-object v0, p1, v1

    .line 375
    .local v0, "browseTab":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    new-instance v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    iget-object v7, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v7, v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    :goto_1
    invoke-direct {v6, v0, v7}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;-><init>(Lcom/google/android/finsky/protos/Browse$BrowseTab;Z)V

    .line 376
    .local v6, "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    new-instance v7, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/16 v8, 0x193

    iget-object v9, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v7, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->elementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    .line 378
    iget-object v7, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 375
    .end local v6    # "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    :cond_0
    const/4 v7, 0x0

    goto :goto_1

    .line 382
    .end local v0    # "browseTab":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/TabbedAdapter;->restoreScrollPositions(Lcom/google/android/finsky/utils/ObjectMap;)Ljava/util/List;

    move-result-object v5

    .line 383
    .local v5, "scrollPositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/ObjectMap;>;"
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/TabbedAdapter;->restoreDfeLists(Lcom/google/android/finsky/utils/ObjectMap;)Ljava/util/List;

    move-result-object v4

    .line 384
    .local v4, "restoredDfeLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/DfeList;>;"
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v7, v8, :cond_4

    const/4 v2, 0x1

    .line 386
    .local v2, "restoreDfeLists":Z
    :goto_2
    if-eqz v5, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ne v7, v8, :cond_5

    const/4 v3, 0x1

    .line 389
    .local v3, "restoreScrollPositions":Z
    :goto_3
    const/4 v1, 0x0

    :goto_4
    array-length v7, p1

    if-ge v1, v7, :cond_6

    .line 390
    iget-object v7, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 392
    .restart local v6    # "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    if-eqz v2, :cond_2

    .line 393
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/DfeList;

    iput-object v7, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 395
    :cond_2
    if-eqz v3, :cond_3

    .line 396
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/utils/ObjectMap;

    # setter for: Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;
    invoke-static {v6, v7}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->access$102(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/utils/ObjectMap;)Lcom/google/android/finsky/utils/ObjectMap;

    .line 389
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 384
    .end local v2    # "restoreDfeLists":Z
    .end local v3    # "restoreScrollPositions":Z
    .end local v6    # "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 386
    .restart local v2    # "restoreDfeLists":Z
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 399
    .restart local v3    # "restoreScrollPositions":Z
    :cond_6
    return-void
.end method

.method private getDfeLists()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/api/model/DfeList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 239
    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 240
    :cond_0
    const/4 v0, 0x0

    .line 246
    :cond_1
    return-object v0

    .line 242
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 243
    .local v0, "dfeLists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/api/model/DfeList;>;"
    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 244
    .local v2, "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    iget-object v3, v2, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getTabInstanceStates()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/utils/ObjectMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 221
    :cond_0
    const/4 v1, 0x0

    .line 231
    :cond_1
    return-object v1

    .line 223
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v1, "instanceStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/utils/ObjectMap;>;"
    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 225
    .local v2, "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    iget-object v3, v2, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    if-eqz v3, :cond_3

    .line 226
    iget-object v3, v2, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    invoke-interface {v3}, Lcom/google/android/finsky/activities/ViewPagerTab;->onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228
    :cond_3
    # getter for: Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;
    invoke-static {v2}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->access$100(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;)Lcom/google/android/finsky/utils/ObjectMap;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private restoreDfeLists(Lcom/google/android/finsky/utils/ObjectMap;)Ljava/util/List;
    .locals 4
    .param p1, "instanceState"    # Lcom/google/android/finsky/utils/ObjectMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/utils/ObjectMap;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/DfeList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 410
    const/4 v2, 0x0

    .line 411
    .local v2, "lists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/DfeList;>;"
    if-eqz p1, :cond_1

    const-string v3, "TabbedAdapter.TabDfeLists"

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 412
    const-string v3, "TabbedAdapter.TabDfeLists"

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/utils/ObjectMap;->getList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 413
    if-eqz v2, :cond_1

    .line 414
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/DfeList;

    .line 415
    .local v1, "list":Lcom/google/android/finsky/api/model/DfeList;
    if-eqz v1, :cond_0

    .line 419
    iget-object v3, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/DfeList;->setDfeApi(Lcom/google/android/finsky/api/DfeApi;)V

    goto :goto_0

    .line 423
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "list":Lcom/google/android/finsky/api/model/DfeList;
    :cond_1
    return-object v2
.end method

.method private restoreScrollPositions(Lcom/google/android/finsky/utils/ObjectMap;)Ljava/util/List;
    .locals 2
    .param p1, "instanceState"    # Lcom/google/android/finsky/utils/ObjectMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/utils/ObjectMap;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/ObjectMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    const/4 v0, 0x0

    .line 403
    .local v0, "scrollPositions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/ObjectMap;>;"
    if-eqz p1, :cond_0

    const-string v1, "TabbedAdapter.TabInstanceStates"

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 404
    const-string v1, "TabbedAdapter.TabInstanceStates"

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/utils/ObjectMap;->getList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 406
    :cond_0
    return-object v0
.end method

.method private showQuickLinksIfNoListsExist([Lcom/google/android/finsky/protos/Browse$QuickLink;)V
    .locals 8
    .param p1, "quickLinks"    # [Lcom/google/android/finsky/protos/Browse$QuickLink;

    .prologue
    const/4 v7, 0x0

    .line 140
    iget-object v4, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    if-eqz p1, :cond_1

    array-length v4, p1

    if-lez v4, :cond_1

    .line 142
    new-instance v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/Browse$BrowseTab;-><init>()V

    .line 143
    .local v0, "browseTab":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    new-instance v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v4}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;-><init>(Lcom/google/android/finsky/protos/Browse$BrowseTab;Z)V

    .line 144
    .local v1, "fallbackTab":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    array-length v4, p1

    new-array v3, v4, [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 145
    .local v3, "quickLinkInfos":[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p1

    if-ge v2, v4, :cond_0

    .line 146
    new-instance v4, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    aget-object v5, p1, v2

    iget v6, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBackendId:I

    invoke-direct {v4, v5, v6}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;-><init>(Lcom/google/android/finsky/protos/Browse$QuickLink;I)V

    aput-object v4, v3, v2

    .line 145
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 148
    :cond_0
    iput-object v3, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 149
    new-instance v4, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/16 v5, 0x191

    iget-object v6, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mParent:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {v4, v5, v7, v7, v6}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v4, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->elementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    .line 151
    iget-object v4, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    .end local v0    # "browseTab":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .end local v1    # "fallbackTab":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    .end local v2    # "i":I
    .end local v3    # "quickLinkInfos":[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    :cond_1
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1, "viewPager"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 191
    move-object v0, p3

    check-cast v0, Lcom/google/android/finsky/activities/ViewPagerTab;

    .line 193
    .local v0, "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "viewPager":Landroid/view/ViewGroup;
    iget v2, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBackendId:I

    invoke-interface {v0, v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->getView(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 194
    iget-object v2, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 196
    .local v1, "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    iget-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v2, :cond_0

    .line 197
    iget-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->flushUnusedPages()V

    .line 198
    iget-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->clearTransientState()V

    .line 202
    :cond_0
    iget-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    invoke-interface {v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;
    invoke-static {v1, v2}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->access$102(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/utils/ObjectMap;)Lcom/google/android/finsky/utils/ObjectMap;

    .line 204
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    .line 206
    invoke-interface {v0}, Lcom/google/android/finsky/activities/ViewPagerTab;->onDestroy()V

    .line 207
    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "viewPager"    # Landroid/view/ViewGroup;

    .prologue
    .line 363
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/TabbedAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 312
    const-string v0, ""

    .line 314
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPageWidth(I)F
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 342
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->isCategoryTab:Z

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 346
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 23
    .param p1, "viewPager"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 269
    const/4 v1, 0x0

    .line 270
    .local v1, "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    .line 271
    .local v6, "tabData":Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    iget-object v0, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    move-object/from16 v22, v0

    .line 274
    .local v22, "browseTab":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->shouldDeferListTabDataDisplay()Z

    move-result v18

    .line 278
    .local v18, "shouldDeferListTabDataDisplay":Z
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/TabbedAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    const/4 v8, 0x0

    .line 281
    .local v8, "tabMode":I
    :goto_0
    iget-boolean v2, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->isCategoryTab:Z

    if-eqz v2, :cond_2

    .line 282
    new-instance v1, Lcom/google/android/finsky/activities/CategoryTab;

    .end local v1    # "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/finsky/activities/CategoryTab;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/api/model/DfeToc;I)V

    .line 294
    .restart local v1    # "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    :goto_1
    # getter for: Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->instanceState:Lcom/google/android/finsky/utils/ObjectMap;
    invoke-static {v6}, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->access$100(Lcom/google/android/finsky/activities/TabbedAdapter$TabData;)Lcom/google/android/finsky/utils/ObjectMap;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V

    .line 295
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mCurrentlySelectedTab:I

    move/from16 v0, p2

    if-ne v2, v0, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-interface {v1, v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->setTabSelected(Z)V

    .line 296
    iput-object v1, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    .line 297
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBackendId:I

    invoke-interface {v1, v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->getView(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 299
    if-eqz v18, :cond_0

    instance-of v2, v1, Lcom/google/android/finsky/activities/ListTab;

    if-eqz v2, :cond_0

    .line 302
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    move-object v2, v1

    check-cast v2, Lcom/google/android/finsky/activities/ListTab;

    # invokes: Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->addDeferredTab(Lcom/google/android/finsky/activities/ListTab;)V
    invoke-static {v3, v2}, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->access$200(Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;Lcom/google/android/finsky/activities/ListTab;)V

    .line 305
    :cond_0
    return-object v1

    .line 278
    .end local v8    # "tabMode":I
    :cond_1
    const/4 v8, 0x2

    goto :goto_0

    .line 285
    .restart local v8    # "tabMode":I
    :cond_2
    iget-object v2, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-nez v2, :cond_3

    .line 286
    new-instance v2, Lcom/google/android/finsky/api/model/DfeList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v2, v6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 288
    :cond_3
    new-instance v1, Lcom/google/android/finsky/activities/ListTab;

    .end local v1    # "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/TabbedAdapter;->mSupportsOverlappingActionBar:Z

    move/from16 v20, v0

    move-object v9, v1

    move-object v15, v6

    move/from16 v21, v8

    invoke-direct/range {v9 .. v21}, Lcom/google/android/finsky/activities/ListTab;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/DfeApi;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;ZLcom/google/android/finsky/layout/actionbar/ActionBarController;ZI)V

    .restart local v1    # "tab":Lcom/google/android/finsky/activities/ViewPagerTab;
    goto :goto_1

    .line 295
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 335
    check-cast p2, Lcom/google/android/finsky/activities/ViewPagerTab;

    .end local p2    # "object":Ljava/lang/Object;
    iget v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mBackendId:I

    invoke-interface {p2, v0}, Lcom/google/android/finsky/activities/ViewPagerTab;->getView(I)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBeforeTabSelected(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    # invokes: Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->onBeforeTabSelected(I)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->access$300(Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;I)V

    .line 428
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "scrollState"    # I

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabSelectionTracker:Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;

    # invokes: Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->onPageScrollStateChanged(I)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;->access$400(Lcom/google/android/finsky/activities/TabbedAdapter$TabSelectionTracker;I)V

    .line 432
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 319
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 320
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    iget-object v1, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mTabDataList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;

    iget-object v2, v1, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->viewPagerTab:Lcom/google/android/finsky/activities/ViewPagerTab;

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v1}, Lcom/google/android/finsky/activities/ViewPagerTab;->setTabSelected(Z)V

    .line 319
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 321
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 330
    :cond_2
    iput p1, p0, Lcom/google/android/finsky/activities/TabbedAdapter;->mCurrentlySelectedTab:I

    .line 331
    return-void
.end method

.method public onSaveInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 2
    .param p1, "instanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 211
    const-string v0, "TabbedAdapter.TabInstanceStates"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedAdapter;->getTabInstanceStates()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 213
    const-string v0, "TabbedAdapter.TabDfeLists"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/TabbedAdapter;->getDfeLists()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 354
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 360
    return-void
.end method
