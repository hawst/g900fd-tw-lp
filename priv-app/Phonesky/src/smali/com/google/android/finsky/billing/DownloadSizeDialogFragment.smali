.class public Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DownloadSizeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;)Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getListener()Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;

    move-result-object v0

    return-object v0
.end method

.method private getListener()Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;

    .line 207
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;

    goto :goto_0
.end method

.method public static makeArguments(ZZZ)Landroid/os/Bundle;
    .locals 2
    .param p0, "setWifiOnlyOption"    # Z
    .param p1, "showWifiOnlyOption"    # Z
    .param p2, "onMobileNetwork"    # Z

    .prologue
    .line 113
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 114
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v1, "setWifiOnly"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 115
    const-string v1, "showWifiOnly"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    const-string v1, "onMobileNetwork"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    return-object v0
.end method

.method public static newInstance(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
    .locals 3
    .param p0, "targetFragment"    # Landroid/support/v4/app/Fragment;
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 85
    if-eqz p0, :cond_0

    instance-of v1, p0, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;

    if-nez v1, :cond_0

    .line 86
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "targetFragment must implement DownloadSizeDialog.Listener"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;-><init>()V

    .line 90
    .local v0, "dialog":Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
    if-eqz p0, :cond_1

    .line 91
    const/4 v1, -0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 93
    :cond_1
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 94
    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getListener()Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;->onDownloadCancel()V

    .line 200
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 201
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 123
    .local v1, "arguments":Landroid/os/Bundle;
    const-string v14, "showWifiOnly"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 124
    .local v11, "showWifiOnlyOption":Z
    const-string v14, "setWifiOnly"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 125
    .local v9, "setWifiOnlyOption":Z
    const-string v14, "onMobileNetwork"

    invoke-virtual {v1, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 126
    .local v8, "onMobileNetwork":Z
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 128
    .local v5, "context":Landroid/content/Context;
    new-instance v13, Landroid/view/ContextThemeWrapper;

    const v14, 0x7f0d01b1

    invoke-direct {v13, v5, v14}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 130
    .local v13, "wrappedContext":Landroid/view/ContextThemeWrapper;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v13}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 131
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    const v14, 0x7f0c02ab

    invoke-virtual {v2, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 133
    invoke-static {v13}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v14

    const v15, 0x7f04008a

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 135
    .local v4, "contentLayout":Landroid/view/View;
    const v14, 0x7f0a01b1

    invoke-virtual {v4, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 139
    .local v7, "messageView":Landroid/widget/TextView;
    if-eqz v11, :cond_2

    .line 140
    const v6, 0x7f0c02ac

    .line 145
    .local v6, "messageTextId":I
    :goto_0
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(I)V

    .line 151
    const v14, 0x7f0a01b2

    invoke-virtual {v4, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 152
    .local v3, "checkboxView":Landroid/widget/CheckBox;
    if-eqz v11, :cond_0

    .line 153
    const/4 v14, 0x0

    invoke-virtual {v3, v14}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 154
    if-nez p1, :cond_0

    .line 155
    invoke-virtual {v3, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 159
    :cond_0
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 161
    const v14, 0x7f0c02b1

    new-instance v15, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v11, v3, v9}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$1;-><init>(Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;ZLandroid/widget/CheckBox;Z)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 171
    const v14, 0x7f0c0134

    new-instance v15, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$2;-><init>(Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 180
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileNetwork()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 181
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.settings.WIFI_SETTINGS"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 182
    .local v12, "test":Landroid/content/Intent;
    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const/high16 v15, 0x10000

    invoke-virtual {v14, v12, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v10

    .line 184
    .local v10, "settings":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_1

    .line 185
    const v14, 0x7f0c02b0

    new-instance v15, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$3;-><init>(Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;)V

    invoke-virtual {v2, v14, v15}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 194
    .end local v10    # "settings":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v12    # "test":Landroid/content/Intent;
    :cond_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    return-object v14

    .line 142
    .end local v3    # "checkboxView":Landroid/widget/CheckBox;
    .end local v6    # "messageTextId":I
    :cond_2
    if-eqz v8, :cond_3

    const v6, 0x7f0c02ad

    .restart local v6    # "messageTextId":I
    :goto_1
    goto :goto_0

    .end local v6    # "messageTextId":I
    :cond_3
    const v6, 0x7f0c02ae

    goto :goto_1
.end method
