.class public Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;
.super Lcom/google/android/finsky/activities/myapps/MyAppsTab;
.source "MyAppsInstalledTab.java"

# interfaces
.implements Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/activities/myapps/MyAppsTab",
        "<",
        "Lcom/google/android/finsky/api/model/MultiWayUpdateController;",
        ">;",
        "Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

.field private mDocIdsByAccount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mInstalledView:Landroid/view/ViewGroup;

.field private mListInitialized:Z

.field private mMyAppsList:Landroid/widget/ListView;

.field private mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p1, "authenticatedActivity"    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 54
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDocIdsByAccount:Ljava/util/Map;

    .line 56
    new-instance v0, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 67
    new-instance v8, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v0, 0x195

    invoke-direct {v8, v0, v1, v1, p6}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 70
    .local v8, "mUiElementNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v4

    move-object v1, p1

    move-object v5, p5

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDocIdsByAccount:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;Ljava/util/Map;)Ljava/util/Map;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDocIdsByAccount:Ljava/util/Map;

    return-object p1
.end method


# virtual methods
.method public bucketsChanged()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method protected getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    return-object v0
.end method

.method protected getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 178
    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getViewDoc(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getFullView()Landroid/view/View;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 3
    .param p1, "backendId"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400dc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 147
    invoke-super {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onDataChanged()V

    .line 149
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mListInitialized:Z

    if-nez v2, :cond_1

    .line 150
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    const v3, 0x7f0a0254

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    const v3, 0x7f0a0276

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    .line 153
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v1

    .line 154
    .local v1, "horizontalPadding":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v2, v1, v3, v1, v4}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 156
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 157
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 158
    iput-boolean v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mListInitialized:Z

    .line 160
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v3, "MyAppsTab.KeyListParcel"

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v4, "MyAppsTab.KeyListParcel"

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/utils/ObjectMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 165
    :cond_0
    const/4 v2, 0x0

    const v3, 0x7f0c0334

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->configureEmptyUI(ZI)V

    .line 168
    .end local v1    # "horizontalPadding":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v2, Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->getDocuments()Ljava/util/List;

    move-result-object v0

    .line 169
    .local v0, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/api/model/Document;>;"
    if-eqz v0, :cond_2

    .line 171
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->addDocs(Ljava/util/Collection;)V

    .line 173
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->syncViewToState()V

    .line 174
    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 213
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOADING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-ne p2, v0, :cond_1

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->invalidateInstallingItem(Ljava/lang/String;Landroid/widget/ListView;)V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    goto :goto_0
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    .line 224
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 0
    .param p1, "savedInstanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 200
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "MyAppsTab.KeyListParcel"

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method protected requestData()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 124
    return-void
.end method

.method protected retryFromError()V
    .locals 0

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->clearState()V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    .line 143
    return-void
.end method
