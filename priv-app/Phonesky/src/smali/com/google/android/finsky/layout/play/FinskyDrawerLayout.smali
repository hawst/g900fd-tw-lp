.class public Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;
.super Lcom/google/android/play/drawer/PlayDrawerLayout;
.source "FinskyDrawerLayout.java"

# interfaces
.implements Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private mCurrentBackendId:I

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

.field private final mRefreshHandler:Landroid/os/Handler;

.field private final mRefreshRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 89
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mAccountManager:Landroid/accounts/AccountManager;

    .line 90
    new-instance v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$1;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    .line 100
    new-instance v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$2;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 106
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshHandler:Landroid/os/Handler;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isTopLevelSideDrawerDestination()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->logMenuClickEvent(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Lcom/google/android/finsky/activities/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    return-object v0
.end method

.method private createPrimaryActions(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 311
    .local v14, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v15

    .line 315
    .local v15, "currentPageType":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    const/16 v7, 0x9

    if-ne v6, v7, :cond_5

    :cond_0
    const/16 v18, 0x1

    .line 321
    .local v18, "treatAsAppCorpus":Z
    :goto_0
    new-instance v2, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v6, 0x7f0c038d

    invoke-virtual {v14, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200aa

    const v5, 0x7f0200ab

    const v6, 0x7f090071

    const/4 v7, 0x1

    if-ne v15, v7, :cond_6

    const/4 v7, 0x1

    :goto_1
    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$4;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$4;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct/range {v2 .. v9}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 331
    .local v2, "homeAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    if-eqz v18, :cond_7

    const/4 v6, 0x3

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->makeMyCollectionAction(I)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-result-object v13

    .line 335
    .local v13, "collectionAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    if-eqz v13, :cond_1

    .line 336
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    :cond_1
    if-nez v18, :cond_2

    .line 341
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getShopAction(Landroid/content/Context;I)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-result-object v17

    .line 342
    .local v17, "shopAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    if-eqz v17, :cond_2

    .line 343
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    .end local v17    # "shopAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_2
    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v6, 0x7f0c0359

    invoke-virtual {v14, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0200c1

    const v6, 0x7f0200c2

    const v7, 0x7f090071

    const/16 v8, 0xa

    if-ne v15, v8, :cond_8

    const/4 v8, 0x1

    :goto_3
    const/4 v9, 0x0

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$5;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$5;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct/range {v3 .. v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 358
    .local v3, "wishlistAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeToc;->getSocialHomeUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 361
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v6, 0x7f0c0391

    invoke-virtual {v14, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0200be

    const v7, 0x7f0200bf

    const v8, 0x7f090071

    const/16 v9, 0xc

    if-ne v15, v9, :cond_9

    const/4 v9, 0x1

    :goto_4
    const/4 v10, 0x0

    new-instance v11, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$6;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$6;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct/range {v4 .. v11}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 374
    .local v4, "socialHomeAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    .end local v4    # "socialHomeAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v16

    .line 378
    .local v16, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    const-string v6, "cl:billing.hide_my_account"

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 379
    new-instance v5, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v6, 0x7f0c0392

    invoke-virtual {v14, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0200b0

    const v8, 0x7f0200b1

    const v9, 0x7f090071

    const/16 v10, 0xd

    if-ne v15, v10, :cond_a

    const/4 v10, 0x1

    :goto_5
    const/4 v11, 0x0

    new-instance v12, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$7;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$7;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct/range {v5 .. v12}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 392
    .local v5, "myAccountAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    .end local v5    # "myAccountAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_4
    return-void

    .line 315
    .end local v2    # "homeAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .end local v3    # "wishlistAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .end local v13    # "collectionAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .end local v16    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    .end local v18    # "treatAsAppCorpus":Z
    :cond_5
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 321
    .restart local v18    # "treatAsAppCorpus":Z
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 333
    .restart local v2    # "homeAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_7
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    goto/16 :goto_2

    .line 347
    .restart local v13    # "collectionAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 361
    .restart local v3    # "wishlistAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_9
    const/4 v9, 0x0

    goto :goto_4

    .line 379
    .restart local v16    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    :cond_a
    const/4 v10, 0x0

    goto :goto_5
.end method

.method private createSecondaryActions(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 494
    .local v0, "context":Landroid/content/Context;
    new-instance v7, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v9, 0x7f0c00fd

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;

    invoke-direct {v10, p0, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Landroid/content/Context;)V

    invoke-direct {v7, v9, v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 510
    .local v7, "redeemGiftCardAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    new-instance v8, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v9, 0x7f0c02d7

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$11;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$11;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct {v8, v9, v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 521
    .local v8, "settingsAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v9, 0x7f0c02e8

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$12;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$12;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-direct {v4, v9, v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 532
    .local v4, "helpAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    sget-object v9, Lcom/google/android/finsky/config/G;->legalNoticeMenuUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 535
    .local v6, "legalNoticeUrl":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 536
    sget-object v9, Lcom/google/android/finsky/config/G;->legalNoticeMenuTitleOverride:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 538
    .local v5, "legalNoticeTitle":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 539
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0c01e1

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 541
    :cond_0
    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    new-instance v9, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$13;

    invoke-direct {v9, p0, v6}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$13;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Ljava/lang/String;)V

    invoke-direct {v3, v5, v9}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 549
    .local v3, "extraAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553
    .end local v3    # "extraAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    .end local v5    # "legalNoticeTitle":Ljava/lang/String;
    :cond_1
    sget-object v9, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 554
    new-instance v2, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v9, 0x7f0c0278

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$14;

    invoke-direct {v10, p0, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$14;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Landroid/content/Context;)V

    invoke-direct {v2, v9, v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 562
    .local v2, "debugOptionsAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    .end local v2    # "debugOptionsAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    :cond_2
    sget-object v9, Lcom/google/android/finsky/config/G;->dcbDebugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 566
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v9, 0x7f0c0136

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$15;

    invoke-direct {v10, p0, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$15;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Landroid/content/Context;)V

    invoke-direct {v1, v9, v10}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 574
    .local v1, "dcbDebugOptionsAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 576
    .end local v1    # "dcbDebugOptionsAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;
    :cond_3
    return-void
.end method

.method private enterActionBarDrawerMode()V
    .locals 6

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isTopLevelSideDrawerDestination()Z

    move-result v3

    .line 139
    .local v3, "isAtTopLevelDestination":Z
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v5}, Lcom/google/android/finsky/activities/MainActivity;->getCurrentBackend()I

    move-result v1

    .line 143
    .local v1, "currentBackendId":I
    if-eqz v3, :cond_0

    if-nez v1, :cond_0

    const/4 v4, 0x1

    .line 145
    .local v4, "useLauncherNameForBreadcrumb":Z
    :goto_0
    if-eqz v4, :cond_1

    const v0, 0x7f0c01ae

    .line 148
    .local v0, "breadcrumbId":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v5}, Lcom/google/android/finsky/activities/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "drawerOpenModeTitle":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v5, v2}, Lcom/google/android/finsky/activities/MainActivity;->enterDrawerOpenMode(Ljava/lang/CharSequence;)V

    .line 150
    return-void

    .line 143
    .end local v0    # "breadcrumbId":I
    .end local v2    # "drawerOpenModeTitle":Ljava/lang/String;
    .end local v4    # "useLauncherNameForBreadcrumb":Z
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 145
    .restart local v4    # "useLauncherNameForBreadcrumb":Z
    :cond_1
    const v0, 0x7f0c0390

    goto :goto_1
.end method

.method private getShopAction(Landroid/content/Context;I)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "currentPageType"    # I

    .prologue
    const/4 v6, 0x0

    .line 397
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v8

    .line 398
    .local v8, "metadata":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-nez v8, :cond_0

    .line 400
    const/4 v0, 0x0

    .line 406
    :goto_0
    return-object v0

    .line 403
    :cond_0
    iget-object v0, v8, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, v8, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    .line 406
    .local v1, "shopName":Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    iget v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    invoke-static {v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getDrawerShopDrawable(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    invoke-static {v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getDrawerShopSelectedDrawable(I)I

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    invoke-static {v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryColorResId(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne p2, v5, :cond_2

    const/4 v5, 0x1

    :goto_2
    new-instance v7, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$8;

    invoke-direct {v7, p0, v8}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$8;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Lcom/google/android/finsky/protos/Toc$CorpusMetadata;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    goto :goto_0

    .line 403
    .end local v1    # "shopName":Ljava/lang/String;
    :cond_1
    const v0, 0x7f0c038f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .restart local v1    # "shopName":Ljava/lang/String;
    :cond_2
    move v5, v6

    .line 406
    goto :goto_2
.end method

.method private isTopLevelSideDrawerDestination()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 590
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v2

    .line 591
    .local v2, "currPageType":I
    if-eq v2, v3, :cond_0

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    const/16 v4, 0xa

    if-eq v2, v4, :cond_0

    const/4 v4, 0x3

    if-eq v2, v4, :cond_0

    const/16 v4, 0xc

    if-eq v2, v4, :cond_0

    const/16 v4, 0xd

    if-ne v2, v4, :cond_2

    .line 603
    .local v3, "isTopLevelSideDrawerDestination":Z
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 604
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    const/4 v4, 0x4

    if-ne v2, v4, :cond_1

    instance-of v4, v0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    if-eqz v4, :cond_1

    move-object v1, v0

    .line 609
    check-cast v1, Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    .line 610
    .local v1, "browseFragment":Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    invoke-virtual {v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(Ljava/lang/String;)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 611
    const/4 v3, 0x1

    .line 614
    .end local v1    # "browseFragment":Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    :cond_1
    return v3

    .line 591
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    .end local v3    # "isTopLevelSideDrawerDestination":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private logMenuClickEvent(I)V
    .locals 3
    .param p1, "elementType"    # I

    .prologue
    .line 585
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 586
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 587
    return-void
.end method

.method private makeMyCollectionAction(I)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .locals 9
    .param p1, "backendId"    # I

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x0

    .line 421
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 423
    .local v8, "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusMyCollectionDescription(I)Ljava/lang/String;

    move-result-object v1

    .line 425
    .local v1, "myCollectionDescription":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 426
    const/4 v0, 0x0

    .line 485
    :goto_0
    return-object v0

    .line 431
    :cond_0
    if-ne p1, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v2

    if-ne v2, v3, :cond_1

    const/4 v5, 0x1

    .line 434
    .local v5, "isActive":Z
    :goto_1
    new-instance v7, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;

    invoke-direct {v7, p0, p1, v8}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$9;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;ILandroid/content/Context;)V

    .line 479
    .local v7, "myCollectionRunnable":Ljava/lang/Runnable;
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-static {p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getDrawerMyCollectionDrawable(I)I

    move-result v2

    invoke-static {p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getDrawerMyCollectionSelectedDrawable(I)I

    move-result v3

    invoke-static {p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryColorResId(I)I

    move-result v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 485
    .local v0, "myCollectionAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    goto :goto_0

    .end local v0    # "myCollectionAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .end local v5    # "isActive":Z
    .end local v7    # "myCollectionRunnable":Ljava/lang/Runnable;
    :cond_1
    move v5, v6

    .line 431
    goto :goto_1
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/activities/MainActivity;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "activity"    # Lcom/google/android/finsky/activities/MainActivity;
    .param p2, "instanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 174
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    .line 177
    .local v7, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-static {p1}, Lcom/google/android/finsky/utils/UiUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v3

    .line 178
    .local v3, "actionBarHeight":I
    if-eqz p2, :cond_1

    const-string v0, "FinskyDrawerLayout.isAccountListExpanded"

    invoke-virtual {p2, v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 181
    .local v2, "isAccountListExpanded":Z
    :goto_0
    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    move-result-object v4

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v6, p0

    invoke-super/range {v0 .. v6}, Lcom/google/android/play/drawer/PlayDrawerLayout;->configure(Landroid/app/Activity;ZILcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V

    .line 184
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    .line 185
    invoke-virtual {p1}, Lcom/google/android/finsky/activities/MainActivity;->getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 187
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->refresh()V

    .line 190
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    new-instance v1, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;-><init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->syncDrawerIndicator()V

    .line 205
    if-eqz p2, :cond_2

    const-string v0, "FinskyDrawerLayout.isDrawerOpened"

    invoke-virtual {p2, v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 207
    .local v8, "isDrawerOpen":Z
    :goto_1
    if-eqz v8, :cond_0

    .line 209
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->enterActionBarDrawerMode()V

    .line 211
    :cond_0
    return-void

    .end local v2    # "isAccountListExpanded":Z
    .end local v8    # "isDrawerOpen":Z
    :cond_1
    move v2, v9

    .line 178
    goto :goto_0

    .restart local v2    # "isAccountListExpanded":Z
    :cond_2
    move v8, v9

    .line 205
    goto :goto_1
.end method

.method public onAccountListToggleButtonClicked(Z)V
    .locals 1
    .param p1, "isListExpanded"    # Z

    .prologue
    .line 266
    if-eqz p1, :cond_0

    const/16 v0, 0x11c

    .line 268
    .local v0, "elementType":I
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->logMenuClickEvent(I)V

    .line 269
    return-void

    .line 266
    .end local v0    # "elementType":I
    :cond_0
    const/16 v0, 0x11b

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onAttachedToWindow()V

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 114
    return-void
.end method

.method public onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
    .locals 4
    .param p1, "isLoaded"    # Z
    .param p2, "playDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    const/4 v1, 0x0

    .line 221
    if-nez p1, :cond_0

    .line 238
    :goto_0
    return v1

    .line 225
    :cond_0
    if-nez p2, :cond_1

    .line 228
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 229
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/GPlusDialogsHelper;->showGPlusSignUpDialog(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 234
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    .line 235
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 236
    .restart local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x7b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 238
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mOnAccountsUpdateListener:Landroid/accounts/OnAccountsUpdateListener;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 120
    invoke-super {p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDetachedFromWindow()V

    .line 121
    return-void
.end method

.method public onDownloadToggleClicked(Z)V
    .locals 0
    .param p1, "isDownloadOnly"    # Z

    .prologue
    .line 274
    return-void
.end method

.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDrawerClosed(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->exitDrawerOpenMode()V

    .line 128
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 0
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDrawerOpened(Landroid/view/View;)V

    .line 134
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->enterActionBarDrawerMode()V

    .line 135
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 4
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 154
    invoke-super {p0, p1, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onDrawerSlide(Landroid/view/View;F)V

    .line 156
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/MainActivity;->isActionBarInOpaqueMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/MainActivity;->getActionBarAlpha()I

    move-result v0

    .line 167
    .local v0, "currActionBarAlpha":I
    int-to-float v2, v0

    rsub-int v3, v0, 0xff

    int-to-float v3, v3

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    float-to-int v1, v2

    .line 170
    .local v1, "newAlpha":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lcom/google/android/finsky/activities/MainActivity;->setActionBarAlpha(IZ)V

    goto :goto_0
.end method

.method public onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
    .locals 1
    .param p1, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .prologue
    .line 244
    iget-boolean v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 248
    :goto_0
    return v0

    .line 247
    :cond_0
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 248
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "instanceState"    # Landroid/os/Bundle;

    .prologue
    .line 214
    const-string v0, "FinskyDrawerLayout.isAccountListExpanded"

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isAccountListExpanded()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    const-string v0, "FinskyDrawerLayout.isDrawerOpened"

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerOpen()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 216
    return-void
.end method

.method public onSecondaryAccountClicked(Ljava/lang/String;)Z
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mMainActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/MainActivity;->switchToAccount(Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x1

    return v0
.end method

.method public onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
    .locals 1
    .param p1, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 254
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public refresh()V
    .locals 6

    .prologue
    .line 277
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 279
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 280
    .local v3, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 283
    .local v5, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    if-eqz v0, :cond_0

    .line 284
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->createPrimaryActions(Ljava/util/List;)V

    .line 287
    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->createSecondaryActions(Ljava/util/List;)V

    .line 289
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 292
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isTopLevelSideDrawerDestination()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 293
    return-void
.end method

.method public updateCurrentBackendId(I)V
    .locals 2
    .param p1, "backendId"    # I

    .prologue
    .line 296
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    if-eq v0, p1, :cond_0

    .line 297
    iput p1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mCurrentBackendId:I

    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 302
    :cond_0
    return-void
.end method
