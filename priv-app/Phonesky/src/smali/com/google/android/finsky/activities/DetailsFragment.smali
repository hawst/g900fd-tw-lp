.class public Lcom/google/android/finsky/activities/DetailsFragment;
.super Lcom/google/android/finsky/activities/DetailsDataBasedFragment;
.source "DetailsFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/ReviewFeedbackDialog$Listener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/activities/TextSectionStateListener;
.implements Lcom/google/android/finsky/installer/InstallerListener;
.implements Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;
.implements Lcom/google/android/finsky/library/Libraries$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;
    }
.end annotation


# instance fields
.field private mAcquisitionOverride:Z

.field private mContinueUrl:Ljava/lang/String;

.field private mCoverInSource:Lcom/google/android/play/image/FifeImageView;

.field private mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

.field private mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

.field private mFetchSocialDetailsDocument:Z

.field private final mMaxCreatorBodyOfWorksRows:I

.field private final mMaxCreatorMoreByRows:I

.field private final mMaxRelatedItemRows:I

.field private final mMaxRelatedMusicItemRows:I

.field private mOriginalCoverBitmap:Landroid/graphics/Bitmap;

.field private mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

.field private final mRelatedMusicCardItemsPerRow:I

.field private mRevealTransitionCoverName:Ljava/lang/String;

.field private mRevealTransitionPrimaryContainerName:Ljava/lang/String;

.field private mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

.field private final mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

.field private mSecondaryDelayedLoadingHandler:Landroid/os/Handler;

.field private mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

.field private mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private final mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

.field private mSourceHeight:I

.field private mSourceLeft:I

.field private mSourceTop:I

.field private mSourceWidth:I

.field private final mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

.field private mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

.field private mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;-><init>()V

    .line 134
    new-instance v1, Lcom/google/android/finsky/activities/SeasonListViewBinder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SeasonListViewBinder;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    .line 137
    new-instance v1, Lcom/google/android/finsky/activities/SongListViewBinder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SongListViewBinder;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    .line 140
    new-instance v1, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .line 177
    iput-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    .line 250
    iget v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mRelatedMusicCardItemsPerRow:I

    .line 252
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 253
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxCreatorMoreByRows:I

    .line 254
    const v1, 0x7f0e0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxCreatorBodyOfWorksRows:I

    .line 255
    const v1, 0x7f0e000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    .line 256
    const v1, 0x7f0e000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedMusicItemRows:I

    .line 258
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSecondaryDelayedLoadingHandler:Landroid/os/Handler;

    .line 260
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/DetailsFragment;->setExitTransition(Ljava/lang/Object;)V

    .line 265
    :cond_0
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/activities/DetailsFragment;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/finsky/activities/DetailsFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/finsky/activities/DetailsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p4, "x4"    # Lcom/google/android/finsky/api/DfeApi;
    .param p5, "x5"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p6, "x6"    # Lcom/google/android/finsky/api/model/Document;
    .param p7, "x7"    # Z

    .prologue
    .line 97
    invoke-direct/range {p0 .. p7}, Lcom/google/android/finsky/activities/DetailsFragment;->updateSecondaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/DetailsFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/DetailsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceLeft:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/finsky/activities/DetailsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceLeft:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/DetailsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceWidth:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/finsky/activities/DetailsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceWidth:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/DetailsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceTop:I

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/finsky/activities/DetailsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceTop:I

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/finsky/activities/DetailsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceHeight:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/finsky/activities/DetailsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSourceHeight:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/play/image/FifeImageView;)Lcom/google/android/play/image/FifeImageView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsFragment;
    .param p1, "x1"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;

    return-object p1
.end method

.method private configureEnterSharedElementCallback()V
    .locals 1

    .prologue
    .line 1473
    new-instance v0, Lcom/google/android/finsky/activities/DetailsFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/DetailsFragment$3;-><init>(Lcom/google/android/finsky/activities/DetailsFragment;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsFragment;->setEnterSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    .line 1612
    return-void
.end method

.method private configureEnterTransition(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1335
    invoke-static {p1}, Landroid/transition/TransitionInflater;->from(Landroid/content/Context;)Landroid/transition/TransitionInflater;

    move-result-object v1

    .line 1336
    .local v1, "transitionInflater":Landroid/transition/TransitionInflater;
    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/transition/TransitionInflater;->inflateTransition(I)Landroid/transition/Transition;

    move-result-object v0

    .line 1339
    .local v0, "enterTransition":Landroid/transition/Transition;
    new-instance v2, Lcom/google/android/finsky/activities/DetailsFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/DetailsFragment$2;-><init>(Lcom/google/android/finsky/activities/DetailsFragment;)V

    invoke-virtual {v0, v2}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 1467
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 1468
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsFragment;->setSharedElementEnterTransition(Ljava/lang/Object;)V

    .line 1469
    return-void
.end method

.method private getRepresentativeBackendId()I
    .locals 1

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    return v0
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/view/View;Landroid/view/View;)Lcom/google/android/finsky/activities/DetailsFragment;
    .locals 6
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "continueUrl"    # Ljava/lang/String;
    .param p3, "dfeAccount"    # Ljava/lang/String;
    .param p4, "acquisitionOverride"    # Z
    .param p5, "sharedPrimaryContainer"    # Landroid/view/View;
    .param p6, "sharedCoverView"    # Landroid/view/View;

    .prologue
    .line 216
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    .line 218
    .local v0, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    new-instance v1, Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/DetailsFragment;-><init>()V

    .line 219
    .local v1, "fragment":Lcom/google/android/finsky/activities/DetailsFragment;
    invoke-virtual {v1, p3}, Lcom/google/android/finsky/activities/DetailsFragment;->setDfeAccount(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v1, v4, p1}, Lcom/google/android/finsky/activities/DetailsFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 221
    invoke-virtual {v1, p0}, Lcom/google/android/finsky/activities/DetailsFragment;->setInitialDocument(Lcom/google/android/finsky/api/model/Document;)V

    .line 222
    const-string v4, "finsky.DetailsFragment.continueUrl"

    invoke-virtual {v1, v4, p2}, Lcom/google/android/finsky/activities/DetailsFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v4, "finsky.DetailsFragment.acquisitionOverride"

    invoke-virtual {v1, v4, p4}, Lcom/google/android/finsky/activities/DetailsFragment;->setArgument(Ljava/lang/String;Z)V

    .line 225
    if-eqz p5, :cond_2

    if-eqz p6, :cond_2

    const/4 v2, 0x1

    .line 230
    .local v2, "hasSharedElements":Z
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 232
    invoke-virtual {p5}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionPrimaryContainerName:Ljava/lang/String;

    .line 234
    invoke-virtual {p6}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    .line 236
    check-cast p6, Landroid/widget/ImageView;

    .end local p6    # "sharedCoverView":Landroid/view/View;
    invoke-virtual {p6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 237
    .local v3, "sourceCoverDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_0

    .line 238
    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    .end local v3    # "sourceCoverDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;

    .line 241
    :cond_0
    invoke-direct {v1, v0}, Lcom/google/android/finsky/activities/DetailsFragment;->configureEnterTransition(Landroid/content/Context;)V

    .line 242
    invoke-direct {v1}, Lcom/google/android/finsky/activities/DetailsFragment;->configureEnterSharedElementCallback()V

    .line 245
    :cond_1
    return-object v1

    .line 225
    .end local v2    # "hasSharedElements":Z
    .restart local p6    # "sharedCoverView":Landroid/view/View;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private requestSocialDetailsDocument(Ljava/lang/String;)V
    .locals 5
    .param p1, "currentAccount"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 358
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 363
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mUrl:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;ZLjava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 365
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 366
    new-instance v0, Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    invoke-direct {v0, p0, v4}, Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;-><init>(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/activities/DetailsFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    .line 367
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 368
    return-void
.end method

.method private updateDetailsSections(Landroid/os/Bundle;Z)V
    .locals 20
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "delaySecondaryLoad"    # Z

    .prologue
    .line 422
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    .line 423
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v3, :cond_0

    .line 481
    :goto_0
    return-void

    .line 429
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 431
    if-eqz p1, :cond_2

    const-string v2, "finsky.DetailsFragment.wasDocOwnedWhenPageWasLoaded"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 434
    const-string v2, "finsky.DetailsFragment.wasDocOwnedWhenPageWasLoaded"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    .line 443
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v19

    .line 444
    .local v19, "fragmentView":Landroid/view/View;
    const v2, 0x7f0a00ca

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    .line 446
    .local v18, "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Lcom/google/android/finsky/activities/DetailsFragment;->inflateSectionsIfNecessary(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;)V

    .line 448
    const v2, 0x7f0a0175

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsSummary;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    .line 450
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDetailsData()Lcom/google/android/finsky/api/model/DfeDetails;

    move-result-object v13

    .line 453
    .local v13, "detailsData":Lcom/google/android/finsky/api/model/DfeDetails;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v5

    .line 455
    .local v5, "socialDataDfeApi":Lcom/google/android/finsky/api/DfeApi;
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 457
    .local v6, "socialDfeDetails":Lcom/google/android/finsky/api/model/DfeDetails;
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v7

    .line 461
    .local v7, "socialDoc":Lcom/google/android/finsky/api/model/Document;
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->hasDetailsDataLoaded()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeDetails;->isReady()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v8, 0x1

    .local v8, "hasDetailsDataLoaded":Z
    :goto_5
    move-object/from16 v2, p0

    move-object/from16 v4, p1

    .line 465
    invoke-direct/range {v2 .. v8}, Lcom/google/android/finsky/activities/DetailsFragment;->updatePrimaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V

    .line 467
    if-eqz p2, :cond_8

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSecondaryDelayedLoadingHandler:Landroid/os/Handler;

    new-instance v9, Lcom/google/android/finsky/activities/DetailsFragment$1;

    move-object/from16 v10, p0

    move-object v11, v3

    move-object/from16 v12, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, v7

    move/from16 v17, v8

    invoke-direct/range {v9 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment$1;-><init>(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V

    const-wide/16 v10, 0x190

    invoke-virtual {v2, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 439
    .end local v5    # "socialDataDfeApi":Lcom/google/android/finsky/api/DfeApi;
    .end local v6    # "socialDfeDetails":Lcom/google/android/finsky/api/model/DfeDetails;
    .end local v7    # "socialDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v8    # "hasDetailsDataLoaded":Z
    .end local v13    # "detailsData":Lcom/google/android/finsky/api/model/DfeDetails;
    .end local v18    # "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    .end local v19    # "fragmentView":Landroid/view/View;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    goto/16 :goto_1

    .line 453
    .restart local v13    # "detailsData":Lcom/google/android/finsky/api/model/DfeDetails;
    .restart local v18    # "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    .restart local v19    # "fragmentView":Landroid/view/View;
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    goto :goto_2

    .restart local v5    # "socialDataDfeApi":Lcom/google/android/finsky/api/DfeApi;
    :cond_4
    move-object v6, v13

    .line 455
    goto :goto_3

    .restart local v6    # "socialDfeDetails":Lcom/google/android/finsky/api/model/DfeDetails;
    :cond_5
    move-object v7, v3

    .line 457
    goto :goto_4

    .line 461
    .restart local v7    # "socialDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->hasDetailsDataLoaded()Z

    move-result v8

    goto :goto_5

    .restart local v8    # "hasDetailsDataLoaded":Z
    :cond_8
    move-object/from16 v10, p0

    move-object v11, v3

    move-object/from16 v12, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, v7

    move/from16 v17, v8

    .line 478
    invoke-direct/range {v10 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->updateSecondaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V

    goto/16 :goto_0
.end method

.method private updatePrimaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V
    .locals 12
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
    .param p3, "socialDataDfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "socialDfeDetails"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p5, "socialDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p6, "hasDetailsDataLoaded"    # Z

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v9

    .line 491
    .local v9, "fragmentView":Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    if-eqz v1, :cond_0

    .line 492
    const v1, 0x7f0a0186

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    .line 494
    .local v10, "thumbnailFrame":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    aput-object v10, v3, v5

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    .line 495
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 500
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setCoverFromBitmap(Landroid/graphics/Bitmap;)V

    .line 504
    .end local v10    # "thumbnailFrame":Landroid/view/ViewGroup;
    :cond_0
    invoke-virtual {p0, v9, p1, p2}, Lcom/google/android/finsky/activities/DetailsFragment;->configurePromoHeroImage(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V

    .line 507
    const v1, 0x7f0a01a3

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/layout/WarningMessageSection;

    .line 509
    .local v11, "warningMessagePanel":Lcom/google/android/finsky/layout/WarningMessageSection;
    if-eqz v11, :cond_1

    .line 510
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v3}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v11, p1, v1, v2, v3}, Lcom/google/android/finsky/layout/WarningMessageSection;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)V

    .line 514
    :cond_1
    const v1, 0x7f0a01b0

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DiscoveryBar;

    .line 515
    .local v0, "discoveryBar":Lcom/google/android/finsky/layout/DiscoveryBar;
    if-eqz v0, :cond_2

    .line 516
    if-eqz p4, :cond_3

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/finsky/api/model/DfeDetails;->getDiscoveryBadges()[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    move-result-object v4

    .line 518
    .local v4, "discoveryBadges":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    move-object/from16 v3, p5

    move/from16 v7, p6

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/layout/DiscoveryBar;->configure(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 522
    .end local v4    # "discoveryBadges":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    :cond_2
    return-void

    .line 516
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private updateSecondaryDetailsSections(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/api/model/Document;Z)V
    .locals 90
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
    .param p3, "detailsData"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p4, "socialDataDfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p5, "socialDfeDetails"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p6, "socialDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p7, "hasDetailsDataLoaded"    # Z

    .prologue
    .line 530
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v71

    .line 531
    .local v71, "fragmentView":Landroid/view/View;
    if-nez v71, :cond_1

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v66

    .line 537
    .local v66, "docType":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v82

    .line 540
    .local v82, "res":Landroid/content/res/Resources;
    const v5, 0x7f0a00d4

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v61

    check-cast v61, Lcom/google/android/finsky/layout/AvatarHeader;

    .line 542
    .local v61, "creatorPageAvatarHeader":Lcom/google/android/finsky/layout/AvatarHeader;
    if-eqz v61, :cond_2

    .line 543
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    if-eqz v5, :cond_1d

    .line 544
    const/16 v5, 0x8

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/AvatarHeader;->setVisibility(I)V

    .line 553
    :cond_2
    :goto_1
    const v5, 0x7f0a0169

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/SubscriptionsSection;

    .line 555
    .local v7, "subscriptionsSection":Lcom/google/android/finsky/layout/SubscriptionsSection;
    if-eqz v7, :cond_3

    .line 556
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    const v10, 0x7f0401b4

    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p0

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->bind(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/layout/SubscriptionsSection;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/Document;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 561
    :cond_3
    const v5, 0x7f0a032a

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/RateReviewSection;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    .line 563
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    if-eqz v5, :cond_4

    .line 564
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->initialize(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v9

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v10

    if-eqz p5, :cond_1e

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/DfeDetails;->getUserReview()Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v14

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v11, p0

    move-object/from16 v12, p6

    move/from16 v13, p7

    move-object/from16 v16, p0

    invoke-virtual/range {v8 .. v16}, Lcom/google/android/finsky/layout/RateReviewSection;->configure(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/library/Libraries;Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/protos/DocumentV2$Review;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 572
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_1f

    const/16 v75, 0x1

    .line 574
    .local v75, "isMagazine":Z
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasCreatorDoc()Z

    move-result v72

    .line 577
    .local v72, "hasCreatorDoc":Z
    const v5, 0x7f0a0166

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v62

    check-cast v62, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 579
    .local v62, "creatorRelatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    const v5, 0x7f0a016c

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;

    .line 581
    .local v8, "creatorEntrySection":Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;
    if-eqz v72, :cond_20

    if-eqz v8, :cond_20

    .line 582
    if-eqz v62, :cond_5

    .line 583
    const/16 v5, 0x8

    move-object/from16 v0, v62

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 585
    :cond_5
    const/4 v5, 0x0

    invoke-virtual {v8, v5}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->setVisibility(I)V

    .line 586
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    invoke-virtual {v8, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 587
    const v5, 0x7f0c02eb

    move-object/from16 v0, v82

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 588
    .local v13, "moreLabel":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_6

    .line 589
    const v5, 0x7f0c0408

    move-object/from16 v0, v82

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 591
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreatorDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByListUrl()Ljava/lang/String;

    move-result-object v12

    const/4 v14, 0x4

    const/16 v15, 0xe

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxCreatorMoreByRows:I

    move/from16 v17, v0

    move/from16 v18, p7

    move-object/from16 v19, p0

    invoke-virtual/range {v8 .. v19}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 610
    .end local v13    # "moreLabel":Ljava/lang/String;
    :cond_7
    :goto_4
    const v5, 0x7f0a00cb

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 612
    .local v14, "descriptionPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    if-eqz v14, :cond_9

    .line 614
    const/4 v5, 0x1

    move/from16 v0, v66

    if-ne v0, v5, :cond_26

    .line 617
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    move-object/from16 v59, v0

    .line 618
    .local v59, "appPackageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v5

    move-object/from16 v0, v59

    invoke-interface {v5, v0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v5

    if-eqz v5, :cond_23

    const/16 v74, 0x1

    .line 620
    .local v74, "isInstalled":Z
    :goto_5
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v5

    move-object/from16 v0, v59

    invoke-interface {v5, v0}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v5

    sget-object v6, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v5, v6, :cond_24

    const/16 v80, 0x1

    .line 623
    .local v80, "isTrackedByInstaller":Z
    :goto_6
    if-nez v74, :cond_8

    if-eqz v80, :cond_25

    :cond_8
    const/16 v18, 0x1

    .line 631
    .end local v59    # "appPackageName":Ljava/lang/String;
    .end local v74    # "isInstalled":Z
    .end local v80    # "isTrackedByInstaller":Z
    .local v18, "isOwned":Z
    :goto_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v16, p1

    move/from16 v17, p7

    move-object/from16 v19, p2

    move-object/from16 v20, p0

    move-object/from16 v21, p0

    invoke-virtual/range {v14 .. v21}, Lcom/google/android/finsky/layout/DetailsTextSection;->bindDescription(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 637
    .end local v18    # "isOwned":Z
    :cond_9
    const v5, 0x7f0a0165

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Lcom/google/android/finsky/layout/ReviewSamplesSection;

    .line 639
    .local v25, "reviewsPanel":Lcom/google/android/finsky/layout/ReviewSamplesSection;
    if-eqz v25, :cond_a

    .line 641
    new-instance v19, Lcom/google/android/finsky/activities/ReviewDialogListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    move-object/from16 v26, v0

    move-object/from16 v21, p0

    move-object/from16 v23, p6

    move-object/from16 v24, p5

    invoke-direct/range {v19 .. v26}, Lcom/google/android/finsky/activities/ReviewDialogListener;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/layout/ReviewSamplesSection;Lcom/google/android/finsky/layout/RateReviewSection;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/DetailsFragment;->mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v30, v0

    move-object/from16 v26, p4

    move-object/from16 v27, p6

    move/from16 v28, p7

    move-object/from16 v29, p0

    move-object/from16 v31, p0

    invoke-virtual/range {v25 .. v31}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->bind(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/Document;ZLandroid/support/v4/app/Fragment;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 649
    :cond_a
    const v5, 0x7f0a00cd

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    .line 651
    .local v26, "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    if-eqz v26, :cond_b

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUrl:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v27, p6

    move/from16 v28, p7

    move-object/from16 v30, p2

    move-object/from16 v31, p4

    move-object/from16 v32, p0

    invoke-virtual/range {v26 .. v32}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->bind(Lcom/google/android/finsky/api/model/Document;ZLjava/lang/String;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 657
    :cond_b
    const v5, 0x7f0a014a

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v60

    check-cast v60, Lcom/google/android/finsky/layout/DetailsBylinesSection;

    .line 659
    .local v60, "bylinesPanel":Lcom/google/android/finsky/layout/DetailsBylinesSection;
    if-eqz v60, :cond_c

    .line 660
    move-object/from16 v0, v60

    move-object/from16 v1, p1

    move/from16 v2, p7

    move-object/from16 v3, p0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->bind(Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 665
    :cond_c
    const v5, 0x7f0a0167

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 667
    .local v27, "relatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v27, :cond_e

    .line 668
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_d

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasCreatorRelatedContent()Z

    move-result v5

    if-nez v5, :cond_d

    if-nez v62, :cond_27

    if-nez v8, :cond_27

    :cond_d
    const/16 v65, 0x1

    .line 672
    .local v65, "displayRelated":Z
    :goto_8
    if-eqz v65, :cond_2b

    .line 673
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_28

    const/16 v77, 0x1

    .line 674
    .local v77, "isMusic":Z
    :goto_9
    if-eqz v77, :cond_29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedMusicItemRows:I

    move/from16 v34, v0

    .line 675
    .local v34, "itemsMaxRows":I
    :goto_a
    if-eqz v77, :cond_2a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRelatedMusicCardItemsPerRow:I

    move/from16 v33, v0

    .line 676
    .local v33, "itemsPerRow":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 677
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedHeader()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedListUrl()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedBrowseUrl()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v28, p1

    move/from16 v35, p7

    move-object/from16 v36, p0

    invoke-virtual/range {v27 .. v36}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 686
    .end local v33    # "itemsPerRow":I
    .end local v34    # "itemsMaxRows":I
    .end local v65    # "displayRelated":Z
    .end local v77    # "isMusic":Z
    :cond_e
    :goto_c
    const v5, 0x7f0a016d

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 688
    .local v35, "crossSellPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v35, :cond_f

    .line 689
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasCrossSell()Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 690
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v35

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 691
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCrossSellHeader()Ljava/lang/String;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCrossSellListUrl()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCrossSellBrowseUrl()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    move/from16 v42, v0

    move-object/from16 v36, p1

    move/from16 v43, p7

    move-object/from16 v44, p0

    invoke-virtual/range {v35 .. v44}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 699
    :cond_f
    :goto_d
    const v5, 0x7f0a0168

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 701
    .local v36, "postPurchasePanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v36, :cond_11

    .line 705
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v67

    .line 706
    .local v67, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    sget-object v5, Lcom/google/android/finsky/config/G;->forcePostPurchaseCrossSell:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_10

    invoke-virtual/range {v67 .. v67}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isPostPurchaseXsellEnabledForAllCorpora()Z

    move-result v5

    if-nez v5, :cond_10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2d

    :cond_10
    const/16 v78, 0x1

    .line 711
    .local v78, "isSectionEnabled":Z
    :goto_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getPostPurchaseCrossSellSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v5

    if-eqz v5, :cond_2e

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getPostPurchaseCrossSellSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v63

    .line 715
    .local v63, "crossSellSection":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    :goto_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    if-eqz v5, :cond_2f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_2f

    const/16 v88, 0x1

    .line 719
    .local v88, "wasNotInLibraryWhenPageWasLoaded":Z
    :goto_10
    if-eqz v88, :cond_30

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_30

    const/16 v89, 0x1

    .line 721
    .local v89, "wasPurchasedInPageLifetime":Z
    :goto_11
    if-eqz v78, :cond_31

    if-eqz v63, :cond_31

    if-eqz v89, :cond_31

    .line 722
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 723
    move-object/from16 v0, v63

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move-object/from16 v0, v63

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    move/from16 v43, v0

    move-object/from16 v37, p1

    move/from16 v44, p7

    move-object/from16 v45, p0

    invoke-virtual/range {v36 .. v45}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 732
    .end local v63    # "crossSellSection":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .end local v67    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    .end local v78    # "isSectionEnabled":Z
    .end local v88    # "wasNotInLibraryWhenPageWasLoaded":Z
    .end local v89    # "wasPurchasedInPageLifetime":Z
    :cond_11
    :goto_12
    const v5, 0x7f0a016e

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 734
    .local v37, "moreByPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v37, :cond_12

    .line 735
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasMoreBy()Z

    move-result v5

    if-eqz v5, :cond_32

    if-eqz v75, :cond_32

    .line 736
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 737
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByHeader()Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByListUrl()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByBrowseUrl()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    move/from16 v44, v0

    move-object/from16 v38, p1

    move/from16 v45, p7

    move-object/from16 v46, p0

    invoke-virtual/range {v37 .. v46}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 745
    :cond_12
    :goto_13
    const/4 v5, 0x6

    move/from16 v0, v66

    if-ne v0, v5, :cond_33

    const/16 v76, 0x1

    .line 748
    .local v76, "isMovie":Z
    :goto_14
    const v5, 0x7f0a016a

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v83

    check-cast v83, Lcom/google/android/finsky/layout/ScreenshotGallery;

    .line 750
    .local v83, "screenshotsPanel":Lcom/google/android/finsky/layout/ScreenshotGallery;
    if-eqz v83, :cond_13

    .line 751
    if-nez v76, :cond_34

    .line 752
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, v83

    move-object/from16 v1, p1

    move/from16 v2, p7

    invoke-virtual {v0, v1, v5, v6, v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Z)V

    .line 760
    :cond_13
    :goto_15
    const v5, 0x7f0a0150

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v68

    check-cast v68, Lcom/google/android/finsky/layout/DetailsFlagItemSection;

    .line 762
    .local v68, "flagContentPanel":Lcom/google/android/finsky/layout/DetailsFlagItemSection;
    if-eqz v68, :cond_14

    .line 763
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, v68

    move-object/from16 v1, p1

    move/from16 v2, p7

    move-object/from16 v3, p0

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 766
    :cond_14
    const v5, 0x7f0a00cc

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Lcom/google/android/finsky/layout/SongList;

    .line 767
    .local v39, "songList":Lcom/google/android/finsky/layout/SongList;
    const/16 v79, 0x0

    .line 768
    .local v79, "isShowingRelatedSongList":Z
    if-eqz v39, :cond_15

    .line 769
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_38

    .line 770
    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/SongList;->setVisibility(I)V

    .line 771
    const/4 v5, 0x3

    move/from16 v0, v66

    if-ne v0, v5, :cond_35

    const/16 v73, 0x1

    .line 772
    .local v73, "isArtist":Z
    :goto_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/activities/SongListViewBinder;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    move-object/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v40

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentHeader()Ljava/lang/String;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v43

    if-nez v73, :cond_36

    const/16 v44, 0x1

    :goto_17
    if-eqz v73, :cond_37

    const/16 v45, 0x5

    :goto_18
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v48, v0

    move/from16 v46, p7

    move-object/from16 v49, p0

    invoke-virtual/range {v38 .. v49}, Lcom/google/android/finsky/activities/SongListViewBinder;->bind(Lcom/google/android/finsky/layout/SongList;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/finsky/library/Libraries;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 787
    .end local v73    # "isArtist":Z
    :goto_19
    invoke-virtual/range {v39 .. v39}, Lcom/google/android/finsky/layout/SongList;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3a

    .line 788
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v64

    .line 789
    .local v64, "detailsUri":Landroid/net/Uri;
    const-string v5, "tid"

    move-object/from16 v0, v64

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v87

    .line 790
    .local v87, "songDocId":Ljava/lang/String;
    if-eqz v87, :cond_15

    .line 791
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f0a00c9

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v85

    check-cast v85, Landroid/widget/ScrollView;

    .line 793
    .local v85, "scrollView":Landroid/widget/ScrollView;
    move-object/from16 v0, v39

    move-object/from16 v1, v87

    move-object/from16 v2, v85

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/SongList;->setHighlightedSong(Ljava/lang/String;Landroid/widget/ScrollView;)V

    .line 801
    .end local v64    # "detailsUri":Landroid/net/Uri;
    .end local v85    # "scrollView":Landroid/widget/ScrollView;
    .end local v87    # "songDocId":Ljava/lang/String;
    :cond_15
    :goto_1a
    const v5, 0x7f0a00ce

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v40

    check-cast v40, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 803
    .local v40, "bodyOfWorkPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v40, :cond_17

    .line 804
    const/4 v5, 0x0

    move-object/from16 v0, v40

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 805
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v40

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 806
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxCreatorBodyOfWorksRows:I

    move/from16 v47, v0

    .line 807
    .local v47, "numRows":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasBodyOfWork()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 808
    const v5, 0x7f0c034d

    move-object/from16 v0, v82

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v40

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setNoDataHeader(Ljava/lang/String;)V

    .line 809
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    move/from16 v47, v0

    .line 811
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBodyOfWorkHeader()Ljava/lang/String;

    move-result-object v42

    const/16 v43, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBodyOfWorkListUrl()Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBodyOfWorkBrowseUrl()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v46, v0

    move-object/from16 v41, p1

    move/from16 v48, p7

    move-object/from16 v49, p0

    invoke-virtual/range {v40 .. v49}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 816
    .end local v47    # "numRows":I
    :cond_17
    const v5, 0x7f0a0164

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v86

    check-cast v86, Lcom/google/android/finsky/layout/GooglePlusShareSection;

    .line 818
    .local v86, "shareSection":Lcom/google/android/finsky/layout/GooglePlusShareSection;
    if-eqz v86, :cond_18

    .line 819
    move-object/from16 v0, v86

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    move/from16 v3, p7

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/GooglePlusShareSection;->bind(Lcom/google/android/finsky/api/model/Document;Landroid/support/v4/app/Fragment;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 823
    :cond_18
    const v5, 0x7f0a016b

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v48

    check-cast v48, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 825
    .local v48, "aboutAuthorPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    if-eqz v48, :cond_19

    .line 826
    if-nez v72, :cond_3b

    .line 827
    const/4 v5, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    .line 828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v49, v0

    move-object/from16 v50, p1

    move/from16 v51, p7

    move-object/from16 v52, p2

    move-object/from16 v53, p0

    move-object/from16 v54, p0

    invoke-virtual/range {v48 .. v54}, Lcom/google/android/finsky/layout/DetailsTextSection;->bindAboutAuthor(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 836
    :cond_19
    :goto_1b
    const v5, 0x7f0a016f

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/EpisodeList;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    .line 837
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    if-eqz v5, :cond_1a

    .line 838
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/layout/EpisodeList;->addSeasonSelectionListener(Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;)V

    .line 839
    const/16 v5, 0x12

    move/from16 v0, v66

    if-ne v0, v5, :cond_3c

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3c

    .line 841
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/EpisodeList;->setVisibility(I)V

    .line 842
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v64

    .line 843
    .restart local v64    # "detailsUri":Landroid/net/Uri;
    const-string v5, "cdid"

    move-object/from16 v0, v64

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 844
    .local v52, "seasonId":Ljava/lang/String;
    const-string v5, "gdid"

    move-object/from16 v0, v64

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    .line 845
    .local v53, "episodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    move-object/from16 v49, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    move-object/from16 v51, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentHeader()Ljava/lang/String;

    move-result-object v54

    const/16 v55, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v56

    move/from16 v57, p7

    move-object/from16 v58, p0

    invoke-virtual/range {v49 .. v58}, Lcom/google/android/finsky/activities/SeasonListViewBinder;->bind(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/layout/EpisodeList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 848
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/activities/SeasonListViewBinder;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 855
    .end local v52    # "seasonId":Ljava/lang/String;
    .end local v53    # "episodeId":Ljava/lang/String;
    .end local v64    # "detailsUri":Landroid/net/Uri;
    :cond_1a
    :goto_1c
    const v5, 0x7f0a0152

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v70

    check-cast v70, Landroid/widget/TextView;

    .line 856
    .local v70, "footerPanel":Landroid/widget/TextView;
    if-eqz v70, :cond_1b

    .line 857
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/api/model/DfeDetails;->getFooterHtml()Ljava/lang/String;

    move-result-object v69

    .line 858
    .local v69, "footerHtml":Ljava/lang/String;
    invoke-static/range {v69 .. v69}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3d

    if-eqz p7, :cond_3d

    .line 859
    const/4 v5, 0x0

    move-object/from16 v0, v70

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 860
    invoke-static/range {v69 .. v69}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    move-object/from16 v0, v70

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 867
    .end local v69    # "footerHtml":Ljava/lang/String;
    :cond_1b
    :goto_1d
    const v5, 0x7f0a0158

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v81

    .line 868
    .local v81, "loadingSection":Landroid/view/View;
    if-eqz v83, :cond_3e

    invoke-virtual/range {v83 .. v83}, Lcom/google/android/finsky/layout/ScreenshotGallery;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3e

    const/16 v84, 0x1

    .line 870
    .local v84, "screenshotsVisible":Z
    :goto_1e
    if-nez p7, :cond_1c

    if-eqz v84, :cond_0

    :cond_1c
    if-eqz v81, :cond_0

    .line 871
    const/16 v5, 0x8

    move-object/from16 v0, v81

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 546
    .end local v7    # "subscriptionsSection":Lcom/google/android/finsky/layout/SubscriptionsSection;
    .end local v8    # "creatorEntrySection":Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;
    .end local v14    # "descriptionPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    .end local v25    # "reviewsPanel":Lcom/google/android/finsky/layout/ReviewSamplesSection;
    .end local v26    # "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    .end local v27    # "relatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v35    # "crossSellPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v36    # "postPurchasePanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v37    # "moreByPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v39    # "songList":Lcom/google/android/finsky/layout/SongList;
    .end local v40    # "bodyOfWorkPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v48    # "aboutAuthorPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    .end local v60    # "bylinesPanel":Lcom/google/android/finsky/layout/DetailsBylinesSection;
    .end local v62    # "creatorRelatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .end local v68    # "flagContentPanel":Lcom/google/android/finsky/layout/DetailsFlagItemSection;
    .end local v70    # "footerPanel":Landroid/widget/TextView;
    .end local v72    # "hasCreatorDoc":Z
    .end local v75    # "isMagazine":Z
    .end local v76    # "isMovie":Z
    .end local v79    # "isShowingRelatedSongList":Z
    .end local v81    # "loadingSection":Landroid/view/View;
    .end local v83    # "screenshotsPanel":Lcom/google/android/finsky/layout/ScreenshotGallery;
    .end local v84    # "screenshotsVisible":Z
    .end local v86    # "shareSection":Lcom/google/android/finsky/layout/GooglePlusShareSection;
    :cond_1d
    const/4 v5, 0x0

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/AvatarHeader;->setVisibility(I)V

    .line 547
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v9, 0x4

    move-object/from16 v0, v61

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v6, v1, v9}, Lcom/google/android/finsky/layout/AvatarHeader;->bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;I)V

    goto/16 :goto_1

    .line 565
    .restart local v7    # "subscriptionsSection":Lcom/google/android/finsky/layout/SubscriptionsSection;
    :cond_1e
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 572
    :cond_1f
    const/16 v75, 0x0

    goto/16 :goto_3

    .line 594
    .restart local v8    # "creatorEntrySection":Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;
    .restart local v62    # "creatorRelatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .restart local v72    # "hasCreatorDoc":Z
    .restart local v75    # "isMagazine":Z
    :cond_20
    if-eqz v62, :cond_7

    .line 595
    if-eqz v8, :cond_21

    .line 596
    const/16 v5, 0x8

    invoke-virtual {v8, v5}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->setVisibility(I)V

    .line 598
    :cond_21
    if-nez v75, :cond_22

    .line 599
    const/4 v5, 0x0

    move-object/from16 v0, v62

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 600
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v10

    move-object/from16 v0, v62

    invoke-virtual {v0, v5, v6, v9, v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 601
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByHeader()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByListUrl()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMoreByBrowseUrl()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxCreatorMoreByRows:I

    move/from16 v21, v0

    move-object/from16 v14, v62

    move-object/from16 v15, p1

    move/from16 v22, p7

    move-object/from16 v23, p0

    invoke-virtual/range {v14 .. v23}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_4

    .line 605
    :cond_22
    const/16 v5, 0x8

    move-object/from16 v0, v62

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    goto/16 :goto_4

    .line 618
    .restart local v14    # "descriptionPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    .restart local v59    # "appPackageName":Ljava/lang/String;
    :cond_23
    const/16 v74, 0x0

    goto/16 :goto_5

    .line 620
    .restart local v74    # "isInstalled":Z
    :cond_24
    const/16 v80, 0x0

    goto/16 :goto_6

    .line 623
    .restart local v80    # "isTrackedByInstaller":Z
    :cond_25
    const/16 v18, 0x0

    goto/16 :goto_7

    .line 627
    .end local v59    # "appPackageName":Ljava/lang/String;
    .end local v74    # "isInstalled":Z
    .end local v80    # "isTrackedByInstaller":Z
    :cond_26
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v6}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v18

    .restart local v18    # "isOwned":Z
    goto/16 :goto_7

    .line 668
    .end local v18    # "isOwned":Z
    .restart local v25    # "reviewsPanel":Lcom/google/android/finsky/layout/ReviewSamplesSection;
    .restart local v26    # "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    .restart local v27    # "relatedPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .restart local v60    # "bylinesPanel":Lcom/google/android/finsky/layout/DetailsBylinesSection;
    :cond_27
    const/16 v65, 0x0

    goto/16 :goto_8

    .line 673
    .restart local v65    # "displayRelated":Z
    :cond_28
    const/16 v77, 0x0

    goto/16 :goto_9

    .line 674
    .restart local v77    # "isMusic":Z
    :cond_29
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mMaxRelatedItemRows:I

    move/from16 v34, v0

    goto/16 :goto_a

    .line 675
    .restart local v34    # "itemsMaxRows":I
    :cond_2a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mCardItemsPerRow:I

    move/from16 v33, v0

    goto/16 :goto_b

    .line 681
    .end local v34    # "itemsMaxRows":I
    .end local v77    # "isMusic":Z
    :cond_2b
    const/16 v5, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    goto/16 :goto_c

    .line 695
    .end local v65    # "displayRelated":Z
    .restart local v35    # "crossSellPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    :cond_2c
    const/16 v5, 0x8

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    goto/16 :goto_d

    .line 706
    .restart local v36    # "postPurchasePanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .restart local v67    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    :cond_2d
    const/16 v78, 0x0

    goto/16 :goto_e

    .line 711
    .restart local v78    # "isSectionEnabled":Z
    :cond_2e
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCrossSellSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v63

    goto/16 :goto_f

    .line 715
    .restart local v63    # "crossSellSection":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    :cond_2f
    const/16 v88, 0x0

    goto/16 :goto_10

    .line 719
    .restart local v88    # "wasNotInLibraryWhenPageWasLoaded":Z
    :cond_30
    const/16 v89, 0x0

    goto/16 :goto_11

    .line 727
    .restart local v89    # "wasPurchasedInPageLifetime":Z
    :cond_31
    const/16 v5, 0x8

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    goto/16 :goto_12

    .line 741
    .end local v63    # "crossSellSection":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .end local v67    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    .end local v78    # "isSectionEnabled":Z
    .end local v88    # "wasNotInLibraryWhenPageWasLoaded":Z
    .end local v89    # "wasPurchasedInPageLifetime":Z
    .restart local v37    # "moreByPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    :cond_32
    const/16 v5, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    goto/16 :goto_13

    .line 745
    :cond_33
    const/16 v76, 0x0

    goto/16 :goto_14

    .line 755
    .restart local v76    # "isMovie":Z
    .restart local v83    # "screenshotsPanel":Lcom/google/android/finsky/layout/ScreenshotGallery;
    :cond_34
    const/16 v5, 0x8

    move-object/from16 v0, v83

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/ScreenshotGallery;->setVisibility(I)V

    goto/16 :goto_15

    .line 771
    .restart local v39    # "songList":Lcom/google/android/finsky/layout/SongList;
    .restart local v68    # "flagContentPanel":Lcom/google/android/finsky/layout/DetailsFlagItemSection;
    .restart local v79    # "isShowingRelatedSongList":Z
    :cond_35
    const/16 v73, 0x0

    goto/16 :goto_16

    .line 773
    .restart local v73    # "isArtist":Z
    :cond_36
    const/16 v44, 0x0

    goto/16 :goto_17

    :cond_37
    const v45, 0x7fffffff

    goto/16 :goto_18

    .line 777
    .end local v73    # "isArtist":Z
    :cond_38
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedDocTypeListUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_39

    .line 778
    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/SongList;->setVisibility(I)V

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    move-object/from16 v38, v0

    const/16 v40, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedDocTypeHeader()Ljava/lang/String;

    move-result-object v41

    const/16 v42, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRelatedDocTypeListUrl()Ljava/lang/String;

    move-result-object v43

    const/16 v44, 0x0

    const/16 v45, 0x5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v48, v0

    move/from16 v46, p7

    move-object/from16 v49, p0

    invoke-virtual/range {v38 .. v49}, Lcom/google/android/finsky/activities/SongListViewBinder;->bind(Lcom/google/android/finsky/layout/SongList;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/finsky/library/Libraries;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 783
    const/16 v79, 0x1

    goto/16 :goto_19

    .line 785
    :cond_39
    const/16 v5, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/SongList;->setVisibility(I)V

    goto/16 :goto_19

    .line 796
    :cond_3a
    const/16 v5, 0x8

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/SongList;->setVisibility(I)V

    goto/16 :goto_1a

    .line 831
    .restart local v40    # "bodyOfWorkPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    .restart local v48    # "aboutAuthorPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    .restart local v86    # "shareSection":Lcom/google/android/finsky/layout/GooglePlusShareSection;
    :cond_3b
    const/16 v5, 0x8

    move-object/from16 v0, v48

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    goto/16 :goto_1b

    .line 850
    :cond_3c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/EpisodeList;->setVisibility(I)V

    goto/16 :goto_1c

    .line 862
    .restart local v69    # "footerHtml":Ljava/lang/String;
    .restart local v70    # "footerPanel":Landroid/widget/TextView;
    :cond_3d
    const/16 v5, 0x8

    move-object/from16 v0, v70

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1d

    .line 868
    .end local v69    # "footerHtml":Ljava/lang/String;
    .restart local v81    # "loadingSection":Landroid/view/View;
    :cond_3e
    const/16 v84, 0x0

    goto/16 :goto_1e
.end method


# virtual methods
.method protected bindPromoHeroImage(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V
    .locals 19
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 879
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUseWideLayout:Z

    if-nez v2, :cond_1

    const/4 v6, 0x1

    .line 880
    .local v6, "hideHeroIfNoImages":Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v15

    .line 881
    .local v15, "docType":I
    sparse-switch v15, :sswitch_data_0

    .line 914
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUseWideLayout:Z

    if-eqz v2, :cond_5

    .line 915
    const/4 v2, 0x5

    if-ne v15, v2, :cond_3

    .line 917
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    const/4 v3, 0x2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    .line 957
    :cond_0
    :goto_1
    return-void

    .line 879
    .end local v6    # "hideHeroIfNoImages":Z
    .end local v15    # "docType":I
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 883
    .restart local v6    # "hideHeroIfNoImages":Z
    .restart local v15    # "docType":I
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsAppPromo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    goto :goto_1

    .line 887
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsCreator(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    goto :goto_1

    .line 890
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsMovieTrailer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    goto :goto_1

    .line 894
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsArtist(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    goto :goto_1

    .line 899
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsTvShow(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    goto :goto_1

    .line 903
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindNewsstand(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    .line 904
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUseWideLayout:Z

    if-nez v2, :cond_0

    .line 905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    if-eqz v2, :cond_0

    .line 906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    const/16 v18, 0x2

    .line 909
    .local v18, "thumbnailMode":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mDetailsPanel:Lcom/google/android/finsky/layout/DetailsSummary;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->setThumbnailMode(I)V

    goto/16 :goto_1

    .line 906
    .end local v18    # "thumbnailMode":I
    :cond_2
    const/16 v18, 0x1

    goto :goto_2

    .line 920
    :cond_3
    const/4 v2, 0x2

    if-ne v15, v2, :cond_4

    .line 921
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsAlbumWithArtist(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_1

    .line 923
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v7, 0x0

    move-object/from16 v3, p1

    move-object/from16 v5, p0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsDefault(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZLcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    goto/16 :goto_1

    .line 928
    :cond_5
    const/4 v2, 0x5

    if-ne v15, v2, :cond_6

    .line 929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->setVisibility(I)V

    goto/16 :goto_1

    .line 940
    :cond_6
    const/4 v2, 0x2

    if-ne v15, v2, :cond_8

    const/16 v17, 0x1

    .line 941
    .local v17, "isAlbum":Z
    :goto_3
    if-eqz v17, :cond_9

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isDetailsAlbumStartCoverExpandedEnabled()Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v14, 0x1

    .line 944
    .local v14, "alwaysStartExpanded":Z
    :goto_4
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->lastAutomaticHeroSequenceOnDetailsTimeShown:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v16

    .line 947
    .local v16, "hasSeenAutoExpandOnCorpus":Z
    if-nez v14, :cond_a

    if-nez v16, :cond_a

    if-eqz p2, :cond_7

    const-string v2, "finsky.DetailsFragment.afterFirstLoad"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_7
    const/4 v13, 0x1

    .line 951
    .local v13, "runAutoExpandTransition":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mSingleColumnScroller:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v9, p1

    move-object/from16 v12, p0

    invoke-virtual/range {v7 .. v14}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->bindDetailsHero(Landroid/view/ViewGroup;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/HeroGraphicView;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZZ)V

    goto/16 :goto_1

    .line 940
    .end local v13    # "runAutoExpandTransition":Z
    .end local v14    # "alwaysStartExpanded":Z
    .end local v16    # "hasSeenAutoExpandOnCorpus":Z
    .end local v17    # "isAlbum":Z
    :cond_8
    const/16 v17, 0x0

    goto :goto_3

    .line 941
    .restart local v17    # "isAlbum":Z
    :cond_9
    const/4 v14, 0x0

    goto :goto_4

    .line 947
    .restart local v14    # "alwaysStartExpanded":Z
    .restart local v16    # "hasSeenAutoExpandOnCorpus":Z
    :cond_a
    const/4 v13, 0x0

    goto :goto_5

    .line 881
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_3
        0x6 -> :sswitch_2
        0x8 -> :sswitch_1
        0x10 -> :sswitch_5
        0x11 -> :sswitch_5
        0x12 -> :sswitch_4
        0x13 -> :sswitch_4
        0x14 -> :sswitch_4
        0x1e -> :sswitch_1
    .end sparse-switch
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 382
    const v0, 0x7f04005d

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 1329
    const/4 v0, 0x2

    return v0
.end method

.method protected inflateSectionsIfNecessary(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;)V
    .locals 22
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "detailsContainer"    # Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    .prologue
    .line 962
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v14

    .line 964
    .local v14, "layoutInflater":Landroid/view/LayoutInflater;
    const v8, 0x7f040069

    .line 965
    .local v8, "currSectionOrderId":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v19

    sparse-switch v19, :sswitch_data_0

    .line 982
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v19

    if-nez v19, :cond_0

    .line 984
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUseWideLayout:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 985
    const v19, 0x7f040059

    const/16 v20, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p2

    move/from16 v2, v20

    invoke-virtual {v14, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 990
    :goto_1
    const/16 v19, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v14, v8, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 993
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getCardTransitionTarget()Landroid/view/View;

    move-result-object v4

    .line 996
    .local v4, "cardTransitionTarget":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionPrimaryContainerName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 1000
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_6

    .line 1002
    new-instance v9, Lcom/google/android/finsky/transition/ReverseHeroTransition;

    invoke-direct {v9}, Lcom/google/android/finsky/transition/ReverseHeroTransition;-><init>()V

    .line 1003
    .local v9, "heroSlideFade":Landroid/transition/Transition;
    const v19, 0x7f0a00c8

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 1004
    const-wide/16 v20, 0x190

    move-wide/from16 v0, v20

    invoke-virtual {v9, v0, v1}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 1006
    new-instance v6, Landroid/transition/Fade;

    invoke-direct {v6}, Landroid/transition/Fade;-><init>()V

    .line 1007
    .local v6, "contentFade":Landroid/transition/Transition;
    new-instance v7, Lcom/google/android/finsky/transition/ReverseContentTransition;

    invoke-direct {v7}, Lcom/google/android/finsky/transition/ReverseContentTransition;-><init>()V

    .line 1009
    .local v7, "contentSlide":Landroid/transition/Transition;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v16

    .line 1010
    .local v16, "sectionCount":I
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v13

    .line 1011
    .local v13, "idsToFade":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    move/from16 v0, v16

    if-ge v10, v0, :cond_4

    .line 1012
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1013
    .local v5, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    if-ne v5, v0, :cond_2

    .line 1011
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 968
    .end local v4    # "cardTransitionTarget":Landroid/view/View;
    .end local v5    # "child":Landroid/view/View;
    .end local v6    # "contentFade":Landroid/transition/Transition;
    .end local v7    # "contentSlide":Landroid/transition/Transition;
    .end local v9    # "heroSlideFade":Landroid/transition/Transition;
    .end local v10    # "i":I
    .end local v13    # "idsToFade":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v16    # "sectionCount":I
    :sswitch_0
    const v8, 0x7f040066

    .line 969
    goto/16 :goto_0

    .line 971
    :sswitch_1
    const v8, 0x7f040067

    .line 972
    goto/16 :goto_0

    .line 976
    :sswitch_2
    const v8, 0x7f04006c

    .line 977
    goto/16 :goto_0

    .line 979
    :sswitch_3
    const v8, 0x7f040068

    goto/16 :goto_0

    .line 987
    :cond_1
    const v19, 0x7f04005f

    const/16 v20, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p2

    move/from16 v2, v20

    invoke-virtual {v14, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto/16 :goto_1

    .line 1016
    .restart local v4    # "cardTransitionTarget":Landroid/view/View;
    .restart local v5    # "child":Landroid/view/View;
    .restart local v6    # "contentFade":Landroid/transition/Transition;
    .restart local v7    # "contentSlide":Landroid/transition/Transition;
    .restart local v9    # "heroSlideFade":Landroid/transition/Transition;
    .restart local v10    # "i":I
    .restart local v13    # "idsToFade":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v16    # "sectionCount":I
    :cond_2
    instance-of v0, v5, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;

    move/from16 v19, v0

    if-eqz v19, :cond_3

    move-object v15, v5

    .line 1018
    check-cast v15, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;

    .line 1020
    .local v15, "partialFadeView":Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
    invoke-interface {v15, v13}, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;->addParticipatingChildViewIds(Ljava/util/List;)V

    goto :goto_3

    .line 1023
    .end local v15    # "partialFadeView":Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    goto :goto_3

    .line 1027
    .end local v5    # "child":Landroid/view/View;
    :cond_4
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 1028
    .local v12, "idToFade":Ljava/lang/Integer;
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    goto :goto_4

    .line 1031
    .end local v12    # "idToFade":Ljava/lang/Integer;
    :cond_5
    const-wide/16 v20, 0x64

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 1032
    const-wide/16 v20, 0x190

    move-wide/from16 v0, v20

    invoke-virtual {v7, v0, v1}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 1035
    new-instance v18, Landroid/transition/Fade;

    invoke-direct/range {v18 .. v18}, Landroid/transition/Fade;-><init>()V

    .line 1036
    .local v18, "titleBackgroundFade":Landroid/transition/Transition;
    const v19, 0x7f0a0176

    invoke-virtual/range {v18 .. v19}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 1037
    const-wide/16 v20, 0xc8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 1039
    new-instance v17, Landroid/transition/TransitionSet;

    invoke-direct/range {v17 .. v17}, Landroid/transition/TransitionSet;-><init>()V

    .line 1040
    .local v17, "set":Landroid/transition/TransitionSet;
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 1041
    invoke-virtual/range {v17 .. v18}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 1042
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 1043
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 1052
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/DetailsFragment;->setEnterTransition(Ljava/lang/Object;)V

    .line 1055
    .end local v4    # "cardTransitionTarget":Landroid/view/View;
    .end local v6    # "contentFade":Landroid/transition/Transition;
    .end local v7    # "contentSlide":Landroid/transition/Transition;
    .end local v9    # "heroSlideFade":Landroid/transition/Transition;
    .end local v10    # "i":I
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "idsToFade":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v16    # "sectionCount":I
    .end local v17    # "set":Landroid/transition/TransitionSet;
    .end local v18    # "titleBackgroundFade":Landroid/transition/Transition;
    :cond_6
    return-void

    .line 965
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0x12 -> :sswitch_2
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x1e -> :sswitch_3
    .end sparse-switch
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 1062
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1063
    const/4 v0, 0x0

    .line 1065
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->isDataReady()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 286
    new-instance v0, Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;-><init>(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/finsky/activities/DetailsFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 289
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 290
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1186
    const/16 v0, 0x2b

    if-ne p1, v0, :cond_0

    .line 1187
    packed-switch p2, :pswitch_data_0

    .line 1204
    :cond_0
    :goto_0
    return-void

    .line 1189
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

    const-string v1, "doc_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rating"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "review_title"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "review_comment"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "author"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/ReviewDialogListener;->onSaveReview(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_0

    .line 1197
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

    const-string v1, "doc_id"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/ReviewDialogListener;->onDeleteReview(Ljava/lang/String;)V

    goto :goto_0

    .line 1201
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->onCancelReview()V

    goto :goto_0

    .line 1187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAllLibrariesLoaded()V
    .locals 0

    .prologue
    .line 1298
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1087
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 1089
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 1090
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 1092
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsFragment;->configureContentView(Landroid/view/View;)V

    .line 1094
    return-object v0
.end method

.method public onDataChanged()V
    .locals 9

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-nez v0, :cond_0

    .line 1074
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getRepresentativeBackendId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/activities/BinderFactory;->getSummaryViewBinder(Lcom/google/android/finsky/api/model/DfeToc;ILandroid/accounts/Account;)Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    .line 1076
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContinueUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    move-object v4, p0

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/fragments/PageFragment;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1081
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onDataChanged()V

    .line 1082
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 1099
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 1100
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 1101
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 1102
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 1103
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSocialDetailsErrorListener:Lcom/google/android/finsky/activities/DetailsFragment$SocialDetailsErrorListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 1108
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->recordState()V

    .line 1110
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-eqz v0, :cond_1

    .line 1111
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onDestroyView()V

    .line 1113
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->onDestroyView()V

    .line 1114
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SeasonListViewBinder;->onDestroyView()V

    .line 1115
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    if-eqz v0, :cond_2

    .line 1116
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/EpisodeList;->removeSeasonSelectionListener()V

    .line 1118
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SongListViewBinder;->onDestroyView()V

    .line 1120
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onDestroyView()V

    .line 1121
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v3, 0x1

    .line 1280
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->canChangeFragmentManagerState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1282
    instance-of v0, p1, Lcom/google/android/finsky/utils/BgDataDisabledError;

    if-eqz v0, :cond_0

    .line 1283
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/BackgroundDataDialog;->show(Landroid/support/v4/app/FragmentManager;Landroid/app/Activity;)V

    .line 1291
    :goto_0
    return-void

    .line 1285
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    goto :goto_0

    .line 1289
    :cond_1
    const-string v0, "Volley error: %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onInitViewBinders()V
    .locals 8

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/SongListViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/library/Libraries;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v6

    move-object v4, p0

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/activities/SeasonListViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/fragments/PageFragment;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 274
    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 1315
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 1319
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOADING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-eq p2, v1, :cond_0

    .line 1322
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/finsky/activities/DetailsFragment;->updateDetailsSections(Landroid/os/Bundle;Z)V

    .line 1324
    :cond_0
    return-void
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 1
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    if-eqz v0, :cond_0

    .line 1306
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->refresh()V

    .line 1308
    :cond_0
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 1165
    packed-switch p1, :pswitch_data_0

    .line 1179
    :pswitch_0
    const-string v0, "Unknown request code %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1182
    :cond_0
    :goto_0
    return-void

    .line 1167
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    if-eqz v0, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1174
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-eqz v0, :cond_0

    .line 1175
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 1131
    packed-switch p1, :pswitch_data_0

    .line 1152
    :pswitch_0
    const-string v1, "Unknown request code %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1155
    :cond_0
    :goto_0
    return-void

    .line 1133
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    if-eqz v1, :cond_0

    .line 1134
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1140
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-eqz v1, :cond_0

    .line 1141
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1145
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1146
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x200a0000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1149
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "reviewId"    # Ljava/lang/String;
    .param p3, "newRating"    # Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mReviewDialogListener:Lcom/google/android/finsky/activities/ReviewDialogListener;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/finsky/activities/ReviewDialogListener;->onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V

    .line 1214
    return-void
.end method

.method public onSeasonSelected(Lcom/google/android/finsky/api/model/Document;)V
    .locals 3
    .param p1, "seasonDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 1218
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0a00cb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 1220
    .local v0, "descriptionPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    if-eqz v0, :cond_0

    .line 1221
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->updateExtras(Lcom/google/android/finsky/api/model/Document;)V

    .line 1222
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->syncCollapsedExtraIcons()V

    .line 1224
    :cond_0
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 407
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 410
    :cond_0
    return-void
.end method

.method protected rebindViews(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->rebindActionBar()V

    .line 399
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 401
    .local v0, "toDelaySecondarySectionsLoad":Z
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/activities/DetailsFragment;->updateDetailsSections(Landroid/os/Bundle;Z)V

    .line 402
    return-void

    .line 399
    .end local v0    # "toDelaySecondarySectionsLoad":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected recordState(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1228
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v6

    .line 1229
    .local v6, "view":Landroid/view/View;
    if-nez v6, :cond_1

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1233
    :cond_1
    const v7, 0x7f0a00ca

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1240
    .local v2, "detailsContainer":Landroid/view/ViewGroup;
    const-string v7, "finsky.DetailsFragment.afterFirstLoad"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1242
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    if-eqz v7, :cond_2

    .line 1243
    const-string v7, "finsky.DetailsFragment.wasDocOwnedWhenPageWasLoaded"

    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mWasOwnedWhenPageLoaded:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1246
    :cond_2
    const v7, 0x7f0a00cb

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1247
    .local v1, "descriptionPanel":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 1251
    :cond_3
    const v7, 0x7f0a0169

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/SubscriptionsSection;

    .line 1253
    .local v5, "subscriptionsSection":Lcom/google/android/finsky/layout/SubscriptionsSection;
    if-eqz v5, :cond_4

    .line 1254
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSubscriptionsViewBinder:Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    invoke-virtual {v7, p1}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->saveInstanceState(Landroid/os/Bundle;)V

    .line 1257
    :cond_4
    const v7, 0x7f0a00cd

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    .line 1259
    .local v3, "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    if-eqz v3, :cond_5

    .line 1260
    invoke-virtual {v3, p1}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->onSavedInstanceState(Landroid/os/Bundle;)V

    .line 1263
    :cond_5
    const v7, 0x7f0a00cc

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/SongList;

    .line 1264
    .local v4, "songList":Lcom/google/android/finsky/layout/SongList;
    if-eqz v4, :cond_6

    .line 1265
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    invoke-virtual {v7, p1}, Lcom/google/android/finsky/activities/SongListViewBinder;->saveInstanceState(Landroid/os/Bundle;)V

    .line 1268
    :cond_6
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mEpisodeList:Lcom/google/android/finsky/layout/EpisodeList;

    if-eqz v7, :cond_7

    .line 1269
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mSeasonListViewBinder:Lcom/google/android/finsky/activities/SeasonListViewBinder;

    invoke-virtual {v7, p1}, Lcom/google/android/finsky/activities/SeasonListViewBinder;->saveInstanceState(Landroid/os/Bundle;)V

    .line 1272
    :cond_7
    const v7, 0x7f0a016b

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1273
    .local v0, "aboutAuthorPanel":Landroid/view/View;
    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method protected requestData()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 299
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->requestData()V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v8, "finsky.DetailsFragment.continueUrl"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mContinueUrl:Ljava/lang/String;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v8, "finsky.DetailsFragment.acquisitionOverride"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mAcquisitionOverride:Z

    .line 303
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    .line 306
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    iget-boolean v5, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mAcquisitionOverride:Z

    if-eqz v5, :cond_0

    .line 307
    const-string v8, "mAcquisitionOverride true for docId=%s - I hope it came from deep link!"

    new-array v9, v6, [Ljava/lang/Object;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    :goto_0
    aput-object v5, v9, v7

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v8, 0x3

    if-ne v5, v8, :cond_4

    move v4, v6

    .line 321
    .local v4, "isApp":Z
    :goto_1
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    .line 322
    if-eqz v4, :cond_2

    .line 323
    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v5}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v2

    .line 324
    .local v2, "dfeAccountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "currentAccountName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 326
    const-string v5, "Using current account %s to fetch social details for %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v6

    invoke-static {v5, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    iput-boolean v6, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    .line 329
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/DetailsFragment;->requestSocialDetailsDocument(Ljava/lang/String;)V

    .line 332
    :cond_1
    new-instance v5, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v6, 0x1fd

    invoke-direct {v5, v6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/finsky/activities/DetailsFragment;->mFetchSocialDetailsDocument:Z

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 336
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 338
    .end local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .end local v1    # "currentAccountName":Ljava/lang/String;
    .end local v2    # "dfeAccountName":Ljava/lang/String;
    :cond_2
    return-void

    .line 307
    .end local v4    # "isApp":Z
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    :cond_4
    move v4, v7

    .line 320
    goto :goto_1
.end method
