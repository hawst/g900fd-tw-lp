.class Lcom/google/android/finsky/layout/DetailsBylinesSection$3;
.super Ljava/lang/Object;
.source "DetailsBylinesSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsBylinesSection;->getEmailLinkEntry(ILjava/lang/String;I)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

.field final synthetic val$clickIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsBylinesSection;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;->this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;->val$clickIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;->this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;->val$clickIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 322
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x73

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;->this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

    # getter for: Lcom/google/android/finsky/layout/DetailsBylinesSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-static {v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->access$000(Lcom/google/android/finsky/layout/DetailsBylinesSection;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 324
    return-void
.end method
