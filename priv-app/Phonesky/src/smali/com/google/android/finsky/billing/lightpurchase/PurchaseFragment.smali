.class public Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
.source "PurchaseFragment.java"

# interfaces
.implements Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;
.implements Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;,
        Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$Listener;
    }
.end annotation


# instance fields
.field private mAppDownloadSizeWarningParameters:Landroid/os/Bundle;

.field private mCommitChallengeResponses:Landroid/os/Bundle;

.field private mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

.field private mCompleteFlowResult:Landroid/os/Bundle;

.field private mDcbInitialized:Z

.field private mDocServerLogsCookie:[B

.field private mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

.field private mEscrowHandleParameterKey:Ljava/lang/String;

.field private mExtraPurchaseData:Landroid/os/Bundle;

.field private mHandledPurchaseStateInstance:I

.field private mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

.field private mPrepareChallengeResponses:Landroid/os/Bundle;

.field private mPreviousState:I

.field private mPreviousSubstate:I

.field private mSelectedInstrumentId:Ljava/lang/String;

.field private mShowWalletIcon:Z

.field private mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

.field private mSkipCheckout:Z

.field private mSucceeded:Z

.field private mUsePinBasedAuth:Z

.field private mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;-><init>()V

    .line 141
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPrepareChallengeResponses:Landroid/os/Bundle;

    .line 156
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    .line 1121
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->onInitialized()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mDcbInitialized:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .param p1, "x1"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .param p2, "x2"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getReauthSettingsOverNetwork(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method private completeCheckoutPurchase()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 756
    const/4 v0, 0x0

    .line 757
    .local v0, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v3

    iget-object v2, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 758
    .local v2, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget v3, v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    if-ne v3, v4, :cond_0

    .line 759
    invoke-static {v2}, Lcom/google/android/finsky/billing/BillingUtils;->getFopVersion(Lcom/google/android/finsky/protos/CommonDevice$Instrument;)I

    move-result v1

    .line 760
    .local v1, "fopVersion":I
    if-ne v1, v4, :cond_1

    .line 761
    invoke-static {}, Lcom/google/android/finsky/billing/carrierbilling/Dcb2Util;->getCompleteParameters()Ljava/util/Map;

    move-result-object v0

    .line 766
    .end local v1    # "fopVersion":I
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->commit(Landroid/os/Bundle;Ljava/util/Map;)V

    .line 767
    return-void

    .line 762
    .restart local v1    # "fopVersion":I
    :cond_1
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 763
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowResult:Landroid/os/Bundle;

    invoke-static {v3}, Lcom/google/android/finsky/billing/carrierbilling/Dcb3Util;->getCompleteParameters(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method private fail(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;)V
    .locals 3
    .param p1, "error"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .prologue
    .line 946
    const-string v0, "Purchase failed: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 947
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .line 948
    return-void
.end method

.method private getReauthSettingsOverNetwork(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 2
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showLoading()V

    .line 507
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    .line 526
    .local v0, "fetchReauthSettingsOverNetwork":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 527
    return-void
.end method

.method private handleCompleteChallenge()V
    .locals 4

    .prologue
    .line 447
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCompleteChallenge()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    move-result-object v0

    .line 448
    .local v0, "challenge":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    iget-object v2, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    if-eqz v2, :cond_0

    .line 449
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->setupAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    .line 458
    :goto_0
    return-void

    .line 450
    :cond_0
    iget-object v2, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    if-eqz v2, :cond_1

    .line 452
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    move-result-object v1

    .line 454
    .local v1, "cvcChallengeStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto :goto_0

    .line 456
    .end local v1    # "cvcChallengeStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    goto :goto_0
.end method

.method private handleCvcEscrowed()V
    .locals 3

    .prologue
    .line 589
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 590
    .local v1, "response":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCvcEscrowHandle()Ljava/lang/String;

    move-result-object v0

    .line 591
    .local v0, "escrowHandle":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mEscrowHandleParameterKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->answerChallenge(Landroid/os/Bundle;)Z

    .line 593
    return-void
.end method

.method private handleError()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 596
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    invoke-direct {v1, v6, v6}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;-><init>(II)V

    .line 597
    .local v1, "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    const v7, 0x7f0c00db

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 598
    .local v2, "errorMessage":Ljava/lang/String;
    const v7, 0x7f0c01e0

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 601
    .local v3, "errorTitle":Ljava/lang/String;
    iget v7, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousState:I

    if-ne v7, v5, :cond_1

    iget v7, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousSubstate:I

    if-ne v7, v5, :cond_1

    move v4, v5

    .line 604
    .local v4, "failPurchase":Z
    :goto_0
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getSubstate()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 628
    :goto_1
    const-string v7, "Error: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 629
    if-eqz v4, :cond_0

    .line 630
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->fail(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;)V

    .line 632
    :cond_0
    invoke-direct {p0, v3, v2, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showError(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 633
    return-void

    .end local v4    # "failPurchase":Z
    :cond_1
    move v4, v6

    .line 601
    goto :goto_0

    .line 606
    .restart local v4    # "failPurchase":Z
    :pswitch_0
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .end local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    invoke-direct {v1, v8, v6}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;-><init>(II)V

    .line 607
    .restart local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v2

    .line 608
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/ErrorStrings;->getTitle(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v3

    .line 609
    goto :goto_1

    .line 611
    :pswitch_1
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .end local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    invoke-direct {v1, v8, v6}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;-><init>(II)V

    .line 612
    .restart local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    const v7, 0x7f0c01e2

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 613
    goto :goto_1

    .line 618
    :pswitch_2
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {v7}, Landroid/os/Bundle;->clear()V

    .line 620
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCheckoutPurchaseError()Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    move-result-object v0

    .line 622
    .local v0, "checkoutPurchaseError":Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .end local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    const/4 v7, 0x3

    iget v8, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;->permissionError:I

    invoke-direct {v1, v7, v8}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;-><init>(II)V

    .line 624
    .restart local v1    # "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;->errorMessageHtml:Ljava/lang/String;

    goto :goto_1

    .line 604
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleInit()V
    .locals 0

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    .line 402
    return-void
.end method

.method private handleInstrumentManager()V
    .locals 2

    .prologue
    .line 428
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getPrepareChallenge()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    iget-object v0, v1, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 430
    .local v0, "tokens":Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 431
    return-void
.end method

.method private handleInstrumentSelected(Ljava/lang/String;)Z
    .locals 1
    .param p1, "instrumentId"    # Ljava/lang/String;

    .prologue
    .line 897
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 898
    const/4 v0, 0x0

    .line 903
    :goto_0
    return v0

    .line 901
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSelectedInstrumentId:Ljava/lang/String;

    .line 902
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    .line 903
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private handlePrepareChallenge()V
    .locals 2

    .prologue
    .line 434
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getPrepareChallenge()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    move-result-object v0

    .line 435
    .local v0, "challenge":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    iget-object v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    if-eqz v1, :cond_1

    .line 436
    iget-object v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;->useClientCart:Z

    if-eqz v1, :cond_0

    .line 437
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePreparedForCartDetails(Z)V

    .line 444
    :goto_0
    return-void

    .line 439
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleInstrumentManager()V

    goto :goto_0

    .line 442
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    goto :goto_0
.end method

.method private handlePreparedForCartDetails(Z)V
    .locals 3
    .param p1, "continueToInstrumentManager"    # Z

    .prologue
    .line 422
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->newInstance(ILcom/google/android/finsky/protos/Purchase$ClientCart;Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    move-result-object v0

    .line 424
    .local v0, "details":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 425
    return-void
.end method

.method private handlePreparedForChangeSubscription()V
    .locals 3

    .prologue
    .line 416
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getChangeSubscription()Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->newInstance(ILcom/google/android/finsky/protos/Purchase$ChangeSubscription;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;

    move-result-object v0

    .line 418
    .local v0, "step":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 419
    return-void
.end method

.method private handlePromoRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)Z
    .locals 2
    .param p1, "redeemPromoCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    const/4 v0, 0x1

    .line 912
    if-nez p1, :cond_0

    .line 913
    const/4 v0, 0x0

    .line 931
    :goto_0
    return v0

    .line 917
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->isInstallAppDeferred()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 920
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSkipCheckout:Z

    .line 922
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showAppDownloadWarningAndContinue()V

    goto :goto_0

    .line 927
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSucceeded:Z

    .line 928
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getExtraPurchaseData()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 929
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    goto :goto_0
.end method

.method private handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .param p2, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 538
    const/4 v0, 0x0

    .line 539
    .local v0, "useGmsCoreForAuth":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    .line 540
    if-eqz p1, :cond_0

    iget v1, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->status:I

    if-nez v1, :cond_0

    .line 541
    const/4 v0, 0x1

    .line 542
    iget-object v1, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->pin:Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;

    if-eqz v1, :cond_0

    const-string v1, "ACTIVE"

    iget-object v2, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->pin:Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;

    iget-object v2, v2, Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;->status:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    .line 548
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 549
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hideLoading()V

    .line 551
    :cond_1
    invoke-direct {p0, p2, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;Z)V

    .line 552
    return-void
.end method

.method private handleSuccess()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 951
    const-string v0, "Purchase succeeded"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    iput-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSucceeded:Z

    .line 954
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->shouldShowAuthChoicesDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->newInstance(Ljava/lang/String;IZ)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 959
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 967
    :goto_0
    return-void

    .line 960
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->shouldShowSessionMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 961
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->newInstance(Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 963
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 965
    :cond_1
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStep;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStep;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto :goto_0
.end method

.method private initializeDcb(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "finishRunnable"    # Ljava/lang/Runnable;

    .prologue
    const/4 v3, 0x0

    .line 280
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mDcbInitialized:Z

    if-eqz v1, :cond_0

    .line 281
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 299
    :goto_0
    return-void

    .line 284
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mDcbInitialized:Z

    .line 285
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Ljava/lang/Runnable;)V

    .line 295
    .local v0, "successRunnable":Ljava/lang/Runnable;
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 296
    const-string v1, "Initializing DCB."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->initializeStorageAndParams(Landroid/content/Context;ZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static newInstance(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "params"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .param p2, "docServerLogsCookie"    # [B
    .param p3, "appDownloadSizeWarningArgs"    # Landroid/os/Bundle;

    .prologue
    .line 172
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 173
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "MultiStepFragment.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 174
    const-string v2, "PurchaseFragment.params"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 175
    const-string v2, "PurchaseFragment.docServerLogsCookie"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 176
    const-string v2, "PurchaseFragment.appDownloadSizeWarningArgs"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 177
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;-><init>()V

    .line 178
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 179
    return-object v1
.end method

.method private onInitialized()V
    .locals 3

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 305
    const-string v0, "Not ready, ignoring."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    if-nez v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .line 310
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const-string v2, "PurchaseFragment.purchaseFragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    goto :goto_0
.end method

.method private removeCompleteFlowFragment()V
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v0, :cond_0

    .line 1075
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1078
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    .line 1080
    :cond_0
    return-void
.end method

.method private runCompleteFlowAndContinue()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 677
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSkipCheckout:Z

    if-eqz v2, :cond_0

    .line 678
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->installApp()V

    .line 679
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    .line 705
    :goto_0
    return-void

    .line 682
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v2

    iget-object v1, v2, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 683
    .local v1, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget v2, v1, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    if-ne v2, v4, :cond_4

    .line 684
    invoke-static {v1}, Lcom/google/android/finsky/billing/BillingUtils;->getFopVersion(Lcom/google/android/finsky/protos/CommonDevice$Instrument;)I

    move-result v0

    .line 685
    .local v0, "fopVersion":I
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v2, :cond_1

    .line 686
    const-string v2, "No complete flow fragment expected."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 689
    :cond_1
    if-ne v0, v4, :cond_3

    .line 690
    const-string v2, "Start complete flow for DCB2 instrument."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/billing/lightpurchase/CompleteDcb2FlowFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/CompleteDcb2FlowFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    .line 696
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v2, :cond_4

    .line 697
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showLoading()V

    .line 698
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    const-string v4, "PurchaseFragment.completeFlowFragment"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 692
    :cond_3
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 693
    const-string v2, "Start complete flow for DCB3 instrument."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 694
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v1}, Lcom/google/android/finsky/billing/lightpurchase/CompleteDcb3FlowFragment;->newInstance(Landroid/accounts/Account;Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Lcom/google/android/finsky/billing/lightpurchase/CompleteDcb3FlowFragment;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    goto :goto_1

    .line 704
    .end local v0    # "fopVersion":I
    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->completeCheckoutPurchase()V

    goto :goto_0
.end method

.method private setupAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 2
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    const/4 v1, 0x0

    .line 466
    invoke-static {}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    invoke-direct {p0, p1, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;Z)V

    .line 498
    :goto_0
    return-void

    .line 471
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private shouldShowAuthChoicesDialog()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 973
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    .line 974
    .local v0, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    const-string v2, "cl:billing.prompt_for_auth_choice_once"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 980
    :goto_0
    return v3

    .line 977
    :cond_0
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 980
    .local v1, "purchaseAuth":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    sget-object v2, Lcom/google/android/finsky/config/G;->defaultPurchaseAuthentication:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private shouldShowSessionMessage()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 988
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 989
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    .line 991
    .local v0, "pref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 995
    .end local v0    # "pref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showAppDownloadWarningAndContinue()V
    .locals 3

    .prologue
    .line 666
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAppDownloadSizeWarningParameters:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 668
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAppDownloadSizeWarningParameters:Landroid/os/Bundle;

    invoke-static {p0, v1}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->newInstance(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;

    move-result-object v0

    .line 670
    .local v0, "dialog":Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PurchaseFragment.appDownloadSizeWarningDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 674
    .end local v0    # "dialog":Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
    :goto_0
    return-void

    .line 673
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->runCompleteFlowAndContinue()V

    goto :goto_0
.end method

.method private showAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;Z)V
    .locals 4
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .param p2, "useGmsCoreForAuth"    # Z

    .prologue
    .line 561
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    invoke-static {v1, v2, p2, v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;ZZ)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    move-result-object v0

    .line 563
    .local v0, "authChallengeStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 564
    return-void
.end method

.method private showError(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "errorTitle"    # Ljava/lang/String;
    .param p2, "errorMessage"    # Ljava/lang/String;
    .param p3, "purchaseFailed"    # Z

    .prologue
    .line 341
    invoke-static {p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;

    move-result-object v0

    .line 342
    .local v0, "errorStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 343
    return-void
.end method

.method private showError(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "errorMessage"    # Ljava/lang/String;
    .param p2, "purchaseFailed"    # Z

    .prologue
    .line 337
    const v0, 0x7f0c01e0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showError(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 338
    return-void
.end method

.method private startChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 6
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    const/4 v5, 0x0

    .line 567
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 568
    .local v1, "params":Landroid/os/Bundle;
    const-string v2, "authAccount"

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    iget-object v2, p1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v2, :cond_0

    .line 571
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-object v3, p1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-static {v2, v3, v1}, Lcom/google/android/finsky/activities/AddressChallengeActivity;->getIntent(ILcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 581
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 582
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 573
    :cond_0
    iget-object v2, p1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    if-eqz v2, :cond_1

    .line 574
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;)Landroid/content/Intent;

    move-result-object v0

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 577
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v2, "Don\'t know how to handle prepare challenge for doc: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    const v2, 0x7f0c00db

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showError(Ljava/lang/String;Z)V

    goto :goto_1
.end method


# virtual methods
.method public answerChallenge(Landroid/os/Bundle;)Z
    .locals 5
    .param p1, "response"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 813
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v0

    .line 814
    .local v0, "state":I
    packed-switch v0, :pswitch_data_0

    .line 825
    const-string v3, "Cannot answer challenge in state %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 826
    :goto_0
    return v1

    .line 816
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPrepareChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 817
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    goto :goto_0

    .line 821
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 822
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->completeCheckoutPurchase()V

    goto :goto_0

    .line 814
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public confirmPurchase()V
    .locals 4

    .prologue
    .line 657
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 658
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getAnalytics(Ljava/lang/String;)Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    .line 659
    .local v0, "analytics":Lcom/google/android/finsky/analytics/Analytics;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "completePurchase?doc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 660
    .local v1, "logUrl":Ljava/lang/String;
    invoke-interface {v0, v1}, Lcom/google/android/finsky/analytics/Analytics;->logAdMobPageView(Ljava/lang/String;)V

    .line 662
    .end local v0    # "analytics":Lcom/google/android/finsky/analytics/Analytics;
    .end local v1    # "logUrl":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showAppDownloadWarningAndContinue()V

    .line 663
    return-void
.end method

.method public escrowCvcCode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cvc"    # Ljava/lang/String;
    .param p2, "escrowHandleParameterKey"    # Ljava/lang/String;

    .prologue
    .line 803
    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mEscrowHandleParameterKey:Ljava/lang/String;

    .line 804
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->escrowCvcCode(Ljava/lang/String;)V

    .line 805
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 747
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$Listener;->onFinished()V

    .line 748
    return-void
.end method

.method public getError()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    return-object v0
.end method

.method public getExtraPurchaseData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mExtraPurchaseData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 1016
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getExtraPurchaseData()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getServerLogsCookie()[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getServerLogsCookie()[B

    move-result-object v0

    .line 941
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mDocServerLogsCookie:[B

    goto :goto_0
.end method

.method public hasFailed()Z
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSucceeded()Z
    .locals 1

    .prologue
    .line 999
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSucceeded:Z

    return v0
.end method

.method public hideProgress()V
    .locals 1

    .prologue
    .line 1038
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isDismissable(I)Z
    .locals 4
    .param p1, "dismissType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 723
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    instance-of v2, v2, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    instance-of v2, v2, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 740
    :cond_1
    :goto_0
    return v0

    .line 728
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    if-eqz v2, :cond_1

    .line 729
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v2

    const/4 v3, 0x7

    if-ne v2, v3, :cond_3

    .line 732
    if-eq p1, v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 734
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v2

    if-ne v2, v0, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getSubstate()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 736
    goto :goto_0
.end method

.method public launchBillingProfile()V
    .locals 5

    .prologue
    .line 770
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->createIntent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)Landroid/content/Intent;

    move-result-object v0

    .line 772
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 773
    return-void
.end method

.method public launchRedeem()V
    .locals 5

    .prologue
    .line 776
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createBuyFlowIntent(Ljava/lang/String;ILcom/google/android/finsky/protos/Common$Docid;I)Landroid/content/Intent;

    move-result-object v0

    .line 778
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 779
    return-void
.end method

.method public nextStep()V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 398
    :goto_0
    return-void

    .line 368
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleInit()V

    goto :goto_0

    .line 371
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showLoading()V

    goto :goto_0

    .line 374
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePrepareChallenge()V

    goto :goto_0

    .line 377
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleInstrumentManager()V

    goto :goto_0

    .line 380
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePreparedForChangeSubscription()V

    goto :goto_0

    .line 383
    :pswitch_5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePreparedForCartDetails(Z)V

    goto :goto_0

    .line 386
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleCompleteChallenge()V

    goto :goto_0

    .line 389
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleCvcEscrowed()V

    goto :goto_0

    .line 392
    :pswitch_8
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleSuccess()V

    goto :goto_0

    .line 395
    :pswitch_9
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleError()V

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_3
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, -0x1

    .line 835
    const/4 v1, 0x0

    .line 837
    .local v1, "handledResult":Z
    sparse-switch p1, :sswitch_data_0

    .line 882
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 885
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->logImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 888
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 889
    return-void

    .line 839
    :sswitch_0
    if-ne p2, v5, :cond_0

    .line 840
    const-string v5, "BillingProfileActivity.selectedInstrumentId"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 842
    .local v4, "selectedInstrument":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleInstrumentSelected(Ljava/lang/String;)Z

    move-result v1

    .line 843
    if-nez v1, :cond_0

    .line 844
    const-string v5, "BillingProfileActivity.redeemPromoCodeResult"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .line 846
    .local v3, "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePromoRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)Z

    move-result v1

    goto :goto_0

    .line 851
    .end local v3    # "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    .end local v4    # "selectedInstrument":Ljava/lang/String;
    :sswitch_1
    if-ne p2, v5, :cond_2

    .line 852
    const-string v5, "challenge_response"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 854
    .local v0, "challengeResponse":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->answerChallenge(Landroid/os/Bundle;)Z

    move-result v1

    .line 855
    goto :goto_0

    .line 856
    .end local v0    # "challengeResponse":Landroid/os/Bundle;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    .line 857
    const/4 v1, 0x1

    .line 859
    goto :goto_0

    .line 861
    :sswitch_2
    if-ne p2, v5, :cond_0

    .line 862
    const-string v5, "RedeemCodeBaseActivity.redeem_code_result"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .line 864
    .restart local v3    # "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    if-eqz v3, :cond_0

    .line 865
    invoke-virtual {v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getStoredValueInstrumentId()Ljava/lang/String;

    move-result-object v2

    .line 866
    .local v2, "instrumentId":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleInstrumentSelected(Ljava/lang/String;)Z

    move-result v1

    .line 867
    if-nez v1, :cond_0

    .line 868
    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handlePromoRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)Z

    move-result v1

    goto :goto_0

    .line 875
    .end local v2    # "instrumentId":Ljava/lang/String;
    .end local v3    # "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v5, :cond_3

    .line 876
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {v5, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 878
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 837
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x65 -> :sswitch_3
    .end sparse-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onAttach(Landroid/app/Activity;)V

    .line 230
    instance-of v0, p1, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement PlayStoreUiElementNode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 184
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 186
    if-eqz p1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "PurchaseFragment.purchaseFragment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    .line 189
    const-string v2, "PurchaseFragment.handledStateInstance"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    .line 190
    const-string v2, "PurchaseFragment.previousState"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousState:I

    .line 191
    const-string v2, "PurchaseFragment.previousSubstate"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousSubstate:I

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 194
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "PurchaseFragment.params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    .line 195
    const-string v2, "PurchaseFragment.docServerLogsCookie"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mDocServerLogsCookie:[B

    .line 196
    const-string v2, "PurchaseFragment.appDownloadSizeWarningArgs"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAppDownloadSizeWarningParameters:Landroid/os/Bundle;

    .line 197
    if-eqz p1, :cond_1

    .line 198
    const-string v2, "PurchaseFragment.selectedInstrumentId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSelectedInstrumentId:Ljava/lang/String;

    .line 199
    const-string v2, "PurchaseFragment.voucherParams"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    .line 200
    const-string v2, "PurchaseFragment.prepareChallengeResponses"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPrepareChallengeResponses:Landroid/os/Bundle;

    .line 202
    const-string v2, "PurchaseFragment.commitChallengeResponses"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    .line 204
    const-string v2, "PurchaseFragment.error"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    .line 205
    const-string v2, "PurchaseFragment.succeeded"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSucceeded:Z

    .line 206
    const-string v2, "PurchaseFragment.skipCheckout"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSkipCheckout:Z

    .line 207
    const-string v2, "PurchaseFragment.extraPurchaseData"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mExtraPurchaseData:Landroid/os/Bundle;

    .line 208
    const-string v2, "PurchaseFragment.escrowHandleParameterKey"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mEscrowHandleParameterKey:Ljava/lang/String;

    .line 210
    const-string v2, "PurchaseFragment.usePinBasedAuth"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    .line 225
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 214
    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->voucherId:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v4}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;-><init>(Ljava/lang/String;ZZ)V

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    goto :goto_0

    .line 219
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/LibraryUtils;->hasVouchers(Lcom/google/android/finsky/library/AccountLibrary;)Z

    move-result v1

    .line 221
    .local v1, "hasVouchers":Z
    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;-><init>(Ljava/lang/String;ZZ)V

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 321
    const v0, 0x7f0400bc

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDownloadCancel()V
    .locals 4

    .prologue
    .line 1103
    const-string v0, "Download size warning dismissed for app = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v3, v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1105
    return-void
.end method

.method public onDownloadOk(Z)V
    .locals 5
    .param p1, "wifiOnly"    # Z

    .prologue
    .line 1087
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v0, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 1088
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "Will download %s using wifi only = %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1089
    if-nez p1, :cond_0

    .line 1090
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 1092
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->runCompleteFlowAndContinue()V

    .line 1093
    return-void
.end method

.method public onFlowCanceled(Lcom/google/android/finsky/billing/BillingFlowFragment;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;

    .prologue
    .line 1054
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055
    const-string v0, "Not resumed, ignoring onFlowCanceled."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1060
    :goto_0
    return-void

    .line 1058
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->removeCompleteFlowFragment()V

    .line 1059
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hideLoading()V

    goto :goto_0
.end method

.method public onFlowError(Lcom/google/android/finsky/billing/BillingFlowFragment;Ljava/lang/String;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 1064
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1065
    const-string v0, "Not resumed, ignoring onFlowError."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1071
    :goto_0
    return-void

    .line 1068
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showError(Ljava/lang/String;Z)V

    .line 1069
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->removeCompleteFlowFragment()V

    .line 1070
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hideLoading()V

    goto :goto_0
.end method

.method public onFlowFinished(Lcom/google/android/finsky/billing/BillingFlowFragment;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "result"    # Landroid/os/Bundle;

    .prologue
    .line 1043
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    const-string v0, "Not resumed, ignoring onFlowFinished."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1050
    :goto_0
    return-void

    .line 1047
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->removeCompleteFlowFragment()V

    .line 1048
    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowResult:Landroid/os/Bundle;

    .line 1049
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->completeCheckoutPurchase()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 237
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 238
    const-string v0, "PurchaseFragment.handledStateInstance"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 239
    const-string v0, "PurchaseFragment.previousState"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 240
    const-string v0, "PurchaseFragment.previousSubstate"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousSubstate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 241
    const-string v0, "PurchaseFragment.prepareChallengeResponses"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPrepareChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 242
    const-string v0, "PurchaseFragment.commitChallengeResponses"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCommitChallengeResponses:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 243
    const-string v0, "PurchaseFragment.selectedInstrumentId"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSelectedInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, "PurchaseFragment.voucherParams"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 245
    const-string v0, "PurchaseFragment.succeeded"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSucceeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 246
    const-string v0, "PurchaseFragment.error"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mError:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 247
    const-string v0, "PurchaseFragment.skipCheckout"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSkipCheckout:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 248
    const-string v0, "PurchaseFragment.extraPurchaseData"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mExtraPurchaseData:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 249
    const-string v0, "PurchaseFragment.escrowHandleParameterKey"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mEscrowHandleParameterKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "PurchaseFragment.usePinBasedAuth"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mUsePinBasedAuth:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    return-void
.end method

.method public onSetupWifi()V
    .locals 2

    .prologue
    .line 1097
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1098
    .local v0, "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->startActivity(Landroid/content/Intent;)V

    .line 1099
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 255
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onStart()V

    .line 256
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "PurchaseFragment.completeFlowFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingFlowFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCompleteFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    .line 258
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->initializeDcb(Ljava/lang/Runnable;)V

    .line 264
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 6
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 347
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getStateInstance()I

    move-result v0

    .line 348
    .local v0, "stateInstance":I
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 349
    const-string v1, "Received state change: state=%d, stateInstance=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    if-ne v0, v1, :cond_2

    .line 354
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 355
    const-string v1, "Already handled state %d"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 363
    :cond_1
    :goto_0
    return-void

    .line 359
    :cond_2
    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mHandledPurchaseStateInstance:I

    .line 360
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->nextStep()V

    .line 361
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getState()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousState:I

    .line 362
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getSubstate()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPreviousSubstate:I

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 276
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onStop()V

    .line 277
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 326
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 327
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    .line 328
    .local v0, "experiments":Lcom/google/android/finsky/experiments/Experiments;
    const-string v1, "cl:billing.purchase_button_show_wallet_3d_icon"

    invoke-interface {v0, v1}, Lcom/google/android/finsky/experiments/Experiments;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mShowWalletIcon:Z

    .line 330
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mContinueButton:Landroid/view/View;

    check-cast v1, Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/IconButtonGroup;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mContinueButton:Landroid/view/View;

    check-cast v1, Lcom/google/android/finsky/layout/IconButtonGroup;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/IconButtonGroup;->setBackendForLabel(I)V

    .line 334
    return-void
.end method

.method public preparePurchase()V
    .locals 7

    .prologue
    .line 405
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v6

    .line 406
    .local v6, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastGaiaAuthTimestamp:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;-><init>(JI)V

    .line 409
    .local v5, "gaiaAuthParams":Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;
    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseOptimisticProvisioning:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v1, v0, v6}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->addPrepareOrBillingProfileParams(ZZLjava/util/Map;)V

    .line 411
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSelectedInstrumentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mPrepareChallengeResponses:Landroid/os/Bundle;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->prepare(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;Ljava/lang/String;Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;Ljava/util/Map;)V

    .line 413
    return-void
.end method

.method public setHostTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 1028
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showProgress(I)V
    .locals 1
    .param p1, "messageId"    # I

    .prologue
    .line 1033
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public switchFromChangeSubscriptionToCart()V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->switchFromChangeSubscriptionToCart()V

    .line 640
    return-void
.end method

.method public switchToInstrumentManager()V
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->switchToInstrumentManager()V

    .line 647
    return-void
.end method

.method public switchVoucher(Ljava/lang/String;)V
    .locals 4
    .param p1, "voucherId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 782
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 783
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    invoke-direct {v0, p1, v3, v3}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;-><init>(Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    .line 792
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    .line 793
    return-void

    .line 787
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;-><init>(Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mVoucherParams:Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;

    goto :goto_0
.end method
