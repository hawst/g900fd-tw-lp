.class public Lcom/google/android/finsky/layout/DetailsSummaryDynamic;
.super Landroid/view/ViewGroup;
.source "DetailsSummaryDynamic.java"


# instance fields
.field private mButtonContainer:Landroid/view/View;

.field private mDownloadProgressPanel:Landroid/view/View;

.field private mSummaryStatus:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 30
    const v0, 0x7f0a0180

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mButtonContainer:Landroid/view/View;

    .line 31
    const v0, 0x7f0a0181

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    .line 32
    const v0, 0x7f0a0182

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    .line 33
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v5, 0x0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->getChildCount()I

    move-result v1

    .line 85
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 86
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 87
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    .line 85
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    .line 93
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 42
    const/4 v0, 0x0

    .line 43
    .local v0, "height":I
    const/4 v1, 0x0

    .line 45
    .local v1, "width":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 50
    .local v2, "widthMode":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mButtonContainer:Landroid/view/View;

    invoke-virtual {v4, p1, p2}, Landroid/view/View;->measure(II)V

    .line 51
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mButtonContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 52
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mButtonContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 54
    const/4 v4, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 60
    .local v3, "zeroWidthSpec":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    if-ne v2, v5, :cond_0

    .line 62
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    invoke-virtual {v4, p1, p2}, Landroid/view/View;->measure(II)V

    .line 66
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 67
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 70
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    if-ne v2, v5, :cond_1

    .line 72
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    invoke-virtual {v4, p1, p2}, Landroid/view/View;->measure(II)V

    .line 76
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 77
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 79
    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->setMeasuredDimension(II)V

    .line 80
    return-void

    .line 64
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mDownloadProgressPanel:Landroid/view/View;

    invoke-virtual {v4, v3, p2}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 74
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->mSummaryStatus:Landroid/view/View;

    invoke-virtual {v4, v3, p2}, Landroid/view/View;->measure(II)V

    goto :goto_1
.end method
