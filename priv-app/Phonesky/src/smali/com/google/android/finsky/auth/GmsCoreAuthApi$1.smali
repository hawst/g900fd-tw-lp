.class Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;
.super Landroid/os/AsyncTask;
.source "GmsCoreAuthApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPassword(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$gaiaPassword:Ljava/lang/String;

.field final synthetic val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    iput-object p2, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$accountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$gaiaPassword:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 61
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;-><init>(Landroid/content/Context;)V

    .line 63
    .local v0, "authClient":Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;
    new-instance v3, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    iget-object v4, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$accountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$gaiaPassword:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->setPassword(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v2

    .line 66
    .local v2, "creds":Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;
    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;->setAccountCredentials(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;->confirmCredentials(Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v3

    return-object v3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    iget-object v1, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;

    # invokes: Lcom/google/android/finsky/auth/GmsCoreAuthApi;->handleVerifyPasswordResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->access$000(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V

    .line 73
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;->onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V

    return-void
.end method
