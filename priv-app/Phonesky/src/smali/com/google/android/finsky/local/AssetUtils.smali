.class public Lcom/google/android/finsky/local/AssetUtils;
.super Ljava/lang/Object;
.source "AssetUtils.java"


# direct methods
.method public static extractObb(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;Ljava/lang/String;Z)Lcom/google/android/finsky/download/obb/Obb;
    .locals 11
    .param p0, "deliveryData"    # Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "extractPatch"    # Z

    .prologue
    .line 34
    if-eqz p2, :cond_0

    const/4 v10, 0x1

    .line 36
    .local v10, "seekFileType":I
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v0, v0

    if-ge v8, v0, :cond_2

    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    aget-object v7, v0, v8

    .line 38
    .local v7, "fileData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    iget v0, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    if-ne v0, v10, :cond_1

    .line 39
    iget v2, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    iget-object v3, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    iget-wide v4, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    const/4 v6, 0x4

    move v0, p2

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/download/obb/ObbFactory;->create(ZLjava/lang/String;ILjava/lang/String;JI)Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v9

    .line 44
    .end local v7    # "fileData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    :goto_2
    return-object v9

    .line 34
    .end local v8    # "i":I
    .end local v10    # "seekFileType":I
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 36
    .restart local v7    # "fileData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .restart local v8    # "i":I
    .restart local v10    # "seekFileType":I
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 44
    .end local v7    # "fileData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public static makeAssetId(Lcom/google/android/finsky/protos/DocDetails$AppDetails;)Ljava/lang/String;
    .locals 2
    .param p0, "appDetails"    # Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-static {v0, v1}, Lcom/google/android/finsky/local/AssetUtils;->makeAssetId(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static makeAssetId(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "versionCode"    # I

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "v2:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":1:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static totalDeliverySize(Lcom/google/android/finsky/protos/DocDetails$AppDetails;)J
    .locals 14
    .param p0, "appDetails"    # Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .prologue
    .line 52
    const-wide/16 v12, 0x0

    .line 53
    .local v12, "result":J
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v1, v1

    if-ge v9, v1, :cond_2

    .line 54
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    aget-object v7, v1, v9

    .line 55
    .local v7, "file":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    iget v8, v7, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    .line 56
    .local v8, "fileType":I
    packed-switch v8, :pswitch_data_0

    .line 70
    const-string v1, "Bad file type %d in %s entry# %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 58
    :pswitch_0
    iget-wide v2, v7, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    add-long/2addr v12, v2

    .line 59
    goto :goto_1

    .line 62
    :pswitch_1
    const/4 v1, 0x2

    if-ne v8, v1, :cond_1

    const/4 v0, 0x1

    .line 63
    .local v0, "extractPatch":Z
    :goto_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    iget v2, v7, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    const/4 v3, 0x0

    iget-wide v4, v7, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    const/4 v6, 0x4

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/download/obb/ObbFactory;->create(ZLjava/lang/String;ILjava/lang/String;JI)Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v10

    .line 65
    .local v10, "obb":Lcom/google/android/finsky/download/obb/Obb;
    invoke-interface {v10}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 66
    invoke-interface {v10}, Lcom/google/android/finsky/download/obb/Obb;->getSize()J

    move-result-wide v2

    add-long/2addr v12, v2

    goto :goto_1

    .line 62
    .end local v0    # "extractPatch":Z
    .end local v10    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 74
    .end local v7    # "file":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .end local v8    # "fileType":I
    :cond_2
    return-wide v12

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
