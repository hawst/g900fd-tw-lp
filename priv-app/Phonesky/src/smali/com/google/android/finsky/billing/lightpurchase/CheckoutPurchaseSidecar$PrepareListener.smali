.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;
.super Ljava/lang/Object;
.source "CheckoutPurchaseSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrepareListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;

    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;)V
    .locals 12
    .param p1, "response"    # Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    .prologue
    const/4 v11, 0x3

    const/4 v0, 0x5

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 517
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    invoke-static {v1, v10}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$602(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 518
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;
    invoke-static {v1, v10}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$702(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 519
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    invoke-static {v1, v10}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$802(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;)Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 520
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mServerLogsCookie:[B
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$902(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;[B)[B

    .line 521
    iget-object v8, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 522
    .local v8, "status":Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/16 v2, 0x12f

    iget v3, v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getPrepareServerLatencyMs()J
    invoke-static {v4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->getTimeElapsedSincePrepareMs()J
    invoke-static {v6}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1100(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)J

    move-result-wide v6

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->log(IIJJ)V
    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1200(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;IIJJ)V

    .line 524
    iget v1, v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    packed-switch v1, :pswitch_data_0

    .line 547
    :pswitch_0
    const-string v1, "Unknown status returned from server: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->statusCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    invoke-direct {v2, v9, v10}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1402(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .line 550
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v1, v11, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1700(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    .line 553
    :goto_0
    return-void

    .line 526
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mPrepareChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$602(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 529
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$702(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 530
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/4 v2, 0x6

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v1, v2, v9}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1300(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_0

    .line 533
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    iget v3, v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->permissionError:I

    iget-object v4, v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;->errorMessageHtml:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;-><init>(ILjava/lang/String;)V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCheckoutPurchaseError:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1402(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;)Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$CheckoutPurchaseError;

    .line 535
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v1, v11, v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1500(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_0

    .line 538
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$702(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 541
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$802(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;)Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 542
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    move-result-object v1

    if-nez v1, :cond_0

    .line 544
    .local v0, "state":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v1, v0, v9}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$1600(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    goto :goto_0

    .line 542
    .end local v0    # "state":I
    :cond_0
    const/4 v0, 0x4

    goto :goto_1

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 514
    check-cast p1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$PrepareListener;->onResponse(Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;)V

    return-void
.end method
