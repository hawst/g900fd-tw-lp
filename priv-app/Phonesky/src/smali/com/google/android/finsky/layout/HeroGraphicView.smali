.class public Lcom/google/android/finsky/layout/HeroGraphicView;
.super Lcom/google/android/finsky/layout/YoutubeFrameView;
.source "HeroGraphicView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# instance fields
.field private mBackendId:I

.field private mCallerOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

.field protected mCorpusFillMode:I

.field protected mDefaultAspectRatio:F

.field private final mDefaultHeight:I

.field private mIsFrozenInCorpusFill:Z

.field private final mIsFullScreenMode:Z

.field private mIsImageLoaded:Z

.field private mMaxHeight:I

.field private mNeedsToShowPlayIconAfterUnfreezing:Z

.field private mPlayIconVerticalSpan:I

.field private mSupportsLeadingOverlayTransparency:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 107
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/YoutubeFrameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 109
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b010f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultHeight:I

    .line 110
    const v2, 0x7fffffff

    iput v2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mMaxHeight:I

    .line 112
    sget-object v2, Lcom/android/vending/R$styleable;->HeroGraphicView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 114
    .local v1, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    .line 116
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 118
    iput v3, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/HeroGraphicView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/HeroGraphicView;

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mBackendId:I

    return v0
.end method

.method private getDetailsHeroAspectRatio(I)F
    .locals 1
    .param p1, "docType"    # I

    .prologue
    const/high16 v0, 0x3f100000    # 0.5625f

    .line 388
    sparse-switch p1, :sswitch_data_0

    .line 408
    invoke-static {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    .line 402
    :sswitch_1
    const/high16 v0, 0x3efa0000    # 0.48828125f

    goto :goto_0

    .line 404
    :sswitch_2
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0

    .line 388
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public bindDetailsAlbumWithArtist(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x0

    .line 482
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v1, v1

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 484
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    .line 485
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreatorDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 486
    .local v0, "artistDoc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    .line 487
    invoke-virtual {p0, v0, p2, p3, v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsArtist(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    .line 492
    .end local v0    # "artistDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    goto :goto_0
.end method

.method public bindDetailsAppPromo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 435
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-virtual {p0, v0, v6, v8}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 436
    const/4 v3, 0x2

    const/16 v4, 0xd

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 438
    const v0, 0x7f0c025a

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 439
    return-void
.end method

.method public bindDetailsArtist(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v4, 0x0

    .line 453
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v4, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 454
    const/4 v3, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 456
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 458
    return-void
.end method

.method public bindDetailsCreator(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 446
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-virtual {p0, v0, v6, v8}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 447
    const/16 v3, 0xe

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 448
    const v0, 0x7f0c025b

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 449
    return-void
.end method

.method public bindDetailsDefault(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZLcom/google/android/play/image/FifeImageView$OnLoadedListener;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z
    .param p5, "onLoadedListener"    # Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    .prologue
    const/4 v4, 0x0

    .line 501
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 503
    iput-object p5, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCallerOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    .line 504
    const/4 v3, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move v6, v4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 506
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 508
    return-void
.end method

.method public bindDetailsMovieTrailer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 417
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-virtual {p0, v0, v3, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 418
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    const/high16 v1, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->setFocusPoint(FF)V

    .line 422
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setDefaultZoom(F)V

    .line 425
    :cond_0
    const/16 v4, 0xd

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 427
    const v0, 0x7f0c0259

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 428
    return-void
.end method

.method public bindDetailsTvShow(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v6, 0x0

    .line 462
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v6, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 463
    const/4 v3, 0x2

    const/4 v4, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 465
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 467
    return-void
.end method

.method protected bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 16
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "preferredHeroType"    # I
    .param p4, "fallbackHeroType"    # I
    .param p5, "hideIfNoImages"    # Z
    .param p6, "preferVideosAppForPlayback"    # Z
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 310
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mBackendId:I

    .line 312
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v14

    .line 313
    .local v14, "preferredHeroImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v9

    .line 315
    .local v9, "fallbackHeroImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v14, :cond_0

    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v11, 0x1

    .line 316
    .local v11, "hasPreferredHero":Z
    :goto_0
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v10, 0x1

    .line 318
    .local v10, "hasFallbackHero":Z
    :goto_1
    if-nez v11, :cond_3

    if-nez v10, :cond_3

    .line 320
    if-eqz p5, :cond_2

    .line 321
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->setVisibility(I)V

    .line 349
    :goto_2
    return-void

    .line 315
    .end local v10    # "hasFallbackHero":Z
    .end local v11    # "hasPreferredHero":Z
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 316
    .restart local v11    # "hasPreferredHero":Z
    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    .line 324
    .restart local v10    # "hasFallbackHero":Z
    :cond_2
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mBackendId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    goto :goto_2

    .line 329
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v2, :cond_4

    .line 331
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mBackendId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    .line 333
    :cond_4
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v15

    .line 334
    .local v15, "videoPreviews":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v15, :cond_7

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v12, 0x1

    .line 336
    .local v12, "hasVideoPreviews":Z
    :goto_3
    if-eqz v11, :cond_8

    const/4 v2, 0x0

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Common$Image;

    move-object v13, v2

    .line 337
    .local v13, "hero":Lcom/google/android/finsky/protos/Common$Image;
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/play/image/FifeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 338
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v2, :cond_5

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/play/image/FifeImageView;->setToFadeInAfterLoad(Z)V

    .line 341
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    iget-object v3, v13, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v4, v13, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 342
    if-eqz v12, :cond_6

    .line 344
    const/4 v2, 0x0

    invoke-interface {v15, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Common$Image;

    iget-object v3, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v7

    move-object/from16 v2, p0

    move/from16 v5, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/HeroGraphicView;->showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 347
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->showHeroOverlay()V

    .line 348
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->setVisibility(I)V

    goto/16 :goto_2

    .line 334
    .end local v12    # "hasVideoPreviews":Z
    .end local v13    # "hero":Lcom/google/android/finsky/protos/Common$Image;
    :cond_7
    const/4 v12, 0x0

    goto :goto_3

    .line 336
    .restart local v12    # "hasVideoPreviews":Z
    :cond_8
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Common$Image;

    move-object v13, v2

    goto :goto_4
.end method

.method public bindNewsstand(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "hideIfNoImages"    # Z

    .prologue
    const/4 v6, 0x0

    .line 471
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v6, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 472
    const/4 v3, 0x2

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 474
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 476
    return-void
.end method

.method protected configureDetailsAspectRatio(IZZ)V
    .locals 3
    .param p1, "docType"    # I
    .param p2, "supportsLeadingOverlayTransparency"    # Z
    .param p3, "limitNonFullScreenHeight"    # Z

    .prologue
    .line 370
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-eqz v1, :cond_1

    .line 374
    const v1, 0x7fffffff

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMaximumHeight(I)V

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iput-boolean p2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mSupportsLeadingOverlayTransparency:Z

    .line 377
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/HeroGraphicView;->getDetailsHeroAspectRatio(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    .line 378
    if-eqz p3, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 380
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_0

    .line 381
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMaximumHeight(I)V

    goto :goto_0
.end method

.method public freezeCorpusFill(J)V
    .locals 3
    .param p1, "fadeDurationMs"    # J

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x1

    .line 191
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    .line 192
    iget-object v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mNeedsToShowPlayIconAfterUnfreezing:Z

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 196
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    :cond_0
    return-void

    .line 192
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentAspectRatio()F
    .locals 3

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v1}, Lcom/google/android/play/image/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 255
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v1}, Lcom/google/android/play/image/FifeImageView;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 256
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    .line 258
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_0
.end method

.method protected getFillColor(I)I
    .locals 1
    .param p1, "backendId"    # I

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public getOverlayTransparencyHeightFromTop()I
    .locals 2

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mSupportsLeadingOverlayTransparency:Z

    if-nez v0, :cond_0

    .line 513
    const/4 v0, 0x0

    .line 515
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultHeight:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public isShowingNoImageFallbackFill()Z
    .locals 2

    .prologue
    .line 304
    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs load(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;[I)V
    .locals 4
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "imageTypes"    # [I

    .prologue
    .line 262
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mMaxHeight:I

    invoke-static {p2, v1, v2, p3}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 264
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-nez v0, :cond_0

    .line 265
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->setVisibility(I)V

    .line 269
    :goto_0
    return-void

    .line 268
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_0
.end method

.method public load(Lcom/google/android/play/image/BitmapLoader;Ljava/util/List;)V
    .locals 4
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/play/image/BitmapLoader;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mMaxHeight:I

    invoke-static {p2, v1, v2}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getBestImage(Ljava/util/List;II)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 274
    .local v0, "bestImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 275
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 21
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 617
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getWidth()I

    move-result v16

    .line 618
    .local v16, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getHeight()I

    move-result v6

    .line 619
    .local v6, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getPaddingRight()I

    move-result v12

    .line 620
    .local v12, "paddingRight":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCurrentOverlayTopOffset:I

    .line 622
    .local v15, "topOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v8

    .line 623
    .local v8, "heroMeasuredWidth":I
    sub-int v17, v16, v8

    sub-int v17, v17, v12

    div-int/lit8 v9, v17, 0x2

    .line 625
    .local v9, "imageLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v17, v0

    add-int v18, v9, v8

    add-int v19, v15, v6

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v9, v15, v1, v2}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    add-int v18, v18, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    add-int v19, v19, v15

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v9, v15, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 634
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v14

    .line 636
    .local v14, "playMeasuredWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    .line 637
    .local v7, "heroMeasuredHeight":I
    sub-int v17, v16, v14

    sub-int v17, v17, v12

    div-int/lit8 v9, v17, 0x2

    .line 638
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayIconVerticalSpan:I

    move/from16 v17, v0

    if-nez v17, :cond_4

    move v13, v6

    .line 640
    .local v13, "playImageVerticalSpan":I
    :goto_0
    sub-int v17, v13, v7

    div-int/lit8 v17, v17, 0x2

    add-int v10, v15, v17

    .line 641
    .local v10, "imageTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    add-int v18, v9, v14

    add-int v19, v10, v7

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v9, v10, v1, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 645
    .end local v7    # "heroMeasuredHeight":I
    .end local v10    # "imageTop":I
    .end local v13    # "playImageVerticalSpan":I
    .end local v14    # "playMeasuredWidth":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 647
    .local v4, "corpusFillHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    int-to-float v0, v15

    move/from16 v17, v0

    const/high16 v18, 0x3f000000    # 0.5f

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v5, v0

    .line 650
    .local v5, "corpusFillTop":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v19

    add-int v20, v5, v4

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 654
    .end local v4    # "corpusFillHeight":I
    .end local v5    # "corpusFillTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 658
    int-to-float v0, v15

    move/from16 v17, v0

    const/high16 v18, 0x3f000000    # 0.5f

    div-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v11, v0

    .line 659
    .local v11, "overlayTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    add-int v19, v19, v11

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    move/from16 v3, v19

    invoke-virtual {v0, v1, v11, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 662
    .end local v11    # "overlayTop":I
    :cond_3
    return-void

    .line 638
    .restart local v7    # "heroMeasuredHeight":I
    .restart local v14    # "playMeasuredWidth":I
    :cond_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayIconVerticalSpan:I

    goto/16 :goto_0

    .end local v7    # "heroMeasuredHeight":I
    .end local v14    # "playMeasuredWidth":I
    .restart local v4    # "corpusFillHeight":I
    :cond_5
    move v5, v15

    .line 647
    goto :goto_1
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 11
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v10, 0x0

    .line 123
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsImageLoaded:Z

    .line 125
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    if-nez v8, :cond_0

    .line 126
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->shouldTopAlignHeroImage()Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz p2, :cond_1

    .line 134
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 135
    .local v2, "bitmapWidth":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 137
    .local v1, "bitmapHeight":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v4

    .line 139
    .local v4, "imageWidth":I
    int-to-float v8, v1

    int-to-float v9, v2

    div-float v0, v8, v9

    .line 141
    .local v0, "bitmapAspectRatio":F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 142
    .local v5, "matrix":Landroid/graphics/Matrix;
    new-instance v6, Landroid/graphics/RectF;

    int-to-float v8, v2

    int-to-float v9, v1

    invoke-direct {v6, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 143
    .local v6, "srcRect":Landroid/graphics/RectF;
    int-to-float v8, v4

    mul-float/2addr v8, v0

    float-to-int v7, v8

    .line 149
    .local v7, "targetHeight":I
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v8, v4

    int-to-float v9, v7

    invoke-direct {v3, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 150
    .local v3, "dstRect":Landroid/graphics/RectF;
    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v6, v3, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 152
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    sget-object v9, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 153
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8, v5}, Lcom/google/android/play/image/FifeImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 156
    .end local v0    # "bitmapAspectRatio":F
    .end local v1    # "bitmapHeight":I
    .end local v2    # "bitmapWidth":I
    .end local v3    # "dstRect":Landroid/graphics/RectF;
    .end local v4    # "imageWidth":I
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "srcRect":Landroid/graphics/RectF;
    .end local v7    # "targetHeight":I
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCallerOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    if-eqz v8, :cond_2

    .line 157
    iget-object v8, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCallerOnLoadedListener:Lcom/google/android/play/image/FifeImageView$OnLoadedListener;

    invoke-interface {v8, p1, p2}, Lcom/google/android/play/image/FifeImageView$OnLoadedListener;->onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V

    .line 159
    :cond_2
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 2
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    if-nez v0, :cond_0

    .line 174
    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mBackendId:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->setCorpusFillMode(II)V

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mNeedsToShowPlayIconAfterUnfreezing:Z

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 22
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/widget/ImageView;->measure(II)V

    .line 543
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 544
    .local v4, "availableWidth":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 547
    .local v3, "availableHeight":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v19

    if-eqz v19, :cond_3

    move v12, v3

    .line 550
    .local v12, "maxHeight":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 554
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMeasuredDimension(II)V

    .line 600
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getMeasuredHeight()I

    move-result v14

    .line 601
    .local v14, "measuredHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 602
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    mul-int/lit8 v19, v14, 0x3

    div-int/lit8 v5, v19, 0x4

    .line 604
    .local v5, "corpusFillHeight":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 608
    .end local v5    # "corpusFillHeight":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 613
    :cond_2
    return-void

    .line 547
    .end local v12    # "maxHeight":I
    .end local v14    # "measuredHeight":I
    :cond_3
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mMaxHeight:I

    goto/16 :goto_0

    .line 559
    .restart local v12    # "maxHeight":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 560
    .local v6, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v6, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->isLoaded()Z

    move-result v19

    if-eqz v19, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v19, v0

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-lez v19, :cond_7

    .line 561
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v19, v0

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-nez v19, :cond_6

    move v7, v12

    .line 563
    .local v7, "height":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 565
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMeasuredDimension(II)V

    .line 591
    .end local v7    # "height":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v9

    .line 593
    .local v9, "heroImageWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v8

    .line 594
    .local v8, "heroImageHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    const/high16 v21, 0x40000000    # 2.0f

    move/from16 v0, v21

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Landroid/view/View;->measure(II)V

    goto/16 :goto_1

    .line 561
    .end local v8    # "heroImageHeight":I
    .end local v9    # "heroImageWidth":I
    :cond_6
    int-to-float v0, v4

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v0, v12}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_3

    .line 567
    :cond_7
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    .line 568
    .local v11, "imWidth":I
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    .line 569
    .local v10, "imHeight":I
    mul-int v19, v4, v10

    div-int v15, v19, v11

    .line 571
    .local v15, "scaledHeight":I
    if-gt v15, v12, :cond_8

    .line 574
    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    .line 576
    .local v16, "scaledHeightSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 577
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMeasuredDimension(II)V

    goto/16 :goto_4

    .line 581
    .end local v16    # "scaledHeightSpec":I
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    move/from16 v19, v0

    if-eqz v19, :cond_9

    move/from16 v17, v4

    .line 583
    .local v17, "scaledWidth":I
    :goto_5
    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v19

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    .line 585
    .local v18, "scaledWidthSpec":I
    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v12, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    .line 587
    .local v13, "maxHeightSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1, v13}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 588
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMeasuredDimension(II)V

    goto/16 :goto_4

    .line 581
    .end local v13    # "maxHeightSpec":I
    .end local v17    # "scaledWidth":I
    .end local v18    # "scaledWidthSpec":I
    :cond_9
    mul-int v19, v12, v11

    div-int v17, v19, v10

    goto :goto_5

    .end local v6    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v10    # "imHeight":I
    .end local v11    # "imWidth":I
    .end local v15    # "scaledHeight":I
    .restart local v14    # "measuredHeight":I
    :cond_a
    move v5, v14

    .line 602
    goto/16 :goto_2
.end method

.method public setCorpusFillMode(II)V
    .locals 5
    .param p1, "corpusFillMode"    # I
    .param p2, "backendId"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    if-nez v0, :cond_0

    .line 286
    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    if-eq v0, p1, :cond_0

    .line 287
    iput p1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    .line 288
    iget-object v3, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    iget v3, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillMode:I

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mCorpusFillView:Landroid/view/View;

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/HeroGraphicView;->getFillColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 288
    goto :goto_1

    :cond_3
    move v1, v2

    .line 290
    goto :goto_2
.end method

.method public setDefaultAspectRatio(F)V
    .locals 0
    .param p1, "defaultAspectRatio"    # F

    .prologue
    .line 250
    iput p1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mDefaultAspectRatio:F

    .line 251
    return-void
.end method

.method public setMaximumHeight(I)V
    .locals 0
    .param p1, "maxHeight"    # I

    .prologue
    .line 536
    iput p1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mMaxHeight:I

    .line 537
    return-void
.end method

.method public setPlayIconVerticalSpan(I)V
    .locals 1
    .param p1, "playIconVerticalSpan"    # I

    .prologue
    .line 529
    iget v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayIconVerticalSpan:I

    if-eq v0, p1, :cond_0

    .line 530
    iput p1, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayIconVerticalSpan:I

    .line 531
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->requestLayout()V

    .line 533
    :cond_0
    return-void
.end method

.method protected shouldTopAlignHeroImage()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    return v0
.end method

.method public showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "youtubeUrl"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "preferVideosAppForPlayback"    # Z
    .param p4, "isMature"    # Z
    .param p5, "backend"    # I
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 239
    invoke-super/range {p0 .. p6}, Lcom/google/android/finsky/layout/YoutubeFrameView;->showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 241
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v0, :cond_0

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mNeedsToShowPlayIconAfterUnfreezing:Z

    .line 245
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 247
    :cond_0
    return-void
.end method

.method public unfreezeCorpusFill(J)V
    .locals 5
    .param p1, "fadeDurationMs"    # J

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 211
    iput-boolean v4, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFrozenInCorpusFill:Z

    .line 213
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsImageLoaded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mIsFullScreenMode:Z

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v4}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/play/image/FifeImageView;->setAlpha(F)V

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/HeroGraphicView$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/HeroGraphicView$1;-><init>(Lcom/google/android/finsky/layout/HeroGraphicView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 224
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mNeedsToShowPlayIconAfterUnfreezing:Z

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 227
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 228
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 230
    iget-object v0, p0, Lcom/google/android/finsky/layout/HeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 234
    :cond_0
    return-void
.end method
