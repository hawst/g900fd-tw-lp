.class public interface abstract Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;
.super Ljava/lang/Object;
.source "PackageInstallerFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/installer/PackageInstallerFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InstallListener"
.end annotation


# virtual methods
.method public abstract installBeginning()V
.end method

.method public abstract installFailed(ILjava/lang/String;)V
.end method

.method public abstract installSucceeded()V
.end method
