.class public Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;
.super Lcom/google/android/finsky/api/model/DfeModel;
.source "MultiDfeBulkDetails.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# instance fields
.field private mLastVolleyError:Lcom/android/volley/VolleyError;

.field protected final mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/DfeBulkDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addRequest(Lcom/google/android/finsky/api/DfeApi;Ljava/util/List;)V
    .locals 2
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeApi;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "docids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-direct {v0, p1, p2}, Lcom/google/android/finsky/api/model/DfeBulkDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/util/List;)V

    .line 38
    .local v0, "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 39
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 40
    iget-object v1, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    return-void
.end method

.method protected addResponsesForTest(Lcom/google/android/finsky/api/DfeApi;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V
    .locals 2
    .param p1, "mockDfe"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "responses"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-direct {v0, p1, p2}, Lcom/google/android/finsky/api/model/DfeBulkDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 53
    .local v0, "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    iget-object v1, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method protected clearErrors()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected collateResponses()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public inErrorState()Z
    .locals 1

    .prologue
    .line 115
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public isReady()Z
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    .line 60
    .local v1, "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->isReady()Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    const/4 v2, 0x0

    .line 64
    .end local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :goto_1
    return v2

    .line 58
    .restart local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    .end local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public onDataChanged()V
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 73
    iget-object v2, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mRequests:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    .line 74
    .local v1, "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->inErrorState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 85
    .end local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :cond_0
    :goto_1
    return-void

    .line 77
    .restart local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->isReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    .end local v1    # "request":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->collateResponses()V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mLastVolleyError:Lcom/android/volley/VolleyError;

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->notifyErrorOccured(Lcom/android/volley/VolleyError;)V

    .line 95
    iput-object p1, p0, Lcom/google/android/finsky/api/model/MultiDfeBulkDetails;->mLastVolleyError:Lcom/android/volley/VolleyError;

    .line 97
    :cond_0
    return-void
.end method
