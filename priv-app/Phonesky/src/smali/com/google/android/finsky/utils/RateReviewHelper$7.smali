.class final Lcom/google/android/finsky/utils/RateReviewHelper$7;
.super Ljava/lang/Object;
.source "RateReviewHelper.java"

# interfaces
.implements Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/RateReviewHelper;->rateDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;

.field final synthetic val$docDetailsUrl:Ljava/lang/String;

.field final synthetic val$docId:Ljava/lang/String;

.field final synthetic val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

.field final synthetic val$rating:I

.field final synthetic val$reviewSource:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;Ljava/lang/String;Landroid/support/v4/app/FragmentActivity;I)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$accountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$docId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rating:I

    iput-object p4, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    iput-object p5, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$docDetailsUrl:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iput p7, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$reviewSource:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckAndConfirmGPlusFailed()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;->onRateReviewFailed()V

    .line 267
    :cond_0
    return-void
.end method

.method public onCheckAndConfirmGPlusPassed(Lcom/google/android/finsky/api/model/Document;)V
    .locals 11
    .param p1, "plusDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 243
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4097700000000000L    # 1500.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit16 v10, v0, 0x1f4

    .line 245
    .local v10, "delay":I
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/finsky/utils/RateReviewHelper$7$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper$7$1;-><init>(Lcom/google/android/finsky/utils/RateReviewHelper$7;Lcom/google/android/finsky/api/model/Document;)V

    int-to-long v2, v10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    .end local v10    # "delay":I
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$accountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$docId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$docDetailsUrl:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rating:I

    const-string v4, ""

    const-string v5, ""

    iget-object v7, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v8, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    iget v9, p0, Lcom/google/android/finsky/utils/RateReviewHelper$7;->val$reviewSource:I

    move-object v6, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/RateReviewHelper;->updateReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V

    goto :goto_0
.end method
