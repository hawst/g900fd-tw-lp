.class Lcom/google/android/finsky/layout/OrderHistoryRowView$3;
.super Ljava/lang/Object;
.source "OrderHistoryRowView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/OrderHistoryRowView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/Boolean;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

.field final synthetic val$analyzer:Lcom/google/android/finsky/activities/AppActionAnalyzer;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$onRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/activities/AppActionAnalyzer;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$onRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

    iput-object p3, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$analyzer:Lcom/google/android/finsky/activities/AppActionAnalyzer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$600(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0xa2b

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$onRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;->val$analyzer:Lcom/google/android/finsky/activities/AppActionAnalyzer;

    iget-object v2, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->refundAccount:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;->onRefundAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    return-void
.end method
