.class Lcom/google/android/finsky/receivers/InstallerTask$8;
.super Landroid/os/AsyncTask;
.source "InstallerTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->startApplyingPatch(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$downloadUri:Landroid/net/Uri;

.field final synthetic val$downloadUriString:Ljava/lang/String;

.field final synthetic val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2508
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUriString:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/io/File;
    .locals 29
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v21

    .line 2513
    .local v21, "packageName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2514
    .local v10, "inputFile":Ljava/io/RandomAccessFile;
    const/16 v22, 0x0

    .line 2515
    .local v22, "patchStream":Ljava/io/InputStream;
    const/16 v18, 0x0

    .line 2516
    .local v18, "outputStream":Ljava/io/OutputStream;
    const/16 v17, 0x0

    .line 2517
    .local v17, "outputFile":Ljava/io/File;
    const/4 v12, 0x0

    .line 2522
    .local v12, "keepOutputFile":Z
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    .line 2523
    .local v4, "appContext":Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    .line 2526
    .local v23, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v20

    .line 2528
    .local v20, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v25, 0x0

    :try_start_1
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 2530
    .local v5, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v11, Ljava/io/RandomAccessFile;

    new-instance v25, Ljava/io/File;

    iget-object v0, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-direct/range {v25 .. v26}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v26, "r"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v11, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2541
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .local v11, "inputFile":Ljava/io/RandomAccessFile;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->getPatch(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v22

    .line 2542
    if-nez v22, :cond_2

    .line 2543
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_0

    if-eqz v17, :cond_0

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_0
    move-object v10, v11

    .end local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    :cond_1
    :goto_0
    return-object v25

    .line 2531
    :catch_0
    move-exception v8

    .line 2532
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "source-NameNotFoundException"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2533
    const-string v25, "NameNotFoundException %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    move-object/from16 v28, v0

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2534
    const/16 v25, 0x0

    .line 2613
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_1

    if-eqz v17, :cond_1

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2535
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v8

    .line 2536
    .local v8, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "source-FileNotFoundException"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2537
    const-string v25, "FileNotFoundException %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    move-object/from16 v28, v0

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2538
    const/16 v25, 0x0

    .line 2613
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_1

    if-eqz v17, :cond_1

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2551
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    :cond_2
    :try_start_5
    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    .line 2552
    .local v6, "cacheDir":Ljava/io/File;
    const/16 v25, 0x1

    const/16 v26, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v25

    if-nez v25, :cond_4

    .line 2553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "cache-dir-executable"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2554
    const-string v25, "Could not make executable %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v6, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2555
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_3

    if-eqz v17, :cond_3

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_3
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2558
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    :cond_4
    :try_start_6
    new-instance v16, Ljava/io/File;

    const-string v25, "patches"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-direct {v0, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2559
    .local v16, "outputDirectory":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdirs()Z

    .line 2561
    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v25

    if-eqz v25, :cond_6

    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v25

    if-eqz v25, :cond_6

    const/4 v13, 0x1

    .line 2563
    .local v13, "madeReadable":Z
    :goto_1
    if-nez v13, :cond_7

    .line 2564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "out-dir-readable"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2565
    const-string v25, "Could not make readable %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v16, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2566
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_5

    if-eqz v17, :cond_5

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_5
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2561
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v13    # "madeReadable":Z
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    :cond_6
    const/4 v13, 0x0

    goto :goto_1

    .line 2568
    .restart local v13    # "madeReadable":Z
    :cond_7
    :try_start_7
    const-string v25, ".apk"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v17

    .line 2570
    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v25

    if-nez v25, :cond_9

    .line 2571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "out-file-readable"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2572
    const-string v25, "Could not make readable %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v17, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2573
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2574
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_8

    if-eqz v17, :cond_8

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_8
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2576
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    :cond_9
    :try_start_8
    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2583
    .end local v18    # "outputStream":Ljava/io/OutputStream;
    .local v19, "outputStream":Ljava/io/OutputStream;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v25

    move-object/from16 v0, v25

    iget-wide v14, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 2585
    .local v14, "maxOutputLength":J
    :try_start_a
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-static {v11, v0, v1, v14, v15}, Lcom/google/android/finsky/installer/Gdiff;->patch(Ljava/io/RandomAccessFile;Ljava/io/InputStream;Ljava/io/OutputStream;J)J
    :try_end_a
    .catch Lcom/google/android/finsky/installer/Gdiff$FileFormatException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 2586
    const/4 v12, 0x1

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_a

    if-eqz v17, :cond_a

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_a
    move-object/from16 v18, v19

    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v18    # "outputStream":Ljava/io/OutputStream;
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    move-object/from16 v25, v17

    goto/16 :goto_0

    .line 2577
    .end local v6    # "cacheDir":Ljava/io/File;
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v13    # "madeReadable":Z
    .end local v14    # "maxOutputLength":J
    .end local v16    # "outputDirectory":Ljava/io/File;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v9

    .line 2578
    .local v9, "e1":Ljava/io/IOException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "out-dir-IOException"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2579
    const-string v25, "IOException while opening: %s"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 2580
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_b

    if-eqz v17, :cond_b

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_b
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2588
    .end local v9    # "e1":Ljava/io/IOException;
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v18    # "outputStream":Ljava/io/OutputStream;
    .restart local v6    # "cacheDir":Ljava/io/File;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v13    # "madeReadable":Z
    .restart local v14    # "maxOutputLength":J
    .restart local v16    # "outputDirectory":Ljava/io/File;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    :catch_3
    move-exception v8

    .line 2589
    .local v8, "e":Lcom/google/android/finsky/installer/Gdiff$FileFormatException;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "gdiff-FileFormatException"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUriString:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    .line 2594
    .local v24, "uriString":Ljava/lang/String;
    const-string v25, "my_downloads"

    const-string v26, "public_downloads"

    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 2595
    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 2596
    .local v7, "contentType":Ljava/lang/String;
    const-string v25, "Patch %s (content-type \'%s\') is not Gdiff, will install as APK"

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v21, v26, v27

    const/16 v27, 0x1

    aput-object v7, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2601
    new-instance v25, Ljava/io/File;

    const-string v26, ""

    invoke-direct/range {v25 .. v26}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_c

    if-eqz v17, :cond_c

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_c
    move-object/from16 v18, v19

    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v18    # "outputStream":Ljava/io/OutputStream;
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2602
    .end local v7    # "contentType":Ljava/lang/String;
    .end local v8    # "e":Lcom/google/android/finsky/installer/Gdiff$FileFormatException;
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v18    # "outputStream":Ljava/io/OutputStream;
    .end local v24    # "uriString":Ljava/lang/String;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    :catch_4
    move-exception v8

    .line 2603
    .local v8, "e":Ljava/io/IOException;
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "gdiff-IOException"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2604
    const-string v25, "Patch %s failed with exception "

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v21, v26, v27

    const/16 v27, 0x1

    aput-object v8, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 2605
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_d

    if-eqz v17, :cond_d

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_d
    move-object/from16 v18, v19

    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v18    # "outputStream":Ljava/io/OutputStream;
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2606
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v18    # "outputStream":Ljava/io/OutputStream;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    :catch_5
    move-exception v8

    .line 2607
    .local v8, "e":Ljava/lang/Exception;
    :try_start_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    move-object/from16 v25, v0

    const-string v26, "gdiff-Exception"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2608
    const-string v25, "Patch %s failed with exception "

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v21, v26, v27

    const/16 v27, 0x1

    aput-object v8, v26, v27

    invoke-static/range {v25 .. v26}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 2609
    const/16 v25, 0x0

    .line 2613
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_e

    if-eqz v17, :cond_e

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_e
    move-object/from16 v18, v19

    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v18    # "outputStream":Ljava/io/OutputStream;
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_0

    .line 2613
    .end local v4    # "appContext":Landroid/content/Context;
    .end local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "cacheDir":Ljava/io/File;
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v13    # "madeReadable":Z
    .end local v14    # "maxOutputLength":J
    .end local v16    # "outputDirectory":Ljava/io/File;
    .end local v20    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v23    # "resolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v25

    :goto_2
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2614
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2615
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2616
    if-nez v12, :cond_f

    if-eqz v17, :cond_f

    .line 2617
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    :cond_f
    throw v25

    .line 2613
    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v4    # "appContext":Landroid/content/Context;
    .restart local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v20    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v23    # "resolver":Landroid/content/ContentResolver;
    :catchall_1
    move-exception v25

    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto :goto_2

    .end local v10    # "inputFile":Ljava/io/RandomAccessFile;
    .end local v18    # "outputStream":Ljava/io/OutputStream;
    .restart local v6    # "cacheDir":Ljava/io/File;
    .restart local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v13    # "madeReadable":Z
    .restart local v16    # "outputDirectory":Ljava/io/File;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    :catchall_2
    move-exception v25

    move-object/from16 v18, v19

    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v18    # "outputStream":Ljava/io/OutputStream;
    move-object v10, v11

    .end local v11    # "inputFile":Ljava/io/RandomAccessFile;
    .restart local v10    # "inputFile":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2508
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$8;->doInBackground([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/io/File;)V
    .locals 8
    .param p1, "patchedFile"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0x3c

    const/4 v4, 0x0

    .line 2630
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V
    invoke-static {v0, v7, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2300(Lcom/google/android/finsky/receivers/InstallerTask;ILandroid/net/Uri;)V

    .line 2633
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 2665
    :goto_0
    return-void

    .line 2638
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1900(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$downloadUri:Landroid/net/Uri;

    invoke-interface {v0, v2}, Lcom/google/android/finsky/download/DownloadQueue;->release(Landroid/net/Uri;)V

    .line 2641
    if-nez p1, :cond_1

    .line 2643
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2000(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 2646
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/4 v2, 0x4

    const/16 v3, 0x8

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z
    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2100(Lcom/google/android/finsky/receivers/InstallerTask;II)Z

    goto :goto_0

    .line 2652
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->isGzippedPatch(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x7a

    .line 2655
    .local v1, "type":I
    :goto_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2657
    const-string v0, "Successfully applied patch to update %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v3, v3, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2660
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V
    invoke-static {v0, v7, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1600(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    .line 2664
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$8;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V

    goto :goto_0

    .line 2652
    .end local v1    # "type":I
    :cond_2
    const/16 v1, 0x6c

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2508
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$8;->onPostExecute(Ljava/io/File;)V

    return-void
.end method
