.class Lcom/google/android/finsky/services/ReviewsService$1;
.super Lcom/android/vending/reviews/IReviewsService$Stub;
.source "ReviewsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/ReviewsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/ReviewsService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/ReviewsService;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/finsky/services/ReviewsService$1;->this$0:Lcom/google/android/finsky/services/ReviewsService;

    invoke-direct {p0}, Lcom/android/vending/reviews/IReviewsService$Stub;-><init>()V

    return-void
.end method

.method private populateResponseBundle(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/protos/DocumentV2$Review;)V
    .locals 9
    .param p1, "response"    # Landroid/os/Bundle;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "plusProfileDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "doc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p5, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    const/4 v8, 0x0

    .line 160
    new-instance v1, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v1, p4}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    if-eqz p5, :cond_1

    iget v4, p5, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    :goto_0
    const/4 v5, 0x1

    move-object v0, p2

    move-object v2, p3

    move-object v3, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/activities/RateReviewActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;IZ)Landroid/content/Intent;

    move-result-object v6

    .line 166
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "reviewsservice"

    iget-object v1, p4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    # getter for: Lcom/google/android/finsky/services/ReviewsService;->sRequestCounter:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {}, Lcom/google/android/finsky/services/ReviewsService;->access$000()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/services/ReviewsService$1;->this$0:Lcom/google/android/finsky/services/ReviewsService;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v8, v6, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 174
    .local v7, "rateAndReviewIntent":Landroid/app/PendingIntent;
    const-string v0, "rate_and_review_intent"

    invoke-virtual {p1, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 175
    const-string v0, "rate_and_review_request_code"

    const/16 v1, 0x2b

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    const-string v0, "doc_id"

    iget-object v1, p4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v0, "doc_title"

    iget-object v1, p4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    if-eqz p5, :cond_0

    .line 179
    const-string v0, "rating"

    iget v1, p5, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    const-string v0, "review_title"

    iget-object v1, p5, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "review_comment"

    iget-object v1, p5, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    const-string v0, "author_title"

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v1, "author_profile_image_url"

    const/4 v0, 0x4

    invoke-virtual {p3, v0}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void

    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "rateAndReviewIntent":Landroid/app/PendingIntent;
    :cond_1
    move v4, v8

    .line 160
    goto :goto_0
.end method


# virtual methods
.method public getRateAndReviewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 43
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "documentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v34

    .line 57
    .local v34, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-static/range {v34 .. v34}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v32

    .line 58
    .local v32, "deviceAccounts":[Landroid/accounts/Account;
    const/16 v28, 0x0

    .line 59
    .local v28, "accountToUse":Landroid/accounts/Account;
    move-object/from16 v29, v32

    .local v29, "arr$":[Landroid/accounts/Account;
    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v37, v0

    .local v37, "len$":I
    const/16 v36, 0x0

    .local v36, "i$":I
    :goto_0
    move/from16 v0, v36

    move/from16 v1, v37

    if-ge v0, v1, :cond_0

    aget-object v31, v29, v36

    .line 60
    .local v31, "deviceAccount":Landroid/accounts/Account;
    move-object/from16 v0, v31

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 61
    move-object/from16 v28, v31

    .line 65
    .end local v31    # "deviceAccount":Landroid/accounts/Account;
    :cond_0
    if-nez v28, :cond_3

    .line 66
    const-string v4, "No account found for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    const/16 v16, 0x0

    .line 150
    :cond_1
    :goto_1
    return-object v16

    .line 59
    .restart local v31    # "deviceAccount":Landroid/accounts/Account;
    :cond_2
    add-int/lit8 v36, v36, 0x1

    goto :goto_0

    .line 70
    .end local v31    # "deviceAccount":Landroid/accounts/Account;
    :cond_3
    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    .line 71
    .local v3, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/services/ReviewsService$1;->this$0:Lcom/google/android/finsky/services/ReviewsService;

    const/4 v5, 0x0

    const/16 v6, 0x202

    move-object/from16 v0, p2

    invoke-static {v4, v0, v5, v3, v6}, Lcom/google/android/finsky/utils/SignatureUtils;->getCallingAppIfAuthorized(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/play/utils/config/GservicesValue;Lcom/google/android/finsky/analytics/FinskyEventLog;I)Ljava/lang/String;

    move-result-object v7

    .line 73
    .local v7, "callingApp":Ljava/lang/String;
    if-nez v7, :cond_4

    .line 75
    const/16 v16, 0x0

    goto :goto_1

    .line 77
    :cond_4
    const-string v4, "Received rate&review request for %s from %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    new-instance v16, Landroid/os/Bundle;

    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v16, "response":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v9

    .line 85
    .local v9, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApi()Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v39

    .line 86
    .local v39, "playDfeApi":Lcom/google/android/play/dfe/api/PlayDfeApi;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v35

    .line 87
    .local v35, "futurePlusCheck":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;>;"
    const/4 v4, 0x1

    move-object/from16 v0, v39

    move-object/from16 v1, v35

    move-object/from16 v2, v35

    invoke-interface {v0, v1, v2, v4}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 90
    :try_start_0
    invoke-virtual/range {v35 .. v35}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    .local v42, "responsePlusCheck":Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    move-object/from16 v0, v42

    iget-object v0, v0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v40, v0

    .line 104
    .local v40, "plusDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedPlusReviews:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    .line 106
    .local v27, "acceptedPlusReviews":Z
    if-eqz v27, :cond_1

    if-eqz v40, :cond_1

    .line 112
    new-instance v18, Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, v18

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 115
    .local v18, "plusProfileDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v14

    .line 116
    .local v14, "futureDetails":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    invoke-static/range {p2 .. p2}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object v15, v14

    invoke-interface/range {v9 .. v15}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/lang/String;ZZLjava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 121
    :try_start_1
    invoke-virtual {v14}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/google/android/finsky/protos/Details$DetailsResponse;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3

    .line 134
    .local v41, "responseDetails":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v19, v0

    .line 135
    .local v19, "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-nez v19, :cond_7

    .line 136
    const-string v4, "No doc in details response for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 91
    .end local v14    # "futureDetails":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    .end local v18    # "plusProfileDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v19    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v27    # "acceptedPlusReviews":Z
    .end local v40    # "plusDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v41    # "responseDetails":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .end local v42    # "responsePlusCheck":Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    :catch_0
    move-exception v33

    .line 92
    .local v33, "e":Ljava/lang/InterruptedException;
    const-string v4, "Interrupted while trying to retrieve plus profile"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 94
    .end local v33    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v33

    .line 95
    .local v33, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual/range {v33 .. v33}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v30

    .line 96
    .local v30, "cause":Ljava/lang/Throwable;
    if-nez v30, :cond_5

    const/4 v8, 0x0

    .line 97
    .local v8, "exceptionType":Ljava/lang/String;
    :goto_2
    const-string v4, "Unable to retrieve plus profile: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    const/16 v4, 0x202

    const-string v6, "fetch-plus-error"

    move-object/from16 v5, p2

    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 96
    .end local v8    # "exceptionType":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    .line 122
    .end local v30    # "cause":Ljava/lang/Throwable;
    .end local v33    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v14    # "futureDetails":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    .restart local v18    # "plusProfileDoc":Lcom/google/android/finsky/api/model/Document;
    .restart local v27    # "acceptedPlusReviews":Z
    .restart local v40    # "plusDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .restart local v42    # "responsePlusCheck":Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    :catch_2
    move-exception v33

    .line 123
    .local v33, "e":Ljava/lang/InterruptedException;
    const-string v4, "Interrupted while trying to retrieve item details"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 125
    .end local v33    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v33

    .line 126
    .local v33, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual/range {v33 .. v33}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v30

    .line 127
    .restart local v30    # "cause":Ljava/lang/Throwable;
    if-nez v30, :cond_6

    const/4 v8, 0x0

    .line 128
    .restart local v8    # "exceptionType":Ljava/lang/String;
    :goto_3
    const-string v4, "Unable to retrieve item details: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    const/16 v4, 0x202

    const-string v6, "fetch-doc-error"

    move-object/from16 v5, p2

    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 127
    .end local v8    # "exceptionType":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    .line 141
    .end local v30    # "cause":Ljava/lang/Throwable;
    .end local v33    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v19    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .restart local v41    # "responseDetails":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    :cond_7
    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v38

    .line 142
    .local v38, "mutationCache":Lcom/google/android/finsky/utils/ClientMutationCache;
    move-object/from16 v0, v41

    iget-object v4, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-object/from16 v0, v38

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v4}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v20

    .local v20, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    move-object/from16 v15, p0

    move-object/from16 v17, p1

    .line 146
    invoke-direct/range {v15 .. v20}, Lcom/google/android/finsky/services/ReviewsService$1;->populateResponseBundle(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    .line 148
    const/16 v22, 0x202

    const/16 v24, 0x0

    const/16 v26, 0x0

    move-object/from16 v21, v3

    move-object/from16 v23, p2

    move-object/from16 v25, v7

    invoke-static/range {v21 .. v26}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
