.class public Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PurchaseActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$Listener;
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# static fields
.field private static final DISABLE_DISMISS_ON_TOUCH_OUTSIDE:Z


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mHitRect:Landroid/graphics/Rect;

.field private mInitialServerLogsCookie:[B

.field protected mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->DISABLE_DISMISS_ON_TOUCH_OUTSIDE:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 46
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mHitRect:Landroid/graphics/Rect;

    return-void
.end method

.method public static createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "params"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .param p2, "serverLogsCookie"    # [B
    .param p3, "appDownloadSizeWarningArgs"    # Landroid/os/Bundle;

    .prologue
    .line 169
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "PurchaseActivity.account"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 171
    const-string v1, "PurchaseActivity.params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 172
    const-string v1, "PurchaseActivity.appDownloadSizeWarningArgs"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 173
    const-string v1, "PurchaseActivity.serverLogsCookie"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 174
    return-object v0
.end method

.method private dismissIfPossible(ILjava/lang/String;)V
    .locals 4
    .param p1, "dismissType"    # I
    .param p2, "debugEvent"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    .line 97
    .local v0, "fragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isDismissable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->onDialogDismissed()V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->finish()V

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v1, "PurchaseFragment not dismissable by %s, ignore."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    return-object v0
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 212
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 115
    sget-boolean v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->DISABLE_DISMISS_ON_TOUCH_OUTSIDE:Z

    if-eqz v0, :cond_0

    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 124
    :goto_0
    return v0

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x259

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 122
    const/4 v0, 0x2

    const-string v1, "click outside"

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->dismissIfPossible(ILjava/lang/String;)V

    .line 124
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 5

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    .line 130
    .local v0, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->onFinish(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V

    .line 132
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v4, 0x25b

    invoke-virtual {v1, v2, v3, v4, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 136
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 137
    return-void

    .line 134
    :cond_0
    const-string v1, "Purchase fragment null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public flushImpression()V
    .locals 2

    .prologue
    .line 222
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 183
    const/16 v4, 0x2bc

    invoke-static {v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    .line 185
    .local v3, "uiElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 186
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->docidStr:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    .line 187
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasDocument:Z

    .line 188
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget v4, v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->offerType:I

    iput v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    .line 189
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasOfferType:Z

    .line 191
    iput-object v0, v3, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 192
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v1

    .line 194
    .local v1, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    if-eqz v1, :cond_0

    .line 195
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getServerLogsCookie()[B

    move-result-object v2

    .line 200
    .local v2, "serverLogsCookie":[B
    :goto_0
    invoke-static {v3, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 202
    return-object v3

    .line 197
    .end local v2    # "serverLogsCookie":[B
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mInitialServerLogsCookie:[B

    .restart local v2    # "serverLogsCookie":[B
    goto :goto_0
.end method

.method protected handleAccessRestriction()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->setResult(I)V

    .line 86
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x258

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 92
    const/4 v0, 0x1

    const-string v1, "back press"

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->dismissIfPossible(ILjava/lang/String;)V

    .line 93
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0400bd

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->setContentView(Landroid/view/View;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 58
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "PurchaseActivity.params"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    .line 59
    const-string v4, "PurchaseActivity.account"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/accounts/Account;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mAccount:Landroid/accounts/Account;

    .line 60
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 61
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isLimitedOrSchoolEduUser()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 62
    invoke-static {p0}, Lcom/google/android/finsky/activities/AccessRestrictedActivity;->showLimitedPurchaseUI(Landroid/app/Activity;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->handleAccessRestriction()V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->finish()V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    const-string v4, "PurchaseActivity.serverLogsCookie"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mInitialServerLogsCookie:[B

    .line 68
    if-nez p1, :cond_0

    .line 69
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 71
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mAccount:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->mInitialServerLogsCookie:[B

    const-string v7, "PurchaseActivity.appDownloadSizeWarningArgs"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->newInstance(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    .line 74
    .local v2, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    const v4, 0x7f0a00c4

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 75
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method protected onDialogDismissed()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->setResult(I)V

    .line 111
    return-void
.end method

.method protected onFinish(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V
    .locals 8
    .param p1, "purchaseFragment"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .prologue
    const/4 v7, 0x0

    const/4 v1, -0x1

    .line 144
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hasSucceeded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->setResult(I)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hasFailed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getError()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;

    move-result-object v0

    .line 148
    .local v0, "error":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
    if-eqz v0, :cond_3

    iget v2, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorType:I

    .line 149
    .local v2, "errorType":I
    :goto_1
    if-eqz v0, :cond_2

    iget v1, v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorSubtype:I

    .line 150
    .local v1, "errorSubtype":I
    :cond_2
    const-string v3, "Purchase failed: %d / %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->setResult(I)V

    goto :goto_0

    .end local v1    # "errorSubtype":I
    .end local v2    # "errorType":I
    :cond_3
    move v2, v1

    .line 148
    goto :goto_1
.end method

.method public onFinished()V
    .locals 0

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->finish()V

    .line 160
    return-void
.end method

.method public startNewImpression()V
    .locals 2

    .prologue
    .line 217
    const-string v0, "Not using impression id\'s."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    return-void
.end method
