.class public abstract Lcom/google/android/finsky/layout/AddCreditCardFields;
.super Landroid/widget/RelativeLayout;
.source "AddCreditCardFields.java"

# interfaces
.implements Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;,
        Lcom/google/android/finsky/layout/AddCreditCardFields$CvcTextWatcher;,
        Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;
    }
.end annotation


# static fields
.field protected static CREDIT_CARD_IMAGES_TYPE_ORDER:[Lcom/google/android/finsky/billing/creditcard/CreditCardType;

.field private static KEY_ALL_FIELDS_VISIBLE:Ljava/lang/String;

.field private static KEY_CARD_TYPE:Ljava/lang/String;

.field private static KEY_PARENT_STATE:Ljava/lang/String;


# instance fields
.field private mAllFieldsVisible:Z

.field private final mAutoFocusHandler:Landroid/os/Handler;

.field protected mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

.field protected mCreditCardImages:[Landroid/widget/ImageView;

.field protected mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

.field protected mCvcField:Landroid/widget/EditText;

.field protected mCvcImage:Landroid/widget/ImageView;

.field protected mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

.field private mIsLightTheme:Z

.field protected mMonthField:Landroid/widget/EditText;

.field protected mMonthYearSeparator:Landroid/widget/TextView;

.field protected mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

.field private mOnAllFieldsVisibleListener:Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;

.field protected mPrivacyFooter:Landroid/widget/TextView;

.field protected mYearField:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-string v0, "parent_instance_state"

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_PARENT_STATE:Ljava/lang/String;

    .line 37
    const-string v0, "all_fields_visible"

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_ALL_FIELDS_VISIBLE:Ljava/lang/String;

    .line 38
    const-string v0, "card_type"

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_CARD_TYPE:Ljava/lang/String;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->VISA:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->MC:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->AMEX:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->DISCOVER:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->JCB:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->CREDIT_CARD_IMAGES_TYPE_ORDER:[Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mIsLightTheme:Z

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAutoFocusHandler:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/AddCreditCardFields;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddCreditCardFields;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAutoFocusHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected static focusNext(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 219
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 221
    .local v0, "nextView":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 224
    :cond_0
    return-void

    .line 219
    .end local v0    # "nextView":Landroid/view/View;
    :cond_1
    const/16 v1, 0x82

    invoke-virtual {p0, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected static focusPrevious(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 232
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 234
    .local v0, "previousView":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 237
    :cond_0
    return-void

    .line 232
    .end local v0    # "previousView":Landroid/view/View;
    :cond_1
    const/16 v1, 0x21

    invoke-virtual {p0, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public areAllFieldsVisible()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAllFieldsVisible:Z

    return v0
.end method

.method protected abstract createCreditCardImagesAnimator()Lcom/google/android/finsky/layout/CreditCardImagesAnimator;
.end method

.method public abstract expandFields()Z
.end method

.method protected onAllFieldsVisible()V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAllFieldsVisible:Z

    .line 244
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mOnAllFieldsVisibleListener:Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mOnAllFieldsVisibleListener:Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;

    invoke-interface {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;->onAllFieldsVisible()V

    .line 247
    :cond_0
    return-void
.end method

.method public onCreditCardTypeChanged(Lcom/google/android/finsky/billing/creditcard/CreditCardType;Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V
    .locals 1
    .param p1, "oldType"    # Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .param p2, "newType"    # Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .prologue
    .line 251
    iput-object p2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 252
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/layout/CreditCardImagesAnimator;->animateToType(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V

    .line 253
    return-void
.end method

.method protected abstract onCvcEntered()V
.end method

.method protected abstract onCvcFocused()V
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 77
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 78
    const v0, 0x7f0a00de

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .line 79
    const v0, 0x7f0a00e0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthField:Landroid/widget/EditText;

    .line 80
    const v0, 0x7f0a00e2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthYearSeparator:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0a00e1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mYearField:Landroid/widget/EditText;

    .line 82
    const v0, 0x7f0a00e3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCvcField:Landroid/widget/EditText;

    .line 83
    const v0, 0x7f0a00e6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/BillingAddress;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    .line 84
    const v0, 0x7f0a00e4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCvcImage:Landroid/widget/ImageView;

    .line 85
    const/4 v0, 0x5

    new-array v1, v0, [Landroid/widget/ImageView;

    const/4 v2, 0x0

    const v0, 0x7f0a00d8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    const v0, 0x7f0a00d9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    const v0, 0x7f0a00da

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    const/4 v2, 0x3

    const v0, 0x7f0a00db

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    const/4 v2, 0x4

    const v0, 0x7f0a00dc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCreditCardImages:[Landroid/widget/ImageView;

    .line 91
    const v0, 0x7f0a00e7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mPrivacyFooter:Landroid/widget/TextView;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->createCreditCardImagesAnimator()Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields$1;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setOnNumberEnteredListener(Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setOnCreditCardTypeChangedListener(Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthField:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthField:Landroid/widget/EditText;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields;Landroid/widget/TextView;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mYearField:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mYearField:Landroid/widget/EditText;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields;Landroid/widget/TextView;I)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCvcField:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$CvcTextWatcher;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/finsky/layout/AddCreditCardFields$CvcTextWatcher;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields;Lcom/google/android/finsky/layout/AddCreditCardFields$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCvcField:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields$2;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 121
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mIsLightTheme:Z

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->updateIconsFromTheme(Z)V

    .line 122
    return-void
.end method

.method protected abstract onNumberEntered()V
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 138
    instance-of v2, p1, Landroid/os/Bundle;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 139
    check-cast v0, Landroid/os/Bundle;

    .line 140
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_ALL_FIELDS_VISIBLE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAllFieldsVisible:Z

    .line 141
    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_CARD_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 142
    .local v1, "cardTypeInt":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 145
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mIsLightTheme:Z

    .line 146
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAllFieldsVisible:Z

    if-eqz v2, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onAllFieldsVisible()V

    .line 149
    :cond_0
    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-super {p0, v2}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 153
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "cardTypeInt":I
    :goto_1
    return-void

    .line 142
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "cardTypeInt":I
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->values()[Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v2

    aget-object v2, v2, v1

    goto :goto_0

    .line 152
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "cardTypeInt":I
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_ALL_FIELDS_VISIBLE:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mAllFieldsVisible:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFields;->KEY_CARD_TYPE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->ordinal()I

    move-result v1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const-string v1, "is_light_theme"

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mIsLightTheme:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    return-object v0

    .line 129
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setIsLightTheme(Z)V
    .locals 0
    .param p1, "isLightTheme"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mIsLightTheme:Z

    .line 167
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/AddCreditCardFields;->updateIconsFromTheme(Z)V

    .line 168
    return-void
.end method

.method public setOnAllFieldsVisibleListener(Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;)V
    .locals 0
    .param p1, "onAllFieldsVisibleListener"    # Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mOnAllFieldsVisibleListener:Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;

    .line 158
    return-void
.end method

.method protected abstract updateIconsFromTheme(Z)V
.end method
