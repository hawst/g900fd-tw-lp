.class public final Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateInstrumentResponse"
.end annotation


# instance fields
.field public createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

.field public hasInstrumentId:Z

.field public hasResult:Z

.field public hasUserMessageHtml:Z

.field public instrumentId:Ljava/lang/String;

.field public result:I

.field public userMessageHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 637
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 638
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 639
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 642
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    .line 643
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasResult:Z

    .line 644
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    .line 645
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasUserMessageHtml:Z

    .line 646
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    .line 647
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasInstrumentId:Z

    .line 648
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 649
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->cachedSize:I

    .line 650
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 673
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 674
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasResult:Z

    if-eqz v1, :cond_1

    .line 675
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasUserMessageHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 679
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasInstrumentId:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 683
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 686
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-eqz v1, :cond_6

    .line 687
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 698
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 699
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 703
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 704
    :sswitch_0
    return-object p0

    .line 709
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 710
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 715
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    .line 716
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasResult:Z

    goto :goto_0

    .line 722
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    .line 723
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasUserMessageHtml:Z

    goto :goto_0

    .line 727
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    .line 728
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasInstrumentId:Z

    goto :goto_0

    .line 732
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-nez v2, :cond_1

    .line 733
    new-instance v2, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 735
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 699
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 710
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 599
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 656
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasResult:Z

    if-eqz v0, :cond_1

    .line 657
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 659
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasUserMessageHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 660
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 662
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->hasInstrumentId:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 663
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 665
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-eqz v0, :cond_6

    .line 666
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 668
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 669
    return-void
.end method
