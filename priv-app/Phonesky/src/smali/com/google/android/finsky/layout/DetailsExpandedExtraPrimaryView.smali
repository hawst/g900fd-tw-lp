.class public Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;
.super Landroid/widget/RelativeLayout;
.source "DetailsExpandedExtraPrimaryView.java"


# instance fields
.field private mDescription:Landroid/widget/TextView;

.field private mThumbnail:Lcom/google/android/play/image/FifeImageView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 38
    const v0, 0x7f0a0197

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mThumbnail:Lcom/google/android/play/image/FifeImageView;

    .line 39
    const v0, 0x7f0a0198

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mTitle:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f0a0199

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mDescription:Landroid/widget/TextView;

    .line 41
    return-void
.end method

.method public populate(Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1, "extraPrimary"    # Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, 0x0

    .line 45
    iget-object v0, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mThumbnail:Lcom/google/android/play/image/FifeImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->description:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mDescription:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    :goto_1
    if-eqz p3, :cond_2

    .line 63
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    :goto_2
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mThumbnail:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mThumbnail:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v2, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->mDescription:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 65
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->setClickable(Z)V

    goto :goto_2
.end method
