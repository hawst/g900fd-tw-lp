.class final Lcom/google/android/finsky/utils/WishlistHelper$3;
.super Ljava/lang/Object;
.source "WishlistHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$view:Landroid/view/View;

.field final synthetic val$wasInWishlist:Z


# direct methods
.method constructor <init>(Landroid/content/Context;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$wasInWishlist:Z

    iput-object p3, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 187
    iget-object v1, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$context:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$wasInWishlist:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0c0352

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$3;->val$view:Landroid/view/View;

    invoke-static {v1, v0, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 190
    return-void

    .line 187
    :cond_0
    const v0, 0x7f0c0351

    goto :goto_0
.end method
