.class Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;
.super Ljava/lang/Object;
.source "RottenTomatoesReviewItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->bind(Lcom/google/android/finsky/protos/DocumentV2$Review;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;

.field final synthetic val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;Lcom/google/android/finsky/protos/DocumentV2$Review;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;->this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;

    iput-object p2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;->val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;->val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 63
    iget-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;->this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 64
    return-void
.end method
