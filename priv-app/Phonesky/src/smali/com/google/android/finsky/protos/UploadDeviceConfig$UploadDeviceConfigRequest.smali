.class public final Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UploadDeviceConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/UploadDeviceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadDeviceConfigRequest"
.end annotation


# instance fields
.field public deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

.field public gcmRegistrationId:Ljava/lang/String;

.field public hasGcmRegistrationId:Z

.field public hasManufacturer:Z

.field public manufacturer:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->clear()Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;

    .line 38
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    .line 43
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasManufacturer:Z

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    .line 45
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasGcmRegistrationId:Z

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 68
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-eqz v1, :cond_0

    .line 69
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasManufacturer:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 73
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasGcmRegistrationId:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 77
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 89
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 93
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 94
    :sswitch_0
    return-object p0

    .line 99
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-nez v1, :cond_1

    .line 100
    new-instance v1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DeviceConfigurationProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 106
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    .line 107
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasManufacturer:Z

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    .line 112
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasGcmRegistrationId:Z

    goto :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 56
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasManufacturer:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 57
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->manufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 59
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasGcmRegistrationId:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 60
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 62
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 63
    return-void
.end method
