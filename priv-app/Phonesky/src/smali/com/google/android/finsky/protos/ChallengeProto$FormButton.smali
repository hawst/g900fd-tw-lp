.class public final Lcom/google/android/finsky/protos/ChallengeProto$FormButton;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormButton"
.end annotation


# instance fields
.field public actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field public actionFailChallenge:Z

.field public actionUrl:Ljava/lang/String;

.field public hasActionFailChallenge:Z

.field public hasActionUrl:Z

.field public hasLabel:Z

.field public label:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2410
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2411
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->clear()Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 2412
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$FormButton;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2415
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    .line 2416
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasLabel:Z

    .line 2417
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    .line 2418
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionUrl:Z

    .line 2419
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    .line 2420
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionFailChallenge:Z

    .line 2421
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 2422
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->cachedSize:I

    .line 2423
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2451
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2452
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasLabel:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2453
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2456
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionUrl:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2457
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2460
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionFailChallenge:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    if-eqz v3, :cond_5

    .line 2461
    :cond_4
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 2464
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 2465
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 2466
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    aget-object v0, v3, v1

    .line 2467
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    if-eqz v0, :cond_6

    .line 2468
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2465
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2473
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .end local v1    # "i":I
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormButton;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 2481
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2482
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2486
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2487
    :sswitch_0
    return-object p0

    .line 2492
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    .line 2493
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasLabel:Z

    goto :goto_0

    .line 2497
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    .line 2498
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionUrl:Z

    goto :goto_0

    .line 2502
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    .line 2503
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionFailChallenge:Z

    goto :goto_0

    .line 2507
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2509
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v5, :cond_2

    move v1, v4

    .line 2510
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 2512
    .local v2, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    if-eqz v1, :cond_1

    .line 2513
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2515
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2516
    new-instance v5, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    aput-object v5, v2, v1

    .line 2517
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2518
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2515
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2509
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v1, v5

    goto :goto_1

    .line 2521
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    aput-object v5, v2, v1

    .line 2522
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2523
    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    goto :goto_0

    .line 2482
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2378
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2429
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasLabel:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2430
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2432
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionUrl:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2433
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2435
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->hasActionFailChallenge:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    if-eqz v2, :cond_5

    .line 2436
    :cond_4
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2438
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 2439
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 2440
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    aget-object v0, v2, v1

    .line 2441
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    if-eqz v0, :cond_6

    .line 2442
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2439
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2446
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2447
    return-void
.end method
