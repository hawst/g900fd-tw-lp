.class Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;
.super Ljava/lang/Object;
.source "RedeemScreenStep.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

.field final synthetic val$codeEntryOriginalTextColors:Landroid/content/res/ColorStateList;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    iput-object p2, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;->val$codeEntryOriginalTextColors:Landroid/content/res/ColorStateList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    # getter for: Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->mCodeEntry:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->access$000(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;->val$codeEntryOriginalTextColors:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep$1;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    # invokes: Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->syncContinueButtonState()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->access$100(Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;)V

    .line 130
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 121
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 117
    return-void
.end method
