.class public Lcom/google/android/finsky/layout/play/PlayRatingStar;
.super Landroid/widget/ImageView;
.source "PlayRatingStar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;
    }
.end annotation


# instance fields
.field private mFilledFocusedResourceId:I

.field private mFilledResourceId:I

.field private mFocusedResourceId:I

.field private mIndex:I

.field private mIsFilled:Z

.field private mIsFocused:Z

.field private mNormalResourceId:I

.field private mOnPressStateChangeListener:Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;

.field private mWasPressed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method private syncState()V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIsFilled:Z

    if-eqz v0, :cond_1

    .line 95
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIsFocused:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFilledFocusedResourceId:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setImageResource(I)V

    .line 99
    :goto_1
    return-void

    .line 95
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFilledResourceId:I

    goto :goto_0

    .line 97
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIsFocused:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFocusedResourceId:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setImageResource(I)V

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mNormalResourceId:I

    goto :goto_2
.end method


# virtual methods
.method public configure(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "backend"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIndex:I

    .line 51
    const v0, 0x7f02005e

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mNormalResourceId:I

    .line 52
    const v0, 0x7f02005f

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFocusedResourceId:I

    .line 53
    invoke-static {p2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRatingBarFilledResourceId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFilledResourceId:I

    .line 54
    invoke-static {p2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRatingBarFilledFocusedResourceId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mFilledFocusedResourceId:I

    .line 57
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->syncState()V

    .line 58
    return-void
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIndex:I

    return v0
.end method

.method public refreshDrawableState()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/ImageView;->refreshDrawableState()V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->isPressed()Z

    move-result v0

    .line 85
    .local v0, "isPressed":Z
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mWasPressed:Z

    if-eq v1, v0, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mOnPressStateChangeListener:Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mOnPressStateChangeListener:Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;

    invoke-interface {v1, p0, v0}, Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;->onPressStateChanged(Landroid/view/View;Z)V

    .line 89
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mWasPressed:Z

    .line 91
    :cond_1
    return-void
.end method

.method public setFilled(Z)V
    .locals 0
    .param p1, "isFilled"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIsFilled:Z

    .line 75
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->syncState()V

    .line 76
    return-void
.end method

.method public setFocused(Z)V
    .locals 0
    .param p1, "isFocused"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mIsFocused:Z

    .line 70
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->syncState()V

    .line 71
    return-void
.end method

.method public setOnPressStateChangeListener(Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingStar;->mOnPressStateChangeListener:Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;

    .line 66
    return-void
.end method
