.class public Lcom/google/android/finsky/layout/ReviewItemLayout;
.super Landroid/widget/RelativeLayout;
.source "ReviewItemLayout.java"


# instance fields
.field private mActionContainer:Landroid/view/View;

.field private mActionImage:Landroid/widget/ImageView;

.field private mActionText:Landroid/widget/TextView;

.field private mAuthor:Landroid/widget/TextView;

.field private mBody:Landroid/widget/TextView;

.field private mCanFeedback:Z

.field private mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

.field private mIsYourReview:Z

.field private mMetadata:Landroid/widget/TextView;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mProfilePicture:Lcom/google/android/play/image/FifeImageView;

.field private mTitle:Landroid/widget/TextView;

.field private mYourReviewLabel:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method private getReviewExtraInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;)Ljava/lang/String;
    .locals 12
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 170
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_1

    move-object v2, v9

    .line 193
    :cond_0
    :goto_0
    return-object v2

    .line 173
    :cond_1
    iget-object v6, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    .line 174
    .local v6, "version":Ljava/lang/String;
    iget-object v2, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    .line 175
    .local v2, "deviceName":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    move v4, v7

    .line 176
    .local v4, "hasVersion":Z
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    move v3, v7

    .line 178
    .local v3, "hasDeviceName":Z
    :goto_2
    if-nez v4, :cond_4

    .line 179
    if-nez v3, :cond_0

    move-object v2, v9

    goto :goto_0

    .end local v3    # "hasDeviceName":Z
    .end local v4    # "hasVersion":Z
    :cond_2
    move v4, v8

    .line 175
    goto :goto_1

    .restart local v4    # "hasVersion":Z
    :cond_3
    move v3, v8

    .line 176
    goto :goto_2

    .line 182
    .restart local v3    # "hasDeviceName":Z
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 184
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-boolean v10, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString:Z

    if-eqz v10, :cond_5

    iget-object v10, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    move v5, v7

    .line 187
    .local v5, "isCurrentVersion":Z
    :goto_3
    if-eqz v5, :cond_6

    .line 189
    if-nez v3, :cond_0

    move-object v2, v9

    goto :goto_0

    .end local v5    # "isCurrentVersion":Z
    :cond_5
    move v5, v8

    .line 184
    goto :goto_3

    .line 192
    .restart local v5    # "isCurrentVersion":Z
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 193
    .local v1, "context":Landroid/content/Context;
    if-eqz v3, :cond_7

    const v9, 0x7f0c0247

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-virtual {v1, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_4
    move-object v2, v7

    goto :goto_0

    :cond_7
    const v7, 0x7f0c0248

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_4
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 68
    const v0, 0x7f0a0356

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mAuthor:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a0358

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    .line 70
    const v0, 0x7f0a035f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0a0360

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0a035d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0a034e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    .line 74
    const v0, 0x7f0a0352

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionContainer:Landroid/view/View;

    .line 75
    const v0, 0x7f0a0353

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionImage:Landroid/widget/ImageView;

    .line 76
    const v0, 0x7f0a02de

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionText:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0a0357

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mYourReviewLabel:Landroid/widget/TextView;

    .line 78
    return-void
.end method

.method public setActionClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionContainer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void
.end method

.method public setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;ILcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 13
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p3, "reviewTextMaxLines"    # I
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "isYourReview"    # Z
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 83
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 86
    .local v7, "res":Landroid/content/res/Resources;
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mIsYourReview:Z

    .line 87
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mIsYourReview:Z

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mCanFeedback:Z

    .line 89
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mIsYourReview:Z

    if-eqz v8, :cond_2

    .line 91
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionImage:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getReviewEditDrawable(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionText:Landroid/widget/TextView;

    const v9, 0x7f0c03af

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionText:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mYourReviewLabel:Landroid/widget/TextView;

    const v9, 0x7f0c03b6

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget-object v10, v10, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mYourReviewLabel:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    :goto_1
    iget-object v8, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v1, v8, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 112
    .local v1, "authorName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 113
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mAuthor:Landroid/widget/TextView;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mAuthor:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    :goto_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    invoke-virtual {v8, p2}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->setReviewInfo(Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    .line 121
    iget-object v8, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 122
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    iget-object v9, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    :goto_3
    iget-object v8, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 129
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    iget-object v9, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 136
    :goto_4
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getReviewExtraInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;)Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "extraInfo":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 138
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :goto_5
    iget-object v5, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 145
    .local v5, "plusDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v5, :cond_8

    .line 146
    const/4 v8, 0x4

    invoke-static {v5, v8}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v3

    .line 147
    .local v3, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    iget-object v9, v3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v10, v3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 149
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 153
    .end local v3    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :goto_6
    if-eqz v5, :cond_9

    iget-boolean v8, v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsUrl:Z

    if-eqz v8, :cond_9

    .line 154
    new-instance v4, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v8, 0x117

    iget-object v9, v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {v4, v8, v9, v10, v11}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 156
    .local v4, "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    new-instance v9, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v9, v5}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v9, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const v9, 0x7f0c03b3

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v7, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setFocusable(Z)V

    .line 167
    .end local v4    # "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_7
    return-void

    .line 87
    .end local v1    # "authorName":Ljava/lang/String;
    .end local v2    # "extraInfo":Ljava/lang/String;
    .end local v5    # "plusDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_0
    iget-object v8, p2, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x1

    goto/16 :goto_0

    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 102
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionImage:Landroid/widget/ImageView;

    const v9, 0x7f020134

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 103
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mCanFeedback:Z

    if-eqz v8, :cond_3

    const/4 v6, 0x0

    .line 104
    .local v6, "ratingVisibility":I
    :goto_8
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionContainer:Landroid/view/View;

    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionText:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mYourReviewLabel:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mActionContainer:Landroid/view/View;

    const v9, 0x7f0c0272

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 103
    .end local v6    # "ratingVisibility":I
    :cond_3
    const/16 v6, 0x8

    goto :goto_8

    .line 116
    .restart local v1    # "authorName":Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mAuthor:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 125
    :cond_5
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 133
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 141
    .restart local v2    # "extraInfo":Ljava/lang/String;
    :cond_7
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 151
    .restart local v5    # "plusDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_8
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 163
    :cond_9
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/play/image/FifeImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/image/FifeImageView;->setFocusable(Z)V

    goto/16 :goto_7
.end method
