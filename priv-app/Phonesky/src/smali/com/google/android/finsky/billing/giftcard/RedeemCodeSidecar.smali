.class public Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "RedeemCodeSidecar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$1;,
        Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;
    }
.end annotation


# instance fields
.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mErrorHtml:Ljava/lang/String;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

.field private final mRedemptionListener:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;

.field private mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

.field private mVolleyError:Lcom/android/volley/VolleyError;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    .line 98
    new-instance v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;-><init>(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRedemptionListener:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;

    .line 101
    new-instance v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    .line 335
    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II[B)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # [B

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->logBackgroundEvent(II[B)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    return-void
.end method

.method private logBackgroundEvent(II[B)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "resultCode"    # I
    .param p3, "serverLogsCookie"    # [B

    .prologue
    .line 326
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLogsCookie([B)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 329
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 330
    invoke-virtual {v0, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 333
    return-void
.end method

.method public static newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;)Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "redemptionContext"    # I
    .param p2, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p3, "offerType"    # I
    .param p4, "partnerPayload"    # Ljava/lang/String;

    .prologue
    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "RedeemCodeSidecar.redemption_context"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    const-string v2, "RedeemCodeSidecar.docid"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 121
    const-string v2, "RedeemCodeSidecar.offer_type"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    const-string v2, "RedeemCodeSidecar.partner_payload"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;-><init>()V

    .line 124
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 125
    return-object v1
.end method

.method private sendRedemptionRequest()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 219
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x320

    invoke-virtual {v2, v3, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 222
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-boolean v2, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasToken:Z

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    .line 224
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v8, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    .line 231
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    .line 232
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v7, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    .line 233
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 236
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v0, v2, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 237
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 238
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v1, :cond_0

    .line 239
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iget v3, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    int-to-long v4, v3

    iput-wide v4, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    .line 240
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v8, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    .line 245
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_0
    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 246
    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    .line 247
    iput-object v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;

    .line 248
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRedemptionListener:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRedemptionListener:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/finsky/api/DfeApi;->redeemCode(Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 249
    invoke-virtual {p0, v8, v7}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V

    .line 250
    return-void

    .line 226
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    const-string v3, ""

    iput-object v3, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    .line 227
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v7, v2, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    goto :goto_0
.end method


# virtual methods
.method public getAddressChallenge()Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .locals 4

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 269
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    const/4 v0, 0x0

    .line 272
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    goto :goto_0
.end method

.method public getConsumptionAppDocid()Lcom/google/android/finsky/protos/Common$Docid;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    goto :goto_0
.end method

.method public getErrorHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getInstrumentManagerTokens()Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    .locals 4

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 277
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    const/4 v0, 0x0

    .line 280
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    goto :goto_0
.end method

.method public getLastRedeemCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRedeemedItemDoc()Lcom/google/android/finsky/api/model/Document;
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    goto :goto_0
.end method

.method public getRedeemedOfferHtml()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 292
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    .line 293
    const-string v1, "Invalid state: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    iget-object v0, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRedemptionSuccess()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    .locals 4

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 285
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    const/4 v0, 0x0

    .line 288
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    goto :goto_0
.end method

.method public getStoredValueInstrumentId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 302
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    goto :goto_0
.end method

.method public getUserConfirmationChallenge()Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;
    .locals 4

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 261
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    const/4 v0, 0x0

    .line 264
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    goto :goto_0
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 130
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 132
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 133
    if-eqz p1, :cond_1

    .line 134
    const-string v3, "RedeemCodeSidecar.request"

    invoke-static {p1, v3}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    .line 135
    const-string v3, "RedeemCodeSidecar.last_redeem_code_response"

    invoke-static {p1, v3}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    iput-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 137
    const-string v3, "RedeemCodeSidecar.error_html"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;

    .line 160
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerUtil;->createClientToken(Landroid/content/Context;)[B

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    .line 162
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v6, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPaymentsIntegratorClientContextToken:Z

    .line 164
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    return-void

    .line 139
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RedeemCodeSidecar.redemption_context"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    .line 141
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v6, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasRedemptionContext:Z

    .line 143
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "RedeemCodeSidecar.docid"

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Docid;

    iput-object v3, v4, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "RedeemCodeSidecar.offer_type"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 146
    .local v1, "offerType":I
    if-eqz v1, :cond_2

    .line 147
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput v1, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    .line 148
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v6, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasOfferType:Z

    .line 151
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "RedeemCodeSidecar.partner_payload"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 152
    .local v2, "partnerPayload":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 153
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-object v2, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    .line 154
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v6, v3, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPartnerPayload:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 170
    const-string v0, "RedeemCodeSidecar.request"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 171
    const-string v0, "RedeemCodeSidecar.last_redeem_code_response"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 173
    const-string v0, "RedeemCodeSidecar.error_html"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public redeem(Ljava/lang/String;)V
    .locals 2
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-object p1, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasCode:Z

    .line 187
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->sendRedemptionRequest()V

    .line 188
    return-void
.end method

.method public respondInstrumentManagerSucceeded()V
    .locals 4

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 211
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->sendRedemptionRequest()V

    goto :goto_0
.end method

.method public respondWithAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;[Ljava/lang/String;)V
    .locals 4
    .param p1, "address"    # Lcom/google/android/finsky/protos/BillingAddress$Address;
    .param p2, "checkedCheckboxIds"    # [Ljava/lang/String;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 201
    const-string v0, "Invalid state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-object p1, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-object p2, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    .line 206
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->sendRedemptionRequest()V

    goto :goto_0
.end method

.method public respondWithConfirmation()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 191
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 192
    const-string v0, "Invalid state: %d"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mRequest:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    iput-boolean v2, v0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    .line 196
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->sendRedemptionRequest()V

    goto :goto_0
.end method
