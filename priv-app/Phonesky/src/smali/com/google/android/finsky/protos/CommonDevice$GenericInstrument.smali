.class public final Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GenericInstrument"
.end annotation


# instance fields
.field public createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

.field public createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

.field public createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

.field public iconImage:Lcom/google/android/finsky/protos/Common$Image;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3229
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3230
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->clear()Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 3231
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3234
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 3235
    invoke-static {}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;->emptyArray()[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    .line 3236
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 3237
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 3238
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->cachedSize:I

    .line 3239
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 3267
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3268
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3269
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 3270
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    aget-object v0, v3, v1

    .line 3271
    .local v0, "element":Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    if-eqz v0, :cond_0

    .line 3272
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3269
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3277
    .end local v0    # "element":Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v3, :cond_2

    .line 3278
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3281
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-eqz v3, :cond_3

    .line 3282
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3285
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_4

    .line 3286
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3289
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3297
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3298
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3302
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3303
    :sswitch_0
    return-object p0

    .line 3308
    :sswitch_1
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3310
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    if-nez v5, :cond_2

    move v1, v4

    .line 3311
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    .line 3313
    .local v2, "newArray":[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    if-eqz v1, :cond_1

    .line 3314
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3316
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3317
    new-instance v5, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;-><init>()V

    aput-object v5, v2, v1

    .line 3318
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3319
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3316
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3310
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    array-length v1, v5

    goto :goto_1

    .line 3322
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;-><init>()V

    aput-object v5, v2, v1

    .line 3323
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3324
    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    goto :goto_0

    .line 3328
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-nez v5, :cond_4

    .line 3329
    new-instance v5, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 3331
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3335
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-nez v5, :cond_5

    .line 3336
    new-instance v5, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 3338
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3342
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_6

    .line 3343
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 3345
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3298
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3200
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3245
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3246
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 3247
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowOption:[Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;

    aget-object v0, v2, v1

    .line 3248
    .local v0, "element":Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    if-eqz v0, :cond_0

    .line 3249
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3246
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3253
    .end local v0    # "element":Lcom/google/android/finsky/protos/CreateInstrument$CreateInstrumentFlowOption;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v2, :cond_2

    .line 3254
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3256
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-eqz v2, :cond_3

    .line 3257
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3259
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_4

    .line 3260
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3262
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3263
    return-void
.end method
