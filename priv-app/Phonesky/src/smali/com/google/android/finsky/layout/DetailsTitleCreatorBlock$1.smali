.class Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;
.super Ljava/lang/Object;
.source "DetailsTitleCreatorBlock.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;

.field final synthetic val$creatorDoc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->this$0:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p5, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$creatorDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x7e

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;->val$creatorDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;)V

    .line 149
    return-void
.end method
