.class public Lcom/google/android/finsky/utils/OrderHistoryHelper;
.super Ljava/lang/Object;
.source "OrderHistoryHelper.java"


# static fields
.field private static sLastMutationTimeMs:J


# direct methods
.method public static hasMutationOccurredSince(J)Z
    .locals 2
    .param p0, "timestampMs"    # J

    .prologue
    .line 13
    sget-wide v0, Lcom/google/android/finsky/utils/OrderHistoryHelper;->sLastMutationTimeMs:J

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onMutationOccurred()V
    .locals 2

    .prologue
    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/utils/OrderHistoryHelper;->sLastMutationTimeMs:J

    .line 18
    return-void
.end method
