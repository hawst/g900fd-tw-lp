.class public final Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$RequestProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Request"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;


# instance fields
.field public ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

.field public billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

.field public checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

.field public checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

.field public contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

.field public inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

.field public inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

.field public modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

.field public purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

.field public restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5022
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5023
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->clear()Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    .line 5024
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;
    .locals 2

    .prologue
    .line 4981
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    if-nez v0, :cond_1

    .line 4982
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 4984
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    if-nez v0, :cond_0

    .line 4985
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    .line 4987
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4989
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    return-object v0

    .line 4987
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5027
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    .line 5028
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    .line 5029
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    .line 5030
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    .line 5031
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    .line 5032
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    .line 5033
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    .line 5034
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    .line 5035
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    .line 5036
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    .line 5037
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->cachedSize:I

    .line 5038
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5079
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5080
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    if-eqz v1, :cond_0

    .line 5081
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5084
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    if-eqz v1, :cond_1

    .line 5085
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5088
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    if-eqz v1, :cond_2

    .line 5089
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5092
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    if-eqz v1, :cond_3

    .line 5093
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5096
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    if-eqz v1, :cond_4

    .line 5097
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5100
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    if-eqz v1, :cond_5

    .line 5101
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5104
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    if-eqz v1, :cond_6

    .line 5105
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5108
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    if-eqz v1, :cond_7

    .line 5109
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5112
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    if-eqz v1, :cond_8

    .line 5113
    const/16 v1, 0x21

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5116
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    if-eqz v1, :cond_9

    .line 5117
    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5120
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5128
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5129
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5133
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5134
    :sswitch_0
    return-object p0

    .line 5139
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    if-nez v1, :cond_1

    .line 5140
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    .line 5142
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5146
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    if-nez v1, :cond_2

    .line 5147
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    .line 5149
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5153
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    if-nez v1, :cond_3

    .line 5154
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    .line 5156
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5160
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    if-nez v1, :cond_4

    .line 5161
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    .line 5163
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5167
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    if-nez v1, :cond_5

    .line 5168
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    .line 5170
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5174
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    if-nez v1, :cond_6

    .line 5175
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    .line 5177
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5181
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    if-nez v1, :cond_7

    .line 5182
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    .line 5184
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5188
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    if-nez v1, :cond_8

    .line 5189
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    .line 5191
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5195
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    if-nez v1, :cond_9

    .line 5196
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    .line 5198
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5202
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    if-nez v1, :cond_a

    .line 5203
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    .line 5205
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5129
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x32 -> :sswitch_1
        0x4a -> :sswitch_2
        0x6a -> :sswitch_3
        0x92 -> :sswitch_4
        0xc2 -> :sswitch_5
        0xd2 -> :sswitch_6
        0xfa -> :sswitch_7
        0x102 -> :sswitch_8
        0x10a -> :sswitch_9
        0x112 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4975
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5044
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    if-eqz v0, :cond_0

    .line 5045
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->modifyCommentRequest:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5047
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    if-eqz v0, :cond_1

    .line 5048
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->contentSyncRequest:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5050
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    if-eqz v0, :cond_2

    .line 5051
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->purchaseMetadataRequest:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5053
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    if-eqz v0, :cond_3

    .line 5054
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkLicenseRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5056
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    if-eqz v0, :cond_4

    .line 5057
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->restoreApplicationsRequest:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5059
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    if-eqz v0, :cond_5

    .line 5060
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->billingEventRequest:Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5062
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    if-eqz v0, :cond_6

    .line 5063
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppRestoreTransactionsRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5065
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    if-eqz v0, :cond_7

    .line 5066
    const/16 v0, 0x20

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->inAppPurchaseInformationRequest:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5068
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    if-eqz v0, :cond_8

    .line 5069
    const/16 v0, 0x21

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->checkForNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5071
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    if-eqz v0, :cond_9

    .line 5072
    const/16 v0, 0x22

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;->ackNotificationsRequest:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5074
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5075
    return-void
.end method
