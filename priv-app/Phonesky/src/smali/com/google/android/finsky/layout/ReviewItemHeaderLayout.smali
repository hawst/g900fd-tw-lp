.class public Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;
.super Lcom/google/android/finsky/layout/play/SingleLineContainer;
.source "ReviewItemHeaderLayout.java"


# instance fields
.field private mDate:Landroid/widget/TextView;

.field private mEdited:Landroid/widget/TextView;

.field private mRating:Lcom/google/android/play/layout/StarRatingBar;

.field private mSource:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/SingleLineContainer;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/SingleLineContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->onFinishInflate()V

    .line 48
    const v0, 0x7f0a035a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mRating:Lcom/google/android/play/layout/StarRatingBar;

    .line 49
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0a035b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mDate:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0a035c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mEdited:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method public setReviewInfo(Lcom/google/android/finsky/protos/DocumentV2$Review;)V
    .locals 12
    .param p1, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 55
    iget-object v2, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    .line 56
    .local v2, "reviewSource":Ljava/lang/String;
    iget-object v3, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    .line 61
    .local v3, "sourceUrl":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 62
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 65
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :goto_0
    iget-boolean v6, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    if-eqz v6, :cond_3

    .line 80
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mRating:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v6, v10}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 81
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mRating:Lcom/google/android/play/layout/StarRatingBar;

    iget v7, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 86
    :goto_1
    iget-boolean v6, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    if-eqz v6, :cond_4

    .line 87
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mDate:Landroid/widget/TextView;

    iget-wide v8, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mEdited:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    iget-boolean v6, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    if-eqz v6, :cond_0

    .line 96
    iget-wide v4, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    .line 97
    .local v4, "reviewTimestamp":J
    iget-wide v0, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    .line 98
    .local v0, "developerReplyTimestamp":J
    cmp-long v6, v4, v0

    if-lez v6, :cond_0

    .line 99
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mEdited:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    .end local v0    # "developerReplyTimestamp":J
    .end local v4    # "reviewTimestamp":J
    :cond_0
    return-void

    .line 67
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    new-instance v7, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout$1;

    invoke-direct {v7, p0, v3}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout$1;-><init>(Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 76
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mSource:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 83
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mRating:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v6, v11}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_1

    .line 90
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
