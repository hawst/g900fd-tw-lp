.class public Lcom/google/android/finsky/layout/HorizontalStrip;
.super Lcom/google/android/finsky/layout/AbsHorizontalStrip;
.source "HorizontalStrip.java"


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

.field private final mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

.field private mEdgeFadeColor:I

.field protected final mScreenScaleFactor:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/HorizontalStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/layout/AbsHorizontalStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image$Dimension;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mScreenScaleFactor:F

    .line 46
    const v1, 0x7f0900ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mEdgeFadeColor:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/HorizontalStrip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/HorizontalStrip;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->syncChildViews()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/HorizontalStrip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/HorizontalStrip;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->recreateChildViews()V

    return-void
.end method

.method private getChildHeight(I)I
    .locals 3
    .param p1, "childIndex"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v2, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mScreenScaleFactor:F

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getImageDimensionAt(ILcom/google/android/finsky/protos/Common$Image$Dimension;F)V

    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    return v0
.end method

.method private getChildWidth(I)I
    .locals 3
    .param p1, "childIndex"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v2, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mScreenScaleFactor:F

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getImageDimensionAt(ILcom/google/android/finsky/protos/Common$Image$Dimension;F)V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mDimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    return v0
.end method

.method private getTotalChildWidth(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 182
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 185
    .local v0, "width":I
    if-nez p1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mLeadingMargin:I

    :goto_0
    add-int/2addr v0, v1

    .line 186
    return v0

    .line 185
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mGapMargin:I

    goto :goto_0
.end method

.method private recreateChildViews()V
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->removeAllViews()V

    .line 70
    iget-object v3, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    if-nez v3, :cond_0

    .line 89
    :goto_0
    return-void

    .line 74
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    invoke-virtual {v3}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 75
    iget-object v3, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    iget-object v4, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, p0, v2}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getViewAt(Landroid/content/Context;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    .line 76
    .local v1, "childView":Landroid/view/View;
    move v0, v2

    .line 77
    .local v0, "childIndex":I
    new-instance v3, Lcom/google/android/finsky/layout/HorizontalStrip$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/layout/HorizontalStrip$2;-><init>(Lcom/google/android/finsky/layout/HorizontalStrip;I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/HorizontalStrip;->addView(Landroid/view/View;)V

    .line 74
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 88
    .end local v0    # "childIndex":I
    .end local v1    # "childView":Landroid/view/View;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->syncChildViews()V

    goto :goto_0
.end method

.method private syncChildViews()V
    .locals 5

    .prologue
    .line 92
    const/4 v2, 0x1

    .line 93
    .local v2, "hasAllImages":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    invoke-virtual {v4}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 94
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 95
    .local v0, "child":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getImageAt(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 96
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v4, v0, Lcom/google/android/finsky/layout/AppScreenshot;

    if-eqz v4, :cond_0

    .line 97
    if-eqz v1, :cond_1

    .line 98
    check-cast v0, Lcom/google/android/finsky/layout/AppScreenshot;

    .end local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/AppScreenshot;->setScreenshotDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 100
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 104
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    if-eqz v2, :cond_3

    .line 105
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->requestLayout()V

    .line 107
    :cond_3
    return-void
.end method


# virtual methods
.method protected getLeftEdgeOfChild(I)F
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 191
    const/4 v2, 0x0

    .line 192
    .local v2, "result":I
    const/4 v0, 0x0

    .line 193
    .local v0, "currLeft":I
    const/4 v1, 0x0

    .line 194
    .local v1, "i":I
    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 195
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/HorizontalStrip;->getTotalChildWidth(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 196
    move v2, v0

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    :cond_0
    if-eqz v1, :cond_1

    .line 201
    iget v3, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mGapMargin:I

    add-int/2addr v2, v3

    .line 203
    :cond_1
    int-to-float v3, v2

    return v3
.end method

.method protected getLeftEdgeOfChildOnLeft(F)F
    .locals 4
    .param p1, "xOffset"    # F

    .prologue
    .line 165
    const/4 v2, 0x0

    .line 166
    .local v2, "result":I
    const/4 v0, 0x0

    .line 167
    .local v0, "currLeft":I
    const/4 v1, 0x0

    .line 168
    .local v1, "i":I
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 169
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/HorizontalStrip;->getTotalChildWidth(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 170
    int-to-float v3, v0

    cmpl-float v3, v3, p1

    if-lez v3, :cond_1

    .line 175
    :cond_0
    int-to-float v3, v2

    return v3

    .line 173
    :cond_1
    move v2, v0

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getLeftEdgeOfChildOnRight(F)F
    .locals 4
    .param p1, "xOffset"    # F

    .prologue
    .line 208
    const/4 v2, 0x0

    .line 209
    .local v2, "result":I
    const/4 v0, 0x0

    .line 210
    .local v0, "currLeft":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 211
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/HorizontalStrip;->getTotalChildWidth(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 212
    move v2, v0

    .line 213
    int-to-float v3, v0

    cmpl-float v3, v3, p1

    if-lez v3, :cond_1

    .line 218
    :cond_0
    iget v3, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mGapMargin:I

    add-int/2addr v2, v3

    .line 219
    int-to-float v3, v2

    return v3

    .line 210
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 229
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getScrollPosition()F

    move-result v2

    .line 230
    .local v2, "scrollPosition":F
    cmpl-float v4, v2, v3

    if-ltz v4, :cond_0

    .line 238
    :goto_0
    return v3

    .line 233
    :cond_0
    neg-float v1, v2

    .line 234
    .local v1, "pixelsBeyondLeft":F
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 235
    .local v0, "fadeLength":I
    int-to-float v3, v0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_1

    .line 236
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 238
    :cond_1
    int-to-float v3, v0

    div-float v3, v1, v3

    goto :goto_0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getScrollPosition()F

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float v1, v3, v4

    .line 249
    .local v1, "pixelsBeyondRight":F
    cmpg-float v3, v1, v2

    if-gtz v3, :cond_0

    .line 256
    :goto_0
    return v2

    .line 252
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 253
    .local v0, "fadeLength":I
    int-to-float v2, v0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 254
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    .line 256
    :cond_1
    int-to-float v2, v0

    div-float v2, v1, v2

    goto :goto_0
.end method

.method public getSolidColor()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mEdgeFadeColor:I

    return v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HorizontalStrip;->setAdapter(Lcom/google/android/finsky/adapters/ImageStripAdapter;)V

    .line 266
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 143
    iget-object v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    if-nez v5, :cond_0

    .line 161
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getHeight()I

    move-result v2

    .line 148
    .local v2, "height":I
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    .line 149
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getPaddingLeft()I

    move-result v5

    iget v6, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mLeadingMargin:I

    add-int v4, v5, v6

    .line 150
    .local v4, "x":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 151
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 152
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 153
    .local v1, "childWidth":I
    const/4 v5, 0x0

    add-int v6, v4, v1

    invoke-virtual {v0, v4, v5, v6, v2}, Landroid/view/View;->layout(IIII)V

    .line 154
    iget v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mGapMargin:I

    add-int/2addr v5, v1

    add-int/2addr v4, v5

    .line 155
    iget v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    int-to-float v6, v1

    add-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    .line 150
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 158
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childWidth":I
    :cond_1
    iget v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    iget v6, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mGapMargin:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    mul-int/2addr v6, v7

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    .line 160
    iget v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    iget v6, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mLeadingMargin:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mTotalChildrenWidth:F

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 111
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 113
    .local v5, "stripHeight":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildCount()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 114
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildWidth(I)I

    move-result v2

    .line 116
    .local v2, "childWidth":I
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/HorizontalStrip;->getChildHeight(I)I

    move-result v1

    .line 117
    .local v1, "childHeight":I
    if-eqz v1, :cond_0

    .line 118
    int-to-float v6, v5

    int-to-float v7, v1

    div-float v4, v6, v7

    .line 119
    .local v4, "scalingFactor":F
    float-to-double v6, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_0

    .line 122
    int-to-float v6, v2

    mul-float/2addr v6, v4

    float-to-int v2, v6

    .line 125
    .end local v4    # "scalingFactor":F
    :cond_0
    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v6, p2}, Landroid/view/View;->measure(II)V

    .line 113
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 128
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childHeight":I
    .end local v2    # "childWidth":I
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual {p0, v6, v5}, Lcom/google/android/finsky/layout/HorizontalStrip;->setMeasuredDimension(II)V

    .line 129
    return-void
.end method

.method public setAdapter(Lcom/google/android/finsky/adapters/ImageStripAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/finsky/adapters/ImageStripAdapter;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/HorizontalStrip;->mAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;

    new-instance v1, Lcom/google/android/finsky/layout/HorizontalStrip$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/HorizontalStrip$1;-><init>(Lcom/google/android/finsky/layout/HorizontalStrip;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 64
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/HorizontalStrip;->recreateChildViews()V

    .line 65
    return-void
.end method
