.class Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;
.super Ljava/lang/Object;
.source "AddCreditCardFields.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/AddCreditCardFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoAdvancer"
.end annotation


# instance fields
.field private mMaxLength:I

.field private final mTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/layout/AddCreditCardFields;Landroid/widget/TextView;I)V
    .locals 0
    .param p2, "textView"    # Landroid/widget/TextView;
    .param p3, "maxLength"    # I

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    iput-object p2, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    .line 303
    iput p3, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mMaxLength:I

    .line 304
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const-wide/16 v2, 0x2ee

    .line 314
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mMaxLength:I

    if-lt v0, v1, :cond_2

    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    # getter for: Lcom/google/android/finsky/layout/AddCreditCardFields;->mAutoFocusHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->access$000(Lcom/google/android/finsky/layout/AddCreditCardFields;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer$1;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->focusNext(Landroid/view/View;)V

    goto :goto_0

    .line 325
    :cond_2
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    # getter for: Lcom/google/android/finsky/layout/AddCreditCardFields;->mAutoFocusHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->access$000(Lcom/google/android/finsky/layout/AddCreditCardFields;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer$2;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$AutoAdvancer;->mTextView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->focusPrevious(Landroid/view/View;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "i2"    # I

    .prologue
    .line 307
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "i2"    # I

    .prologue
    .line 310
    return-void
.end method
