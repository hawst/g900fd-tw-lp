.class public Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;
.super Landroid/widget/ImageView;
.source "DetailsSummaryWishlistView.java"

# interfaces
.implements Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;


# instance fields
.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mIsConfigured:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private syncVisuals(ZI)V
    .locals 2
    .param p1, "isInWishlist"    # Z
    .param p2, "backendId"    # I

    .prologue
    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-static {p2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWishlistOnDrawable(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setImageResource(I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0257

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 81
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {p2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWishlistOffDrawable(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setImageResource(I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0256

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 5
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    .line 39
    .local v1, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {p1, v1}, Lcom/google/android/finsky/utils/WishlistHelper;->shouldHideWishlistAction(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)Z

    move-result v2

    .line 41
    .local v2, "shouldHideWishlistButton":Z
    if-eqz v2, :cond_0

    .line 42
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setVisibility(I)V

    .line 69
    :goto_0
    return-void

    .line 46
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 48
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setVisibility(I)V

    .line 49
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 50
    .local v0, "currAccount":Landroid/accounts/Account;
    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->syncVisuals(ZI)V

    .line 52
    new-instance v3, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;

    invoke-direct {v3, p0, p1, v0, p2}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView$1;-><init>(Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->mIsConfigured:Z

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 94
    invoke-static {p0}, Lcom/google/android/finsky/utils/WishlistHelper;->addWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 95
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 99
    invoke-static {p0}, Lcom/google/android/finsky/utils/WishlistHelper;->removeWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 100
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 101
    return-void
.end method

.method public onWishlistStatusChanged(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "isInWishlist"    # Z
    .param p3, "isCommitted"    # Z

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->mIsConfigured:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->syncVisuals(ZI)V

    .line 89
    :cond_0
    return-void
.end method
