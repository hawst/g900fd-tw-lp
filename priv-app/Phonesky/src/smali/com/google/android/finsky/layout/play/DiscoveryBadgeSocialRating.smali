.class public Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;
.super Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.source "DiscoveryBadgeSocialRating.java"


# instance fields
.field private mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private mRatingBarContainer:Landroid/view/View;

.field private mRatingBarContainerBackground:Landroid/graphics/drawable/GradientDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainerBackground:Landroid/graphics/drawable/GradientDrawable;

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->setWillNotDraw(Z)V

    .line 46
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "badge"    # Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 66
    invoke-super/range {p0 .. p7}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    iget v1, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->userStarRating:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainerBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 70
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainerBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainerBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 82
    const/16 v0, 0x70b

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onFinishInflate()V

    .line 51
    const v0, 0x7f0a01af

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 52
    const v0, 0x7f0a01ae

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;->mRatingBarContainer:Landroid/view/View;

    .line 53
    return-void
.end method
