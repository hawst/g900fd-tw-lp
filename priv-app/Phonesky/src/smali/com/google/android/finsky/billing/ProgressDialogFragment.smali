.class public Lcom/google/android/finsky/billing/ProgressDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ProgressDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 3
    .param p0, "messageId"    # I

    .prologue
    .line 24
    new-instance v1, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;-><init>()V

    .line 25
    .local v1, "progressDialogFragment":Lcom/google/android/finsky/billing/ProgressDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 26
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "message_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 27
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 28
    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 3
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 35
    new-instance v1, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;-><init>()V

    .line 36
    .local v1, "progressDialogFragment":Lcom/google/android/finsky/billing/ProgressDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    .local v0, "args":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 38
    const-string v2, "message"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 46
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->setCancelable(Z)V

    .line 47
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 48
    .local v0, "dialog":Landroid/app/ProgressDialog;
    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "message_id"

    const v4, 0x7f0c01d6

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 50
    .local v1, "messageId":I
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 55
    :cond_0
    :goto_0
    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 56
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 57
    return-object v0

    .line 52
    :cond_1
    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
