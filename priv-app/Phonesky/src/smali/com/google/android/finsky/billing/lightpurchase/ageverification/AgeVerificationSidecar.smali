.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "AgeVerificationSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/SidecarFragment;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;",
        ">;",
        "Lcom/google/android/finsky/api/model/OnDataChangedListener;"
    }
.end annotation


# instance fields
.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mErrorHtml:Ljava/lang/String;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    return-void
.end method

.method private logBackgroundEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "resultCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v2, 0x206

    invoke-direct {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 206
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 207
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 210
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;-><init>()V

    .line 78
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 81
    return-object v1
.end method


# virtual methods
.method public checkDocumentMaturity(Ljava/lang/String;)V
    .locals 4
    .param p1, "docidStr"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 98
    if-nez p1, :cond_0

    .line 100
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {p1}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 106
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0
.end method

.method public getAgeChallenge()Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    return-object v0
.end method

.method public getChallengeError()Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;
    .locals 3

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSubstate()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 182
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with substate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSubstate()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    return-object v0
.end method

.method public getErrorHtml()Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSubstate()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 190
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with substate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSubstate()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mErrorHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsCodeChallenge()Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 175
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 88
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 89
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    return-void
.end method

.method public onDataChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 217
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_0

    .line 218
    const v1, 0x7f0c00e5

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mErrorHtml:Ljava/lang/String;

    .line 219
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 226
    :goto_0
    return-void

    .line 220
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0

    .line 224
    :cond_1
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 260
    const-string v0, "Volley error received: %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->logBackgroundEvent(ILjava/lang/String;)V

    .line 262
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mErrorHtml:Ljava/lang/String;

    .line 263
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 264
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 232
    iget-object v0, p1, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 233
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v0, :cond_1

    .line 234
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;->challengePassed:Z

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->logBackgroundEvent(ILjava/lang/String;)V

    .line 236
    invoke-virtual {p0, v3, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 254
    :goto_0
    return-void

    .line 239
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received no challenge."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    if-eqz v0, :cond_2

    .line 243
    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->logBackgroundEvent(ILjava/lang/String;)V

    .line 244
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    if-eqz v0, :cond_3

    .line 246
    invoke-direct {p0, v4, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->logBackgroundEvent(ILjava/lang/String;)V

    .line 247
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0

    .line 248
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    if-eqz v0, :cond_4

    .line 249
    const/4 v0, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->logBackgroundEvent(ILjava/lang/String;)V

    .line 250
    const/4 v0, 0x1

    invoke-virtual {p0, v4, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0

    .line 252
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received unknown challenge."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->onResponse(Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;)V

    return-void
.end method

.method public requestAgeVerificationForm()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->requestAgeVerificationForm(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 114
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 115
    return-void
.end method

.method public resendSmsCode(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, p1, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->resendAgeVerificationCode(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 136
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 137
    return-void
.end method

.method public startChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 2
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    const/4 v1, 0x0

    .line 156
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 164
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mLastChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    if-eqz v0, :cond_1

    .line 160
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    goto :goto_0

    .line 162
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received unknown challenge."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public verifyAge(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p2, "postParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, p1, p2, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->verifyAge(Ljava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 125
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 126
    return-void
.end method

.method public verifySmsCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "codeParamKey"    # Ljava/lang/String;
    .param p3, "code"    # Ljava/lang/String;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p0

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->verifyAgeVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 149
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setState(II)V

    .line 150
    return-void
.end method
