.class public final Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AndroidAppDelivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/AndroidAppDelivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppFileMetadata"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;


# instance fields
.field public downloadUrl:Ljava/lang/String;

.field public fileType:I

.field public hasDownloadUrl:Z

.field public hasFileType:Z

.field public hasSize:Z

.field public hasVersionCode:Z

.field public size:J

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    .line 43
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    sput-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    iput v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    .line 47
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasFileType:Z

    .line 48
    iput v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    .line 49
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasVersionCode:Z

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    .line 51
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasSize:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    .line 53
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasDownloadUrl:Z

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 79
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasFileType:Z

    if-eqz v1, :cond_1

    .line 80
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasVersionCode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    if-eqz v1, :cond_3

    .line 84
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasSize:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 88
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasDownloadUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 92
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 103
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 104
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 108
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    :sswitch_0
    return-object p0

    .line 114
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 115
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 118
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    .line 119
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasFileType:Z

    goto :goto_0

    .line 125
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    .line 126
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasVersionCode:Z

    goto :goto_0

    .line 130
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    .line 131
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasSize:Z

    goto :goto_0

    .line 135
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    .line 136
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasDownloadUrl:Z

    goto :goto_0

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasFileType:Z

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->fileType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 64
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasVersionCode:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    if-eqz v0, :cond_3

    .line 65
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 67
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasSize:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 68
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->size:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 70
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->hasDownloadUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 71
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 73
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 74
    return-void
.end method
