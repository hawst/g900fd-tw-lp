.class public Lcom/google/android/finsky/api/DfeApiImpl;
.super Ljava/lang/Object;
.source "DfeApiImpl.java"

# interfaces
.implements Lcom/google/android/finsky/api/DfeApi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/api/DfeApiImpl$TocListener;
    }
.end annotation


# static fields
.field private static final AGE_VERIFICATION_TIMEOUT_MS:I

.field private static final BACKUP_DEVICES_BACKOFF_MULT:F

.field private static final BACKUP_DEVICES_MAX_RETRIES:I

.field private static final BACKUP_DEVICES_TIMEOUT_MS:I

.field private static final BULK_DETAILS_BACKOFF_MULT:F

.field private static final BULK_DETAILS_MAX_RETRIES:I

.field private static final BULK_DETAILS_TIMEOUT_MS:I

.field private static final EARLY_BACKOFF_MULT:F

.field private static final EARLY_MAX_RETRIES:I

.field private static final EARLY_TIMEOUT_MS:I

.field private static final PURCHASE_TIMEOUT_MS:I

.field private static final REPLICATE_LIBRARY_BACKOFF_MULT:F

.field private static final REPLICATE_LIBRARY_MAX_RETRIES:I

.field private static final REPLICATE_LIBRARY_TIMEOUT_MS:I

.field private static final SEND_AD_ID_IN_REQUESTS_FOR_RADS:Z

.field private static final SEND_PUBLIC_ANDROID_ID_IN_REQUESTS_FOR_RADS:Z

.field private static final VERIFY_ASSOCIATION_MAX_RETRIES:I

.field private static final VERIFY_ASSOCIATION_TIMEOUT_MS:I


# instance fields
.field private final mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

.field private final mQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->purchaseStatusTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->PURCHASE_TIMEOUT_MS:I

    .line 95
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->sendPublicAndroidIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/finsky/api/DfeApiImpl;->SEND_PUBLIC_ANDROID_ID_IN_REQUESTS_FOR_RADS:Z

    .line 102
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->sendAdIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/finsky/api/DfeApiImpl;->SEND_AD_ID_IN_REQUESTS_FOR_RADS:Z

    .line 112
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->ageVerificationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->AGE_VERIFICATION_TIMEOUT_MS:I

    .line 116
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_TIMEOUT_MS:I

    .line 119
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_MAX_RETRIES:I

    .line 123
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->backupDevicesBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_BACKOFF_MULT:F

    .line 136
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_TIMEOUT_MS:I

    .line 139
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_MAX_RETRIES:I

    .line 143
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->bulkDetailsBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_BACKOFF_MULT:F

    .line 147
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->verifyAssociationTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->VERIFY_ASSOCIATION_TIMEOUT_MS:I

    .line 151
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->verifyAssociationMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->VERIFY_ASSOCIATION_MAX_RETRIES:I

    .line 158
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_TIMEOUT_MS:I

    .line 162
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_MAX_RETRIES:I

    .line 166
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->replicateLibraryBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_BACKOFF_MULT:F

    .line 170
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_TIMEOUT_MS:I

    .line 173
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_MAX_RETRIES:I

    .line 176
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->earlyUpdateBackoffMultiplier:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_BACKOFF_MULT:F

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/RequestQueue;Lcom/google/android/finsky/api/DfeApiContext;)V
    .locals 0
    .param p1, "queue"    # Lcom/android/volley/RequestQueue;
    .param p2, "apiContext"    # Lcom/google/android/finsky/api/DfeApiContext;

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    .line 190
    iput-object p2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    .line 191
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/api/DfeApiImpl;)Lcom/google/android/finsky/api/DfeApiContext;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/api/DfeApiImpl;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    return-object v0
.end method

.method private addPublicAndroidIdHeaderIfNecessary(Lcom/google/android/finsky/api/DfeRequest;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 585
    .local p1, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<*>;"
    sget-boolean v3, Lcom/google/android/finsky/api/DfeApiImpl;->SEND_PUBLIC_ANDROID_ID_IN_REQUESTS_FOR_RADS:Z

    if-eqz v3, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->getApiContext()Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getPublicAndroidId()Ljava/lang/String;

    move-result-object v2

    .line 588
    .local v2, "publicAndroidId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 589
    const-string v3, "X-Public-Android-Id"

    invoke-virtual {p1, v3, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    .end local v2    # "publicAndroidId":Ljava/lang/String;
    :cond_0
    sget-boolean v3, Lcom/google/android/finsky/api/DfeApiImpl;->SEND_AD_ID_IN_REQUESTS_FOR_RADS:Z

    if-eqz v3, :cond_2

    .line 594
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->getApiContext()Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getAdId()Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "adId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->getApiContext()Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->isLimitAdTrackingEnabled()Ljava/lang/Boolean;

    move-result-object v1

    .line 596
    .local v1, "isLimitAdTrackingEnabled":Ljava/lang/Boolean;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 597
    const-string v3, "X-Ad-Id"

    invoke-virtual {p1, v3, v0}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    :cond_1
    if-eqz v1, :cond_2

    .line 600
    const-string v3, "X-Limit-Ad-Tracking-Enabled"

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    .end local v0    # "adId":Ljava/lang/String;
    .end local v1    # "isLimitAdTrackingEnabled":Ljava/lang/Boolean;
    :cond_2
    return-void
.end method

.method private static base64EncodeToken([B)Ljava/lang/String;
    .locals 1
    .param p0, "token"    # [B

    .prologue
    .line 954
    const/16 v0, 0x8

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getReviewsUrl(Ljava/lang/String;ZIII)Ljava/lang/String;
    .locals 3
    .param p0, "baseUrl"    # Ljava/lang/String;
    .param p1, "filterByDevice"    # Z
    .param p2, "versionFilter"    # I
    .param p3, "ratingFilter"    # I
    .param p4, "sortOrder"    # I

    .prologue
    .line 408
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 409
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p1, :cond_0

    .line 410
    const-string v1, "dfil"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 412
    :cond_0
    if-lez p2, :cond_1

    .line 413
    const-string v1, "vc"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 416
    :cond_1
    if-lez p3, :cond_2

    .line 417
    const-string v1, "rating"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 420
    :cond_2
    if-ltz p4, :cond_3

    .line 421
    const-string v1, "sort"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 423
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private makeAgeVerificationRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;
    .locals 5

    .prologue
    .line 1043
    new-instance v0, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v1, Lcom/google/android/finsky/api/DfeApiImpl;->AGE_VERIFICATION_TIMEOUT_MS:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    return-object v0
.end method

.method private makeEarlyUpdateRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;
    .locals 5

    .prologue
    .line 1254
    new-instance v0, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v1, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_TIMEOUT_MS:I

    sget v2, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_MAX_RETRIES:I

    sget v3, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_BACKOFF_MULT:F

    iget-object v4, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    return-object v0
.end method

.method private makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;
    .locals 5

    .prologue
    .line 1048
    new-instance v0, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v1, Lcom/google/android/finsky/api/DfeApiImpl;->PURCHASE_TIMEOUT_MS:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    return-object v0
.end method


# virtual methods
.method public acceptTos(Ljava/lang/String;Ljava/lang/Boolean;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "tosToken"    # Ljava/lang/String;
    .param p2, "optedIntoMarketingEmails"    # Ljava/lang/Boolean;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 439
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->ACCEPT_TOS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 442
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;>;"
    const-string v1, "tost"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    if-eqz p2, :cond_0

    .line 444
    const-string v1, "toscme"

    invoke-virtual {p2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public ackNotification(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 429
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/finsky/api/DfeApiImpl;->ACK_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "nid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "url":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 433
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;>;"
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public addReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "docRating"    # I
    .param p5, "isPlusReview"    # Z
    .param p7, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 358
    .local p6, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->ADDREVIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 361
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v1, "rating"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v1, "ipr"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public addToLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1055
    .local p1, "docids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;-><init>()V

    .line 1056
    .local v2, "modifyLibraryRequest":Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
    iput-object p2, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    .line 1057
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    .line 1058
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    .line 1059
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->MODIFY_LIBRARY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1062
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 1064
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public archiveFromLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1085
    .local p1, "docids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;-><init>()V

    .line 1086
    .local v2, "modifyLibraryRequest":Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
    iput-object p2, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    .line 1087
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    .line 1088
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    .line 1089
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->MODIFY_LIBRARY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1092
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 1094
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public billingProfile(Ljava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 8
    .param p1, "purchaseContextToken"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 818
    .local p2, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->BILLING_PROFILE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 821
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 822
    const-string v1, "ct"

    const-string v2, "dummy-token"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    if-eqz p1, :cond_0

    .line 824
    const-string v1, "pct"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_0
    const-string v1, "bppcc"

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerUtil;->createClientToken(Landroid/content/Context;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeApiImpl;->base64EncodeToken([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    if-eqz p2, :cond_1

    .line 829
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 830
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 833
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public checkIabPromo(ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docType"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "simIdentifier"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 786
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CHECK_IAB_PROMO_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 789
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;>;"
    const-string v1, "dt"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const-string v1, "shpn"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 792
    const-string v1, "dcbch"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public checkInstrument(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 713
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CHECK_INSTRUMENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 716
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;>;"
    const-string v1, "ct"

    const-string v2, "dummy-token"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public checkPromoOffers(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 802
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CHECK_PROMO_OFFER_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 805
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 806
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 808
    const-string v1, "X-DFE-Hardware-Id"

    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeApiContext;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public commitPurchase(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/finsky/api/DfeRequest;
    .locals 8
    .param p1, "purchaseContextToken"    # Ljava/lang/String;
    .param p3, "riskHeader"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/google/android/finsky/api/DfeRequest",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 653
    .local p2, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->COMMIT_PURCHASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 656
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;>;"
    const-string v1, "pct"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 658
    const-string v1, "ct"

    const-string v2, "dummy-token"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    if-eqz p2, :cond_0

    .line 660
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 661
    .local v6, "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 664
    .end local v6    # "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_0
    if-eqz p3, :cond_1

    .line 665
    const-string v1, "chdi"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/DfeApiImpl;->addPublicAndroidIdHeaderIfNecessary(Lcom/google/android/finsky/api/DfeRequest;)V

    .line 669
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 672
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/DfeRequest;

    return-object v1
.end method

.method public consumePurchase(Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "purchaseToken"    # Ljava/lang/String;
    .param p2, "offerType"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ConsumePurchaseResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1100
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ConsumePurchaseResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CONSUME_PURCHASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1103
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ConsumePurchaseResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1104
    const-string v1, "pt"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    const-string v1, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    const-string v1, "shpn"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 1109
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "request"    # Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 738
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CREATE_INSTRUMENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 742
    .local v0, "dfeRequest":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setAvoidBulkCancel()V

    .line 743
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 744
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public deleteReview(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 372
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->DELETEREVIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 375
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public delivery(Ljava/lang/String;I[BLjava/lang/Integer;Ljava/lang/Integer;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 13
    .param p1, "docid"    # Ljava/lang/String;
    .param p2, "offerType"    # I
    .param p3, "serverToken"    # [B
    .param p4, "appVersionCode"    # Ljava/lang/Integer;
    .param p5, "previousVersionCode"    # Ljava/lang/Integer;
    .param p6, "patchFormats"    # [Ljava/lang/String;
    .param p7, "certificateHashSelfUpdateMD5"    # Ljava/lang/String;
    .param p8, "certificateHash"    # Ljava/lang/String;
    .param p9, "deliveryToken"    # Ljava/lang/String;
    .param p10, "checkinConsistencyToken"    # Ljava/lang/String;
    .param p12, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I[B",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 895
    .local p11, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    sget-object v2, Lcom/google/android/finsky/api/DfeApiImpl;->DELIVERY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "doc"

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v12

    .line 898
    .local v12, "uriBuilder":Landroid/net/Uri$Builder;
    if-eqz p3, :cond_0

    .line 899
    invoke-static/range {p3 .. p3}, Lcom/google/android/finsky/api/DfeApiImpl;->base64EncodeToken([B)Ljava/lang/String;

    move-result-object v8

    .line 900
    .local v8, "encodedServerToken":Ljava/lang/String;
    const-string v2, "st"

    invoke-virtual {v12, v2, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 902
    .end local v8    # "encodedServerToken":Ljava/lang/String;
    :cond_0
    if-eqz p4, :cond_1

    .line 903
    const-string v2, "vc"

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 905
    :cond_1
    if-eqz p5, :cond_2

    .line 906
    const-string v2, "bvc"

    invoke-virtual/range {p5 .. p5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 908
    if-eqz p6, :cond_2

    .line 909
    move-object/from16 v7, p6

    .local v7, "arr$":[Ljava/lang/String;
    array-length v10, v7

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_2

    aget-object v11, v7, v9

    .line 910
    .local v11, "patchFormat":Ljava/lang/String;
    const-string v2, "pf"

    invoke-virtual {v12, v2, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 909
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 916
    .end local v7    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "patchFormat":Ljava/lang/String;
    :cond_2
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 917
    const-string v2, "shh"

    move-object/from16 v0, p7

    invoke-virtual {v12, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 921
    :cond_3
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 922
    const-string v2, "ch"

    move-object/from16 v0, p8

    invoke-virtual {v12, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 925
    :cond_4
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 926
    const-string v2, "dtok"

    move-object/from16 v0, p9

    invoke-virtual {v12, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 929
    :cond_5
    new-instance v1, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v12}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    move-object/from16 v5, p11

    move-object/from16 v6, p12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 932
    .local v1, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 933
    const-string v2, "X-DFE-Device-Checkin-Consistency-Token"

    invoke-static/range {p10 .. p10}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 937
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public earlyDelivery(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 12
    .param p1, "docid"    # Ljava/lang/String;
    .param p2, "offerType"    # I
    .param p3, "appVersionCode"    # Ljava/lang/Integer;
    .param p4, "previousVersionCode"    # Ljava/lang/Integer;
    .param p5, "patchFormats"    # [Ljava/lang/String;
    .param p6, "certificateHashSelfUpdateMD5"    # Ljava/lang/String;
    .param p7, "certificateHash"    # Ljava/lang/String;
    .param p8, "deliveryToken"    # Ljava/lang/String;
    .param p9, "checkinConsistencyToken"    # Ljava/lang/String;
    .param p11, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1208
    .local p10, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    sget-object v2, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_DELIVERY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "doc"

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v11

    .line 1211
    .local v11, "uriBuilder":Landroid/net/Uri$Builder;
    if-eqz p3, :cond_0

    .line 1212
    const-string v2, "vc"

    invoke-virtual {p3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1214
    :cond_0
    if-eqz p4, :cond_1

    .line 1215
    const-string v2, "bvc"

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1217
    if-eqz p5, :cond_1

    .line 1218
    move-object/from16 v7, p5

    .local v7, "arr$":[Ljava/lang/String;
    array-length v9, v7

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v10, v7, v8

    .line 1219
    .local v10, "patchFormat":Ljava/lang/String;
    const-string v2, "pf"

    invoke-virtual {v11, v2, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1218
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1225
    .end local v7    # "arr$":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "patchFormat":Ljava/lang/String;
    :cond_1
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1226
    const-string v2, "shh"

    move-object/from16 v0, p6

    invoke-virtual {v11, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1230
    :cond_2
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1231
    const-string v2, "ch"

    move-object/from16 v0, p7

    invoke-virtual {v11, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1234
    :cond_3
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1235
    const-string v2, "dtok"

    move-object/from16 v0, p8

    invoke-virtual {v11, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1238
    :cond_4
    new-instance v1, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v11}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    move-object/from16 v5, p10

    move-object/from16 v6, p11

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1241
    .local v1, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1242
    const-string v2, "X-DFE-Device-Checkin-Consistency-Token"

    invoke-static/range {p9 .. p9}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    :cond_5
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeEarlyUpdateRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1246
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 1247
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public earlyUpdate(Lcom/google/android/finsky/protos/DeviceConfigurationProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "deviceConfig"    # Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/DeviceConfigurationProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1179
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;>;"
    new-instance v2, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateRequest;-><init>()V

    .line 1180
    .local v2, "requestProto":Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateRequest;
    if-eqz p1, :cond_0

    .line 1181
    iput-object p1, v2, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 1183
    :cond_0
    sget-object v3, Lcom/google/android/finsky/api/DfeApiImpl;->EARLY_UPDATE_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1184
    .local v1, "url":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1187
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeEarlyUpdateRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1188
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 1189
    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v3

    return-object v3
.end method

.method public flagContent(Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "contentFlagType"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 294
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->FLAG_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 297
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;>;"
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v1, "cft"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeApiContext;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeApiContext;->getAccountName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApiContext()Lcom/google/android/finsky/api/DfeApiContext;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    return-object v0
.end method

.method public getBackupDeviceChoices(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "checkinConsistencyTokenHeader"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 750
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->GET_BACKUP_DEVICE_CHOICES_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 755
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 756
    const-string v1, "X-DFE-Device-Checkin-Consistency-Token"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    :cond_0
    new-instance v1, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v2, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_TIMEOUT_MS:I

    sget v3, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_MAX_RETRIES:I

    sget v4, Lcom/google/android/finsky/api/DfeApiImpl;->BACKUP_DEVICES_BACKOFF_MULT:F

    iget-object v5, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 761
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getBackupDocumentChoices(JLjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "androidId"    # J
    .param p3, "checkinConsistencyTokenHeader"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 767
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;>;"
    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->GET_BACKUP_DOCUMENT_CHOICES_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 768
    .local v6, "builder":Landroid/net/Uri$Builder;
    const-string v1, "raid"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 771
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 776
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;>;"
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 777
    const-string v1, "X-DFE-Device-Checkin-Consistency-Token"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getBrowseLayout(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Browse$BrowseResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 286
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Browse$BrowseResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 288
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Browse$BrowseResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getDetails(Ljava/lang/String;ZZLjava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 9
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "noPrefetch"    # Z
    .param p3, "avoidBulkCancel"    # Z
    .param p6, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Details$DetailsResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 454
    .local p4, "voucherIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p5, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Details$DetailsResponse;

    move-object v1, p1

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 456
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    if-eqz p2, :cond_0

    .line 457
    const-string v1, "X-DFE-No-Prefetch"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_0
    if-eqz p3, :cond_1

    .line 460
    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->setAvoidBulkCancel()V

    .line 462
    :cond_1
    if-eqz p4, :cond_4

    invoke-interface {p4}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->vouchersInDetailsRequestsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 464
    const-string v1, "X-DFE-Client-Has-Vouchers"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-interface {p4}, Ljava/util/Collection;->size()I

    move-result v2

    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->maxVouchersInDetailsRequest:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gt v2, v1, :cond_4

    .line 467
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 468
    .local v7, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 469
    .local v8, "voucherId":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 470
    const/16 v1, 0x2c

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 472
    :cond_2
    invoke-static {v8}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 474
    .end local v8    # "voucherId":Ljava/lang/String;
    :cond_3
    const-string v1, "X-DFE-Vouchers-Backend-Docids-CSV"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getDetails(Ljava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 483
    .local p1, "docids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;>;"
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApiImpl;->getDetails(Ljava/util/Collection;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    return-object v0
.end method

.method public getDetails(Ljava/util/Collection;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 10
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "includeDetails"    # Z
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .local p1, "docids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;>;"
    const/4 v9, 0x1

    .line 490
    new-instance v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;

    invoke-direct {v3}, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;-><init>()V

    .line 491
    .local v3, "bulkDetailsRequest":Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 492
    .local v8, "sortedDocids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 493
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v8, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    .line 494
    iput-boolean p3, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    .line 495
    iput-boolean v9, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeDetails:Z

    .line 496
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 497
    iput-object p2, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    .line 498
    iput-boolean v9, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasSourcePackageName:Z

    .line 500
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/DfeApiImpl$1;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v5, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    move-object v1, p0

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/api/DfeApiImpl$1;-><init>(Lcom/google/android/finsky/api/DfeApiImpl;Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Ljava/util/List;)V

    .line 517
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;>;"
    invoke-virtual {v0, v9}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 518
    new-instance v1, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v2, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_TIMEOUT_MS:I

    sget v4, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_MAX_RETRIES:I

    sget v5, Lcom/google/android/finsky/api/DfeApiImpl;->BULK_DETAILS_BACKOFF_MULT:F

    iget-object v6, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v1, v2, v4, v5, v6}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 520
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getInitialInstrumentFlowState(Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "instrumentFlowHandle"    # Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 725
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->GET_INITIAL_INSTRUMENT_FLOW_STATE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 729
    .local v0, "dfeRequest":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 730
    const-string v1, "ifh"

    iget-object v2, p1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;->token:[B

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeApiImpl;->base64EncodeToken([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getLibraryUrl(ILjava/lang/String;I[B)Ljava/lang/String;
    .locals 5
    .param p1, "corpus"    # I
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p3, "docType"    # I
    .param p4, "serverToken"    # [B

    .prologue
    .line 942
    sget-object v2, Lcom/google/android/finsky/api/DfeApiImpl;->LIBRARY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "c"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "dt"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "libid"

    invoke-virtual {v2, v3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 946
    .local v1, "uriBuilder":Landroid/net/Uri$Builder;
    if-eqz p4, :cond_0

    .line 947
    invoke-static {p4}, Lcom/google/android/finsky/api/DfeApiImpl;->base64EncodeToken([B)Ljava/lang/String;

    move-result-object v0

    .line 948
    .local v0, "encodedServerToken":Ljava/lang/String;
    const-string v2, "st"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 950
    .end local v0    # "encodedServerToken":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getList(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/DocList$ListResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 306
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/DocList$ListResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/DocList$ListResponse;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 308
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/DocList$ListResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getMyAccount(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 348
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->MY_ACCOUNT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 351
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getReviews(Ljava/lang/String;ZIIILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "filterByDevice"    # Z
    .param p3, "versionFilter"    # I
    .param p4, "ratingFilter"    # I
    .param p5, "sortOrder"    # I
    .param p7, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZIII",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 400
    .local p6, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/finsky/api/DfeApiImpl;->getReviewsUrl(Ljava/lang/String;ZIII)Ljava/lang/String;

    move-result-object v1

    .line 401
    .local v1, "fullUrl":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 403
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public getSelfUpdate(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "deviceConfigurationToken"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 261
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->SELFUPDATE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 265
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    const-string v1, "X-DFE-Device-Config-Token"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public getToc(ZLjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "allowDouble"    # Z
    .param p2, "deviceConfigurationToken"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Toc$TocResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 216
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Toc$TocResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CHANNELS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Toc$TocResponse;

    new-instance v4, Lcom/google/android/finsky/api/DfeApiImpl$TocListener;

    invoke-direct {v4, p0, p3}, Lcom/google/android/finsky/api/DfeApiImpl$TocListener;-><init>(Lcom/google/android/finsky/api/DfeApiImpl;Lcom/android/volley/Response$Listener;)V

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 218
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Toc$TocResponse;>;"
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/DfeRequest;->setAllowMultipleResponses(Z)V

    .line 220
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 221
    const-string v1, "X-DFE-Device-Config-Token"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public initiateAssociation(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "dcbInstrumentKey"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 839
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->DCB_INITIATE_ASSOCIATION_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 843
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;>;"
    const-string v1, "dcbch"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public invalidateDetailsCache(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "detailsUrl"    # Ljava/lang/String;
    .param p2, "fullExpire"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1128
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Details$DetailsResponse;

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1130
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/google/android/finsky/api/DfeClearCacheRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5, p2, v4}, Lcom/google/android/finsky/api/DfeClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1132
    return-void
.end method

.method public invalidateListCache(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "listUrl"    # Ljava/lang/String;
    .param p2, "fullExpire"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1138
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/DocList$ListResponse;

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1140
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/DocList$ListResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/google/android/finsky/api/DfeClearCacheRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5, p2, v4}, Lcom/google/android/finsky/api/DfeClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1142
    return-void
.end method

.method public invalidateReviewsCache(Ljava/lang/String;ZIIIZ)V
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "filterByDevice"    # Z
    .param p3, "versionFilter"    # I
    .param p4, "ratingFilter"    # I
    .param p5, "sortOrder"    # I
    .param p6, "fullExpire"    # Z

    .prologue
    const/4 v4, 0x0

    .line 1115
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/finsky/api/DfeApiImpl;->getReviewsUrl(Ljava/lang/String;ZIII)Ljava/lang/String;

    move-result-object v1

    .line 1118
    .local v1, "fullUrl":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1120
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    new-instance v3, Lcom/google/android/finsky/api/DfeClearCacheRequest;

    iget-object v5, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6, p6, v4}, Lcom/google/android/finsky/api/DfeClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 1122
    return-void
.end method

.method public invalidateSelfUpdateCache()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 277
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->SELFUPDATE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 279
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/google/android/finsky/api/DfeClearCacheRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v3, v5, v6, v4}, Lcom/google/android/finsky/api/DfeClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 281
    return-void
.end method

.method public invalidateTocCache()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 252
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->CHANNELS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Toc$TocResponse;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 254
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Toc$TocResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    new-instance v2, Lcom/google/android/finsky/api/DfeClearCacheRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->getCacheKey()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v3, v5, v6, v4}, Lcom/google/android/finsky/api/DfeClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 256
    return-void
.end method

.method public log(Lcom/google/android/finsky/protos/Log$LogRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "request"    # Lcom/google/android/finsky/protos/Log$LogRequest;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/Log$LogRequest;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Log$LogResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 867
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Log$LogResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->LOG_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/Log$LogResponse;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 870
    .local v0, "dfeRequest":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<*>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setAvoidBulkCancel()V

    .line 871
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public makePurchase(Lcom/google/android/finsky/api/model/Document;IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p3, "apiVersion"    # I
    .param p4, "iabPackageName"    # Ljava/lang/String;
    .param p5, "iabPackageSignatureHash"    # Ljava/lang/String;
    .param p6, "iabPackageVersion"    # I
    .param p7, "iabDeveloperPayload"    # Ljava/lang/String;
    .param p10, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/Document;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Buy$BuyResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 547
    .local p8, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p9, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Buy$BuyResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->PURCHASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Buy$BuyResponse;

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 549
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Buy$BuyResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 550
    const-string v1, "doc"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v1, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 553
    const-string v1, "vc"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :cond_0
    const-string v1, "ct"

    const-string v2, "dummy-token"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    if-eqz p4, :cond_2

    .line 558
    const-string v1, "bav"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string v1, "shpn"

    invoke-virtual {v0, v1, p4}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string v1, "shh"

    invoke-virtual {v0, v1, p5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v1, "shvc"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    if-nez p7, :cond_1

    .line 566
    const-string p7, ""

    .line 568
    :cond_1
    const-string v1, "payload"

    invoke-virtual {v0, v1, p7}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    :cond_2
    if-eqz p8, :cond_3

    .line 571
    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 572
    .local v6, "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 575
    .end local v6    # "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/DfeApiImpl;->addPublicAndroidIdHeaderIfNecessary(Lcom/google/android/finsky/api/DfeRequest;)V

    .line 576
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 578
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public makePurchase(Lcom/google/android/finsky/api/model/Document;ILjava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/Document;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Buy$BuyResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 538
    .local p3, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Buy$BuyResponse;>;"
    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v8, p3

    move-object v9, p4

    move-object/from16 v10, p5

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchase(Lcom/google/android/finsky/api/model/Document;IILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    return-object v0
.end method

.method public preloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "deviceConfigurationToken"    # Ljava/lang/String;
    .param p2, "checkinConsistencyToken"    # Ljava/lang/String;
    .param p3, "stubPackageDocId"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1262
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;>;"
    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->PRELOADS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 1263
    .local v6, "uriBuilder":Landroid/net/Uri$Builder;
    const-string v1, "doc"

    invoke-virtual {v6, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1264
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1267
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1268
    const-string v1, "X-DFE-Device-Config-Token"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1272
    const-string v1, "X-DFE-Device-Checkin-Consistency-Token"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public preparePurchase(Ljava/lang/String;ILcom/google/android/finsky/api/DfeApi$IabParameters;Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;Ljava/lang/String;Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;ILjava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/finsky/api/DfeRequest;
    .locals 8
    .param p1, "docid"    # Ljava/lang/String;
    .param p2, "offerType"    # I
    .param p3, "iabParameters"    # Lcom/google/android/finsky/api/DfeApi$IabParameters;
    .param p4, "gaiaAuthParameters"    # Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;
    .param p5, "instrumentId"    # Ljava/lang/String;
    .param p6, "voucherParams"    # Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;
    .param p7, "appVersionCode"    # I
    .param p10, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/finsky/api/DfeApi$IabParameters;",
            "Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/google/android/finsky/api/DfeRequest",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 611
    .local p8, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p9, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->PREPARE_PURCHASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    move-object/from16 v4, p9

    move-object/from16 v5, p10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 614
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 615
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-string v1, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string v1, "ct"

    const-string v2, "dummy-token"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v1, "bppcc"

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerUtil;->createClientToken(Landroid/content/Context;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeApiImpl;->base64EncodeToken([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    if-eqz p5, :cond_0

    .line 621
    const-string v1, "ii"

    invoke-virtual {v0, v1, p5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_0
    const-string v1, "chv"

    iget-boolean v2, p6, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->hasVouchers:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    const-string v1, "aav"

    iget-boolean v2, p6, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->autoApply:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v1, p6, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->selectedVoucherId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 628
    const-string v1, "usvid"

    iget-object v2, p6, Lcom/google/android/finsky/billing/lightpurchase/VoucherParams;->selectedVoucherId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_1
    if-lez p7, :cond_2

    .line 632
    const-string v1, "vc"

    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_2
    if-eqz p3, :cond_3

    .line 635
    invoke-virtual {p3, v0}, Lcom/google/android/finsky/api/DfeApi$IabParameters;->addToRequest(Lcom/google/android/finsky/api/DfeApi$DfePostRequest;)V

    .line 637
    :cond_3
    if-eqz p4, :cond_4

    .line 638
    invoke-virtual {p4, v0}, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->addToRequest(Lcom/google/android/finsky/api/DfeApi$DfePostRequest;)V

    .line 640
    :cond_4
    if-eqz p8, :cond_5

    .line 641
    invoke-interface/range {p8 .. p8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 642
    .local v6, "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 646
    .end local v6    # "extraPostParam":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/DfeRequest;

    return-object v1
.end method

.method public rateReview(Ljava/lang/String;Ljava/lang/String;ILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "reviewId"    # Ljava/lang/String;
    .param p3, "reviewRating"    # I
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 383
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->RATEREVIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "doc"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "revId"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "rating"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 391
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Rev$ReviewResponse;>;"
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 392
    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeRequest;->setAvoidBulkCancel()V

    .line 393
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public rateSuggestedContent(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "dismissalUrl"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/RateSuggestedContentResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 975
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/RateSuggestedContentResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 978
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/RateSuggestedContentResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public redeemCode(Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "redeemCodeRequest"    # Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 984
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->REDEEM_CODE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 988
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;>;"
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 990
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 991
    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/DfeApiImpl;->addPublicAndroidIdHeaderIfNecessary(Lcom/google/android/finsky/api/DfeRequest;)V

    .line 993
    invoke-virtual {v0}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setAvoidBulkCancel()V

    .line 994
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public removeFromLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1070
    .local p1, "docids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;-><init>()V

    .line 1071
    .local v2, "modifyLibraryRequest":Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
    iput-object p2, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    .line 1072
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    .line 1073
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v2, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    .line 1074
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->MODIFY_LIBRARY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1077
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 1079
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public replicateLibrary(Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "replicateLibraryRequest"    # Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationRequest;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationRequest;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 877
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 881
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;>;"
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 883
    invoke-virtual {v0}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setAvoidBulkCancel()V

    .line 884
    new-instance v1, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v2, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_TIMEOUT_MS:I

    sget v3, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_MAX_RETRIES:I

    sget v4, Lcom/google/android/finsky/api/DfeApiImpl;->REPLICATE_LIBRARY_BACKOFF_MULT:F

    iget-object v5, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 886
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public requestAgeVerificationForm(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1001
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->REQUEST_AGE_VERIFICATION_FORM_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1004
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeAgeVerificationRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1005
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public resendAgeVerificationCode(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1024
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1027
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeAgeVerificationRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1028
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public resolveLink(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "referringPackage"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 960
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;>;"
    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->RESOLVE_LINK:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    .line 961
    .local v6, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 962
    const-string v1, "ref"

    invoke-virtual {v6, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 964
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 967
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;>;"
    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/DfeApiImpl;->addPublicAndroidIdHeaderIfNecessary(Lcom/google/android/finsky/api/DfeRequest;)V

    .line 968
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public revoke(Ljava/lang/String;ILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "offerType"    # I
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/RevokeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 678
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/RevokeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->REVOKE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/RevokeResponse;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 681
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/RevokeResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 682
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const-string v1, "ot"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    new-instance v1, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeResponseVerifierImpl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V

    .line 686
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public search(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Search$SearchResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 314
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Search$SearchResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/Search$SearchResponse;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 316
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/Search$SearchResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public searchSuggest(Ljava/lang/String;IIZZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "backendId"    # I
    .param p3, "iconSize"    # I
    .param p4, "requestQuery"    # Z
    .param p5, "requestNavigational"    # Z
    .param p7, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIZZ",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 323
    .local p6, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;>;"
    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->SEARCH_SUGGEST_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "q"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "c"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ssis"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    .line 330
    .local v6, "uriBuilder":Landroid/net/Uri$Builder;
    if-eqz p4, :cond_0

    .line 331
    const-string v1, "sst"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 334
    :cond_0
    if-eqz p5, :cond_1

    .line 335
    const-string v1, "sst"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 339
    :cond_1
    new-instance v0, Lcom/google/android/finsky/api/DfeRequest;

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 342
    .local v0, "request":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;>;"
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public setPlusOne(Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "plusOne"    # Z
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 526
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->PLUSONE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 529
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;>;"
    const-string v1, "doc"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string v2, "rating"

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1

    .line 530
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateInstrument(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "request"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 693
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;>;"
    const-string v1, "dummy-token"

    iput-object v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;->checkoutToken:Ljava/lang/String;

    .line 694
    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;->hasCheckoutToken:Z

    .line 695
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->UPDATE_INSTRUMENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 700
    .local v0, "dfeRequest":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<*>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setAvoidBulkCancel()V

    .line 701
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 702
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 704
    const-string v1, "X-DFE-Hardware-Id"

    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeApiContext;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/ProtoDfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public uploadDeviceConfig(Lcom/google/android/finsky/protos/DeviceConfigurationProto;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 7
    .param p1, "deviceConfig"    # Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .param p2, "gcmRegistrationId"    # Ljava/lang/String;
    .param p3, "deviceConfigurationToken"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/DeviceConfigurationProto;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1151
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;>;"
    new-instance v2, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;-><init>()V

    .line 1152
    .local v2, "requestProto":Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;
    if-eqz p1, :cond_0

    .line 1153
    iput-object p1, v2, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 1155
    :cond_0
    if-eqz p2, :cond_1

    .line 1156
    iput-object p2, v2, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->gcmRegistrationId:Ljava/lang/String;

    .line 1157
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigRequest;->hasGcmRegistrationId:Z

    .line 1160
    :cond_1
    sget-object v3, Lcom/google/android/finsky/api/DfeApiImpl;->UPLOAD_DEVICE_CONFIG_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1161
    .local v1, "url":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/api/ProtoDfeRequest;

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v4, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/ProtoDfeRequest;-><init>(Ljava/lang/String;Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1164
    .local v0, "request":Lcom/google/android/finsky/api/ProtoDfeRequest;, "Lcom/google/android/finsky/api/ProtoDfeRequest<Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makePurchaseRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/ProtoDfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1166
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1167
    const-string v3, "X-DFE-Device-Config-Token"

    invoke-static {p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/finsky/api/ProtoDfeRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v3

    return-object v3
.end method

.method public verifyAge(Ljava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1011
    .local p2, "postParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1013
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 1014
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1016
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeAgeVerificationRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1017
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public verifyAgeVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "codeParamKey"    # Ljava/lang/String;
    .param p3, "code"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1034
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    move-object v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 1037
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;>;"
    invoke-virtual {v0, p2, p3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    invoke-direct {p0}, Lcom/google/android/finsky/api/DfeApiImpl;->makeAgeVerificationRetryPolicy()Lcom/google/android/finsky/api/DfeRetryPolicy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 1039
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method

.method public verifyAssociation(Ljava/lang/String;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1, "dcbInstrumentKey"    # Ljava/lang/String;
    .param p2, "acceptedPiiTosVersion"    # Ljava/lang/String;
    .param p3, "getSubscriberAddress"    # Z
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 851
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;>;"
    new-instance v0, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;

    sget-object v1, Lcom/google/android/finsky/api/DfeApiImpl;->DCB_VERIFY_ASSOCIATION_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    const-class v3, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;-><init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 854
    .local v0, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;>;"
    const-string v1, "dcbch"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 856
    const-string v1, "dcbptosv"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    :cond_0
    const-string v1, "dcbreqaddr"

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    new-instance v1, Lcom/google/android/finsky/api/DfeRetryPolicy;

    sget v2, Lcom/google/android/finsky/api/DfeApiImpl;->VERIFY_ASSOCIATION_TIMEOUT_MS:I

    sget v3, Lcom/google/android/finsky/api/DfeApiImpl;->VERIFY_ASSOCIATION_MAX_RETRIES:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(IIFLcom/google/android/finsky/api/DfeApiContext;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 861
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeApiImpl;->mQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v1

    return-object v1
.end method
