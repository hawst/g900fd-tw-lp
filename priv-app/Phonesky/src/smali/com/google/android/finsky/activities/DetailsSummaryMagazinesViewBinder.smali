.class public Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;
.super Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.source "DetailsSummaryMagazinesViewBinder.java"


# instance fields
.field private mSubscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    .line 28
    return-void
.end method


# virtual methods
.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V
    .locals 3
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bindDynamicSection"    # Z
    .param p3, "views"    # [Landroid/view/View;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    .line 35
    return-void
.end method

.method protected displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
    .locals 1
    .param p1, "launchButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p3, "buyButton2"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p4, "tryButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p5, "downloadButton"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 62
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->displayActionButtonsIfNecessaryNew(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z

    move-result v0

    return v0
.end method

.method protected setupActionButtons(Z)V
    .locals 2
    .param p1, "isInTransientState"    # Z

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupActionButtons(Z)V

    .line 41
    const v1, 0x7f0a013e

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "downloadButton":Landroid/view/View;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->syncButtonSectionVisibility()V

    .line 45
    return-void
.end method

.method protected shouldDisplayOfferNote()Z
    .locals 4

    .prologue
    .line 52
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    .line 53
    .local v0, "libraries":Lcom/google/android/finsky/library/Libraries;
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mSubscriptions:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v0, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v1

    .line 55
    .local v1, "owner":Landroid/accounts/Account;
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
