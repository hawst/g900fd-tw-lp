.class public final Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentSyncRequestProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;,
        Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    }
.end annotation


# instance fields
.field public assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

.field public hasIncremental:Z

.field public hasSideloadedAppCount:Z

.field public incremental:Z

.field public sideloadedAppCount:I

.field public systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    .line 414
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 417
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    .line 418
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasIncremental:Z

    .line 419
    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    .line 420
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasSideloadedAppCount:Z

    .line 421
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    .line 422
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->cachedSize:I

    .line 424
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 457
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 458
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasIncremental:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    if-eqz v3, :cond_1

    .line 459
    :cond_0
    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 462
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 463
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 464
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    aget-object v0, v3, v1

    .line 465
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    if-eqz v0, :cond_2

    .line 466
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 463
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 471
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 472
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 473
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    aget-object v0, v3, v1

    .line 474
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    if-eqz v0, :cond_4

    .line 475
    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 472
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 480
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasSideloadedAppCount:Z

    if-nez v3, :cond_6

    iget v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    if-eqz v3, :cond_7

    .line 481
    :cond_6
    const/16 v3, 0xe

    iget v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 484
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 493
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 497
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 498
    :sswitch_0
    return-object p0

    .line 503
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    .line 504
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasIncremental:Z

    goto :goto_0

    .line 508
    :sswitch_2
    const/16 v5, 0x13

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 510
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    if-nez v5, :cond_2

    move v1, v4

    .line 511
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    .line 513
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    if-eqz v1, :cond_1

    .line 514
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 516
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 517
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;-><init>()V

    aput-object v5, v2, v1

    .line 518
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 519
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 516
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 510
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    array-length v1, v5

    goto :goto_1

    .line 522
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;-><init>()V

    aput-object v5, v2, v1

    .line 523
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 524
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    goto :goto_0

    .line 528
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    :sswitch_3
    const/16 v5, 0x53

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 530
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    if-nez v5, :cond_5

    move v1, v4

    .line 531
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    .line 533
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    if-eqz v1, :cond_4

    .line 534
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 536
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 537
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;-><init>()V

    aput-object v5, v2, v1

    .line 538
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 539
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 536
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 530
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    array-length v1, v5

    goto :goto_3

    .line 542
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;-><init>()V

    aput-object v5, v2, v1

    .line 543
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 544
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    goto/16 :goto_0

    .line 548
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    .line 549
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasSideloadedAppCount:Z

    goto/16 :goto_0

    .line 493
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x53 -> :sswitch_3
        0x70 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 430
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasIncremental:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    if-eqz v2, :cond_1

    .line 431
    :cond_0
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->incremental:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 433
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 434
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 435
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    aget-object v0, v2, v1

    .line 436
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    if-eqz v0, :cond_2

    .line 437
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 434
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 441
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 442
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 443
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    aget-object v0, v2, v1

    .line 444
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    if-eqz v0, :cond_4

    .line 445
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 442
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 449
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasSideloadedAppCount:Z

    if-nez v2, :cond_6

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    if-eqz v2, :cond_7

    .line 450
    :cond_6
    const/16 v2, 0xe

    iget v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 452
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 453
    return-void
.end method
