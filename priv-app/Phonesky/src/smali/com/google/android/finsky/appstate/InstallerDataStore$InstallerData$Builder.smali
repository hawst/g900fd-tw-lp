.class public Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
.super Ljava/lang/Object;
.source "InstallerDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    new-instance v0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 291
    return-void
.end method

.method public static buildUpon(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 5
    .param p0, "original"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 294
    new-instance v0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    invoke-direct {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "builder":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    if-eqz p0, :cond_1

    .line 296
    # getter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->packageName:Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$100(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    const-string v1, "Package name mismatch,  %s != %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    # getter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->packageName:Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$100(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setAutoUpdate(Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDesiredVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastNotifiedVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setLastNotifiedVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 303
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryDataTimestampMs()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDeliveryData(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 305
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setInstallerState(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 306
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDownloadUri(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFirstDownloadMs()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setFirstDownloadMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 308
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setExternalReferrer(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 309
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getContinueUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setContinueUrl(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 310
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setAccountName(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 311
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 312
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setFlags(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 313
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastUpdateTimestampMs()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setLastUpdateTimestampMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 314
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountForUpdate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setAccountForUpdate(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 315
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoAcquireTags()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setAutoAcquireTags([Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 316
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrerTimestampMs()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setExternalReferrerTimestampMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 318
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPersistentFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setPersistentFlags(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 319
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPermissionsVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setPermissionsVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 320
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->setDeliveryToken(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;

    .line 322
    :cond_1
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    return-object v0
.end method

.method public setAccountForUpdate(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "accountForUpdate"    # Ljava/lang/String;

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->accountForUpdate:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1602(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 393
    return-object p0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->accountName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1102(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 368
    return-object p0
.end method

.method public setAutoAcquireTags([Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "autoAcquireTags"    # [Ljava/lang/String;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->autoAcquireTags:[Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1702(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;[Ljava/lang/String;)[Ljava/lang/String;

    .line 398
    return-object p0
.end method

.method public setAutoUpdate(Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "autoUpdate"    # Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->autoUpdate:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$202(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    .line 327
    return-object p0
.end method

.method public setContinueUrl(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "continueUrl"    # Ljava/lang/String;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->continueUrl:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1402(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 383
    return-object p0
.end method

.method public setDeliveryData(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 2
    .param p1, "deliveryData"    # Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .param p2, "timestampMs"    # J

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->deliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$502(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 342
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->deliveryDataTimestampMs:J
    invoke-static {v0, p2, p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$602(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)J

    .line 343
    return-object p0
.end method

.method public setDeliveryToken(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "deliveryToken"    # Ljava/lang/String;

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->deliveryToken:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$2102(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 418
    return-object p0
.end method

.method public setDesiredVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "desiredVersion"    # I

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->desiredVersion:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$302(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 332
    return-object p0
.end method

.method public setDownloadUri(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "downloadUri"    # Ljava/lang/String;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->downloadUri:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$802(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 353
    return-object p0
.end method

.method public setExternalReferrer(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "externalReferrer"    # Ljava/lang/String;

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->externalReferrer:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1002(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 363
    return-object p0
.end method

.method public setExternalReferrerTimestampMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "externalReferrerTimestampMs"    # J

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->externalReferrerTimestampMs:J
    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1802(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)J

    .line 403
    return-object p0
.end method

.method public setFirstDownloadMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "firstDownloadMs"    # J

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->firstDownloadMs:J
    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$902(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)J

    .line 358
    return-object p0
.end method

.method public setFlags(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->flags:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1302(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 378
    return-object p0
.end method

.method public setInstallerState(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "installerState"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->installerState:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$702(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 348
    return-object p0
.end method

.method public setLastNotifiedVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "lastNotifiedVersion"    # I

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->lastNotifiedVersion:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$402(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 337
    return-object p0
.end method

.method public setLastUpdateTimestampMs(J)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "lastUpdateTimestampMs"    # J

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->lastUpdateTimestampMs:J
    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1502(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)J

    .line 388
    return-object p0
.end method

.method public setPermissionsVersion(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "permissionsVersion"    # I

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->permissionsVersion:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$2002(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 413
    return-object p0
.end method

.method public setPersistentFlags(I)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "persistentFlags"    # I

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->persistentFlags:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1902(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;I)I

    .line 408
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData$Builder;->mInstance:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    # setter for: Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->title:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->access$1202(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/lang/String;)Ljava/lang/String;

    .line 373
    return-object p0
.end method
