.class public Lcom/google/android/finsky/library/LibraryLoader;
.super Ljava/lang/Object;
.source "LibraryLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/library/LibraryLoader$State;
    }
.end annotation


# instance fields
.field private final mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

.field private final mBackgroundHandler:Landroid/os/Handler;

.field private final mLoadingCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationHandler:Landroid/os/Handler;

.field private final mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

.field private mState:Lcom/google/android/finsky/library/LibraryLoader$State;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/library/SQLiteLibrary;Lcom/google/android/finsky/library/AccountLibrary;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1
    .param p1, "sqLiteLibrary"    # Lcom/google/android/finsky/library/SQLiteLibrary;
    .param p2, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;
    .param p3, "notificationHandler"    # Landroid/os/Handler;
    .param p4, "backgroundHandler"    # Landroid/os/Handler;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryLoader;->mLoadingCallbacks:Ljava/util/List;

    .line 27
    sget-object v0, Lcom/google/android/finsky/library/LibraryLoader$State;->UNINITIALIZED:Lcom/google/android/finsky/library/LibraryLoader$State;

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryLoader;->mState:Lcom/google/android/finsky/library/LibraryLoader$State;

    .line 40
    iput-object p1, p0, Lcom/google/android/finsky/library/LibraryLoader;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    .line 41
    iput-object p2, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    .line 42
    iput-object p4, p0, Lcom/google/android/finsky/library/LibraryLoader;->mBackgroundHandler:Landroid/os/Handler;

    .line 43
    iput-object p3, p0, Lcom/google/android/finsky/library/LibraryLoader;->mNotificationHandler:Landroid/os/Handler;

    .line 44
    return-void
.end method


# virtual methods
.method public declared-synchronized load(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryLoader;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryLoader;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/library/LibraryLoader$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/library/LibraryLoader$1;-><init>(Lcom/google/android/finsky/library/LibraryLoader;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method loadBlocking()V
    .locals 13

    .prologue
    .line 64
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mState:Lcom/google/android/finsky/library/LibraryLoader$State;

    sget-object v12, Lcom/google/android/finsky/library/LibraryLoader$State;->UNINITIALIZED:Lcom/google/android/finsky/library/LibraryLoader$State;

    if-ne v11, v12, :cond_4

    .line 65
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/AccountLibrary;->disableListeners()V

    .line 66
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/SQLiteLibrary;->reopen()V

    .line 67
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v11

    iget-object v0, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 68
    .local v0, "accountName":Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/SQLiteLibrary;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/library/LibraryEntry;

    .line 69
    .local v7, "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    invoke-virtual {v7}, Lcom/google/android/finsky/library/LibraryEntry;->getAccountName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 70
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11, v7}, Lcom/google/android/finsky/library/AccountLibrary;->add(Lcom/google/android/finsky/library/LibraryEntry;)V

    goto :goto_0

    .line 74
    .end local v7    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    :cond_1
    sget-object v1, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_IDS:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_3

    aget-object v8, v1, v5

    .line 75
    .local v8, "libraryId":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/finsky/utils/FinskyPreferences;->getLibraryServerToken(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v11

    invoke-virtual {v11, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 77
    .local v4, "encodedToken":Ljava/lang/String;
    if-eqz v4, :cond_2

    const/4 v11, 0x0

    invoke-static {v4, v11}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v10

    .line 79
    .local v10, "token":[B
    :goto_2
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11, v8, v10}, Lcom/google/android/finsky/library/AccountLibrary;->setServerToken(Ljava/lang/String;[B)V

    .line 74
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 77
    .end local v10    # "token":[B
    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    .line 82
    .end local v4    # "encodedToken":Ljava/lang/String;
    .end local v8    # "libraryId":Ljava/lang/String;
    :cond_3
    sget-object v11, Lcom/google/android/finsky/utils/FinskyPreferences;->autoAcquireTags:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v11, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 84
    .local v3, "autoAcquireTagsString":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "autoAcquireTags":[Ljava/lang/String;
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11, v2}, Lcom/google/android/finsky/library/AccountLibrary;->setAutoAcquireTags([Ljava/lang/String;)V

    .line 87
    sget-object v11, Lcom/google/android/finsky/library/LibraryLoader$State;->LOADED:Lcom/google/android/finsky/library/LibraryLoader$State;

    iput-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mState:Lcom/google/android/finsky/library/LibraryLoader$State;

    .line 88
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/SQLiteLibrary;->close()V

    .line 89
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v11}, Lcom/google/android/finsky/library/AccountLibrary;->enableListeners()V

    .line 91
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "autoAcquireTags":[Ljava/lang/String;
    .end local v3    # "autoAcquireTagsString":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_4
    monitor-enter p0

    .line 92
    :try_start_0
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Runnable;

    .line 93
    .local v9, "loadingCallback":Ljava/lang/Runnable;
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v11, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 96
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v9    # "loadingCallback":Ljava/lang/Runnable;
    :catchall_0
    move-exception v11

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 95
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_1
    iget-object v11, p0, Lcom/google/android/finsky/library/LibraryLoader;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 96
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    return-void
.end method
