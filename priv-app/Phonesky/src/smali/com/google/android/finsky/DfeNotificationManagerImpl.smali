.class public Lcom/google/android/finsky/DfeNotificationManagerImpl;
.super Ljava/lang/Object;
.source "DfeNotificationManagerImpl.java"

# interfaces
.implements Lcom/google/android/finsky/api/DfeNotificationManager;


# instance fields
.field private final mAccounts:Lcom/google/android/finsky/library/Accounts;

.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mContext:Landroid/content/Context;

.field private final mHandledNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private final mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

.field private final mNotifier:Lcom/google/android/finsky/utils/Notifier;

.field private final mPendingAcks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/library/Accounts;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p3, "notifier"    # Lcom/google/android/finsky/utils/Notifier;
    .param p4, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p5, "libraryReplicators"    # Lcom/google/android/finsky/library/LibraryReplicators;
    .param p6, "accounts"    # Lcom/google/android/finsky/library/Accounts;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mHandledNotifications:Ljava/util/List;

    .line 56
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    .line 67
    iput-object p2, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 68
    iput-object p3, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    .line 69
    iput-object p1, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mContext:Landroid/content/Context;

    .line 70
    iput-object p4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 71
    iput-object p5, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    .line 72
    iput-object p6, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    .line 74
    invoke-direct {p0}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->loadPendingAcks()V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/DfeNotificationManagerImpl;Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/DfeNotificationManagerImpl;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/DfeNotificationManagerImpl;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/DfeNotificationManagerImpl;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/DfeNotificationManagerImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/DfeNotificationManagerImpl;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->savePendingAcks()V

    return-void
.end method

.method private ackNotification(Ljava/lang/String;)V
    .locals 3
    .param p1, "notificationId"    # Ljava/lang/String;

    .prologue
    .line 331
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/DfeNotificationManagerImpl$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl$2;-><init>(Lcom/google/android/finsky/DfeNotificationManagerImpl;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/finsky/DfeNotificationManagerImpl$3;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl$3;-><init>(Lcom/google/android/finsky/DfeNotificationManagerImpl;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->ackNotification(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 345
    return-void
.end method

.method private ackPendingNotifications(Ljava/lang/String;)V
    .locals 3
    .param p1, "notificationId"    # Ljava/lang/String;

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->ackNotification(Ljava/lang/String;)V

    .line 319
    iget-object v2, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 320
    .local v1, "pendingAck":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 321
    invoke-direct {p0, v1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->ackNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 324
    .end local v1    # "pendingAck":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private static containsIabMutations(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;)Z
    .locals 9
    .param p0, "libraryUpdate"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 380
    .local v3, "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    iget-object v6, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v6}, Lcom/google/android/finsky/utils/DocUtils;->isInAppDocid(Lcom/google/android/finsky/protos/Common$Docid;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 381
    const-string v6, "Encountered IAB item in notification: %s."

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, v3, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v8, v8, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    aput-object v8, v7, v5

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 386
    .end local v3    # "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :goto_1
    return v4

    .line 379
    .restart local v3    # "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v3    # "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :cond_1
    move v4, v5

    .line 386
    goto :goto_1
.end method

.method private handleCheckPromoOffersNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 5
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    const/4 v4, 0x1

    .line 308
    const-string v0, "Received CheckPromoOffers notification for account %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->checkPromoOffers:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 311
    return-void
.end method

.method private handleInAppNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 3
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    iget-object v2, v2, Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;->inAppNotificationId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sendNotify(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 279
    return-void
.end method

.method private handleLibraryDirtyNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 8
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 282
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    if-nez v3, :cond_0

    .line 283
    const-string v3, "Received LibraryDirty notification without LibraryDirtyData: id=%s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 288
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_1

    .line 289
    const-string v3, "Received LibraryDirty notification for invalid account: id=%s, account=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 296
    :cond_1
    new-array v2, v7, [Ljava/lang/String;

    .line 297
    .local v2, "libraryIds":[Ljava/lang/String;
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;->libraryId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 298
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;->libraryId:Ljava/lang/String;

    aput-object v3, v2, v6

    .line 303
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notification-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v0, v2, v4, v5}, Lcom/google/android/finsky/library/LibraryReplicators;->replicateAccount(Landroid/accounts/Account;[Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_2
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    iget v1, v3, Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;->backend:I

    .line 301
    .local v1, "backend":I
    invoke-static {v1}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    goto :goto_1
.end method

.method private handleNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 9
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 101
    iget-object v3, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    .line 103
    .local v3, "notificationId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mHandledNotifications:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 104
    const-string v4, "Notification [%s] ignored, already handled."

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v3, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-direct {p0, v3}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->ackPendingNotifications(Ljava/lang/String;)V

    .line 167
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v4, "Handling notification type=[%s], id=[%s]"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v3, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v4, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v4, :cond_1

    .line 112
    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    .line 113
    .local v1, "accountName":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    invoke-interface {v4, v1}, Lcom/google/android/finsky/library/Accounts;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 114
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_3

    .line 115
    const-string v4, "Processing notification library update."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v2, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 121
    .local v2, "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    invoke-static {v2}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->containsIabMutations(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 122
    const-string v4, "Ignoring notification LibraryUpdate with IAB mutations."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v2    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_1
    :goto_1
    iget v4, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    packed-switch v4, :pswitch_data_0

    .line 154
    const-string v4, "Unhandled notification type [%s]"

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    :goto_2
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mHandledNotifications:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    :goto_3
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/16 v5, 0xa

    if-lt v4, v5, :cond_4

    .line 161
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 124
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountName":Ljava/lang/String;
    .restart local v2    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->makeReplicatorDebugTag(Lcom/google/android/finsky/protos/Notifications$Notification;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v0, v2, v5}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    goto :goto_1

    .line 128
    .end local v2    # "libraryUpdate":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_3
    const-string v4, "Could not process library update for unknown account."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 133
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handlePurchaseDeliveryNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 136
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handlePurchaseRemovalNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 139
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handlePurchaseDeclinedNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 142
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleUserNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 145
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleInAppNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 148
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleLibraryDirtyNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 151
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleCheckPromoOffersNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    goto :goto_2

    .line 163
    :cond_4
    iget-object v4, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-direct {p0}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->savePendingAcks()V

    .line 166
    invoke-direct {p0, v3}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->ackPendingNotifications(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private handlePurchaseDeclinedNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 8
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 217
    iget-object v0, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    iget v7, v0, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;->reason:I

    .line 218
    .local v7, "reason":I
    iget-object v0, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 220
    .local v2, "packageName":Ljava/lang/String;
    const-string v0, "Received PURCHASE_DECLINED tickle for %s reason=%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    const/4 v3, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 224
    return-void
.end method

.method private handlePurchaseDeliveryNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 17
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 227
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    move-object/from16 v16, v0

    .line 230
    .local v16, "notificationData":Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    if-nez v1, :cond_0

    .line 231
    const-string v1, "Ignoring PurchaseDeliveryNotification because AppData was null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    :goto_0
    return-void

    .line 233
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v1, :cond_1

    .line 234
    const-string v1, "Ignoring PurchaseDeliveryNotification because delivery data was null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 238
    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v3, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 240
    .local v3, "backendDocId":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    if-nez v1, :cond_2

    .line 241
    const-string v1, "Ignoring PurchaseDeliveryNotification with !server_initiated: pkg=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 246
    :cond_2
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    .line 249
    .local v11, "accountName":Ljava/lang/String;
    new-instance v7, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v7}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 250
    .local v7, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    move-object/from16 v0, v16

    iget v1, v0, Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;->versionCode:I

    iput v1, v7, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 251
    const/4 v1, 0x1

    iput-boolean v1, v7, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 252
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0xc9

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    move-object/from16 v0, v16

    iget v10, v0, Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;->versionCode:I

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "tickle"

    const/4 v15, 0x2

    move-object v9, v3

    invoke-interface/range {v8 .. v15}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method private handlePurchaseRemovalNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 11
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 175
    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v2, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 176
    .local v2, "packageName":Ljava/lang/String;
    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;->malicious:Z

    if-eqz v1, :cond_2

    move v7, v0

    .line 178
    .local v7, "isMalicious":Z
    :goto_0
    iget-object v9, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    .line 180
    .local v9, "title":Ljava/lang/String;
    const-string v1, "Removing package \'%s\'. Malicious=\'%s\'"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v4

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v5, v0

    invoke-static {v1, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v8

    .line 185
    .local v8, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    const/4 v6, 0x0

    .line 186
    .local v6, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    if-eqz v8, :cond_0

    .line 187
    new-instance v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .end local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-direct {v6}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 188
    .restart local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iget v1, v8, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v1, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 189
    iput-boolean v0, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 190
    iget-boolean v1, v8, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    iput-boolean v1, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 191
    iput-boolean v0, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 193
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0xca

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 197
    if-eqz v8, :cond_1

    .line 199
    if-eqz v7, :cond_3

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v0, v9, v2}, Lcom/google/android/finsky/utils/Notifier;->showMaliciousAssetRemovedMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_1
    :goto_1
    if-eqz v7, :cond_4

    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, v2}, Lcom/google/android/finsky/receivers/Installer;->uninstallPackagesByUid(Ljava/lang/String;)V

    .line 212
    :goto_2
    return-void

    .end local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .end local v7    # "isMalicious":Z
    .end local v8    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .end local v9    # "title":Ljava/lang/String;
    :cond_2
    move v7, v4

    .line 176
    goto :goto_0

    .line 202
    .restart local v6    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .restart local v7    # "isMalicious":Z
    .restart local v8    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .restart local v9    # "title":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v0, v9, v2}, Lcom/google/android/finsky/utils/Notifier;->showNormalAssetRemovedMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 210
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, v2}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private handleUserNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 5
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 271
    iget-object v0, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    .line 272
    .local v0, "notificationData":Lcom/google/android/finsky/protos/Notifications$UserNotificationData;
    iget-object v1, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/finsky/utils/Notifier;->showMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method private loadPendingAcks()V
    .locals 5

    .prologue
    .line 353
    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 354
    .local v1, "pendingAckList":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 355
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 356
    .local v2, "pendingAcks":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 357
    iget-object v3, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    aget-object v4, v2, v0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v3, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mHandledNotifications:Ljava/util/List;

    aget-object v4, v2, v0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 361
    .end local v0    # "i":I
    .end local v2    # "pendingAcks":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private makeReplicatorDebugTag(Lcom/google/android/finsky/protos/Notifications$Notification;)Ljava/lang/String;
    .locals 2
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notification (type=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "],id=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "])"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private savePendingAcks()V
    .locals 4

    .prologue
    .line 368
    iget-object v1, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 376
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 371
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    iget-object v2, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 373
    :cond_1
    const-string v1, ","

    iget-object v2, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mPendingAcks:Ljava/util/List;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 374
    .local v0, "pendingList":Ljava/lang/String;
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public processNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V
    .locals 2
    .param p1, "notification"    # Lcom/google/android/finsky/protos/Notifications$Notification;

    .prologue
    .line 87
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl;->handleNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    .line 98
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/DfeNotificationManagerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    new-instance v1, Lcom/google/android/finsky/DfeNotificationManagerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/DfeNotificationManagerImpl$1;-><init>(Lcom/google/android/finsky/DfeNotificationManagerImpl;Lcom/google/android/finsky/protos/Notifications$Notification;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
