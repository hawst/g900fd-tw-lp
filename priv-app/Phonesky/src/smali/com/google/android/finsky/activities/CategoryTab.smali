.class public Lcom/google/android/finsky/activities/CategoryTab;
.super Ljava/lang/Object;
.source "CategoryTab.java"

# interfaces
.implements Lcom/google/android/finsky/activities/ViewPagerTab;


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

.field private mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

.field private mCategoryView:Landroid/view/ViewGroup;

.field private final mContext:Landroid/content/Context;

.field private mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

.field private mIsCurrentlySelected:Z

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mListView:Landroid/widget/ListView;

.field private final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

.field private final mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field private final mTabMode:I

.field private final mTabTitle:Ljava/lang/String;

.field private final mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/api/model/DfeToc;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "inflater"    # Landroid/view/LayoutInflater;
    .param p5, "tabData"    # Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    .param p6, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p7, "tabMode"    # I

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 74
    iput-object p1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mContext:Landroid/content/Context;

    .line 75
    iput-object p2, p0, Lcom/google/android/finsky/activities/CategoryTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 76
    iput-object p3, p0, Lcom/google/android/finsky/activities/CategoryTab;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 77
    iput-object p4, p0, Lcom/google/android/finsky/activities/CategoryTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 78
    iput-object p6, p0, Lcom/google/android/finsky/activities/CategoryTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 79
    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabTitle:Ljava/lang/String;

    .line 80
    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 81
    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 82
    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->elementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    .line 83
    iput p7, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabMode:I

    .line 84
    return-void
.end method


# virtual methods
.method public getView(I)Landroid/view/View;
    .locals 11
    .param p1, "backendId"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04004d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    const v1, 0x7f0a0118

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 94
    new-instance v0, Lcom/google/android/finsky/adapters/CategoryAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategories:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    iget-object v3, p0, Lcom/google/android/finsky/activities/CategoryTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/activities/CategoryTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/activities/CategoryTab;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v7, p0, Lcom/google/android/finsky/activities/CategoryTab;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iget-object v8, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabTitle:Ljava/lang/String;

    iget v9, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabMode:I

    iget-object v10, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    move v4, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/finsky/adapters/CategoryAdapter;-><init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Browse$BrowseLink;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "CategoryTab.ListViewState"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v2, "CategoryTab.ListViewState"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/utils/ObjectMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    .line 109
    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    .line 110
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 0
    .param p1, "instanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 155
    if-eqz p1, :cond_0

    .line 156
    iput-object p1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 158
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "CategoryTab.ListViewState"

    iget-object v2, p0, Lcom/google/android/finsky/activities/CategoryTab;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method public setTabSelected(Z)V
    .locals 2
    .param p1, "isSelected"    # Z

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mIsCurrentlySelected:Z

    if-eq p1, v0, :cond_1

    .line 115
    if-eqz p1, :cond_2

    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressions(Landroid/view/ViewGroup;)V

    .line 131
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mIsCurrentlySelected:Z

    .line 133
    :cond_1
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    goto :goto_0
.end method
