.class public final Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
.super Lcom/google/protobuf/nano/MessageNano;
.source "SearchSuggest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/SearchSuggest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Suggestion"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;


# instance fields
.field public hasServerLogsCookie:Z

.field public hasSuggestedQuery:Z

.field public hasType:Z

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

.field public serverLogsCookie:[B

.field public suggestedQuery:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->clear()Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    .line 248
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    .locals 2

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->_emptyArray:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    if-nez v0, :cond_1

    .line 218
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 220
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->_emptyArray:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    if-nez v0, :cond_0

    .line 221
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    sput-object v0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->_emptyArray:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    .line 223
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->_emptyArray:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    return-object v0

    .line 223
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 251
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    .line 252
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasType:Z

    .line 253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    .line 254
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasSuggestedQuery:Z

    .line 255
    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    .line 256
    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 257
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    .line 258
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasServerLogsCookie:Z

    .line 259
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->cachedSize:I

    .line 260
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 286
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 287
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasType:Z

    if-eqz v1, :cond_1

    .line 288
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasSuggestedQuery:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 292
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    if-eqz v1, :cond_4

    .line 296
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasServerLogsCookie:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    .line 300
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_7

    .line 304
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 316
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 320
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 321
    :sswitch_0
    return-object p0

    .line 326
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 327
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 331
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    .line 332
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasType:Z

    goto :goto_0

    .line 338
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    .line 339
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasSuggestedQuery:Z

    goto :goto_0

    .line 343
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    if-nez v2, :cond_1

    .line 344
    new-instance v2, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    .line 346
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 350
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    .line 351
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasServerLogsCookie:Z

    goto :goto_0

    .line 355
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v2, :cond_2

    .line 356
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 358
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 316
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 327
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 266
    iget v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasType:Z

    if-eqz v0, :cond_1

    .line 267
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 269
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasSuggestedQuery:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 270
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    if-eqz v0, :cond_4

    .line 273
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 275
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->hasServerLogsCookie:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    .line 276
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 278
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_7

    .line 279
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 281
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 282
    return-void
.end method
