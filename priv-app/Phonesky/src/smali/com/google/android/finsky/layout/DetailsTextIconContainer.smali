.class public Lcom/google/android/finsky/layout/DetailsTextIconContainer;
.super Landroid/view/ViewGroup;
.source "DetailsTextIconContainer.java"


# instance fields
.field private final mChildGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->mChildGap:I

    .line 39
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 21
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 116
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getWidth()I

    move-result v17

    .line 117
    .local v17, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getHeight()I

    move-result v8

    .line 118
    .local v8, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildCount()I

    move-result v4

    .line 119
    .local v4, "childCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingLeft()I

    move-result v12

    .line 120
    .local v12, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingRight()I

    move-result v13

    .line 124
    .local v13, "paddingRight":I
    const/4 v15, 0x0

    .line 125
    .local v15, "visibleChildCount":I
    const/16 v16, 0x0

    .line 126
    .local v16, "visibleChildTotalWidth":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v4, :cond_1

    .line 127
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 128
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v19

    if-nez v19, :cond_0

    .line 129
    add-int/lit8 v15, v15, 0x1

    .line 130
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v19

    add-int v16, v16, v19

    .line 126
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 134
    .end local v3    # "child":Landroid/view/View;
    :cond_1
    const/16 v19, 0x1

    move/from16 v0, v19

    if-le v15, v0, :cond_2

    .line 135
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->mChildGap:I

    move/from16 v19, v0

    add-int/lit8 v20, v15, -0x1

    mul-int v19, v19, v20

    add-int v16, v16, v19

    .line 139
    :cond_2
    sub-int v19, v17, v16

    sub-int v19, v19, v12

    sub-int v19, v19, v13

    div-int/lit8 v19, v19, 0x2

    add-int v18, v12, v19

    .line 140
    .local v18, "x":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingTop()I

    move-result v14

    .line 141
    .local v14, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingBottom()I

    move-result v11

    .line 142
    .local v11, "paddingBottom":I
    sub-int v19, v8, v14

    sub-int v9, v19, v11

    .line 143
    .local v9, "heightForContent":I
    const/4 v10, 0x0

    :goto_1
    if-ge v10, v4, :cond_4

    .line 144
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 145
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v19

    if-nez v19, :cond_3

    .line 146
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 147
    .local v6, "childWidth":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 148
    .local v5, "childHeight":I
    sub-int v19, v9, v5

    div-int/lit8 v19, v19, 0x2

    add-int v7, v14, v19

    .line 149
    .local v7, "childY":I
    add-int v19, v18, v6

    add-int v20, v7, v5

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v3, v0, v7, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 150
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->mChildGap:I

    move/from16 v19, v0

    add-int v19, v19, v6

    add-int v18, v18, v19

    .line 143
    .end local v5    # "childHeight":I
    .end local v6    # "childWidth":I
    .end local v7    # "childY":I
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 153
    .end local v3    # "child":Landroid/view/View;
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 85
    const/4 v9, 0x0

    .line 86
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingLeft()I

    move-result v6

    .line 87
    .local v6, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingRight()I

    move-result v7

    .line 88
    .local v7, "paddingRight":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    int-to-float v10, v10

    const/high16 v11, 0x3f400000    # 0.75f

    mul-float/2addr v10, v11

    float-to-int v10, v10

    sub-int/2addr v10, v6

    sub-int v5, v10, v7

    .line 90
    .local v5, "maxContentWidth":I
    const/4 v3, 0x0

    .line 93
    .local v3, "height":I
    move v8, v5

    .line 94
    .local v8, "spaceLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildCount()I

    move-result v1

    .line 95
    .local v1, "childCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_1

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 97
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 98
    .local v2, "childLp":Landroid/view/ViewGroup$LayoutParams;
    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gt v10, v8, :cond_0

    .line 99
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v10, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    iget v11, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v0, v10, v11}, Landroid/view/View;->measure(II)V

    .line 102
    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v11, p0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->mChildGap:I

    add-int/2addr v10, v11

    sub-int/2addr v8, v10

    .line 103
    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v11, p0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->mChildGap:I

    add-int/2addr v10, v11

    add-int/2addr v9, v10

    .line 104
    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 95
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 106
    :cond_0
    const/16 v10, 0x8

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 110
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childLp":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    add-int v10, v9, v6

    add-int/2addr v10, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingTop()I

    move-result v11

    add-int/2addr v11, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getPaddingBottom()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->setMeasuredDimension(II)V

    .line 112
    return-void
.end method

.method public populate(Ljava/util/List;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 1
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/play/image/BitmapLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "imagePairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->populate(Ljava/util/List;Lcom/google/android/play/image/BitmapLoader;I)V

    .line 43
    return-void
.end method

.method public populate(Ljava/util/List;Lcom/google/android/play/image/BitmapLoader;I)V
    .locals 11
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "color"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/play/image/BitmapLoader;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p1, "imagePairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;>;"
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 47
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 48
    :cond_0
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->setVisibility(I)V

    .line 81
    :cond_1
    return-void

    .line 52
    :cond_2
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->setVisibility(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 56
    .local v6, "inflater":Landroid/view/LayoutInflater;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildCount()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 57
    .local v5, "imagesToCreate":I
    :goto_0
    if-lez v5, :cond_3

    .line 58
    const v7, 0x7f04007f

    invoke-virtual {v6, v7, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/FifeImageView;

    .line 60
    .local v4, "imageView":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->addView(Landroid/view/View;)V

    .line 61
    add-int/lit8 v5, v5, -0x1

    .line 62
    goto :goto_0

    .line 65
    .end local v4    # "imageView":Lcom/google/android/play/image/FifeImageView;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildCount()I

    move-result v0

    .line 66
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 67
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/FifeImageView;

    .line 68
    .restart local v4    # "imageView":Lcom/google/android/play/image/FifeImageView;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_5

    .line 69
    invoke-virtual {v4, v9}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 70
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 71
    .local v3, "imagePair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;"
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/finsky/protos/Common$Image;

    .line 72
    .local v2, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v7, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v8, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v4, v7, v8, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 73
    iget-object v7, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v4, v7}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 74
    if-eqz p3, :cond_4

    .line 75
    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, p3, v7}, Lcom/google/android/play/image/FifeImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 66
    .end local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v3    # "imagePair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;"
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 78
    :cond_5
    invoke-virtual {v4, v10}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_2
.end method
