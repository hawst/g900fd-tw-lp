.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryAppDetails"
.end annotation


# instance fields
.field public certificateHash:[Ljava/lang/String;

.field public hasPostDeliveryRefundWindowMsec:Z

.field public hasRefundTimeoutTimestampMsec:Z

.field public postDeliveryRefundWindowMsec:J

.field public refundTimeoutTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 511
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    .line 512
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 515
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    .line 516
    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    .line 517
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasRefundTimeoutTimestampMsec:Z

    .line 518
    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    .line 519
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasPostDeliveryRefundWindowMsec:Z

    .line 520
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->cachedSize:I

    .line 521
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 546
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 547
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 548
    const/4 v0, 0x0

    .line 549
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 550
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 551
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 552
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 553
    add-int/lit8 v0, v0, 0x1

    .line 554
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 550
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 558
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 559
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 561
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasRefundTimeoutTimestampMsec:Z

    if-nez v5, :cond_3

    iget-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    .line 562
    :cond_3
    const/4 v5, 0x3

    iget-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 565
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v5, :cond_5

    iget-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    .line 566
    :cond_5
    const/4 v5, 0x4

    iget-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 569
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 578
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 582
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 583
    :sswitch_0
    return-object p0

    .line 588
    :sswitch_1
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 590
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 591
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 592
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 593
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 595
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 596
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 597
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 595
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 590
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 600
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 601
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    goto :goto_0

    .line 605
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    .line 606
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasRefundTimeoutTimestampMsec:Z

    goto :goto_0

    .line 610
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    .line 611
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasPostDeliveryRefundWindowMsec:Z

    goto :goto_0

    .line 578
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 482
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 527
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 528
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 529
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 530
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 531
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 528
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 535
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasRefundTimeoutTimestampMsec:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 536
    :cond_2
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 538
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_5

    .line 539
    :cond_4
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 541
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 542
    return-void
.end method
