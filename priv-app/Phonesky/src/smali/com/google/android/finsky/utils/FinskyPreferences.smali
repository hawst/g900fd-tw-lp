.class public Lcom/google/android/finsky/utils/FinskyPreferences;
.super Ljava/lang/Object;
.source "FinskyPreferences.java"


# static fields
.field public static AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final acceptedPlusReviews:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final acceptedTosToken:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final accountHasFop:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final accountHasFopLastUpdateMs:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoAcquireTags:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateFirstTimeForAccounts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateMigrationDialogShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final checkPromoOffers:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final contentPin:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final currentAccount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneHoldoffComplete:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final deviceConfigToken:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final directedRestoreStarted:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadManagerUsesFroyoStrings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final earlyUpdateSkipPackage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gcmRegistrationIdOnServer:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final hadPreJsAutoUpdateSettings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final hasAcceptedAutoUpdateMigrationDialog:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final installationReplicationRetries:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final isGaiaAuthOptedOut:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final lastAutomaticHeroSequenceOnDetailsTimeShown:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lastDeviceFeatureLoggedTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lastGaiaAuthTimestamp:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lastUpdateMigrationDialogTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final libraryWidgetData:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetBottomAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetLeftAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetRightAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetTopAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupConfigurationId:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupHoldoffAfterInstall:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionLastTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playGamesInstallSuggestionShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopAddedFop:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopLastSnoozedTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopNumDialogImpressions:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopNumFopSelectorImpressions:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final promptForFopNumSnoozed:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final purchaseAuthVersionCode:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final replicatedAccountAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final replicatedSystemAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

.field public static final setupWizardStartDownloads:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final successfulUpdateNotificationAppList:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final verifyInstalledPackagesLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final versionCode:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final warmWelcomeOwnProfileAcknowledged:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final widgetUrlsByBackend:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final wifiAutoUpdateFailedAttempts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final wifiAutoUpdateFirstFailMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, -0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 16
    new-instance v0, Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "finsky"

    invoke-direct {v0, v2, v4}, Lcom/google/android/finsky/config/PreferenceFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    .line 18
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last_version_code"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->versionCode:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 21
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "content-filter-level"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 24
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "account"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->currentAccount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 27
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "pin_code"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->contentPin:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 30
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "had-pre-js-auto-update-settings"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hadPreJsAutoUpdateSettings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 33
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "update-migration-dialog-accepted"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasAcceptedAutoUpdateMigrationDialog:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 36
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "update-migration-dialog-times-shown-count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateMigrationDialogShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 39
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last-update-migration-dialog-times-shown"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateMigrationDialogTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 42
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last-device-feature-logged-timestamp-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastDeviceFeatureLoggedTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 45
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "play-games-install-suggestion-shown-count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 48
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "play-games-install-suggestion-last-time-shown"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionLastTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 55
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "device-config-token"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->deviceConfigToken:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 61
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "gcm-registration-id-on-server"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->gcmRegistrationIdOnServer:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 67
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "dailyhygiene-failed"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 73
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "dailyhygiene-lastsuccess"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 79
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "dailyhygiene-holdoff-complete"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneHoldoffComplete:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 86
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "auto_update_enabled"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 93
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "update_over_wifi_only"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 99
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "installation-replication-retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->installationReplicationRetries:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 103
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "dfe-notification-pending-acks"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dfeNotificationPendingAcks:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 107
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last-replicated-database-version"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 113
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "replicated-system-apps-hash"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->replicatedSystemAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 119
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last-tos-token"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedTosToken:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 125
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "accepted-anti-malware-consent"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 131
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "verify-installed-apps-last-success-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->verifyInstalledPackagesLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 137
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "replicated-account-apps-hash:"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->replicatedAccountAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 143
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "check-promo-offers"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->checkPromoOffers:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 149
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "has-fop"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->accountHasFop:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 155
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "has-fop-last-update-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->accountHasFopLastUpdateMs:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 161
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "prompt-for-fop-num-snoozed"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->promptForFopNumSnoozed:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 168
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "prompt-for-fop-last-snooze-timestamp-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->promptForFopLastSnoozedTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 174
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "prompt-for-fop-num-dialog-impressions"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->promptForFopNumDialogImpressions:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 180
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "prompt-for-fop-num-fop-selector-impressions"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->promptForFopNumFopSelectorImpressions:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 186
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "prompt-for-fop-fop-added"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->promptForFopAddedFop:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 192
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "last-gaia-auth-timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastGaiaAuthTimestamp:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 200
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "gaia-auth-opt-out"

    move-object v0, v1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->isGaiaAuthOptedOut:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 209
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "purchase-auth-type"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthType:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 218
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "purchase-auth-version-code"

    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->purchaseAuthVersionCode:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 224
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "experiment-list"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 241
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "autoAcquireTags"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->autoAcquireTags:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 245
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "my-library-widget-top-affinities"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->myLibraryWidgetTopAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 247
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "my-library-widget-bottom-affinities"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->myLibraryWidgetBottomAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 249
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "my-library-widget-left-affinities"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->myLibraryWidgetLeftAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 251
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "my-library-widget-right-affinities"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->myLibraryWidgetRightAffinity:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 260
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "download-manager-uses-froyo-strings"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->downloadManagerUsesFroyoStrings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 266
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "accepted-plus-reviews"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedPlusReviews:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 276
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "nlp-cleanup-config-id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupConfigurationId:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 280
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "nlp-cleanup-config-holdoff-until-boot"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 284
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "nlp-cleanup-config-holdoff-after-install"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffAfterInstall:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 287
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v3, "widgetUrl"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->widgetUrlsByBackend:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 293
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v2, "libraryWidgetData"

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->libraryWidgetData:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 300
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "successful-update-notification-app-list"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->successfulUpdateNotificationAppList:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 306
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "last-update-avail-notification-timestamp-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 313
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "wifi-auto-update-failed-attempts"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFailedAttempts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 320
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "wifi-auto-update-first-fail-ms"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFirstFailMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 327
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "warm-welcome-own-profile-acknowledged"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->warmWelcomeOwnProfileAcknowledged:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 333
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "internal-fake-item-rater-enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 342
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "auto-update-first-time-for-accounts"

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateFirstTimeForAccounts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 349
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "has-seen-purchase-session-message"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPurchaseSessionMessage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 356
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "has-seen-permission-buckets-learn-more"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 363
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "last-automatic-hero-sequence-on-details-time-shown"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->lastAutomaticHeroSequenceOnDetailsTimeShown:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 372
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "directed-restore-started"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->directedRestoreStarted:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 379
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "early-update-skip-package"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->earlyUpdateSkipPackage:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 387
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    const-string v1, "setup-wizard-start-downloads"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->setupWizardStartDownloads:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    return-void
.end method

.method public static getLibraryServerToken(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;
    .locals 3
    .param p0, "libraryId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "server_token:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/config/PreferenceFile;->prefixPreference(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v0

    return-object v0
.end method

.method public static getPreferencesFile()Lcom/google/android/finsky/config/PreferenceFile;
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->sPrefs:Lcom/google/android/finsky/config/PreferenceFile;

    return-object v0
.end method
