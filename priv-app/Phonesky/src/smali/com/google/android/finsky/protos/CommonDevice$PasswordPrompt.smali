.class public final Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PasswordPrompt"
.end annotation


# instance fields
.field public forgotPasswordUrl:Ljava/lang/String;

.field public hasForgotPasswordUrl:Z

.field public hasPrompt:Z

.field public prompt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2567
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2568
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->clear()Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    .line 2569
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2572
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    .line 2573
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasPrompt:Z

    .line 2574
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    .line 2575
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasForgotPasswordUrl:Z

    .line 2576
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->cachedSize:I

    .line 2577
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2594
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2595
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasPrompt:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2596
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2599
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasForgotPasswordUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2600
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2603
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2611
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2612
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2616
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2617
    :sswitch_0
    return-object p0

    .line 2622
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    .line 2623
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasPrompt:Z

    goto :goto_0

    .line 2627
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    .line 2628
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasForgotPasswordUrl:Z

    goto :goto_0

    .line 2612
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2542
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2583
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasPrompt:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2584
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->prompt:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2586
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->hasForgotPasswordUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2587
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;->forgotPasswordUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2589
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2590
    return-void
.end method
