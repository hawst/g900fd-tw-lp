.class Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
.source "DetailsScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

.field final synthetic val$imageHeight:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;I)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iput p2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->val$imageHeight:I

    invoke-direct {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 124
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->lastAutomaticHeroSequenceOnDetailsTimeShown:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDocBackend:I
    invoke-static {v2}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$200(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->getScrollY()I

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    const-wide/16 v2, 0x5dc

    const-wide/16 v4, 0xc8

    iget v6, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->val$imageHeight:I

    iget-object v7, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDefaultHeroImageHeight:I
    invoke-static {v7}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$000(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I

    move-result v7

    sub-int/2addr v6, v7

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getVerticalScrollByAnimation(Landroid/widget/ScrollView;JJILandroid/view/animation/Animation$AnimationListener;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v1

    # setter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    invoke-static {v8, v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$302(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    .line 138
    new-instance v0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;-><init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;)V

    .line 145
    .local v0, "collapserGuard":Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->addOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 149
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mParent:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$500(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    invoke-static {v2}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$300(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
