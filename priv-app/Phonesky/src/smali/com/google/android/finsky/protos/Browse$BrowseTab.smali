.class public final Lcom/google/android/finsky/protos/Browse$BrowseTab;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrowseTab"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseTab;


# instance fields
.field public category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

.field public hasListUrl:Z

.field public hasServerLogsCookie:Z

.field public hasTitle:Z

.field public listUrl:Ljava/lang/String;

.field public serverLogsCookie:[B

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 433
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseTab;->clear()Lcom/google/android/finsky/protos/Browse$BrowseTab;

    .line 434
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .locals 2

    .prologue
    .line 406
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    if-nez v0, :cond_1

    .line 407
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 409
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    if-nez v0, :cond_0

    .line 410
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Browse$BrowseTab;

    sput-object v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    .line 412
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    return-object v0

    .line 412
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 437
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    .line 438
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasTitle:Z

    .line 439
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    .line 440
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasServerLogsCookie:Z

    .line 441
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    .line 442
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasListUrl:Z

    .line 443
    invoke-static {}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 444
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->cachedSize:I

    .line 445
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 473
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 474
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 475
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 478
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasServerLogsCookie:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_3

    .line 479
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 482
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasListUrl:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 483
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 486
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 487
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 488
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v3, v1

    .line 489
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_6

    .line 490
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 487
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 495
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 503
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 504
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 508
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 509
    :sswitch_0
    return-object p0

    .line 514
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    .line 515
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasTitle:Z

    goto :goto_0

    .line 519
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    .line 520
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasServerLogsCookie:Z

    goto :goto_0

    .line 524
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    .line 525
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasListUrl:Z

    goto :goto_0

    .line 529
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 531
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-nez v5, :cond_2

    move v1, v4

    .line 532
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 534
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v1, :cond_1

    .line 535
    iget-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 537
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 538
    new-instance v5, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v5, v2, v1

    .line 539
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 540
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 537
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 531
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v1, v5

    goto :goto_1

    .line 543
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v5, v2, v1

    .line 544
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 545
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto :goto_0

    .line 504
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$BrowseTab;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseTab;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 451
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 452
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 454
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasServerLogsCookie:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 455
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 457
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->hasListUrl:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 458
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->listUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 460
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 461
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 462
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseTab;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v2, v1

    .line 463
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_6

    .line 464
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 461
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 468
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 469
    return-void
.end method
