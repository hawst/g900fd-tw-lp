.class public Lcom/google/android/finsky/fragments/DeepLinkShimFragment;
.super Lcom/google/android/finsky/fragments/UrlBasedPageFragment;
.source "DeepLinkShimFragment.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/UrlBasedPageFragment;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;",
        ">;"
    }
.end annotation


# instance fields
.field private mReferringPackage:Ljava/lang/String;

.field private mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method protected static findInstallAccount([Landroid/accounts/Account;Landroid/net/Uri;)Ljava/lang/String;
    .locals 7
    .param p0, "accounts"    # [Landroid/accounts/Account;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 441
    const-string v6, "ah"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 442
    .local v1, "accountHash":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-object v5

    .line 445
    :cond_1
    move-object v2, p0

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    .line 446
    .local v0, "account":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 447
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    .line 445
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static getContinueUrl(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 427
    const-string v1, "url"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 429
    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 431
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getExternalReferrer(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 414
    const-string v2, "referrer"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 415
    .local v0, "externalReferrer":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 417
    const-string v2, "gclid"

    invoke-virtual {p0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 418
    .local v1, "gclid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 419
    const/4 v2, 0x0

    .line 423
    .end local v1    # "gclid":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 421
    .restart local v1    # "gclid":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gclid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .end local v1    # "gclid":Ljava/lang/String;
    :cond_1
    move-object v2, v0

    .line 423
    goto :goto_0
.end method

.method private static logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p0, "backgroundEventType"    # I
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installedVersion"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 396
    new-instance v2, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    .line 399
    .local v1, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    if-ltz p2, :cond_0

    .line 400
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 401
    .local v0, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iput p2, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 402
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 403
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAppData(Lcom/google/android/finsky/analytics/PlayStore$AppData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 405
    .end local v0    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 406
    return-void
.end method

.method public static newInstance(Landroid/net/Uri;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "referringPackage"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v1, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;-><init>()V

    .line 90
    .local v1, "fragment":Lcom/google/android/finsky/fragments/DeepLinkShimFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 91
    .local v0, "accounts":[Landroid/accounts/Account;
    invoke-static {v0, p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->findInstallAccount([Landroid/accounts/Account;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "overrideAccount":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 93
    invoke-virtual {v1, v2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->setDfeAccount(Ljava/lang/String;)V

    .line 94
    const-string v3, "DeepLinkShimFragment.overrideAccount"

    invoke-virtual {v1, v3, v2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 97
    iput-object p1, v1, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mReferringPackage:Ljava/lang/String;

    .line 98
    return-object v1
.end method

.method public static saveExternalReferrer(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;)V
    .locals 4
    .param p0, "externalReferrer"    # Ljava/lang/String;
    .param p1, "docId"    # Lcom/google/android/finsky/protos/Common$Docid;

    .prologue
    .line 279
    iget v2, p1, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    iget v2, p1, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 288
    iget-object v1, p1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 289
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    .line 290
    .local v0, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    new-instance v2, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;

    invoke-direct {v2, v0, v1, p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;-><init>(Lcom/google/android/finsky/appstate/AppStates;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private static saveExternalReferrerForDocId(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p0, "incomingUri"    # Landroid/net/Uri;
    .param p1, "doc"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    .line 267
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/google/android/finsky/utils/DocUtils;->docidToBackend(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 268
    const/4 v1, 0x1

    invoke-static {v2, v1, p1}, Lcom/google/android/finsky/utils/DocUtils;->createDocid(IILjava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    .line 269
    .local v0, "id":Lcom/google/android/finsky/protos/Common$Docid;
    invoke-static {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getExternalReferrer(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrer(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;)V

    .line 271
    .end local v0    # "id":Lcom/google/android/finsky/protos/Common$Docid;
    :cond_0
    return-void
.end method

.method private static saveExternalReferrerForUrl(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p0, "incomingUri"    # Landroid/net/Uri;
    .param p1, "detailsUrl"    # Ljava/lang/String;

    .prologue
    .line 254
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 255
    .local v0, "detailsUri":Landroid/net/Uri;
    const-string v2, "doc"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 256
    .local v1, "doc":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrerForDocId(Landroid/net/Uri;Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method static saveReferrer(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates$AppState;Lcom/google/android/finsky/receivers/Installer$InstallerState;Lcom/google/android/finsky/appstate/InstallerDataStore;)Ljava/lang/String;
    .locals 20
    .param p0, "externalReferrer"    # Ljava/lang/String;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;
    .param p3, "installerState"    # Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .param p4, "installerDataStore"    # Lcom/google/android/finsky/appstate/InstallerDataStore;

    .prologue
    .line 335
    const/4 v6, 0x0

    .line 336
    .local v6, "erase":Z
    const/4 v5, 0x0

    .line 339
    .local v5, "dropReason":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v9

    .line 341
    .local v9, "isInstalling":Z
    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v15, :cond_2

    const/4 v8, 0x1

    .line 342
    .local v8, "isInstalled":Z
    :goto_0
    if-nez v9, :cond_3

    if-nez v8, :cond_3

    .line 345
    const/4 v4, 0x1

    .line 377
    .local v4, "capture":Z
    :goto_1
    if-eqz v6, :cond_0

    .line 379
    const/4 v15, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v15}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrer(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-wide/16 v16, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-wide/from16 v2, v16

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrerTimestampMs(Ljava/lang/String;J)V

    .line 382
    :cond_0
    if-eqz v4, :cond_1

    .line 384
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrer(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-wide/from16 v2, v16

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrerTimestampMs(Ljava/lang/String;J)V

    .line 388
    :cond_1
    return-object v5

    .line 341
    .end local v4    # "capture":Z
    .end local v8    # "isInstalled":Z
    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    .line 349
    .restart local v8    # "isInstalled":Z
    :cond_3
    const/4 v14, 0x0

    .line 350
    .local v14, "storedReferrer":Ljava/lang/String;
    if-eqz p2, :cond_4

    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v15, :cond_4

    .line 351
    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 352
    .local v7, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrer()Ljava/lang/String;

    move-result-object v14

    .line 354
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 355
    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrerTimestampMs()J

    move-result-wide v12

    .line 356
    .local v12, "referrerTimestamp":J
    sget-object v15, Lcom/google/android/finsky/config/G;->externalReferrerLifespanMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 357
    .local v10, "lifespan":J
    const-wide/16 v16, 0x0

    cmp-long v15, v10, v16

    if-lez v15, :cond_4

    add-long v16, v12, v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    cmp-long v15, v16, v18

    if-gez v15, :cond_4

    .line 359
    const/4 v14, 0x0

    .line 360
    const/4 v6, 0x1

    .line 364
    .end local v7    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .end local v10    # "lifespan":J
    .end local v12    # "referrerTimestamp":J
    :cond_4
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 365
    const/4 v4, 0x0

    .line 366
    .restart local v4    # "capture":Z
    const-string v5, "already-installed"

    goto :goto_1

    .line 367
    .end local v4    # "capture":Z
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 369
    const/4 v4, 0x1

    .restart local v4    # "capture":Z
    goto :goto_1

    .line 372
    .end local v4    # "capture":Z
    :cond_6
    const/4 v4, 0x0

    .line 373
    .restart local v4    # "capture":Z
    const-string v5, "already-captured"

    goto :goto_1
.end method


# virtual methods
.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUri:Landroid/net/Uri;

    .line 105
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->requestData()V

    .line 106
    return-void
.end method

.method public onDataChanged()V
    .locals 15

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->canChangeFragmentManagerState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->popBackStack()V

    .line 164
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v11

    .line 166
    .local v11, "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v14, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    .line 168
    .local v14, "serverLogsCookie":[B
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v3, v0, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrerForUrl(Landroid/net/Uri;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getContinueUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "DeepLinkShimFragment.overrideAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v4, v0, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v5, v0, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget v4, v4, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSearch(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 182
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v0, v1, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToMyWishlist()V

    goto/16 :goto_0

    .line 184
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 185
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v0, v1, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 187
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToMyAccount()V

    goto/16 :goto_0

    .line 188
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    if-eqz v0, :cond_7

    .line 190
    const-string v0, "Direct purchase deprecated."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v9, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    .line 192
    .local v9, "directPurchase":Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v0, v1, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->purchaseDocid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrerForDocId(Landroid/net/Uri;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, v9, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getContinueUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "DeepLinkShimFragment.overrideAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    .end local v9    # "directPurchase":Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 198
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v0, v1, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-eqz v0, :cond_b

    .line 202
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v0, v1, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 205
    const/4 v12, 0x0

    .line 206
    .local v12, "redeemCodePrefill":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v12, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 209
    :cond_9
    const/4 v13, 0x0

    .line 210
    .local v13, "redeemPartnerPayload":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 211
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v13, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 213
    :cond_a
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v12, v13}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 218
    .local v10, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->startActivity(Landroid/content/Intent;)V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    .line 221
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v12    # "redeemCodePrefill":Ljava/lang/String;
    .end local v13    # "redeemPartnerPayload":Ljava/lang/String;
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v1, v0, v2, v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 223
    new-instance v8, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    .local v8, "browse":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 225
    const/high16 v0, 0x10000000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 226
    const-string v0, "dont_resolve_again"

    invoke-virtual {v8, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 230
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v8, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 232
    .local v6, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_d

    .line 233
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 234
    .local v7, "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 236
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 239
    :cond_c
    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    .end local v7    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :cond_d
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 142
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 136
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->onDataChanged()V

    .line 137
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 62
    check-cast p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->onResponse(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)V

    return-void
.end method

.method protected rebindViews()V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method protected requestData()V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->mReferringPackage:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->resolveLink(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->switchToLoading()V

    .line 131
    return-void
.end method
