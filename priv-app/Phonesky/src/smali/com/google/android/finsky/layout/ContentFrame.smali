.class public Lcom/google/android/finsky/layout/ContentFrame;
.super Landroid/widget/FrameLayout;
.source "ContentFrame.java"


# instance fields
.field private mDataLayout:Landroid/view/ViewGroup;

.field private final mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/layout/ContentFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/ContentFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    iput-object v4, p0, Lcom/google/android/finsky/layout/ContentFrame;->mInflater:Landroid/view/LayoutInflater;

    .line 59
    iget-object v4, p0, Lcom/google/android/finsky/layout/ContentFrame;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f040106

    invoke-virtual {v4, v5, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 60
    .local v1, "inflated":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 61
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ContentFrame;->addView(Landroid/view/View;)V

    .line 63
    iget-object v4, p0, Lcom/google/android/finsky/layout/ContentFrame;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f040105

    invoke-virtual {v4, v5, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 64
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 65
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ContentFrame;->addView(Landroid/view/View;)V

    .line 68
    sget-object v4, Lcom/android/vending/R$styleable;->ContentFrame:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 70
    .local v3, "resource":I
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 71
    .local v2, "resId":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    if-eqz v3, :cond_0

    .line 73
    invoke-virtual {p0, v3, v2}, Lcom/google/android/finsky/layout/ContentFrame;->inflateDataLayout(II)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/ContentFrame;->addView(Landroid/view/View;)V

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public inflateDataLayout(II)Landroid/view/ViewGroup;
    .locals 1
    .param p1, "resource"    # I
    .param p2, "resId"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/layout/ContentFrame;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/finsky/layout/ContentFrame;->inflateDataLayout(Landroid/view/LayoutInflater;II)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public inflateDataLayout(Landroid/view/LayoutInflater;II)Landroid/view/ViewGroup;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "resource"    # I
    .param p3, "resId"    # I

    .prologue
    .line 89
    if-nez p2, :cond_0

    .line 90
    const/4 v0, 0x0

    .line 97
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, p2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ContentFrame;->mDataLayout:Landroid/view/ViewGroup;

    .line 94
    if-eqz p3, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/layout/ContentFrame;->mDataLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->setId(I)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/ContentFrame;->mDataLayout:Landroid/view/ViewGroup;

    goto :goto_0
.end method
