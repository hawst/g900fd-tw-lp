.class final Lcom/google/android/finsky/billing/IabParameters$1;
.super Ljava/lang/Object;
.source "IabParameters.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/IabParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/finsky/billing/IabParameters;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/finsky/billing/IabParameters;
    .locals 8
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 22
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 23
    .local v3, "packageVersionCode":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 24
    .local v4, "packageSignatureHash":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-lez v0, :cond_1

    const/4 v7, 0x1

    .line 25
    .local v7, "hasDeveloperPayload":Z
    :goto_0
    const/4 v5, 0x0

    .line 26
    .local v5, "developerPayload":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 29
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 30
    .local v1, "apiVersion":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v6, "oldSkus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 32
    new-instance v0, Lcom/google/android/finsky/billing/IabParameters;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/IabParameters;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0

    .line 24
    .end local v1    # "apiVersion":I
    .end local v5    # "developerPayload":Ljava/lang/String;
    .end local v6    # "oldSkus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "hasDeveloperPayload":Z
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/IabParameters$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/finsky/billing/IabParameters;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/finsky/billing/IabParameters;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 38
    new-array v0, p1, [Lcom/google/android/finsky/billing/IabParameters;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/IabParameters$1;->newArray(I)[Lcom/google/android/finsky/billing/IabParameters;

    move-result-object v0

    return-object v0
.end method
