.class Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;
.super Ljava/lang/Object;
.source "MultiStepFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->access$300(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    iget-object v1, v1, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onContinueButtonClicked()V

    .line 164
    :cond_0
    return-void
.end method
