.class public final Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateInstrumentRequest"
.end annotation


# instance fields
.field public flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

.field public profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

.field public usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 503
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->clear()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;

    .line 504
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 507
    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 508
    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    .line 509
    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    .line 510
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->cachedSize:I

    .line 511
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 531
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 532
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v1, :cond_0

    .line 533
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 540
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    if-eqz v1, :cond_2

    .line 541
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 553
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 557
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 558
    :sswitch_0
    return-object p0

    .line 563
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-nez v1, :cond_1

    .line 564
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 566
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 570
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    if-nez v1, :cond_2

    .line 571
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    .line 573
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 577
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    if-nez v1, :cond_3

    .line 578
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    .line 580
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 553
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 476
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v0, :cond_0

    .line 518
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    if-eqz v0, :cond_1

    .line 521
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->profileFormInput:Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 523
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    if-eqz v0, :cond_2

    .line 524
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 526
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 527
    return-void
.end method
