.class public final Lcom/google/android/finsky/protos/Search$SearchResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Search.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Search;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchResponse"
.end annotation


# instance fields
.field public aggregateQuery:Z

.field public bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

.field public doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public fullPageReplaced:Z

.field public hasAggregateQuery:Z

.field public hasFullPageReplaced:Z

.field public hasOriginalQuery:Z

.field public hasServerLogsCookie:Z

.field public hasSuggestedQuery:Z

.field public originalQuery:Ljava/lang/String;

.field public relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

.field public serverLogsCookie:[B

.field public suggestedQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Search$SearchResponse;->clear()Lcom/google/android/finsky/protos/Search$SearchResponse;

    .line 56
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Search$SearchResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    .line 60
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasOriginalQuery:Z

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    .line 62
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasSuggestedQuery:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    .line 64
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasFullPageReplaced:Z

    .line 65
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    .line 66
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasAggregateQuery:Z

    .line 67
    invoke-static {}, Lcom/google/android/finsky/protos/DocList$Bucket;->emptyArray()[Lcom/google/android/finsky/protos/DocList$Bucket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 68
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 69
    invoke-static {}, Lcom/google/android/finsky/protos/Search$RelatedSearch;->emptyArray()[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    .line 70
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    .line 71
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasServerLogsCookie:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->cachedSize:I

    .line 73
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 123
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 124
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasOriginalQuery:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 125
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 128
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasSuggestedQuery:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 129
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 132
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasAggregateQuery:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    if-eqz v3, :cond_5

    .line 133
    :cond_4
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 136
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 137
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 138
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    aget-object v0, v3, v1

    .line 139
    .local v0, "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v0, :cond_6

    .line 140
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 137
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 146
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 147
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 148
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 149
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 146
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 154
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 155
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 156
    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    aget-object v0, v3, v1

    .line 157
    .local v0, "element":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    if-eqz v0, :cond_a

    .line 158
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 155
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 163
    .end local v0    # "element":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .end local v1    # "i":I
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasServerLogsCookie:Z

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_d

    .line 164
    :cond_c
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 167
    :cond_d
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasFullPageReplaced:Z

    if-nez v3, :cond_e

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    if-eqz v3, :cond_f

    .line 168
    :cond_e
    const/16 v3, 0x8

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 171
    :cond_f
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Search$SearchResponse;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 180
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 184
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 185
    :sswitch_0
    return-object p0

    .line 190
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    .line 191
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasOriginalQuery:Z

    goto :goto_0

    .line 195
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    .line 196
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasSuggestedQuery:Z

    goto :goto_0

    .line 200
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    .line 201
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasAggregateQuery:Z

    goto :goto_0

    .line 205
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 207
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-nez v5, :cond_2

    move v1, v4

    .line 208
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 210
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v1, :cond_1

    .line 211
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 213
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 214
    new-instance v5, Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocList$Bucket;-><init>()V

    aput-object v5, v2, v1

    .line 215
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 216
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 207
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v1, v5

    goto :goto_1

    .line 219
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocList$Bucket;-><init>()V

    aput-object v5, v2, v1

    .line 220
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 221
    iput-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    goto :goto_0

    .line 225
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 227
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 228
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 230
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 231
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 233
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 234
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 235
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 236
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 233
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 227
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 239
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 240
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 241
    iput-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 245
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 247
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    if-nez v5, :cond_8

    move v1, v4

    .line 248
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Search$RelatedSearch;

    .line 250
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    if-eqz v1, :cond_7

    .line 251
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 254
    new-instance v5, Lcom/google/android/finsky/protos/Search$RelatedSearch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Search$RelatedSearch;-><init>()V

    aput-object v5, v2, v1

    .line 255
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 256
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 247
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    array-length v1, v5

    goto :goto_5

    .line 259
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    :cond_9
    new-instance v5, Lcom/google/android/finsky/protos/Search$RelatedSearch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Search$RelatedSearch;-><init>()V

    aput-object v5, v2, v1

    .line 260
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 261
    iput-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    goto/16 :goto_0

    .line 265
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    .line 266
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 270
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    .line 271
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasFullPageReplaced:Z

    goto/16 :goto_0

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Search$SearchResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Search$SearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasOriginalQuery:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 80
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->originalQuery:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 82
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasSuggestedQuery:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 83
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 85
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasAggregateQuery:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    if-eqz v2, :cond_5

    .line 86
    :cond_4
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 88
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 89
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 90
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    aget-object v0, v2, v1

    .line 91
    .local v0, "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v0, :cond_6

    .line 92
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 89
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 97
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 99
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 100
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 97
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 104
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 105
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 106
    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    aget-object v0, v2, v1

    .line 107
    .local v0, "element":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    if-eqz v0, :cond_a

    .line 108
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 105
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 112
    .end local v0    # "element":Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .end local v1    # "i":I
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_d

    .line 113
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 115
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->hasFullPageReplaced:Z

    if-nez v2, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    if-eqz v2, :cond_f

    .line 116
    :cond_e
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 118
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 119
    return-void
.end method
