.class public final Lcom/google/android/finsky/activities/ListTab;
.super Ljava/lang/Object;
.source "ListTab.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/ViewPagerTab;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field protected final mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field private mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

.field private final mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mContainerDocument:Lcom/google/android/finsky/api/model/Document;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

.field private mIsCurrentlySelected:Z

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

.field private final mList:Lcom/google/android/finsky/api/model/DfeList;

.field private mListBoundAlready:Z

.field private mListTabWrapper:Landroid/view/ViewGroup;

.field protected final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

.field private final mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field private mShouldDeferDataDisplay:Z

.field private final mSupportsOverlappingActionBar:Z

.field private final mTabMode:I

.field private final mTitle:Ljava/lang/String;

.field protected final mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/DfeApi;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabData;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;ZLcom/google/android/finsky/layout/actionbar/ActionBarController;ZI)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p5, "inflater"    # Landroid/view/LayoutInflater;
    .param p6, "tabData"    # Lcom/google/android/finsky/activities/TabbedAdapter$TabData;
    .param p7, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p8, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p9, "shouldDeferDataDisplay"    # Z
    .param p10, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .param p11, "supportsOverlappingActionBar"    # Z
    .param p12, "tabMode"    # I

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v3, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v3}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 64
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    .line 88
    const/16 v3, 0x198

    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 115
    iput-object p5, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 117
    iget-object v3, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->dfeList:Lcom/google/android/finsky/api/model/DfeList;

    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    .line 118
    iget-object v3, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->quickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 119
    const/4 v3, 0x2

    move/from16 v0, p12

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    .line 120
    .local v2, "showPlainHeader":Z
    :goto_0
    if-eqz v2, :cond_1

    iget-object v3, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->browseTab:Lcom/google/android/finsky/protos/Browse$BrowseTab;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Browse$BrowseTab;->title:Ljava/lang/String;

    :goto_1
    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mTitle:Ljava/lang/String;

    .line 121
    iget-object v3, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabData;->elementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    iput-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    .line 122
    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v5

    mul-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/DfeList;->setWindowDistance(I)V

    .line 125
    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 126
    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 127
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mShouldDeferDataDisplay:Z

    .line 128
    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 129
    iput-object p7, p0, Lcom/google/android/finsky/activities/ListTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 130
    iput-object p2, p0, Lcom/google/android/finsky/activities/ListTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 131
    iput-object p4, p0, Lcom/google/android/finsky/activities/ListTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 132
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 133
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .line 134
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mSupportsOverlappingActionBar:Z

    .line 135
    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/finsky/activities/ListTab;->mTabMode:I

    .line 137
    invoke-direct {p0, p1, p3}, Lcom/google/android/finsky/activities/ListTab;->createBinder(Landroid/content/Context;Lcom/google/android/play/image/BitmapLoader;)V

    .line 138
    return-void

    .line 119
    .end local v2    # "showPlainHeader":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 120
    .restart local v2    # "showPlainHeader":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private bindList()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 258
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/fragments/DfeListBinder;->setData(Lcom/google/android/finsky/api/model/DfeList;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    iget-object v4, p0, Lcom/google/android/finsky/activities/ListTab;->mTitle:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/finsky/activities/ListTab;->mTabMode:I

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    if-nez v6, :cond_1

    move-object v6, v8

    :goto_0
    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/fragments/DfeListBinder;->bind(Landroid/view/ViewGroup;Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 265
    iput-object v8, p0, Lcom/google/android/finsky/activities/ListTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    .line 268
    :cond_0
    return-void

    .line 260
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    invoke-virtual {v6}, Lcom/google/android/finsky/utils/ObjectMap;->getBundle()Landroid/os/Bundle;

    move-result-object v6

    goto :goto_0
.end method

.method private createBinder(Landroid/content/Context;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/finsky/fragments/DfeListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/fragments/DfeListBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/ClientMutationCache;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/android/finsky/fragments/DfeListBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V

    .line 152
    return-void
.end method

.method private syncViewToState()V
    .locals 4

    .prologue
    const v3, 0x7f0a0118

    .line 214
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->inErrorState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 222
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    if-nez v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeList;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/ListTab;->mShouldDeferDataDisplay:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 230
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    if-nez v1, :cond_3

    .line 231
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    .line 233
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_4

    .line 235
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode(I)V

    .line 240
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayListView;

    .line 241
    .local v0, "list":Lcom/google/android/finsky/layout/play/PlayListView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->setAnimateChanges(Z)V

    .line 243
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->bindList()V

    goto :goto_0

    .line 245
    .end local v0    # "list":Lcom/google/android/finsky/layout/play/PlayListView;
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToLoadingMode()V

    goto :goto_0
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 317
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 318
    return-void
.end method

.method public exitDeferredDataDisplayMode()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mShouldDeferDataDisplay:Z

    .line 146
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    .line 147
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 7
    .param p1, "backendId"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400d0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    .line 159
    new-instance v0, Lcom/google/android/finsky/layout/LayoutSwitcher;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const/4 v2, -0x1

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0254

    const/4 v6, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/LayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 163
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    .line 181
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/DfeListBinder;->onDestroyView()V

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->flushUnusedPages()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    .line 176
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    .line 273
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 0
    .param p1, "instanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/finsky/activities/ListTab;->mInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 208
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->resetItems()V

    .line 252
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->clearTransientState()V

    .line 253
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 254
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    .line 255
    return-void
.end method

.method public onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
    .locals 4

    .prologue
    .line 189
    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const v3, 0x7f0a0118

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 191
    .local v0, "list":Landroid/widget/ListView;
    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 192
    new-instance v1, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v1}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    .line 193
    .local v1, "output":Lcom/google/android/finsky/utils/ObjectMap;
    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/DfeListBinder;

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ObjectMap;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/finsky/fragments/DfeListBinder;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 197
    .end local v0    # "list":Landroid/widget/ListView;
    .end local v1    # "output":Lcom/google/android/finsky/utils/ObjectMap;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTabSelected(Z)V
    .locals 2
    .param p1, "isSelected"    # Z

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mIsCurrentlySelected:Z

    if-eq p1, v0, :cond_1

    .line 278
    if-eqz p1, :cond_2

    .line 280
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressions(Landroid/view/ViewGroup;)V

    .line 294
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/ListTab;->mIsCurrentlySelected:Z

    .line 296
    :cond_1
    return-void

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    goto :goto_0
.end method
