.class public Lcom/google/android/finsky/previews/MediaPlayerWrapper;
.super Landroid/media/MediaPlayer;
.source "MediaPlayerWrapper.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field private mCurrentState:I

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private final mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/previews/StatusListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/google/android/finsky/previews/StatusListener;

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 39
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 41
    iput-object v2, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 42
    iput-object v2, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 43
    iput-object v2, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 51
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 52
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 53
    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 55
    iput-object p1, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    .line 56
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 58
    .local v0, "powerManager":Landroid/os/PowerManager;
    const/4 v1, 0x6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 60
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 181
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentState()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    return v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 153
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->completed()V

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 158
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->releaseWakeLock()V

    .line 161
    :cond_1
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 169
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->error()V

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 144
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->prepared()V

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-interface {v0, p1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 149
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Landroid/media/MediaPlayer;->pause()V

    .line 130
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->paused()V

    .line 132
    invoke-direct {p0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->releaseWakeLock()V

    .line 133
    return-void
.end method

.method public prepare()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepare()V

    .line 106
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->prepared()V

    .line 108
    return-void
.end method

.method public prepareAsync()V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 99
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->preparing()V

    .line 101
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    .line 138
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->releaseWakeLock()V

    .line 140
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->reset()V

    .line 81
    invoke-direct {p0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->releaseWakeLock()V

    .line 82
    return-void
.end method

.method public resetWhileStayingAwake()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->reset()V

    .line 88
    return-void
.end method

.method public setBetweenTrackState()V
    .locals 1

    .prologue
    .line 164
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 165
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 93
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 94
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 74
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 69
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Landroid/media/MediaPlayer;->start()V

    .line 113
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 114
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/StatusListener;->playing()V

    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 118
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 123
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->mCurrentState:I

    .line 124
    invoke-direct {p0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->releaseWakeLock()V

    .line 125
    return-void
.end method
