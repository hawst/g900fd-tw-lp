.class Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;
.super Ljava/lang/Object;
.source "SetupWizardRestoreAppsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getAppsMenuOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 398
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$500(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const/16 v3, 0x9ce

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 401
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$600(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 402
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->showAppListDialog()V
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$700(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$000(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 407
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$300(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I
    invoke-static {v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$400(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)I

    move-result v3

    aget-object v2, v2, v3

    iget-wide v0, v2, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    .line 408
    .local v0, "androidId":J
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z
    invoke-static {v2, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$802(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Z)Z

    .line 409
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mShowAppDialogPostLoad:Z
    invoke-static {v2, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$902(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Z)Z

    .line 410
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$600(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->fetchBackupDocs(J)V

    goto :goto_0
.end method
