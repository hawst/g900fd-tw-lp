.class public Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;
.super Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
.source "SearchResultCorrectionLayout.java"


# instance fields
.field private mFullPageReplaced:Z

.field private mReplacedLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

.field private mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "originalQuery"    # Ljava/lang/String;
    .param p2, "suggestionQuery"    # Ljava/lang/String;
    .param p3, "fullPageReplaced"    # Z

    .prologue
    const/4 v3, 0x0

    .line 37
    iput-boolean p3, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mFullPageReplaced:Z

    .line 39
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mFullPageReplaced:Z

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0324

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setDisplayLine(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mReplacedLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0325

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setDisplayLine(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setVisibility(I)V

    .line 52
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0326

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setDisplayLine(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mReplacedLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;->onFinishInflate()V

    .line 32
    const v0, 0x7f0a0375

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mSuggestionLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    .line 33
    const v0, 0x7f0a0376

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SearchResultCorrectionLayout;->mReplacedLine:Lcom/google/android/finsky/layout/SuggestionBarLayout;

    .line 34
    return-void
.end method
