.class final Lcom/google/android/finsky/api/model/DfeSearch$1;
.super Ljava/lang/Object;
.source "DfeSearch.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/api/model/DfeSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/finsky/api/model/DfeSearch;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/finsky/api/model/DfeSearch;
    .locals 11
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 214
    .local v8, "urlMapCount":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v1, "urls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v8, :cond_0

    .line 216
    new-instance v5, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v9, v10}, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 219
    .local v2, "count":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "query":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 221
    .local v6, "aggregatedQueryInt":I
    const/4 v4, 0x0

    .line 222
    .local v4, "isAggregatedQuery":Ljava/lang/Boolean;
    if-ltz v6, :cond_1

    .line 223
    if-ne v6, v0, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 225
    :cond_1
    new-instance v0, Lcom/google/android/finsky/api/model/DfeSearch;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/api/model/DfeSearch;-><init>(Ljava/util/List;ILjava/lang/String;Ljava/lang/Boolean;Lcom/google/android/finsky/api/model/DfeSearch$1;)V

    return-object v0

    .line 223
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeSearch$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/finsky/api/model/DfeSearch;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/finsky/api/model/DfeSearch;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 230
    new-array v0, p1, [Lcom/google/android/finsky/api/model/DfeSearch;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeSearch$1;->newArray(I)[Lcom/google/android/finsky/api/model/DfeSearch;

    move-result-object v0

    return-object v0
.end method
