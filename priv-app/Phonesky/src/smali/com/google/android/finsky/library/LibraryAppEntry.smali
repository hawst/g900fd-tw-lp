.class public Lcom/google/android/finsky/library/LibraryAppEntry;
.super Lcom/google/android/finsky/library/LibraryEntry;
.source "LibraryAppEntry.java"


# static fields
.field public static final ANY_CERTIFICATE_HASHES:[Ljava/lang/String;


# instance fields
.field public final certificateHashes:[Ljava/lang/String;

.field public final refundPostDeliveryWindowMs:J

.field public final refundPreDeliveryEndtimeMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/library/LibraryAppEntry;->ANY_CERTIFICATE_HASHES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IJ[Ljava/lang/String;)V
    .locals 12
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "offerType"    # I
    .param p4, "documentHash"    # J
    .param p6, "certificateHashes"    # [Ljava/lang/String;

    .prologue
    .line 46
    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-wide/from16 v5, p4

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/finsky/library/LibraryAppEntry;-><init>(Ljava/lang/String;Ljava/lang/String;IJ[Ljava/lang/String;JJ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IJ[Ljava/lang/String;JJ)V
    .locals 15
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "offerType"    # I
    .param p4, "documentHash"    # J
    .param p6, "certificateHashes"    # [Ljava/lang/String;
    .param p7, "refundPreDeliveryEndtimeMs"    # J
    .param p9, "refundPostDeliveryWindowMs"    # J

    .prologue
    .line 65
    sget-object v5, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    const/4 v6, 0x3

    const/4 v8, 0x1

    const-wide v12, 0x7fffffffffffffffL

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    move/from16 v9, p3

    move-wide/from16 v10, p4

    invoke-direct/range {v3 .. v13}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIJJ)V

    .line 68
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    .line 69
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->refundPreDeliveryEndtimeMs:J

    .line 70
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->refundPostDeliveryWindowMs:J

    .line 71
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 9
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/google/android/finsky/library/LibraryEntry;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v5

    .line 113
    :cond_1
    instance-of v7, p1, Lcom/google/android/finsky/library/LibraryAppEntry;

    if-nez v7, :cond_2

    move v5, v6

    .line 115
    goto :goto_0

    :cond_2
    move-object v4, p1

    .line 119
    check-cast v4, Lcom/google/android/finsky/library/LibraryAppEntry;

    .line 120
    .local v4, "libraryAppEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    iget-object v7, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    array-length v7, v7

    iget-object v8, v4, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    array-length v8, v8

    if-ne v7, v8, :cond_0

    .line 124
    iget-object v7, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    array-length v0, v7

    .line 125
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_5

    .line 126
    const/4 v2, 0x0

    .line 127
    .local v2, "innerMatchFound":Z
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-ge v3, v0, :cond_3

    .line 128
    iget-object v7, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    aget-object v7, v7, v1

    iget-object v8, v4, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 129
    const/4 v2, 0x1

    .line 133
    :cond_3
    if-eqz v2, :cond_0

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 127
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v2    # "innerMatchFound":Z
    .end local v3    # "j":I
    :cond_5
    move v5, v6

    .line 137
    goto :goto_0
.end method

.method public hasMatchingCertificateHash([Ljava/lang/String;)Z
    .locals 10
    .param p1, "matchCertificateHashes"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 85
    sget-object v9, Lcom/google/android/finsky/library/LibraryAppEntry;->ANY_CERTIFICATE_HASHES:[Ljava/lang/String;

    if-ne p1, v9, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v8

    .line 88
    :cond_1
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v7, v0, v3

    .line 89
    .local v7, "matchHash":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryAppEntry;->certificateHashes:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .end local v3    # "i$":I
    .restart local v2    # "i$":I
    :goto_2
    if-ge v2, v5, :cond_2

    aget-object v6, v1, v2

    .line 90
    .local v6, "libraryHash":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 88
    .end local v6    # "libraryHash":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_1

    .line 95
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "len$":I
    .end local v7    # "matchHash":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    const-string v0, "{package=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/library/LibraryAppEntry;->getDocId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
