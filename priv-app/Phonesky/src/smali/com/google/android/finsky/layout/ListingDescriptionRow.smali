.class public Lcom/google/android/finsky/layout/ListingDescriptionRow;
.super Landroid/widget/LinearLayout;
.source "ListingDescriptionRow.java"


# instance fields
.field private mIcon:Lcom/google/android/play/image/FifeImageView;

.field private mText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 38
    const v0, 0x7f0a0257

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingDescriptionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingDescriptionRow;->mText:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0a0256

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingDescriptionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingDescriptionRow;->mIcon:Lcom/google/android/play/image/FifeImageView;

    .line 40
    return-void
.end method
