.class public Lcom/google/android/finsky/FinskyApp;
.super Landroid/app/Application;
.source "FinskyApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/FinskyApp$CrashHandler;
    }
.end annotation


# static fields
.field private static final RECEIVERS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static sInstance:Lcom/google/android/finsky/FinskyApp;


# instance fields
.field private mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

.field private mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

.field private mApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

.field private mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private mAppStatesReplicator:Lcom/google/android/finsky/appstate/AppStatesReplicator;

.field private mBitmapCache:Lcom/android/volley/Cache;

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

.field private mCache:Lcom/android/volley/Cache;

.field private final mClientMutationCaches:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/utils/ClientMutationCache;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentAccount:Landroid/accounts/Account;

.field private mDfeApiNonAuthenticated:Lcom/google/android/finsky/api/DfeApi;

.field private mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

.field private final mDfeApis:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/api/DfeApi;",
            ">;"
        }
    .end annotation
.end field

.field private mDfeNotificationManager:Lcom/google/android/finsky/api/DfeNotificationManager;

.field private mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private mEventLoggerInUnitTestMode:Z

.field private final mEventLoggers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/analytics/FinskyEventLog;",
            ">;"
        }
    .end annotation
.end field

.field private mExperimentsByAccountName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/experiments/FinskyExperiments;",
            ">;"
        }
    .end annotation
.end field

.field private mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

.field private mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

.field private mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

.field private mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

.field private final mPendingNotificationsHandler:Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;

.field private mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

.field private final mPlayDfeApis:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/dfe/api/PlayDfeApi;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentSuggestions:Landroid/provider/SearchRecentSuggestions;

.field private mRequestQueue:Lcom/android/volley/RequestQueue;

.field private mSelfUpdateScheduler:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

.field private mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mUsers:Lcom/google/android/finsky/utils/Users;

.field private mVersionCodeOfLastRun:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 149
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/finsky/receivers/BootCompletedReceiver;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$RegisteredReceiver;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/google/android/finsky/receivers/AccountsChangedReceiver;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/FinskyApp;->RECEIVERS:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 179
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    .line 189
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApis:Ljava/util/Map;

    .line 196
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mClientMutationCaches:Ljava/util/Map;

    .line 201
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggers:Ljava/util/Map;

    .line 216
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mExperimentsByAccountName:Ljava/util/Map;

    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggerInUnitTestMode:Z

    .line 1119
    new-instance v0, Lcom/google/android/finsky/FinskyApp$9;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/FinskyApp$9;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPendingNotificationsHandler:Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/finsky/FinskyApp;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/FinskyApp;)Lcom/google/android/finsky/library/LibraryReplicators;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/FinskyApp;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/FinskyApp;)Lcom/android/volley/Cache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/FinskyApp;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mBitmapCache:Lcom/android/volley/Cache;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/FinskyApp;)Lcom/android/volley/RequestQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/FinskyApp;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0
.end method

.method private checkForCrashOnLastRun(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 296
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "crashed"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 297
    .local v0, "crashFile":Ljava/io/File;
    new-instance v1, Lcom/google/android/finsky/FinskyApp$CrashHandler;

    invoke-direct {v1, p0, v0}, Lcom/google/android/finsky/FinskyApp$CrashHandler;-><init>(Lcom/google/android/finsky/FinskyApp;Ljava/io/File;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 300
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    return v1
.end method

.method private cleanupOldFinsky()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 528
    iget-object v5, p0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    const-string v6, "com.google.android.finsky"

    invoke-interface {v5, v6}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 529
    .local v1, "finskyPackage":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 534
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v5, "com.google.android.finsky"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v4

    .line 535
    .local v4, "setting":I
    if-eq v4, v8, :cond_2

    .line 536
    const-string v5, "com.google.android.finsky"

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v6, v7}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "setting":I
    :cond_2
    :goto_1
    sget-object v5, Lcom/google/android/finsky/config/G;->tabskyMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 546
    .local v2, "minVersion":I
    iget v5, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    if-ge v5, v2, :cond_0

    .line 547
    const-string v5, "Updating com.google.android.finsky from %d to %d"

    new-array v6, v8, [Ljava/lang/Object;

    iget v7, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 549
    const-string v5, "com.google.android.finsky"

    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/FinskyApp;->downloadSuicidalTabsky(Ljava/lang/String;I)V

    goto :goto_0

    .line 540
    .end local v2    # "minVersion":I
    :catch_0
    move-exception v0

    .line 541
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v5, "Unable to disable old finsky package."

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static createNetwork()Lcom/android/volley/Network;
    .locals 4

    .prologue
    .line 597
    sget-object v0, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->isBackgroundDataEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    new-instance v1, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v2, Lcom/google/android/volley/GoogleHttpClientStack;

    sget-object v3, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    sget-object v0, Lcom/google/android/finsky/config/G;->enableSensitiveLogging:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    new-instance v3, Lcom/android/volley/toolbox/ByteArrayPool;

    sget-object v0, Lcom/google/android/finsky/config/G;->volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    invoke-direct {v3, v0}, Lcom/android/volley/toolbox/ByteArrayPool;-><init>(I)V

    invoke-direct {v1, v2, v3}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;Lcom/android/volley/toolbox/ByteArrayPool;)V

    move-object v0, v1

    .line 602
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/finsky/utils/DenyAllNetwork;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/DenyAllNetwork;-><init>()V

    goto :goto_0
.end method

.method private downloadSuicidalTabsky(Ljava/lang/String;I)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "version"    # I

    .prologue
    const/4 v5, 0x0

    .line 558
    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v8

    .line 559
    .local v8, "account":Landroid/accounts/Account;
    if-nez v8, :cond_0

    .line 560
    const-string v0, "No current account"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 567
    :goto_0
    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 564
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p1, v5, v5, v5}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 565
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    iget-object v3, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    const v1, 0x7f0c01ac

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "suicidal_tabsky"

    const/4 v7, 0x3

    move-object v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static drain(Lcom/android/volley/RequestQueue;)V
    .locals 1
    .param p0, "queue"    # Lcom/android/volley/RequestQueue;

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/finsky/FinskyApp;->drain(Lcom/android/volley/RequestQueue;I)V

    .line 245
    return-void
.end method

.method public static drain(Lcom/android/volley/RequestQueue;I)V
    .locals 1
    .param p0, "queue"    # Lcom/android/volley/RequestQueue;
    .param p1, "seq"    # I

    .prologue
    .line 248
    new-instance v0, Lcom/google/android/finsky/FinskyApp$1;

    invoke-direct {v0, p1}, Lcom/google/android/finsky/FinskyApp$1;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/android/volley/RequestQueue;->cancelAll(Lcom/android/volley/RequestQueue$RequestFilter;)V

    .line 269
    return-void
.end method

.method private enableReceivers()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 570
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 571
    .local v6, "pm":Landroid/content/pm/PackageManager;
    sget-object v0, Lcom/google/android/finsky/FinskyApp;->RECEIVERS:[Ljava/lang/Class;

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 573
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 574
    .local v2, "component":Landroid/content/ComponentName;
    invoke-virtual {v6, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    .line 575
    .local v7, "setting":I
    if-eq v7, v12, :cond_0

    .line 576
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v6, v2, v8, v9}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 571
    .end local v2    # "component":Landroid/content/ComponentName;
    .end local v7    # "setting":I
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 580
    :catch_0
    move-exception v3

    .line 581
    .local v3, "e":Ljava/lang/SecurityException;
    const-string v8, "Unable to enable %s"

    new-array v9, v12, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 584
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "e":Ljava/lang/SecurityException;
    :cond_1
    return-void
.end method

.method public static get()Lcom/google/android/finsky/FinskyApp;
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    return-object v0
.end method

.method public static getCacheDir(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "suffix"    # Ljava/lang/String;

    .prologue
    .line 591
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 592
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 593
    return-object v0
.end method

.method private setupAccountLibrariesAndReplicator(Landroid/os/Handler;Landroid/os/Handler;Lcom/google/android/finsky/library/Accounts;)V
    .locals 7
    .param p1, "notificationHandler"    # Landroid/os/Handler;
    .param p2, "backgroundHandler"    # Landroid/os/Handler;
    .param p3, "accounts"    # Lcom/google/android/finsky/library/Accounts;

    .prologue
    .line 501
    new-instance v2, Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/library/SQLiteLibrary;-><init>(Landroid/content/Context;)V

    .line 502
    .local v2, "sqLiteLibrary":Lcom/google/android/finsky/library/SQLiteLibrary;
    new-instance v0, Lcom/google/android/finsky/library/Libraries;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p3, v2, v1, p2}, Lcom/google/android/finsky/library/Libraries;-><init>(Lcom/google/android/finsky/library/Accounts;Lcom/google/android/finsky/library/SQLiteLibrary;Landroid/os/Handler;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 504
    new-instance v0, Lcom/google/android/finsky/library/LibraryReplicatorsImpl;

    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getDfeApiProvider()Lcom/google/android/finsky/api/DfeApiProvider;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    sget-object v4, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/library/LibraryReplicatorsImpl;-><init>(Lcom/google/android/finsky/api/DfeApiProvider;Lcom/google/android/finsky/library/SQLiteLibrary;Lcom/google/android/finsky/library/Libraries;Landroid/os/Handler;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    .line 506
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    new-instance v1, Lcom/google/android/finsky/FinskyApp$5;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/FinskyApp$5;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 517
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 518
    return-void
.end method

.method private static updateCachedWidgetUrls(Lcom/google/android/finsky/api/model/DfeToc;)Z
    .locals 9
    .param p0, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1037
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->widgetUrlsByBackend:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    .line 1038
    .local v1, "pref":Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference<Ljava/lang/String;>;"
    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeToc;->getRecsWidgetUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v0

    .line 1042
    .local v0, "changed":Z
    invoke-virtual {v1, v7}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/api/model/DfeToc;->getWidgetUrl(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v3

    .line 1045
    :goto_0
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/api/model/DfeToc;->getWidgetUrl(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_6

    :cond_1
    move v0, v3

    .line 1048
    :goto_1
    invoke-virtual {v1, v6}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/google/android/finsky/api/model/DfeToc;->getWidgetUrl(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v0, :cond_7

    :cond_2
    move v0, v3

    .line 1051
    :goto_2
    invoke-virtual {v1, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/api/model/DfeToc;->getWidgetUrl(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    if-eqz v0, :cond_8

    :cond_3
    move v0, v3

    .line 1054
    :goto_3
    invoke-virtual {v1, v8}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/api/model/DfeToc;->getWidgetUrl(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/FinskyApp;->updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_9

    :cond_4
    move v0, v3

    .line 1057
    :goto_4
    return v0

    :cond_5
    move v0, v2

    .line 1042
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1045
    goto :goto_1

    :cond_7
    move v0, v2

    .line 1048
    goto :goto_2

    :cond_8
    move v0, v2

    .line 1051
    goto :goto_3

    :cond_9
    move v0, v2

    .line 1054
    goto :goto_4
.end method

.method private static updateWidgetUrl(Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;Ljava/lang/String;)Z
    .locals 1
    .param p1, "newUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1061
    .local p0, "preference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1062
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 1063
    const/4 v0, 0x1

    .line 1065
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private whoopsWeBrokeEverything()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 521
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 522
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/ComponentName;

    const-class v2, Lcom/android/vending/AssetBrowserActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 523
    .local v0, "market":Landroid/content/ComponentName;
    invoke-virtual {v1, v0, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 525
    return-void
.end method


# virtual methods
.method public clearCacheAsync(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    new-instance v1, Lcom/android/volley/toolbox/ClearCacheRequest;

    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mCache:Lcom/android/volley/Cache;

    new-instance v3, Lcom/google/android/finsky/FinskyApp$8;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/FinskyApp$8;-><init>(Lcom/google/android/finsky/FinskyApp;Ljava/lang/Runnable;)V

    invoke-direct {v1, v2, v3}, Lcom/android/volley/toolbox/ClearCacheRequest;-><init>(Lcom/android/volley/Cache;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 946
    return-void
.end method

.method public drainAllRequests(II)V
    .locals 1
    .param p1, "sequenceNumber"    # I
    .param p2, "bitmapSequenceNumber"    # I

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-static {v0, p1}, Lcom/google/android/finsky/FinskyApp;->drain(Lcom/android/volley/RequestQueue;I)V

    .line 240
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/play/image/BitmapLoader;->removeInFlightRequests(I)V

    .line 241
    return-void
.end method

.method public getAdIdProvider()Lcom/google/android/gms/ads/identifier/AdIdProvider;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    return-object v0
.end method

.method public getAnalytics()Lcom/google/android/finsky/analytics/Analytics;
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    return-object v0
.end method

.method public getAnalytics(Ljava/lang/String;)Lcom/google/android/finsky/analytics/Analytics;
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 871
    new-instance v0, Lcom/google/android/finsky/analytics/DfeAnalytics;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/analytics/DfeAnalytics;-><init>(Landroid/os/Handler;Lcom/google/android/finsky/api/DfeApi;)V

    return-object v0
.end method

.method public getAppStates()Lcom/google/android/finsky/appstate/AppStates;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    return-object v0
.end method

.method public getAppStatesReplicator()Lcom/google/android/finsky/appstate/AppStatesReplicator;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mAppStatesReplicator:Lcom/google/android/finsky/appstate/AppStatesReplicator;

    return-object v0
.end method

.method public getBitmapCache()Lcom/android/volley/Cache;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mBitmapCache:Lcom/android/volley/Cache;

    return-object v0
.end method

.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    return-object v0
.end method

.method public getBitmapRequestQueue()Lcom/android/volley/RequestQueue;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0
.end method

.method public getCache()Lcom/android/volley/Cache;
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mCache:Lcom/android/volley/Cache;

    return-object v0
.end method

.method public getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 660
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 661
    const-string v1, "No account specified."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 663
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mClientMutationCaches:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 665
    .local v0, "clientMutations":Lcom/google/android/finsky/utils/ClientMutationCache;
    if-nez v0, :cond_1

    .line 666
    new-instance v0, Lcom/google/android/finsky/utils/ClientMutationCache;

    .end local v0    # "clientMutations":Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-direct {v0}, Lcom/google/android/finsky/utils/ClientMutationCache;-><init>()V

    .line 667
    .restart local v0    # "clientMutations":Lcom/google/android/finsky/utils/ClientMutationCache;
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mClientMutationCaches:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    :cond_1
    return-object v0
.end method

.method public getCurrentAccount()Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 993
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    if-nez v1, :cond_1

    .line 994
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->currentAccount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {p0, v1}, Lcom/google/android/finsky/api/AccountHandler;->getAccountFromPreferences(Landroid/content/Context;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)Landroid/accounts/Account;

    move-result-object v0

    .line 996
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 997
    const-string v1, "No account configured on this device."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 998
    const/4 v1, 0x0

    .line 1002
    .end local v0    # "account":Landroid/accounts/Account;
    :goto_0
    return-object v1

    .line 1000
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    .line 1002
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    goto :goto_0
.end method

.method public getCurrentAccountName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1010
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 1011
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDfeApi()Lcom/google/android/finsky/api/DfeApi;
    .locals 1

    .prologue
    .line 676
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;
    .locals 10
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 731
    if-nez p1, :cond_0

    .line 733
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object p1

    .line 734
    if-nez p1, :cond_0

    .line 735
    const-string v0, "No account configured on this device."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 736
    const/4 v8, 0x0

    .line 753
    :goto_0
    return-object v8

    .line 739
    :cond_0
    iget-object v9, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    monitor-enter v9

    .line 740
    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/api/DfeApi;

    .line 741
    .local v8, "result":Lcom/google/android/finsky/api/DfeApi;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v2

    .line 742
    .local v2, "experiment":Lcom/google/android/finsky/experiments/Experiments;
    if-nez v8, :cond_2

    .line 743
    invoke-static {p0}, Lcom/google/android/finsky/config/ContentLevel;->importFromSettings(Landroid/content/Context;)Lcom/google/android/finsky/config/ContentLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/config/ContentLevel;->getDfeValue()I

    move-result v5

    .line 744
    .local v5, "contentFilterLevel":I
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCache()Lcom/android/volley/Cache;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/FinskyApp;->mDfeNotificationManager:Lcom/google/android/finsky/api/DfeNotificationManager;

    iget-object v6, p0, Lcom/google/android/finsky/FinskyApp;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    move-object v0, p0

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeApiContext;->create(Landroid/content/Context;Lcom/android/volley/Cache;Lcom/google/android/finsky/experiments/Experiments;Lcom/google/android/finsky/api/DfeNotificationManager;Ljava/lang/String;ILcom/google/android/gms/ads/identifier/AdIdProvider;)Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v7

    .line 747
    .local v7, "dfeApiContext":Lcom/google/android/finsky/api/DfeApiContext;
    sget-boolean v0, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 748
    const-string v0, "Created new context: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v7, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 750
    :cond_1
    new-instance v8, Lcom/google/android/finsky/api/DfeApiImpl;

    .end local v8    # "result":Lcom/google/android/finsky/api/DfeApi;
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-direct {v8, v0, v7}, Lcom/google/android/finsky/api/DfeApiImpl;-><init>(Lcom/android/volley/RequestQueue;Lcom/google/android/finsky/api/DfeApiContext;)V

    .line 751
    .restart local v8    # "result":Lcom/google/android/finsky/api/DfeApi;
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    invoke-interface {v0, p1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 753
    .end local v5    # "contentFilterLevel":I
    .end local v7    # "dfeApiContext":Lcom/google/android/finsky/api/DfeApiContext;
    :cond_2
    monitor-exit v9

    goto :goto_0

    .line 754
    .end local v2    # "experiment":Lcom/google/android/finsky/experiments/Experiments;
    .end local v8    # "result":Lcom/google/android/finsky/api/DfeApi;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized getDfeApiNonAuthenticated()Lcom/google/android/finsky/api/DfeApi;
    .locals 3

    .prologue
    .line 764
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiNonAuthenticated:Lcom/google/android/finsky/api/DfeApi;

    if-nez v1, :cond_0

    .line 765
    new-instance v1, Lcom/android/volley/toolbox/NoCache;

    invoke-direct {v1}, Lcom/android/volley/toolbox/NoCache;-><init>()V

    invoke-static {p0, v1}, Lcom/google/android/finsky/api/DfeApiContext;->createNonAuthenticated(Landroid/content/Context;Lcom/android/volley/Cache;)Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v0

    .line 766
    .local v0, "dfeApiContext":Lcom/google/android/finsky/api/DfeApiContext;
    new-instance v1, Lcom/google/android/finsky/api/DfeApiImpl;

    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-direct {v1, v2, v0}, Lcom/google/android/finsky/api/DfeApiImpl;-><init>(Lcom/android/volley/RequestQueue;Lcom/google/android/finsky/api/DfeApiContext;)V

    iput-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiNonAuthenticated:Lcom/google/android/finsky/api/DfeApi;

    .line 768
    .end local v0    # "dfeApiContext":Lcom/google/android/finsky/api/DfeApiContext;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiNonAuthenticated:Lcom/google/android/finsky/api/DfeApi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 764
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getDfeApiProvider()Lcom/google/android/finsky/api/DfeApiProvider;
    .locals 1

    .prologue
    .line 691
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

    if-nez v0, :cond_0

    .line 692
    new-instance v0, Lcom/google/android/finsky/FinskyApp$6;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/FinskyApp$6;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDfeNotificationManager()Lcom/google/android/finsky/api/DfeNotificationManager;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDfeNotificationManager:Lcom/google/android/finsky/api/DfeNotificationManager;

    return-object v0
.end method

.method public getDownloadQueue()Lcom/google/android/finsky/download/DownloadQueue;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    return-object v0
.end method

.method public getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1

    .prologue
    .line 927
    iget-boolean v0, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggerInUnitTestMode:Z

    if-eqz v0, :cond_0

    .line 928
    const/4 v0, 0x0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 930
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 897
    iget-boolean v2, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggerInUnitTestMode:Z

    if-eqz v2, :cond_0

    .line 898
    const/4 p1, 0x0

    .line 900
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggers:Ljava/util/Map;

    monitor-enter v3

    .line 901
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 903
    .local v0, "accountName":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggers:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 904
    .local v1, "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    if-nez v1, :cond_1

    .line 905
    new-instance v1, Lcom/google/android/finsky/analytics/FinskyEventLog;

    .end local v1    # "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v2

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/finsky/experiments/FinskyExperiments;)V

    .line 906
    .restart local v1    # "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggers:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    :cond_1
    monitor-exit v3

    return-object v1

    .line 901
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    :cond_2
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    .line 910
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 917
    iget-boolean v0, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggerInUnitTestMode:Z

    if-eqz v0, :cond_0

    .line 918
    const/4 v0, 0x0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 920
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    goto :goto_0
.end method

.method public getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;
    .locals 1

    .prologue
    .line 851
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    return-object v0
.end method

.method public getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 836
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mExperimentsByAccountName:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/experiments/FinskyExperiments;

    .line 840
    .local v0, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    if-nez v0, :cond_0

    .line 841
    new-instance v0, Lcom/google/android/finsky/experiments/FinskyExperiments;

    .end local v0    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    invoke-direct {v0, p1}, Lcom/google/android/finsky/experiments/FinskyExperiments;-><init>(Ljava/lang/String;)V

    .line 842
    .restart local v0    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mExperimentsByAccountName:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    :cond_0
    return-object v0
.end method

.method public getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    return-object v0
.end method

.method public getInstaller()Lcom/google/android/finsky/receivers/Installer;
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    return-object v0
.end method

.method public getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    return-object v0
.end method

.method public getLibraries()Lcom/google/android/finsky/library/Libraries;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-object v0
.end method

.method public getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    return-object v0
.end method

.method public getNotifier()Lcom/google/android/finsky/utils/Notifier;
    .locals 1

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

    return-object v0
.end method

.method public getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    return-object v0
.end method

.method public getPackageMonitorReceiver()Lcom/google/android/finsky/receivers/PackageMonitorReceiver;
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    return-object v0
.end method

.method public getPendingNotificationsHandler()Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;
    .locals 1

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPendingNotificationsHandler:Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;

    return-object v0
.end method

.method public getPlayDfeApi()Lcom/google/android/play/dfe/api/PlayDfeApi;
    .locals 1

    .prologue
    .line 683
    const/4 v0, 0x0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getPlayDfeApi(Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApi;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v3, 0x0

    .line 790
    if-nez p1, :cond_0

    .line 791
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object p1

    .line 793
    :cond_0
    if-nez p1, :cond_1

    .line 794
    const-string v2, "No account configured on this device."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    const/4 v1, 0x0

    .line 809
    :goto_0
    return-object v1

    .line 797
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApis:Ljava/util/Map;

    monitor-enter v3

    .line 798
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApis:Ljava/util/Map;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/dfe/api/PlayDfeApi;

    .line 799
    .local v1, "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    if-nez v1, :cond_3

    .line 800
    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getCache()Lcom/android/volley/Cache;

    move-result-object v2

    invoke-static {p0, v2, p1}, Lcom/google/android/play/dfe/api/PlayDfeApiContext;->create(Landroid/content/Context;Lcom/android/volley/Cache;Landroid/accounts/Account;)Lcom/google/android/play/dfe/api/PlayDfeApiContext;

    move-result-object v0

    .line 803
    .local v0, "playDfeApiContext":Lcom/google/android/play/dfe/api/PlayDfeApiContext;
    sget-boolean v2, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 804
    const-string v2, "Created new PlayDfeApiContext: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 806
    :cond_2
    new-instance v1, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;

    .end local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-direct {v1, v2, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiImpl;-><init>(Lcom/android/volley/RequestQueue;Lcom/google/android/play/dfe/api/PlayDfeApiContext;)V

    .line 807
    .restart local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApis:Ljava/util/Map;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    .end local v0    # "playDfeApiContext":Lcom/google/android/play/dfe/api/PlayDfeApiContext;
    :cond_3
    monitor-exit v3

    goto :goto_0

    .line 810
    .end local v1    # "result":Lcom/google/android/play/dfe/api/PlayDfeApi;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public declared-synchronized getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .locals 1

    .prologue
    .line 707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    if-nez v0, :cond_0

    .line 708
    new-instance v0, Lcom/google/android/finsky/FinskyApp$7;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/FinskyApp$7;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    iput-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 707
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getRecentSuggestions()Landroid/provider/SearchRecentSuggestions;
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mRecentSuggestions:Landroid/provider/SearchRecentSuggestions;

    return-object v0
.end method

.method public getRequestQueue()Lcom/android/volley/RequestQueue;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0
.end method

.method public getSelfUpdateScheduler()Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    .locals 1

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mSelfUpdateScheduler:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    return-object v0
.end method

.method public getToc()Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method public getUsers()Lcom/google/android/finsky/utils/Users;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mUsers:Lcom/google/android/finsky/utils/Users;

    return-object v0
.end method

.method public getVendingApi()Lcom/google/android/vending/remoting/api/VendingApi;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 817
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    .line 818
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 819
    const-string v1, "Fall back to primary account."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 820
    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 822
    :cond_0
    if-nez v0, :cond_1

    .line 823
    const-string v1, "No account configured on this device."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 824
    const/4 v1, 0x0

    .line 826
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/FinskyApp;->getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v1

    goto :goto_0
.end method

.method public getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/vending/remoting/api/VendingApiFactory;->getApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v0

    return-object v0
.end method

.method public getVersionCode()I
    .locals 2

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    invoke-virtual {p0}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v0

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    return v0
.end method

.method public getVersionCodeOfLastRun()I
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lcom/google/android/finsky/FinskyApp;->mVersionCodeOfLastRun:I

    return v0
.end method

.method initEventLoggingForTest()V
    .locals 1

    .prologue
    .line 888
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/FinskyApp;->mEventLoggerInUnitTestMode:Z

    .line 889
    return-void
.end method

.method public onCreate()V
    .locals 26

    .prologue
    .line 316
    sput-object p0, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    .line 318
    sget-object v2, Lcom/google/android/finsky/config/G;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    sget-object v4, Lcom/google/android/play/utils/config/PlayG;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/ArrayUtils;->concatenate([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Ljava/lang/String;

    .line 320
    .local v24, "prefixsToCache":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->init(Landroid/content/Context;[Ljava/lang/String;)V

    .line 321
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/config/PreferenceFile;->init(Landroid/content/Context;)V

    .line 322
    new-instance v2, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v2, v4, v0}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;-><init>(Landroid/content/ContentResolver;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    .line 324
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->versionCode:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/finsky/FinskyApp;->mVersionCodeOfLastRun:I

    .line 327
    new-instance v4, Lcom/android/volley/toolbox/DiskBasedCache;

    const-string v2, "main"

    invoke-static {v2}, Lcom/google/android/finsky/FinskyApp;->getCacheDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    sget-object v2, Lcom/google/android/finsky/config/G;->mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v2, v2, 0x400

    invoke-direct {v4, v5, v2}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mCache:Lcom/android/volley/Cache;

    .line 331
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/FinskyApp;->checkForCrashOnLastRun(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    const-string v2, "Clearing cache due to crash on previous run."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mCache:Lcom/android/volley/Cache;

    invoke-interface {v2}, Lcom/android/volley/Cache;->clear()V

    .line 337
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->whoopsWeBrokeEverything()V

    .line 339
    const-string v2, "Initializing network with DFE %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/volley/GoogleHttpClient;

    const-string v9, ""

    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v9, v10}, Lcom/google/android/volley/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    sget-object v9, Lcom/google/android/finsky/api/DfeApi;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/google/android/volley/GoogleHttpClient;->rewriteURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    new-instance v2, Lcom/android/volley/RequestQueue;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mCache:Lcom/android/volley/Cache;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->createNetwork()Lcom/android/volley/Network;

    move-result-object v5

    const/4 v6, 0x2

    invoke-direct {v2, v4, v5, v6}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2}, Lcom/android/volley/RequestQueue;->start()V

    .line 344
    new-instance v2, Lcom/google/android/vending/remoting/api/VendingApiFactory;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/FinskyApp;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-direct {v2, v4, v5}, Lcom/google/android/vending/remoting/api/VendingApiFactory;-><init>(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

    .line 346
    new-instance v2, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-direct {v2}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    new-instance v4, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->attach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 351
    new-instance v4, Lcom/android/volley/toolbox/DiskBasedCache;

    const-string v2, "images"

    invoke-static {v2}, Lcom/google/android/finsky/FinskyApp;->getCacheDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    sget-object v2, Lcom/google/android/finsky/config/G;->imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v2, v2, 0x400

    invoke-direct {v4, v5, v2}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapCache:Lcom/android/volley/Cache;

    .line 353
    new-instance v2, Lcom/android/volley/RequestQueue;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapCache:Lcom/android/volley/Cache;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->createNetwork()Lcom/android/volley/Network;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v2}, Lcom/android/volley/RequestQueue;->start()V

    .line 356
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v23

    .line 357
    .local v23, "metrics":Landroid/util/DisplayMetrics;
    new-instance v2, Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapRequestQueue:Lcom/android/volley/RequestQueue;

    move-object/from16 v0, v23

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v23

    iget v6, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v9, Lcom/google/android/play/image/TentativeGcRunner;

    invoke-direct {v9}, Lcom/google/android/play/image/TentativeGcRunner;-><init>()V

    invoke-direct {v2, v4, v5, v6, v9}, Lcom/google/android/play/image/BitmapLoader;-><init>(Lcom/android/volley/RequestQueue;IILcom/google/android/play/image/TentativeGcRunner;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 360
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->initSingleton()V

    .line 363
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/vending/VendingBackupAgent;->registerWithBackup(Landroid/content/Context;)V

    .line 365
    invoke-static {}, Lcom/google/android/finsky/download/obb/ObbFactory;->initialize()V

    .line 367
    new-instance v2, Lcom/google/android/finsky/utils/NotificationManager;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/utils/NotificationManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

    .line 369
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->init(Landroid/content/Context;)V

    .line 370
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/download/DownloadManagerFactory;->getDownloadManager(Landroid/content/Context;)Lcom/google/android/finsky/download/DownloadManagerFacade;

    move-result-object v20

    .line 372
    .local v20, "downloadManager":Lcom/google/android/finsky/download/DownloadManagerFacade;
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1}, Lcom/google/android/finsky/download/DownloadQueueImpl;-><init>(Landroid/content/Context;Lcom/google/android/finsky/download/DownloadManagerFacade;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

    invoke-static {v2}, Lcom/google/android/finsky/receivers/RemoveAssetReceiver;->initialize(Lcom/google/android/finsky/utils/Notifier;)V

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-static {v2}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->initialize(Lcom/google/android/finsky/download/DownloadQueue;)V

    .line 377
    new-instance v4, Lcom/google/android/finsky/appstate/PackageManagerRepository;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getPackageMonitorReceiver()Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    move-result-object v6

    const-string v2, "device_policy"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/admin/DevicePolicyManager;

    invoke-direct {v4, v5, v6, v2}, Lcom/google/android/finsky/appstate/PackageManagerRepository;-><init>(Landroid/content/pm/PackageManager;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;Landroid/app/admin/DevicePolicyManager;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v25

    .line 381
    .local v25, "vendingPackage":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    new-instance v2, Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    move-object/from16 v0, v25

    iget v5, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    invoke-direct {v2, v4, v5}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;-><init>(Lcom/google/android/finsky/download/DownloadQueue;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mSelfUpdateScheduler:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    .line 384
    move-object/from16 v0, v25

    iget v2, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/finsky/services/DailyHygiene;->goMakeHygieneIfDirty(Landroid/content/Context;I)V

    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    new-instance v4, Lcom/google/android/finsky/download/obb/ObbPackageTracker;

    invoke-direct {v4}, Lcom/google/android/finsky/download/obb/ObbPackageTracker;-><init>()V

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->attach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 388
    new-instance v2, Landroid/provider/SearchRecentSuggestions;

    const-string v4, "com.google.android.finsky.RecentSuggestionsProvider"

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v5}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mRecentSuggestions:Landroid/provider/SearchRecentSuggestions;

    .line 391
    new-instance v2, Lcom/google/android/finsky/utils/Users;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/utils/Users;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mUsers:Lcom/google/android/finsky/utils/Users;

    .line 393
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->enableReceivers()V

    .line 395
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V

    .line 398
    new-instance v19, Landroid/os/HandlerThread;

    const-string v2, "libraries-thread"

    const/16 v4, 0xa

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 400
    .local v19, "backgroundThread":Landroid/os/HandlerThread;
    invoke-virtual/range {v19 .. v19}, Landroid/os/HandlerThread;->start()V

    .line 403
    new-instance v7, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v7, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 404
    .local v7, "notificationHandler":Landroid/os/Handler;
    new-instance v22, Landroid/os/Handler;

    invoke-virtual/range {v19 .. v19}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 406
    .local v22, "librariesHandler":Landroid/os/Handler;
    new-instance v3, Lcom/google/android/finsky/FinskyApp$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/finsky/FinskyApp$2;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    .line 417
    .local v3, "accounts":Lcom/google/android/finsky/library/Accounts;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1, v3}, Lcom/google/android/finsky/FinskyApp;->setupAccountLibrariesAndReplicator(Landroid/os/Handler;Landroid/os/Handler;Lcom/google/android/finsky/library/Accounts;)V

    .line 418
    sget-object v2, Lcom/google/android/finsky/FinskyApp;->sInstance:Lcom/google/android/finsky/FinskyApp;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/finsky/widget/WidgetUtils;->registerLibraryMutationsListener(Landroid/content/Context;Lcom/google/android/finsky/library/LibraryReplicators;)V

    .line 421
    new-instance v8, Landroid/os/Handler;

    invoke-virtual/range {v19 .. v19}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v8, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 422
    .local v8, "appStatehandler":Landroid/os/Handler;
    new-instance v21, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;

    new-instance v2, Lcom/google/android/finsky/appstate/InMemoryInstallerDataStore;

    invoke-direct {v2}, Lcom/google/android/finsky/appstate/InMemoryInstallerDataStore;-><init>()V

    new-instance v4, Lcom/google/android/finsky/appstate/SQLiteInstallerDataStore;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/finsky/appstate/SQLiteInstallerDataStore;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v21

    invoke-direct {v0, v2, v4, v8, v7}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;-><init>(Lcom/google/android/finsky/appstate/InstallerDataStore;Lcom/google/android/finsky/appstate/InstallerDataStore;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 425
    .local v21, "installerDataStore":Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/FinskyApp;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 428
    new-instance v2, Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-object/from16 v0, v21

    invoke-direct {v2, v0, v4}, Lcom/google/android/finsky/appstate/AppStates;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Lcom/google/android/finsky/appstate/PackageStateRepository;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 431
    new-instance v2, Lcom/google/android/finsky/appstate/AppStatesReplicator;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/FinskyApp;->mApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/FinskyApp;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/finsky/appstate/AppStatesReplicator;-><init>(Lcom/google/android/finsky/library/Accounts;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/vending/remoting/api/VendingApiFactory;Landroid/os/Handler;Landroid/os/Handler;Lcom/google/android/gms/ads/identifier/AdIdProvider;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mAppStatesReplicator:Lcom/google/android/finsky/appstate/AppStatesReplicator;

    .line 433
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-static {v2, v4}, Lcom/google/android/finsky/services/ContentSyncService;->setupListeners(Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;)V

    .line 436
    new-instance v2, Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-direct {v2, v4, v5, v6, v9}, Lcom/google/android/finsky/installer/InstallPolicies;-><init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    .line 439
    new-instance v9, Lcom/google/android/finsky/receivers/InstallerImpl;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/FinskyApp;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/FinskyApp;->mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/FinskyApp;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/FinskyApp;->mUsers:Lcom/google/android/finsky/utils/Users;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-result-object v18

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v18}, Lcom/google/android/finsky/receivers/InstallerImpl;-><init>(Landroid/content/Context;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;Lcom/google/android/finsky/utils/Users;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 446
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    new-instance v4, Lcom/google/android/finsky/FinskyApp$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/finsky/FinskyApp$3;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    invoke-interface {v2, v4}, Lcom/google/android/finsky/receivers/Installer;->start(Ljava/lang/Runnable;)V

    .line 459
    new-instance v2, Lcom/google/android/finsky/utils/GELInstallerListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/finsky/utils/GELInstallerListener;-><init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;)V

    .line 461
    new-instance v9, Lcom/google/android/finsky/DfeNotificationManagerImpl;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/FinskyApp;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/FinskyApp;->mNotificationHelper:Lcom/google/android/finsky/utils/Notifier;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/FinskyApp;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    move-object/from16 v10, p0

    move-object v15, v3

    invoke-direct/range {v9 .. v15}, Lcom/google/android/finsky/DfeNotificationManagerImpl;-><init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/library/Accounts;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/finsky/FinskyApp;->mDfeNotificationManager:Lcom/google/android/finsky/api/DfeNotificationManager;

    .line 464
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardUtils;->initializeCardTrackers()V

    .line 466
    new-instance v2, Lcom/google/android/finsky/utils/UninstallRefundTracker;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v5}, Lcom/google/android/finsky/utils/UninstallRefundTracker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;)V

    .line 467
    new-instance v2, Lcom/google/android/finsky/utils/UninstallSubscriptionsTracker;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/FinskyApp;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/FinskyApp;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v5}, Lcom/google/android/finsky/utils/UninstallSubscriptionsTracker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;)V

    .line 469
    new-instance v2, Lcom/google/android/finsky/appstate/MigrationAsyncTask;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/appstate/MigrationAsyncTask;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/appstate/MigrationAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 471
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/utils/Utils;->syncDebugActivityStatus(Landroid/content/Context;)V

    .line 474
    new-instance v2, Lcom/google/android/finsky/FinskyApp$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/FinskyApp$4;-><init>(Lcom/google/android/finsky/FinskyApp;)V

    invoke-static {v2}, Lcom/google/android/play/image/FifeUrlUtils;->setNetworkInfoCacheInstance(Lcom/google/android/play/image/FifeUrlUtils$NetworkInfoCache;)V

    .line 483
    sget-object v2, Lcom/google/android/finsky/config/G;->analyticsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 484
    new-instance v2, Lcom/google/android/finsky/analytics/DfeAnalytics;

    new-instance v4, Landroid/os/Handler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/google/android/finsky/analytics/DfeAnalytics;-><init>(Landroid/os/Handler;Lcom/google/android/finsky/api/DfeApi;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    .line 491
    :goto_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->migrateAntiMalwareConsent(Landroid/content/Context;)V

    .line 496
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->cleanupOldFinsky()V

    .line 497
    return-void

    .line 486
    :cond_1
    new-instance v2, Lcom/google/android/finsky/analytics/StubAnalytics;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/StubAnalytics;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/FinskyApp;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    goto :goto_0
.end method

.method public setToc(Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 4
    .param p1, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 1020
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    if-nez v2, :cond_2

    const/4 v0, 0x1

    .line 1021
    .local v0, "isOldTocNull":Z
    :goto_0
    iput-object p1, p0, Lcom/google/android/finsky/FinskyApp;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 1022
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    if-eqz v2, :cond_1

    .line 1023
    invoke-static {p1}, Lcom/google/android/finsky/FinskyApp;->updateCachedWidgetUrls(Lcom/google/android/finsky/api/model/DfeToc;)Z

    move-result v1

    .line 1026
    .local v1, "widgetUrlsChanged":Z
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 1027
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.finsky.action.TOC_SET"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/FinskyApp;->sendBroadcast(Landroid/content/Intent;)V

    .line 1030
    .end local v1    # "widgetUrlsChanged":Z
    :cond_1
    return-void

    .line 1020
    .end local v0    # "isOldTocNull":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public switchCurrentAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "newAccount"    # Landroid/accounts/Account;

    .prologue
    .line 971
    iget-object v2, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    monitor-enter v2

    .line 972
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mDfeApis:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 973
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 975
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    invoke-virtual {v1, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 977
    .local v0, "changed":Z
    :goto_0
    iput-object p1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    .line 978
    if-eqz v0, :cond_1

    .line 979
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mCurrentAccount:Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->currentAccount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v1, v2}, Lcom/google/android/finsky/api/AccountHandler;->saveAccountToPreferences(Landroid/accounts/Account;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)V

    .line 981
    iget-object v1, p0, Lcom/google/android/finsky/FinskyApp;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;

    invoke-interface {v1}, Lcom/google/android/finsky/analytics/Analytics;->reset()V

    .line 983
    invoke-static {p0}, Lcom/google/android/finsky/widget/WidgetUtils;->notifyAccountSwitch(Landroid/content/Context;)V

    .line 985
    :cond_1
    return-void

    .line 973
    .end local v0    # "changed":Z
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 975
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
