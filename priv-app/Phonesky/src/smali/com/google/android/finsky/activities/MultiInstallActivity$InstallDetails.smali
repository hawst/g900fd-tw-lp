.class Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
.super Ljava/lang/Object;
.source "MultiInstallActivity.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/MultiInstallActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InstallDetails"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private autoUpdateDisabled:Z

.field private final docId:Ljava/lang/String;

.field private largeDownload:Z

.field private newPermissions:Z

.field private final packageName:Ljava/lang/String;

.field private final packageTitle:Ljava/lang/String;

.field private final permissions:[Ljava/lang/String;

.field private final reason:Ljava/lang/String;

.field private final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails$1;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-lez v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    .line 446
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-lez v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    .line 447
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-lez v1, :cond_2

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    .line 448
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->docId:Ljava/lang/String;

    .line 449
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;

    .line 450
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;

    .line 451
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I

    .line 452
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;

    .line 453
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    if-eqz v1, :cond_3

    .line 454
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 455
    .local v0, "numPermissions":I
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    .line 456
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 460
    .end local v0    # "numPermissions":I
    :goto_3
    return-void

    :cond_0
    move v1, v3

    .line 445
    goto :goto_0

    :cond_1
    move v1, v3

    .line 446
    goto :goto_1

    :cond_2
    move v2, v3

    .line 447
    goto :goto_2

    .line 458
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    goto :goto_3
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;Ljava/lang/String;)V
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "installWarnings"    # Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iget-boolean v0, p2, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->autoUpdateDisabled:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    .line 402
    iget-boolean v0, p2, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->largeDownload:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    .line 403
    iget-boolean v0, p2, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->newPermissions:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    .line 404
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->docId:Ljava/lang/String;

    .line 405
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;

    .line 406
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;

    .line 407
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getVersionCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I

    .line 408
    iput-object p3, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;

    .line 409
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    .line 414
    :goto_0
    return-void

    .line 412
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->docId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    return v0
.end method

.method public done()Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needsAutoUpdateWarning()Z
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    return v0
.end method

.method public needsLargeDownloadWarning()Z
    .locals 1

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    return v0
.end method

.method public needsPermissionsWarning()Z
    .locals 1

    .prologue
    .line 433
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    return v0
.end method

.method public setAutoUpdateDisableWarningAccepted()V
    .locals 1

    .prologue
    .line 421
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    .line 422
    return-void
.end method

.method public setLargeDownloadWarningAccepted()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    .line 430
    return-void
.end method

.method public setPermissionsWarningAccepted()V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    .line 438
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 469
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->largeDownload:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 470
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 471
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->autoUpdateDisabled:Z

    if-eqz v0, :cond_3

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 472
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->docId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 475
    iget v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 477
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->newPermissions:Z

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 479
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 481
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 469
    goto :goto_0

    :cond_2
    move v0, v2

    .line 470
    goto :goto_1

    :cond_3
    move v1, v2

    .line 471
    goto :goto_2
.end method
