.class final Lcom/google/android/finsky/widget/FinskyWidgetProvider$1;
.super Ljava/lang/Object;
.source "FinskyWidgetProvider.java"

# interfaces
.implements Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/widget/FinskyWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getImage(Lcom/google/android/finsky/api/model/Document;I)Lcom/google/android/finsky/protos/Common$Image;
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "maxHeight"    # I

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getPromoBitmapFromDocument(Lcom/google/android/finsky/api/model/Document;II)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    return-object v0
.end method

.method public getImageType(Lcom/google/android/finsky/api/model/Document;)I
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 43
    const/4 v0, 0x2

    return v0
.end method
