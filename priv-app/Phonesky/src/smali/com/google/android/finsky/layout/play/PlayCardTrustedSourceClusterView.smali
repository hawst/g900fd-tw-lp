.class public Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardTrustedSourceClusterView.java"


# instance fields
.field private final mContentVerticalMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 27
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b0153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->mContentVerticalMargin:I

    .line 28
    return-void
.end method


# virtual methods
.method public configurePersonProfile(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "clusterNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;

    .line 43
    .local v1, "trustedSourceContent":Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 45
    .local v0, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->mContentVerticalMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 46
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->mContentVerticalMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 48
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->configurePersonProfile(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->logEmptyClusterImpression()V

    .line 54
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x19b

    return v0
.end method
