.class public Lcom/google/android/finsky/layout/BucketRow;
.super Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
.source "BucketRow.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/Identifiable;


# instance fields
.field private mSameChildHeight:Z

.field private mSameChildWidth:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/BucketRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/BucketRow;->setOrientation(I)V

    .line 38
    sget-object v1, Lcom/android/vending/R$styleable;->BucketRow:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/BucketRow;->mSameChildHeight:Z

    .line 40
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/BucketRow;->mSameChildWidth:Z

    .line 41
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 42
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingLeft()I

    move-result v5

    .line 114
    .local v5, "x":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getHeight()I

    move-result v3

    .line 115
    .local v3, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingBottom()I

    move-result v6

    sub-int v0, v3, v6

    .line 116
    .local v0, "bottomY":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 117
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/BucketRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 118
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 119
    .local v2, "childLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v6, v5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v0, v7

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v7, v8

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v5

    iget v9, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v9, v0, v9

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 122
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    .line 116
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 124
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 67
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 68
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getChildCount()I

    move-result v1

    .line 70
    .local v1, "childCount":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingLeft()I

    move-result v12

    sub-int v12, v9, v12

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingRight()I

    move-result v13

    sub-int v10, v12, v13

    .line 71
    .local v10, "widthForChildren":I
    const/4 v6, 0x0

    .line 72
    .local v6, "maxChildHeight":I
    move v11, v10

    .line 73
    .local v11, "widthLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getWeightSum()F

    move-result v8

    .line 77
    .local v8, "weightLeft":F
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_1

    .line 78
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/BucketRow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "child":Landroid/view/View;
    iget-boolean v12, p0, Lcom/google/android/finsky/layout/BucketRow;->mSameChildWidth:Z

    if-eqz v12, :cond_0

    .line 80
    sub-int v12, v1, v5

    div-int v12, v11, v12

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 88
    .local v3, "childWidthMeasureSpec":I
    :goto_1
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    iget v14, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v12, v13, v14}, Lcom/google/android/finsky/layout/BucketRow;->getChildMeasureSpec(III)I

    move-result v2

    .line 90
    .local v2, "childHeightMeasureSpec":I
    invoke-virtual {v0, v3, v2}, Landroid/view/View;->measure(II)V

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    sub-int/2addr v11, v12

    .line 93
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    invoke-static {v6, v12}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 77
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 83
    .end local v2    # "childHeightMeasureSpec":I
    .end local v3    # "childWidthMeasureSpec":I
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    iget v7, v12, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 84
    .local v7, "weight":F
    int-to-float v12, v11

    mul-float/2addr v12, v7

    div-float/2addr v12, v8

    float-to-int v12, v12

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 86
    .restart local v3    # "childWidthMeasureSpec":I
    sub-float/2addr v8, v7

    goto :goto_1

    .line 96
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "childWidthMeasureSpec":I
    .end local v7    # "weight":F
    :cond_1
    iget-boolean v12, p0, Lcom/google/android/finsky/layout/BucketRow;->mSameChildHeight:Z

    if-eqz v12, :cond_3

    .line 97
    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v6, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 98
    .local v4, "heightSpec":I
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v1, :cond_3

    .line 99
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/BucketRow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 100
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    if-eq v12, v6, :cond_2

    .line 101
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    invoke-virtual {v0, v12, v4}, Landroid/view/View;->measure(II)V

    .line 98
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 107
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "heightSpec":I
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingTop()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingBottom()I

    move-result v13

    add-int/2addr v12, v13

    add-int/2addr v6, v12

    .line 108
    invoke-virtual {p0, v9, v6}, Lcom/google/android/finsky/layout/BucketRow;->setMeasuredDimension(II)V

    .line 109
    return-void
.end method

.method public setContentHorizontalPadding(I)V
    .locals 2
    .param p1, "cardContentHorizontalPadding"    # I

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BucketRow;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0, p1, v0, p1, v1}, Lcom/google/android/finsky/layout/BucketRow;->setPadding(IIII)V

    .line 63
    return-void
.end method

.method public setSameChildHeight(Z)V
    .locals 0
    .param p1, "sameChildHeight"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/BucketRow;->mSameChildHeight:Z

    .line 50
    return-void
.end method
