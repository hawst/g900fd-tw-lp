.class Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;
.super Ljava/lang/Object;
.source "DetailsDataBasedFragment.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->configureContentView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsDataBasedFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsDataBasedFragment;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;->this$0:Lcom/google/android/finsky/activities/DetailsDataBasedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(II)V
    .locals 5
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 375
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;->this$0:Lcom/google/android/finsky/activities/DetailsDataBasedFragment;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->getHeight()I

    move-result v2

    .line 377
    .local v2, "wideHeroPromoHeight":I
    const/4 v3, 0x0

    invoke-static {v3, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v2

    div-float v0, v3, v4

    .line 378
    .local v0, "fullFraction":F
    int-to-float v3, v2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    mul-float/2addr v3, v0

    float-to-int v1, v3

    .line 380
    .local v1, "heroTopPadding":I
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment$1;->this$0:Lcom/google/android/finsky/activities/DetailsDataBasedFragment;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    neg-int v4, v1

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setOverlayableImageTopPadding(I)V

    .line 381
    return-void
.end method
