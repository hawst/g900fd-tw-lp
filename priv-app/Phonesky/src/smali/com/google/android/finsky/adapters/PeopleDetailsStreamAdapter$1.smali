.class Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;
.super Ljava/lang/Object;
.source "PeopleDetailsStreamAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->bindWarmWelcomeCardView(Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

.field final synthetic val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;I)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    iput p3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 234
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x4cf

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 237
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->warmWelcomeOwnProfileAcknowledged:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingWarmWelcome:Z
    invoke-static {v0, v1}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->access$002(Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;Z)Z

    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    iget v1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->notifyItemRemoved(I)V

    .line 240
    return-void
.end method
