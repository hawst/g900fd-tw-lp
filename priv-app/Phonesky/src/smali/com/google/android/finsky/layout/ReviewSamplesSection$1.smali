.class Lcom/google/android/finsky/layout/ReviewSamplesSection$1;
.super Ljava/lang/Object;
.source "ReviewSamplesSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/ReviewSamplesSection;->onDataChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/ReviewSamplesSection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ReviewSamplesSection;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;->this$0:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;->this$0:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    iget-object v0, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;->this$0:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    # getter for: Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->access$000(Lcom/google/android/finsky/layout/ReviewSamplesSection;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;->this$0:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    # getter for: Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->access$000(Lcom/google/android/finsky/layout/ReviewSamplesSection;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getReviewsUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAllReviews(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V

    .line 193
    return-void
.end method
