.class Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;
.super Ljava/lang/Object;
.source "DeviceConfigurationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/DeviceConfigurationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestRecord"
.end annotation


# instance fields
.field public final dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field public final gcmOnly:Z

.field public final listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V
    .locals 0
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "gcmOnly"    # Z
    .param p3, "listener"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->dfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 61
    iput-boolean p2, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->gcmOnly:Z

    .line 62
    iput-object p3, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    .line 63
    return-void
.end method
