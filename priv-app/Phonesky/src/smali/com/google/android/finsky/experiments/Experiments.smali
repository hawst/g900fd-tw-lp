.class public interface abstract Lcom/google/android/finsky/experiments/Experiments;
.super Ljava/lang/Object;
.source "Experiments.java"


# virtual methods
.method public abstract getEnabledExperimentsHeaderValue()Ljava/lang/String;
.end method

.method public abstract getUnsupportedExperimentsHeaderValue()Ljava/lang/String;
.end method

.method public abstract hasEnabledExperiments()Z
.end method

.method public abstract hasUnsupportedExperiments()Z
.end method

.method public abstract isEnabled(Ljava/lang/String;)Z
.end method

.method public abstract setExperiments([Ljava/lang/String;)V
.end method
