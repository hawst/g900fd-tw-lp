.class public Lcom/google/android/finsky/utils/WishlistHelper;
.super Ljava/lang/Object;
.source "WishlistHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;
    }
.end annotation


# static fields
.field private static sLastWishlistMutationTimeMs:J

.field private static sWishlistStatusListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/WishlistHelper;->sWishlistStatusListeners:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 33
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/WishlistHelper;->invokeWishlistStatusListeners(Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$102(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 33
    sput-wide p0, Lcom/google/android/finsky/utils/WishlistHelper;->sLastWishlistMutationTimeMs:J

    return-wide p0
.end method

.method public static addWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/finsky/utils/WishlistHelper;->sWishlistStatusListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public static hasMutationOccurredSince(J)Z
    .locals 2
    .param p0, "timeMs"    # J

    .prologue
    .line 196
    sget-wide v0, Lcom/google/android/finsky/utils/WishlistHelper;->sLastWishlistMutationTimeMs:J

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static invokeWishlistStatusListeners(Ljava/lang/String;ZZ)V
    .locals 2
    .param p0, "docId"    # Ljava/lang/String;
    .param p1, "isInWishlist"    # Z
    .param p2, "isCommited"    # Z

    .prologue
    .line 73
    sget-object v1, Lcom/google/android/finsky/utils/WishlistHelper;->sWishlistStatusListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 74
    sget-object v1, Lcom/google/android/finsky/utils/WishlistHelper;->sWishlistStatusListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

    invoke-interface {v1, p0, p1, p2}, Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;->onWishlistStatusChanged(Ljava/lang/String;ZZ)V

    .line 73
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method

.method public static isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z
    .locals 5
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 119
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v3, "u-wl"

    const/4 v4, 0x1

    invoke-static {v2, v3, p0, v4}, Lcom/google/android/finsky/library/LibraryEntry;->fromDocument(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;I)Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v1

    .line 121
    .local v1, "entry":Lcom/google/android/finsky/library/LibraryEntry;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 123
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/AccountLibrary;->contains(Lcom/google/android/finsky/library/LibraryEntry;)Z

    move-result v2

    return v2
.end method

.method public static processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V
    .locals 12
    .param p0, "view"    # Landroid/view/View;
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 128
    if-nez p1, :cond_1

    .line 129
    const-string v8, "Tried to wishlist an item but there is no document active"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-interface {p2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v10

    invoke-static {p1, v10}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v7

    .line 134
    .local v7, "wasInWishlist":Z
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "docId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "docTitle":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 137
    .local v6, "res":Landroid/content/res/Resources;
    new-instance v5, Lcom/google/android/finsky/utils/WishlistHelper$1;

    invoke-direct {v5, p2, v1, v7}, Lcom/google/android/finsky/utils/WishlistHelper$1;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    .line 149
    .local v5, "modifyResponseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;>;"
    new-instance v3, Lcom/google/android/finsky/utils/WishlistHelper$2;

    invoke-direct {v3, v7, v6, v2, v1}, Lcom/google/android/finsky/utils/WishlistHelper$2;-><init>(ZLandroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .local v3, "errorListener":Lcom/android/volley/Response$ErrorListener;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 166
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v4

    .line 167
    .local v4, "isAccessibilityEnabled":Z
    if-eqz v7, :cond_2

    .line 168
    new-array v10, v8, [Ljava/lang/String;

    aput-object v1, v10, v9

    invoke-static {v10}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v10

    const-string v11, "u-wl"

    invoke-interface {p2, v10, v11, v5, v3}, Lcom/google/android/finsky/api/DfeApi;->removeFromLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 178
    :goto_1
    if-nez v7, :cond_4

    :goto_2
    invoke-static {v1, v8, v9}, Lcom/google/android/finsky/utils/WishlistHelper;->invokeWishlistStatusListeners(Ljava/lang/String;ZZ)V

    .line 180
    if-eqz v4, :cond_0

    .line 184
    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v9, Lcom/google/android/finsky/utils/WishlistHelper$3;

    invoke-direct {v9, v0, v7, p0}, Lcom/google/android/finsky/utils/WishlistHelper$3;-><init>(Landroid/content/Context;ZLandroid/view/View;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 171
    :cond_2
    if-nez v4, :cond_3

    .line 172
    const v10, 0x7f0c0351

    invoke-static {v0, v10, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 174
    :cond_3
    new-array v10, v8, [Ljava/lang/String;

    aput-object v1, v10, v9

    invoke-static {v10}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v10

    const-string v11, "u-wl"

    invoke-interface {p2, v10, v11, v5, v3}, Lcom/google/android/finsky/api/DfeApi;->addToLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    goto :goto_1

    :cond_4
    move v8, v9

    .line 178
    goto :goto_2
.end method

.method public static removeWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/finsky/utils/WishlistHelper;->sWishlistStatusListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method

.method public static shouldHideWishlistAction(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)Z
    .locals 8
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 83
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v5

    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v6

    const/16 v7, 0x14

    if-eq v6, v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v6

    const/16 v7, 0x1e

    if-ne v6, v7, :cond_3

    :cond_2
    move v5, v4

    .line 91
    goto :goto_0

    .line 94
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    .line 95
    .local v2, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-interface {p1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    invoke-static {p0, v2, v6}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v3

    .line 97
    .local v3, "owner":Landroid/accounts/Account;
    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    const/4 v7, 0x6

    if-ne v6, v7, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v7

    invoke-static {v6, v2, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v3

    .line 104
    :cond_4
    const/4 v1, 0x0

    .line 105
    .local v1, "isInstalled":Z
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v6

    if-ne v6, v4, :cond_5

    .line 106
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v6

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 107
    .local v0, "appPackageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v6

    if-eqz v6, :cond_7

    move v1, v4

    .line 111
    .end local v0    # "appPackageName":Ljava/lang/String;
    :cond_5
    :goto_1
    if-nez v3, :cond_6

    if-eqz v1, :cond_0

    :cond_6
    move v5, v4

    .line 112
    goto :goto_0

    .restart local v0    # "appPackageName":Ljava/lang/String;
    :cond_7
    move v1, v5

    .line 107
    goto :goto_1
.end method
