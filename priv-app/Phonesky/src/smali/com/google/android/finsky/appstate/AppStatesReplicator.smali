.class public Lcom/google/android/finsky/appstate/AppStatesReplicator;
.super Ljava/lang/Object;
.source "AppStatesReplicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;
    }
.end annotation


# instance fields
.field private final mAccounts:Lcom/google/android/finsky/library/Accounts;

.field private final mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mBackgroundHandler:Landroid/os/Handler;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mNotificationHandler:Landroid/os/Handler;

.field private final mReplicationListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mVendingApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/library/Accounts;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/vending/remoting/api/VendingApiFactory;Landroid/os/Handler;Landroid/os/Handler;Lcom/google/android/gms/ads/identifier/AdIdProvider;)V
    .locals 1
    .param p1, "accounts"    # Lcom/google/android/finsky/library/Accounts;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4, "vendingApiFactory"    # Lcom/google/android/vending/remoting/api/VendingApiFactory;
    .param p5, "notificationHandler"    # Landroid/os/Handler;
    .param p6, "backgroundHandler"    # Landroid/os/Handler;
    .param p7, "adIdProvider"    # Lcom/google/android/gms/ads/identifier/AdIdProvider;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mReplicationListeners:Ljava/util/List;

    .line 77
    iput-object p1, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    .line 78
    iput-object p2, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 79
    iput-object p3, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 80
    iput-object p4, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mVendingApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

    .line 81
    iput-object p5, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mNotificationHandler:Landroid/os/Handler;

    .line 82
    iput-object p6, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mBackgroundHandler:Landroid/os/Handler;

    .line 83
    iput-object p7, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/appstate/AppStatesReplicator;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/AppStatesReplicator;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mNotificationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/appstate/AppStatesReplicator;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/AppStatesReplicator;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->internalReplicate()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/appstate/AppStatesReplicator;III)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/AppStatesReplicator;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->handleContentSyncResponse(III)V

    return-void
.end method

.method private static getAccountList(Ljava/util/Map;Landroid/accounts/Account;)Ljava/util/List;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;>;",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    .local p0, "accountLists":Ljava/util/Map;, "Ljava/util/Map<Landroid/accounts/Account;Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;>;"
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 393
    .local v0, "accountList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    if-nez v0, :cond_0

    .line 394
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 395
    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    :cond_0
    return-object v0
.end method

.method private getAccountOrdinal(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 261
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 262
    .local v0, "accounts":[Landroid/accounts/Account;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 263
    aget-object v2, v0, v1

    invoke-virtual {v2, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 264
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 267
    :goto_1
    return-object v2

    .line 262
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 267
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private declared-synchronized handleContentSyncResponse(III)V
    .locals 4
    .param p1, "expectedCount"    # I
    .param p2, "finished"    # I
    .param p3, "successful"    # I

    .prologue
    .line 275
    monitor-enter p0

    if-ne p3, p1, :cond_2

    .line 276
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mReplicationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;

    .line 277
    .local v1, "replicationListener":Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;
    if-eqz v1, :cond_0

    .line 278
    iget-object v2, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mNotificationHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/finsky/appstate/AppStatesReplicator$5;

    invoke-direct {v3, p0, v1, p2, p1}, Lcom/google/android/finsky/appstate/AppStatesReplicator$5;-><init>(Lcom/google/android/finsky/appstate/AppStatesReplicator;Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;II)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 275
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "replicationListener":Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 286
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mReplicationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private internalReplicate()V
    .locals 36

    .prologue
    .line 139
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    invoke-interface {v4}, Lcom/google/android/finsky/library/Accounts;->getAccounts()Ljava/util/List;

    move-result-object v30

    .line 140
    .local v30, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    sget-object v4, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->computeConfigurationHash(Ljava/util/List;Ljava/lang/String;)I

    move-result v32

    .line 142
    .local v32, "configurationHash":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/finsky/appstate/PackageStateRepository;->getAllBlocking()Ljava/util/Collection;

    move-result-object v31

    .line 144
    .local v31, "all":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v35

    .line 145
    .local v35, "statesByAccount":Ljava/util/Map;, "Ljava/util/Map<Landroid/accounts/Account;Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 146
    .local v7, "systemApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2, v7}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->bucketAppsByOwner(Ljava/util/Collection;Ljava/util/Map;Ljava/util/List;)I

    move-result v11

    .line 147
    .local v11, "sideloadedCount":I
    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v7, v1}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->computeHash(Ljava/util/Collection;I)I

    move-result v5

    .line 150
    .local v5, "currentSystemAppsHash":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 153
    .local v12, "nowMs":J
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v22

    .line 155
    .local v22, "accountsToReplicate":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    const/4 v4, 0x1

    new-array v0, v4, [I

    move-object/from16 v20, v0

    const/4 v4, 0x0

    const/4 v14, 0x0

    aput v14, v20, v4

    .line 157
    .local v20, "finishedReplications":[I
    const/4 v4, 0x1

    new-array v0, v4, [I

    move-object/from16 v21, v0

    const/4 v4, 0x0

    const/4 v14, 0x0

    aput v14, v21, v4

    .line 158
    .local v21, "successfulReplications":[I
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v27

    .line 159
    .local v27, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v28

    .line 162
    .local v28, "responseListeners":Ljava/util/List;, "Ljava/util/List<Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;>;>;"
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v33

    .local v33, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/accounts/Account;

    .line 163
    .local v29, "account":Landroid/accounts/Account;
    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 164
    .local v10, "accountApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v10, v1}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->computeHash(Ljava/util/Collection;I)I

    move-result v8

    .line 165
    .local v8, "currentAccountAppsHash":I
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->replicatedAccountAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-object/from16 v0, v29

    iget-object v14, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v14}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v18

    .line 167
    .local v18, "accountAppsPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->replicatedSystemAppsHash:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-object/from16 v0, v29

    iget-object v14, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v14}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v16

    .line 169
    .local v16, "systemAppsPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 170
    .local v6, "replicatedSystemAppsHash":I
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .local v9, "replicatedAccountAppsHash":I
    move-object/from16 v4, p0

    .line 172
    invoke-virtual/range {v4 .. v13}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->makeContentSyncRequest(IILjava/util/List;IILjava/util/List;IJ)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    move-result-object v34

    .line 175
    .local v34, "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    if-nez v34, :cond_1

    .line 176
    sget-boolean v4, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 177
    const-string v4, "No installation states replication required: account=%s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, v29

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v14, v15

    invoke-static {v4, v14}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 182
    :cond_1
    sget-boolean v4, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 183
    const-string v4, "Replicating installation states: account=%s, numSystemApps=%d, numAccountApps=%d, numSideloaded=%d"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, v29

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v14, v15

    const/4 v15, 0x2

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v14, v15

    const/4 v15, 0x3

    move-object/from16 v0, v34

    iget v0, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v14, v15

    invoke-static {v4, v14}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    :cond_2
    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    move-object/from16 v0, v27

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    new-instance v14, Lcom/google/android/finsky/appstate/AppStatesReplicator$3;

    move-object/from16 v15, p0

    move/from16 v17, v5

    move/from16 v19, v8

    invoke-direct/range {v14 .. v22}, Lcom/google/android/finsky/appstate/AppStatesReplicator$3;-><init>(Lcom/google/android/finsky/appstate/AppStatesReplicator;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;ILcom/google/android/finsky/config/PreferenceFile$SharedPreference;I[I[ILjava/util/List;)V

    move-object/from16 v0, v28

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .end local v6    # "replicatedSystemAppsHash":I
    .end local v8    # "currentAccountAppsHash":I
    .end local v9    # "replicatedAccountAppsHash":I
    .end local v10    # "accountApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    .end local v16    # "systemAppsPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    .end local v18    # "accountAppsPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Integer;>;"
    .end local v29    # "account":Landroid/accounts/Account;
    .end local v34    # "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    :cond_3
    move-object/from16 v23, p0

    move-object/from16 v24, v22

    move-object/from16 v25, v20

    move-object/from16 v26, v21

    .line 206
    invoke-direct/range {v23 .. v28}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->performRequests(Ljava/util/List;[I[ILjava/util/List;Ljava/util/List;)V

    .line 208
    return-void
.end method

.method private makeInstallState(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;J)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .locals 4
    .param p1, "packageState"    # Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .param p2, "nowMs"    # J

    .prologue
    const/4 v3, 0x1

    .line 410
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;-><init>()V

    .line 411
    .local v0, "assetInstallState":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    iget-object v1, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    invoke-static {v1, v2}, Lcom/google/android/finsky/local/AssetUtils;->makeAssetId(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    .line 413
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetId:Z

    .line 414
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    .line 415
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetState:Z

    .line 416
    iput-wide p2, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    .line 417
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasInstallTime:Z

    .line 418
    iget-object v1, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    .line 419
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasPackageName:Z

    .line 420
    iget v1, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v1, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    .line 421
    iput-boolean v3, v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasVersionCode:Z

    .line 423
    return-object v0
.end method

.method private makeSystemApp(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .locals 4
    .param p1, "packageState"    # Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .prologue
    const/4 v3, 0x1

    .line 427
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;-><init>()V

    .line 428
    .local v1, "systemApp":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    iget-object v2, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    .line 429
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasPackageName:Z

    .line 430
    iget v2, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v2, v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    .line 431
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasVersionCode:Z

    .line 435
    iget-object v2, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 436
    .local v0, "certsToSort":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 437
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    .line 438
    return-object v1
.end method

.method private performRequests(Ljava/util/List;[I[ILjava/util/List;Ljava/util/List;)V
    .locals 18
    .param p2, "finishedReplications"    # [I
    .param p3, "successfulReplications"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;[I[I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p1, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .local p4, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;>;"
    .local p5, "responseListeners":Ljava/util/List;, "Ljava/util/List<Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;>;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 230
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v9}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->handleContentSyncResponse(III)V

    .line 258
    :cond_0
    return-void

    .line 233
    :cond_1
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v12, v6, :cond_0

    .line 234
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/accounts/Account;

    .line 235
    .local v10, "account":Landroid/accounts/Account;
    sget-boolean v6, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v6, :cond_2

    .line 236
    const-string v6, "ContentSyncRequestProto for account %s:"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v0, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v6}, Lcom/google/protobuf/nano/MessageNanoPrinter;->print(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v15

    .line 239
    .local v15, "prettyProto":Ljava/lang/String;
    const-string v6, "\n"

    invoke-virtual {v15, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .local v11, "arr$":[Ljava/lang/String;
    array-length v14, v11

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v14, :cond_2

    aget-object v16, v11, v13

    .line 240
    .local v16, "s":Ljava/lang/String;
    const-string v6, ": %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v16, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 243
    .end local v11    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "prettyProto":Ljava/lang/String;
    .end local v16    # "s":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    .line 244
    .local v5, "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    move-object/from16 v0, p5

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/volley/Response$Listener;

    .line 246
    .local v8, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mVendingApiFactory:Lcom/google/android/vending/remoting/api/VendingApiFactory;

    iget-object v7, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/vending/remoting/api/VendingApiFactory;->getApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v4

    .line 247
    .local v4, "vendingApi":Lcom/google/android/vending/remoting/api/VendingApi;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAdIdProvider:Lcom/google/android/gms/ads/identifier/AdIdProvider;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->getAccountOrdinal(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Lcom/google/android/finsky/appstate/AppStatesReplicator$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/google/android/finsky/appstate/AppStatesReplicator$4;-><init>(Lcom/google/android/finsky/appstate/AppStatesReplicator;[ILjava/util/List;[I)V

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/vending/remoting/api/VendingApi;->syncContent(Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;Lcom/google/android/gms/ads/identifier/AdIdProvider;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 233
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method bucketAppsByOwner(Ljava/util/Collection;Ljava/util/Map;Ljava/util/List;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, "packageStates":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    .local p2, "byAccount":Ljava/util/Map;, "Ljava/util/Map<Landroid/accounts/Account;Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;>;"
    .local p3, "systemApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    const/4 v7, 0x0

    .line 356
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    if-eqz v8, :cond_1

    .line 357
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Buckets must be empty"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 360
    :cond_1
    const/4 v6, 0x0

    .line 361
    .local v6, "unaccounted":I
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 364
    .local v5, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget-boolean v8, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    if-eqz v8, :cond_3

    iget-boolean v8, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isUpdatedSystemApp:Z

    if-eqz v8, :cond_5

    :cond_3
    const/4 v2, 0x1

    .line 365
    .local v2, "isUserApp":Z
    :goto_1
    if-eqz v2, :cond_4

    .line 366
    iget-object v8, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v9, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    iget-object v10, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 368
    .local v4, "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_6

    .line 369
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    .line 370
    .local v3, "owner":Landroid/accounts/Account;
    invoke-static {p2, v3}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->getAccountList(Ljava/util/Map;Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    .end local v3    # "owner":Landroid/accounts/Account;
    .end local v4    # "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_4
    :goto_2
    iget-boolean v8, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    if-eqz v8, :cond_2

    iget-boolean v8, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-nez v8, :cond_2

    .line 378
    invoke-interface {p3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v2    # "isUserApp":Z
    :cond_5
    move v2, v7

    .line 364
    goto :goto_1

    .line 372
    .restart local v2    # "isUserApp":Z
    .restart local v4    # "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 382
    .end local v2    # "isUserApp":Z
    .end local v4    # "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .end local v5    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_7
    iget-object v7, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    invoke-interface {v7}, Lcom/google/android/finsky/library/Accounts;->getAccounts()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 383
    .local v0, "account":Landroid/accounts/Account;
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 384
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-interface {p2, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 387
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_9
    return v6
.end method

.method computeConfigurationHash(Ljava/util/List;Ljava/lang/String;)I
    .locals 4
    .param p2, "systemFingerprint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 219
    .local v1, "hash":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 220
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v3

    xor-int/2addr v1, v3

    .line 221
    goto :goto_0

    .line 222
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return v1
.end method

.method computeHash(Ljava/util/Collection;I)I
    .locals 1
    .param p2, "configurationHash"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 406
    .local p1, "packageStates":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    invoke-interface {p1}, Ljava/util/Collection;->hashCode()I

    move-result v0

    xor-int/2addr v0, p2

    return v0
.end method

.method public load(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/finsky/appstate/AppStatesReplicator$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/appstate/AppStatesReplicator$1;-><init>(Lcom/google/android/finsky/appstate/AppStatesReplicator;Ljava/lang/Runnable;)V

    .line 100
    .local v0, "countdown":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 101
    iget-object v1, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method makeContentSyncRequest(IILjava/util/List;IILjava/util/List;IJ)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    .locals 12
    .param p1, "currentSystemAppsHash"    # I
    .param p2, "replicatedSystemAppsHash"    # I
    .param p4, "currentAccountAppsHash"    # I
    .param p5, "replicatedAccountAppsHash"    # I
    .param p7, "sideloadedAppCount"    # I
    .param p8, "nowMs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;II",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;",
            ">;IJ)",
            "Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;"
        }
    .end annotation

    .prologue
    .line 295
    .local p3, "systemApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    .local p6, "accountApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;>;"
    if-eq p1, p2, :cond_2

    const/4 v4, 0x1

    .line 299
    .local v4, "needsSystemAppsUpload":Z
    :goto_0
    if-nez v4, :cond_0

    move/from16 v0, p4

    move/from16 v1, p5

    if-eq v0, v1, :cond_3

    :cond_0
    const/4 v3, 0x1

    .line 303
    .local v3, "needsAccountAppsUpload":Z
    :goto_1
    if-nez v3, :cond_1

    if-eqz v4, :cond_6

    .line 304
    :cond_1
    new-instance v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;-><init>()V

    .line 306
    .local v8, "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    if-eqz p6, :cond_4

    .line 307
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v5

    .line 308
    .local v5, "numAccountApps":I
    if-lez v5, :cond_4

    .line 309
    new-array v9, v5, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    iput-object v9, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    .line 310
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v5, :cond_4

    .line 311
    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 312
    .local v7, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget-object v9, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->assetInstallState:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    move-wide/from16 v0, p8

    invoke-direct {p0, v7, v0, v1}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->makeInstallState(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;J)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    move-result-object v10

    aput-object v10, v9, v2

    .line 310
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 295
    .end local v2    # "i":I
    .end local v3    # "needsAccountAppsUpload":Z
    .end local v4    # "needsSystemAppsUpload":Z
    .end local v5    # "numAccountApps":I
    .end local v7    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .end local v8    # "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 299
    .restart local v4    # "needsSystemAppsUpload":Z
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 317
    .restart local v3    # "needsAccountAppsUpload":Z
    .restart local v8    # "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    :cond_4
    if-eqz v4, :cond_5

    .line 318
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    .line 319
    .local v6, "numSysApps":I
    if-lez v6, :cond_5

    .line 320
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    iput-object v9, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    .line 321
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    if-ge v2, v6, :cond_5

    .line 322
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 323
    .restart local v7    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget-object v9, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->systemApp:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    invoke-direct {p0, v7}, Lcom/google/android/finsky/appstate/AppStatesReplicator;->makeSystemApp(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    move-result-object v10

    aput-object v10, v9, v2

    .line 321
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 327
    .end local v2    # "i":I
    .end local v6    # "numSysApps":I
    .end local v7    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_5
    move/from16 v0, p7

    iput v0, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->sideloadedAppCount:I

    .line 328
    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;->hasSideloadedAppCount:Z

    .line 331
    .end local v8    # "request":Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    :goto_4
    return-object v8

    :cond_6
    const/4 v8, 0x0

    goto :goto_4
.end method

.method public declared-synchronized replicate(Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/finsky/appstate/AppStatesReplicator$Listener;

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mReplicationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mReplicationListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 126
    :goto_0
    monitor-exit p0

    return-void

    .line 120
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/appstate/AppStatesReplicator;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/AppStatesReplicator$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/appstate/AppStatesReplicator$2;-><init>(Lcom/google/android/finsky/appstate/AppStatesReplicator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
