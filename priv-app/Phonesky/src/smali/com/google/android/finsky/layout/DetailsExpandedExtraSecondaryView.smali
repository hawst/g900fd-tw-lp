.class public Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;
.super Landroid/widget/LinearLayout;
.source "DetailsExpandedExtraSecondaryView.java"


# instance fields
.field private mDescription:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 35
    const v0, 0x7f0a0198

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mTitle:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f0a0199

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mDescription:Landroid/widget/TextView;

    .line 37
    return-void
.end method

.method public populate(Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;)V
    .locals 2
    .param p1, "extraSecondary"    # Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;->description:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mDescription:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mDescription:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->mDescription:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
