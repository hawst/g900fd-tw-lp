.class public Lcom/google/android/finsky/layout/DetailsMarginBox;
.super Lcom/google/android/finsky/layout/DetailsColumnLayout;
.source "DetailsMarginBox.java"


# instance fields
.field private mCardTransitionTarget:Landroid/view/View;

.field private mDummyHeader:Landroid/view/View;

.field protected mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mScreenshotGallery:Landroid/view/View;

.field private final mScrollThreshold:I

.field private final mUseWideLayout:Z

.field private mXDistanceScrolledSinceLastDown:F

.field private mYDistanceScrolledSinceLastDown:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsMarginBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsColumnLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScrollThreshold:I

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mUseWideLayout:Z

    .line 52
    return-void
.end method

.method private sendCancelEventToHero(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 151
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 152
    .local v0, "cancelEvent":Landroid/view/MotionEvent;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 153
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 154
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 155
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f0a00c8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HeroGraphicView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    .line 59
    const v0, 0x7f0a020f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    .line 60
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 64
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    if-nez v7, :cond_0

    .line 147
    :goto_0
    return v9

    .line 69
    :cond_0
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsMarginBox;->sendCancelEventToHero(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 83
    .local v1, "currMotionX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 84
    .local v2, "currMotionY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 95
    :goto_1
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mLastMotionX:F

    .line 96
    iput v2, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mLastMotionY:F

    .line 97
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v4

    .line 100
    .local v4, "mainScrollY":I
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mXDistanceScrolledSinceLastDown:F

    iget v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScrollThreshold:I

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-gtz v7, :cond_2

    iget v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mYDistanceScrolledSinceLastDown:F

    iget v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScrollThreshold:I

    int-to-float v8, v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_3

    .line 103
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsMarginBox;->sendCancelEventToHero(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 86
    .end local v4    # "mainScrollY":I
    :pswitch_1
    iput v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mXDistanceScrolledSinceLastDown:F

    .line 87
    iput v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mYDistanceScrolledSinceLastDown:F

    goto :goto_1

    .line 90
    :pswitch_2
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mXDistanceScrolledSinceLastDown:F

    iget v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mLastMotionX:F

    sub-float v8, v1, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    add-float/2addr v7, v8

    iput v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mXDistanceScrolledSinceLastDown:F

    .line 91
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mYDistanceScrolledSinceLastDown:F

    iget v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mLastMotionY:F

    sub-float v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    add-float/2addr v7, v8

    iput v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mYDistanceScrolledSinceLastDown:F

    goto :goto_1

    .line 108
    .restart local v4    # "mainScrollY":I
    :cond_3
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getSideMargin()I

    move-result v0

    .line 109
    .local v0, "contentSideMargin":I
    int-to-float v7, v0

    cmpg-float v7, v1, v7

    if-lez v7, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->getWidth()I

    move-result v7

    sub-int/2addr v7, v0

    int-to-float v7, v7

    cmpl-float v7, v1, v7

    if-ltz v7, :cond_7

    .line 112
    :cond_4
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScreenshotGallery:Landroid/view/View;

    if-nez v7, :cond_5

    .line 113
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsScroller:Landroid/widget/ScrollView;

    const v8, 0x7f0a016a

    invoke-virtual {v7, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScreenshotGallery:Landroid/view/View;

    .line 115
    :cond_5
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScreenshotGallery:Landroid/view/View;

    if-eqz v7, :cond_6

    .line 116
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScreenshotGallery:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    sub-int v6, v7, v4

    .line 117
    .local v6, "screenshotVisibleTop":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mScreenshotGallery:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int v5, v6, v7

    .line 118
    .local v5, "screenshotVisibleBottom":I
    int-to-float v7, v6

    cmpl-float v7, v2, v7

    if-ltz v7, :cond_6

    int-to-float v7, v5

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_6

    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsMarginBox;->sendCancelEventToHero(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 127
    .end local v5    # "screenshotVisibleBottom":I
    .end local v6    # "screenshotVisibleTop":I
    :cond_6
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v7, p1}, Lcom/google/android/finsky/layout/HeroGraphicView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    .line 131
    :cond_7
    iget-boolean v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mUseWideLayout:Z

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDummyHeader:Landroid/view/View;

    if-nez v7, :cond_8

    .line 132
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsScroller:Landroid/widget/ScrollView;

    const v8, 0x7f0a014e

    invoke-virtual {v7, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDummyHeader:Landroid/view/View;

    .line 135
    :cond_8
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDummyHeader:Landroid/view/View;

    if-eqz v7, :cond_9

    .line 136
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDummyHeader:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 137
    .local v3, "dummyHeaderVisibleHeight":I
    int-to-float v7, v3

    cmpg-float v7, v2, v7

    if-gtz v7, :cond_9

    .line 139
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v7, p1}, Lcom/google/android/finsky/layout/HeroGraphicView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    .line 146
    .end local v3    # "dummyHeaderVisibleHeight":I
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsMarginBox;->sendCancelEventToHero(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 215
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->getWidth()I

    move-result v3

    .line 216
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->getHeight()I

    move-result v2

    .line 218
    .local v2, "height":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    if-eqz v4, :cond_0

    .line 219
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v4, v6, v6, v3, v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->layout(IIII)V

    .line 222
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v4, v6, v6, v3, v2}, Landroid/widget/ScrollView;->layout(IIII)V

    .line 223
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v4, v6, v6, v3, v2}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->layout(IIII)V

    .line 225
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    .line 227
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 228
    .local v1, "cardTransitionTargetWidth":I
    sub-int v4, v3, v1

    div-int/lit8 v0, v4, 0x2

    .line 229
    .local v0, "cardTransitionTargetLeft":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    add-int v6, v0, v1

    invoke-virtual {v4, v0, v5, v6, v2}, Landroid/view/View;->layout(IIII)V

    .line 233
    .end local v0    # "cardTransitionTargetLeft":I
    .end local v1    # "cardTransitionTargetWidth":I
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v12, 0x8

    const/4 v9, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    .line 159
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 160
    .local v7, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 162
    .local v3, "height":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    if-eqz v8, :cond_0

    .line 163
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    const/high16 v10, -0x80000000

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, p1, v10}, Lcom/google/android/finsky/layout/HeroGraphicView;->measure(II)V

    .line 169
    :cond_0
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mUseWideLayout:Z

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/finsky/utils/UiUtils;->getGridColumnContentWidth(Landroid/content/res/Resources;)I

    move-result v1

    .line 172
    .local v1, "contentColumnWidth":I
    :goto_0
    invoke-static {v7, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 173
    .local v6, "mainContentWidth":I
    sub-int v8, v7, v6

    div-int/lit8 v5, v8, 0x2

    .line 175
    .local v5, "mainContentPadding":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v8, v5}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSideMargin(I)V

    .line 177
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v8, v6}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setScrollerWidth(I)V

    .line 181
    iget-object v10, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    if-ge v6, v7, :cond_4

    const/4 v8, 0x1

    :goto_1
    invoke-virtual {v10, v8}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setExpandCardSections(Z)V

    .line 183
    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 184
    .local v0, "childHeightSpec":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v8, p1, v0}, Landroid/widget/ScrollView;->measure(II)V

    .line 185
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v8, p1, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->measure(II)V

    .line 187
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eq v8, v12, :cond_2

    .line 190
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mUseWideLayout:Z

    if-eqz v8, :cond_5

    .line 191
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsMarginBox;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b00f9

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 193
    .local v4, "leadingVerticalGap":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/HeroGraphicView;->isShowingNoImageFallbackFill()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 194
    div-int/lit8 v4, v4, 0x2

    .line 205
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mCardTransitionTarget:Landroid/view/View;

    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    sub-int v10, v3, v4

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V

    .line 210
    .end local v4    # "leadingVerticalGap":I
    :cond_2
    invoke-virtual {p0, v7, v3}, Lcom/google/android/finsky/layout/DetailsMarginBox;->setMeasuredDimension(II)V

    .line 211
    return-void

    .end local v0    # "childHeightSpec":I
    .end local v1    # "contentColumnWidth":I
    .end local v5    # "mainContentPadding":I
    .end local v6    # "mainContentWidth":I
    :cond_3
    move v1, v7

    .line 169
    goto :goto_0

    .restart local v1    # "contentColumnWidth":I
    .restart local v5    # "mainContentPadding":I
    .restart local v6    # "mainContentWidth":I
    :cond_4
    move v8, v9

    .line 181
    goto :goto_1

    .line 197
    .restart local v0    # "childHeightSpec":I
    :cond_5
    const/4 v4, 0x0

    .line 198
    .restart local v4    # "leadingVerticalGap":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsMarginBox;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 199
    .local v2, "firstChild":Landroid/view/View;
    instance-of v8, v2, Lcom/google/android/finsky/layout/HeroGraphicView;

    if-eqz v8, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eq v8, v12, :cond_1

    .line 201
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    goto :goto_2
.end method
