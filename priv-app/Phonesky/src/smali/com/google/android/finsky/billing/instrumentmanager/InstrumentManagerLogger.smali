.class public Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;
.super Ljava/lang/Object;
.source "InstrumentManagerLogger.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;


# instance fields
.field private final mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/FinskyEventLog;)V
    .locals 2
    .param p1, "node"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p2, "logger"    # Lcom/google/android/finsky/analytics/FinskyEventLog;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 39
    iput-object p2, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 40
    if-nez p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "node cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    return-void
.end method

.method private convertUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 5
    .param p1, "imElement"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->createPlayStoreUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    .line 177
    .local v2, "playElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 178
    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 179
    .local v1, "numChildren":I
    new-array v3, v1, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v3, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 181
    iget-object v4, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->convertUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    aput-object v3, v4, v0

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "i":I
    .end local v1    # "numChildren":I
    :cond_0
    return-object v2
.end method

.method private createPlayStoreUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 2
    .param p1, "imElement"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .prologue
    .line 188
    iget v1, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 191
    .local v0, "playElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iget-object v1, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    array-length v1, v1

    if-lez v1, :cond_0

    .line 192
    iget-object v1, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 193
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    .line 195
    :cond_0
    return-object v0
.end method


# virtual methods
.method convertBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 7
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    .prologue
    const/4 v6, 0x1

    .line 62
    new-instance v3, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    iget v4, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->backgroundEventType:I

    invoke-direct {v3, v4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    iget-object v4, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->exceptionType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->serverLatencyMs:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->clientLatencyMs:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setClientLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v3

    iget v4, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->resultCode:I

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 69
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->integratorLogToken:[B

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->integratorLogToken:[B

    array-length v3, v3

    if-lez v3, :cond_0

    .line 70
    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->integratorLogToken:[B

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLogsCookie([B)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 72
    :cond_0
    iget-object v1, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->creditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 74
    .local v1, "imAction":Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    if-eqz v1, :cond_1

    .line 75
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;-><init>()V

    .line 77
    .local v2, "playAction":Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panOcrEnabled:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    .line 78
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanOcrEnabled:Z

    .line 80
    iget v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panEntryType:I

    iput v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    .line 81
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanEntryType:Z

    .line 83
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panRecognizedByOcr:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    .line 84
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanRecognizedByOcr:Z

    .line 86
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 87
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanValidationErrorOccurred:Z

    .line 89
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateOcrEnabled:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    .line 90
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateOcrEnabled:Z

    .line 92
    iget v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    iput v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    .line 93
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateEntryType:Z

    .line 95
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateRecognizedByOcr:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    .line 96
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateRecognizedByOcr:Z

    .line 98
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 99
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateValidationErrorOccurred:Z

    .line 101
    iget v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    iput v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    .line 102
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasNumOcrAttempts:Z

    .line 104
    iget v3, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->ocrExitReason:I

    iput v3, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    .line 105
    iput-boolean v6, v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasOcrExitReason:Z

    .line 107
    invoke-virtual {v0, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setCreditCardEntryAction(Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 109
    .end local v2    # "playAction":Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v3

    return-object v3
.end method

.method convertClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    .locals 8
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;

    .prologue
    .line 114
    iget-object v6, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    .line 115
    .local v2, "length":I
    if-nez v2, :cond_0

    .line 116
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Click path must have at least one item"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 120
    :cond_0
    add-int/lit8 v6, v2, 0x1

    invoke-static {v6}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 123
    .local v4, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 124
    iget-object v6, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {p0, v6}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->createPlayStoreUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 131
    .local v3, "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :cond_2
    invoke-interface {v3}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 132
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-interface {v3}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v3

    .line 134
    if-nez v3, :cond_2

    .line 136
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v5

    .line 137
    .local v5, "playEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v6, v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 138
    return-object v5
.end method

.method convertImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 9
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;

    .prologue
    const/4 v8, 0x0

    .line 144
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 145
    .local v2, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;>;"
    iget-object v1, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 146
    .local v1, "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_0
    if-eqz v1, :cond_0

    .line 148
    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    goto :goto_0

    .line 151
    :cond_0
    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->pathToTree(Ljava/util/List;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v5

    .line 154
    .local v5, "tree":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    move-object v0, v5

    .line 155
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :goto_1
    iget-object v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v6, v6

    if-eqz v6, :cond_1

    .line 157
    iget-object v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    aget-object v0, v6, v8

    goto :goto_1

    .line 160
    :cond_1
    iget v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    iget-object v7, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v7}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v7

    iget v7, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    if-eq v6, v7, :cond_2

    .line 161
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected types in tree: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v8}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v8

    iget v8, v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 167
    :cond_2
    iget-object v6, p1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->impressionTree:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {p0, v6}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->convertUiElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    .line 168
    .local v4, "subTree":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    aput-object v4, v6, v8

    iput-object v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 170
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v3

    .line 171
    .local v3, "playEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    iput-object v5, v3, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 172
    return-object v3
.end method

.method public onBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V
    .locals 2
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->convertBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 48
    return-void
.end method

.method public onClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)V
    .locals 2
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->convertClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 53
    return-void
.end method

.method public onImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)V
    .locals 2
    .param p1, "imEvent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;->convertImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;)V

    .line 58
    return-void
.end method
