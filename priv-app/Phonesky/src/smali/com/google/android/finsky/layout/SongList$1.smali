.class Lcom/google/android/finsky/layout/SongList$1;
.super Ljava/lang/Object;
.source "SongList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/SongList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/SongList;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/SongList;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 48
    iget-object v3, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    # getter for: Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/finsky/layout/SongList;->access$100(Lcom/google/android/finsky/layout/SongList;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    # getter for: Lcom/google/android/finsky/layout/SongList;->mHighlightedSongDocId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/finsky/layout/SongList;->access$000(Lcom/google/android/finsky/layout/SongList;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/SongSnippet;

    .line 49
    .local v2, "song":Lcom/google/android/finsky/layout/SongSnippet;
    if-nez v2, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/SongSnippet;->setState(I)V

    .line 55
    iget-object v3, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    # getter for: Lcom/google/android/finsky/layout/SongList;->mParent:Landroid/widget/ScrollView;
    invoke-static {v3}, Lcom/google/android/finsky/layout/SongList;->access$200(Lcom/google/android/finsky/layout/SongList;)Landroid/widget/ScrollView;

    move-result-object v3

    if-nez v3, :cond_2

    .line 56
    const-string v3, "Unable to scroll the highlighted song into view."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 60
    .local v0, "position":Landroid/graphics/Rect;
    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/SongSnippet;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 62
    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/SongList;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/UiUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v4

    sub-int v1, v3, v4

    .line 63
    .local v1, "scrollY":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/SongList$1;->this$0:Lcom/google/android/finsky/layout/SongList;

    # getter for: Lcom/google/android/finsky/layout/SongList;->mParent:Landroid/widget/ScrollView;
    invoke-static {v3}, Lcom/google/android/finsky/layout/SongList;->access$200(Lcom/google/android/finsky/layout/SongList;)Landroid/widget/ScrollView;

    move-result-object v3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3, v4, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0
.end method
