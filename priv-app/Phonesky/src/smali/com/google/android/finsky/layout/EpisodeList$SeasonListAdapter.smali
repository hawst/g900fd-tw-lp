.class Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "EpisodeList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/EpisodeList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeasonListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/finsky/api/model/Document;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/EpisodeList;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/layout/EpisodeList;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p3, "seasons":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->this$0:Lcom/google/android/finsky/layout/EpisodeList;

    .line 336
    const v0, 0x7f0401bc

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/finsky/api/model/Document;

    invoke-interface {p3, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 337
    return-void
.end method

.method private getSeasonTitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;
    .locals 1
    .param p1, "item"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 379
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 356
    if-nez p2, :cond_0

    .line 357
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0401bc

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 361
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 363
    .local v0, "item":Lcom/google/android/finsky/api/model/Document;
    const v3, 0x7f0a03ae

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 364
    .local v2, "spinnerText":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getSeasonTitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->this$0:Lcom/google/android/finsky/layout/EpisodeList;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/EpisodeList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 368
    .local v1, "resources":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->this$0:Lcom/google/android/finsky/layout/EpisodeList;

    # getter for: Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v3}, Lcom/google/android/finsky/layout/EpisodeList;->access$000(Lcom/google/android/finsky/layout/EpisodeList;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    if-ne v0, v3, :cond_1

    .line 369
    const v3, 0x7f090062

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 370
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->this$0:Lcom/google/android/finsky/layout/EpisodeList;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/EpisodeList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09009c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 375
    :goto_0
    return-object p2

    .line 372
    :cond_1
    const v3, 0x7f02017f

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 373
    const v3, 0x7f09005a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0401bd

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 345
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .local v0, "item":Lcom/google/android/finsky/api/model/Document;
    move-object v1, p2

    .line 347
    check-cast v1, Landroid/widget/TextView;

    .line 348
    .local v1, "spinnerText":Landroid/widget/TextView;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;->getSeasonTitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    return-object p2
.end method
