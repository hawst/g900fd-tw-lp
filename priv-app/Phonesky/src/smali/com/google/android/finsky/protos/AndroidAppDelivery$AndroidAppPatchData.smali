.class public final Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AndroidAppDelivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/AndroidAppDelivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidAppPatchData"
.end annotation


# instance fields
.field public baseSignature:Ljava/lang/String;

.field public baseVersionCode:I

.field public downloadUrl:Ljava/lang/String;

.field public hasBaseSignature:Z

.field public hasBaseVersionCode:Z

.field public hasDownloadUrl:Z

.field public hasMaxPatchSize:Z

.field public hasPatchFormat:Z

.field public maxPatchSize:J

.field public patchFormat:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 420
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 421
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    .line 422
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 425
    iput v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    .line 426
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseVersionCode:Z

    .line 427
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    .line 428
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseSignature:Z

    .line 429
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    .line 430
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasDownloadUrl:Z

    .line 431
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    .line 432
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasPatchFormat:Z

    .line 433
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    .line 434
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasMaxPatchSize:Z

    .line 435
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->cachedSize:I

    .line 436
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 462
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 463
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseVersionCode:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    if-eqz v1, :cond_1

    .line 464
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseSignature:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 468
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasDownloadUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 472
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_5
    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasPatchFormat:Z

    if-eqz v1, :cond_7

    .line 476
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasMaxPatchSize:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 480
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 491
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 492
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 496
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 497
    :sswitch_0
    return-object p0

    .line 502
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    .line 503
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseVersionCode:Z

    goto :goto_0

    .line 507
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    .line 508
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseSignature:Z

    goto :goto_0

    .line 512
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    .line 513
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasDownloadUrl:Z

    goto :goto_0

    .line 517
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 518
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 521
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    .line 522
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasPatchFormat:Z

    goto :goto_0

    .line 528
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    .line 529
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasMaxPatchSize:Z

    goto :goto_0

    .line 492
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 518
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 442
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseVersionCode:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    if-eqz v0, :cond_1

    .line 443
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 445
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasBaseSignature:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 446
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 448
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasDownloadUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 449
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 451
    :cond_5
    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    if-ne v0, v2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasPatchFormat:Z

    if-eqz v0, :cond_7

    .line 452
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 454
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->hasMaxPatchSize:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    .line 455
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 457
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 458
    return-void
.end method
