.class public Lcom/google/android/finsky/utils/PlayCardUtils;
.super Ljava/lang/Object;
.source "PlayCardUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;,
        Lcom/google/android/finsky/utils/PlayCardUtils$CardDismissalAction;
    }
.end annotation


# static fields
.field private static final sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

.field private static final sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

.field private static final sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

.field private static sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

.field private static sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

.field private static sWindowAttachedCards:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/play/layout/PlayCardViewBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .line 75
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .line 77
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .line 113
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sWindowAttachedCards:Ljava/util/Set;

    .line 119
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    .line 121
    sget-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    const-string v1, "transition_card_details:card:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    .line 125
    sget-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    const-string v1, "transition_card_details:cover:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardUtils;->updateTrackedCardLabels()V

    return-void
.end method

.method static synthetic access$100()Ljava/util/Set;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/finsky/utils/PlayCardUtils;->sWindowAttachedCards:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/layout/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "x3"    # Landroid/accounts/Account;
    .param p4, "x4"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "x5"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 72
    invoke-static/range {p0 .. p5}, Lcom/google/android/finsky/utils/PlayCardUtils;->configureActions(Lcom/google/android/play/layout/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method static synthetic access$600(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/finsky/utils/PlayCardUtils;->convertCardTypeToUiElementType(I)I

    move-result v0

    return v0
.end method

.method public static bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "parentId"    # Ljava/lang/String;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 605
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    .line 607
    return-void
.end method

.method public static bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V
    .locals 33
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "parentId"    # Ljava/lang/String;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "isDismissed"    # Z
    .param p6, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p8, "useDocumentAspectRatio"    # Z
    .param p9, "displayIndex"    # I

    .prologue
    .line 613
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 615
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/PlayCardUtils;->setLoggingDataIfNecessary(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 616
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;

    .line 618
    .local v9, "cardLoggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->getInstance()Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->getCardCustomizer(Lcom/google/android/play/layout/PlayCardViewBase;)Lcom/google/android/finsky/utils/PlayCardCustomizer;

    move-result-object v10

    .line 620
    .local v10, "cardCustomizer":Lcom/google/android/finsky/utils/PlayCardCustomizer;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v10, v0, v1}, Lcom/google/android/finsky/utils/PlayCardCustomizer;->preBind(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;)V

    .line 622
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getTitle()Landroid/widget/TextView;

    move-result-object v29

    .line 623
    .local v29, "title":Landroid/widget/TextView;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 624
    .local v12, "displayTitle":Ljava/lang/String;
    if-ltz p9, :cond_0

    .line 625
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0355

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    add-int/lit8 v32, p9, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v7, v8

    const/4 v8, 0x1

    aput-object v12, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 628
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/PlayCardUtils;->updateTitleContentDescription(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 632
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getThumbnail()Lcom/google/android/play/layout/PlayCardThumbnail;

    move-result-object v27

    .line 633
    .local v27, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 634
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->updateCoverPadding(I)V

    .line 637
    move-object/from16 v0, p0

    instance-of v4, v0, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;

    if-eqz v4, :cond_e

    move-object/from16 v4, p0

    check-cast v4, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;

    invoke-interface {v4}, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->getImageTypePreference()[I

    move-result-object v16

    .line 640
    .local v16, "imageTypeSequence":[I
    :goto_0
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v28

    check-cast v28, Lcom/google/android/finsky/layout/DocImageView;

    .line 641
    .local v28, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    move-object/from16 v0, v28

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 642
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 643
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v15

    .line 647
    .local v15, "docType":I
    const/4 v4, 0x2

    if-eq v15, v4, :cond_1

    const/4 v4, 0x4

    if-eq v15, v4, :cond_1

    const/16 v4, 0x18

    if-eq v15, v4, :cond_1

    const/16 v4, 0x19

    if-ne v15, v4, :cond_f

    :cond_1
    const/16 v24, 0x1

    .line 652
    .local v24, "skipTransition":Z
    :goto_1
    if-nez v24, :cond_10

    .line 653
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    const-string v6, "transition_card_details:card:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 655
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    const/16 v6, 0x3a

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 657
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCardBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setTransitionName(Ljava/lang/String;)V

    .line 660
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    const-string v6, "transition_card_details:cover:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 662
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 663
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    const/16 v6, 0x3a

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 664
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 665
    sget-object v4, Lcom/google/android/finsky/utils/PlayCardUtils;->sTransitionNameCoverBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/DocImageView;->setTransitionName(Ljava/lang/String;)V

    .line 667
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setTransitionGroup(Z)V

    .line 672
    .end local v15    # "docType":I
    .end local v24    # "skipTransition":Z
    :cond_2
    :goto_2
    if-eqz p8, :cond_3

    .line 673
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setThumbnailAspectRatio(F)V

    .line 677
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getSubtitle()Lcom/google/android/play/layout/PlayTextView;

    move-result-object v25

    check-cast v25, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 678
    .local v25, "subtitle":Lcom/google/android/finsky/layout/DecoratedTextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getRatingBar()Lcom/google/android/play/layout/StarRatingBar;

    move-result-object v19

    .line 679
    .local v19, "ratingBar":Lcom/google/android/play/layout/StarRatingBar;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getItemBadge()Lcom/google/android/play/layout/PlayTextView;

    move-result-object v17

    check-cast v17, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 680
    .local v17, "itemBadge":Lcom/google/android/finsky/layout/DecoratedTextView;
    if-eqz v25, :cond_4

    .line 681
    const/4 v4, 0x4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 683
    :cond_4
    if-eqz v19, :cond_5

    .line 684
    const/4 v4, 0x4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 686
    :cond_5
    if-eqz v17, :cond_6

    .line 687
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 692
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->supportsSubtitleAndRating()Z

    move-result v26

    .line 693
    .local v26, "supportsSubtitleAndRating":Z
    if-eqz v19, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_11

    const/16 v18, 0x1

    .line 695
    .local v18, "preferRating":Z
    :goto_3
    if-nez v26, :cond_7

    if-eqz v18, :cond_12

    :cond_7
    const/16 v22, 0x1

    .line 696
    .local v22, "showRating":Z
    :goto_4
    if-nez v26, :cond_8

    if-nez v18, :cond_13

    :cond_8
    const/16 v23, 0x1

    .line 698
    .local v23, "showSubtitle":Z
    :goto_5
    if-eqz v22, :cond_9

    .line 699
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/BadgeUtils;->configureRatingItemSection(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/StarRatingBar;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 702
    :cond_9
    if-eqz v23, :cond_a

    if-eqz v25, :cond_a

    .line 703
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 704
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/PlayCardUtils;->getDocDisplaySubtitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v14

    .line 705
    .local v14, "docSubtitle":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->shouldShowInlineCreatorBadge()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 707
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 711
    .end local v14    # "docSubtitle":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/PlayCardUtils;->updateCardLabel(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 713
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getDescription()Lcom/google/android/play/layout/PlayTextView;

    move-result-object v11

    .line 714
    .local v11, "description":Lcom/google/android/play/layout/PlayTextView;
    if-eqz v11, :cond_b

    .line 715
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v13

    .line 716
    .local v13, "docDescription":Ljava/lang/CharSequence;
    invoke-virtual {v11, v13}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_14

    const/16 v4, 0x8

    :goto_6
    invoke-virtual {v11, v4}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 720
    .end local v13    # "docDescription":Ljava/lang/CharSequence;
    :cond_b
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCardReasons(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 722
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getOverflow()Landroid/widget/ImageView;

    move-result-object v5

    .line 723
    .local v5, "overflow":Landroid/widget/ImageView;
    if-eqz v5, :cond_c

    if-eqz p1, :cond_c

    .line 724
    if-eqz p5, :cond_15

    .line 726
    const/4 v4, 0x4

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 728
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 757
    :cond_c
    :goto_7
    if-eqz p5, :cond_1a

    .line 758
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setDisplayAsDisabled(Z)V

    .line 759
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 760
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setClickable(Z)V

    .line 768
    :cond_d
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoadingIndicator()Landroid/view/View;

    move-result-object v4

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 771
    invoke-virtual {v9}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 775
    invoke-virtual {v9}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v4

    invoke-interface {v4, v9}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 777
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 778
    return-void

    .line 637
    .end local v5    # "overflow":Landroid/widget/ImageView;
    .end local v11    # "description":Lcom/google/android/play/layout/PlayTextView;
    .end local v16    # "imageTypeSequence":[I
    .end local v17    # "itemBadge":Lcom/google/android/finsky/layout/DecoratedTextView;
    .end local v18    # "preferRating":Z
    .end local v19    # "ratingBar":Lcom/google/android/play/layout/StarRatingBar;
    .end local v22    # "showRating":Z
    .end local v23    # "showSubtitle":Z
    .end local v25    # "subtitle":Lcom/google/android/finsky/layout/DecoratedTextView;
    .end local v26    # "supportsSubtitleAndRating":Z
    .end local v28    # "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    :cond_e
    sget-object v16, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    goto/16 :goto_0

    .line 647
    .restart local v15    # "docType":I
    .restart local v16    # "imageTypeSequence":[I
    .restart local v28    # "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    :cond_f
    const/16 v24, 0x0

    goto/16 :goto_1

    .line 669
    .restart local v24    # "skipTransition":Z
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setTransitionGroup(Z)V

    goto/16 :goto_2

    .line 693
    .end local v15    # "docType":I
    .end local v24    # "skipTransition":Z
    .restart local v17    # "itemBadge":Lcom/google/android/finsky/layout/DecoratedTextView;
    .restart local v19    # "ratingBar":Lcom/google/android/play/layout/StarRatingBar;
    .restart local v25    # "subtitle":Lcom/google/android/finsky/layout/DecoratedTextView;
    .restart local v26    # "supportsSubtitleAndRating":Z
    :cond_11
    const/16 v18, 0x0

    goto/16 :goto_3

    .line 695
    .restart local v18    # "preferRating":Z
    :cond_12
    const/16 v22, 0x0

    goto/16 :goto_4

    .line 696
    .restart local v22    # "showRating":Z
    :cond_13
    const/16 v23, 0x0

    goto/16 :goto_5

    .line 717
    .restart local v11    # "description":Lcom/google/android/play/layout/PlayTextView;
    .restart local v13    # "docDescription":Ljava/lang/CharSequence;
    .restart local v23    # "showSubtitle":Z
    :cond_14
    const/4 v4, 0x0

    goto :goto_6

    .line 730
    .end local v13    # "docDescription":Ljava/lang/CharSequence;
    .restart local v5    # "overflow":Landroid/widget/ImageView;
    :cond_15
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p6

    .line 731
    invoke-static/range {v4 .. v9}, Lcom/google/android/finsky/utils/PlayCardUtils;->configureOverflowView(Lcom/google/android/play/layout/PlayCardViewBase;Landroid/widget/ImageView;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 734
    invoke-virtual {v5}, Landroid/widget/ImageView;->isFocusable()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 737
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet1()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v20

    .line 738
    .local v20, "reason1":Lcom/google/android/play/layout/PlayCardSnippet;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet2()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v21

    .line 739
    .local v21, "reason2":Lcom/google/android/play/layout/PlayCardSnippet;
    const/16 v30, 0x0

    .line 740
    .local v30, "toWire":Lcom/google/android/play/layout/PlayCardSnippet;
    if-eqz v20, :cond_17

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v4

    if-nez v4, :cond_17

    .line 741
    move-object/from16 v30, v20

    .line 745
    :cond_16
    :goto_9
    if-nez v30, :cond_18

    const/16 v31, 0x0

    .line 746
    .local v31, "toWireImage":Landroid/view/View;
    :goto_a
    if-eqz v30, :cond_19

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_19

    .line 747
    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    .line 748
    invoke-virtual {v5}, Landroid/widget/ImageView;->getId()I

    move-result v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 749
    const/4 v4, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    goto/16 :goto_7

    .line 742
    .end local v31    # "toWireImage":Landroid/view/View;
    :cond_17
    if-eqz v21, :cond_16

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v4

    if-nez v4, :cond_16

    .line 743
    move-object/from16 v30, v21

    goto :goto_9

    .line 745
    :cond_18
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/play/layout/PlayCardSnippet;->getImageView()Landroid/widget/ImageView;

    move-result-object v31

    goto :goto_a

    .line 751
    .restart local v31    # "toWireImage":Landroid/view/View;
    :cond_19
    const/4 v4, -0x1

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    goto/16 :goto_7

    .line 762
    .end local v20    # "reason1":Lcom/google/android/play/layout/PlayCardSnippet;
    .end local v21    # "reason2":Lcom/google/android/play/layout/PlayCardSnippet;
    .end local v30    # "toWire":Lcom/google/android/play/layout/PlayCardSnippet;
    .end local v31    # "toWireImage":Landroid/view/View;
    :cond_1a
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setDisplayAsDisabled(Z)V

    .line 763
    if-eqz p4, :cond_d

    .line 764
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v9}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_8
.end method

.method private static bindCardReasons(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 16
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 385
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet1()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v0

    .line 386
    .local v0, "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet2()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v7

    .line 388
    .local v7, "snippet2":Lcom/google/android/play/layout/PlayCardSnippet;
    if-nez v0, :cond_1

    if-nez v7, :cond_1

    .line 448
    .end local v0    # "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    :cond_0
    :goto_0
    return-void

    .line 394
    .restart local v0    # "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    :cond_1
    if-eqz v0, :cond_2

    .line 395
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setVisibility(I)V

    .line 396
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setSeparatorVisible(Z)V

    .line 398
    :cond_2
    if-eqz v7, :cond_3

    .line 399
    const/16 v2, 0x8

    invoke-virtual {v7, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setVisibility(I)V

    .line 400
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setSeparatorVisible(Z)V

    .line 404
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getTextOnlySnippetMarginLeft()I

    move-result v4

    .line 405
    .local v4, "textOnlySnippetMarginLeft":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getAvatarSnippetMarginLeft()I

    move-result v5

    .line 406
    .local v5, "avatarSnippetMarginLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v15

    .line 407
    .local v15, "reasons":Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    if-eqz v15, :cond_4

    iget-object v2, v15, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v2, v2

    if-nez v2, :cond_5

    .line 410
    :cond_4
    if-eqz v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->hasOptimalDeviceClassWarning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 411
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getOptimalDeviceClassWarning()Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v7, v2, v4, v5}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 413
    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayCardSnippet;->getImageView()Landroid/widget/ImageView;

    move-result-object v14

    check-cast v14, Lcom/google/android/play/image/FifeImageView;

    .line 414
    .local v14, "reasonAvatar":Lcom/google/android/play/image/FifeImageView;
    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 415
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setVisibility(I)V

    goto :goto_0

    .line 420
    .end local v14    # "reasonAvatar":Lcom/google/android/play/image/FifeImageView;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 422
    .local v6, "cardNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    iget-object v2, v15, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v2, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    if-eqz v0, :cond_6

    if-nez v7, :cond_8

    .line 425
    :cond_6
    invoke-static {v15}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v1

    .line 428
    .local v1, "highestPriReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v7, :cond_7

    move-object v0, v7

    .end local v0    # "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    :cond_7
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReason(Lcom/google/android/play/layout/PlayCardSnippet;Lcom/google/android/finsky/protos/DocumentV2$Reason;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 433
    .end local v1    # "highestPriReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .restart local v0    # "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    :cond_8
    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Lcom/google/android/play/layout/PlayCardSnippet;->setSeparatorVisible(Z)V

    .line 436
    invoke-static {v15}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v1

    .line 439
    .restart local v1    # "highestPriReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    invoke-static {v15, v1}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;Lcom/google/android/finsky/protos/DocumentV2$Reason;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v8

    .local v8, "secondHighestPriReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    .line 441
    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReason(Lcom/google/android/play/layout/PlayCardSnippet;Lcom/google/android/finsky/protos/DocumentV2$Reason;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move v11, v4

    move v12, v5

    move-object v13, v6

    .line 444
    invoke-static/range {v7 .. v13}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReason(Lcom/google/android/play/layout/PlayCardSnippet;Lcom/google/android/finsky/protos/DocumentV2$Reason;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0
.end method

.method private static bindReason(Lcom/google/android/play/layout/PlayCardSnippet;Lcom/google/android/finsky/protos/DocumentV2$Reason;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 27
    .param p0, "cardSnippet"    # Lcom/google/android/play/layout/PlayCardSnippet;
    .param p1, "reason"    # Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "textOnlyReasonMarginLeft"    # I
    .param p5, "avatarReasonMarginLeft"    # I
    .param p6, "cardNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 251
    if-nez p1, :cond_0

    .line 364
    :goto_0
    return-void

    .line 254
    :cond_0
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardSnippet;->setVisibility(I)V

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getImageView()Landroid/widget/ImageView;

    move-result-object v20

    check-cast v20, Lcom/google/android/play/image/FifeImageView;

    .line 256
    .local v20, "snippetAvatar":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 258
    .local v16, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    .line 259
    .local v14, "reasonReview":Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;
    if-eqz v14, :cond_f

    .line 262
    iget-object v0, v14, Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;->review:Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-object/from16 v17, v0

    .line 264
    .local v17, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    if-eqz v23, :cond_1

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 265
    .local v4, "author":Ljava/lang/String;
    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_2

    const/4 v7, 0x1

    .line 266
    .local v7, "hasAuthor":Z
    :goto_2
    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    move/from16 v21, v0

    .line 267
    .local v21, "stars":I
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_3

    const/4 v9, 0x1

    .line 268
    .local v9, "hasTitle":Z
    :goto_3
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_4

    const/4 v8, 0x1

    .line 269
    .local v8, "hasComment":Z
    :goto_4
    if-eqz v7, :cond_9

    .line 272
    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v23

    if-eqz v23, :cond_5

    .line 275
    const v23, 0x7f100009

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v4, v24, v25

    const/16 v25, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    const/16 v25, 0x3

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    move-object/from16 v0, v16

    move/from16 v1, v23

    move/from16 v2, v21

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 298
    .local v18, "reviewText":Ljava/lang/String;
    :goto_5
    invoke-static/range {v18 .. v18}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 301
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v22, v0

    .line 302
    .local v22, "user":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v22, :cond_d

    const/16 v23, 0x4

    invoke-static/range {v22 .. v23}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v13

    .line 306
    .local v13, "reasonImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_6
    if-eqz v13, :cond_e

    .line 307
    iget-object v0, v13, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    iget-boolean v0, v13, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 309
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, p3

    move-object/from16 v3, p6

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReasonUser(Lcom/google/android/play/image/FifeImageView;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 310
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 264
    .end local v4    # "author":Ljava/lang/String;
    .end local v7    # "hasAuthor":Z
    .end local v8    # "hasComment":Z
    .end local v9    # "hasTitle":Z
    .end local v13    # "reasonImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v18    # "reviewText":Ljava/lang/String;
    .end local v21    # "stars":I
    .end local v22    # "user":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 265
    .restart local v4    # "author":Ljava/lang/String;
    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 267
    .restart local v7    # "hasAuthor":Z
    .restart local v21    # "stars":I
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 268
    .restart local v9    # "hasTitle":Z
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 277
    .restart local v8    # "hasComment":Z
    :cond_5
    if-nez v8, :cond_6

    if-eqz v9, :cond_8

    .line 280
    :cond_6
    const v24, 0x7f10000a

    const/16 v23, 0x3

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v23, 0x0

    aput-object v4, v25, v23

    const/16 v23, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v25, v23

    const/16 v26, 0x2

    if-eqz v8, :cond_7

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    move-object/from16 v23, v0

    :goto_7
    aput-object v23, v25, v26

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "reviewText":Ljava/lang/String;
    goto/16 :goto_5

    .end local v18    # "reviewText":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    move-object/from16 v23, v0

    goto :goto_7

    .line 284
    :cond_8
    const v23, 0x7f10000c

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v4, v24, v25

    const/16 v25, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    move-object/from16 v0, v16

    move/from16 v1, v23

    move/from16 v2, v21

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "reviewText":Ljava/lang/String;
    goto/16 :goto_5

    .line 288
    .end local v18    # "reviewText":Ljava/lang/String;
    :cond_9
    if-nez v8, :cond_a

    if-eqz v9, :cond_c

    .line 290
    :cond_a
    const v24, 0x7f10000b

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v23, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v25, v23

    const/16 v26, 0x1

    if-eqz v8, :cond_b

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    move-object/from16 v23, v0

    :goto_8
    aput-object v23, v25, v26

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v21

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "reviewText":Ljava/lang/String;
    goto/16 :goto_5

    .end local v18    # "reviewText":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    move-object/from16 v23, v0

    goto :goto_8

    .line 294
    :cond_c
    const v23, 0x7f10000d

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    move-object/from16 v0, v16

    move/from16 v1, v23

    move/from16 v2, v21

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "reviewText":Ljava/lang/String;
    goto/16 :goto_5

    .line 302
    .restart local v22    # "user":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_6

    .line 312
    .restart local v13    # "reasonImage":Lcom/google/android/finsky/protos/Common$Image;
    :cond_e
    const/16 v23, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 314
    .end local v4    # "author":Ljava/lang/String;
    .end local v7    # "hasAuthor":Z
    .end local v8    # "hasComment":Z
    .end local v9    # "hasTitle":Z
    .end local v13    # "reasonImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v17    # "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v18    # "reviewText":Ljava/lang/String;
    .end local v21    # "stars":I
    .end local v22    # "user":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_f
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    move-object/from16 v23, v0

    if-eqz v23, :cond_12

    .line 317
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    .line 318
    .local v12, "profiles":Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v5, v0

    .line 319
    .local v5, "count":I
    const/16 v23, 0x1

    move/from16 v0, v23

    if-le v5, v0, :cond_10

    .line 320
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v6, v23, v24

    .line 321
    .local v6, "firstUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    aget-object v19, v23, v24

    .line 323
    .local v19, "secondUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    const v23, 0x7f0c032a

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    const/16 v25, 0x1

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 327
    const/16 v23, 0x4

    move/from16 v0, v23

    invoke-static {v6, v0}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v10

    .line 328
    .local v10, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v0, v10, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    iget-boolean v0, v10, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 330
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-static {v0, v6, v1, v2}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReasonUser(Lcom/google/android/play/image/FifeImageView;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 331
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 332
    .end local v6    # "firstUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v10    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v19    # "secondUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_10
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v5, v0, :cond_11

    .line 333
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v6, v23, v24

    .line 335
    .restart local v6    # "firstUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    const v23, 0x7f0c0329

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    move-object/from16 v0, v16

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 339
    const/16 v23, 0x4

    move/from16 v0, v23

    invoke-static {v6, v0}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v10

    .line 340
    .restart local v10    # "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v0, v10, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    iget-boolean v0, v10, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 341
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-static {v0, v6, v1, v2}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReasonUser(Lcom/google/android/play/image/FifeImageView;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 342
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 344
    .end local v6    # "firstUser":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v10    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_11
    const-string v23, "Server returned plus profile reason with no profiles"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 346
    .end local v5    # "count":I
    .end local v12    # "profiles":Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    :cond_12
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    move-object/from16 v23, v0

    if-eqz v23, :cond_13

    .line 347
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonUserAction:Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    .line 349
    .local v15, "reasonUserAction":Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;
    iget-object v0, v15, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 352
    iget-object v11, v15, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 353
    .local v11, "person":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    const/16 v23, 0x4

    move/from16 v0, v23

    invoke-static {v11, v0}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v10

    .line 354
    .restart local v10    # "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v0, v10, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    iget-boolean v0, v10, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v24, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 356
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 357
    move-object/from16 v0, v20

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-static {v0, v11, v1, v2}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindReasonUser(Lcom/google/android/play/image/FifeImageView;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0

    .line 360
    .end local v10    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v11    # "person":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v15    # "reasonUserAction":Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;
    :cond_13
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayCardSnippet;->setSnippetText(Ljava/lang/CharSequence;II)V

    .line 362
    const/16 v23, 0x8

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private static bindReasonUser(Lcom/google/android/play/image/FifeImageView;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p0, "reasonAvatarImage"    # Lcom/google/android/play/image/FifeImageView;
    .param p1, "reasonUser"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "cardNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 368
    new-instance v0, Lcom/google/android/finsky/utils/PlayCardUtils$7;

    invoke-direct {v0, p2, p1, p3}, Lcom/google/android/finsky/utils/PlayCardUtils$7;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 381
    return-void
.end method

.method private static configureActions(Lcom/google/android/play/layout/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 18
    .param p0, "popupMenu"    # Lcom/google/android/play/layout/PlayPopupMenu;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "clickedNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 972
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v12

    .line 973
    .local v12, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 974
    .local v2, "currentAccount":Landroid/accounts/Account;
    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v3

    .line 975
    .local v3, "installer":Lcom/google/android/finsky/receivers/Installer;
    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v4

    .line 976
    .local v4, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v5

    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v6

    sget-object v8, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    move-object/from16 v7, p2

    invoke-static/range {v2 .. v8}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    .line 979
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 980
    .local v16, "resources":Landroid/content/res/Resources;
    const/4 v11, 0x0

    .line 981
    .local v11, "actionsAdded":I
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-virtual {v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasAction()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 982
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v6, v6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge v13, v6, :cond_1

    .line 983
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-virtual {v6, v13}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->getAction(I)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    move-result-object v5

    .line 984
    .local v5, "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    invoke-static {v5}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->canCreateClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 985
    const-string v6, "Can\'t make click listener for action %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v5, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 982
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 988
    :cond_0
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v6, v6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    sget-object v7, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleLong(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 990
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v6, v6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    const/4 v8, 0x0

    move-object/from16 v7, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p1

    invoke-static/range {v5 .. v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v15

    .line 994
    .local v15, "purchaseOpenListener":Landroid/view/View$OnClickListener;
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v14

    .line 995
    .local v14, "priceActionTitle":Ljava/lang/CharSequence;
    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/finsky/utils/PlayCardUtils$9;

    invoke-direct {v7, v15}, Lcom/google/android/finsky/utils/PlayCardUtils$9;-><init>(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v6, v7}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 1002
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1005
    .end local v5    # "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .end local v13    # "i":I
    .end local v14    # "priceActionTitle":Ljava/lang/CharSequence;
    .end local v15    # "purchaseOpenListener":Landroid/view/View$OnClickListener;
    :cond_1
    if-nez v11, :cond_2

    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-virtual {v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasStatus()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1006
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    sget-object v7, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStatus(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 1007
    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v17

    .line 1008
    .local v17, "statusTitle":Ljava/lang/String;
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6, v7}, Lcom/google/android/play/layout/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 1010
    .end local v17    # "statusTitle":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private static configureOverflowView(Lcom/google/android/play/layout/PlayCardViewBase;Landroid/widget/ImageView;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 8
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p1, "overflowView"    # Landroid/widget/ImageView;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p5, "clickedNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 822
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 823
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/finsky/utils/PlayCardUtils$8;

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move-object v5, p3

    move-object v6, p0

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/PlayCardUtils$8;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 898
    return-void
.end method

.method private static convertCardTypeToUiElementType(I)I
    .locals 3
    .param p0, "cardType"    # I

    .prologue
    .line 1048
    packed-switch p0, :pswitch_data_0

    .line 1082
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown card type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050
    :pswitch_0
    const/16 v0, 0x1fd

    .line 1080
    :goto_0
    return v0

    .line 1052
    :pswitch_1
    const/16 v0, 0x1ff

    goto :goto_0

    .line 1055
    :pswitch_2
    const/16 v0, 0x1fb

    goto :goto_0

    .line 1057
    :pswitch_3
    const/16 v0, 0x1fc

    goto :goto_0

    .line 1059
    :pswitch_4
    const/16 v0, 0x1f9

    goto :goto_0

    .line 1061
    :pswitch_5
    const/16 v0, 0x1fa

    goto :goto_0

    .line 1063
    :pswitch_6
    const/16 v0, 0x200

    goto :goto_0

    .line 1065
    :pswitch_7
    const/16 v0, 0x201

    goto :goto_0

    .line 1067
    :pswitch_8
    const/16 v0, 0x1f8

    goto :goto_0

    .line 1069
    :pswitch_9
    const/16 v0, 0x1fe

    goto :goto_0

    .line 1071
    :pswitch_a
    const/16 v0, 0x206

    goto :goto_0

    .line 1073
    :pswitch_b
    const/16 v0, 0x1f5

    goto :goto_0

    .line 1075
    :pswitch_c
    const/16 v0, 0x203

    goto :goto_0

    .line 1077
    :pswitch_d
    const/16 v0, 0x202

    goto :goto_0

    .line 1080
    :pswitch_e
    const/4 v0, 0x0

    goto :goto_0

    .line 1048
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_3
        :pswitch_4
        :pswitch_c
        :pswitch_d
        :pswitch_a
        :pswitch_e
    .end packed-switch
.end method

.method public static findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .locals 1
    .param p0, "reasons"    # Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .prologue
    .line 455
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;Lcom/google/android/finsky/protos/DocumentV2$Reason;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v0

    return-object v0
.end method

.method public static findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;Lcom/google/android/finsky/protos/DocumentV2$Reason;)Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .locals 4
    .param p0, "reasons"    # Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    .param p1, "reasonToIgnore"    # Lcom/google/android/finsky/protos/DocumentV2$Reason;

    .prologue
    .line 465
    if-nez p0, :cond_1

    .line 466
    const/4 v0, 0x0

    .line 491
    :cond_0
    :goto_0
    return-object v0

    .line 473
    :cond_1
    const/4 v0, 0x0

    .line 474
    .local v0, "highestPri":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 475
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    aget-object v2, v3, v1

    .line 477
    .local v2, "reason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-ne v2, p1, :cond_3

    .line 474
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 480
    :cond_3
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v3, :cond_4

    .line 481
    move-object v0, v2

    .line 482
    goto :goto_0

    .line 483
    :cond_4
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-eqz v3, :cond_5

    .line 484
    move-object v0, v2

    goto :goto_2

    .line 485
    :cond_5
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    if-eqz v0, :cond_6

    iget-object v3, v0, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-nez v3, :cond_2

    .line 487
    :cond_6
    move-object v0, v2

    goto :goto_2
.end method

.method public static getCardParentNode(Lcom/google/android/play/layout/PlayCardViewBase;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 2
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 1110
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;

    .line 1111
    .local v0, "loggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    if-nez v0, :cond_0

    .line 1112
    const/4 v1, 0x0

    .line 1115
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDocDisplaySubtitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;
    .locals 2
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 495
    const/4 v0, 0x0

    .line 496
    .local v0, "subtitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 515
    :goto_0
    :pswitch_0
    return-object v0

    .line 505
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v0

    .line 506
    goto :goto_0

    .line 512
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static initializeCardTrackers()V
    .locals 3

    .prologue
    .line 133
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$1;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$1;-><init>()V

    invoke-interface {v1, v2}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 143
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$2;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$2;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 155
    invoke-static {}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->getInstance()Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$3;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$3;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->registerListener(Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker$CardLifecycleListener;)V

    .line 168
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->getInstance()Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;

    move-result-object v0

    .line 172
    .local v0, "customizerRepository":Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;, "Lcom/google/android/finsky/utils/PlayCardCustomizerRepository<Lcom/google/android/play/layout/PlayCardViewBase;>;"
    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$4;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$4;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->registerCardCustomizer(ILcom/google/android/finsky/utils/PlayCardCustomizer;)V

    .line 206
    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$5;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$5;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->registerCardCustomizer(ILcom/google/android/finsky/utils/PlayCardCustomizer;)V

    .line 219
    const/16 v1, 0xa

    new-instance v2, Lcom/google/android/finsky/utils/PlayCardUtils$6;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PlayCardUtils$6;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/PlayCardCustomizerRepository;->registerCardCustomizer(ILcom/google/android/finsky/utils/PlayCardCustomizer;)V

    .line 230
    return-void
.end method

.method public static recycleLoggingData(Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 1
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 1091
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;

    .line 1092
    .local v0, "loggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    if-eqz v0, :cond_0

    .line 1093
    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;->onRecycle()V

    .line 1095
    :cond_0
    return-void
.end method

.method private static setLoggingDataIfNecessary(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p0, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p1, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 1099
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;

    .line 1100
    .local v0, "loggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    if-eqz v0, :cond_0

    .line 1101
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;->setParentNode(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1107
    :goto_0
    return-void

    .line 1105
    :cond_0
    new-instance v0, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;

    .end local v0    # "loggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;-><init>(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1106
    .restart local v0    # "loggingData":Lcom/google/android/finsky/utils/PlayCardUtils$CardUiElementNode;
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->setLoggingData(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static declared-synchronized updateCardLabel(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 17
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 519
    const-class v16, Lcom/google/android/finsky/utils/PlayCardUtils;

    monitor-enter v16

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getLabel()Lcom/google/android/play/layout/PlayCardLabelView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 520
    .local v10, "label":Lcom/google/android/play/layout/PlayCardLabelView;
    if-eqz p0, :cond_0

    if-nez v10, :cond_1

    .line 581
    :cond_0
    :goto_0
    monitor-exit v16

    return-void

    .line 527
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getOwnershipRenderingType()I

    move-result v11

    .line 529
    .local v11, "ownershipRenderingType":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    .line 530
    .local v9, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    sget-object v7, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    move-object/from16 v6, p0

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    .line 534
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    sget-object v2, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getListingStyle(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 536
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget-boolean v8, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 537
    .local v8, "displayAsOwned":Z
    and-int/lit8 v1, v11, 0x1

    if-eqz v1, :cond_3

    const/4 v14, 0x1

    .line 539
    .local v14, "supportsIconRenderingForOwnedItems":Z
    :goto_1
    and-int/lit8 v1, v11, 0x2

    if-eqz v1, :cond_4

    const/4 v15, 0x1

    .line 542
    .local v15, "supportsLabelRenderingForOwnedItems":Z
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/play/layout/PlayCardViewBase;->setItemOwned(Z)V

    .line 543
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 545
    if-eqz v8, :cond_5

    if-eqz v14, :cond_5

    .line 546
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 547
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getOwnedItemIndicator(I)I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setIcon(I)V

    .line 554
    :goto_3
    if-eqz v8, :cond_2

    if-eqz v15, :cond_6

    :cond_2
    const/4 v13, 0x1

    .line 555
    .local v13, "showLabel":Z
    :goto_4
    if-eqz v13, :cond_8

    .line 556
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 559
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    iget-object v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isHideSalesPricesExperimentEnabled()Z

    move-result v1

    if-nez v1, :cond_7

    .line 561
    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c03ab

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    iget-object v5, v5, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    iget-object v5, v5, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 564
    .local v12, "priceDescription":Ljava/lang/String;
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    iget-object v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    sget-object v2, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    iget-object v2, v2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-virtual {v10, v1, v2, v3, v12}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 577
    .end local v12    # "priceDescription":Ljava/lang/String;
    :goto_5
    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardLabelView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    if-eqz v8, :cond_0

    if-eqz v14, :cond_0

    if-nez v15, :cond_0

    .line 579
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 519
    .end local v8    # "displayAsOwned":Z
    .end local v9    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    .end local v10    # "label":Lcom/google/android/play/layout/PlayCardLabelView;
    .end local v11    # "ownershipRenderingType":I
    .end local v13    # "showLabel":Z
    .end local v14    # "supportsIconRenderingForOwnedItems":Z
    .end local v15    # "supportsLabelRenderingForOwnedItems":Z
    :catchall_0
    move-exception v1

    monitor-exit v16

    throw v1

    .line 537
    .restart local v8    # "displayAsOwned":Z
    .restart local v9    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    .restart local v10    # "label":Lcom/google/android/play/layout/PlayCardLabelView;
    .restart local v11    # "ownershipRenderingType":I
    :cond_3
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 539
    .restart local v14    # "supportsIconRenderingForOwnedItems":Z
    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 549
    .restart local v15    # "supportsLabelRenderingForOwnedItems":Z
    :cond_5
    :try_start_2
    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardLabelView;->clearIcon()V

    goto :goto_3

    .line 554
    :cond_6
    const/4 v13, 0x0

    goto :goto_4

    .line 567
    .restart local v13    # "showLabel":Z
    :cond_7
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-virtual {v10}, Lcom/google/android/play/layout/PlayCardLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v10, v1, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;I)V

    goto :goto_5

    .line 572
    :cond_8
    const/4 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v10, v1, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method

.method public static updatePrimaryActionButton(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 13
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 917
    if-eqz p0, :cond_0

    if-nez p3, :cond_1

    .line 963
    :cond_0
    :goto_0
    return-void

    .line 921
    :cond_1
    const v1, 0x7f0a02c3

    invoke-virtual {p2, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/play/layout/PlayActionButton;

    .line 923
    .local v7, "button":Lcom/google/android/play/layout/PlayActionButton;
    if-eqz v7, :cond_0

    .line 928
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 929
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    .line 932
    .local v8, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    sget-object v6, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    .line 938
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    sget-object v2, Lcom/google/android/finsky/utils/PlayCardUtils;->sListingStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getListingStyle(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 939
    const/4 v11, 0x0

    .line 940
    .local v11, "purchaseOpenListener":Landroid/view/View$OnClickListener;
    const/4 v10, 0x0

    .line 941
    .local v10, "priceActionTitle":Ljava/lang/CharSequence;
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasAction()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 942
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 943
    .local v12, "resources":Landroid/content/res/Resources;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge v9, v1, :cond_3

    .line 944
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-virtual {v1, v9}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->getAction(I)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    move-result-object v0

    .line 945
    .local v0, "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    invoke-static {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->canCreateClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 946
    const-string v1, "Can\'t make click listener for action %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 943
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 949
    :cond_2
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    sget-object v2, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleLong(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 951
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sDocumentActions:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    const/4 v3, 0x0

    move-object/from16 v2, p4

    move-object/from16 v4, p3

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v11

    .line 953
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardUtils;->sActionStyle:Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-virtual {v1, v12}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v10

    .line 958
    .end local v0    # "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .end local v9    # "i":I
    .end local v12    # "resources":Landroid/content/res/Resources;
    :cond_3
    if-eqz v11, :cond_0

    if-eqz v10, :cond_0

    .line 959
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2, v11}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 961
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private static updateTitleContentDescription(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 6
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 584
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 585
    .local v2, "title":Ljava/lang/CharSequence;
    :goto_0
    if-eqz p0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 600
    :cond_0
    :goto_1
    return-void

    .line 584
    .end local v2    # "title":Ljava/lang/CharSequence;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 589
    .restart local v2    # "title":Ljava/lang/CharSequence;
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 590
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getTitleContentDescriptionResourceId(Landroid/content/res/Resources;I)I

    move-result v3

    .line 593
    .local v3, "titleDescriptionResourceId":I
    if-ltz v3, :cond_0

    .line 594
    invoke-virtual {p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    .line 595
    .local v0, "cardTitleView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 596
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private static updateTrackedCardLabels()V
    .locals 3

    .prologue
    .line 238
    sget-object v2, Lcom/google/android/finsky/utils/PlayCardUtils;->sWindowAttachedCards:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 239
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/PlayCardUtils;->updateCardLabel(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V

    goto :goto_0

    .line 241
    .end local v0    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_0
    return-void
.end method
