.class public interface abstract Lcom/google/android/finsky/download/DownloadManagerFacade;
.super Ljava/lang/Object;
.source "DownloadManagerFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;
    }
.end annotation


# virtual methods
.method public abstract enqueue(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/download/Download;",
            "Lcom/google/android/finsky/utils/ParameterizedRunnable",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getFileUriForContentUri(Landroid/net/Uri;)Landroid/net/Uri;
.end method

.method public abstract getUriFromBroadcast(Landroid/content/Intent;)Landroid/net/Uri;
.end method

.method public abstract query(Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation
.end method

.method public abstract remove(Landroid/net/Uri;)V
.end method

.method public abstract unregisterListener(Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)V
.end method
