.class public Lcom/google/android/finsky/layout/EpisodeList;
.super Landroid/widget/LinearLayout;
.source "EpisodeList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
.implements Lcom/google/android/finsky/library/Libraries$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;,
        Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;
    }
.end annotation


# instance fields
.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mEpisodeIdFromBundle:Ljava/lang/String;

.field private final mEpisodeSnippets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/layout/EpisodeSnippet;",
            ">;"
        }
    .end annotation
.end field

.field private mEpisodesContainer:Landroid/widget/LinearLayout;

.field private mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mInProgressSnippet:Landroid/view/View;

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mLoadingOverlay:Landroid/view/View;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mOldSeason:Lcom/google/android/finsky/api/model/Document;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSeasonIdFromBundle:Ljava/lang/String;

.field private mSeasonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mSeasonSelectedListener:Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

.field private mSeasonSpinner:Landroid/widget/Spinner;

.field private mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

.field private mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    .line 103
    const/16 v0, 0xd3

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/EpisodeList;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/EpisodeList;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method private hideUi()V
    .locals 2

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 324
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 329
    :goto_0
    return-void

    .line 327
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/EpisodeList;->setVisibility(I)V

    goto :goto_0
.end method

.method private setDefaultSelectionState(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "selectedSeasonId"    # Ljava/lang/String;
    .param p2, "selectedEpisodeId"    # Ljava/lang/String;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeIdFromBundle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iput-object p2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeIdFromBundle:Ljava/lang/String;

    .line 219
    :cond_1
    return-void
.end method

.method private setEpisodeList(Lcom/google/android/finsky/api/model/Document;Ljava/util/List;)V
    .locals 13
    .param p1, "selectedSeason"    # Lcom/google/android/finsky/api/model/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/Document;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p2, "episodeList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 230
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 233
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    .line 234
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonNode()V

    .line 237
    const/4 v11, 0x0

    .line 238
    .local v11, "selectedSnippet":Lcom/google/android/finsky/layout/EpisodeSnippet;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 239
    .local v2, "episode":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040091

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesContainer:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/EpisodeSnippet;

    .line 242
    .local v0, "snippet":Lcom/google/android/finsky/layout/EpisodeSnippet;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v10

    .line 244
    .local v10, "library":Lcom/google/android/finsky/library/Library;
    invoke-static {v2, v10}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v9

    .line 245
    .local v9, "isOwned":Z
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/Set;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    .line 246
    .local v12, "wasOwned":Z
    if-eqz v9, :cond_2

    if-nez v12, :cond_2

    const/4 v5, 0x1

    .line 248
    .local v5, "isNewPurchase":Z
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeList;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v6, p0

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setEpisodeDetails(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 250
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 251
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeIdFromBundle:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    move-object v11, v0

    goto :goto_0

    .line 246
    .end local v5    # "isNewPurchase":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 256
    .end local v0    # "snippet":Lcom/google/android/finsky/layout/EpisodeSnippet;
    .end local v2    # "episode":Lcom/google/android/finsky/api/model/Document;
    .end local v9    # "isOwned":Z
    .end local v10    # "library":Lcom/google/android/finsky/library/Library;
    .end local v12    # "wasOwned":Z
    :cond_3
    if-eqz v11, :cond_4

    .line 257
    invoke-virtual {v11}, Lcom/google/android/finsky/layout/EpisodeSnippet;->expand()V

    .line 261
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->hasSeasonOffer(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isInProgressSeason()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 263
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mInProgressSnippet:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 271
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeIdFromBundle:Ljava/lang/String;

    .line 272
    return-void

    .line 265
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mInProgressSnippet:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private shouldEnableLoadingOverlay(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 533
    if-eqz p1, :cond_0

    .line 534
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLoadingOverlay:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLoadingOverlay:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/layout/EpisodeList$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/EpisodeList$1;-><init>(Lcom/google/android/finsky/layout/EpisodeList;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 545
    :goto_0
    return-void

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLoadingOverlay:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 543
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLoadingOverlay:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private updateSeasonBuyButton()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 454
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/layout/EpisodeList;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v3, v2

    move-object v4, v2

    move-object v8, p0

    invoke-static/range {v0 .. v8}, Lcom/google/android/finsky/layout/EpisodeSnippet;->updateBuyButtonState(Landroid/content/res/Resources;Lcom/google/android/play/layout/PlayActionButton;Landroid/widget/TextView;Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 456
    return-void
.end method

.method private updateSeasonNode()V
    .locals 4

    .prologue
    .line 281
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v0, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 283
    .local v0, "currentCookie":[B
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    .line 284
    .local v1, "selectedSeasonCookie":[B
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 286
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->emptyArray()[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 287
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    .line 288
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v3, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 292
    .end local v0    # "currentCookie":[B
    .end local v1    # "selectedSeasonCookie":[B
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 293
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSelectedListener:Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

    if-eqz v2, :cond_1

    .line 294
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSelectedListener:Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v2, v3}, Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;->onSeasonSelected(Lcom/google/android/finsky/api/model/Document;)V

    .line 296
    :cond_1
    return-void
.end method


# virtual methods
.method public addSeasonSelectionListener(Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSelectedListener:Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

    .line 519
    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 561
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 562
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public getSelectedEpisodeId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 309
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    if-eqz v2, :cond_1

    .line 310
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 311
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/EpisodeSnippet;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/EpisodeSnippet;->isExpanded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 316
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelectedSeasonId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAllLibrariesLoaded()V
    .locals 0

    .prologue
    .line 463
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0401bc

    invoke-direct {v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 142
    :cond_0
    return-void
.end method

.method public onCollapsedStateChanged(Lcom/google/android/finsky/layout/EpisodeSnippet;Z)V
    .locals 3
    .param p1, "snippet"    # Lcom/google/android/finsky/layout/EpisodeSnippet;
    .param p2, "collapsed"    # Z

    .prologue
    .line 385
    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeSnippets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/EpisodeSnippet;

    .line 386
    .local v0, "current":Lcom/google/android/finsky/layout/EpisodeSnippet;
    if-eq v0, p1, :cond_0

    .line 387
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->collapseWithoutNotifyingListeners()V

    goto :goto_0

    .line 390
    .end local v0    # "current":Lcom/google/android/finsky/layout/EpisodeSnippet;
    :cond_1
    return-void
.end method

.method public onDataChanged()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 492
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/EpisodeList;->shouldEnableLoadingOverlay(Z)V

    .line 493
    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    iget-object v5, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 495
    .local v2, "firstTimeThroughForSeasonId":Z
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 498
    .local v4, "ownedDocumentsFromLibrary":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 499
    .local v1, "episodes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 500
    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v5, v3}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 501
    .local v0, "episode":Lcom/google/android/finsky/api/model/Document;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    if-eqz v2, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {v0, v5}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 503
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 499
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 510
    .end local v0    # "episode":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    if-eqz v2, :cond_4

    .line 511
    new-instance v5, Landroid/util/Pair;

    iget-object v6, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    .line 514
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0, v5, v1}, Lcom/google/android/finsky/layout/EpisodeList;->setEpisodeList(Lcom/google/android/finsky/api/model/Document;Ljava/util/List;)V

    .line 515
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 153
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    .line 154
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 155
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v3, 0x0

    .line 476
    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/EpisodeList;->shouldEnableLoadingOverlay(Z)V

    .line 478
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mOldSeason:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mOldSeason:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    if-eq v0, v1, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mOldSeason:Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    .line 482
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonNode()V

    .line 483
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mOldSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 486
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 488
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 125
    const v0, 0x7f0a01c1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesContainer:Landroid/widget/LinearLayout;

    .line 126
    const v0, 0x7f0a01c2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLoadingOverlay:Landroid/view/View;

    .line 127
    const v0, 0x7f0a01c0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    .line 128
    const v0, 0x7f0a0138

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 129
    const v0, 0x7f0a01be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mInProgressSnippet:Landroid/view/View;

    .line 130
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 431
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mOldSeason:Lcom/google/android/finsky/api/model/Document;

    .line 432
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    .line 433
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonNode()V

    .line 435
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonBuyButton()V

    .line 437
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/EpisodeList;->shouldEnableLoadingOverlay(Z)V

    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 442
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 446
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    .line 448
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 449
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 450
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeasonRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 451
    return-void
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 1
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    .line 468
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonBuyButton()V

    .line 469
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->onDataChanged()V

    .line 471
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public removeSeasonSelectionListener()V
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSelectedListener:Lcom/google/android/finsky/layout/EpisodeList$SeasonSelectionListener;

    .line 526
    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 414
    const-string v1, "SeasonListViewBinder.SelectedSeasonId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    const-string v1, "SeasonListViewBinder.SelectedSeasonId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    .line 417
    :cond_0
    const-string v1, "SeasonListViewBinder.SelectedEpisodeId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 418
    const-string v1, "SeasonListViewBinder.SelectedEpisodeId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodeIdFromBundle:Ljava/lang/String;

    .line 420
    :cond_1
    const-string v1, "SeasonListViewBinder.OwnedEpisodes"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 421
    .local v0, "ownedEpisodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    .line 422
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    .line 425
    :cond_2
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getSelectedSeasonId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 397
    const-string v1, "SeasonListViewBinder.SelectedSeasonId"

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getSelectedSeasonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getSelectedEpisodeId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400
    const-string v1, "SeasonListViewBinder.SelectedEpisodeId"

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getSelectedEpisodeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 404
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeList;->mEpisodesInLibraryFromFirstLoad:Landroid/util/Pair;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    .line 405
    .local v0, "docs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v1, "SeasonListViewBinder.OwnedEpisodes"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 408
    .end local v0    # "docs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public setSeasonList(Lcom/google/android/finsky/fragments/PageFragment;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Landroid/os/Bundle;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 7
    .param p1, "parentFragment"    # Lcom/google/android/finsky/fragments/PageFragment;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "restoreBundle"    # Landroid/os/Bundle;
    .param p8, "selectedSeasonId"    # Ljava/lang/String;
    .param p9, "selectedEpisodeId"    # Ljava/lang/String;
    .param p10, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/fragments/PageFragment;",
            "Lcom/google/android/finsky/api/DfeApi;",
            "Lcom/google/android/finsky/library/Libraries;",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Lcom/google/android/play/image/BitmapLoader;",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 166
    .local p7, "seasonList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    if-eqz p7, :cond_0

    invoke-interface {p7}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->hideUi()V

    .line 205
    :goto_0
    return-void

    .line 171
    :cond_1
    iput-object p7, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    .line 172
    iput-object p3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 173
    iput-object p2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 174
    iput-object p4, p0, Lcom/google/android/finsky/layout/EpisodeList;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 175
    iput-object p5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 176
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeList;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 178
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 179
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 181
    move-object/from16 v0, p9

    invoke-direct {p0, p8, v0}, Lcom/google/android/finsky/layout/EpisodeList;->setDefaultSelectionState(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeList;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    invoke-direct {v4, p0, v5, v6}, Lcom/google/android/finsky/layout/EpisodeList$SeasonListAdapter;-><init>(Lcom/google/android/finsky/layout/EpisodeList;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 184
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 187
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    iput-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    .line 188
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 189
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 190
    .local v2, "season":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonIdFromBundle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 191
    iput-object v2, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    goto :goto_1

    .line 195
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "season":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonNode()V

    .line 197
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSelectedSeason:Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v4, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 198
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 199
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 200
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeList;->mSeasonSpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 204
    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeList;->updateSeasonBuyButton()V

    goto/16 :goto_0
.end method
