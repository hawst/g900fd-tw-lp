.class public final Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedeemCodeRequest"
.end annotation


# instance fields
.field public address:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field public addressCheckedCheckboxId:[Ljava/lang/String;

.field public code:Ljava/lang/String;

.field public consumptionAppVersionCode:J

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasCode:Z

.field public hasConsumptionAppVersionCode:Z

.field public hasHasUserConfirmation:Z

.field public hasOfferType:Z

.field public hasPartnerPayload:Z

.field public hasPaymentsIntegratorClientContextToken:Z

.field public hasRedemptionContext:Z

.field public hasToken:Z

.field public hasUserConfirmation:Z

.field public offerType:I

.field public partnerPayload:Ljava/lang/String;

.field public paymentsIntegratorClientContextToken:[B

.field public redemptionContext:I

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->clear()Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    .line 75
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    .line 79
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasCode:Z

    .line 80
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    .line 81
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasHasUserConfirmation:Z

    .line 82
    iput-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 83
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    .line 85
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    .line 86
    iput v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    .line 87
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasRedemptionContext:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    .line 89
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPartnerPayload:Z

    .line 90
    iput-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 91
    iput v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    .line 92
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasOfferType:Z

    .line 93
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    .line 94
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    .line 95
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    .line 96
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPaymentsIntegratorClientContextToken:Z

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->cachedSize:I

    .line 98
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 147
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 148
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasCode:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 149
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    invoke-static {v7, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 152
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasHasUserConfirmation:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    if-eqz v5, :cond_3

    .line 153
    :cond_2
    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 156
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v5, :cond_4

    .line 157
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 160
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 163
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_6

    .line 164
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 165
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 166
    add-int/lit8 v0, v0, 0x1

    .line 167
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 163
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 171
    .end local v2    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v4, v1

    .line 172
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 174
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 175
    :cond_8
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 178
    :cond_9
    iget v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    if-ne v5, v7, :cond_a

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasRedemptionContext:Z

    if-eqz v5, :cond_b

    .line 179
    :cond_a
    const/4 v5, 0x6

    iget v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 182
    :cond_b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPartnerPayload:Z

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 183
    :cond_c
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 186
    :cond_d
    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v5, :cond_e

    .line 187
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 190
    :cond_e
    iget v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    if-ne v5, v7, :cond_f

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasOfferType:Z

    if-eqz v5, :cond_10

    .line 191
    :cond_f
    const/16 v5, 0x9

    iget v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 194
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    if-nez v5, :cond_11

    iget-wide v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_12

    .line 195
    :cond_11
    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 198
    :cond_12
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPaymentsIntegratorClientContextToken:Z

    if-nez v5, :cond_13

    iget-object v5, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    sget-object v6, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_14

    .line 199
    :cond_13
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v5

    add-int/2addr v4, v5

    .line 202
    :cond_14
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 211
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 215
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 216
    :sswitch_0
    return-object p0

    .line 221
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    .line 222
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasCode:Z

    goto :goto_0

    .line 226
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    .line 227
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasHasUserConfirmation:Z

    goto :goto_0

    .line 231
    :sswitch_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v6, :cond_1

    .line 232
    new-instance v6, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 234
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 238
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 240
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    if-nez v6, :cond_3

    move v1, v5

    .line 241
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 242
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 243
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 245
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 246
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 247
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 240
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 250
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 251
    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    goto :goto_0

    .line 255
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    .line 256
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    goto :goto_0

    .line 260
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 261
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 267
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    .line 268
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasRedemptionContext:Z

    goto :goto_0

    .line 274
    .end local v4    # "value":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    .line 275
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPartnerPayload:Z

    goto/16 :goto_0

    .line 279
    :sswitch_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v6, :cond_5

    .line 280
    new-instance v6, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 282
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 286
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 287
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto/16 :goto_0

    .line 300
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    .line 301
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasOfferType:Z

    goto/16 :goto_0

    .line 307
    .end local v4    # "value":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    .line 308
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    goto/16 :goto_0

    .line 312
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    .line 313
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPaymentsIntegratorClientContextToken:Z

    goto/16 :goto_0

    .line 211
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch

    .line 261
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 287
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 104
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasCode:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 105
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->code:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasHasUserConfirmation:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    if-eqz v2, :cond_3

    .line 108
    :cond_2
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasUserConfirmation:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 110
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v2, :cond_4

    .line 111
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 113
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 114
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 115
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->addressCheckedCheckboxId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 116
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 117
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 114
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasToken:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 122
    :cond_7
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->token:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 124
    :cond_8
    iget v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    if-ne v2, v4, :cond_9

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasRedemptionContext:Z

    if-eqz v2, :cond_a

    .line 125
    :cond_9
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->redemptionContext:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 127
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPartnerPayload:Z

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 128
    :cond_b
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->partnerPayload:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 130
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_d

    .line 131
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 133
    :cond_d
    iget v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    if-ne v2, v4, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasOfferType:Z

    if-eqz v2, :cond_f

    .line 134
    :cond_e
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->offerType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 136
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasConsumptionAppVersionCode:Z

    if-nez v2, :cond_10

    iget-wide v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_11

    .line 137
    :cond_10
    const/16 v2, 0xa

    iget-wide v4, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->consumptionAppVersionCode:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 139
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->hasPaymentsIntegratorClientContextToken:Z

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_13

    .line 140
    :cond_12
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeRequest;->paymentsIntegratorClientContextToken:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 142
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 143
    return-void
.end method
