.class Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;
.super Ljava/util/AbstractList;
.source "ArrayUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/ArrayUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArrayAsList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mArray:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;, "Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;->mArray:[Ljava/lang/Object;

    .line 21
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;, "Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;->mArray:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;, "Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/ArrayUtils$ArrayAsList;->mArray:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method
