.class public Lcom/google/android/finsky/analytics/FinskyEventLog;
.super Ljava/lang/Object;
.source "FinskyEventLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;
    }
.end annotation


# static fields
.field private static sInitializedImpressionId:Z

.field private static sLogTestListener:Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;

.field private static sNextImpressionId:J


# instance fields
.field private final mEventLogger:Lcom/google/android/play/analytics/EventLogger;

.field private final mExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

.field private final mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/finsky/experiments/FinskyExperiments;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "experiments"    # Lcom/google/android/finsky/experiments/FinskyExperiments;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v2, 0x0

    .line 109
    .local v2, "logger":Lcom/google/android/play/analytics/EventLogger;
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    .line 110
    if-eqz p2, :cond_0

    sget-object v3, Lcom/google/android/finsky/config/G;->enablePlayLogs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    new-instance v12, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v12}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    .line 113
    .local v12, "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    sget-object v3, Lcom/google/android/finsky/config/G;->playLogMaxStorageSize:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v12, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    .line 114
    sget-object v3, Lcom/google/android/finsky/config/G;->playLogRecommendedFileSize:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v12, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    .line 115
    sget-object v3, Lcom/google/android/finsky/config/G;->playLogDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v12, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    .line 116
    sget-object v3, Lcom/google/android/finsky/config/G;->playLogMinDelayBetweenUploadsMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v12, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    .line 117
    sget-object v3, Lcom/google/android/finsky/config/G;->playLogServerUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v12, Lcom/google/android/play/analytics/EventLogger$Configuration;->mServerUrl:Ljava/lang/String;

    .line 119
    const-string v3, "phone"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/telephony/TelephonyManager;

    .line 121
    .local v14, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v14}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v11

    .line 123
    .local v11, "mccmnc":Ljava/lang/String;
    new-instance v2, Lcom/google/android/play/analytics/EventLogger;

    .end local v2    # "logger":Lcom/google/android/play/analytics/EventLogger;
    const/4 v4, 0x0

    const-string v5, "androidmarket"

    sget-object v6, Lcom/google/android/play/analytics/EventLogger$LogSource;->MARKET:Lcom/google/android/play/analytics/EventLogger$LogSource;

    const/4 v7, 0x0

    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getVersionCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v3, p1

    move-object/from16 v13, p2

    invoke-direct/range {v2 .. v13}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V

    .line 127
    .end local v11    # "mccmnc":Ljava/lang/String;
    .end local v12    # "config":Lcom/google/android/play/analytics/EventLogger$Configuration;
    .end local v14    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .restart local v2    # "logger":Lcom/google/android/play/analytics/EventLogger;
    :cond_0
    iput-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    .line 128
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    .line 129
    return-void
.end method

.method static synthetic access$000(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p3, "x2"    # Z

    .prologue
    .line 49
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    return-void
.end method

.method private static addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V
    .locals 5
    .param p0, "leaf"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1, "clickEvent"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    .prologue
    .line 330
    :goto_0
    if-eqz p0, :cond_0

    .line 332
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 333
    .local v1, "treeElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-nez v1, :cond_1

    .line 334
    const-string v2, "Unexpected null PlayStoreUiElement from node %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    .end local v1    # "treeElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_0
    return-void

    .line 337
    .restart local v1    # "treeElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_1
    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 338
    .local v0, "pathElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iget-object v2, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v2, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v2, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 339
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object p0

    .line 340
    goto :goto_0
.end method

.method public static childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p0, "receiverNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 459
    invoke-interface {p1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 460
    .local v1, "newChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-nez v1, :cond_0

    .line 461
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "childNode has null element"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 463
    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v0

    .line 465
    .local v0, "foundChild":Z
    if-eqz v0, :cond_1

    iget-object v2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v2, v2

    if-nez v2, :cond_1

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_1
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public static cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 2
    .param p0, "original"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    .line 654
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 655
    .local v0, "copy":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-static {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V

    .line 656
    return-object v0
.end method

.method protected static cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V
    .locals 1
    .param p0, "from"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .param p1, "to"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    .line 661
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    iput v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    .line 662
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    iput-boolean v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    .line 663
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    iput-object v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 664
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    iput-object v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 665
    iget-object v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    .line 666
    return-void

    .line 665
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static deepCloneAndWipeChildren(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V
    .locals 6
    .param p0, "from"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .param p1, "to"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    .line 643
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V

    .line 644
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .local v0, "arr$":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 645
    .local v1, "fromChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    .line 646
    .local v4, "toChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-static {v1, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->deepCloneAndWipeChildren(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V

    .line 647
    iget-object v5, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v5, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v5, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 644
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 649
    .end local v1    # "fromChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .end local v4    # "toChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->emptyArray()[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 650
    return-void
.end method

.method public static findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 7
    .param p0, "receiverNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p1, "newChild"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    .line 481
    const/4 v1, 0x0

    .line 482
    .local v1, "foundChild":Z
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v5

    .line 483
    .local v5, "receiverElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iget-object v0, v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .local v0, "arr$":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 484
    .local v4, "receiverChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-static {p1, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->isEqual(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 485
    const/4 v1, 0x1

    .line 489
    .end local v4    # "receiverChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_0
    if-nez v1, :cond_1

    .line 490
    iget-object v6, v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v6, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v6, v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 492
    :cond_1
    return v1

    .line 483
    .restart local v4    # "receiverChild":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static flushImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/RootUiElementNode;)V
    .locals 3
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "impressionId"    # J
    .param p3, "rootNode"    # Lcom/google/android/finsky/layout/play/RootUiElementNode;

    .prologue
    .line 624
    invoke-interface {p3}, Lcom/google/android/finsky/layout/play/RootUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 629
    .local v0, "rootElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 632
    const/4 v1, 0x1

    invoke-static {p1, p2, p3, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    .line 633
    return-void
.end method

.method public static flushImpressionAtRoot(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p0, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 604
    :goto_0
    if-eqz p0, :cond_1

    .line 605
    instance-of v0, p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    if-eqz v0, :cond_0

    .line 606
    check-cast p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    .end local p0    # "parentNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/RootUiElementNode;->flushImpression()V

    .line 613
    .restart local p0    # "parentNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_1
    return-void

    .line 609
    :cond_0
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object p0

    goto :goto_0

    .line 612
    :cond_1
    const-string v0, "No RootUiElementNode found in parent chain"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private getActiveExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mExperiments:Lcom/google/android/finsky/experiments/FinskyExperiments;

    invoke-virtual {v0}, Lcom/google/android/finsky/experiments/FinskyExperiments;->getActiveExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getNextImpressionId()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 734
    sget-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    if-nez v0, :cond_0

    .line 735
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    xor-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    .line 736
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sInitializedImpressionId:Z

    .line 739
    :cond_0
    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    add-long/2addr v0, v4

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 740
    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    add-long/2addr v0, v4

    sput-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    .line 742
    :cond_1
    sget-wide v0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sNextImpressionId:J

    return-wide v0
.end method

.method public static isEqual(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 4
    .param p0, "a"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .param p1, "b"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 671
    if-ne p0, p1, :cond_1

    .line 680
    :cond_0
    :goto_0
    return v0

    .line 673
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 675
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    iget v3, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 677
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    iget-object v3, p1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static obtainPlayStoreBackgroundActionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    .prologue
    .line 185
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    return-object v0
.end method

.method public static obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    .locals 1

    .prologue
    .line 190
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    return-object v0
.end method

.method public static obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    return-object v0
.end method

.method public static obtainPlayStoreSearchEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreSearchEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v0

    return-object v0
.end method

.method public static obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 177
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 178
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iput p0, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    .line 179
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    .line 180
    return-object v0
.end method

.method public static pathToTree(Ljava/util/List;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;",
            ">;)",
            "Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;"
        }
    .end annotation

    .prologue
    .line 419
    .local p0, "path":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;>;"
    const/4 v3, 0x0

    .line 420
    .local v3, "previousNode":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 421
    .local v0, "current":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->cloneElement(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 422
    .local v1, "currentCopy":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v3, :cond_0

    .line 423
    iget-object v4, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v4, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v4, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 425
    :cond_0
    move-object v3, v1

    .line 426
    goto :goto_0

    .line 427
    .end local v0    # "current":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .end local v1    # "currentCopy":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_1
    return-object v3
.end method

.method public static removeChild(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z
    .locals 3
    .param p0, "parentNode"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .param p1, "childToRemove"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 503
    .local v0, "children":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 504
    aget-object v2, v0, v1

    invoke-static {v2, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->isEqual(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 505
    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ArrayUtils;->remove([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 506
    const/4 v2, 0x1

    .line 509
    :goto_1
    return v2

    .line 503
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 509
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p0, "node"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 724
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    .line 725
    .local v0, "parent":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    if-eqz v0, :cond_0

    .line 726
    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 728
    :cond_0
    return-void
.end method

.method public static requestImpressions(Landroid/view/ViewGroup;)V
    .locals 0
    .param p0, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 688
    if-eqz p0, :cond_0

    .line 689
    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressionsImpl(Landroid/view/ViewGroup;)V

    .line 691
    :cond_0
    return-void
.end method

.method private static requestImpressionsImpl(Landroid/view/ViewGroup;)V
    .locals 6
    .param p0, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 695
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    .line 696
    .local v4, "numChildren":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_1

    .line 697
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 698
    .local v1, "child":Landroid/view/View;
    instance-of v5, v1, Landroid/view/ViewGroup;

    if-eqz v5, :cond_0

    .line 699
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "child":Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressionsImpl(Landroid/view/ViewGroup;)V

    .line 696
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 703
    :cond_1
    instance-of v5, p0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v5, :cond_3

    .line 704
    check-cast p0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .end local p0    # "view":Landroid/view/ViewGroup;
    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 718
    :cond_2
    :goto_1
    return-void

    .line 705
    .restart local p0    # "view":Landroid/view/ViewGroup;
    :cond_3
    instance-of v5, p0, Lcom/google/android/play/layout/PlayCardViewBase;

    if-eqz v5, :cond_4

    move-object v0, p0

    .line 708
    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 709
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getLoggingData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 710
    .local v3, "loggingData":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    if-eqz v3, :cond_2

    .line 711
    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1

    .line 713
    .end local v0    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v3    # "loggingData":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :cond_4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v5, :cond_2

    .line 716
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1
.end method

.method public static rootImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "impressionId"    # J
    .param p3, "rootNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 525
    if-eqz p4, :cond_0

    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    if-nez v0, :cond_1

    .line 526
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null child node or element"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 528
    :cond_1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->rootImpressionImpl(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 529
    return-void
.end method

.method private static rootImpressionImpl(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 7
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "impressionId"    # J
    .param p3, "rootNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p4, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 551
    invoke-interface {p3}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    .line 552
    .local v0, "rootElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz p4, :cond_0

    .line 553
    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->findOrAddChild(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)Z

    .line 560
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 561
    new-instance v2, Lcom/google/android/finsky/analytics/FinskyEventLog$1;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/finsky/analytics/FinskyEventLog$1;-><init>(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    sget-object v1, Lcom/google/android/finsky/config/G;->playLogImpressionSettleTimeMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 567
    return-void
.end method

.method private static safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "array":[Ljava/lang/Object;, "[TT;"
    .local p1, "newElement":Ljava/lang/Object;, "TT;"
    const/4 v3, 0x0

    .line 264
    if-nez p1, :cond_0

    .line 265
    const-string v1, "Adding null to element array."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    .end local p0    # "array":[Ljava/lang/Object;, "[TT;"
    :goto_0
    return-object p0

    .line 271
    .restart local p0    # "array":[Ljava/lang/Object;, "[TT;"
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    array-length v2, p0

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 272
    .local v0, "newArray":[Ljava/lang/Object;, "[TT;"
    array-length v1, p0

    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    move-object p0, v0

    .line 276
    goto :goto_0
.end method

.method private sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .prologue
    .line 994
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    .line 995
    .local v0, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .line 996
    const-string v1, "4"

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 997
    return-void
.end method

.method private static sendImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 6
    .param p0, "impressionId"    # J
    .param p2, "rootNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p3, "dropIfEmpty"    # Z

    .prologue
    .line 579
    if-eqz p3, :cond_0

    .line 580
    invoke-interface {p2}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->child:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v3, v3

    if-nez v3, :cond_0

    .line 598
    :goto_0
    return-void

    .line 585
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/analytics/EventProtoCache;->getInstance()Lcom/google/android/finsky/analytics/EventProtoCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v1

    .line 587
    .local v1, "impression":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v2

    .line 588
    .local v2, "impressionRootElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    invoke-interface {p2}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->deepCloneAndWipeChildren(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;)V

    .line 589
    iput-object v2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 590
    const-wide/16 v4, 0x0

    cmp-long v3, p0, v4

    if-eqz v3, :cond_1

    .line 591
    iput-wide p0, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    .line 592
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    .line 596
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 597
    .local v0, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;)V

    goto :goto_0
.end method

.method private serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "logEvent"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    .prologue
    .line 135
    invoke-static {p2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    .line 136
    .local v0, "eventBytes":[B
    invoke-static {p1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->writeLogToListener(Ljava/lang/String;[B)V

    .line 137
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-nez v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mEventLogger:Lcom/google/android/play/analytics/EventLogger;

    invoke-direct {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getActiveExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[B[Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/EventProtoCache;->recycle(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    goto :goto_0
.end method

.method public static declared-synchronized setLogTestListener(Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;)V
    .locals 2
    .param p0, "listener"    # Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;

    .prologue
    .line 170
    const-class v0, Lcom/google/android/finsky/analytics/FinskyEventLog;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->sLogTestListener:Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    monitor-exit v0

    return-void

    .line 170
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1
    .param p0, "element"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .param p1, "serverLogsCookie"    # [B

    .prologue
    .line 213
    if-eqz p1, :cond_0

    .line 214
    iput-object p1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    .line 217
    :cond_0
    return-object p0
.end method

.method public static startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p0, "node"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 750
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    .line 751
    .local v0, "parent":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_0
    if-eqz v0, :cond_0

    .line 752
    move-object p0, v0

    .line 753
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    goto :goto_0

    .line 756
    :cond_0
    instance-of v1, p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    if-eqz v1, :cond_1

    .line 757
    check-cast p0, Lcom/google/android/finsky/layout/play/RootUiElementNode;

    .end local p0    # "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-interface {p0}, Lcom/google/android/finsky/layout/play/RootUiElementNode;->startNewImpression()V

    .line 759
    :cond_1
    return-void
.end method

.method private static declared-synchronized writeLogToListener(Ljava/lang/String;[B)V
    .locals 6
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "eventBytes"    # [B

    .prologue
    .line 151
    const-class v2, Lcom/google/android/finsky/analytics/FinskyEventLog;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/finsky/analytics/FinskyEventLog;->sLogTestListener:Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;

    if-eqz v1, :cond_1

    .line 152
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;-><init>()V

    .line 153
    .local v0, "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->eventTimeMs:J

    .line 154
    iput-object p0, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->tag:Ljava/lang/String;

    .line 157
    if-eqz p1, :cond_0

    .line 158
    iput-object p1, v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->sourceExtension:[B

    .line 160
    :cond_0
    sget-object v1, Lcom/google/android/finsky/analytics/FinskyEventLog;->sLogTestListener:Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog$LogListener;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .end local v0    # "event":Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;
    :cond_1
    monitor-exit v2

    return-void

    .line 151
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "errorCode"    # I
    .param p5, "exceptionType"    # Ljava/lang/String;
    .param p6, "appData"    # Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .prologue
    .line 894
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAppData(Lcom/google/android/finsky/analytics/PlayStore$AppData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 900
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 901
    return-void
.end method

.method public logBackgroundEvent(I[B)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "serverLogsCookie"    # [B

    .prologue
    .line 907
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLogsCookie([B)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 909
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 910
    return-void
.end method

.method public logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .prologue
    .line 987
    invoke-direct {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 988
    return-void
.end method

.method public logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "leafType"    # I
    .param p2, "leafServerLogsCookie"    # [B
    .param p3, "clickLogParentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x1

    .line 244
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    .line 245
    .local v0, "clickEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 246
    .local v1, "leafElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iput p1, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    .line 247
    iput-boolean v3, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    .line 249
    if-eqz p2, :cond_0

    .line 250
    iput-object p2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie:[B

    .line 251
    iput-boolean v3, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    .line 254
    :cond_0
    iget-object v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v2, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 256
    if-eqz p3, :cond_1

    .line 257
    invoke-static {p3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 259
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 260
    return-void
.end method

.method public logClickEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V
    .locals 2
    .param p1, "clickEvent"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    .prologue
    .line 315
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    .line 316
    .local v0, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    .line 319
    const-string v1, "3"

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 320
    return-void
.end method

.method public logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 230
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    .line 228
    .local v0, "clickEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    invoke-static {p1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 229
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    goto :goto_0
.end method

.method public logClickEventWithClientCookie(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "leafType"    # I
    .param p2, "leafClientLogsCookie"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .param p3, "clickLogParentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 292
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreClickEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    move-result-object v0

    .line 293
    .local v0, "clickEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 294
    .local v1, "leafElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iput p1, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    .line 295
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    .line 297
    if-eqz p2, :cond_0

    .line 298
    iput-object p2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 300
    :cond_0
    iget-object v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v2, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->safeAddElementToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iput-object v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;->elementPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 301
    if-eqz p3, :cond_1

    .line 302
    invoke-static {p3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->addClickPath(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 304
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;)V

    .line 305
    return-void
.end method

.method public logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 4
    .param p1, "resolvedType"    # I
    .param p2, "externalUrl"    # Ljava/lang/String;
    .param p3, "externalReferrer"    # Ljava/lang/String;
    .param p4, "serverLogsCookie"    # [B

    .prologue
    const/4 v3, 0x1

    .line 1028
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;-><init>()V

    .line 1029
    .local v0, "deepLinkEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;
    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    .line 1030
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    .line 1032
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1033
    iput-object p2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    .line 1034
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    .line 1037
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1038
    iput-object p3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    .line 1039
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalReferrer:Z

    .line 1042
    :cond_1
    if-eqz p4, :cond_2

    .line 1043
    iput-object p4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    .line 1044
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasServerLogsCookie:Z

    .line 1051
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v1

    .line 1052
    .local v1, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object v0, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    .line 1055
    const-string v2, "6"

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 1056
    return-void
.end method

.method public logDeepLinkEvent(Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 4
    .param p1, "externalUrl"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "minVersion"    # I
    .param p4, "newEnough"    # Z
    .param p5, "canResolve"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1070
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;-><init>()V

    .line 1071
    .local v0, "deepLinkEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;
    const/4 v2, 0x7

    iput v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    .line 1072
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    .line 1074
    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    .line 1075
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    .line 1077
    iput-object p2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    .line 1078
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasPackageName:Z

    .line 1080
    iput p3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    .line 1081
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasMinVersion:Z

    .line 1083
    iput-boolean p4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    .line 1084
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasNewEnough:Z

    .line 1086
    iput-boolean p5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    .line 1087
    iput-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasCanResolve:Z

    .line 1093
    iget-object v2, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v2}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v1

    .line 1094
    .local v1, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object v0, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    .line 1097
    const-string v2, "6"

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 1098
    return-void
.end method

.method public logImpressionEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;)V
    .locals 2
    .param p1, "impressionEvent"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    .prologue
    .line 440
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    .line 441
    .local v0, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    .line 444
    const-string v1, "1"

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 445
    return-void
.end method

.method public logNlpCleanupData(Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)V
    .locals 3
    .param p1, "report"    # Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .prologue
    .line 977
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setNlpRepairStatus(Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 980
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 981
    return-void
.end method

.method public logOperationSuccessBackgroundEvent(IZ)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "operationSuccess"    # Z

    .prologue
    .line 946
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 948
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 949
    return-void
.end method

.method public logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 7
    .param p1, "impressionId"    # J
    .param p3, "leafType"    # I
    .param p4, "parentPath"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v6, 0x1

    .line 359
    iget-object v4, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v4}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    .line 360
    .local v0, "impressionEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-eqz v4, :cond_0

    .line 361
    iput-wide p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    .line 362
    iput-boolean v6, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    .line 364
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 365
    .local v2, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;>;"
    iget-object v4, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v4}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    .line 366
    .local v1, "leafElement":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    iput p3, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->type:I

    .line 367
    iput-boolean v6, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    .line 368
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    :goto_0
    if-eqz p4, :cond_1

    .line 370
    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    invoke-interface {p4}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object p4

    goto :goto_0

    .line 373
    :cond_1
    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->pathToTree(Ljava/util/List;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    .line 374
    .local v3, "tree":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v3, :cond_2

    .line 375
    iput-object v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 379
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;)V

    .line 380
    return-void

    .line 377
    :cond_2
    const-string v4, "Encountered empty tree."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 7
    .param p1, "impressionId"    # J
    .param p3, "leaf"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 391
    iget-object v4, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v4}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreImpressionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    .line 392
    .local v0, "impressionEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-eqz v4, :cond_0

    .line 393
    iput-wide p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    .line 394
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    .line 396
    :cond_0
    move-object v1, p3

    .line 397
    .local v1, "parent":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 398
    .local v2, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;>;"
    :goto_0
    if-eqz v1, :cond_1

    .line 399
    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    goto :goto_0

    .line 402
    :cond_1
    invoke-static {v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->pathToTree(Ljava/util/List;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    .line 403
    .local v3, "tree":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v3, :cond_2

    .line 404
    iput-object v3, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 408
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logImpressionEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;)V

    .line 409
    return-void

    .line 406
    :cond_2
    const-string v4, "Encountered empty tree."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "docId"    # Ljava/lang/String;
    .param p3, "offerType"    # I
    .param p4, "exceptionType"    # Ljava/lang/String;
    .param p5, "errorCode"    # I
    .param p6, "serverLogsCookie"    # [B
    .param p7, "serverLatencyMs"    # J
    .param p9, "clientLatencyMs"    # J

    .prologue
    .line 918
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLogsCookie([B)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOfferType(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setServerLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p9, p10}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setClientLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 926
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 927
    return-void
.end method

.method public logReviewAdded(IIZ)V
    .locals 4
    .param p1, "reviewSource"    # I
    .param p2, "rating"    # I
    .param p3, "containsText"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1109
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;-><init>()V

    .line 1110
    .local v1, "reviewData":Lcom/google/android/finsky/analytics/PlayStore$ReviewData;
    iput p1, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    .line 1111
    iput-boolean v2, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasReviewSource:Z

    .line 1112
    iput p2, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    .line 1113
    iput-boolean v2, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasRating:Z

    .line 1114
    iput-boolean p3, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    .line 1115
    iput-boolean v2, v1, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasContainsText:Z

    .line 1116
    new-instance v2, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v3, 0x201

    invoke-direct {v2, v3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReviewData(Lcom/google/android/finsky/analytics/PlayStore$ReviewData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 1119
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 1120
    return-void
.end method

.method public logSearchEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;)V
    .locals 2
    .param p1, "searchEvent"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    .prologue
    .line 1008
    iget-object v1, p0, Lcom/google/android/finsky/analytics/FinskyEventLog;->mProtoCache:Lcom/google/android/finsky/analytics/EventProtoCache;

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/EventProtoCache;->obtainPlayStoreLogEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    .line 1009
    .local v0, "logEvent":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    .line 1012
    const-string v1, "5"

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->serializeAndWrite(Ljava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;)V

    .line 1013
    return-void
.end method

.method public logSessionData(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;)V
    .locals 3
    .param p1, "sessionData"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .prologue
    .line 967
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setSessionInfo(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 970
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 971
    return-void
.end method

.method public logSettingsBackgroundEvent(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "toSetting"    # Ljava/lang/Integer;
    .param p3, "fromSetting"    # Ljava/lang/Integer;
    .param p4, "reason"    # Ljava/lang/String;

    .prologue
    .line 934
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setToSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setFromSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 938
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 939
    return-void
.end method

.method public logSettingsForPackageEvent(IILjava/lang/Integer;Ljava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "setting"    # I
    .param p3, "previousSetting"    # Ljava/lang/Integer;
    .param p4, "documentId"    # Ljava/lang/String;

    .prologue
    .line 956
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setToSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setFromSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 960
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 961
    return-void
.end method

.method public logWifiAutoUpdateAttempt(Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;Ljava/lang/String;)V
    .locals 3
    .param p1, "attempt"    # Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 1101
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v2, 0x73

    invoke-direct {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setWifiAutoUpdateAttempt(Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 1105
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->sendBackgroundEventToSinks(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 1106
    return-void
.end method
