.class public interface abstract Lcom/google/android/finsky/protos/ChallengeProto;
.super Ljava/lang/Object;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;,
        Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;,
        Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;,
        Lcom/google/android/finsky/protos/ChallengeProto$FormButton;,
        Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;,
        Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$Country;,
        Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;,
        Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;,
        Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;,
        Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    }
.end annotation
