.class Lcom/google/android/finsky/activities/SettingsActivity$1;
.super Ljava/lang/Object;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/SettingsActivity;->doSelfUpdateCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/SettingsActivity;

.field final synthetic val$accountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/SettingsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsActivity$1;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/SettingsActivity$1;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$1;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/finsky/activities/SettingsActivity;->showSelfUpdateCheckResult(Z)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/SettingsActivity;->access$100(Lcom/google/android/finsky/activities/SettingsActivity;Z)V

    .line 279
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .prologue
    .line 267
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getSelfUpdateScheduler()Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    move-result-object v1

    .line 269
    .local v1, "selfUpdateScheduler":Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    invoke-virtual {v1, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getNewVersion(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)I

    move-result v0

    .line 270
    .local v0, "newVersion":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/SettingsActivity$1;->val$accountName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->checkForSelfUpdate(ILjava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/google/android/finsky/activities/SettingsActivity;->access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 272
    iget-object v2, p0, Lcom/google/android/finsky/activities/SettingsActivity$1;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    # getter for: Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;
    invoke-static {}, Lcom/google/android/finsky/activities/SettingsActivity;->access$000()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    # invokes: Lcom/google/android/finsky/activities/SettingsActivity;->showSelfUpdateCheckResult(Z)V
    invoke-static {v2, v3}, Lcom/google/android/finsky/activities/SettingsActivity;->access$100(Lcom/google/android/finsky/activities/SettingsActivity;Z)V

    .line 273
    return-void
.end method
