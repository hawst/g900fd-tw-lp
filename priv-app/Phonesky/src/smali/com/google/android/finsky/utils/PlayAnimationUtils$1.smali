.class final Lcom/google/android/finsky/utils/PlayAnimationUtils$1;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
.source "PlayAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PlayAnimationUtils;->getVerticalScrollByAnimation(Landroid/widget/ScrollView;JJILandroid/view/animation/Animation$AnimationListener;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field currScrolledAmount:I

.field final synthetic val$scrollView:Landroid/widget/ScrollView;

.field final synthetic val$verticalScrollAmount:I


# direct methods
.method constructor <init>(FFFFILandroid/widget/ScrollView;)V
    .locals 1
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "x2"    # F
    .param p4, "x3"    # F

    .prologue
    .line 119
    iput p5, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->val$verticalScrollAmount:I

    iput-object p6, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->val$scrollView:Landroid/widget/ScrollView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;-><init>(FFFF)V

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->currScrolledAmount:I

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 124
    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-nez v2, :cond_0

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->setStartedScrolling()V

    .line 128
    iget v2, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->val$verticalScrollAmount:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v0, v2

    .line 129
    .local v0, "newScrolledAmount":I
    iget v2, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->currScrolledAmount:I

    sub-int v1, v0, v2

    .line 130
    .local v1, "scrollDelta":I
    iget-object v2, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->val$scrollView:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 131
    iput v0, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$1;->currScrolledAmount:I

    goto :goto_0
.end method
