.class final Lcom/google/android/finsky/utils/WishlistHelper$2;
.super Ljava/lang/Object;
.source "WishlistHelper.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$docId:Ljava/lang/String;

.field final synthetic val$docTitle:Ljava/lang/String;

.field final synthetic val$res:Landroid/content/res/Resources;

.field final synthetic val$wasInWishlist:Z


# direct methods
.method constructor <init>(ZLandroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$wasInWishlist:Z

    iput-object p2, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$res:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$docTitle:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$docId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 7
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 152
    iget-boolean v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$wasInWishlist:Z

    if-eqz v2, :cond_0

    const v0, 0x7f0c0354

    .line 154
    .local v0, "resId":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$res:Landroid/content/res/Resources;

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$docTitle:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "toast":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-static {v2, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 156
    iget-boolean v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$wasInWishlist:Z

    if-eqz v2, :cond_1

    .line 157
    const-string v2, "Unable to remove from wishlist: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$docId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/finsky/utils/WishlistHelper$2;->val$wasInWishlist:Z

    # invokes: Lcom/google/android/finsky/utils/WishlistHelper;->invokeWishlistStatusListeners(Ljava/lang/String;ZZ)V
    invoke-static {v2, v3, v5}, Lcom/google/android/finsky/utils/WishlistHelper;->access$000(Ljava/lang/String;ZZ)V

    .line 162
    return-void

    .line 152
    .end local v0    # "resId":I
    .end local v1    # "toast":Ljava/lang/String;
    :cond_0
    const v0, 0x7f0c0353

    goto :goto_0

    .line 159
    .restart local v0    # "resId":I
    .restart local v1    # "toast":Ljava/lang/String;
    :cond_1
    const-string v2, "Unable to add to wishlist: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
