.class public Lcom/google/android/finsky/utils/BadgeUtils;
.super Ljava/lang/Object;
.source "BadgeUtils.java"


# direct methods
.method static synthetic access$000(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 0
    .param p0, "x0"    # Landroid/widget/ImageView;
    .param p1, "x1"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/BadgeUtils;->bitmapLoaded(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method

.method private static bitmapLoaded(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "container"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 72
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 75
    :cond_0
    return-void
.end method

.method public static configureContentRatingBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Landroid/widget/ImageView;)V
    .locals 6
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "ratingView"    # Landroid/widget/ImageView;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 49
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 68
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBadgeForContentRating()Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v1

    .line 54
    .local v1, "ratingBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-nez v1, :cond_1

    .line 55
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 57
    :cond_1
    iget-object v2, v1, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v2, v1, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    new-instance v3, Lcom/google/android/finsky/utils/BadgeUtils$1;

    invoke-direct {v3, p2}, Lcom/google/android/finsky/utils/BadgeUtils$1;-><init>(Landroid/widget/ImageView;)V

    invoke-virtual {p1, v2, v4, v4, v3}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v0

    .line 66
    .local v0, "newContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-static {p2, v0}, Lcom/google/android/finsky/utils/BadgeUtils;->bitmapLoaded(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    goto :goto_0
.end method

.method public static configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 5
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "textView"    # Lcom/google/android/finsky/layout/DecoratedTextView;

    .prologue
    const/4 v4, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCreatorBadges()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 35
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFirstCreatorBadge()Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v2

    .line 36
    .local v2, "firstCreatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTextSize()F

    move-result v3

    float-to-int v1, v3

    .line 37
    .local v1, "badgeSize":I
    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 38
    .local v0, "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;I)V

    .line 44
    .end local v0    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "badgeSize":I
    .end local v2    # "firstCreatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p2, v4, v4, v4, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static configureRatingItemSection(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/StarRatingBar;Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 11
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "ratingBar"    # Lcom/google/android/play/layout/StarRatingBar;
    .param p2, "textView"    # Lcom/google/android/finsky/layout/DecoratedTextView;

    .prologue
    const/4 v10, 0x0

    .line 79
    if-eqz p1, :cond_0

    .line 80
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 82
    :cond_0
    if-eqz p2, :cond_1

    .line 83
    const/16 v5, 0x8

    invoke-virtual {p2, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 86
    :cond_1
    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasItemBadges()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 87
    invoke-virtual {p2, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFirstItemBadge()Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v3

    .line 89
    .local v3, "firstItemBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTextSize()F

    move-result v5

    float-to-int v1, v5

    .line 90
    .local v1, "badgeSize":I
    const/4 v5, 0x6

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 92
    .local v0, "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_2

    .line 93
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v2

    .line 94
    .local v2, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    invoke-virtual {p2, v2, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;I)V

    .line 96
    .end local v2    # "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    :cond_2
    iget-object v5, v3, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const v5, 0x7f0900fc

    invoke-virtual {p2, v5, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->setContentColorStateListId(IZ)V

    .line 98
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {p2, v10, v5, v10, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    .line 112
    .end local v0    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "badgeSize":I
    .end local v3    # "firstItemBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_3
    :goto_0
    return-void

    .line 99
    :cond_4
    if-eqz p2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCensoring()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasReleaseType()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 100
    :cond_5
    invoke-static {p0, p2}, Lcom/google/android/finsky/utils/BadgeUtils;->configureTipperSticker(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayTextView;)V

    .line 101
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0145

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 103
    .local v4, "stickerPadding":I
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {p2, v4, v5, v4, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    goto :goto_0

    .line 105
    .end local v4    # "stickerPadding":I
    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v5

    const/16 v6, 0x14

    if-ne v5, v6, :cond_7

    .line 106
    invoke-static {p0, p2}, Lcom/google/android/finsky/utils/BadgeUtils;->configureReleaseDate(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayTextView;)V

    .line 107
    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {p2, v10, v5, v10, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    goto :goto_0

    .line 108
    :cond_7
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getRatingCount()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    .line 109
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 110
    invoke-virtual {p1, v10}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private static configureReleaseDate(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayTextView;)V
    .locals 3
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "textView"    # Lcom/google/android/play/layout/PlayTextView;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p1, v2}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 118
    const v0, 0x7f09009f

    invoke-virtual {p1, v0, v2}, Lcom/google/android/play/layout/PlayTextView;->setContentColorId(IZ)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {p1, v1, v1, v1, v1}, Lcom/google/android/play/layout/PlayTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 124
    :cond_0
    return-void
.end method

.method public static configureTipperSticker(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayTextView;)V
    .locals 6
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "textView"    # Lcom/google/android/play/layout/PlayTextView;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 127
    const/4 v2, -0x1

    .line 129
    .local v2, "stringId":I
    const v0, 0x7f0900b1

    .line 130
    .local v0, "colorId":I
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCensoring()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getCensoring()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 142
    :cond_0
    :goto_0
    if-ne v2, v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasReleaseType()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getReleaseType()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 155
    :cond_1
    :goto_1
    if-le v2, v5, :cond_2

    .line 156
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 157
    invoke-virtual {p1}, Lcom/google/android/play/layout/PlayTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "stickerLabel":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, Lcom/google/android/play/layout/PlayTextView;->setContentColorId(IZ)V

    .line 162
    invoke-virtual {p1, v4, v4, v4, v4}, Lcom/google/android/play/layout/PlayTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 166
    .end local v1    # "stickerLabel":Ljava/lang/String;
    :goto_2
    return-void

    .line 133
    :pswitch_0
    const v2, 0x7f0c0348

    .line 134
    const v0, 0x7f0900b0

    .line 135
    goto :goto_0

    .line 137
    :pswitch_1
    const v2, 0x7f0c0349

    goto :goto_0

    .line 145
    :pswitch_2
    const v2, 0x7f0c034b

    .line 146
    goto :goto_1

    .line 148
    :pswitch_3
    const v2, 0x7f0c034c

    .line 149
    goto :goto_1

    .line 151
    :pswitch_4
    const v2, 0x7f0c034a

    goto :goto_1

    .line 164
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_2

    .line 131
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 143
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;
    .locals 5
    .param p0, "badge"    # Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .param p1, "imageType"    # I

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Image;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 25
    .local v2, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget v4, v2, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-ne v4, p1, :cond_0

    .line 29
    .end local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :goto_1
    return-object v2

    .line 24
    .restart local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 29
    .end local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
