.class Lcom/google/android/finsky/activities/AuthenticatedActivity$7;
.super Ljava/lang/Object;
.source "AuthenticatedActivity.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GetTocHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/AuthenticatedActivity;->loadTocAndContinue(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$responseReceived:[Z

.field final synthetic val$shouldHandleIntent:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;[ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 870
    iput-object p1, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$responseReceived:[Z

    iput-object p3, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$accountName:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$shouldHandleIntent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->hideLoadingIndicator()V

    .line 919
    iget-object v0, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->handleAuthenticationError(Lcom/android/volley/VolleyError;)V

    .line 920
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/Toc$TocResponse;)V
    .locals 7
    .param p1, "response"    # Lcom/google/android/finsky/protos/Toc$TocResponse;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 877
    iget-object v4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$responseReceived:[Z

    aget-boolean v4, v4, v5

    if-ne v4, v6, :cond_0

    .line 878
    new-instance v0, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    iget-object v4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;-><init>(Landroid/content/Context;)V

    .line 880
    .local v0, "deferrer":Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;
    new-instance v4, Lcom/google/android/finsky/activities/AuthenticatedActivity$7$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity$7$1;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity$7;)V

    const/16 v5, 0x2710

    invoke-virtual {v0, v4, v5}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;->runOnApplicationClose(Ljava/lang/Runnable;I)V

    .line 914
    .end local v0    # "deferrer":Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;
    :goto_0
    return-void

    .line 890
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$responseReceived:[Z

    aput-boolean v6, v4, v5

    .line 892
    new-instance v1, Lcom/google/android/finsky/api/model/DfeToc;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/api/model/DfeToc;-><init>(Lcom/google/android/finsky/protos/Toc$TocResponse;)V

    .line 894
    .local v1, "newToc":Lcom/google/android/finsky/api/model/DfeToc;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/finsky/FinskyApp;->setToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 901
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getSelfUpdateScheduler()Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    move-result-object v3

    .line 903
    .local v3, "selfUpdateScheduler":Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    invoke-virtual {v3, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getNewVersion(Lcom/google/android/finsky/protos/Toc$TocResponse;)I

    move-result v2

    .line 904
    .local v2, "newVersion":I
    iget-object v4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$accountName:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->checkForSelfUpdate(ILjava/lang/String;)Z

    .line 907
    iget-object v4, p1, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    if-eqz v4, :cond_1

    .line 908
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    invoke-static {v4, v5}, Lcom/google/android/finsky/billing/InAppBillingSetting;->setVersionFromBillingConfig(Ljava/lang/String;Lcom/google/android/finsky/protos/Toc$BillingConfig;)V

    .line 913
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->this$0:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    iget-boolean v5, p0, Lcom/google/android/finsky/activities/AuthenticatedActivity$7;->val$shouldHandleIntent:Z

    # invokes: Lcom/google/android/finsky/activities/AuthenticatedActivity;->restrictLimitedOrEduUserAndContinue(Z)V
    invoke-static {v4, v5}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->access$200(Lcom/google/android/finsky/activities/AuthenticatedActivity;Z)V

    goto :goto_0
.end method
