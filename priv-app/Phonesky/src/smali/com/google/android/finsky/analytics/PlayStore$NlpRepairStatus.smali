.class public final Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NlpRepairStatus"
.end annotation


# instance fields
.field public enabled:I

.field public flags:I

.field public foundTestKeys:Z

.field public hasEnabled:Z

.field public hasFlags:Z

.field public hasFoundTestKeys:Z

.field public hasHoldoffAfterInstall:Z

.field public hasHoldoffUntilBoot:Z

.field public hasHoldoffUntilWipe:Z

.field public hasRepairStatus:Z

.field public hasSignatureHash:Z

.field public hasVersionCode:Z

.field public holdoffAfterInstall:Z

.field public holdoffUntilBoot:Z

.field public holdoffUntilWipe:Z

.field public repairStatus:I

.field public signatureHash:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4304
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4305
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->clear()Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .line 4306
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4309
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 4310
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 4311
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    .line 4312
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFlags:Z

    .line 4313
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    .line 4314
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasVersionCode:Z

    .line 4315
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    .line 4316
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasSignatureHash:Z

    .line 4317
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    .line 4318
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFoundTestKeys:Z

    .line 4319
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    .line 4320
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilBoot:Z

    .line 4321
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    .line 4322
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilWipe:Z

    .line 4323
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    .line 4324
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffAfterInstall:Z

    .line 4325
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    .line 4326
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasEnabled:Z

    .line 4327
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->cachedSize:I

    .line 4328
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4366
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4367
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    if-eqz v1, :cond_1

    .line 4368
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4371
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFlags:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    if-eqz v1, :cond_3

    .line 4372
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4375
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasVersionCode:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    if-eqz v1, :cond_5

    .line 4376
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4379
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasSignatureHash:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 4380
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4383
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFoundTestKeys:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    if-eqz v1, :cond_9

    .line 4384
    :cond_8
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4387
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilBoot:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    if-eqz v1, :cond_b

    .line 4388
    :cond_a
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4391
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilWipe:Z

    if-nez v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    if-eqz v1, :cond_d

    .line 4392
    :cond_c
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4395
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffAfterInstall:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    if-eqz v1, :cond_f

    .line 4396
    :cond_e
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4399
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasEnabled:Z

    if-nez v1, :cond_10

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    if-eqz v1, :cond_11

    .line 4400
    :cond_10
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4403
    :cond_11
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 4411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4412
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4416
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4417
    :sswitch_0
    return-object p0

    .line 4422
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4423
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 4434
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 4435
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    goto :goto_0

    .line 4441
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    .line 4442
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFlags:Z

    goto :goto_0

    .line 4446
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    .line 4447
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasVersionCode:Z

    goto :goto_0

    .line 4451
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    .line 4452
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasSignatureHash:Z

    goto :goto_0

    .line 4456
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    .line 4457
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFoundTestKeys:Z

    goto :goto_0

    .line 4461
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    .line 4462
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilBoot:Z

    goto :goto_0

    .line 4466
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    .line 4467
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilWipe:Z

    goto :goto_0

    .line 4471
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    .line 4472
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffAfterInstall:Z

    goto :goto_0

    .line 4476
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    .line 4477
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasEnabled:Z

    goto :goto_0

    .line 4412
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    .line 4423
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4239
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4334
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    if-eqz v0, :cond_1

    .line 4335
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4337
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFlags:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    if-eqz v0, :cond_3

    .line 4338
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4340
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasVersionCode:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    if-eqz v0, :cond_5

    .line 4341
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4343
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasSignatureHash:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4344
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4346
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFoundTestKeys:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    if-eqz v0, :cond_9

    .line 4347
    :cond_8
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4349
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilBoot:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    if-eqz v0, :cond_b

    .line 4350
    :cond_a
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4352
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilWipe:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    if-eqz v0, :cond_d

    .line 4353
    :cond_c
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilWipe:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4355
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffAfterInstall:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    if-eqz v0, :cond_f

    .line 4356
    :cond_e
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4358
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasEnabled:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    if-eqz v0, :cond_11

    .line 4359
    :cond_10
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4361
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4362
    return-void
.end method
