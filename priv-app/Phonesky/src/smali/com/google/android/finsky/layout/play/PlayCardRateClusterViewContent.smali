.class public Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.source "PlayCardRateClusterViewContent.java"


# instance fields
.field private mTileIndexToDocumentIndexMapping:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p5, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->syncIndexMapping()V

    .line 62
    invoke-super/range {p0 .. p6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 64
    return-void
.end method

.method public hasItemsToRate()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    aget v1, v1, v0

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public syncIndexMapping()V
    .locals 12

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->getDocCount()I

    move-result v5

    .line 25
    .local v5, "docCount":I
    if-eqz v5, :cond_0

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    if-nez v10, :cond_1

    .line 53
    :cond_0
    return-void

    .line 28
    :cond_1
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v7

    .line 29
    .local v7, "tileCount":I
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    if-nez v10, :cond_2

    .line 30
    new-array v10, v7, [I

    iput-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    .line 34
    :cond_2
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_3

    .line 35
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    const/4 v11, -0x1

    aput v11, v10, v6

    .line 34
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 38
    :cond_3
    const/4 v3, 0x0

    .line 39
    .local v3, "currTileIndex":I
    const/4 v1, 0x0

    .local v1, "currDocIndex":I
    move v2, v1

    .end local v1    # "currDocIndex":I
    .local v2, "currDocIndex":I
    move v4, v3

    .line 43
    .end local v3    # "currTileIndex":I
    .local v4, "currTileIndex":I
    :goto_1
    if-ge v2, v5, :cond_0

    if-ge v4, v7, :cond_0

    .line 44
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->getDoc(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "childDocId":Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    const/4 v11, 0x0

    invoke-virtual {v10, v0, v11}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v10

    if-eqz v10, :cond_5

    const/4 v8, 0x1

    .line 46
    .local v8, "wasReviewed":Z
    :goto_2
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v9

    .line 47
    .local v9, "wasSkipped":Z
    if-nez v8, :cond_4

    if-eqz v9, :cond_6

    .line 48
    :cond_4
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "currDocIndex":I
    .restart local v1    # "currDocIndex":I
    move v2, v1

    .line 49
    .end local v1    # "currDocIndex":I
    .restart local v2    # "currDocIndex":I
    goto :goto_1

    .line 45
    .end local v8    # "wasReviewed":Z
    .end local v9    # "wasSkipped":Z
    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    .line 51
    .restart local v8    # "wasReviewed":Z
    .restart local v9    # "wasSkipped":Z
    :cond_6
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "currTileIndex":I
    .restart local v3    # "currTileIndex":I
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "currDocIndex":I
    .restart local v1    # "currDocIndex":I
    aput v2, v10, v4

    move v2, v1

    .end local v1    # "currDocIndex":I
    .restart local v2    # "currDocIndex":I
    move v4, v3

    .line 52
    .end local v3    # "currTileIndex":I
    .restart local v4    # "currTileIndex":I
    goto :goto_1
.end method

.method protected tileIndexToDocumentIndex(I)I
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->mTileIndexToDocumentIndexMapping:[I

    aget v0, v0, p1

    return v0
.end method
