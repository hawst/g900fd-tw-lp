.class public Lcom/google/android/finsky/layout/play/PlayCardHeap;
.super Ljava/lang/Object;
.source "PlayCardHeap.java"


# instance fields
.field private final mHeap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/PlayCardViewBase;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public getCard(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;)Lcom/google/android/play/layout/PlayCardViewBase;
    .locals 4
    .param p1, "cardMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const/4 v3, 0x0

    .line 98
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 100
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    .line 101
    .local v1, "layoutId":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 102
    .local v0, "available":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/layout/PlayCardViewBase;>;"
    if-nez v0, :cond_0

    .line 103
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 104
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 106
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 107
    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 109
    :goto_0
    return-object v2

    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/PlayCardViewBase;

    goto :goto_0
.end method

.method public recycle(Lcom/google/android/finsky/layout/play/PlayCardClusterView;)V
    .locals 12
    .param p1, "cluster"    # Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 40
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hasCards()Z

    move-result v11

    if-nez v11, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    .line 45
    .local v2, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v9

    .line 46
    .local v9, "tileCount":I
    const/4 v10, 0x0

    .local v10, "tileIndex":I
    :goto_1
    if-ge v10, v9, :cond_6

    .line 47
    invoke-virtual {p1, v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 48
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {v2, v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    .line 50
    .local v1, "cardMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    if-eqz v0, :cond_5

    .line 52
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getThumbnail()Lcom/google/android/play/layout/PlayCardThumbnail;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v4

    .line 53
    .local v4, "imageView":Landroid/widget/ImageView;
    instance-of v11, v4, Lcom/google/android/play/image/FifeImageView;

    if-eqz v11, :cond_2

    move-object v3, v4

    .line 54
    check-cast v3, Lcom/google/android/play/image/FifeImageView;

    .line 55
    .local v3, "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 59
    .end local v3    # "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet1()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v6

    .line 60
    .local v6, "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    if-eqz v6, :cond_3

    .line 61
    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardSnippet;->getImageView()Landroid/widget/ImageView;

    move-result-object v8

    .line 62
    .local v8, "snippetImageView":Landroid/widget/ImageView;
    instance-of v11, v4, Lcom/google/android/play/image/FifeImageView;

    if-eqz v11, :cond_3

    move-object v3, v8

    .line 63
    check-cast v3, Lcom/google/android/play/image/FifeImageView;

    .line 64
    .restart local v3    # "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 67
    .end local v3    # "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    .end local v8    # "snippetImageView":Landroid/widget/ImageView;
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getSnippet2()Lcom/google/android/play/layout/PlayCardSnippet;

    move-result-object v7

    .line 68
    .local v7, "snippet2":Lcom/google/android/play/layout/PlayCardSnippet;
    if-eqz v7, :cond_4

    .line 69
    invoke-virtual {v7}, Lcom/google/android/play/layout/PlayCardSnippet;->getImageView()Landroid/widget/ImageView;

    move-result-object v8

    .line 70
    .restart local v8    # "snippetImageView":Landroid/widget/ImageView;
    instance-of v11, v4, Lcom/google/android/play/image/FifeImageView;

    if-eqz v11, :cond_4

    move-object v3, v8

    .line 71
    check-cast v3, Lcom/google/android/play/image/FifeImageView;

    .line 72
    .restart local v3    # "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 78
    .end local v3    # "fifeImageView":Lcom/google/android/play/image/FifeImageView;
    .end local v8    # "snippetImageView":Landroid/widget/ImageView;
    :cond_4
    invoke-static {v0}, Lcom/google/android/finsky/utils/PlayCardUtils;->recycleLoggingData(Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 79
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v5

    .line 83
    .local v5, "layoutId":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v11, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    .end local v4    # "imageView":Landroid/widget/ImageView;
    .end local v5    # "layoutId":I
    .end local v6    # "snippet1":Lcom/google/android/play/layout/PlayCardSnippet;
    .end local v7    # "snippet2":Lcom/google/android/play/layout/PlayCardSnippet;
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 86
    .end local v0    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v1    # "cardMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->removeAllCards()V

    goto :goto_0
.end method
