.class public Lcom/google/android/finsky/utils/CorpusResourceUtils;
.super Ljava/lang/Object;
.source "CorpusResourceUtils.java"


# direct methods
.method public static getCorpusMyCollectionDescription(I)Ljava/lang/String;
    .locals 6
    .param p0, "backend"    # I

    .prologue
    const/4 v4, 0x0

    .line 195
    if-nez p0, :cond_0

    .line 196
    const/4 p0, 0x3

    .line 199
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    .line 200
    .local v3, "toc":Lcom/google/android/finsky/api/model/DfeToc;
    if-nez v3, :cond_2

    .line 221
    :cond_1
    :goto_0
    return-object v4

    .line 204
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v0

    .line 205
    .local v0, "corpusList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Toc$CorpusMetadata;>;"
    if-eqz v0, :cond_1

    .line 210
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 211
    .local v2, "metadata":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    iget-boolean v5, v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    if-eqz v5, :cond_3

    .line 215
    iget v5, v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    if-ne v5, p0, :cond_3

    .line 216
    iget-object v5, v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 217
    iget-object v4, v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getCorpusSpinnerDrawable(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 229
    packed-switch p0, :pswitch_data_0

    .line 239
    :pswitch_0
    const v0, 0x7f0201c0

    :goto_0
    return v0

    .line 231
    :pswitch_1
    const v0, 0x7f0201c1

    goto :goto_0

    .line 233
    :pswitch_2
    const v0, 0x7f0201c4

    goto :goto_0

    .line 235
    :pswitch_3
    const v0, 0x7f0201c2

    goto :goto_0

    .line 237
    :pswitch_4
    const v0, 0x7f0201c3

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getDrawerMyCollectionDrawable(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 689
    packed-switch p0, :pswitch_data_0

    .line 701
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 691
    :pswitch_1
    const v0, 0x7f0200b2

    goto :goto_0

    .line 693
    :pswitch_2
    const v0, 0x7f0200b4

    goto :goto_0

    .line 695
    :pswitch_3
    const v0, 0x7f0200ba

    goto :goto_0

    .line 697
    :pswitch_4
    const v0, 0x7f0200b6

    goto :goto_0

    .line 699
    :pswitch_5
    const v0, 0x7f0200b8

    goto :goto_0

    .line 689
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getDrawerMyCollectionSelectedDrawable(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 706
    packed-switch p0, :pswitch_data_0

    .line 718
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 708
    :pswitch_1
    const v0, 0x7f0200b3

    goto :goto_0

    .line 710
    :pswitch_2
    const v0, 0x7f0200b5

    goto :goto_0

    .line 712
    :pswitch_3
    const v0, 0x7f0200bb

    goto :goto_0

    .line 714
    :pswitch_4
    const v0, 0x7f0200b7

    goto :goto_0

    .line 716
    :pswitch_5
    const v0, 0x7f0200b9

    goto :goto_0

    .line 706
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getDrawerShopDrawable(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 655
    packed-switch p0, :pswitch_data_0

    .line 667
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 657
    :pswitch_1
    const v0, 0x7f0200a6

    goto :goto_0

    .line 659
    :pswitch_2
    const v0, 0x7f0200a8

    goto :goto_0

    .line 661
    :pswitch_3
    const v0, 0x7f0200bc

    goto :goto_0

    .line 663
    :pswitch_4
    const v0, 0x7f0200ac

    goto :goto_0

    .line 665
    :pswitch_5
    const v0, 0x7f0200ae

    goto :goto_0

    .line 655
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getDrawerShopSelectedDrawable(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 672
    packed-switch p0, :pswitch_data_0

    .line 684
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 674
    :pswitch_1
    const v0, 0x7f0200a7

    goto :goto_0

    .line 676
    :pswitch_2
    const v0, 0x7f0200a9

    goto :goto_0

    .line 678
    :pswitch_3
    const v0, 0x7f0200bd

    goto :goto_0

    .line 680
    :pswitch_4
    const v0, 0x7f0200ad

    goto :goto_0

    .line 682
    :pswitch_5
    const v0, 0x7f0200af

    goto :goto_0

    .line 672
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 406
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "title":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 435
    :goto_0
    return-object v1

    .line 411
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 413
    :pswitch_1
    const v1, 0x7f0c02dc

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 415
    :pswitch_2
    const v1, 0x7f0c02dd

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 417
    :pswitch_3
    const v1, 0x7f0c02de

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 420
    :pswitch_4
    const v1, 0x7f0c02e2

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 422
    :pswitch_5
    const v1, 0x7f0c02e3

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 427
    :pswitch_6
    const v1, 0x7f0c02e1

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 429
    :pswitch_7
    const v1, 0x7f0c02df

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 433
    :pswitch_8
    const v1, 0x7f0c02e0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public static getMenuExpanderMaximized(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 355
    packed-switch p0, :pswitch_data_0

    .line 367
    :pswitch_0
    const v0, 0x7f0200df

    :goto_0
    return v0

    .line 357
    :pswitch_1
    const v0, 0x7f0200db

    goto :goto_0

    .line 359
    :pswitch_2
    const v0, 0x7f0200dd

    goto :goto_0

    .line 361
    :pswitch_3
    const v0, 0x7f0200e7

    goto :goto_0

    .line 363
    :pswitch_4
    const v0, 0x7f0200e3

    goto :goto_0

    .line 365
    :pswitch_5
    const v0, 0x7f0200e5

    goto :goto_0

    .line 355
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getMenuExpanderMinimized(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 338
    packed-switch p0, :pswitch_data_0

    .line 350
    :pswitch_0
    const v0, 0x7f0200e1

    :goto_0
    return v0

    .line 340
    :pswitch_1
    const v0, 0x7f0200dc

    goto :goto_0

    .line 342
    :pswitch_2
    const v0, 0x7f0200de

    goto :goto_0

    .line 344
    :pswitch_3
    const v0, 0x7f0200e8

    goto :goto_0

    .line 346
    :pswitch_4
    const v0, 0x7f0200e4

    goto :goto_0

    .line 348
    :pswitch_5
    const v0, 0x7f0200e6

    goto :goto_0

    .line 338
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getNoSearchResultsMessageId(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 389
    packed-switch p0, :pswitch_data_0

    .line 401
    :pswitch_0
    const v0, 0x7f0c01d0

    :goto_0
    return v0

    .line 391
    :pswitch_1
    const v0, 0x7f0c01d1

    goto :goto_0

    .line 393
    :pswitch_2
    const v0, 0x7f0c01d2

    goto :goto_0

    .line 395
    :pswitch_3
    const v0, 0x7f0c01d5

    goto :goto_0

    .line 397
    :pswitch_4
    const v0, 0x7f0c01d3

    goto :goto_0

    .line 399
    :pswitch_5
    const v0, 0x7f0c01d4

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getOpenButtonStringId(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 312
    packed-switch p0, :pswitch_data_0

    .line 321
    :pswitch_0
    const v0, 0x7f0c020b

    :goto_0
    return v0

    .line 314
    :pswitch_1
    const v0, 0x7f0c01e4

    goto :goto_0

    .line 316
    :pswitch_2
    const v0, 0x7f0c020e

    goto :goto_0

    .line 319
    :pswitch_3
    const v0, 0x7f0c01e5

    goto :goto_0

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getOwnedItemIndicator(I)I
    .locals 3
    .param p0, "backendId"    # I

    .prologue
    .line 372
    packed-switch p0, :pswitch_data_0

    .line 384
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :pswitch_1
    const v0, 0x7f020093

    .line 382
    :goto_0
    return v0

    .line 376
    :pswitch_2
    const v0, 0x7f020094

    goto :goto_0

    .line 378
    :pswitch_3
    const v0, 0x7f020097

    goto :goto_0

    .line 380
    :pswitch_4
    const v0, 0x7f020095

    goto :goto_0

    .line 382
    :pswitch_5
    const v0, 0x7f020096

    goto :goto_0

    .line 372
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPlayActionButtonBaseBackgroundDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 100
    packed-switch p1, :pswitch_data_0

    .line 112
    :pswitch_0
    const v0, 0x7f020167

    :goto_0
    return v0

    .line 102
    :pswitch_1
    const v0, 0x7f020158

    goto :goto_0

    .line 104
    :pswitch_2
    const v0, 0x7f02015d

    goto :goto_0

    .line 106
    :pswitch_3
    const v0, 0x7f020171

    goto :goto_0

    .line 108
    :pswitch_4
    const v0, 0x7f020162

    goto :goto_0

    .line 110
    :pswitch_5
    const v0, 0x7f02016c

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPlayActionButtonStartBackgroundDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 117
    packed-switch p1, :pswitch_data_0

    .line 129
    :pswitch_0
    const v0, 0x7f02016a

    :goto_0
    return v0

    .line 119
    :pswitch_1
    const v0, 0x7f02015b

    goto :goto_0

    .line 121
    :pswitch_2
    const v0, 0x7f020160

    goto :goto_0

    .line 123
    :pswitch_3
    const v0, 0x7f020174

    goto :goto_0

    .line 125
    :pswitch_4
    const v0, 0x7f020165

    goto :goto_0

    .line 127
    :pswitch_5
    const v0, 0x7f02016f

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPrimaryColor(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 28
    packed-switch p1, :pswitch_data_0

    .line 45
    :pswitch_0
    const v0, 0x7f090065

    .line 48
    .local v0, "colorResourceId":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 30
    .end local v0    # "colorResourceId":I
    :pswitch_1
    const v0, 0x7f090060

    .line 31
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 33
    .end local v0    # "colorResourceId":I
    :pswitch_2
    const v0, 0x7f090061

    .line 34
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 36
    .end local v0    # "colorResourceId":I
    :pswitch_3
    const v0, 0x7f090064

    .line 37
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 39
    .end local v0    # "colorResourceId":I
    :pswitch_4
    const v0, 0x7f090062

    .line 40
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 42
    .end local v0    # "colorResourceId":I
    :pswitch_5
    const v0, 0x7f090063

    .line 43
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getRateString(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "docType"    # I

    .prologue
    .line 461
    packed-switch p1, :pswitch_data_0

    .line 484
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported doc type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :pswitch_1
    const v0, 0x7f0c0396

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 482
    :goto_0
    return-object v0

    .line 465
    :pswitch_2
    const v0, 0x7f0c0397

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 467
    :pswitch_3
    const v0, 0x7f0c0398

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 469
    :pswitch_4
    const v0, 0x7f0c0399

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 474
    :pswitch_5
    const v0, 0x7f0c039a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 476
    :pswitch_6
    const v0, 0x7f0c039e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 478
    :pswitch_7
    const v0, 0x7f0c039b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 480
    :pswitch_8
    const v0, 0x7f0c039c

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 482
    :pswitch_9
    const v0, 0x7f0c039d

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 461
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static getRatingBarFilledFocusedResourceId(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 506
    packed-switch p0, :pswitch_data_0

    .line 518
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 508
    :pswitch_1
    const v0, 0x7f020056

    .line 516
    :goto_0
    return v0

    .line 510
    :pswitch_2
    const v0, 0x7f02005b

    goto :goto_0

    .line 512
    :pswitch_3
    const v0, 0x7f020059

    goto :goto_0

    .line 514
    :pswitch_4
    const v0, 0x7f02005d

    goto :goto_0

    .line 516
    :pswitch_5
    const v0, 0x7f020054

    goto :goto_0

    .line 506
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getRatingBarFilledResourceId(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 489
    packed-switch p0, :pswitch_data_0

    .line 501
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491
    :pswitch_1
    const v0, 0x7f020055

    .line 499
    :goto_0
    return v0

    .line 493
    :pswitch_2
    const v0, 0x7f02005a

    goto :goto_0

    .line 495
    :pswitch_3
    const v0, 0x7f020058

    goto :goto_0

    .line 497
    :pswitch_4
    const v0, 0x7f02005c

    goto :goto_0

    .line 499
    :pswitch_5
    const v0, 0x7f020053

    goto :goto_0

    .line 489
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getRecommendationWidgetStripResourceId(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 440
    packed-switch p0, :pswitch_data_0

    .line 453
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 442
    :pswitch_1
    const v0, 0x7f0201aa

    .line 451
    :goto_0
    return v0

    .line 444
    :pswitch_2
    const v0, 0x7f0201ac

    goto :goto_0

    .line 446
    :pswitch_3
    const v0, 0x7f0201ab

    goto :goto_0

    .line 448
    :pswitch_4
    const v0, 0x7f0201ad

    goto :goto_0

    .line 451
    :pswitch_5
    const v0, 0x7f0201a9

    goto :goto_0

    .line 440
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getRegularDetailsIconHeight(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "docType"    # I

    .prologue
    .line 282
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 283
    .local v0, "res":Landroid/content/res/Resources;
    sparse-switch p1, :sswitch_data_0

    .line 302
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported document type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 292
    :sswitch_0
    const v1, 0x7f0b00dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 300
    :goto_0
    return v1

    .line 294
    :sswitch_1
    const v1, 0x7f0b00da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 297
    :sswitch_2
    const v1, 0x7f0b0165

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 300
    :sswitch_3
    const v1, 0x7f0b00db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 283
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_3
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_2
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public static getRegularDetailsIconWidth(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "docType"    # I

    .prologue
    .line 251
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 252
    .local v0, "res":Landroid/content/res/Resources;
    sparse-switch p1, :sswitch_data_0

    .line 270
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported document type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 254
    :sswitch_0
    const v1, 0x7f0b00da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 268
    :goto_0
    return v1

    .line 257
    :sswitch_1
    const v1, 0x7f0b0165

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 262
    :sswitch_2
    const v1, 0x7f0b00dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 268
    :sswitch_3
    const v1, 0x7f0b00db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0

    .line 252
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_3
        0x8 -> :sswitch_1
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1e -> :sswitch_1
    .end sparse-switch
.end method

.method public static getReviewEditDrawable(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 523
    packed-switch p0, :pswitch_data_0

    .line 535
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525
    :pswitch_1
    const v0, 0x7f0200c4

    .line 533
    :goto_0
    return v0

    .line 527
    :pswitch_2
    const v0, 0x7f0200c6

    goto :goto_0

    .line 529
    :pswitch_3
    const v0, 0x7f0200c5

    goto :goto_0

    .line 531
    :pswitch_4
    const v0, 0x7f0200c7

    goto :goto_0

    .line 533
    :pswitch_5
    const v0, 0x7f0200c3

    goto :goto_0

    .line 523
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getSecondaryColorResId(I)I
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 55
    packed-switch p0, :pswitch_data_0

    .line 67
    :pswitch_0
    const v0, 0x7f090071

    :goto_0
    return v0

    .line 57
    :pswitch_1
    const v0, 0x7f09006c

    goto :goto_0

    .line 59
    :pswitch_2
    const v0, 0x7f09006d

    goto :goto_0

    .line 61
    :pswitch_3
    const v0, 0x7f090070

    goto :goto_0

    .line 63
    :pswitch_4
    const v0, 0x7f09006e

    goto :goto_0

    .line 65
    :pswitch_5
    const v0, 0x7f09006f

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 76
    packed-switch p1, :pswitch_data_0

    .line 93
    :pswitch_0
    const v0, 0x7f0900f5

    .line 96
    .local v0, "colorResourceId":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    return-object v1

    .line 78
    .end local v0    # "colorResourceId":I
    :pswitch_1
    const v0, 0x7f0900ee

    .line 79
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 81
    .end local v0    # "colorResourceId":I
    :pswitch_2
    const v0, 0x7f0900f0

    .line 82
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 84
    .end local v0    # "colorResourceId":I
    :pswitch_3
    const v0, 0x7f0900f9

    .line 85
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 87
    .end local v0    # "colorResourceId":I
    :pswitch_4
    const v0, 0x7f0900f3

    .line 88
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 90
    .end local v0    # "colorResourceId":I
    :pswitch_5
    const v0, 0x7f0900f7

    .line 91
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getShareHeaderId(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 329
    packed-switch p0, :pswitch_data_0

    .line 333
    const v0, 0x7f0c0262

    :goto_0
    return v0

    .line 331
    :pswitch_0
    const v0, 0x7f0c0263

    goto :goto_0

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static getStatusBarColor(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 631
    packed-switch p1, :pswitch_data_0

    .line 648
    :pswitch_0
    const v0, 0x7f0900b8

    .line 651
    .local v0, "colorResourceId":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 633
    .end local v0    # "colorResourceId":I
    :pswitch_1
    const v0, 0x7f0900b9

    .line 634
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 636
    .end local v0    # "colorResourceId":I
    :pswitch_2
    const v0, 0x7f0900ba

    .line 637
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 639
    .end local v0    # "colorResourceId":I
    :pswitch_3
    const v0, 0x7f0900bd

    .line 640
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 642
    .end local v0    # "colorResourceId":I
    :pswitch_4
    const v0, 0x7f0900bc

    .line 643
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 645
    .end local v0    # "colorResourceId":I
    :pswitch_5
    const v0, 0x7f0900bb

    .line 646
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 631
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getTitleContentDescriptionResourceId(Landroid/content/res/Resources;I)I
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "docType"    # I

    .prologue
    const/4 v0, -0x1

    .line 151
    packed-switch p1, :pswitch_data_0

    .line 183
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported doc type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    :goto_0
    :pswitch_1
    return v0

    .line 153
    :pswitch_2
    const v0, 0x7f0c039f

    goto :goto_0

    .line 155
    :pswitch_3
    const v0, 0x7f0c03a0

    goto :goto_0

    .line 157
    :pswitch_4
    const v0, 0x7f0c03a1

    goto :goto_0

    .line 159
    :pswitch_5
    const v0, 0x7f0c03a2

    goto :goto_0

    .line 161
    :pswitch_6
    const v0, 0x7f0c03a3

    goto :goto_0

    .line 163
    :pswitch_7
    const v0, 0x7f0c03a4

    goto :goto_0

    .line 168
    :pswitch_8
    const v0, 0x7f0c03a5

    goto :goto_0

    .line 170
    :pswitch_9
    const v0, 0x7f0c03a9

    goto :goto_0

    .line 172
    :pswitch_a
    const v0, 0x7f0c03a6

    goto :goto_0

    .line 174
    :pswitch_b
    const v0, 0x7f0c03a7

    goto :goto_0

    .line 176
    :pswitch_c
    const v0, 0x7f0c03a8

    goto :goto_0

    .line 178
    :pswitch_d
    const v0, 0x7f0c03aa

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getWarningDrawable(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 571
    packed-switch p0, :pswitch_data_0

    .line 583
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :pswitch_1
    const v0, 0x7f020143

    .line 581
    :goto_0
    return v0

    .line 575
    :pswitch_2
    const v0, 0x7f020145

    goto :goto_0

    .line 577
    :pswitch_3
    const v0, 0x7f020144

    goto :goto_0

    .line 579
    :pswitch_4
    const v0, 0x7f020146

    goto :goto_0

    .line 581
    :pswitch_5
    const v0, 0x7f020142

    goto :goto_0

    .line 571
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getWhatsNewBackgroundDrawable(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 611
    packed-switch p0, :pswitch_data_0

    .line 623
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 613
    :pswitch_1
    const v0, 0x7f0201eb

    .line 621
    :goto_0
    return v0

    .line 615
    :pswitch_2
    const v0, 0x7f0201ed

    goto :goto_0

    .line 617
    :pswitch_3
    const v0, 0x7f0201ec

    goto :goto_0

    .line 619
    :pswitch_4
    const v0, 0x7f0201ee

    goto :goto_0

    .line 621
    :pswitch_5
    const v0, 0x7f0201ea

    goto :goto_0

    .line 611
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getWhatsNewFillColor(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backend"    # I

    .prologue
    .line 588
    packed-switch p1, :pswitch_data_0

    .line 605
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported backend ID ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 590
    :pswitch_1
    const v0, 0x7f0900be

    .line 607
    .local v0, "colorResourceId":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 593
    .end local v0    # "colorResourceId":I
    :pswitch_2
    const v0, 0x7f0900bf

    .line 594
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 596
    .end local v0    # "colorResourceId":I
    :pswitch_3
    const v0, 0x7f0900c2

    .line 597
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 599
    .end local v0    # "colorResourceId":I
    :pswitch_4
    const v0, 0x7f0900c1

    .line 600
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 602
    .end local v0    # "colorResourceId":I
    :pswitch_5
    const v0, 0x7f0900c0

    .line 603
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 588
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getWishlistOffDrawable(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 555
    packed-switch p0, :pswitch_data_0

    .line 567
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :pswitch_1
    const v0, 0x7f0200f4

    .line 565
    :goto_0
    return v0

    .line 559
    :pswitch_2
    const v0, 0x7f0200f8

    goto :goto_0

    .line 561
    :pswitch_3
    const v0, 0x7f0200f6

    goto :goto_0

    .line 563
    :pswitch_4
    const v0, 0x7f0200fa

    goto :goto_0

    .line 565
    :pswitch_5
    const v0, 0x7f0200f2

    goto :goto_0

    .line 555
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getWishlistOnDrawable(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 539
    packed-switch p0, :pswitch_data_0

    .line 551
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 541
    :pswitch_1
    const v0, 0x7f0200f5

    .line 549
    :goto_0
    return v0

    .line 543
    :pswitch_2
    const v0, 0x7f0200f9

    goto :goto_0

    .line 545
    :pswitch_3
    const v0, 0x7f0200f7

    goto :goto_0

    .line 547
    :pswitch_4
    const v0, 0x7f0200fb

    goto :goto_0

    .line 549
    :pswitch_5
    const v0, 0x7f0200f3

    goto :goto_0

    .line 539
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
