.class final Lcom/google/android/finsky/utils/RateReviewHelper$6;
.super Ljava/lang/Object;
.source "RateReviewHelper.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/RateReviewHelper;->deleteReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$docId:Ljava/lang/String;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/ClientMutationCache;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$clientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    iput-object p2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$docId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$clientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    iget-object v1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$docId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->removeCachedReview(Ljava/lang/String;)V

    .line 215
    const-string v0, "Error posting review: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$context:Landroid/content/Context;

    # invokes: Lcom/google/android/finsky/utils/RateReviewHelper;->showReviewDeleteError(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/finsky/utils/RateReviewHelper;->access$100(Landroid/content/Context;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$6;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;->onRateReviewFailed()V

    .line 223
    :cond_0
    return-void
.end method
