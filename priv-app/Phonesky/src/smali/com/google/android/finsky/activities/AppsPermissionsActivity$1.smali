.class Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;
.super Ljava/lang/Object;
.source "AppsPermissionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/AppsPermissionsActivity;->updateFromDoc()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

.field final synthetic val$learnMoreLink:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/AppsPermissionsActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;->this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;->val$learnMoreLink:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;->this$0:Lcom/google/android/finsky/activities/AppsPermissionsActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;->val$learnMoreLink:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 254
    return-void
.end method
