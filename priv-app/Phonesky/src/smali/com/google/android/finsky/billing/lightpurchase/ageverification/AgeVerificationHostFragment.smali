.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;
.super Landroid/support/v4/app/Fragment;
.source "AgeVerificationHostFragment.java"

# interfaces
.implements Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment$Listener;
.implements Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;
.implements Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mBackend:I

.field private mDocidStr:Ljava/lang/String;

.field private mLastStateInstance:I

.field private mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

.field private mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    return-void
.end method

.method private fail()V
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;->onFinished(Z)V

    .line 203
    return-void
.end method

.method private getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;

    return-object v0
.end method

.method private handleSuccess(Z)V
    .locals 2
    .param p1, "isAgeVerified"    # Z

    .prologue
    .line 195
    if-eqz p1, :cond_0

    .line 196
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->setAgeVerificationRequired(Z)V

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment$Listener;->onFinished(Z)V

    .line 199
    return-void
.end method

.method private launchAgeChallengeFragment(Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;)V
    .locals 2
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mBackend:I

    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;->newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeChallengeFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 178
    return-void
.end method

.method private launchChallengeErrorFragment(Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;)V
    .locals 2
    .param p1, "challengeError"    # Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mBackend:I

    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$ChallengeError;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 186
    return-void
.end method

.method private launchSmsCodeFragment(Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;)V
    .locals 2
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mBackend:I

    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 182
    return-void
.end method

.method public static newInstance(Ljava/lang/String;ILjava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "docidStr"    # Ljava/lang/String;

    .prologue
    .line 61
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;-><init>()V

    .line 62
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v2, "AgeVerificationHostFragment.backend"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    const-string v2, "AgeVerificationHostFragment.docid_str"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method

.method private showFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 171
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 172
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 173
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 174
    return-void
.end method

.method private showLoading()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/ProgressSpinnerFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 166
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AgeVerificationHostFragment.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mBackend:I

    .line 75
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AgeVerificationHostFragment.docid_str"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mDocidStr:Ljava/lang/String;

    .line 76
    if-eqz p1, :cond_0

    .line 77
    const-string v0, "AgeVerificationHostFragment.last_state_instance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mLastStateInstance:I

    .line 79
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const v0, 0x7f04001f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onFail()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->fail()V

    .line 233
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 113
    return-void
.end method

.method public onResendSmsCode(Ljava/lang/String;)V
    .locals 1
    .param p1, "actionUrl"    # Ljava/lang/String;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->resendSmsCode(Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 107
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    const-string v0, "AgeVerificationHostFragment.last_state_instance"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mLastStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "AgeVerificationHostFragment.sidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    .line 99
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    const-string v2, "AgeVerificationHostFragment.sidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 101
    :cond_0
    return-void
.end method

.method public onStartChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 1
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->startChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    .line 238
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getStateInstance()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mLastStateInstance:I

    if-gt v0, v1, :cond_0

    .line 120
    const-string v0, "Already received state instance %d, ignore."

    new-array v1, v2, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mLastStateInstance:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getStateInstance()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mLastStateInstance:I

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->checkDocumentMaturity(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->requestAgeVerificationForm()V

    goto :goto_0

    .line 133
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->showLoading()V

    goto :goto_0

    .line 136
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getAgeChallenge()Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->launchAgeChallengeFragment(Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;)V

    goto :goto_0

    .line 139
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSmsCodeChallenge()Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->launchSmsCodeFragment(Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;)V

    goto :goto_0

    .line 142
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getSubstate()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getChallengeError()Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->launchChallengeErrorFragment(Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;)V

    goto :goto_0

    .line 145
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->getErrorHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    goto :goto_0

    .line 150
    :pswitch_6
    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->handleSuccess(Z)V

    goto :goto_0

    .line 153
    :pswitch_7
    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->handleSuccess(Z)V

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method public onSubmit(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "actionUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p2, "postParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->verifyAge(Ljava/lang/String;Ljava/util/Map;)V

    .line 214
    return-void
.end method

.method public onVerifySmsCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "actionUrl"    # Ljava/lang/String;
    .param p2, "codePostParam"    # Ljava/lang/String;
    .param p3, "code"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationHostFragment;->mSidecar:Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/AgeVerificationSidecar;->verifySmsCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    return-void
.end method
