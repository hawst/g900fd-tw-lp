.class public final Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaypalAuthResponse"
.end annotation


# instance fields
.field public encryptedAuthMessage:[B

.field public hasEncryptedAuthMessage:Z

.field public hasHashedDeviceId:Z

.field public hashedDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1829
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1830
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->clear()Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    .line 1831
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1834
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    .line 1835
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasEncryptedAuthMessage:Z

    .line 1836
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    .line 1837
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasHashedDeviceId:Z

    .line 1838
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->cachedSize:I

    .line 1839
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1856
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1857
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasEncryptedAuthMessage:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1858
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1861
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasHashedDeviceId:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1862
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1865
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1873
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1874
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1878
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1879
    :sswitch_0
    return-object p0

    .line 1884
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    .line 1885
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasEncryptedAuthMessage:Z

    goto :goto_0

    .line 1889
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    .line 1890
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasHashedDeviceId:Z

    goto :goto_0

    .line 1874
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1804
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1845
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasEncryptedAuthMessage:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1846
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1848
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasHashedDeviceId:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1849
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1851
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1852
    return-void
.end method
