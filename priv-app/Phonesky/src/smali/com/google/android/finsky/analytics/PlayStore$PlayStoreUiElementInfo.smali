.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreUiElementInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    }
.end annotation


# instance fields
.field public authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

.field public document:Ljava/lang/String;

.field public hasDocument:Z

.field public hasHost:Z

.field public hasIsImeAction:Z

.field public hasOfferType:Z

.field public hasSerialDocid:Z

.field public host:Ljava/lang/String;

.field public instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

.field public isImeAction:Z

.field public offerType:I

.field public serialDocid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 158
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 161
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    .line 163
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    .line 165
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    .line 167
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasDocument:Z

    .line 168
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    .line 169
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasOfferType:Z

    .line 170
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 171
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 172
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 173
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->cachedSize:I

    .line 174
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 206
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 207
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    if-eqz v1, :cond_0

    .line 208
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 212
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 216
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasDocument:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 220
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasOfferType:Z

    if-nez v1, :cond_7

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    if-eqz v1, :cond_8

    .line 224
    :cond_7
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    if-eqz v1, :cond_a

    .line 228
    :cond_9
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-eqz v1, :cond_b

    .line 232
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 243
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 244
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 248
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    :sswitch_0
    return-object p0

    .line 254
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    if-nez v1, :cond_1

    .line 255
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 261
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    .line 262
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    goto :goto_0

    .line 266
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    .line 267
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    goto :goto_0

    .line 271
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    .line 272
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasDocument:Z

    goto :goto_0

    .line 276
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    .line 277
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasOfferType:Z

    goto :goto_0

    .line 281
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 282
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    goto :goto_0

    .line 286
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-nez v1, :cond_2

    .line 287
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 244
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    if-eqz v0, :cond_0

    .line 181
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 183
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 186
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 187
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 189
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasDocument:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 190
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->document:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 192
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasOfferType:Z

    if-nez v0, :cond_7

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    if-eqz v0, :cond_8

    .line 193
    :cond_7
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->offerType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 195
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    if-eqz v0, :cond_a

    .line 196
    :cond_9
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 198
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-eqz v0, :cond_b

    .line 199
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 201
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 202
    return-void
.end method
