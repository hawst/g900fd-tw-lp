.class Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;
.super Ljava/lang/Object;
.source "SearchSuggestionFetcher.java"

# interfaces
.implements Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/providers/SearchSuggestionFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RefCountedOnCompleteListener"
.end annotation


# instance fields
.field mRefCount:I

.field mWrappedListener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I

    .line 56
    iput-object p1, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mWrappedListener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    .line 57
    return-void
.end method


# virtual methods
.method public declared-synchronized addProducer()V
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onComplete()V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gtz v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 65
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I

    .line 66
    iget v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mRefCount:I

    if-gtz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->mWrappedListener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    invoke-interface {v0}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
