.class public final Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CarrierBilling.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CarrierBilling;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerifyAssociationResponse"
.end annotation


# instance fields
.field public billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field public carrierErrorMessage:Ljava/lang/String;

.field public carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

.field public hasCarrierErrorMessage:Z

.field public hasStatus:Z

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->clear()Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    .line 134
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 137
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    .line 138
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasStatus:Z

    .line 139
    iput-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 140
    iput-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    .line 142
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasCarrierErrorMessage:Z

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->cachedSize:I

    .line 144
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 167
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 168
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasStatus:Z

    if-eqz v1, :cond_1

    .line 169
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v1, :cond_2

    .line 173
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    if-eqz v1, :cond_3

    .line 177
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasCarrierErrorMessage:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 181
    :cond_4
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 192
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 193
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 197
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 198
    :sswitch_0
    return-object p0

    .line 203
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 204
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 209
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    .line 210
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasStatus:Z

    goto :goto_0

    .line 216
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v2, :cond_1

    .line 217
    new-instance v2, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 219
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 223
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    if-nez v2, :cond_2

    .line 224
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    .line 226
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 230
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    .line 231
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasCarrierErrorMessage:Z

    goto :goto_0

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 150
    iget v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasStatus:Z

    if-eqz v0, :cond_1

    .line 151
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->status:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v0, :cond_2

    .line 154
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    if-eqz v0, :cond_3

    .line 157
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 159
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->hasCarrierErrorMessage:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 160
    :cond_4
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;->carrierErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 162
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 163
    return-void
.end method
