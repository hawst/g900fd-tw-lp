.class public Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;
.super Landroid/app/Activity;
.source "LaunchUrlHandlerActivity.java"


# static fields
.field private static final BASE_DETAILS_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "http://market.android.com/details"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->BASE_DETAILS_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static getGoToMarketHomeIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected static handleUrl(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/analytics/FinskyEventLog;)Landroid/content/Intent;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "input"    # Landroid/content/Intent;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "eventLogger"    # Lcom/google/android/finsky/analytics/FinskyEventLog;

    .prologue
    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v15

    .line 97
    .local v15, "uri":Landroid/net/Uri;
    const-string v1, "url"

    invoke-virtual {v15, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 98
    .local v10, "encodedContinuation":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    const-string v1, "Launch URL without continue URL"

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    invoke-virtual {v15}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v17, "http"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v17, "market.android.com"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v17, "details"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v15

    .line 109
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 110
    invoke-static/range {p0 .. p1}, Lcom/google/android/finsky/utils/IntentUtils;->createForwardToMainActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v12

    .line 174
    :cond_0
    :goto_0
    return-object v12

    .line 114
    :cond_1
    const-string v1, "id"

    invoke-virtual {v15, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    const-string v1, "Launch URL without package name"

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->getGoToMarketHomeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v12

    goto :goto_0

    .line 121
    :cond_2
    const-string v1, "min_version"

    invoke-virtual {v15, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 122
    .local v16, "version":Ljava/lang/String;
    const/4 v4, -0x1

    .line 123
    .local v4, "versionCode":I
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 125
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 132
    :cond_3
    :goto_1
    const/4 v13, -0x1

    .line 134
    .local v13, "localVersion":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v7

    .line 135
    .local v7, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v7, :cond_4

    iget-object v1, v7, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v1, :cond_4

    .line 136
    iget-object v1, v7, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v13, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 138
    :cond_4
    if-ltz v13, :cond_5

    if-lt v13, v4, :cond_5

    const/4 v5, 0x1

    .line 141
    .local v5, "newEnough":Z
    :goto_2
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 142
    .local v2, "decodedUrl":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 143
    .local v14, "pm":Landroid/content/pm/PackageManager;
    invoke-static {v14, v3, v2}, Lcom/google/android/finsky/utils/IntentUtils;->canResolveUrl(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .local v6, "canResolveUrl":Z
    move-object/from16 v1, p3

    .line 145
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 148
    if-eqz v5, :cond_6

    if-eqz v6, :cond_6

    .line 150
    new-instance v12, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v12, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v12, "intent":Landroid/content/Intent;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v12, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 154
    if-eqz v7, :cond_0

    iget-object v1, v7, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v1, :cond_0

    .line 155
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/receivers/ExpireLaunchUrlReceiver;->cancel(Landroid/content/Context;Ljava/lang/String;)V

    .line 156
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-interface {v1, v3, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    .end local v2    # "decodedUrl":Ljava/lang/String;
    .end local v5    # "newEnough":Z
    .end local v6    # "canResolveUrl":Z
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v14    # "pm":Landroid/content/pm/PackageManager;
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .line 160
    .restart local v2    # "decodedUrl":Ljava/lang/String;
    .restart local v5    # "newEnough":Z
    .restart local v6    # "canResolveUrl":Z
    .restart local v14    # "pm":Landroid/content/pm/PackageManager;
    :cond_6
    const-string v1, "referrer"

    invoke-virtual {v15, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 164
    .local v11, "externalReferrer":Ljava/lang/String;
    sget-object v1, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->BASE_DETAILS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    .line 165
    .local v9, "detailsUri":Landroid/net/Uri$Builder;
    const-string v1, "id"

    invoke-virtual {v9, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 166
    const-string v1, "url"

    invoke-virtual {v9, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 167
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 168
    const-string v1, "referrer"

    invoke-virtual {v9, v1, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 172
    :cond_7
    new-instance v8, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v8, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 173
    .local v8, "detailsIntent":Landroid/content/Intent;
    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 174
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/finsky/utils/IntentUtils;->createForwardToMainActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v12

    goto/16 :goto_0

    .line 126
    .end local v2    # "decodedUrl":Ljava/lang/String;
    .end local v5    # "newEnough":Z
    .end local v6    # "canResolveUrl":Z
    .end local v7    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v8    # "detailsIntent":Landroid/content/Intent;
    .end local v9    # "detailsUri":Landroid/net/Uri$Builder;
    .end local v11    # "externalReferrer":Ljava/lang/String;
    .end local v13    # "localVersion":I
    .end local v14    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isLimitedOrSchoolEduUser()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    invoke-static {p0}, Lcom/google/android/finsky/activities/AccessRestrictedActivity;->showLimitedUserUI(Landroid/app/Activity;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->finish()V

    .line 89
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->getGoToMarketHomeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 63
    .local v1, "homeIntent":Landroid/content/Intent;
    sget-object v2, Lcom/google/android/finsky/config/G;->launchUrlsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 64
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->startActivity(Landroid/content/Intent;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;->finish()V

    goto :goto_0

    .line 71
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    .line 72
    .local v0, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    new-instance v2, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity$1;-><init>(Lcom/google/android/finsky/activities/LaunchUrlHandlerActivity;Landroid/content/Intent;Lcom/google/android/finsky/appstate/AppStates;)V

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
