.class Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;
.super Ljava/lang/Object;
.source "DetailsSummaryAppsViewBinder.java"

# interfaces
.implements Lcom/google/android/finsky/utils/AppSupport$RefundListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refundApp(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRefundComplete(Z)V
    .locals 2
    .param p1, "succeeded"    # Z

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mIsPendingRefund:Z

    .line 455
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refresh()V

    .line 456
    return-void
.end method

.method public onRefundStart()V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mIsPendingRefund:Z

    .line 449
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refresh()V

    .line 450
    return-void
.end method
