.class public final Lcom/google/android/finsky/protos/DocumentV2$Template;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation


# instance fields
.field public actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

.field public addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

.field public containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

.field public dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

.field public editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

.field public emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

.field public moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

.field public myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

.field public nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

.field public purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

.field public rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

.field public rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

.field public recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

.field public recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

.field public seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

.field public singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

.field public tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

.field public warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3006
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3007
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Template;->clear()Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 3008
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3011
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3012
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3013
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3014
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3015
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3016
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3017
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3018
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3019
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 3020
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3021
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 3022
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 3023
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 3024
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 3025
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 3026
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 3027
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 3028
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 3029
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 3030
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3031
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3032
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    .line 3033
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 3034
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 3035
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    .line 3036
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->cachedSize:I

    .line 3037
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3123
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3124
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v1, :cond_0

    .line 3125
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3128
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_1

    .line 3129
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3132
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_2

    .line 3133
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3136
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_3

    .line 3137
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3140
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_4

    .line 3141
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3144
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_5

    .line 3145
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3148
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_6

    .line 3149
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3152
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v1, :cond_7

    .line 3153
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3156
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v1, :cond_8

    .line 3157
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3160
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_9

    .line 3161
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3164
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v1, :cond_a

    .line 3165
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3168
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v1, :cond_b

    .line 3169
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3172
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v1, :cond_c

    .line 3173
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3176
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v1, :cond_d

    .line 3177
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3180
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v1, :cond_e

    .line 3181
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3184
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v1, :cond_f

    .line 3185
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3188
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v1, :cond_10

    .line 3189
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3192
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v1, :cond_11

    .line 3193
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3196
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v1, :cond_12

    .line 3197
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3200
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v1, :cond_13

    .line 3201
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3204
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v1, :cond_14

    .line 3205
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3208
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v1, :cond_15

    .line 3209
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3212
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v1, :cond_16

    .line 3213
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3216
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-eqz v1, :cond_17

    .line 3217
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3220
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-eqz v1, :cond_18

    .line 3221
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3224
    :cond_18
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3233
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3237
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3238
    :sswitch_0
    return-object p0

    .line 3243
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-nez v1, :cond_1

    .line 3244
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3246
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3250
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_2

    .line 3251
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3253
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3257
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_3

    .line 3258
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3260
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3264
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_4

    .line 3265
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3267
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3271
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_5

    .line 3272
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3274
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3278
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_6

    .line 3279
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3281
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3285
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_7

    .line 3286
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3288
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3292
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-nez v1, :cond_8

    .line 3293
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 3295
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3299
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-nez v1, :cond_9

    .line 3300
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3302
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3306
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_a

    .line 3307
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3309
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3313
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-nez v1, :cond_b

    .line 3314
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 3316
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3320
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-nez v1, :cond_c

    .line 3321
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 3323
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3327
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-nez v1, :cond_d

    .line 3328
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 3330
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3334
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-nez v1, :cond_e

    .line 3335
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 3337
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3341
    :sswitch_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-nez v1, :cond_f

    .line 3342
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 3344
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3348
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-nez v1, :cond_10

    .line 3349
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 3351
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3355
    :sswitch_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-nez v1, :cond_11

    .line 3356
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 3358
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3362
    :sswitch_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-nez v1, :cond_12

    .line 3363
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3365
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3369
    :sswitch_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-nez v1, :cond_13

    .line 3370
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3372
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3376
    :sswitch_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-nez v1, :cond_14

    .line 3377
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 3379
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3383
    :sswitch_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-nez v1, :cond_15

    .line 3384
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 3386
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3390
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-nez v1, :cond_16

    .line 3391
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 3393
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3397
    :sswitch_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-nez v1, :cond_17

    .line 3398
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 3400
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3404
    :sswitch_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-nez v1, :cond_18

    .line 3405
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    .line 3407
    :cond_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3411
    :sswitch_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-nez v1, :cond_19

    .line 3412
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    .line 3414
    :cond_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3233
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2914
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Template;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3043
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v0, :cond_0

    .line 3044
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3046
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_1

    .line 3047
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3049
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_2

    .line 3050
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3052
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_3

    .line 3053
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3055
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_4

    .line 3056
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3058
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_5

    .line 3059
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3061
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_6

    .line 3062
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3064
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v0, :cond_7

    .line 3065
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3067
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v0, :cond_8

    .line 3068
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3070
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_9

    .line 3071
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3073
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v0, :cond_a

    .line 3074
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3076
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v0, :cond_b

    .line 3077
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3079
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v0, :cond_c

    .line 3080
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3082
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v0, :cond_d

    .line 3083
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3085
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v0, :cond_e

    .line 3086
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3088
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v0, :cond_f

    .line 3089
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3091
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v0, :cond_10

    .line 3092
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3094
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v0, :cond_11

    .line 3095
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3097
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v0, :cond_12

    .line 3098
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3100
    :cond_12
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v0, :cond_13

    .line 3101
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3103
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v0, :cond_14

    .line 3104
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3106
    :cond_14
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v0, :cond_15

    .line 3107
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3109
    :cond_15
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v0, :cond_16

    .line 3110
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3112
    :cond_16
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    if-eqz v0, :cond_17

    .line 3113
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->moreByCreatorContainer:Lcom/google/android/finsky/protos/DocumentV2$MoreByCreatorContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3115
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    if-eqz v0, :cond_18

    .line 3116
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->purchaseHistoryContainer:Lcom/google/android/finsky/protos/DocumentV2$PurchaseHistoryContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3118
    :cond_18
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3119
    return-void
.end method
