.class public final Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OBSOLETE_FinskyTranslatedText"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;


# instance fields
.field public hasSourceLocale:Z

.field public hasTargetLocale:Z

.field public hasText:Z

.field public sourceLocale:Ljava/lang/String;

.field public targetLocale:Ljava/lang/String;

.field public text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->clear()Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    .line 414
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    .locals 2

    .prologue
    .line 389
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    if-nez v0, :cond_1

    .line 390
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 392
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    if-nez v0, :cond_0

    .line 393
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    .line 395
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    return-object v0

    .line 395
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 417
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    .line 418
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasText:Z

    .line 419
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    .line 420
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasSourceLocale:Z

    .line 421
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    .line 422
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasTargetLocale:Z

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->cachedSize:I

    .line 424
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 444
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 445
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasText:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 446
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasSourceLocale:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 450
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasTargetLocale:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 454
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 465
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 466
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 470
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 471
    :sswitch_0
    return-object p0

    .line 476
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    .line 477
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasText:Z

    goto :goto_0

    .line 481
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    .line 482
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasSourceLocale:Z

    goto :goto_0

    .line 486
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    .line 487
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasTargetLocale:Z

    goto :goto_0

    .line 466
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasText:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->text:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 433
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasSourceLocale:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 434
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->sourceLocale:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 436
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->hasTargetLocale:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 437
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->targetLocale:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 439
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 440
    return-void
.end method
