.class Lcom/google/android/finsky/navigationmanager/NavigationManager$4;
.super Ljava/lang/Object;
.source "NavigationManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/navigationmanager/NavigationManager;->getResolvedLinkClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field final synthetic val$searchBackend:I

.field final synthetic val$searchQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 839
    iput-object p1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iput-object p3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iput-object p5, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$searchQuery:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$searchBackend:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 843
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v5, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 870
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 846
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$searchQuery:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$searchBackend:I

    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSearch(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 847
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 848
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 849
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    goto :goto_0

    .line 850
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 851
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v5, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 853
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-eqz v0, :cond_0

    .line 854
    const/4 v7, 0x0

    .line 855
    .local v7, "redeemCodePrefill":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 856
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v7, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 858
    :cond_5
    const/4 v8, 0x0

    .line 859
    .local v8, "redeemPartnerPayload":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 860
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v8, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 862
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1, v7, v8}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 867
    .local v6, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->val$clickLogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 868
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;->this$0:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivityForResolveLink()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
