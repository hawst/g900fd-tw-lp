.class public final Lcom/google/android/finsky/protos/Preloads$Preload;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Preloads.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Preloads;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Preload"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Preloads$Preload;


# instance fields
.field public deliveryToken:Ljava/lang/String;

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasDeliveryToken:Z

.field public hasTitle:Z

.field public hasVersionCode:Z

.field public icon:Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Preloads$Preload;->clear()Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 173
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Preloads$Preload;
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/finsky/protos/Preloads$Preload;->_emptyArray:[Lcom/google/android/finsky/protos/Preloads$Preload;

    if-nez v0, :cond_1

    .line 143
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 145
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Preloads$Preload;->_emptyArray:[Lcom/google/android/finsky/protos/Preloads$Preload;

    if-nez v0, :cond_0

    .line 146
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Preloads$Preload;

    sput-object v0, Lcom/google/android/finsky/protos/Preloads$Preload;->_emptyArray:[Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 148
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Preloads$Preload;->_emptyArray:[Lcom/google/android/finsky/protos/Preloads$Preload;

    return-object v0

    .line 148
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Preloads$Preload;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 176
    iput-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 177
    iput v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    .line 178
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasVersionCode:Z

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    .line 180
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasTitle:Z

    .line 181
    iput-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    .line 182
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    .line 183
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasDeliveryToken:Z

    .line 184
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->cachedSize:I

    .line 185
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 212
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 213
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasVersionCode:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    if-eqz v1, :cond_2

    .line 217
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasTitle:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 221
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_5

    .line 225
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasDeliveryToken:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 229
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Preloads$Preload;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 240
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 241
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 245
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    :sswitch_0
    return-object p0

    .line 251
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 252
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 254
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 258
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    .line 259
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasVersionCode:Z

    goto :goto_0

    .line 263
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    .line 264
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasTitle:Z

    goto :goto_0

    .line 268
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_2

    .line 269
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 275
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    .line 276
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasDeliveryToken:Z

    goto :goto_0

    .line 241
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Preloads$Preload;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Preloads$Preload;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 194
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasVersionCode:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    if-eqz v0, :cond_2

    .line 195
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 197
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasTitle:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 198
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_5

    .line 201
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 203
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->hasDeliveryToken:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 204
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 206
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 207
    return-void
.end method
