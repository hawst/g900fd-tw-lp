.class public final Lcom/google/android/finsky/protos/Notifications$Notification;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Notification"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Notifications$Notification;


# instance fields
.field public appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

.field public appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

.field public docTitle:Ljava/lang/String;

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasDocTitle:Z

.field public hasNotificationId:Z

.field public hasNotificationType:Z

.field public hasTimestamp:Z

.field public hasUserEmail:Z

.field public inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

.field public libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

.field public libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public notificationId:Ljava/lang/String;

.field public notificationType:I

.field public purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

.field public purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

.field public timestamp:J

.field public userEmail:Ljava/lang/String;

.field public userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 778
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 779
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Notifications$Notification;->clear()Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 780
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Notifications$Notification;
    .locals 2

    .prologue
    .line 720
    sget-object v0, Lcom/google/android/finsky/protos/Notifications$Notification;->_emptyArray:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-nez v0, :cond_1

    .line 721
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 723
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Notifications$Notification;->_emptyArray:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-nez v0, :cond_0

    .line 724
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Notifications$Notification;

    sput-object v0, Lcom/google/android/finsky/protos/Notifications$Notification;->_emptyArray:[Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 726
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 728
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Notifications$Notification;->_emptyArray:[Lcom/google/android/finsky/protos/Notifications$Notification;

    return-object v0

    .line 726
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/protos/Notifications$Notification;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1036
    new-instance v0, Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/Notifications$Notification;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Notifications$Notification;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Notifications$Notification;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 783
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    .line 784
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationType:Z

    .line 785
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    .line 786
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationId:Z

    .line 787
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    .line 788
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasTimestamp:Z

    .line 789
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 790
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    .line 791
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasDocTitle:Z

    .line 792
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    .line 793
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasUserEmail:Z

    .line 794
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 795
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    .line 796
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 797
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    .line 798
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    .line 799
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    .line 800
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    .line 801
    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    .line 802
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->cachedSize:I

    .line 803
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 856
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 857
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationType:Z

    if-eqz v1, :cond_1

    .line 858
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 861
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasTimestamp:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 862
    :cond_2
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 865
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_4

    .line 866
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 869
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasDocTitle:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 870
    :cond_5
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 873
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasUserEmail:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 874
    :cond_7
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 877
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    if-eqz v1, :cond_9

    .line 878
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 881
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v1, :cond_a

    .line 882
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 885
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    if-eqz v1, :cond_b

    .line 886
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 889
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    if-eqz v1, :cond_c

    .line 890
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 893
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    if-eqz v1, :cond_d

    .line 894
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 897
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    if-eqz v1, :cond_e

    .line 898
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    :cond_e
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationId:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 902
    :cond_f
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 905
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_11

    .line 906
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 909
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    if-eqz v1, :cond_12

    .line 910
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 913
    :cond_12
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$Notification;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 921
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 922
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 926
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 927
    :sswitch_0
    return-object p0

    .line 932
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 933
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 941
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    .line 942
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationType:Z

    goto :goto_0

    .line 948
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    .line 949
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasTimestamp:Z

    goto :goto_0

    .line 953
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v2, :cond_1

    .line 954
    new-instance v2, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 956
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 960
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    .line 961
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasDocTitle:Z

    goto :goto_0

    .line 965
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    .line 966
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasUserEmail:Z

    goto :goto_0

    .line 970
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    if-nez v2, :cond_2

    .line 971
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    .line 973
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 977
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v2, :cond_3

    .line 978
    new-instance v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 980
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 984
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    if-nez v2, :cond_4

    .line 985
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    .line 987
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 991
    :sswitch_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    if-nez v2, :cond_5

    .line 992
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    .line 994
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 998
    :sswitch_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    if-nez v2, :cond_6

    .line 999
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    .line 1001
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1005
    :sswitch_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    if-nez v2, :cond_7

    .line 1006
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    .line 1008
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1012
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    .line 1013
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationId:Z

    goto/16 :goto_0

    .line 1017
    :sswitch_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v2, :cond_8

    .line 1018
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1020
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1024
    :sswitch_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    if-nez v2, :cond_9

    .line 1025
    new-instance v2, Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    .line 1027
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 922
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch

    .line 933
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 705
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Notifications$Notification;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$Notification;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 809
    iget v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationType:Z

    if-eqz v0, :cond_1

    .line 810
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationType:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 812
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasTimestamp:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 813
    :cond_2
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->timestamp:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 815
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_4

    .line 816
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 818
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasDocTitle:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 819
    :cond_5
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->docTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 821
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasUserEmail:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 822
    :cond_7
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userEmail:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 824
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    if-eqz v0, :cond_9

    .line 825
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appData:Lcom/google/android/finsky/protos/Notifications$AndroidAppNotificationData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 827
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v0, :cond_a

    .line 828
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 830
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    if-eqz v0, :cond_b

    .line 831
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseRemovalData:Lcom/google/android/finsky/protos/Notifications$PurchaseRemovalData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 833
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    if-eqz v0, :cond_c

    .line 834
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->userNotificationData:Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 836
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    if-eqz v0, :cond_d

    .line 837
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->inAppNotificationData:Lcom/google/android/finsky/protos/Notifications$InAppNotificationData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 839
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    if-eqz v0, :cond_e

    .line 840
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->purchaseDeclinedData:Lcom/google/android/finsky/protos/Notifications$PurchaseDeclinedData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 842
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->hasNotificationId:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 843
    :cond_f
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->notificationId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 845
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v0, :cond_11

    .line 846
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 848
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    if-eqz v0, :cond_12

    .line 849
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$Notification;->libraryDirtyData:Lcom/google/android/finsky/protos/Notifications$LibraryDirtyData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 851
    :cond_12
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 852
    return-void
.end method
