.class public Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;
.super Ljava/lang/Object;
.source "DownloadManagerLegacyImpl.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadManagerFacade;


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final FROYO_CONTENT_URI:Landroid/net/Uri;

.field private static final QUERY_FILENAME_PROJECTION:[Ljava/lang/String;

.field private static sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;


# instance fields
.field private mContentObserver:Landroid/database/ContentObserver;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;

.field private mListenerCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 113
    const-string v0, "content://downloads/my_downloads"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->CONTENT_URI:Landroid/net/Uri;

    .line 118
    const-string v0, "content://downloads/download"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->FROYO_CONTENT_URI:Landroid/net/Uri;

    .line 121
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    .line 127
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->QUERY_FILENAME_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;

    .line 149
    const-string v1, "download-manager-thread"

    invoke-static {v1}, Lcom/google/android/finsky/utils/BackgroundThreadFactory;->createHandlerThread(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 150
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 151
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mHandler:Landroid/os/Handler;

    .line 153
    new-instance v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$1;

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$1;-><init>(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentObserver:Landroid/database/ContentObserver;

    .line 164
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->onChange()V

    return-void
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$300(Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sniffDownloadManagerVersion(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->synchronousRemove(Landroid/net/Uri;)V

    return-void
.end method

.method private getContentUriForContentObserver()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 503
    invoke-static {}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->isFroyoDownloadManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    sget-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->FROYO_CONTENT_URI:Landroid/net/Uri;

    .line 506
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private getContentUriString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 518
    invoke-static {}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->isFroyoDownloadManager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://downloads/download/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://downloads/my_downloads/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isFroyoDownloadManager()Z
    .locals 2

    .prologue
    .line 475
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 476
    const/4 v0, 0x0

    .line 489
    :goto_0
    return v0

    .line 480
    :cond_0
    sget-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 481
    sget-object v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 489
    :cond_1
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->downloadManagerUsesFroyoStrings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method private makeRequest(Lcom/google/android/finsky/download/Download;)Landroid/content/ContentValues;
    .locals 11
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x2

    .line 194
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 198
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v6, "otheruid"

    const/16 v7, 0x3e8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 201
    const-string v6, "uri"

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getRequestedDestination()Landroid/net/Uri;

    move-result-object v3

    .line 205
    .local v3, "fileUri":Landroid/net/Uri;
    if-eqz v3, :cond_3

    .line 206
    const-string v6, "destination"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 207
    const-string v6, "hint"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :goto_0
    const-string v6, "notificationpackage"

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v6, "notificationclass"

    const-class v7, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getCookieName()Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "cookieName":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getCookieValue()Ljava/lang/String;

    move-result-object v2

    .line 219
    .local v2, "cookieValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 220
    const-string v6, "cookiedata"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getInvisible()Z

    move-result v4

    .line 225
    .local v4, "invisible":Z
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 226
    .local v5, "title":Ljava/lang/String;
    if-nez v4, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 227
    :cond_1
    const-string v6, "visibility"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 234
    :goto_1
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getWifiOnly()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 235
    const-string v6, "allowed_network_types"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const-string v6, "is_public_api"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 245
    const-string v6, "allow_roaming"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 248
    :cond_2
    return-object v0

    .line 209
    .end local v1    # "cookieName":Ljava/lang/String;
    .end local v2    # "cookieValue":Ljava/lang/String;
    .end local v4    # "invisible":Z
    .end local v5    # "title":Ljava/lang/String;
    :cond_3
    const-string v6, "destination"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 229
    .restart local v1    # "cookieName":Ljava/lang/String;
    .restart local v2    # "cookieValue":Ljava/lang/String;
    .restart local v4    # "invisible":Z
    .restart local v5    # "title":Ljava/lang/String;
    :cond_4
    const-string v6, "visibility"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    const-string v6, "title"

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private onChange()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListener:Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;

    .line 359
    .local v0, "listener":Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->unregisterListener(Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)V

    .line 360
    if-eqz v0, :cond_0

    .line 361
    invoke-interface {v0}, Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;->onChange()V

    .line 363
    :cond_0
    return-void
.end method

.method private static sniffDownloadManagerVersion(Landroid/net/Uri;)V
    .locals 5
    .param p0, "fromUri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 448
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    sget-object v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 455
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 456
    .local v0, "uriString":Ljava/lang/String;
    const-string v1, "content://downloads/download"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 457
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sput-object v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    .line 458
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->downloadManagerUsesFroyoStrings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 459
    :cond_2
    const-string v1, "content://downloads/my_downloads"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 460
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->sDownloadManagerUsesFroyoStrings:Ljava/lang/Boolean;

    .line 461
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->downloadManagerUsesFroyoStrings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 463
    :cond_3
    const-string v1, "Unknown download manager URI string: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private synchronousRemove(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 262
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 267
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, p1, v1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 271
    :cond_0
    return-void
.end method


# virtual methods
.method public enqueue(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V
    .locals 5
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/download/Download;",
            "Lcom/google/android/finsky/utils/ParameterizedRunnable",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p2, "listener":Lcom/google/android/finsky/utils/ParameterizedRunnable;, "Lcom/google/android/finsky/utils/ParameterizedRunnable<Landroid/net/Uri;>;"
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "google_sdk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    const-string v1, "Skip download of %s because emulator"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :goto_0
    return-void

    .line 172
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->makeRequest(Lcom/google/android/finsky/download/Download;)Landroid/content/ContentValues;

    move-result-object v0

    .line 173
    .local v0, "request":Landroid/content/ContentValues;
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$2;-><init>(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;Landroid/content/ContentValues;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public getFileUriForContentUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 9
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    .line 376
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    .end local p1    # "contentUri":Landroid/net/Uri;
    :goto_0
    return-object p1

    .line 380
    .restart local p1    # "contentUri":Landroid/net/Uri;
    :cond_0
    const/4 v7, 0x0

    .line 381
    .local v7, "fileName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 383
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->QUERY_FILENAME_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 385
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 386
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 389
    :cond_1
    if-eqz v6, :cond_2

    .line 390
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 394
    :cond_2
    if-nez v7, :cond_4

    move-object p1, v8

    .line 395
    goto :goto_0

    .line 389
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 390
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 398
    :cond_4
    new-instance v0, Ljava/io/File;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0
.end method

.method public getUriFromBroadcast(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, -0x1

    .line 406
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 408
    .local v3, "intentUri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 428
    .end local v3    # "intentUri":Landroid/net/Uri;
    :goto_0
    return-object v3

    .line 414
    .restart local v3    # "intentUri":Landroid/net/Uri;
    :cond_0
    const-string v4, "extra_download_id"

    invoke-virtual {p1, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 415
    .local v0, "id":J
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    .line 417
    const-string v4, "extra_click_download_ids"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    .line 419
    .local v2, "idArray":[J
    if-eqz v2, :cond_1

    array-length v4, v2

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 420
    const/4 v4, 0x0

    aget-wide v0, v2, v4

    .line 423
    .end local v2    # "idArray":[J
    :cond_1
    cmp-long v4, v0, v6

    if-nez v4, :cond_2

    .line 425
    const/4 v3, 0x0

    goto :goto_0

    .line 428
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->getContentUriString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)Ljava/util/List;
    .locals 23
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "listener"    # Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 282
    if-nez p1, :cond_0

    .line 283
    if-nez p2, :cond_1

    .line 284
    sget-object p1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->CONTENT_URI:Landroid/net/Uri;

    .line 292
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 293
    .local v10, "cursor":Landroid/database/Cursor;
    if-nez v10, :cond_2

    .line 294
    const-string v21, "Download progress cursor null"

    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v19

    .line 354
    :goto_1
    return-object v19

    .line 286
    .end local v10    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->getContentUriForContentObserver()Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    .line 298
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    .line 299
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v19

    .line 340
    .local v19, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    :cond_3
    if-nez p2, :cond_6

    .line 342
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 301
    .end local v19    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v19

    .line 302
    .restart local v19    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    const-string v21, "_id"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 304
    .local v15, "indexId":I
    const-string v21, "status"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 306
    .local v17, "indexStatus":I
    const-string v21, "current_bytes"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 308
    .local v14, "indexCurrentBytes":I
    const-string v21, "total_bytes"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 311
    .local v18, "indexTotalBytes":I
    const-string v21, "allowed_network_types"

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 313
    .local v16, "indexNetworkTypes":I
    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 314
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 315
    .local v12, "id":J
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    .line 316
    .local v11, "idString":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->getContentUriString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 317
    .local v20, "uriString":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 318
    .local v3, "uri":Landroid/net/Uri;
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 319
    .local v8, "status":I
    invoke-interface {v10, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 320
    .local v4, "currentBytes":J
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 325
    .local v6, "totalBytes":J
    const/16 v21, 0xc3

    move/from16 v0, v21

    if-ne v8, v0, :cond_5

    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    .line 327
    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 328
    .local v9, "allowedNetworks":I
    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v9, v0, :cond_5

    .line 329
    const/16 v8, 0xc4

    .line 333
    .end local v9    # "allowedNetworks":I
    :cond_5
    new-instance v2, Lcom/google/android/finsky/download/DownloadProgress;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/finsky/download/DownloadProgress;-><init>(Landroid/net/Uri;JJI)V

    .line 335
    .local v2, "entry":Lcom/google/android/finsky/download/DownloadProgress;
    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 346
    .end local v2    # "entry":Lcom/google/android/finsky/download/DownloadProgress;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "currentBytes":J
    .end local v6    # "totalBytes":J
    .end local v8    # "status":I
    .end local v11    # "idString":Ljava/lang/String;
    .end local v12    # "id":J
    .end local v14    # "indexCurrentBytes":I
    .end local v15    # "indexId":I
    .end local v16    # "indexNetworkTypes":I
    .end local v17    # "indexStatus":I
    .end local v18    # "indexTotalBytes":I
    .end local v20    # "uriString":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    move-object/from16 v21, v0

    if-eqz v21, :cond_7

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentObserver:Landroid/database/ContentObserver;

    move-object/from16 v22, v0

    invoke-interface/range {v21 .. v22}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_7
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentObserver:Landroid/database/ContentObserver;

    move-object/from16 v22, v0

    invoke-interface/range {v21 .. v22}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 352
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListener:Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;

    goto/16 :goto_1
.end method

.method public remove(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl$3;-><init>(Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 259
    return-void
.end method

.method public unregisterListener(Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mContentObserver:Landroid/database/ContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListenerCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 371
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadManagerLegacyImpl;->mListener:Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;

    .line 372
    return-void
.end method
