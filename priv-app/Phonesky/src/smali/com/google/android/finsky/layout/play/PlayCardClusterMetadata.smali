.class public Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;,
        Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    }
.end annotation


# static fields
.field public static final CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_LARGE_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MINI:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_MINI_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_PERSON:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field public static final CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private mAlignToParentEndIfNecessary:Z

.field private mCardMetadataForMinCellHeight:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field private mHeight:I

.field private mLeadingGap:I

.field private mRespectChildHeight:Z

.field private mRespectChildThumbnailAspectRatio:Z

.field private mTiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mViewportWidth:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const v6, 0x3fb872b0    # 1.441f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x2

    const/4 v3, 0x4

    .line 141
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040128

    invoke-direct {v0, v1, v4, v7, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MINI:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 144
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040136

    invoke-direct {v0, v1, v4, v7, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 147
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040125

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 150
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040126

    invoke-direct {v0, v1, v3, v7, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 153
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040122

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 156
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040128

    invoke-direct {v0, v1, v4, v3, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MINI_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 159
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040136

    invoke-direct {v0, v1, v4, v3, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 162
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040125

    invoke-direct {v0, v1, v3, v4, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 165
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040126

    invoke-direct {v0, v1, v3, v3, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 168
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040123

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 171
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040122

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 174
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f04012d

    invoke-direct {v0, v1, v4, v7, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIF)V

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_PERSON:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    .line 209
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mWidth:I

    .line 210
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mHeight:I

    .line 211
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mViewportWidth:I

    .line 212
    return-void
.end method

.method public static getAspectRatio(I)F
    .locals 1
    .param p0, "documentType"    # I

    .prologue
    .line 312
    sparse-switch p0, :sswitch_data_0

    .line 326
    const v0, 0x3fb872b0    # 1.441f

    :goto_0
    return v0

    .line 314
    :sswitch_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0

    .line 324
    :sswitch_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 312
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x18 -> :sswitch_1
        0x19 -> :sswitch_1
        0x1e -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 3
    .param p1, "cardMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;-><init>(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;IIZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    return-object p0
.end method

.method public addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 3
    .param p1, "cardMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;-><init>(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;IIZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-object p0
.end method

.method public getCardMetadataForMinCellHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mCardMetadataForMinCellHeight:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mHeight:I

    return v0
.end method

.method public getLeadingGap()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mLeadingGap:I

    return v0
.end method

.method public getTileCount()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    return-object v0
.end method

.method public getTrailingGap()I
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x0

    return v0
.end method

.method public getViewportWidth()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mViewportWidth:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mWidth:I

    return v0
.end method

.method public setAlignToParentEndIfNecessary()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mAlignToParentEndIfNecessary:Z

    .line 250
    return-object p0
.end method

.method public setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 0
    .param p1, "cardMetadataForMinCellHeight"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mCardMetadataForMinCellHeight:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 240
    return-object p0
.end method

.method public setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mRespectChildHeight:Z

    .line 229
    return-object p0
.end method

.method public setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mRespectChildThumbnailAspectRatio:Z

    .line 234
    return-object p0
.end method

.method public shouldAlignToParentEndIfNecessary()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mAlignToParentEndIfNecessary:Z

    return v0
.end method

.method public shouldRespectChildHeight()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mRespectChildHeight:Z

    return v0
.end method

.method public shouldRespectChildThumbnailAspectRatio(I)Z
    .locals 1
    .param p1, "tileIndex"    # I

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mRespectChildThumbnailAspectRatio:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mTiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mRespectChildThumbnailAspectRatio:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->access$000(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public withLeadingGap(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 0
    .param p1, "leadingGap"    # I

    .prologue
    .line 244
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mLeadingGap:I

    .line 245
    return-object p0
.end method

.method public withViewportWidth(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 0
    .param p1, "viewportWidth"    # I

    .prologue
    .line 258
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->mViewportWidth:I

    .line 259
    return-object p0
.end method
