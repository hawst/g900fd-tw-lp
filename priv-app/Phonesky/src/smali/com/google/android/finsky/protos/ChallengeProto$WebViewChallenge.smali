.class public final Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WebViewChallenge"
.end annotation


# instance fields
.field public cancelButtonDisplayLabel:Ljava/lang/String;

.field public cancelUrlRegexp:Ljava/lang/String;

.field public hasCancelButtonDisplayLabel:Z

.field public hasCancelUrlRegexp:Z

.field public hasResponseTargetUrlParam:Z

.field public hasStartUrl:Z

.field public hasTargetUrlRegexp:Z

.field public hasTitle:Z

.field public responseTargetUrlParam:Ljava/lang/String;

.field public startUrl:Ljava/lang/String;

.field public targetUrlRegexp:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1434
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    .line 1435
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1438
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    .line 1439
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasStartUrl:Z

    .line 1440
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    .line 1441
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTargetUrlRegexp:Z

    .line 1442
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    .line 1443
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelUrlRegexp:Z

    .line 1444
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    .line 1445
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTitle:Z

    .line 1446
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    .line 1447
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelButtonDisplayLabel:Z

    .line 1448
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    .line 1449
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasResponseTargetUrlParam:Z

    .line 1450
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cachedSize:I

    .line 1451
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1480
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1481
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasStartUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1482
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1485
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTargetUrlRegexp:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1486
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1489
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelButtonDisplayLabel:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1490
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1493
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasResponseTargetUrlParam:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1494
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1497
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelUrlRegexp:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1498
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1501
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTitle:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1502
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1505
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1514
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1518
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1519
    :sswitch_0
    return-object p0

    .line 1524
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    .line 1525
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasStartUrl:Z

    goto :goto_0

    .line 1529
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    .line 1530
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTargetUrlRegexp:Z

    goto :goto_0

    .line 1534
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    .line 1535
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelButtonDisplayLabel:Z

    goto :goto_0

    .line 1539
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    .line 1540
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasResponseTargetUrlParam:Z

    goto :goto_0

    .line 1544
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    .line 1545
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelUrlRegexp:Z

    goto :goto_0

    .line 1549
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    .line 1550
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTitle:Z

    goto :goto_0

    .line 1514
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1392
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1457
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasStartUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1458
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1460
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTargetUrlRegexp:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1461
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->targetUrlRegexp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1463
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelButtonDisplayLabel:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1464
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1466
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasResponseTargetUrlParam:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1467
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1469
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelUrlRegexp:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1470
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelUrlRegexp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1472
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTitle:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1473
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1475
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1476
    return-void
.end method
