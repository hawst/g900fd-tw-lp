.class public Lcom/google/android/finsky/library/LibraryInAppEntry;
.super Lcom/google/android/finsky/library/LibraryEntry;
.source "LibraryInAppEntry.java"


# instance fields
.field public final signature:Ljava/lang/String;

.field public final signedPurchaseData:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V
    .locals 15
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p3, "docId"    # Ljava/lang/String;
    .param p4, "offerType"    # I
    .param p5, "signedPurchaseData"    # Ljava/lang/String;
    .param p6, "signature"    # Ljava/lang/String;
    .param p7, "documentHash"    # J

    .prologue
    .line 19
    const/4 v6, 0x3

    const/16 v8, 0xb

    const-wide v12, 0x7fffffffffffffffL

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v7, p3

    move/from16 v9, p4

    move-wide/from16 v10, p7

    invoke-direct/range {v3 .. v13}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIJJ)V

    .line 22
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryInAppEntry;->signedPurchaseData:Ljava/lang/String;

    .line 23
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryInAppEntry;->signature:Ljava/lang/String;

    .line 24
    return-void
.end method
