.class public Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;
.super Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.source "DiscoveryBadgeDowloadCount.java"


# instance fields
.field private mDownloadCountNumber:Landroid/widget/TextView;

.field private mDownloadMagnitude:Landroid/widget/TextView;

.field private mDownloadMagnitudeBackground:Landroid/graphics/drawable/GradientDrawable;

.field private mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

.field private mFocusedOutlineColor:I

.field private mOutlineStrokeWidth:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPressedFillColor:I

.field private mPressedOutlineColor:I

.field private mRingStrokeWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    .line 60
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeBackground:Landroid/graphics/drawable/GradientDrawable;

    .line 63
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    .line 65
    const v1, 0x7f0b017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mRingStrokeWidth:I

    .line 68
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x7f0b0078

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mOutlineStrokeWidth:F

    .line 69
    const v1, 0x7f09007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPressedFillColor:I

    .line 70
    const v1, 0x7f090080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPressedOutlineColor:I

    .line 71
    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mFocusedOutlineColor:I

    .line 72
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->setWillNotDraw(Z)V

    .line 73
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "badge"    # Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 95
    invoke-super/range {p0 .. p7}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 99
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    .line 100
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadCountNumber:Landroid/widget/TextView;

    const v3, 0x7f0b01ae

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 108
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadCountNumber:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadCount:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitude:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 125
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeBackground:Landroid/graphics/drawable/GradientDrawable;

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mCurrentFillColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 126
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_3

    .line 127
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 133
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mTitle:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadCountNumber:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitude:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 138
    return-void

    .line 104
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadCountNumber:Landroid/widget/TextView;

    const v3, 0x7f0b01af

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 115
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 116
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 118
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->downloadUnits:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 119
    const/4 v2, 0x7

    const v3, 0x7f0a0191

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 123
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 121
    :cond_2
    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_3

    .line 129
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->getWidth()I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    .line 162
    .local v1, "x":I
    iget v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    .line 164
    .local v2, "y":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->isDuplicateParentStateEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 165
    .local v0, "drawPressed":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 168
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPressedFillColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 169
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 170
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 173
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPressedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 174
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 175
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mOutlineStrokeWidth:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 176
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 187
    :cond_1
    :goto_1
    return-void

    .line 164
    .end local v0    # "drawPressed":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 180
    .restart local v0    # "drawPressed":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 182
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mFocusedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 184
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mOutlineStrokeWidth:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 185
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->drawableStateChanged()V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->invalidate()V

    .line 144
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 190
    const/16 v0, 0x70d

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->getWidth()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    .line 149
    .local v0, "x":I
    iget v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    .line 152
    .local v1, "y":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mCurrentFillColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 153
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mRingStrokeWidth:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 155
    int-to-float v2, v0

    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mBadgeRadius:I

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mRingStrokeWidth:I

    mul-int/lit8 v5, v5, 0x2

    div-int/lit8 v5, v5, 0x3

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 156
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onFinishInflate()V

    .line 79
    const v0, 0x7f0a01a7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadCountNumber:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0a01a9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitude:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0a01a8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;->mDownloadMagnitudeContainer:Landroid/widget/RelativeLayout;

    .line 83
    return-void
.end method
