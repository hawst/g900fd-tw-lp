.class final Lcom/google/android/finsky/utils/AppSupport$2;
.super Ljava/lang/Object;
.source "AppSupport.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fragmentManager:Landroid/support/v4/app/FragmentManager;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/utils/AppSupport$RefundListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$fragmentManager:Landroid/support/v4/app/FragmentManager;

    iput-object p2, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$fragmentManager:Landroid/support/v4/app/FragmentManager;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-static {v0}, Lcom/google/android/finsky/utils/AppSupport;->showRefundFailureDialog(Landroid/support/v4/app/FragmentManager;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$2;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/AppSupport$RefundListener;->onRefundComplete(Z)V

    .line 67
    :cond_1
    return-void
.end method
