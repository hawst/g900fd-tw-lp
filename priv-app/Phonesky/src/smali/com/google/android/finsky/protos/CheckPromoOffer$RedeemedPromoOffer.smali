.class public final Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CheckPromoOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CheckPromoOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedeemedPromoOffer"
.end annotation


# instance fields
.field public descriptionHtml:Ljava/lang/String;

.field public hasDescriptionHtml:Z

.field public hasHeaderText:Z

.field public headerText:Ljava/lang/String;

.field public image:Lcom/google/android/finsky/protos/Common$Image;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 410
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->clear()Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 411
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 414
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    .line 415
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasHeaderText:Z

    .line 416
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    .line 417
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasDescriptionHtml:Z

    .line 418
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 419
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->cachedSize:I

    .line 420
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 440
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 441
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasHeaderText:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 442
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 446
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_4

    .line 450
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 461
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 462
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 466
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 467
    :sswitch_0
    return-object p0

    .line 472
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    .line 473
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasHeaderText:Z

    goto :goto_0

    .line 477
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    .line 478
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasDescriptionHtml:Z

    goto :goto_0

    .line 482
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 483
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 485
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 462
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasHeaderText:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 427
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->headerText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 429
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 430
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 432
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_4

    .line 433
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 435
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 436
    return-void
.end method
