.class Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;
.super Ljava/lang/Object;
.source "DetailsScrollView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->getScrollY()I

    move-result v0

    .line 99
    .local v0, "currScrollY":I
    if-lez v0, :cond_0

    neg-int v6, v0

    .line 101
    .local v6, "scrollBy":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x12c

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getVerticalScrollByAnimation(Landroid/widget/ScrollView;JJILandroid/view/animation/Animation$AnimationListener;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v8

    .line 103
    .local v8, "scrollHero":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v1, v8}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 104
    return-void

    .line 99
    .end local v6    # "scrollBy":I
    .end local v8    # "scrollHero":Landroid/view/animation/Animation;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$100(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/HeroGraphicView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDefaultHeroImageHeight:I
    invoke-static {v2}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$000(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I

    move-result v2

    sub-int v6, v1, v2

    goto :goto_0
.end method
