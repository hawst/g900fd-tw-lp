.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreImpressionEvent"
.end annotation


# instance fields
.field public hasId:Z

.field public id:J

.field public referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field public tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1287
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1288
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    .line 1289
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 2

    .prologue
    .line 1292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 1293
    invoke-static {}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->emptyArray()[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 1294
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    .line 1295
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    .line 1296
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    .line 1297
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 1322
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1323
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-eqz v3, :cond_0

    .line 1324
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1327
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 1328
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 1329
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    aget-object v0, v3, v1

    .line 1330
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v0, :cond_1

    .line 1331
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1328
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1336
    .end local v0    # "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .end local v1    # "i":I
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 1337
    :cond_3
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1340
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1349
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1353
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1354
    :sswitch_0
    return-object p0

    .line 1359
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-nez v5, :cond_1

    .line 1360
    new-instance v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v5}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 1362
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1366
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1368
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-nez v5, :cond_3

    move v1, v4

    .line 1369
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 1371
    .local v2, "newArray":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v1, :cond_2

    .line 1372
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1374
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 1375
    new-instance v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v5}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    aput-object v5, v2, v1

    .line 1376
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1377
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1374
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1368
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v1, v5

    goto :goto_1

    .line 1380
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v5}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    aput-object v5, v2, v1

    .line 1381
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1382
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    .line 1386
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    .line 1387
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    goto :goto_0

    .line 1349
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1260
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1303
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-eqz v2, :cond_0

    .line 1304
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->tree:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1306
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1307
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 1308
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath:[Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    aget-object v0, v2, v1

    .line 1309
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    if-eqz v0, :cond_1

    .line 1310
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1307
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1314
    .end local v0    # "element":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .end local v1    # "i":I
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1315
    :cond_3
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;->id:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1317
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1318
    return-void
.end method
