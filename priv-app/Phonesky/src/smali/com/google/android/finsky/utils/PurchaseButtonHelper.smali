.class public Lcom/google/android/finsky/utils/PurchaseButtonHelper;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;,
        Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;,
        Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    }
.end annotation


# direct methods
.method static synthetic access$000(Lcom/google/android/finsky/protos/Common$Offer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v0

    return v0
.end method

.method private static addOfferActions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V
    .locals 19
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "listingOffer"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p2, "currentAccount"    # Landroid/accounts/Account;
    .param p3, "accountLibrary"    # Lcom/google/android/finsky/library/Library;
    .param p4, "outDocumentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .prologue
    .line 565
    const/4 v4, 0x4

    move-object/from16 v0, p4

    iput v4, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 566
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferText:Ljava/lang/String;

    .line 567
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 568
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferFullText:Ljava/lang/String;

    .line 571
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v6

    .line 576
    .local v6, "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static {v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getSampleOffer([Lcom/google/android/finsky/protos/Common$Offer;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v18

    .line 577
    .local v18, "sampleOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v18, :cond_2

    const/4 v12, 0x1

    .line 578
    .local v12, "hasSampleOffer":Z
    :goto_0
    if-eqz v12, :cond_3

    const/4 v4, 0x1

    :goto_1
    rsub-int/lit8 v11, v4, 0x2

    .line 580
    .local v11, "availableButtons":I
    array-length v4, v6

    const/4 v5, 0x2

    if-gt v4, v5, :cond_9

    .line 583
    move-object v10, v6

    .local v10, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v14, v10

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_2
    if-ge v13, v14, :cond_a

    aget-object v15, v10, v13

    .line 584
    .local v15, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v0, v15, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    move/from16 v16, v0

    .line 585
    .local v16, "offerType":I
    const/4 v4, 0x2

    move/from16 v0, v16

    if-ne v0, v4, :cond_4

    .line 583
    :cond_1
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 577
    .end local v10    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v11    # "availableButtons":I
    .end local v12    # "hasSampleOffer":Z
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v16    # "offerType":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_0

    .line 578
    .restart local v12    # "hasSampleOffer":Z
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 587
    .restart local v10    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v11    # "availableButtons":I
    .restart local v13    # "i$":I
    .restart local v14    # "len$":I
    .restart local v15    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v16    # "offerType":I
    :cond_4
    const/16 v4, 0xb

    move/from16 v0, v16

    if-eq v0, v4, :cond_1

    .line 593
    invoke-static {v15}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 594
    const/4 v4, 0x5

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v4, v15, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_3

    .line 596
    :cond_5
    sget-object v4, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->RENTAL:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 597
    const/4 v4, 0x3

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v4, v15, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_3

    .line 599
    :cond_6
    sget-object v4, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->PURCHASE:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 600
    iget-wide v4, v15, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_7

    .line 601
    const/4 v4, 0x7

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v4, v15, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_3

    .line 604
    :cond_7
    const/4 v4, 0x1

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-virtual {v0, v4, v15, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_3

    .line 609
    :cond_8
    const-string v4, "Don\'t know how to show action for offer type %d on document %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    aput-object p0, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 613
    .end local v10    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v16    # "offerType":I
    :cond_9
    const/4 v4, 0x2

    if-lt v11, v4, :cond_c

    .line 616
    const/16 v17, 0x0

    .line 617
    .local v17, "offersHandled":I
    const/4 v5, 0x1

    sget-object v7, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->PURCHASE:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-object/from16 v4, p4

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addActionFromOfferList(I[Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)I

    move-result v4

    add-int v17, v17, v4

    .line 619
    const/4 v5, 0x3

    sget-object v7, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->RENTAL:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-object/from16 v4, p4

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addActionFromOfferList(I[Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)I

    move-result v4

    add-int v17, v17, v4

    .line 626
    array-length v4, v6

    move/from16 v0, v17

    if-eq v0, v4, :cond_a

    .line 627
    const-string v4, "Could only handle %d of %d offers"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    array-length v8, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 638
    .end local v17    # "offersHandled":I
    :cond_a
    :goto_4
    if-eqz v12, :cond_b

    .line 639
    const/4 v4, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 640
    const/16 v4, 0xa

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 647
    :cond_b
    :goto_5
    return-void

    .line 629
    :cond_c
    const/4 v4, 0x1

    if-ne v11, v4, :cond_d

    .line 632
    const/16 v4, 0xd

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addDisambiguationAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_4

    .line 635
    :cond_d
    const-string v4, "We ran out of buttons without displaying any offers?"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 643
    :cond_e
    const/16 v4, 0xb

    move-object/from16 v0, p4

    move-object/from16 v1, v18

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_5
.end method

.method public static canCreateClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;)Z
    .locals 2
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    .prologue
    .line 878
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getActionClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/content/Context;)Landroid/view/View$OnClickListener;
    .locals 8
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p1, "backend"    # I
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "clickNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x4

    .line 887
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    packed-switch v0, :pswitch_data_0

    .line 940
    :pswitch_0
    const-string v0, "Unrecognized action %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 941
    const/4 v0, 0x0

    .line 943
    :goto_0
    return-object v0

    .line 890
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    invoke-virtual {p2, v0, v1, p4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_0

    .line 893
    :pswitch_2
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;

    invoke-direct {v0, p0, p5}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;-><init>(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;Landroid/content/Context;)V

    goto :goto_0

    .line 904
    :pswitch_3
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 905
    const/16 v6, 0xe8

    .line 943
    .local v6, "elementType":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    iget-object v4, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-object v0, p2

    move-object v5, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_0

    .line 906
    .end local v6    # "elementType":I
    :cond_0
    if-ne p1, v2, :cond_1

    .line 907
    const/16 v6, 0xe7

    .restart local v6    # "elementType":I
    goto :goto_1

    .line 909
    .end local v6    # "elementType":I
    :cond_1
    const/16 v6, 0xc8

    .line 911
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 913
    .end local v6    # "elementType":I
    :pswitch_4
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-ne v0, v2, :cond_2

    .line 914
    const/16 v6, 0xe5

    .restart local v6    # "elementType":I
    goto :goto_1

    .line 915
    .end local v6    # "elementType":I
    :cond_2
    if-ne p1, v2, :cond_3

    .line 916
    const/16 v6, 0xe4

    .restart local v6    # "elementType":I
    goto :goto_1

    .line 918
    .end local v6    # "elementType":I
    :cond_3
    const/16 v6, 0xc8

    .line 920
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 922
    .end local v6    # "elementType":I
    :pswitch_5
    const/16 v6, 0xc8

    .line 923
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 925
    .end local v6    # "elementType":I
    :pswitch_6
    const/16 v6, 0xc8

    .line 926
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 928
    .end local v6    # "elementType":I
    :pswitch_7
    const/16 v6, 0xe2

    .line 929
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 931
    .end local v6    # "elementType":I
    :pswitch_8
    const/16 v6, 0xdd

    .line 932
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 934
    .end local v6    # "elementType":I
    :pswitch_9
    const/16 v6, 0xd9

    .line 935
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 937
    .end local v6    # "elementType":I
    :pswitch_a
    const/16 v6, 0xde

    .line 938
    .restart local v6    # "elementType":I
    goto :goto_1

    .line 887
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_7
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_a
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static getActionStatus(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 4
    .param p0, "documentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;
    .param p1, "outActionStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    .line 733
    invoke-virtual {p1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->reset()V

    .line 734
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    packed-switch v0, :pswitch_data_0

    .line 748
    :pswitch_0
    const-string v0, "Expected to have an available action with status %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 751
    :goto_0
    return-void

    .line 736
    :pswitch_1
    const v0, 0x7f0c029d

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 739
    :pswitch_2
    const v0, 0x7f0c029e

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 742
    :pswitch_3
    const v0, 0x7f0c01c9

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 745
    :pswitch_4
    const v0, 0x7f0c01cb

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 734
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getActionStyle(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 2
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p1, "backend"    # I
    .param p2, "outActionStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    .line 708
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleForFormat(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;IZZLcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 709
    return-void
.end method

.method private static getActionStyleForFormat(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;IZZLcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 4
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p1, "backend"    # I
    .param p2, "longFormat"    # Z
    .param p3, "useBuyPrefix"    # Z
    .param p4, "outActionStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    const/4 v2, 0x7

    const/4 v1, 0x4

    .line 759
    invoke-virtual {p4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->reset()V

    .line 760
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    packed-switch v0, :pswitch_data_0

    .line 866
    const-string v0, "Unrecognized action %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 868
    :goto_0
    return-void

    .line 762
    :pswitch_0
    const v0, 0x7f0c0204

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 765
    :pswitch_1
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-nez v0, :cond_0

    .line 767
    const v0, 0x7f0c0200

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    .line 781
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    .line 782
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    goto :goto_0

    .line 768
    :cond_0
    if-ne p1, v1, :cond_2

    .line 769
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-ne v0, v2, :cond_1

    .line 770
    const v0, 0x7f0c01ff

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_1

    .line 772
    :cond_1
    const v0, 0x7f0c01fe

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_1

    .line 774
    :cond_2
    if-eqz p3, :cond_3

    .line 775
    const v0, 0x7f0c01f9

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_1

    .line 779
    :cond_3
    const/4 v0, -0x1

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_1

    .line 785
    :pswitch_2
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-nez v0, :cond_4

    .line 787
    const v0, 0x7f0c01fd

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    .line 797
    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    .line 798
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    goto :goto_0

    .line 788
    :cond_4
    if-ne p1, v1, :cond_6

    .line 789
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-ne v0, v1, :cond_5

    .line 790
    const v0, 0x7f0c01fb

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_2

    .line 792
    :cond_5
    const v0, 0x7f0c01fc

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_2

    .line 795
    :cond_6
    const v0, 0x7f0c01fa

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_2

    .line 801
    :pswitch_3
    const v0, 0x7f0c0201

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    .line 802
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    .line 803
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    goto :goto_0

    .line 806
    :pswitch_4
    if-ne p1, v1, :cond_8

    .line 807
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    if-ne v0, v2, :cond_7

    .line 808
    const v0, 0x7f0c0207

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    .line 815
    :goto_3
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    .line 816
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    goto/16 :goto_0

    .line 810
    :cond_7
    const v0, 0x7f0c0206

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_3

    .line 813
    :cond_8
    const v0, 0x7f0c0205

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_3

    .line 819
    :pswitch_5
    if-eqz p2, :cond_9

    const v0, 0x7f0c0203

    :goto_4
    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    .line 821
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    .line 822
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    goto/16 :goto_0

    .line 819
    :cond_9
    const v0, 0x7f0c0202

    goto :goto_4

    .line 825
    :pswitch_6
    packed-switch p1, :pswitch_data_1

    .line 837
    :pswitch_7
    const v0, 0x7f0c020b

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 828
    :pswitch_8
    const v0, 0x7f0c01e5

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 831
    :pswitch_9
    const v0, 0x7f0c020e

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 834
    :pswitch_a
    const v0, 0x7f0c01e4

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 841
    :pswitch_b
    const/4 v0, 0x3

    if-ne p1, v0, :cond_a

    .line 842
    const v0, 0x7f0c01e6

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 843
    :cond_a
    const/4 v0, 0x6

    if-ne p1, v0, :cond_b

    .line 844
    const v0, 0x7f0c0395

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 846
    :cond_b
    const v0, 0x7f0c035d

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 850
    :pswitch_c
    const v0, 0x7f0c01cd

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 853
    :pswitch_d
    const v0, 0x7f0c0228

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 857
    :pswitch_e
    const v0, 0x7f0c0209

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 860
    :pswitch_f
    const v0, 0x7f0c020a

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 863
    :pswitch_10
    const v0, 0x7f0c020f

    iput v0, p4, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto/16 :goto_0

    .line 760
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_3
        :pswitch_10
    .end packed-switch

    .line 825
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getActionStyleLong(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p1, "backend"    # I
    .param p2, "outActionStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    const/4 v0, 0x1

    .line 717
    invoke-static {p0, p1, v0, v0, p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleForFormat(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;IZZLcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 718
    return-void
.end method

.method public static getActionStyleWithoutBuyPrefix(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 1
    .param p0, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p1, "backend"    # I
    .param p2, "outActionStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    const/4 v0, 0x0

    .line 725
    invoke-static {p0, p1, v0, v0, p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleForFormat(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;IZZLcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 726
    return-void
.end method

.method private static getActionsForApp(Landroid/accounts/Account;Landroid/accounts/Account;ZLcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V
    .locals 5
    .param p0, "currentAccount"    # Landroid/accounts/Account;
    .param p1, "owner"    # Landroid/accounts/Account;
    .param p2, "ownedByUser"    # Z
    .param p3, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p4, "accountLibrary"    # Lcom/google/android/finsky/library/Library;
    .param p5, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p6, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p7, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p8, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p9, "listingOffer"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p10, "outDocumentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .prologue
    .line 417
    const/4 v3, 0x0

    iput-boolean v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 419
    if-nez p9, :cond_1

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    if-eqz p1, :cond_2

    move-object v0, p1

    .line 426
    .local v0, "accountToUse":Landroid/accounts/Account;
    :goto_1
    invoke-virtual {p8}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 427
    invoke-virtual {p8}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    .line 428
    .local v2, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    new-instance v1, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    iget-object v3, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-direct {v1, v3, p6, p5}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 430
    .local v1, "appActions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    iget-boolean v3, v1, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v3, :cond_6

    .line 431
    iget-boolean v3, v1, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-eqz v3, :cond_3

    .line 432
    const/4 v3, 0x2

    iput v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 433
    const/16 v3, 0xe

    invoke-virtual {p10, v3, p8, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 451
    :goto_2
    const/4 v3, 0x1

    iput-boolean v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    goto :goto_0

    .end local v0    # "accountToUse":Landroid/accounts/Account;
    .end local v1    # "appActions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    .end local v2    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_2
    move-object v0, p0

    .line 424
    goto :goto_1

    .line 434
    .restart local v0    # "accountToUse":Landroid/accounts/Account;
    .restart local v1    # "appActions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    .restart local v2    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_3
    invoke-virtual {v1, p8}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->hasUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 436
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {p3, v3}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v3

    sget-object v4, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v3, v4, :cond_4

    .line 438
    const/16 v3, 0x9

    iput v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_2

    .line 443
    :cond_4
    const/16 v3, 0x8

    invoke-virtual {p10, v3, p9, p8, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 445
    const/4 v3, 0x5

    iput v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_2

    .line 448
    :cond_5
    const/4 v3, 0x6

    invoke-virtual {p10, v3, p8, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_2

    .line 453
    :cond_6
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {p3, v3}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v3

    sget-object v4, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v3, v4, :cond_7

    .line 455
    const/4 v3, 0x1

    iput v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_0

    .line 459
    .end local v1    # "appActions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    .end local v2    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_7
    invoke-static {p8, p7, p4}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    if-eqz p2, :cond_8

    iget-boolean v3, p9, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v3, :cond_8

    .line 463
    const/4 v3, 0x1

    iput-boolean v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 464
    const/4 v3, 0x7

    invoke-virtual {p10, v3, p9, p8, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 465
    const/4 v3, 0x6

    iput v3, p10, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_0

    .line 466
    :cond_8
    invoke-static {p8, p7, p4}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 467
    invoke-static {p8, p9, v0, p4, p10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->addOfferActions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    goto :goto_0
.end method

.method private static getActionsForMagazine(Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V
    .locals 8
    .param p0, "currentAccount"    # Landroid/accounts/Account;
    .param p1, "accountLibrary"    # Lcom/google/android/finsky/library/Library;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "listingOffer"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p6, "outDocumentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .prologue
    .line 484
    if-eqz p5, :cond_0

    .line 485
    iget-object v4, p5, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    iput-object v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferText:Ljava/lang/String;

    .line 486
    invoke-static {p5}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 487
    iget-object v4, p5, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    iput-object v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferFullText:Ljava/lang/String;

    .line 493
    :cond_0
    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v5, 0x11

    if-eq v4, v5, :cond_1

    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v5, 0x19

    if-ne v4, v5, :cond_8

    :cond_1
    move-object v1, p4

    .line 496
    .local v1, "issueDoc":Lcom/google/android/finsky/api/model/Document;
    :goto_0
    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v5, 0x10

    if-eq v4, v5, :cond_2

    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v5, 0x18

    if-ne v4, v5, :cond_3

    :cond_2
    invoke-static {p4}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineCurrentIssueDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 500
    :cond_3
    const/4 v3, 0x0

    .line 501
    .local v3, "issueOwned":Z
    if-eqz v1, :cond_4

    .line 502
    invoke-static {v1, p2, p0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 505
    const/4 v3, 0x1

    .line 506
    const/4 v4, 0x1

    iput-boolean v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 507
    const/4 v4, 0x6

    invoke-virtual {p6, v4, v1, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 512
    :cond_4
    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 513
    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v4

    invoke-static {v4, p2, p0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 517
    if-nez v1, :cond_5

    .line 518
    const/4 v4, 0x1

    iput-boolean v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 519
    const/4 v4, 0x6

    invoke-virtual {p6, v4, p4, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 523
    :cond_5
    const/16 v4, 0x8

    iput v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 531
    :cond_6
    :goto_1
    if-nez v3, :cond_7

    if-eqz v1, :cond_7

    invoke-static {v1, p3, p1}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 533
    invoke-static {v1, p3, p1}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineIssueOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v2

    .line 534
    .local v2, "issueOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_7

    .line 535
    iget v4, v2, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a

    .line 536
    const/4 v4, 0x6

    invoke-virtual {p6, v4, v1, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 544
    :goto_2
    invoke-virtual {p6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasStatus()Z

    move-result v4

    if-nez v4, :cond_7

    .line 545
    const/4 v4, 0x4

    iput v4, p6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 549
    .end local v2    # "issueOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_7
    return-void

    .line 493
    .end local v1    # "issueDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "issueOwned":Z
    :cond_8
    const/4 v1, 0x0

    goto :goto_0

    .line 524
    .restart local v1    # "issueDoc":Lcom/google/android/finsky/api/model/Document;
    .restart local v3    # "issueOwned":Z
    :cond_9
    invoke-static {p4, p3, p1}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 525
    const/4 v4, 0x2

    invoke-virtual {p6, v4, p5, p4, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addDisambiguationAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_1

    .line 537
    .restart local v2    # "issueOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_a
    iget-boolean v4, v2, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v4, :cond_c

    .line 538
    iget-wide v4, v2, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_b

    const/4 v0, 0x4

    .line 539
    .local v0, "actionType":I
    :goto_3
    invoke-virtual {p6, v0, v2, v1, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_2

    .line 538
    .end local v0    # "actionType":I
    :cond_b
    const/4 v0, 0x7

    goto :goto_3

    .line 542
    :cond_c
    const/4 v4, 0x6

    invoke-virtual {p6, v4, v1, p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_2
.end method

.method public static getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V
    .locals 21
    .param p0, "currentAccount"    # Landroid/accounts/Account;
    .param p1, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p6, "outDocumentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .prologue
    .line 340
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->reset()V

    .line 341
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    move-object/from16 v0, p6

    iput v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    .line 343
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v7

    .line 344
    .local v7, "library":Lcom/google/android/finsky/library/Library;
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/utils/DocUtils;->getListingOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v12

    .line 345
    .local v12, "listingOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v12, :cond_1

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    .line 352
    .local v4, "owner":Landroid/accounts/Account;
    if-eqz v4, :cond_2

    const/4 v5, 0x1

    .line 353
    .local v5, "ownedByUser":Z
    :goto_1
    if-eqz v5, :cond_3

    invoke-static {v12}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v20

    .line 355
    .local v20, "isPreordered":Z
    :goto_2
    move-object/from16 v0, p6

    iput-boolean v5, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 357
    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    packed-switch v3, :pswitch_data_0

    .line 399
    :pswitch_0
    const-string v3, "Unsupported backend: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v3, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 401
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    move-object/from16 v2, p6

    invoke-static {v0, v12, v1, v7, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->addOfferActions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    goto :goto_0

    .line 352
    .end local v5    # "ownedByUser":Z
    .end local v20    # "isPreordered":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 353
    .restart local v5    # "ownedByUser":Z
    :cond_3
    const/16 v20, 0x0

    goto :goto_2

    .restart local v20    # "isPreordered":Z
    :pswitch_1
    move-object/from16 v3, p0

    move-object/from16 v6, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v13, p6

    .line 359
    invoke-static/range {v3 .. v13}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionsForApp(Landroid/accounts/Account;Landroid/accounts/Account;ZLcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    goto :goto_0

    :pswitch_2
    move-object/from16 v13, p0

    move-object v14, v7

    move-object/from16 v15, p2

    move-object/from16 v16, p4

    move-object/from16 v17, p5

    move-object/from16 v18, v12

    move-object/from16 v19, p6

    .line 364
    invoke-static/range {v13 .. v19}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionsForMagazine(Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    goto :goto_0

    .line 370
    :pswitch_3
    if-eqz v5, :cond_7

    .line 371
    if-eqz v20, :cond_4

    .line 372
    const/16 v3, 0x9

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-virtual {v0, v3, v1, v4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 373
    const/4 v3, 0x3

    move-object/from16 v0, p6

    iput v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_0

    .line 375
    :cond_4
    const/4 v3, 0x6

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-virtual {v0, v3, v1, v4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 376
    const/4 v3, 0x3

    move-object/from16 v0, p5

    invoke-static {v0, v7, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x4

    move-object/from16 v0, p5

    invoke-static {v0, v7, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 379
    :cond_5
    const/4 v3, 0x7

    move-object/from16 v0, p6

    iput v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 386
    :goto_3
    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    const/4 v6, 0x4

    if-ne v3, v6, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/utils/PlayUtils;->isTv(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 388
    const/16 v3, 0xc

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    move-object/from16 v2, p0

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto/16 :goto_0

    .line 381
    :cond_6
    const/4 v3, 0x6

    move-object/from16 v0, p6

    iput v3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    goto :goto_3

    .line 392
    :cond_7
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 393
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    move-object/from16 v2, p6

    invoke-static {v0, v12, v1, v7, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->addOfferActions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Landroid/accounts/Account;Lcom/google/android/finsky/library/Library;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    goto/16 :goto_0

    .line 357
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getListingStyle(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V
    .locals 4
    .param p0, "documentActions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;
    .param p1, "outListingStyle"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    .prologue
    .line 662
    invoke-virtual {p1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->reset()V

    .line 663
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 664
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    packed-switch v0, :pswitch_data_0

    .line 694
    const-string v0, "Unrecognized status %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 666
    :pswitch_0
    const v0, 0x7f0c029d

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 669
    :pswitch_1
    const v0, 0x7f0c029e

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 672
    :pswitch_2
    const v0, 0x7f0c01c9

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 675
    :pswitch_3
    const v0, 0x7f0c01cb

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 678
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferFullText:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerFullText:Ljava/lang/String;

    .line 679
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferText:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->offerText:Ljava/lang/String;

    goto :goto_0

    .line 682
    :pswitch_5
    const v0, 0x7f0c01cd

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 685
    :pswitch_6
    const v0, 0x7f0c01ca

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 688
    :pswitch_7
    const v0, 0x7f0c01cc

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 691
    :pswitch_8
    const v0, 0x7f0c01ce

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 696
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    if-eqz v0, :cond_0

    .line 697
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 698
    const v0, 0x7f0c01c8

    iput v0, p1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->resourceId:I

    goto :goto_0

    .line 664
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
    .end packed-switch
.end method

.method private static getSampleOffer([Lcom/google/android/finsky/protos/Common$Offer;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 6
    .param p0, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 650
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 651
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v4, v3, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 655
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_1
    return-object v3

    .line 650
    .restart local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 655
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z
    .locals 4
    .param p0, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
