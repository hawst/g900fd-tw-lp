.class final Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;
.super Landroid/os/AsyncTask;
.source "GaiaRecoveryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->prefetchAndCacheGaiaAuthRecoveryIntent(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Context;",
        "Ljava/lang/Void;",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 6
    .param p1, "arg0"    # [Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 71
    const/4 v2, 0x0

    .line 73
    .local v2, "recoveryIntent":Landroid/app/PendingIntent;
    const/4 v4, 0x0

    :try_start_0
    aget-object v1, p1, v4

    .line 75
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 76
    .local v0, "code":I
    if-eqz v0, :cond_0

    .line 77
    const-string v4, "GooglePlayServices is not available."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    .end local v0    # "code":I
    .end local v1    # "context":Landroid/content/Context;
    :goto_0
    return-object v3

    .line 81
    .restart local v0    # "code":I
    .restart local v1    # "context":Landroid/content/Context;
    :cond_0
    # getter for: Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->access$000()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/gms/auth/GoogleAuthUtil;->getRecoveryIfSuggested(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .end local v0    # "code":I
    .end local v1    # "context":Landroid/content/Context;
    :goto_1
    move-object v3, v2

    .line 87
    goto :goto_0

    .line 83
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 68
    check-cast p1, [Landroid/content/Context;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;->doInBackground([Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/app/PendingIntent;)V
    .locals 0
    .param p1, "result"    # Landroid/app/PendingIntent;

    .prologue
    .line 92
    # setter for: Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;
    invoke-static {p1}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->access$102(Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    .line 93
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 68
    check-cast p1, Landroid/app/PendingIntent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;->onPostExecute(Landroid/app/PendingIntent;)V

    return-void
.end method
