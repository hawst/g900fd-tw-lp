.class public Lcom/google/android/finsky/utils/PlayCardCustomizer;
.super Ljava/lang/Object;
.source "PlayCardCustomizer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/play/layout/PlayCardViewBase;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    .local p0, "this":Lcom/google/android/finsky/utils/PlayCardCustomizer;, "Lcom/google/android/finsky/utils/PlayCardCustomizer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public preBind(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;)V
    .locals 1
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/finsky/api/model/Document;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/finsky/utils/PlayCardCustomizer;, "Lcom/google/android/finsky/utils/PlayCardCustomizer<TT;>;"
    .local p1, "card":Lcom/google/android/play/layout/PlayCardViewBase;, "TT;"
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->setData(Ljava/lang/Object;I)V

    .line 25
    return-void
.end method
