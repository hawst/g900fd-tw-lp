.class public final Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StatusBarNotificationProto"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;


# instance fields
.field public contentText:Ljava/lang/String;

.field public contentTitle:Ljava/lang/String;

.field public hasContentText:Z

.field public hasContentTitle:Z

.field public hasSeverity:Z

.field public hasTickerText:Z

.field public severity:I

.field public tickerText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3993
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3994
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    .line 3995
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    .locals 2

    .prologue
    .line 3966
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    if-nez v0, :cond_1

    .line 3967
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3969
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    if-nez v0, :cond_0

    .line 3970
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    .line 3972
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3974
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    return-object v0

    .line 3972
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3998
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    .line 3999
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasTickerText:Z

    .line 4000
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    .line 4001
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentTitle:Z

    .line 4002
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    .line 4003
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentText:Z

    .line 4004
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    .line 4005
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasSeverity:Z

    .line 4006
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->cachedSize:I

    .line 4007
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4030
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4031
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasTickerText:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4032
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4035
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentTitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4036
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4039
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentText:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4040
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4043
    :cond_5
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasSeverity:Z

    if-eqz v1, :cond_7

    .line 4044
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4047
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 4055
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4056
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4060
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4061
    :sswitch_0
    return-object p0

    .line 4066
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    .line 4067
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasTickerText:Z

    goto :goto_0

    .line 4071
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    .line 4072
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentTitle:Z

    goto :goto_0

    .line 4076
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    .line 4077
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentText:Z

    goto :goto_0

    .line 4081
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4082
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 4085
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    .line 4086
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasSeverity:Z

    goto :goto_0

    .line 4056
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 4082
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3956
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4013
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasTickerText:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4014
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4016
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentTitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4017
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4019
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasContentText:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 4020
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4022
    :cond_5
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->hasSeverity:Z

    if-eqz v0, :cond_7

    .line 4023
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->severity:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4025
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4026
    return-void
.end method
