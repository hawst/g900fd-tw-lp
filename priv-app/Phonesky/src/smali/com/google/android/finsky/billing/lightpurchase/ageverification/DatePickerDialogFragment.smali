.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DatePickerDialogFragment.java"


# instance fields
.field private mDatePickerDialog:Landroid/app/DatePickerDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;)Landroid/app/DatePickerDialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    return-object v0
.end method

.method private getListener()Landroid/app/DatePickerDialog$OnDateSetListener;
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 108
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Landroid/app/DatePickerDialog$OnDateSetListener;

    if-eqz v2, :cond_0

    .line 109
    check-cast v1, Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 115
    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v1

    .line 111
    .restart local v1    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 112
    .local v0, "activity":Landroid/app/Activity;
    instance-of v2, v0, Landroid/app/DatePickerDialog$OnDateSetListener;

    if-eqz v2, :cond_1

    .line 113
    check-cast v0, Landroid/app/DatePickerDialog$OnDateSetListener;

    .end local v0    # "activity":Landroid/app/Activity;
    move-object v1, v0

    goto :goto_0

    .line 115
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/util/Calendar;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;
    .locals 4
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 36
    if-nez p0, :cond_0

    .line 37
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Calendar is not set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 39
    :cond_0
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;-><init>()V

    .line 40
    .local v1, "fragment":Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "DatePickerDialogFragment.calendar"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 42
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x10

    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getListener()Landroid/app/DatePickerDialog$OnDateSetListener;

    move-result-object v7

    .line 54
    .local v7, "listener":Landroid/app/DatePickerDialog$OnDateSetListener;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_1

    const/4 v2, 0x0

    .line 56
    .local v2, "constructorListener":Landroid/app/DatePickerDialog$OnDateSetListener;
    :goto_0
    if-nez p1, :cond_2

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DatePickerDialogFragment.calendar"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/Calendar;

    .line 58
    .local v6, "calendar":Ljava/util/Calendar;
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v6, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v6, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x5

    invoke-virtual {v6, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    .line 66
    .end local v6    # "calendar":Ljava/util/Calendar;
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;

    invoke-direct {v4, p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;Landroid/app/DatePickerDialog$OnDateSetListener;)V

    invoke-virtual {v0, v1, v3, v4}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;)V

    invoke-virtual {v0, v1, v3, v4}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    return-object v0

    .end local v2    # "constructorListener":Landroid/app/DatePickerDialog$OnDateSetListener;
    :cond_1
    move-object v2, v7

    .line 54
    goto :goto_0

    .line 62
    .restart local v2    # "constructorListener":Landroid/app/DatePickerDialog$OnDateSetListener;
    :cond_2
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    .line 63
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    const-string v1, "DatePickerDialogFragment.calendar"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DatePickerDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    const-string v0, "DatePickerDialogFragment.calendar"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;

    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 97
    return-void
.end method

.method public setTargetFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "targetFragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 101
    return-void
.end method
