.class final Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;
.super Ljava/lang/Object;
.source "DeviceConfigurationHelper.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doUploadDeviceConfiguration(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$gcmRegistrationId:Ljava/lang/String;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

.field final synthetic val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;Lcom/google/android/finsky/protos/DeviceConfigurationProto;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iput-object p2, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$gcmRegistrationId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iput-object p4, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p5, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    iput-object p6, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;)V
    .locals 8
    .param p1, "response"    # Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x78

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$gcmRegistrationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$gcmRegistrationId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->setRegisteredOnServer(Ljava/lang/String;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$deviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p1, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 203
    iget-object v7, p1, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    .line 204
    .local v7, "token":Ljava/lang/String;
    const-string v0, "Received device config token %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v7, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->deviceConfigToken:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/GetTocHelper;->changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;->onSuccess()V

    .line 218
    .end local v7    # "token":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    # invokes: Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doNextRequest(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    invoke-static {v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->access$000(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    .line 219
    return-void

    .line 212
    :cond_2
    const-string v0, "Unexpected - missing UploadDeviceConfigToken"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    new-instance v1, Lcom/android/volley/ServerError;

    invoke-direct {v1}, Lcom/android/volley/ServerError;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;->onError(Lcom/android/volley/VolleyError;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 189
    check-cast p1, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;->onResponse(Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;)V

    return-void
.end method
