.class public final Lcom/google/android/finsky/protos/Targeting$Targets;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Targeting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Targeting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Targets"
.end annotation


# instance fields
.field public targetId:[J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Targeting$Targets;->clear()Lcom/google/android/finsky/protos/Targeting$Targets;

    .line 30
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Targeting$Targets;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->cachedSize:I

    .line 35
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 52
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v5, v5

    if-lez v5, :cond_1

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "dataSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 55
    iget-object v5, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    aget-wide v2, v5, v1

    .line 56
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v5

    add-int/2addr v0, v5

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    .end local v2    # "element":J
    :cond_0
    add-int/2addr v4, v0

    .line 60
    iget-object v5, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 62
    .end local v0    # "dataSize":I
    .end local v1    # "i":I
    :cond_1
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Targeting$Targets;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 71
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 75
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 76
    :sswitch_0
    return-object p0

    .line 81
    :sswitch_1
    const/16 v8, 0x8

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 83
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    if-nez v8, :cond_2

    move v1, v7

    .line 84
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 85
    .local v4, "newArray":[J
    if-eqz v1, :cond_1

    .line 86
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 90
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 83
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v1, v8

    goto :goto_1

    .line 93
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 94
    iput-object v4, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    goto :goto_0

    .line 98
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 99
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 101
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 102
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 103
    .local v5, "startPos":I
    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_4

    .line 104
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 107
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 108
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    if-nez v8, :cond_6

    move v1, v7

    .line 109
    .restart local v1    # "i":I
    :goto_4
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 110
    .restart local v4    # "newArray":[J
    if-eqz v1, :cond_5

    .line 111
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    :cond_5
    :goto_5
    array-length v8, v4

    if-ge v1, v8, :cond_7

    .line 114
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 108
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v1, v8

    goto :goto_4

    .line 116
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_7
    iput-object v4, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    .line 117
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Targeting$Targets;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Targeting$Targets;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v1, v1

    if-lez v1, :cond_0

    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 43
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Targeting$Targets;->targetId:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "i":I
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 47
    return-void
.end method
