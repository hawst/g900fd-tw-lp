.class public final Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AckNotificationsRequestProto"
.end annotation


# instance fields
.field public nackNotificationId:[Ljava/lang/String;

.field public notificationId:[Ljava/lang/String;

.field public signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4366
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4367
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    .line 4368
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;
    .locals 1

    .prologue
    .line 4371
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    .line 4372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 4373
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    .line 4374
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->cachedSize:I

    .line 4375
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 4405
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 4406
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 4407
    const/4 v0, 0x0

    .line 4408
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 4409
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 4410
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 4411
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 4412
    add-int/lit8 v0, v0, 0x1

    .line 4413
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 4409
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 4417
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 4418
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 4420
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v5, :cond_3

    .line 4421
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 4424
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 4425
    const/4 v0, 0x0

    .line 4426
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 4427
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 4428
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 4429
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 4430
    add-int/lit8 v0, v0, 0x1

    .line 4431
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 4427
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 4435
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v1

    .line 4436
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 4438
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4446
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4447
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4451
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4452
    :sswitch_0
    return-object p0

    .line 4457
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4459
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 4460
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 4461
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 4462
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4464
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4465
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 4466
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4464
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4459
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 4469
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 4470
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    goto :goto_0

    .line 4474
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-nez v5, :cond_4

    .line 4475
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 4477
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4481
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4483
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    if-nez v5, :cond_6

    move v1, v4

    .line 4484
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 4485
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 4486
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4488
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 4489
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 4490
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4488
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4483
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 4493
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 4494
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 4447
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4340
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4381
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 4382
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 4383
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->notificationId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 4384
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 4385
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4382
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4389
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v2, :cond_2

    .line 4390
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4392
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 4393
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 4394
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;->nackNotificationId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 4395
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 4396
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4393
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4400
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4401
    return-void
.end method
