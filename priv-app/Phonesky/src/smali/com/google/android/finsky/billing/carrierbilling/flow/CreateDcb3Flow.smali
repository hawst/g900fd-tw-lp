.class public Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
.super Lcom/google/android/finsky/billing/InstrumentFlow;
.source "CreateDcb3Flow.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;
.implements Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener;
.implements Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog$CarrierBillingErrorListener;
.implements Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;
.implements Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$3;,
        Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;,
        Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/InstrumentFlow;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;",
        ">;",
        "Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener;",
        "Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog$CarrierBillingErrorListener;",
        "Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;",
        "Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;"
    }
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

.field private mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

.field private mAddFragmentShown:Z

.field private mAddResult:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

.field private mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

.field private mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

.field private mAssociationAddress:Ljava/lang/String;

.field private mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

.field private mAssociationPrefix:Ljava/lang/String;

.field private mAssociationRequired:Z

.field private mBillingUiMode:I

.field private mCarrierName:Ljava/lang/String;

.field private final mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

.field private mDcbTosUrl:Ljava/lang/String;

.field private mDcbTosVersion:Ljava/lang/String;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

.field private mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

.field private final mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mPiiTosUrl:Ljava/lang/String;

.field private mPiiTosVersion:Ljava/lang/String;

.field private mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

.field private final mStorage:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

.field private mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field private mTitle:Ljava/lang/String;

.field private mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

.field private mVerifyFragment:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;I)V
    .locals 7
    .param p1, "billingFlowContext"    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2, "listener"    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "status"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .param p5, "mode"    # I

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;-><init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;I)V

    .line 154
    return-void
.end method

.method constructor <init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;I)V
    .locals 11
    .param p1, "billingFlowContext"    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2, "listener"    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3, "storage"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;
    .param p4, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p5, "status"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .param p6, "mode"    # I

    .prologue
    .line 160
    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, v6}, Lcom/google/android/finsky/billing/InstrumentFlow;-><init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)V

    .line 110
    sget-object v6, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->INIT:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 114
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddResult:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    .line 120
    sget-object v6, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->FULL_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    .line 161
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    .line 162
    iput-object p3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mStorage:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    .line 163
    iput-object p4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 164
    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mBillingUiMode:I

    .line 165
    sget-object v6, Lcom/google/android/finsky/config/G;->enableDcbReducedBillingAddress:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 168
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getBillingCountries()Ljava/util/List;

    move-result-object v3

    .line 169
    .local v3, "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    if-eqz v3, :cond_0

    .line 170
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/finsky/billing/BillingUtils;->getDefaultCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v1

    .line 172
    .local v1, "carrierBillingCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    if-eqz v1, :cond_0

    .line 173
    iget-boolean v6, v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->allowsReducedBillingAddress:Z

    if-eqz v6, :cond_4

    sget-object v6, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->REDUCED_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    :goto_0
    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    .line 178
    .end local v1    # "carrierBillingCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .end local v3    # "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    :cond_0
    invoke-interface {p4}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAccountName:Ljava/lang/String;

    .line 179
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-interface {p4}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 180
    move-object/from16 v0, p5

    iget-object v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    if-eqz v6, :cond_1

    .line 181
    move-object/from16 v0, p5

    iget-object v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    iget-object v6, v6, Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;->userTokenRequestAddress:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAddress:Ljava/lang/String;

    .line 182
    move-object/from16 v0, p5

    iget-object v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    iget-object v6, v6, Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;->userTokenRequestMessage:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationPrefix:Ljava/lang/String;

    .line 184
    :cond_1
    move-object/from16 v0, p5

    iget-boolean v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasName:Z

    if-eqz v6, :cond_5

    .line 185
    move-object/from16 v0, p5

    iget-object v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->name:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mCarrierName:Ljava/lang/String;

    .line 186
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    const v7, 0x7f0c00d0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mCarrierName:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/finsky/FinskyApp;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mTitle:Ljava/lang/String;

    .line 190
    :goto_1
    move-object/from16 v0, p5

    iget-object v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    if-eqz v6, :cond_3

    .line 191
    move-object/from16 v0, p5

    iget-object v2, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    .line 192
    .local v2, "carrierTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    iget-object v6, v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v6, :cond_2

    .line 193
    iget-object v4, v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 194
    .local v4, "dcbTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;
    iget-object v6, v4, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->url:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    .line 195
    iget-object v6, v4, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->version:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    .line 197
    .end local v4    # "dcbTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;
    :cond_2
    iget-object v6, v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v6, :cond_3

    .line 198
    iget-object v5, v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 199
    .local v5, "piiTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;
    iget-object v6, v5, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->url:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    .line 200
    iget-object v6, v5, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->version:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    .line 203
    .end local v2    # "carrierTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    .end local v5    # "piiTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;
    :cond_3
    move-object/from16 v0, p5

    iget-boolean v6, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->associationRequired:Z

    iput-boolean v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationRequired:Z

    .line 204
    return-void

    .line 173
    .restart local v1    # "carrierBillingCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .restart local v3    # "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    :cond_4
    sget-object v6, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->FULL_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    goto :goto_0

    .line 188
    .end local v1    # "carrierBillingCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .end local v3    # "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    :cond_5
    const-string v6, "No carrier name available in status."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->continueResume(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideVerifyAssociationFragment()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;)Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Lcom/google/android/finsky/billing/BillingUtils$AddressMode;)Lcom/google/android/finsky/billing/BillingUtils$AddressMode;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    return-object p1
.end method

.method private continueResume(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    sget-object v1, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->INIT:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    if-eq v0, v1, :cond_0

    .line 285
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 287
    :cond_0
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    sget-object v1, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SENDING_REQUEST:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    if-ne v0, v1, :cond_1

    .line 291
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->finish()V

    .line 293
    :cond_1
    const-string v0, "add_fragment_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragmentShown:Z

    .line 294
    const-string v0, "user_provided_address"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 295
    const-string v0, "dcb_tos_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    const-string v0, "dcb_tos_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    .line 298
    :cond_2
    const-string v0, "dcb_tos_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 299
    const-string v0, "dcb_tos_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    .line 301
    :cond_3
    const-string v0, "pii_tos_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 302
    const-string v0, "pii_tos_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    .line 304
    :cond_4
    const-string v0, "pii_tos_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 305
    const-string v0, "pii_tos_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    .line 307
    :cond_5
    const-string v0, "error_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 308
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "error_dialog"

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/billing/BillingFlowContext;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog$CarrierBillingErrorListener;)V

    .line 312
    :cond_6
    const-string v0, "add_fragment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "add_fragment"

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/billing/BillingFlowContext;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener;)V

    .line 317
    :cond_7
    const-string v0, "edit_fragment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 318
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "edit_fragment"

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/billing/BillingFlowContext;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    .line 320
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;)V

    .line 322
    :cond_8
    const-string v0, "verify_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 323
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/flow/association/CarrierOutAssociation;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationPrefix:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/CarrierOutAssociation;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    .line 325
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;-><init>(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    .line 326
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    invoke-interface {v0, p1, v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->resumeState(Landroid/os/Bundle;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 327
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "verify_dialog"

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/billing/BillingFlowContext;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    .line 328
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    if-eqz v0, :cond_a

    .line 329
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V

    .line 334
    :cond_9
    :goto_0
    return-void

    .line 330
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;

    if-eqz v0, :cond_9

    .line 331
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V

    goto :goto_0
.end method

.method private createCarrierBillingInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 488
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;-><init>()V

    .line 489
    .local v1, "dcbInstrument":Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mStorage:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;->getCurrentSimIdentifier()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;->instrumentKey:Ljava/lang/String;

    .line 490
    iput-boolean v5, v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;->hasInstrumentKey:Z

    .line 492
    new-instance v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;-><init>()V

    .line 493
    .local v0, "carrierTos":Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 494
    new-instance v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-direct {v3}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;-><init>()V

    iput-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 495
    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->version:Ljava/lang/String;

    .line 496
    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    iput-boolean v5, v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->hasVersion:Z

    .line 498
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 499
    new-instance v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-direct {v3}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;-><init>()V

    iput-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 500
    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->version:Ljava/lang/String;

    .line 501
    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    iput-boolean v5, v3, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;->hasVersion:Z

    .line 503
    :cond_1
    iput-object v0, v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;->acceptedCarrierTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    .line 504
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    .line 505
    .local v2, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    if-eqz v3, :cond_2

    .line 506
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {v3, v4}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->subscriberInfoToAddress(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Lcom/google/android/finsky/billing/BillingUtils$AddressMode;)Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 515
    :goto_0
    iput-object v1, v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    .line 516
    return-object v2

    .line 509
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v3, :cond_3

    .line 510
    const-string v3, "No Subscriber address available."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 512
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iput-object v3, v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    goto :goto_0
.end method

.method private hideEditFragment()V
    .locals 3

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->hideFragment(Landroid/support/v4/app/Fragment;Z)V

    .line 585
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    .line 587
    :cond_0
    return-void
.end method

.method private hideProgress()V
    .locals 2

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/BillingFlowContext;->hideProgress()V

    .line 645
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->enableUi(Z)V

    .line 648
    :cond_0
    return-void
.end method

.method private hideTosFragment()V
    .locals 3

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->hideFragment(Landroid/support/v4/app/Fragment;Z)V

    .line 568
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    .line 570
    :cond_0
    return-void
.end method

.method private hideVerifyAssociationFragment()V
    .locals 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 606
    iget v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mBillingUiMode:I

    if-nez v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->dismiss()V

    .line 612
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    .line 614
    :cond_0
    return-void

    .line 609
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->hideFragment(Landroid/support/v4/app/Fragment;Z)V

    .line 610
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideProgress()V

    goto :goto_0
.end method

.method private isSubscriberAddressValid(Lcom/google/android/finsky/protos/BillingAddress$Address;)Z
    .locals 5
    .param p1, "subscriberAddress"    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 470
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mStorage:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    invoke-static {v3, v4}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->isPhoneNumberRequired(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v0

    .line 472
    .local v0, "requiresPhoneNumber":Z
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p1, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    iget-object v3, p1, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v1, v2

    .line 484
    :cond_1
    :goto_0
    return v1

    .line 479
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    sget-object v4, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->FULL_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    if-ne v3, v4, :cond_1

    .line 480
    iget-object v3, p1, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p1, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private logDcbAddError(ILjava/lang/String;)V
    .locals 7
    .param p1, "errorCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x156

    move-object v3, v2

    move v4, p1

    move-object v5, p2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 213
    return-void
.end method

.method private logDcbAddError(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 207
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logDcbAddError(ILjava/lang/String;)V

    .line 208
    return-void
.end method

.method private logError(Ljava/lang/String;)V
    .locals 0
    .param p1, "logError"    # Ljava/lang/String;

    .prologue
    .line 218
    return-void
.end method

.method private showErrorDialog(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "fatal"    # Z

    .prologue
    .line 617
    invoke-static {p1, p2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;->newInstance(Ljava/lang/String;Z)Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    .line 618
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog$CarrierBillingErrorListener;)V

    .line 619
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    const-string v2, "error"

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->showDialogFragment(Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 620
    return-void
.end method

.method private showProgress()V
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const v1, 0x7f0c00c6

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/BillingFlowContext;->showProgress(I)V

    .line 638
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->enableUi(Z)V

    .line 641
    :cond_0
    return-void
.end method

.method private showVerifyAssociationFragment()V
    .locals 4

    .prologue
    .line 590
    iget v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mBillingUiMode:I

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    .line 592
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V

    .line 593
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    const-string v2, "verifying pin"

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->showDialogFragment(Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 602
    :goto_0
    return-void

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    .line 597
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V

    .line 598
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const v3, 0x7f0c00cd

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/BillingFlowContext;->showFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 600
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showProgress()V

    goto :goto_0
.end method


# virtual methods
.method public back()V
    .locals 0

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->cancel()V

    .line 253
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x1

    return v0
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 259
    invoke-super {p0}, Lcom/google/android/finsky/billing/InstrumentFlow;->cancel()V

    .line 260
    return-void
.end method

.method getState()Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    return-object v0
.end method

.method initParams()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 236
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mCarrierName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    if-nez v1, :cond_1

    .line 237
    :cond_0
    const-string v1, "Cannot run this BillingFlow since carrier name or DFE api is null."

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const v2, 0x7f0c00db

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->fail(Ljava/lang/String;)V

    .line 242
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onActivityResume()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    invoke-interface {v0, v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->setListener(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 281
    :cond_0
    return-void
.end method

.method public onAddCarrierBillingResult(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    .prologue
    .line 659
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddResult:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    .line 660
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideTosFragment()V

    .line 661
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;->SUCCESS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    if-ne p1, v0, :cond_0

    .line 662
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performNext()V

    .line 670
    :goto_0
    return-void

    .line 663
    :cond_0
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;->EDIT_ADDRESS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    if-ne p1, v0, :cond_1

    .line 664
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performNext()V

    goto :goto_0

    .line 665
    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;->CANCELED:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    if-ne p1, v0, :cond_2

    .line 666
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->cancel()V

    goto :goto_0

    .line 668
    :cond_2
    const-string v0, "Invalid error code."

    const-string v1, "UNKNOWN"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showGenericError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onEditCarrierBillingResult(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)V
    .locals 0
    .param p1, "returnAddress"    # Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .prologue
    .line 674
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideEditFragment()V

    .line 675
    if-eqz p1, :cond_0

    .line 676
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 677
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performNext()V

    .line 681
    :goto_0
    return-void

    .line 679
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->cancel()V

    goto :goto_0
.end method

.method public onErrorDismiss(Z)V
    .locals 2
    .param p1, "errorFatal"    # Z

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->cancel()V

    .line 698
    :cond_0
    if-eqz p1, :cond_1

    .line 699
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    const v1, 0x7f0c00db

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->fail(Ljava/lang/String;)V

    .line 703
    :goto_0
    return-void

    .line 701
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->cancel()V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x0

    .line 759
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideProgress()V

    .line 760
    const-string v0, "Error received: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 761
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logDcbAddError(Lcom/android/volley/VolleyError;)V

    .line 762
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showErrorDialog(Ljava/lang/String;Z)V

    .line 763
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 653
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->hideProgress()V

    .line 654
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performNext()V

    .line 655
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 75
    check-cast p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->onResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V

    return-void
.end method

.method public onVerifyCancel()V
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->cancel()V

    .line 690
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->cancel()V

    .line 691
    return-void
.end method

.method performNext()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 373
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$3;->$SwitchMap$com$google$android$finsky$billing$carrierbilling$flow$CreateDcb3Flow$State:[I

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :pswitch_0
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_PII_TOS:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 376
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showTosFragment(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performNext()V

    goto :goto_0

    .line 383
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationRequired:Z

    if-eqz v0, :cond_1

    .line 384
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->ASSOCIATING_PIN:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 385
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->startAssociation()V

    goto :goto_0

    .line 387
    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_EDIT_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 388
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showEditAddressFragment(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 392
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->isSubscriberAddressValid(Lcom/google/android/finsky/protos/BillingAddress$Address;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 393
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_EDIT_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 394
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showEditAddressFragment(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 397
    :cond_2
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_DCB_TOS_AND_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 398
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 401
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->getSubscriberInfo(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showTosFragment(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v1, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showTosFragment(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 412
    :pswitch_3
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_DCB_TOS_AND_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 413
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showTosFragment(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 416
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddResult:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    sget-object v1, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;->EDIT_ADDRESS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener$AddResult;

    if-ne v0, v1, :cond_4

    .line 417
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_EDIT_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 418
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showEditAddressFragment(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 420
    :cond_4
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SENDING_REQUEST:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 421
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showProgress()V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->performRequest()V

    goto :goto_0

    .line 426
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    if-nez v0, :cond_5

    .line 427
    const-string v0, "Update instrument response is null."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428
    const-string v0, "Update instrument response is null."

    const-string v1, "UNKNOWN"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showGenericError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 429
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    if-nez v0, :cond_6

    .line 431
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x155

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 434
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->DONE:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 435
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V

    goto/16 :goto_0

    .line 436
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    if-eqz v0, :cond_7

    .line 437
    const-string v0, "Unexpected checkout_token_required."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 438
    const-string v0, "Unexpected checkout_token_required."

    const-string v1, "UNKNOWN"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showGenericError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 441
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->getRetriableErrors(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Ljava/util/ArrayList;

    move-result-object v7

    .line 443
    .local v7, "retriableErrors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v7, :cond_8

    .line 444
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logDcbAddError(ILjava/lang/String;)V

    .line 445
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->SHOWING_EDIT_USERINFO:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 446
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showEditAddressFragment(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 448
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    if-eqz v0, :cond_9

    .line 449
    const/4 v0, 0x3

    invoke-direct {p0, v0, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logDcbAddError(ILjava/lang/String;)V

    .line 450
    const-string v0, "Update carrier billing instrument had error"

    const-string v1, "UNKNOWN"

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddCarrierBillingResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 454
    :cond_9
    invoke-direct {p0, v4, v2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logDcbAddError(ILjava/lang/String;)V

    .line 455
    const-string v0, "Could not add carrier billing instrument."

    const-string v1, "UNKNOWN"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showGenericError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected performRequest()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 521
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x154

    const/4 v4, 0x0

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 523
    new-instance v7, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;

    invoke-direct {v7}, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;-><init>()V

    .line 524
    .local v7, "request":Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->createCarrierBillingInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 525
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, v7, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->updateInstrument(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 526
    return-void
.end method

.method public resumeFromSavedState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->initParams()Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    :goto_0
    return-void

    .line 267
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;-><init>()V

    .line 268
    .local v0, "dcbAction":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$2;-><init>(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;->run(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 338
    const-string v0, "state"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v0, "add_fragment_shown"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragmentShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 340
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "error_dialog"

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mErrorFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/CarrierBillingErrorDialog;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "add_fragment"

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    if-eqz v0, :cond_2

    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "edit_fragment"

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_3

    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    const-string v1, "verify_dialog"

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mVerifyFragment:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/billing/BillingFlowContext;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    if-eqz v0, :cond_3

    .line 352
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->saveState(Landroid/os/Bundle;)V

    .line 355
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    if-eqz v0, :cond_4

    .line 356
    const-string v0, "user_provided_address"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 358
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 359
    const-string v0, "dcb_tos_url"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v0, "dcb_tos_version"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDcbTosVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 363
    const-string v0, "pii_tos_version"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v0, "pii_tos_url"

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_6
    return-void
.end method

.method setState(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;)V
    .locals 0
    .param p1, "state"    # Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .prologue
    .line 767
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mState:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$State;

    .line 768
    return-void
.end method

.method showEditAddressFragment(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 574
    .local p1, "errorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mUserProvidedAddress:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 576
    .local v0, "prefillAddress":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    iget v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mBillingUiMode:I

    invoke-static {v1, v2, v0, p1, v3}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/util/ArrayList;I)Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    .line 578
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;)V

    .line 579
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mEditFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mTitle:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/BillingFlowContext;->showFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 580
    return-void

    .line 574
    .end local v0    # "prefillAddress":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mSubscriberAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v1}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->getSubscriberInfo(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    move-result-object v0

    goto :goto_0
.end method

.method showError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "finskyLogString"    # Ljava/lang/String;
    .param p2, "logString"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "fatal"    # Z

    .prologue
    .line 624
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 625
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->logError(Ljava/lang/String;)V

    .line 626
    invoke-direct {p0, p3, p4}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showErrorDialog(Ljava/lang/String;Z)V

    .line 627
    return-void
.end method

.method showGenericError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "finskyLogString"    # Ljava/lang/String;
    .param p2, "logString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 631
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    const v1, 0x7f0c00df

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mCarrierName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/FinskyApp;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 634
    return-void
.end method

.method showTosFragment(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "editedAddress"    # Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "snippet"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 540
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 541
    if-eqz p1, :cond_0

    .line 542
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;->FULL_ADDRESS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;

    .line 558
    .local v0, "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mCarrierName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAccountName:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mBillingUiMode:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->newInstance(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    .line 560
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$AddCarrierBillingResultListener;)V

    .line 561
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragmentShown:Z

    .line 562
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mContext:Lcom/google/android/finsky/billing/BillingFlowContext;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAddFragment:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mTitle:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v7}, Lcom/google/android/finsky/billing/BillingFlowContext;->showFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 563
    .end local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    :goto_1
    return-void

    .line 543
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 544
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;->ADDRESS_SNIPPET:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;

    .restart local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    goto :goto_0

    .line 546
    .end local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    :cond_1
    const-string v1, "showTosFragment has no address and tos. wrong fragment."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 550
    :cond_2
    if-eqz p1, :cond_3

    .line 551
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;->FULL_ADDRESS_AND_TOS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;

    .restart local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    goto :goto_0

    .line 552
    .end local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 553
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;->ADDRESS_SNIPPET_AND_TOS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;

    .restart local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    goto :goto_0

    .line 555
    .end local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    :cond_4
    sget-object v0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;->TOS:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;

    .restart local v0    # "type":Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$Type;
    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->initParams()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    :goto_0
    return-void

    .line 225
    :cond_0
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;-><init>()V

    .line 226
    .local v0, "dcbAction":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$1;-><init>(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;->run(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method startAssociation()V
    .locals 6

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->showVerifyAssociationFragment()V

    .line 531
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/flow/association/CarrierOutAssociation;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationPrefix:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mPiiTosVersion:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/CarrierOutAssociation;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    .line 533
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;-><init>(Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    .line 534
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationAction:Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;

    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow;->mAssociationListener:Lcom/google/android/finsky/billing/carrierbilling/flow/CreateDcb3Flow$AssociationListener;

    invoke-interface {v0, v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/flow/association/AssociationAction;->start(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 535
    return-void
.end method
