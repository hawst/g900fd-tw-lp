.class public final Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DebugInfoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Timing"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;


# instance fields
.field public hasName:Z

.field public hasTimeInMs:Z

.field public name:Ljava/lang/String;

.field public timeInMs:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->clear()Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    .line 38
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->_emptyArray:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->_emptyArray:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    sput-object v0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->_emptyArray:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->_emptyArray:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    .line 42
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasName:Z

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    .line 44
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasTimeInMs:Z

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->cachedSize:I

    .line 46
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 65
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasTimeInMs:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 71
    :cond_2
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 82
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 83
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 87
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    :sswitch_0
    return-object p0

    .line 93
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    .line 94
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasName:Z

    goto :goto_0

    .line 98
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    .line 99
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasTimeInMs:Z

    goto :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x21 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 55
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->hasTimeInMs:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 57
    :cond_2
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->timeInMs:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 59
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 60
    return-void
.end method
