.class Lcom/google/android/finsky/providers/AppIconProvider$IconCache;
.super Landroid/support/v4/util/LruCache;
.source "AppIconProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/providers/AppIconProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IconCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "maxSize"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    .line 145
    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Z
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Ljava/lang/Object;
    .param p4, "x3"    # Ljava/lang/Object;

    .prologue
    .line 142
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    check-cast p3, Ljava/io/File;

    .end local p3    # "x2":Ljava/lang/Object;
    check-cast p4, Ljava/io/File;

    .end local p4    # "x3":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->entryRemoved(ZLjava/lang/String;Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Ljava/io/File;
    .param p4, "newValue"    # Ljava/io/File;

    .prologue
    .line 148
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 149
    return-void
.end method
