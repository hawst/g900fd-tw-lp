.class public final Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditorialSeriesContainer"
.end annotation


# instance fields
.field public colorThemeArgb:Ljava/lang/String;

.field public episodeSubtitle:Ljava/lang/String;

.field public episodeTitle:Ljava/lang/String;

.field public hasColorThemeArgb:Z

.field public hasEpisodeSubtitle:Z

.field public hasEpisodeTitle:Z

.field public hasSeriesSubtitle:Z

.field public hasSeriesTitle:Z

.field public seriesSubtitle:Ljava/lang/String;

.field public seriesTitle:Ljava/lang/String;

.field public videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4244
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4245
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->clear()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 4246
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    .line 4250
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    .line 4251
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    .line 4252
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    .line 4253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    .line 4254
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    .line 4255
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    .line 4256
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    .line 4257
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    .line 4258
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    .line 4259
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 4260
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->cachedSize:I

    .line 4261
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4295
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4296
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 4297
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4300
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 4301
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4304
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 4305
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4308
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 4309
    :cond_6
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4312
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 4313
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4316
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 4317
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 4318
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    aget-object v0, v3, v1

    .line 4319
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v0, :cond_a

    .line 4320
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4317
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4325
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .end local v1    # "i":I
    :cond_b
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 4333
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4334
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4338
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4339
    :sswitch_0
    return-object p0

    .line 4344
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    .line 4345
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    goto :goto_0

    .line 4349
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    .line 4350
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    goto :goto_0

    .line 4354
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    .line 4355
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    goto :goto_0

    .line 4359
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    .line 4360
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    goto :goto_0

    .line 4364
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    .line 4365
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    goto :goto_0

    .line 4369
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4371
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v5, :cond_2

    move v1, v4

    .line 4372
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 4374
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v1, :cond_1

    .line 4375
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4377
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4378
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;-><init>()V

    aput-object v5, v2, v1

    .line 4379
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4380
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4377
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4371
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v1, v5

    goto :goto_1

    .line 4383
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;-><init>()V

    aput-object v5, v2, v1

    .line 4384
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4385
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    goto :goto_0

    .line 4334
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4204
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4267
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4268
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4270
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 4271
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4273
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 4274
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4276
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 4277
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4279
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 4280
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4282
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 4283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 4284
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    aget-object v0, v2, v1

    .line 4285
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v0, :cond_a

    .line 4286
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4283
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4290
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .end local v1    # "i":I
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4291
    return-void
.end method
