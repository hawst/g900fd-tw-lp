.class final Lcom/google/android/finsky/utils/WishlistHelper$1;
.super Ljava/lang/Object;
.source "WishlistHelper.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$docId:Ljava/lang/String;

.field final synthetic val$wasInWishlist:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p2, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$docId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$wasInWishlist:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    .prologue
    const/4 v1, 0x1

    .line 141
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    const-string v4, "modifed_wishlist"

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    .line 144
    iget-object v2, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$docId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/finsky/utils/WishlistHelper$1;->val$wasInWishlist:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    # invokes: Lcom/google/android/finsky/utils/WishlistHelper;->invokeWishlistStatusListeners(Ljava/lang/String;ZZ)V
    invoke-static {v2, v0, v1}, Lcom/google/android/finsky/utils/WishlistHelper;->access$000(Ljava/lang/String;ZZ)V

    .line 145
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    # setter for: Lcom/google/android/finsky/utils/WishlistHelper;->sLastWishlistMutationTimeMs:J
    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/WishlistHelper;->access$102(J)J

    .line 146
    return-void

    .line 144
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 138
    check-cast p1, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/WishlistHelper$1;->onResponse(Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;)V

    return-void
.end method
