.class public Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;
.super Ljava/lang/Object;
.source "PackageInstallerLegacyImpl.java"

# interfaces
.implements Lcom/google/android/finsky/installer/PackageInstallerFacade;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;->mContext:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public cancelSession(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 35
    return-void
.end method

.method public createSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installSize"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "appIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 26
    return-void
.end method

.method public getAppIconSize()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    return v0
.end method

.method public hasSession(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 11
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "expectedSize"    # J
    .param p5, "expectedSignature"    # Ljava/lang/String;
    .param p6, "isForwardLocked"    # Z
    .param p7, "packageName"    # Ljava/lang/String;
    .param p8, "callback"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    .line 57
    new-instance v7, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;

    move-object/from16 v0, p8

    invoke-direct {v7, p0, v0}, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl$1;-><init>(Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V

    .local v7, "helperListener":Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    .line 74
    invoke-static/range {v2 .. v9}, Lcom/google/android/finsky/utils/PackageManagerHelper;->installPackage(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;)V

    .line 76
    return-void
.end method

.method public pruneSessions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "packagesToRetain":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public reportProgress(Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bytesCompleted"    # J
    .param p4, "bytesTotal"    # J

    .prologue
    .line 43
    return-void
.end method

.method public setAppIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 52
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p1}, Lcom/google/android/finsky/utils/PackageManagerHelper;->uninstallPackage(Ljava/lang/String;)V

    .line 81
    return-void
.end method
