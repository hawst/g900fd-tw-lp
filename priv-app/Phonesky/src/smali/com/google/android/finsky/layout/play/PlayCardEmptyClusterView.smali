.class public Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;
.super Lcom/google/android/finsky/layout/play/PlayClusterView;
.source "PlayCardEmptyClusterView.java"


# instance fields
.field private mEmptyImage:Lcom/google/android/play/image/FifeImageView;

.field private mEmptyText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 7
    .param p1, "clusterDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 53
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->configureLogging([BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 54
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasEmptyContainer()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getEmptyContainer()Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    move-result-object v2

    iget-object v1, v2, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;->emptyMessage:Ljava/lang/String;

    .line 56
    .local v1, "emptyMessage":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackingDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 59
    .local v0, "emptyImage":Lcom/google/android/finsky/protos/Common$Image;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    :goto_1
    if-eqz v0, :cond_2

    .line 66
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v3, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 68
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v2, v6}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 75
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->logEmptyClusterImpression()V

    .line 76
    return-void

    .line 54
    .end local v0    # "emptyImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "emptyMessage":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 63
    .restart local v0    # "emptyImage":Lcom/google/android/finsky/protos/Common$Image;
    .restart local v1    # "emptyMessage":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 70
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 134
    const/16 v0, 0x1a0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->onFinishInflate()V

    .line 42
    const v0, 0x7f0a02bf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    .line 43
    const v0, 0x7f0a02c0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    .line 44
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->getWidth()I

    move-result v3

    .line 106
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->getPaddingTop()I

    move-result v4

    .line 108
    .local v4, "y":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_0

    .line 109
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v5, v7, v4, v3, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    .line 110
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 113
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v5

    if-eq v5, v8, :cond_1

    .line 114
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 116
    .local v0, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    .line 117
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    invoke-virtual {v5, v7, v4, v3, v6}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 118
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v5

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 121
    .end local v0    # "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 122
    .local v1, "textWidth":I
    sub-int v5, v3, v1

    div-int/lit8 v2, v5, 0x2

    .line 123
    .local v2, "textX":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    add-int v6, v2, v1

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v5, v2, v4, v6, v7}, Landroid/widget/TextView;->layout(IIII)V

    .line 124
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 80
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 81
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->getPaddingBottom()I

    move-result v5

    add-int v0, v4, v5

    .line 83
    .local v0, "height":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 84
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4, p1, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->measure(II)V

    .line 85
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 88
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v4

    if-eq v4, v7, :cond_1

    .line 89
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 91
    .local v2, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 92
    .local v1, "imageHeightSpec":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4, p1, v1}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 93
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 96
    .end local v1    # "imageHeightSpec":I
    .end local v2    # "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    div-int/lit8 v5, v3, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 98
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 100
    invoke-virtual {p0, v3, v0}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->setMeasuredDimension(II)V

    .line 101
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->onRecycle()V

    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->mEmptyImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 130
    return-void
.end method
