.class public Lcom/google/android/finsky/layout/play/PlaySocialHeader;
.super Lcom/google/android/finsky/layout/IdentifiableFrameLayout;
.source "PlaySocialHeader.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;


# instance fields
.field private mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

.field private mCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

.field private mCoverImageFrameOverlay:Landroid/view/View;

.field private final mIdealCoverHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mIdealCoverHeight:I

    .line 43
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "coverImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p2, "primaryPerson"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p3, "secondaryPersons"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->setData(Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 60
    return-void
.end method

.method public getOverlayTransparencyHeightFromTop()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public getOverlayableImageHeight()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;->onFinishInflate()V

    .line 49
    const v0, 0x7f0a02ac

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HeroImageFrame;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    const v1, 0x7f0a02ad

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/HeroImageFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 51
    const v0, 0x7f0a02ae

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrameOverlay:Landroid/view/View;

    .line 52
    const v0, 0x7f0a02bb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    .line 53
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v4, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->getWidth()I

    move-result v1

    .line 99
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->getHeight()I

    move-result v0

    .line 101
    .local v0, "height":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/HeroImageFrame;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v2, v4, v4, v1, v3}, Lcom/google/android/finsky/layout/HeroImageFrame;->layout(IIII)V

    .line 102
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrameOverlay:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrameOverlay:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v2, v4, v4, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 103
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v4, v3, v1, v0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->layout(IIII)V

    .line 104
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 79
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 80
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v2, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 81
    .local v2, "screenHeight":I
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mIdealCoverHeight:I

    div-int/lit8 v6, v2, 0x3

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 83
    .local v1, "coverHeight":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, p1, v6}, Lcom/google/android/finsky/layout/HeroImageFrame;->measure(II)V

    .line 85
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrameOverlay:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrameOverlay:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, p1, v6}, Landroid/view/View;->measure(II)V

    .line 88
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->measure(II)V

    .line 90
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getMeasuredHeight()I

    move-result v0

    .line 91
    .local v0, "avatarPackHeight":I
    int-to-float v5, v0

    const v6, 0x3e99999a    # 0.3f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    add-int v3, v1, v5

    .line 93
    .local v3, "totalHeight":I
    invoke-virtual {p0, v4, v3}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->setMeasuredDimension(II)V

    .line 94
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->onRecycle()V

    .line 110
    return-void
.end method

.method public setOverlayableImageTopPadding(I)V
    .locals 2
    .param p1, "topPadding"    # I

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->mCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-virtual {v0, v1, p1, v1, v1}, Lcom/google/android/finsky/layout/HeroImageFrame;->setPadding(IIII)V

    .line 75
    return-void
.end method
