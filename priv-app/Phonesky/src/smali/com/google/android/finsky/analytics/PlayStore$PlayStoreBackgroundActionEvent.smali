.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreBackgroundActionEvent"
.end annotation


# instance fields
.field public appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

.field public attempts:I

.field public authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

.field public callingPackage:Ljava/lang/String;

.field public clientLatencyMs:J

.field public creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

.field public deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

.field public document:Ljava/lang/String;

.field public errorCode:I

.field public exceptionType:Ljava/lang/String;

.field public fromSetting:I

.field public hasAttempts:Z

.field public hasCallingPackage:Z

.field public hasClientLatencyMs:Z

.field public hasDocument:Z

.field public hasErrorCode:Z

.field public hasExceptionType:Z

.field public hasFromSetting:Z

.field public hasHost:Z

.field public hasLastUrl:Z

.field public hasOfferCheckoutFlowRequired:Z

.field public hasOfferType:Z

.field public hasOperationSuccess:Z

.field public hasReason:Z

.field public hasServerLatencyMs:Z

.field public hasServerLogsCookie:Z

.field public hasToSetting:Z

.field public hasType:Z

.field public host:Ljava/lang/String;

.field public lastUrl:Ljava/lang/String;

.field public nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

.field public offerCheckoutFlowRequired:Z

.field public offerType:I

.field public operationSuccess:Z

.field public reason:Ljava/lang/String;

.field public reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

.field public rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

.field public searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

.field public serverLatencyMs:J

.field public serverLogsCookie:[B

.field public sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

.field public toSetting:I

.field public type:I

.field public widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

.field public wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2439
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .line 2440
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2443
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    .line 2444
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    .line 2445
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    .line 2446
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    .line 2447
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    .line 2448
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    .line 2449
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    .line 2450
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    .line 2451
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    .line 2452
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    .line 2453
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    .line 2454
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    .line 2455
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    .line 2456
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    .line 2457
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    .line 2458
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    .line 2459
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    .line 2460
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    .line 2461
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .line 2462
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 2463
    iput-wide v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    .line 2464
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    .line 2465
    iput-wide v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    .line 2466
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    .line 2467
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .line 2468
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    .line 2469
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    .line 2470
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    .line 2471
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasHost:Z

    .line 2472
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    .line 2473
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    .line 2474
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    .line 2475
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAttempts:Z

    .line 2476
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    .line 2477
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferCheckoutFlowRequired:Z

    .line 2478
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 2479
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    .line 2480
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasCallingPackage:Z

    .line 2481
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    .line 2482
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    .line 2483
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasLastUrl:Z

    .line 2484
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 2485
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    .line 2486
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    .line 2487
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    .line 2488
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    .line 2489
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2584
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2585
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    if-eqz v1, :cond_1

    .line 2586
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2589
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2590
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2593
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2594
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2597
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    if-eqz v1, :cond_7

    .line 2598
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2601
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2602
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2605
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2606
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2609
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    if-nez v1, :cond_c

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    if-eqz v1, :cond_d

    .line 2610
    :cond_c
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2613
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    if-nez v1, :cond_e

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    if-eqz v1, :cond_f

    .line 2614
    :cond_e
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2617
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    if-nez v1, :cond_10

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    if-eqz v1, :cond_11

    .line 2618
    :cond_10
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2621
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    if-eqz v1, :cond_12

    .line 2622
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2625
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    if-eqz v1, :cond_13

    .line 2626
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2629
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    if-nez v1, :cond_14

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 2630
    :cond_14
    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2633
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    if-nez v1, :cond_16

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_17

    .line 2634
    :cond_16
    const/16 v1, 0xd

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2637
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    if-eqz v1, :cond_18

    .line 2638
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2641
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    if-nez v1, :cond_19

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    if-eqz v1, :cond_1a

    .line 2642
    :cond_19
    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2645
    :cond_1a
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasHost:Z

    if-nez v1, :cond_1b

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 2646
    :cond_1b
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2649
    :cond_1c
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    if-eqz v1, :cond_1d

    .line 2650
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2653
    :cond_1d
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    if-eqz v1, :cond_1e

    .line 2654
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2657
    :cond_1e
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAttempts:Z

    if-nez v1, :cond_1f

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    if-eqz v1, :cond_20

    .line 2658
    :cond_1f
    const/16 v1, 0x13

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2661
    :cond_20
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferCheckoutFlowRequired:Z

    if-nez v1, :cond_21

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    if-eqz v1, :cond_22

    .line 2662
    :cond_21
    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2665
    :cond_22
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    if-eqz v1, :cond_23

    .line 2666
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2669
    :cond_23
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasCallingPackage:Z

    if-nez v1, :cond_24

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_25

    .line 2670
    :cond_24
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2673
    :cond_25
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    if-eqz v1, :cond_26

    .line 2674
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2677
    :cond_26
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasLastUrl:Z

    if-nez v1, :cond_27

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_28

    .line 2678
    :cond_27
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2681
    :cond_28
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-eqz v1, :cond_29

    .line 2682
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2685
    :cond_29
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    if-eqz v1, :cond_2a

    .line 2686
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2689
    :cond_2a
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    if-eqz v1, :cond_2b

    .line 2690
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2693
    :cond_2b
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    if-eqz v1, :cond_2c

    .line 2694
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2697
    :cond_2c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2706
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2710
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2711
    :sswitch_0
    return-object p0

    .line 2716
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2717
    .local v1, "value":I
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    .line 2836
    :sswitch_2
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    .line 2837
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    goto :goto_0

    .line 2843
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    .line 2844
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    goto :goto_0

    .line 2848
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    .line 2849
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    goto :goto_0

    .line 2853
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    .line 2854
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    goto :goto_0

    .line 2858
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    .line 2859
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    goto :goto_0

    .line 2863
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    .line 2864
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    goto :goto_0

    .line 2868
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    .line 2869
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    goto :goto_0

    .line 2873
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    .line 2874
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    goto :goto_0

    .line 2878
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    .line 2879
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    goto :goto_0

    .line 2883
    :sswitch_b
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    if-nez v2, :cond_1

    .line 2884
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .line 2886
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2890
    :sswitch_c
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    if-nez v2, :cond_2

    .line 2891
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 2893
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2897
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    .line 2898
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    goto/16 :goto_0

    .line 2902
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    .line 2903
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    goto/16 :goto_0

    .line 2907
    :sswitch_f
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    if-nez v2, :cond_3

    .line 2908
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .line 2910
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2914
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    .line 2915
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    goto/16 :goto_0

    .line 2919
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    .line 2920
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasHost:Z

    goto/16 :goto_0

    .line 2924
    :sswitch_12
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    if-nez v2, :cond_4

    .line 2925
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    .line 2927
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2931
    :sswitch_13
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    if-nez v2, :cond_5

    .line 2932
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    .line 2934
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2938
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    .line 2939
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAttempts:Z

    goto/16 :goto_0

    .line 2943
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    .line 2944
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferCheckoutFlowRequired:Z

    goto/16 :goto_0

    .line 2948
    :sswitch_16
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    if-nez v2, :cond_6

    .line 2949
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 2951
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2955
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    .line 2956
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasCallingPackage:Z

    goto/16 :goto_0

    .line 2960
    :sswitch_18
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    if-nez v2, :cond_7

    .line 2961
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    .line 2963
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2967
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    .line 2968
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasLastUrl:Z

    goto/16 :goto_0

    .line 2972
    :sswitch_1a
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-nez v2, :cond_8

    .line 2973
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 2975
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2979
    :sswitch_1b
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    if-nez v2, :cond_9

    .line 2980
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    .line 2982
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2986
    :sswitch_1c
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    if-nez v2, :cond_a

    .line 2987
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    .line 2989
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2993
    :sswitch_1d
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    if-nez v2, :cond_b

    .line 2994
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    .line 2996
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2706
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x38 -> :sswitch_8
        0x40 -> :sswitch_9
        0x48 -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
        0x68 -> :sswitch_e
        0x72 -> :sswitch_f
        0x78 -> :sswitch_10
        0x82 -> :sswitch_11
        0x8a -> :sswitch_12
        0x92 -> :sswitch_13
        0x98 -> :sswitch_14
        0xa0 -> :sswitch_15
        0xaa -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
    .end sparse-switch

    .line 2717
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
        0x66 -> :sswitch_2
        0x67 -> :sswitch_2
        0x68 -> :sswitch_2
        0x69 -> :sswitch_2
        0x6a -> :sswitch_2
        0x6b -> :sswitch_2
        0x6c -> :sswitch_2
        0x6d -> :sswitch_2
        0x6e -> :sswitch_2
        0x6f -> :sswitch_2
        0x70 -> :sswitch_2
        0x71 -> :sswitch_2
        0x72 -> :sswitch_2
        0x73 -> :sswitch_2
        0x74 -> :sswitch_2
        0x75 -> :sswitch_2
        0x76 -> :sswitch_2
        0x77 -> :sswitch_2
        0x78 -> :sswitch_2
        0x79 -> :sswitch_2
        0x7a -> :sswitch_2
        0x7b -> :sswitch_2
        0x7c -> :sswitch_2
        0x7d -> :sswitch_2
        0x7e -> :sswitch_2
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xca -> :sswitch_2
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x130 -> :sswitch_2
        0x131 -> :sswitch_2
        0x132 -> :sswitch_2
        0x140 -> :sswitch_2
        0x141 -> :sswitch_2
        0x142 -> :sswitch_2
        0x143 -> :sswitch_2
        0x144 -> :sswitch_2
        0x14a -> :sswitch_2
        0x14b -> :sswitch_2
        0x14c -> :sswitch_2
        0x14f -> :sswitch_2
        0x150 -> :sswitch_2
        0x151 -> :sswitch_2
        0x154 -> :sswitch_2
        0x155 -> :sswitch_2
        0x156 -> :sswitch_2
        0x15e -> :sswitch_2
        0x15f -> :sswitch_2
        0x160 -> :sswitch_2
        0x161 -> :sswitch_2
        0x162 -> :sswitch_2
        0x163 -> :sswitch_2
        0x164 -> :sswitch_2
        0x165 -> :sswitch_2
        0x168 -> :sswitch_2
        0x169 -> :sswitch_2
        0x16a -> :sswitch_2
        0x16b -> :sswitch_2
        0x16c -> :sswitch_2
        0x16d -> :sswitch_2
        0x16e -> :sswitch_2
        0x17c -> :sswitch_2
        0x17d -> :sswitch_2
        0x17e -> :sswitch_2
        0x17f -> :sswitch_2
        0x180 -> :sswitch_2
        0x190 -> :sswitch_2
        0x191 -> :sswitch_2
        0x192 -> :sswitch_2
        0x193 -> :sswitch_2
        0x194 -> :sswitch_2
        0x196 -> :sswitch_2
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_2
        0x1f6 -> :sswitch_2
        0x1f7 -> :sswitch_2
        0x1f8 -> :sswitch_2
        0x1f9 -> :sswitch_2
        0x1fa -> :sswitch_2
        0x1fb -> :sswitch_2
        0x1fc -> :sswitch_2
        0x1fd -> :sswitch_2
        0x1fe -> :sswitch_2
        0x1ff -> :sswitch_2
        0x200 -> :sswitch_2
        0x201 -> :sswitch_2
        0x202 -> :sswitch_2
        0x203 -> :sswitch_2
        0x204 -> :sswitch_2
        0x205 -> :sswitch_2
        0x206 -> :sswitch_2
        0x258 -> :sswitch_2
        0x259 -> :sswitch_2
        0x2bc -> :sswitch_2
        0x2bd -> :sswitch_2
        0x2be -> :sswitch_2
        0x2c6 -> :sswitch_2
        0x2c7 -> :sswitch_2
        0x2c8 -> :sswitch_2
        0x2c9 -> :sswitch_2
        0x2d0 -> :sswitch_2
        0x2d1 -> :sswitch_2
        0x2d2 -> :sswitch_2
        0x2d3 -> :sswitch_2
        0x302 -> :sswitch_2
        0x320 -> :sswitch_2
        0x321 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2200
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 2495
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    if-eqz v0, :cond_1

    .line 2496
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2498
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2499
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2501
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2502
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2504
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    if-eqz v0, :cond_7

    .line 2505
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2507
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2508
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2510
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2511
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 2513
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    if-nez v0, :cond_c

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    if-eqz v0, :cond_d

    .line 2514
    :cond_c
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2516
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    if-eqz v0, :cond_f

    .line 2517
    :cond_e
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2519
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    if-eqz v0, :cond_11

    .line 2520
    :cond_10
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2522
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    if-eqz v0, :cond_12

    .line 2523
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2525
    :cond_12
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    if-eqz v0, :cond_13

    .line 2526
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2528
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    if-nez v0, :cond_14

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 2529
    :cond_14
    const/16 v0, 0xc

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2531
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    if-nez v0, :cond_16

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_17

    .line 2532
    :cond_16
    const/16 v0, 0xd

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2534
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    if-eqz v0, :cond_18

    .line 2535
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2537
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    if-nez v0, :cond_19

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    if-eqz v0, :cond_1a

    .line 2538
    :cond_19
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2540
    :cond_1a
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasHost:Z

    if-nez v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 2541
    :cond_1b
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2543
    :cond_1c
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    if-eqz v0, :cond_1d

    .line 2544
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2546
    :cond_1d
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    if-eqz v0, :cond_1e

    .line 2547
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2549
    :cond_1e
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAttempts:Z

    if-nez v0, :cond_1f

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    if-eqz v0, :cond_20

    .line 2550
    :cond_1f
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2552
    :cond_20
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferCheckoutFlowRequired:Z

    if-nez v0, :cond_21

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    if-eqz v0, :cond_22

    .line 2553
    :cond_21
    const/16 v0, 0x14

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2555
    :cond_22
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    if-eqz v0, :cond_23

    .line 2556
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2558
    :cond_23
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasCallingPackage:Z

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 2559
    :cond_24
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2561
    :cond_25
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    if-eqz v0, :cond_26

    .line 2562
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2564
    :cond_26
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasLastUrl:Z

    if-nez v0, :cond_27

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 2565
    :cond_27
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->lastUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2567
    :cond_28
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    if-eqz v0, :cond_29

    .line 2568
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2570
    :cond_29
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    if-eqz v0, :cond_2a

    .line 2571
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->deviceFeature:Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2573
    :cond_2a
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    if-eqz v0, :cond_2b

    .line 2574
    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->rpcReport:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2576
    :cond_2b
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    if-eqz v0, :cond_2c

    .line 2577
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2579
    :cond_2c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2580
    return-void
.end method
