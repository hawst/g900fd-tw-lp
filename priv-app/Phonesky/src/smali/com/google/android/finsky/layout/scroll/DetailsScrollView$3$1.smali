.class Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;
.super Ljava/lang/Object;
.source "DetailsScrollView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->onAnimationEnd(Landroid/view/animation/Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;->this$1:Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(II)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;->this$1:Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;

    iget-object v0, v0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;->this$1:Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;

    iget-object v1, v1, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    invoke-static {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$300(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->cancelScrollAnimationIfWaiting(Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$400(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3$1;->this$1:Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;

    iget-object v0, v0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->removeOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 143
    return-void
.end method
