.class public final Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseStatusResponse"
.end annotation


# instance fields
.field public appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

.field public briefMessage:Ljava/lang/String;

.field public hasBriefMessage:Z

.field public hasInfoUrl:Z

.field public hasStatus:Z

.field public hasStatusMsg:Z

.field public hasStatusTitle:Z

.field public infoUrl:Ljava/lang/String;

.field public libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field public status:I

.field public statusMsg:Ljava/lang/String;

.field public statusTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1900
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1901
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->clear()Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    .line 1902
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1905
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    .line 1906
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatus:Z

    .line 1907
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    .line 1908
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusMsg:Z

    .line 1909
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    .line 1910
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusTitle:Z

    .line 1911
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    .line 1912
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasBriefMessage:Z

    .line 1913
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    .line 1914
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasInfoUrl:Z

    .line 1915
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1916
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 1917
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 1918
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->cachedSize:I

    .line 1919
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1954
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1955
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatus:Z

    if-eqz v1, :cond_1

    .line 1956
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1959
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusMsg:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1960
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1963
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusTitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1964
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1967
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasBriefMessage:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1968
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1971
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasInfoUrl:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1972
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1975
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_a

    .line 1976
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1979
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v1, :cond_b

    .line 1980
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1983
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v1, :cond_c

    .line 1984
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1987
    :cond_c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1995
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1996
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2000
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2001
    :sswitch_0
    return-object p0

    .line 2006
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2007
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2012
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    .line 2013
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatus:Z

    goto :goto_0

    .line 2019
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    .line 2020
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusMsg:Z

    goto :goto_0

    .line 2024
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    .line 2025
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusTitle:Z

    goto :goto_0

    .line 2029
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    .line 2030
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasBriefMessage:Z

    goto :goto_0

    .line 2034
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    .line 2035
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasInfoUrl:Z

    goto :goto_0

    .line 2039
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v2, :cond_1

    .line 2040
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 2042
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2046
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v2, :cond_2

    .line 2047
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 2049
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2053
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v2, :cond_3

    .line 2054
    new-instance v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 2056
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1996
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    .line 2007
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1848
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1925
    iget v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatus:Z

    if-eqz v0, :cond_1

    .line 1926
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1928
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusMsg:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1929
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusMsg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1931
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasStatusTitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1932
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->statusTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1934
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasBriefMessage:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1935
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->briefMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1937
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->hasInfoUrl:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1938
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->infoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1940
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v0, :cond_a

    .line 1941
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1943
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v0, :cond_b

    .line 1944
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->rejectedInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1946
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v0, :cond_c

    .line 1947
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1949
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1950
    return-void
.end method
