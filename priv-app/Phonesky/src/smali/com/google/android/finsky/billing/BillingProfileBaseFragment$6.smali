.class Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;
.super Ljava/lang/Object;
.source "BillingProfileBaseFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->billingProfileOptionToActionEntry(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;[B)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

.field final synthetic val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

.field final synthetic val$paymentsIntegratorCommonToken:[B


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/BillingProfileBaseFragment;Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;[B)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->this$0:Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    iput-object p3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$paymentsIntegratorCommonToken:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 448
    const-string v2, "CREDIT_CARD"

    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    iget-object v3, v3, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 449
    const/16 v0, 0x32a

    .line 451
    .local v0, "clickType":I
    const/16 v1, 0x8

    .line 461
    .local v1, "requestCode":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->this$0:Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    iget-object v2, v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->this$0:Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 465
    iget-object v2, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->this$0:Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$paymentsIntegratorCommonToken:[B

    iget-object v4, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    iget-object v4, v4, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->addInstrumentManager([B[BI)V

    .line 467
    return-void

    .line 452
    .end local v0    # "clickType":I
    .end local v1    # "requestCode":I
    :cond_0
    const-string v2, "PAYPAL"

    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    iget-object v3, v3, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 453
    const/16 v0, 0x32e

    .line 455
    .restart local v0    # "clickType":I
    const/16 v1, 0x9

    .restart local v1    # "requestCode":I
    goto :goto_0

    .line 457
    .end local v0    # "clickType":I
    .end local v1    # "requestCode":I
    :cond_1
    const-string v2, "Unexpected typeName=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$6;->val$option:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    iget-object v5, v5, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 458
    const/4 v0, -0x1

    .line 459
    .restart local v0    # "clickType":I
    const/4 v1, 0x7

    .restart local v1    # "requestCode":I
    goto :goto_0
.end method
