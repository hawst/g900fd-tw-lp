.class public Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;
.super Lcom/google/android/finsky/fragments/LoggingDialogFragment;
.source "VerifyAssociationDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingDialogFragment;-><init>()V

    .line 63
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;-><init>()V

    .line 26
    .local v1, "dialog":Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->setCancelable(Z)V

    .line 27
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v1
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x34a

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;->onVerifyCancel()V

    .line 56
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 35
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 36
    .local v0, "dialog":Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 38
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 39
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 40
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 41
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 42
    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 44
    return-object v0
.end method

.method public setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    .line 49
    return-void
.end method
