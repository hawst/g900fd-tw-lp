.class Lcom/google/android/finsky/setup/PlaySetupService$3;
.super Ljava/lang/Object;
.source "PlaySetupService.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/PlaySetupService;->doCancelEarlyUpdate()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/PlaySetupService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/finsky/setup/PlaySetupService$3;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 452
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService$3;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # getter for: Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/setup/PlaySetupService;->access$1000(Lcom/google/android/finsky/setup/PlaySetupService;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 453
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 464
    :goto_0
    return-object v2

    .line 456
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    .line 457
    .local v1, "installer":Lcom/google/android/finsky/receivers/Installer;
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService$3;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # getter for: Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/setup/PlaySetupService;->access$1000(Lcom/google/android/finsky/setup/PlaySetupService;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/finsky/receivers/Installer;->cancel(Ljava/lang/String;)V

    .line 459
    iget-object v2, p0, Lcom/google/android/finsky/setup/PlaySetupService$3;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # getter for: Lcom/google/android/finsky/setup/PlaySetupService;->mEarlyUpdatePackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/setup/PlaySetupService;->access$1000(Lcom/google/android/finsky/setup/PlaySetupService;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v0

    .line 460
    .local v0, "afterCancelState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 462
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 464
    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/PlaySetupService$3;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
