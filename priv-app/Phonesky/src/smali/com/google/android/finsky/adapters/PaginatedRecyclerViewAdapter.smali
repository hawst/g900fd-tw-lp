.class public abstract Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PaginatedRecyclerViewAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

.field protected final mLayoutInflater:Landroid/view/LayoutInflater;

.field protected final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field protected mRetryClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "isRequestInErrorState"    # Z
    .param p4, "isRequestLoading"    # Z

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;-><init>(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;)V

    iput-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mRetryClickListener:Landroid/view/View$OnClickListener;

    .line 79
    iput-object p1, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mContext:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 81
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 82
    if-eqz p3, :cond_0

    .line 83
    sget-object v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    iput-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    .line 89
    :goto_0
    return-void

    .line 84
    :cond_0
    if-eqz p4, :cond_1

    .line 85
    sget-object v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    iput-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    iput-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;)Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;
    .param p1, "x1"    # Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->setFooterMode(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V

    return-void
.end method

.method private setFooterMode(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 135
    return-void
.end method


# virtual methods
.method protected bindErrorFooterView(Landroid/view/View;)Landroid/view/View;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 117
    check-cast p1, Lcom/google/android/finsky/layout/ErrorFooter;

    .end local p1    # "view":Landroid/view/View;
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/ErrorFooter;

    .line 118
    .local v0, "errorFooter":Lcom/google/android/finsky/layout/ErrorFooter;
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->getLastRequestError()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mRetryClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/ErrorFooter;->configure(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 119
    const-string v1, "error_footer"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ErrorFooter;->setIdentifier(Ljava/lang/String;)V

    .line 120
    return-object v0
.end method

.method protected bindLoadingFooterView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 102
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;

    .line 103
    .local v0, "layout":Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
    const-string v1, "loading_footer"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;->setIdentifier(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method protected createErrorFooterView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 110
    const v0, 0x7f040093

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected createLoadingFooterView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 95
    const v0, 0x7f0400d3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 125
    int-to-long v0, p1

    return-wide v0
.end method

.method protected abstract getLastRequestError()Ljava/lang/String;
.end method

.method protected inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1
    .param p1, "resource"    # I
    .param p2, "root"    # Landroid/view/ViewGroup;
    .param p3, "attachToRoot"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract isMoreDataAvailable()Z
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->isMoreDataAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->setFooterMode(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->setFooterMode(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V

    goto :goto_0
.end method

.method protected abstract retryLoadingItems()V
.end method
