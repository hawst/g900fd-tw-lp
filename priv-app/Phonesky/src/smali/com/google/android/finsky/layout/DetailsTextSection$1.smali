.class Lcom/google/android/finsky/layout/DetailsTextSection$1;
.super Ljava/lang/Object;
.source "DetailsTextSection.java"

# interfaces
.implements Lcom/google/android/play/utils/UrlSpanUtils$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsTextSection;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsTextSection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsTextSection;)V
    .locals 0

    .prologue
    .line 630
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsTextSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsTextSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 633
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsTextSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsTextSection;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/finsky/layout/DetailsTextSection;->mUrlSpanClicked:Z

    .line 635
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 636
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 637
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 640
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 641
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 644
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsTextSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsTextSection;

    # getter for: Lcom/google/android/finsky/layout/DetailsTextSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v2}, Lcom/google/android/finsky/layout/DetailsTextSection;->access$000(Lcom/google/android/finsky/layout/DetailsTextSection;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v2

    invoke-virtual {v2, p2, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->handleDeepLink(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :goto_0
    return-void

    .line 650
    :cond_0
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 651
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
