.class final Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;
.super Ljava/lang/Object;
.source "GetSelfUpdateHelper.java"

# interfaces
.implements Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$allowRetry:Z

.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;


# direct methods
.method constructor <init>(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$allowRetry:Z

    iput-object p2, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p3, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 107
    const-string v0, "Upload device configuration failed - try selfupdate anyway"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$allowRetry:Z

    iget-object v1, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    # invokes: Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->access$000(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    .line 109
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$allowRetry:Z

    iget-object v1, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;->val$listener:Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    # invokes: Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->access$000(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    .line 103
    return-void
.end method
