.class public Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;
.super Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;
.source "CardRecyclerViewAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;,
        Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected final mCardContentPadding:I

.field private final mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

.field private final mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

.field private final mColumnCount:I

.field private final mContainerId:Ljava/lang/String;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mExtraLeadingSpacerHeight:I

.field private final mHasBannerHeader:Z

.field private mHasFilters:Z

.field private final mHasPlainHeader:Z

.field private final mHasSocialHeader:Z

.field private final mIsOrdered:Z

.field public mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mLeadingSpacerHeight:I

.field private final mLooseDocsWithReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private final mLooseItemCellId:I

.field private final mLooseItemColCount:I

.field private final mNumQuickLinkRows:I

.field private final mNumQuickLinksPerRow:I

.field protected final mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field private final mShowLooseItemReasons:Z

.field private final mTitle:Ljava/lang/String;

.field private final mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private final mUseMiniCards:Z

.field private final mUseMiniCardsForLooseItems:Z

.field private final mUseTallTemplates:Z

.field private final mWarmWelcomeCardColumns:I

.field private final mWarmWelcomeHideGraphic:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p7, "containerList"    # Lcom/google/android/finsky/api/model/ContainerList;
    .param p8, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p9, "title"    # Ljava/lang/String;
    .param p10, "isRestoring"    # Z
    .param p11, "showLooseItemReasons"    # Z
    .param p12, "tabMode"    # I
    .param p13, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 319
    move-object/from16 v0, p7

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 297
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 320
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 321
    .local v5, "res":Landroid/content/res/Resources;
    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 322
    iput-object p4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 323
    iput-object p5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 324
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 325
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mTitle:Ljava/lang/String;

    .line 326
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v5, v8, v9}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    .line 327
    new-instance v7, Lcom/google/android/finsky/layout/play/PlayCardHeap;

    invoke-direct {v7}, Lcom/google/android/finsky/layout/play/PlayCardHeap;-><init>()V

    iput-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    .line 328
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v6, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 329
    .local v6, "screenHeightPx":I
    const v7, 0x7f0b0139

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    if-le v6, v7, :cond_2

    const/4 v7, 0x1

    :goto_0
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    .line 331
    const v7, 0x7f0f000c

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseMiniCards:Z

    .line 333
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    .line 335
    if-eqz p8, :cond_3

    move-object/from16 v0, p8

    array-length v4, v0

    .line 336
    .local v4, "quickLinkCount":I
    :goto_1
    const/4 v7, 0x0

    invoke-static {v5, v4, v7}, Lcom/google/android/finsky/utils/UiUtils;->getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinksPerRow:I

    .line 337
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 338
    iget v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinksPerRow:I

    invoke-static {v4, v7}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinkRows:I

    .line 340
    const v7, 0x7f0e0011

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mWarmWelcomeCardColumns:I

    .line 342
    iget v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mWarmWelcomeCardColumns:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_4

    const v7, 0x7f0f000e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v7, :cond_4

    const/4 v7, 0x1

    :goto_2
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mWarmWelcomeHideGraphic:Z

    .line 345
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 347
    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    .line 350
    .local v2, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->isOrdered()Z

    move-result v7

    :goto_3
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mIsOrdered:Z

    .line 351
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasContainerViews()Z

    move-result v7

    :goto_4
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasFilters:Z

    .line 352
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    :goto_5
    iput-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerId:Ljava/lang/String;

    .line 354
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mShowLooseItemReasons:Z

    .line 357
    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseMiniCards:Z

    if-eqz v7, :cond_8

    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mShowLooseItemReasons:Z

    if-nez v7, :cond_8

    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mIsOrdered:Z

    if-nez v7, :cond_8

    const/4 v7, 0x1

    :goto_6
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseMiniCardsForLooseItems:Z

    .line 361
    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v7

    const/16 v8, 0x9

    if-ne v7, v8, :cond_9

    const/4 v3, 0x1

    .line 362
    .local v3, "isPeopleList":Z
    :goto_7
    if-eqz v3, :cond_a

    .line 363
    const v7, 0x7f04012d

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemCellId:I

    .line 366
    iget v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v7, v8}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    .line 379
    :goto_8
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    .line 381
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasContainerWithBannerTemplate()Z

    move-result v7

    :goto_9
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasBannerHeader:Z

    .line 383
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasRecommendationsContainerWithHeaderTemplate()Z

    move-result v7

    :goto_a
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasSocialHeader:Z

    .line 386
    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasBannerHeader:Z

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasSocialHeader:Z

    if-eqz v7, :cond_f

    .line 387
    :cond_0
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasPlainHeader:Z

    .line 392
    :goto_b
    const/4 v7, 0x0

    move/from16 v0, p12

    invoke-static {p1, v0, v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLeadingSpacerHeight:I

    .line 393
    const v7, 0x7f0b0176

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mExtraLeadingSpacerHeight:I

    .line 396
    if-nez p10, :cond_1

    .line 398
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->syncItemEntries()V

    .line 406
    :cond_1
    new-instance v7, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)V

    iput-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .line 413
    return-void

    .line 329
    .end local v2    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "isPeopleList":Z
    .end local v4    # "quickLinkCount":I
    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 335
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 342
    .restart local v4    # "quickLinkCount":I
    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 350
    .restart local v2    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 351
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 352
    :cond_7
    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/ContainerList;->getListPageUrls()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    goto/16 :goto_5

    .line 357
    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 361
    :cond_9
    const/4 v3, 0x0

    goto :goto_7

    .line 368
    .restart local v3    # "isPeopleList":Z
    :cond_a
    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mShowLooseItemReasons:Z

    if-eqz v7, :cond_b

    .line 369
    const v7, 0x7f040136

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemCellId:I

    .line 370
    iget v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    goto :goto_8

    .line 371
    :cond_b
    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseMiniCardsForLooseItems:Z

    if-eqz v7, :cond_c

    .line 372
    const v7, 0x7f040128

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemCellId:I

    .line 373
    const v7, 0x7f0e000a

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    goto/16 :goto_8

    .line 375
    :cond_c
    const v7, 0x7f040124

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemCellId:I

    .line 376
    invoke-static {v5}, Lcom/google/android/finsky/utils/UiUtils;->getRegularGridColumnCount(Landroid/content/res/Resources;)I

    move-result v7

    iput v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    goto/16 :goto_8

    .line 381
    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_9

    .line 383
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_a

    .line 389
    :cond_f
    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mTitle:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_10

    const/4 v7, 0x1

    :goto_c
    iput-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasPlainHeader:Z

    goto/16 :goto_b

    :cond_10
    const/4 v7, 0x0

    goto :goto_c
.end method

.method static synthetic access$400(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/utils/ClientMutationCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    return-object v0
.end method

.method private bindActionBannerCluster(ILandroid/view/View;)V
    .locals 13
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 1419
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;

    .line 1422
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 1427
    .local v3, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v2, v4}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v12

    .line 1430
    .local v12, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getActionBanner()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    move-result-object v10

    .line 1431
    .local v10, "actionBanner":Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    iget-object v11, v10, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 1432
    .local v11, "actions":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    array-length v2, v11

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v9, v11, v2

    .line 1433
    .local v9, "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :goto_0
    invoke-direct {p0, v3, v12, v0, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1434
    if-eqz v9, :cond_1

    iget-object v6, v9, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    .line 1435
    .local v6, "buttonText":Ljava/lang/String;
    :goto_1
    new-instance v8, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;

    invoke-direct {v8, p0, v0, v9}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;)V

    .line 1447
    .local v8, "exploreClickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, v10, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v5, v10, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object v7, v0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->configureExtraContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/view/View$OnClickListener;)V

    .line 1450
    return-void

    .end local v6    # "buttonText":Ljava/lang/String;
    .end local v8    # "exploreClickListener":Landroid/view/View$OnClickListener;
    .end local v9    # "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_0
    move-object v9, v1

    .line 1432
    goto :goto_0

    .restart local v9    # "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_1
    move-object v6, v1

    .line 1434
    goto :goto_1
.end method

.method private bindAddToCirclesCluster(ILandroid/view/View;)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1376
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;

    .line 1378
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1379
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v6, v7}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1381
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1382
    .local v1, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1384
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;->getCardChildCount()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 1385
    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .line 1386
    .local v5, "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    .line 1384
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1388
    .end local v5    # "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    :cond_0
    return-void
.end method

.method private bindBannerHeaderView(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1068
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/DocImageView;

    .line 1069
    .local v0, "bannerImage":Lcom/google/android/finsky/layout/DocImageView;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 1070
    .local v1, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getContainerWithBannerTemplate()Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    move-result-object v2

    .line 1072
    .local v2, "template":Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1073
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DocImageView;->setBackgroundColor(I)V

    .line 1075
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/16 v6, 0x9

    aput v6, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 1076
    return-void
.end method

.method private bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V
    .locals 16
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p3, "cluster"    # Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .param p4, "clusterClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1559
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1560
    if-eqz p4, :cond_1

    invoke-direct/range {p0 .. p2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getMoreResultsStringForCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;)Ljava/lang/String;

    move-result-object v6

    .line 1562
    .local v6, "moreString":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    move-object/from16 v2, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1569
    .end local v6    # "moreString":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object/from16 v8, p2

    invoke-virtual/range {v7 .. v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1573
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setCardContentHorizontalPadding(I)V

    .line 1575
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1576
    return-void

    .line 1560
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private bindContainerFilterView(Landroid/view/View;)V
    .locals 3
    .param p1, "vew"    # Landroid/view/View;

    .prologue
    .line 1697
    const v2, 0x7f0a0311

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 1698
    .local v1, "spinner":Landroid/widget/Spinner;
    const v2, 0x7f0a0312

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1699
    .local v0, "corpusHeaderStrip":Landroid/view/View;
    check-cast p1, Lcom/google/android/finsky/layout/play/Identifiable;

    .end local p1    # "vew":Landroid/view/View;
    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V

    .line 1700
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1701
    return-void
.end method

.method private bindEmptyCluster(ILandroid/view/View;)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1473
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;

    .line 1474
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 1475
    .local v7, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v3

    iget v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1477
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v7, v1}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->createContent(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1478
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1479
    return-void
.end method

.method private bindExtraLeadingSpacer(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1057
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mExtraLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1058
    return-void
.end method

.method private bindGenericCluster(ILandroid/view/View;)V
    .locals 5
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1367
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .line 1369
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1370
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getGenericClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1371
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1372
    .local v1, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1373
    return-void
.end method

.method private bindLeadingSpacer(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1052
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1053
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 1054
    return-void
.end method

.method private bindLooseItem(Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Z)V
    .locals 11
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "trueIndex"    # I
    .param p3, "docEntry"    # Landroid/view/View;
    .param p4, "bindNoDoc"    # Z

    .prologue
    .line 1145
    move-object v0, p3

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 1147
    .local v0, "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-nez p1, :cond_3

    .line 1149
    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    if-ge p2, v1, :cond_2

    .line 1150
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 1151
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 1168
    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_1
    :goto_0
    return-void

    .line 1153
    .restart local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->clearCardState()V

    goto :goto_0

    .line 1156
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->isDismissed(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v5

    .line 1157
    .local v5, "isDismissed":Z
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v8, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mIsOrdered:Z

    if-eqz v1, :cond_4

    move v9, p2

    :goto_1
    move-object v1, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    .line 1163
    instance-of v1, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    if-eqz v1, :cond_1

    .line 1164
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMyCirclesContainer()Z

    move-result v10

    .line 1165
    .local v10, "isMyCircles":Z
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-nez v10, :cond_5

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    goto :goto_0

    .line 1157
    .end local v10    # "isMyCircles":Z
    .restart local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_4
    const/4 v9, -0x1

    goto :goto_1

    .line 1165
    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    .restart local v10    # "isMyCircles":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private bindLooseItemRow(IILandroid/view/View;)V
    .locals 1
    .param p1, "mTrueStartIndex"    # I
    .param p2, "mTrueEndIndex"    # I
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 1096
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mShowLooseItemReasons:Z

    if-eqz v0, :cond_0

    .line 1097
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindRowOfLooseItemsWithReasons(IILandroid/view/View;)V

    .line 1101
    :goto_0
    return-void

    .line 1099
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindRowOfLooseItemsWithoutReasons(IILandroid/view/View;)V

    goto :goto_0
.end method

.method private bindMerchBanner(ILandroid/view/View;)V
    .locals 8
    .param p1, "trueStartIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1685
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;

    .line 1686
    .local v0, "banner":Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v2, v4}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->init(II)V

    .line 1687
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 1688
    .local v7, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setIdentifier(Ljava/lang/String;)V

    .line 1689
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getNextBannerInfo()Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    move-result-object v1

    .line 1690
    .local v1, "nextBanner":Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    const/16 v2, 0xe

    invoke-virtual {v7, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    .line 1691
    .local v3, "bannerImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v4, v7, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->configureMerch(Lcom/google/android/finsky/protos/DocumentV2$NextBanner;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V

    .line 1694
    return-void
.end method

.method private bindMerchCluster(ILandroid/view/View;)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 1622
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    .line 1623
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Document;

    .line 1624
    .local v6, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v6}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getMerchClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v7

    .line 1625
    .local v7, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v6, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 1626
    .local v5, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v6, v7, v0, v5}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1627
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/16 v3, 0xe

    invoke-virtual {v6, v3}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1630
    return-void
.end method

.method private bindMyCirclesCluster(ILandroid/view/View;)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1391
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;

    .line 1393
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1394
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v6, v7}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1397
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1398
    .local v1, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1400
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;->getCardChildCount()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 1401
    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .line 1402
    .local v5, "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    .line 1400
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1404
    .end local v5    # "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    :cond_0
    return-void
.end method

.method private bindOrderedCluster(ILandroid/view/View;)V
    .locals 7
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1043
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;

    .line 1045
    .local v0, "clusterView":Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1046
    .local v1, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1047
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getChildren()[Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->bind(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1049
    return-void
.end method

.method private bindPlainHeaderView(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 1061
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 1062
    .local v0, "plainHeader":Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mTitle:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1063
    iget v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setExtraHorizontalPadding(I)V

    .line 1064
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "plain_header:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setIdentifier(Ljava/lang/String;)V

    .line 1065
    return-void
.end method

.method private bindQuickSuggestionsCluster(ILandroid/view/View;)V
    .locals 6
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1462
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    .line 1463
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1464
    .local v1, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V

    .line 1465
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v3, v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    .line 1469
    .local v2, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1470
    return-void
.end method

.method private bindRateCluster(ILandroid/view/View;)V
    .locals 4
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1453
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    .line 1454
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1455
    .local v1, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V

    .line 1456
    iget v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    invoke-static {v3}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterRepository;->getMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    .line 1458
    .local v2, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1459
    return-void
.end method

.method private bindRowOfLooseItemsWithReasons(IILandroid/view/View;)V
    .locals 16
    .param p1, "trueStartIndex"    # I
    .param p2, "trueEndIndex"    # I
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 1336
    move-object/from16 v11, p3

    check-cast v11, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .line 1337
    .local v11, "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1338
    const/4 v13, 0x0

    .line 1339
    .local v13, "firstNonNull":Lcom/google/android/finsky/api/model/Document;
    move/from16 v14, p1

    .local v14, "i":I
    :goto_0
    move/from16 v0, p2

    if-gt v14, v0, :cond_2

    .line 1340
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, v14}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/api/model/Document;

    .line 1341
    .local v10, "child":Lcom/google/android/finsky/api/model/Document;
    if-eqz v10, :cond_1

    .line 1342
    if-nez v13, :cond_0

    .line 1343
    move-object v13, v10

    .line 1345
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339
    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 1350
    .end local v10    # "child":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v12

    .line 1351
    .local v12, "documentType":I
    :goto_1
    if-eqz v13, :cond_4

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v11, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1353
    const/16 v1, 0x1c

    if-ne v12, v1, :cond_5

    const/4 v15, 0x1

    .line 1354
    .local v15, "isPersonCluster":Z
    :goto_3
    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    .line 1359
    .local v2, "looseClusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerId:Ljava/lang/String;

    invoke-virtual {v11, v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->withLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1362
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    invoke-virtual {v11, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setCardContentHorizontalPadding(I)V

    .line 1363
    invoke-virtual {v11}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hideHeader()V

    .line 1364
    return-void

    .line 1350
    .end local v2    # "looseClusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v12    # "documentType":I
    .end local v15    # "isPersonCluster":Z
    :cond_3
    const/4 v12, -0x1

    goto :goto_1

    .line 1351
    .restart local v12    # "documentType":I
    :cond_4
    const-string v1, ""

    goto :goto_2

    .line 1353
    :cond_5
    const/4 v15, 0x0

    goto :goto_3

    .line 1354
    .restart local v15    # "isPersonCluster":Z
    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    const/4 v4, 0x0

    invoke-static {v12, v1, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->getMetadata(IIZI)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    goto :goto_4
.end method

.method private bindRowOfLooseItemsWithoutReasons(IILandroid/view/View;)V
    .locals 7
    .param p1, "trueStartIndex"    # I
    .param p2, "trueEndIndex"    # I
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 1118
    move-object v0, p3

    check-cast v0, Lcom/google/android/finsky/layout/BucketRow;

    .line 1119
    .local v0, "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    const/4 v2, 0x0

    .line 1120
    .local v2, "firstDocId":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    if-ge v3, v6, :cond_3

    .line 1121
    add-int v5, p1, v3

    .line 1124
    .local v5, "trueIndex":I
    if-le v5, p2, :cond_1

    const/4 v4, 0x1

    .line 1125
    .local v4, "pastBounds":Z
    :goto_1
    if-eqz v4, :cond_2

    const/4 v1, 0x0

    .line 1126
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v1, :cond_0

    .line 1127
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    .line 1129
    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/BucketRow;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-direct {p0, v1, v5, v6, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindLooseItem(Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Z)V

    .line 1120
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1124
    .end local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "pastBounds":Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1125
    .restart local v4    # "pastBounds":Z
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6, v5}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Document;

    move-object v1, v6

    goto :goto_2

    .line 1131
    .end local v4    # "pastBounds":Z
    .end local v5    # "trueIndex":I
    :cond_3
    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/BucketRow;->setIdentifier(Ljava/lang/String;)V

    .line 1132
    return-void
.end method

.method private bindSingleDocCluster(ILandroid/view/View;Z)V
    .locals 18
    .param p1, "trueStartIndex"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "showActionButton"    # Z

    .prologue
    .line 1633
    move-object/from16 v13, p2

    check-cast v13, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    .line 1634
    .local v13, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/api/model/Document;

    .line 1635
    .local v14, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v12

    .line 1636
    .local v12, "child":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1641
    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/finsky/api/model/Document;->setDescription(Ljava/lang/String;)V

    .line 1643
    if-eqz p3, :cond_1

    const/16 v17, 0x1a1

    .line 1646
    .local v17, "uiElementType":I
    :goto_0
    new-instance v15, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v15, v0, v1, v14}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$5;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;ILcom/google/android/finsky/api/model/Document;)V

    .line 1657
    .local v15, "headerClickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    move/from16 v0, p3

    invoke-static {v2, v4, v0}, Lcom/google/android/finsky/layout/play/PlayCardSingleCardClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardSingleCardClusterMetadata;

    move-result-object v3

    .line 1663
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1667
    const/16 v2, 0xe

    invoke-virtual {v14, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v16

    .line 1668
    .local v16, "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    const/4 v4, 0x2

    if-le v2, v4, :cond_2

    if-eqz v16, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1669
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v6, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    move-object v4, v13

    move-object v9, v15

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1675
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v11

    .line 1676
    .local v11, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    if-eqz p3, :cond_0

    .line 1677
    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {v12, v2, v11, v13, v4}, Lcom/google/android/finsky/utils/PlayCardUtils;->updatePrimaryActionButton(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 1680
    :cond_0
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    move-object v4, v13

    move-object v9, v15

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1682
    return-void

    .line 1643
    .end local v3    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v11    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v15    # "headerClickListener":Landroid/view/View$OnClickListener;
    .end local v16    # "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .end local v17    # "uiElementType":I
    :cond_1
    const/16 v17, 0x197

    goto/16 :goto_0

    .line 1673
    .restart local v3    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local v15    # "headerClickListener":Landroid/view/View$OnClickListener;
    .restart local v16    # "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .restart local v17    # "uiElementType":I
    :cond_2
    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureNoMerch()V

    goto :goto_1
.end method

.method private bindSocialHeaderView(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1079
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;

    .line 1080
    .local v0, "header":Lcom/google/android/finsky/layout/play/PlaySocialHeader;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    .line 1081
    .local v6, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getRecommendationsContainerWithHeaderTemplate()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    move-result-object v7

    .line 1084
    .local v7, "template":Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    const/16 v4, 0xe

    invoke-virtual {v6, v4}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Image;

    .line 1085
    .local v1, "cover":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, v7, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1086
    .local v2, "primaryPerson":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v3, v7, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1087
    .local v3, "secondaryPersons":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->bind(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1088
    return-void
.end method

.method private bindTrustedSourceCluster(ILandroid/view/View;)V
    .locals 7
    .param p1, "startIndex"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1407
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;

    .line 1409
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1410
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1412
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1413
    .local v1, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1414
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTrustedSourcePersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->configurePersonProfile(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1416
    return-void
.end method

.method private bindWarmWelcomeCardView(ILandroid/view/View;)V
    .locals 22
    .param p1, "index"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1482
    move-object/from16 v2, p2

    check-cast v2, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    .line 1483
    .local v2, "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/api/model/Document;

    .line 1484
    .local v12, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getWarmWelcome()Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    move-result-object v21

    .line 1485
    .local v21, "warmWelcomeTemplate":Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    const/4 v3, 0x4

    invoke-virtual {v12, v3}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v13

    .line 1486
    .local v13, "docImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v20, 0x0

    .line 1487
    .local v20, "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    :goto_0
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mWarmWelcomeHideGraphic:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V

    .line 1490
    const/4 v11, 0x0

    .line 1491
    .local v11, "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    const/16 v19, 0x0

    .line 1493
    .local v19, "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .local v9, "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    array-length v0, v9

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_3

    aget-object v10, v9, v17

    .line 1494
    .local v10, "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    iget v3, v10, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1495
    move-object v11, v10

    .line 1493
    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 1486
    .end local v9    # "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v11    # "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v17    # "i$":I
    .end local v18    # "len$":I
    .end local v19    # "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v20    # "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    move-object/from16 v20, v3

    goto :goto_0

    .restart local v20    # "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    :cond_1
    move-object/from16 v5, v20

    .line 1487
    goto :goto_1

    .line 1497
    .restart local v9    # "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v11    # "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v17    # "i$":I
    .restart local v18    # "len$":I
    .restart local v19    # "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_2
    move-object/from16 v19, v10

    goto :goto_3

    .line 1500
    .end local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_3
    move-object v14, v11

    .line 1501
    .local v14, "finalDismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v15, v19

    .line 1502
    .local v15, "finalSecondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v16, v2

    .line 1503
    .local v16, "finalWarmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    iget-object v3, v14, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    iget-object v4, v14, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    new-instance v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v14, v12}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$3;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/model/Document;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureDismissAction(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 1531
    if-nez v15, :cond_4

    .line 1532
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->hideSecondaryAction()V

    .line 1549
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setPadding(IIII)V

    .line 1550
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setIdentifier(Ljava/lang/String;)V

    .line 1551
    return-void

    .line 1534
    :cond_4
    iget-object v3, v15, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    iget-object v4, v15, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    new-instance v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v15}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$4;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->showSecondaryButton(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method

.method private createOrderedCluster(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1034
    const v2, 0x7f04012c

    const/4 v3, 0x0

    invoke-virtual {p0, v2, p1, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;

    .line 1036
    .local v0, "clusterView":Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1037
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v1}, Lcom/google/android/finsky/utils/UiUtils;->getRegularGridColumnCount(Landroid/content/res/Resources;)I

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->inflateGrid(III)V

    .line 1039
    return-object v0
.end method

.method private createRowOfLooseItemsWithoutReasons(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 1106
    const v2, 0x7f040044

    invoke-virtual {p0, v2, p1, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/BucketRow;

    .line 1107
    .local v0, "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/BucketRow;->setContentHorizontalPadding(I)V

    .line 1108
    const/4 v1, 0x0

    .local v1, "column":I
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    if-ge v1, v2, :cond_0

    .line 1111
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemCellId:I

    invoke-virtual {p0, v2, v0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 1108
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1113
    :cond_0
    return-object v0
.end method

.method private endLastEntry(Ljava/util/List;I)V
    .locals 4
    .param p2, "newStartIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 775
    .local p1, "entryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 776
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 777
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .line 778
    .local v1, "lastItem":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 779
    add-int/lit8 v2, p2, -0x1

    # setter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v1, v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$102(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;I)I

    .line 782
    .end local v1    # "lastItem":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    :cond_0
    return-void
.end method

.method private getBaseCount()I
    .locals 3

    .prologue
    .line 786
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 787
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v1, v2, :cond_0

    .line 788
    add-int/lit8 v0, v0, 0x1

    .line 791
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getBasePrependedRowsCount()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method private getBasePrependedRowsCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 796
    iget v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinkRows:I

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->isShowingPlainHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v3, v0

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasBannerHeader:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasFilters()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasSocialHeader:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method private getCardListAdapterViewType(I)I
    .locals 10
    .param p1, "position"    # I

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x6

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1197
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasLeadingSpacer()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1198
    if-nez p1, :cond_1

    .line 1199
    const/16 v5, 0x16

    .line 1328
    :cond_0
    :goto_0
    return v5

    .line 1201
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 1204
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasExtraLeadingSpacer()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1205
    if-nez p1, :cond_3

    .line 1206
    const/16 v5, 0x17

    goto :goto_0

    .line 1208
    :cond_3
    add-int/lit8 p1, p1, -0x1

    .line 1211
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v2

    .line 1212
    .local v2, "footerMode":Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    const/4 v3, 0x1

    .line 1213
    .local v3, "footerOffset":I
    sget-object v9, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v2, v9, :cond_6

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getBaseCount()I

    move-result v9

    sub-int/2addr v9, v3

    if-ne p1, v9, :cond_6

    .line 1215
    sget-object v7, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v2, v7, :cond_0

    .line 1217
    sget-object v7, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-ne v2, v7, :cond_5

    move v5, v6

    .line 1218
    goto :goto_0

    .line 1220
    :cond_5
    const-string v7, "Unexpected footer mode: %d"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v6

    .line 1221
    goto :goto_0

    .line 1225
    :cond_6
    iget v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinkRows:I

    if-ge p1, v5, :cond_7

    .line 1226
    const/4 v5, 0x2

    goto :goto_0

    .line 1230
    :cond_7
    iget v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNumQuickLinkRows:I

    sub-int/2addr p1, v5

    .line 1232
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasSocialHeader:Z

    if-eqz v5, :cond_9

    .line 1233
    if-nez p1, :cond_8

    .line 1234
    const/16 v5, 0x11

    goto :goto_0

    .line 1236
    :cond_8
    add-int/lit8 p1, p1, -0x1

    .line 1239
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasBannerHeader:Z

    if-eqz v5, :cond_b

    .line 1240
    if-nez p1, :cond_a

    .line 1241
    const/16 v5, 0x10

    goto :goto_0

    .line 1243
    :cond_a
    add-int/lit8 p1, p1, -0x1

    .line 1246
    :cond_b
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->isShowingPlainHeader()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1247
    if-nez p1, :cond_c

    .line 1248
    const/4 v5, 0x7

    goto :goto_0

    .line 1250
    :cond_c
    add-int/lit8 p1, p1, -0x1

    .line 1253
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasFilters()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1254
    if-nez p1, :cond_e

    .line 1255
    const/16 v5, 0x8

    goto :goto_0

    .line 1257
    :cond_e
    add-int/lit8 p1, p1, -0x1

    .line 1260
    :cond_f
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .line 1261
    .local v0, "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1264
    .local v1, "firstItem":Lcom/google/android/finsky/api/model/Document;
    if-nez v1, :cond_10

    move v5, v7

    .line 1265
    goto/16 :goto_0

    .line 1268
    :cond_10
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasNextBanner()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1269
    const/4 v5, 0x3

    goto/16 :goto_0

    .line 1272
    :cond_11
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1273
    const/4 v5, 0x5

    goto/16 :goto_0

    .line 1276
    :cond_12
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1277
    const/16 v5, 0xc

    goto/16 :goto_0

    .line 1280
    :cond_13
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1281
    const/16 v5, 0xd

    goto/16 :goto_0

    .line 1284
    :cond_14
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isAddToCirclesContainer()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1285
    const/16 v5, 0xa

    goto/16 :goto_0

    .line 1288
    :cond_15
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMyCirclesContainer()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1289
    const/16 v5, 0x13

    goto/16 :goto_0

    .line 1292
    :cond_16
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isTrustedSourceContainer()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1293
    const/16 v5, 0xb

    goto/16 :goto_0

    .line 1296
    :cond_17
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isEmptyContainer()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1297
    const/16 v5, 0xf

    goto/16 :goto_0

    .line 1300
    :cond_18
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isActionBanner()Z

    move-result v5

    if-eqz v5, :cond_19

    move v5, v8

    .line 1301
    goto/16 :goto_0

    .line 1304
    :cond_19
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1305
    const/16 v5, 0x12

    goto/16 :goto_0

    .line 1308
    :cond_1a
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isSingleCardWithButton()Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1309
    const/16 v5, 0x15

    goto/16 :goto_0

    .line 1314
    :cond_1b
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v4

    .line 1315
    .local v4, "hasContainer":Z
    if-nez v4, :cond_1c

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1316
    :cond_1c
    invoke-virtual {v1, v8}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1317
    const/4 v5, 0x4

    goto/16 :goto_0

    .line 1320
    :cond_1d
    if-eqz v4, :cond_1e

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    if-eqz v5, :cond_1e

    .line 1321
    const/16 v5, 0x9

    goto/16 :goto_0

    .line 1325
    :cond_1e
    const/16 v5, 0x14

    goto/16 :goto_0

    :cond_1f
    move v5, v7

    .line 1328
    goto/16 :goto_0
.end method

.method private getGenericClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x0

    .line 1802
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v3

    .line 1803
    .local v3, "numChildren":I
    const/4 v2, 0x0

    .line 1804
    .local v2, "firstChildHasReason":Z
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1811
    .local v0, "documentType":I
    if-lez v3, :cond_0

    .line 1812
    invoke-virtual {p1, v5}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 1813
    .local v1, "firstChild":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1814
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v6

    if-eqz v6, :cond_1

    const/4 v2, 0x1

    .line 1818
    .end local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getSignalStrengthForCluster(Lcom/google/android/finsky/api/model/Document;)I

    move-result v4

    .line 1821
    .local v4, "signalStrength":I
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseMiniCards:Z

    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    if-nez v4, :cond_2

    .line 1824
    iget v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0, v5}, Lcom/google/android/finsky/layout/play/PlayCardMiniClusterRepository;->getMetadata(II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v5

    .line 1828
    :goto_1
    return-object v5

    .end local v4    # "signalStrength":I
    .restart local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    move v2, v5

    .line 1814
    goto :goto_0

    .line 1828
    .end local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    .restart local v4    # "signalStrength":I
    :cond_2
    iget v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v0, v5, v6, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->getMetadata(IIZI)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v5

    goto :goto_1
.end method

.method private getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 859
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private getMerchClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 1795
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1797
    .local v0, "documentType":I
    :goto_0
    iget v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mColumnCount:I

    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mUseTallTemplates:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v1

    return-object v1

    .line 1795
    .end local v0    # "documentType":I
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    goto :goto_0
.end method

.method private getMoreResultsStringForCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;)Ljava/lang/String;
    .locals 12
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    .prologue
    .line 1587
    const/4 v1, 0x0

    .line 1588
    .local v1, "moreString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    .line 1590
    .local v0, "containerData":Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 1592
    iget-wide v2, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    .line 1593
    .local v2, "estimatedResults":J
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-lez v5, :cond_3

    .line 1597
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v8

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1599
    .local v4, "numDocsShown":I
    int-to-long v8, v4

    sub-long v6, v2, v8

    .line 1600
    .local v6, "remaining":J
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_1

    .line 1618
    .end local v2    # "estimatedResults":J
    .end local v4    # "numDocsShown":I
    .end local v6    # "remaining":J
    :cond_0
    :goto_0
    return-object v1

    .line 1602
    .restart local v2    # "estimatedResults":J
    .restart local v4    # "numDocsShown":I
    .restart local v6    # "remaining":J
    :cond_1
    const-wide/16 v8, 0x63

    cmp-long v5, v6, v8

    if-gtz v5, :cond_2

    .line 1604
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02e9

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1607
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1611
    .end local v4    # "numDocsShown":I
    .end local v6    # "remaining":J
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1613
    .end local v2    # "estimatedResults":J
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1616
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getSignalStrengthForCluster(Lcom/google/android/finsky/api/model/Document;)I
    .locals 7
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1838
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v6

    if-ge v6, v5, :cond_1

    .line 1839
    const-string v5, "Not enough children in cluster."

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1867
    :cond_0
    :goto_0
    return v4

    .line 1843
    :cond_1
    invoke-virtual {p1, v4}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 1844
    .local v0, "firstChild":Lcom/google/android/finsky/api/model/Document;
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    .line 1845
    .local v2, "secondChild":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v1

    .line 1847
    .local v1, "firstReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v3

    .line 1851
    .local v3, "secondReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v1, :cond_0

    .line 1855
    iget-object v6, v1, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v6, :cond_3

    .line 1857
    if-eqz v3, :cond_2

    iget-object v4, v3, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v4, :cond_2

    .line 1858
    const/4 v4, 0x4

    goto :goto_0

    :cond_2
    move v4, v5

    .line 1861
    goto :goto_0

    .line 1862
    :cond_3
    iget-object v5, v1, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-eqz v5, :cond_0

    .line 1864
    const/4 v4, 0x3

    goto :goto_0
.end method

.method public static hasRestoreData(Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 497
    if-eqz p0, :cond_0

    const-string v0, "CardListAdapter.itemEntriesList"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isShowingPlainHeader()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 837
    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasPlainHeader:Z

    if-nez v3, :cond_1

    .line 855
    :cond_0
    :goto_0
    return v2

    .line 841
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    .line 842
    .local v1, "listSize":I
    if-lez v1, :cond_2

    .line 845
    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasPlainHeader:Z

    goto :goto_0

    .line 848
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v0

    .line 849
    .local v0, "footerMode":Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->shouldHidePlainHeaderDuringInitialLoading()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v0, v3, :cond_0

    .line 852
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->shouldHidePlainHeaderOnEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v0, v3, :cond_0

    .line 855
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private restoreDespiteColumnCountChange(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Ljava/util/ArrayList;IIII)V
    .locals 25
    .param p1, "list"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p3, "oldFirstVisibleRow"    # I
    .param p4, "oldRowPixelOffset"    # I
    .param p5, "oldRowPixelHeight"    # I
    .param p6, "oldLooseItemColCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/layout/play/PlayRecyclerView;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;",
            ">;IIII)V"
        }
    .end annotation

    .prologue
    .line 552
    .local p2, "restoredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    add-int v18, p5, p4

    .line 553
    .local v18, "oldRowVisiblePixelHeight":I
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    const/high16 v23, 0x3f000000    # 0.5f

    cmpl-float v22, v22, v23

    if-lez v22, :cond_0

    const/4 v6, 0x1

    .line 555
    .local v6, "isOldRowSufficientlyVisible":Z
    :goto_0
    if-eqz v6, :cond_1

    const/16 v22, 0x0

    :goto_1
    add-int p3, p3, v22

    .line 561
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v22

    move/from16 v0, p3

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    .line 563
    move/from16 v11, p3

    .line 601
    .local v11, "newFirstVisibleRow":I
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v12

    .line 602
    .local v12, "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v5, v0, :cond_9

    .line 607
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 611
    move v9, v5

    .line 612
    .local v9, "lastLooseItemRow":I
    const/16 v20, 0x0

    .line 616
    .local v20, "totalDocsFound":I
    move v7, v5

    .local v7, "j":I
    :goto_4
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v7, v0, :cond_6

    .line 617
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .line 618
    .local v19, "row":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 619
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v23

    sub-int v22, v22, v23

    add-int v20, v20, v22

    .line 620
    move v9, v7

    .line 616
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 553
    .end local v5    # "i":I
    .end local v6    # "isOldRowSufficientlyVisible":Z
    .end local v7    # "j":I
    .end local v9    # "lastLooseItemRow":I
    .end local v11    # "newFirstVisibleRow":I
    .end local v12    # "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    .end local v19    # "row":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    .end local v20    # "totalDocsFound":I
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 555
    .restart local v6    # "isOldRowSufficientlyVisible":Z
    :cond_1
    const/16 v22, 0x1

    goto :goto_1

    .line 564
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v22

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v23

    add-int v22, v22, v23

    move/from16 v0, p3

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    .line 569
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v22

    sub-int v4, p3, v22

    .line 572
    .local v4, "firstVisibleItem":I
    const/4 v15, 0x0

    .line 573
    .local v15, "numLooseRowsBeforeFirstVisibleItem":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_5
    if-ge v5, v4, :cond_4

    .line 574
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 575
    add-int/lit8 v15, v15, 0x1

    .line 573
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 580
    :cond_4
    mul-int v21, p6, v15

    .line 584
    .local v21, "totalLooseItems":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/google/android/finsky/utils/IntMath;->floor(II)I

    move-result v16

    .line 588
    .local v16, "numNewLooseRows":I
    sub-int v17, v4, v15

    .line 592
    .local v17, "numNonLooseRows":I
    add-int v22, v17, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v23

    add-int v11, v22, v23

    .line 593
    .restart local v11    # "newFirstVisibleRow":I
    goto/16 :goto_2

    .line 596
    .end local v4    # "firstVisibleItem":I
    .end local v5    # "i":I
    .end local v11    # "newFirstVisibleRow":I
    .end local v15    # "numLooseRowsBeforeFirstVisibleItem":I
    .end local v16    # "numNewLooseRows":I
    .end local v17    # "numNonLooseRows":I
    .end local v21    # "totalLooseItems":I
    :cond_5
    const v11, 0x7fffffff

    .restart local v11    # "newFirstVisibleRow":I
    goto/16 :goto_2

    .line 626
    .restart local v5    # "i":I
    .restart local v7    # "j":I
    .restart local v9    # "lastLooseItemRow":I
    .restart local v12    # "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    .restart local v20    # "totalDocsFound":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v13

    .line 627
    .local v13, "newRowCount":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v3

    .line 628
    .local v3, "firstLooseItemIndex":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v8

    .line 631
    .local v8, "lastLooseItemIndex":I
    const/4 v14, 0x0

    .local v14, "newRowIndex":I
    :goto_6
    if-ge v14, v13, :cond_7

    .line 633
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    add-int v22, v22, v3

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 635
    .local v10, "newEndIndex":I
    new-instance v22, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    const/16 v23, 0x1

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v3, v10, v1, v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;)V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    add-int v3, v3, v22

    .line 631
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 640
    .end local v10    # "newEndIndex":I
    :cond_7
    move v5, v9

    .line 602
    .end local v3    # "firstLooseItemIndex":I
    .end local v7    # "j":I
    .end local v8    # "lastLooseItemIndex":I
    .end local v9    # "lastLooseItemRow":I
    .end local v13    # "newRowCount":I
    .end local v14    # "newRowIndex":I
    .end local v20    # "totalDocsFound":I
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 643
    :cond_8
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 646
    :cond_9
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 650
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 652
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getItemCount()I

    move-result v22

    move/from16 v0, v22

    if-ge v11, v0, :cond_a

    .line 655
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->scrollToPosition(I)V

    .line 656
    return-void

    .line 652
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getItemCount()I

    move-result v22

    add-int/lit8 v11, v22, -0x1

    goto :goto_8
.end method

.method private syncItemEntries()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 688
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v5

    .line 691
    .local v5, "underlyingCount":I
    const/4 v3, 0x0

    .line 692
    .local v3, "lastItemEntryIndex":I
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 693
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v6}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v6

    add-int/lit8 v3, v6, 0x1

    .line 698
    :cond_0
    move v2, v3

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_a

    .line 701
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6, v2, v10}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(IZ)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 702
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_2

    .line 707
    const-string v6, "Loaded null doc, forcing a hard reload of list data."

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 708
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->resetItems()V

    .line 709
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->startLoadItems()V

    .line 710
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 768
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    :goto_1
    return-void

    .line 714
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasNextBanner()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isActionBanner()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isEmptyContainer()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 718
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->endLastEntry(Ljava/util/List;I)V

    .line 724
    sget-boolean v6, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v6, :cond_6

    .line 726
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-static {v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 698
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 732
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-static {v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 739
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 745
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    invoke-direct {v7, v2, v2, v10, v11}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 749
    :cond_8
    const/4 v4, 0x0

    .line 750
    .local v4, "rowReused":Z
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_9

    .line 751
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .line 752
    .local v1, "existingItem":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z

    move-result v6

    if-eqz v6, :cond_9

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v6

    sub-int v6, v2, v6

    iget v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    if-ge v6, v7, :cond_9

    .line 754
    const/4 v4, 0x1

    .line 757
    .end local v1    # "existingItem":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    :cond_9
    if-nez v4, :cond_4

    .line 759
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->endLastEntry(Ljava/util/List;I)V

    .line 760
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    const/4 v8, -0x1

    const/4 v9, 0x1

    invoke-direct {v7, v2, v8, v9, v11}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 764
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "rowReused":Z
    :cond_a
    if-lez v5, :cond_1

    .line 766
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->endLastEntry(Ljava/util/List;I)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V
    .locals 7
    .param p1, "identifiable"    # Lcom/google/android/finsky/layout/play/Identifiable;
    .param p2, "spinner"    # Landroid/widget/Spinner;
    .param p3, "corpusHeaderStrip"    # Landroid/view/View;

    .prologue
    .line 1705
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 1706
    .local v0, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/google/android/finsky/layout/play/Identifiable;->setIdentifier(Ljava/lang/String;)V

    .line 1707
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusSpinnerDrawable(I)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 1710
    if-eqz p3, :cond_0

    .line 1711
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p3, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1716
    :cond_0
    invoke-virtual {p2}, Landroid/widget/Spinner;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b012c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardContentPadding:I

    add-int v2, v5, v6

    .line 1718
    .local v2, "horizontalMargin":I
    invoke-virtual {p2}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1720
    .local v4, "spinnerLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1721
    iput v2, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1722
    invoke-virtual {p2, v4}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1724
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getContainerViews()[Lcom/google/android/finsky/protos/Containers$ContainerView;

    move-result-object v1

    .line 1725
    .local v1, "containerViews":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    new-instance v5, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6, v1}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;-><init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1726
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_1

    .line 1727
    aget-object v5, v1, v3

    iget-boolean v5, v5, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    if-eqz v5, :cond_2

    .line 1728
    invoke-virtual {p2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1733
    :cond_1
    new-instance v5, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;

    invoke-direct {v5, p0, p2, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$6;-><init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Landroid/widget/Spinner;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1761
    return-void

    .line 1726
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 1581
    invoke-static {p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getDataRowsCount()I
    .locals 1

    .prologue
    .line 824
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getBaseCount()I

    move-result v0

    return v0
.end method

.method public final getItemCount()I
    .locals 3

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getDataRowsCount()I

    move-result v0

    .line 806
    .local v0, "dataRowsCount":I
    if-nez v0, :cond_1

    .line 809
    const/4 v1, 0x0

    .line 820
    :cond_0
    :goto_0
    return v1

    .line 812
    :cond_1
    move v1, v0

    .line 813
    .local v1, "result":I
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasLeadingSpacer()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 814
    add-int/lit8 v1, v1, 0x1

    .line 816
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasExtraLeadingSpacer()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 817
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1188
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getCardListAdapterViewType(I)I

    move-result v0

    return v0
.end method

.method protected getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .locals 0

    .prologue
    .line 1779
    return-object p0
.end method

.method public getPrependedRowsCount()I
    .locals 1

    .prologue
    .line 833
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getBasePrependedRowsCount()I

    move-result v0

    return v0
.end method

.method protected hasExtraLeadingSpacer()Z
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasBannerHeader:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasSocialHeader:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasFilters()Z
    .locals 1

    .prologue
    .line 444
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mHasFilters:Z

    return v0
.end method

.method protected hasLeadingSpacer()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x1

    return v0
.end method

.method protected isDismissed(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 1179
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->isDismissable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 8
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 946
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    .line 949
    .local v2, "itemViewType":I
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v4

    sub-int v7, p2, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasLeadingSpacer()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    sub-int/2addr v7, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasExtraLeadingSpacer()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_1
    sub-int v0, v7, v4

    .line 951
    .local v0, "adjustedIndex":I
    const/4 v1, 0x0

    .line 952
    .local v1, "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    if-ltz v0, :cond_0

    .line 953
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    check-cast v1, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .line 956
    .restart local v1    # "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    :cond_0
    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 957
    .local v3, "v":Landroid/view/View;
    packed-switch v2, :pswitch_data_0

    .line 1029
    :pswitch_0
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindGenericCluster(ILandroid/view/View;)V

    .line 1031
    :goto_2
    :pswitch_1
    return-void

    .end local v0    # "adjustedIndex":I
    .end local v1    # "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    move v4, v6

    .line 949
    goto :goto_0

    :cond_2
    move v4, v6

    goto :goto_1

    .line 959
    .restart local v0    # "adjustedIndex":I
    .restart local v1    # "entry":Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    .restart local v3    # "v":Landroid/view/View;
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindErrorFooterView(Landroid/view/View;)Landroid/view/View;

    goto :goto_2

    .line 962
    :pswitch_3
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindLoadingFooterView(Landroid/view/View;)V

    goto :goto_2

    .line 965
    :pswitch_4
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindLeadingSpacer(Landroid/view/View;)V

    goto :goto_2

    .line 968
    :pswitch_5
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindExtraLeadingSpacer(Landroid/view/View;)V

    goto :goto_2

    .line 974
    :pswitch_6
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindPlainHeaderView(Landroid/view/View;)V

    goto :goto_2

    .line 977
    :pswitch_7
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindBannerHeaderView(Landroid/view/View;)V

    goto :goto_2

    .line 980
    :pswitch_8
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSocialHeaderView(Landroid/view/View;)V

    goto :goto_2

    .line 983
    :pswitch_9
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindMerchBanner(ILandroid/view/View;)V

    goto :goto_2

    .line 986
    :pswitch_a
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindContainerFilterView(Landroid/view/View;)V

    goto :goto_2

    .line 989
    :pswitch_b
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v5

    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindLooseItemRow(IILandroid/view/View;)V

    goto :goto_2

    .line 992
    :pswitch_c
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindMerchCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 995
    :pswitch_d
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindAddToCirclesCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 998
    :pswitch_e
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindTrustedSourceCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1001
    :pswitch_f
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindActionBannerCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1004
    :pswitch_10
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3, v6}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSingleDocCluster(ILandroid/view/View;Z)V

    goto :goto_2

    .line 1007
    :pswitch_11
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindOrderedCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1010
    :pswitch_12
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindRateCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1013
    :pswitch_13
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindQuickSuggestionsCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1016
    :pswitch_14
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindEmptyCluster(ILandroid/view/View;)V

    goto :goto_2

    .line 1019
    :pswitch_15
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindWarmWelcomeCardView(ILandroid/view/View;)V

    goto/16 :goto_2

    .line 1022
    :pswitch_16
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindMyCirclesCluster(ILandroid/view/View;)V

    goto/16 :goto_2

    .line 1025
    :pswitch_17
    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v4, v3, v5}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindSingleDocCluster(ILandroid/view/View;Z)V

    goto/16 :goto_2

    .line 957
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_9
        :pswitch_c
        :pswitch_10
        :pswitch_b
        :pswitch_6
        :pswitch_a
        :pswitch_11
        :pswitch_d
        :pswitch_e
        :pswitch_12
        :pswitch_13
        :pswitch_f
        :pswitch_14
        :pswitch_7
        :pswitch_8
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const v3, 0x7f040119

    const v2, 0x7f040127

    const/4 v4, 0x0

    .line 864
    const/4 v1, 0x0

    .line 865
    .local v1, "v":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 939
    :pswitch_0
    invoke-virtual {p0, v3, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 941
    :goto_0
    new-instance v2, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ViewHolder;

    invoke-direct {v2, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v2

    .line 867
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->createErrorFooterView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 868
    goto :goto_0

    .line 870
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->createLoadingFooterView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 871
    goto :goto_0

    .line 874
    :pswitch_3
    const v2, 0x7f04014d

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 875
    goto :goto_0

    .line 877
    :pswitch_4
    const v2, 0x7f04016b

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 878
    goto :goto_0

    .line 880
    :pswitch_5
    const v2, 0x7f04011a

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 881
    goto :goto_0

    .line 883
    :pswitch_6
    const v2, 0x7f04004e

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 884
    goto :goto_0

    .line 886
    :pswitch_7
    const v2, 0x7f0401b2

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 887
    goto :goto_0

    .line 889
    :pswitch_8
    const v2, 0x7f04014e

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 890
    goto :goto_0

    .line 892
    :pswitch_9
    const v2, 0x7f04015c

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 893
    goto :goto_0

    .line 895
    :pswitch_a
    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mShowLooseItemReasons:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, v3, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 897
    :goto_1
    goto :goto_0

    .line 895
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->createRowOfLooseItemsWithoutReasons(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 899
    :pswitch_b
    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 900
    goto :goto_0

    .line 902
    :pswitch_c
    const v2, 0x7f040116

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 903
    goto :goto_0

    .line 905
    :pswitch_d
    const v2, 0x7f04013c

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 906
    goto :goto_0

    .line 908
    :pswitch_e
    const v2, 0x7f040115

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 909
    goto :goto_0

    .line 911
    :pswitch_f
    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 912
    goto :goto_0

    .line 914
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->createOrderedCluster(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 915
    goto :goto_0

    .line 917
    :pswitch_11
    const v2, 0x7f040132

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 918
    goto/16 :goto_0

    .line 920
    :pswitch_12
    const v2, 0x7f040131

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 921
    goto/16 :goto_0

    .line 923
    :pswitch_13
    const v2, 0x7f04011b

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 924
    goto/16 :goto_0

    .line 926
    :pswitch_14
    iget v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mWarmWelcomeCardColumns:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const v0, 0x7f0401d2

    .line 929
    .local v0, "layoutId":I
    :goto_2
    invoke-virtual {p0, v0, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 930
    goto/16 :goto_0

    .line 926
    .end local v0    # "layoutId":I
    :cond_1
    const v0, 0x7f0401d1

    goto :goto_2

    .line 932
    :pswitch_15
    const v2, 0x7f040129

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 933
    goto/16 :goto_0

    .line 935
    :pswitch_16
    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 936
    goto/16 :goto_0

    .line 865
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_8
        :pswitch_b
        :pswitch_f
        :pswitch_a
        :pswitch_5
        :pswitch_9
        :pswitch_10
        :pswitch_c
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_e
        :pswitch_13
        :pswitch_6
        :pswitch_7
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onDataChanged()V
    .locals 0

    .prologue
    .line 661
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->syncItemEntries()V

    .line 662
    invoke-super {p0}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->onDataChanged()V

    .line 663
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1184
    return-void
.end method

.method public onDismissDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 1784
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 1785
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 1786
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "restoreBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, -0x1

    .line 502
    const-string v0, "CardListAdapter.itemEntriesList"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 507
    .local v2, "restoredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 508
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->syncItemEntries()V

    .line 509
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 510
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->startLoadItems()V

    .line 511
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 534
    :cond_1
    :goto_0
    return-void

    .line 515
    :cond_2
    const-string v0, "CardListAdapter.firstVisibleRow"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 516
    .local v3, "firstVisibleRow":I
    const-string v0, "CardListAdapter.rowPixelOffset"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 517
    .local v4, "rowPixelOffset":I
    const-string v0, "CardListAdapter.rowPixelHeight"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 518
    .local v5, "rowPixelHeight":I
    const-string v0, "CardListAdapter.columnCount"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 520
    .local v6, "oldLooseItemColCount":I
    iget v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    if-eq v6, v0, :cond_3

    if-ne v6, v1, :cond_4

    .line 522
    :cond_3
    iput-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    .line 525
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 526
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v0, :cond_1

    .line 527
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v7

    check-cast v7, Landroid/support/v7/widget/LinearLayoutManager;

    .line 528
    .local v7, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {v7, v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    goto :goto_0

    .end local v7    # "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    :cond_4
    move-object v0, p0

    move-object v1, p1

    .line 531
    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->restoreDespiteColumnCountChange(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Ljava/util/ArrayList;IIII)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 465
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 467
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 494
    :goto_0
    return-void

    .line 470
    :cond_0
    const/4 v0, 0x0

    .line 471
    .local v0, "firstVisibleRow":I
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    instance-of v4, v4, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v4, :cond_1

    .line 472
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    .line 476
    :cond_1
    const/4 v3, 0x0

    .line 477
    .local v3, "rowPixelOffset":I
    const/4 v2, 0x0

    .line 478
    .local v2, "rowPixelHeight":I
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 479
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 480
    .local v1, "firstVisibleView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 481
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 485
    .end local v1    # "firstVisibleView":Landroid/view/View;
    :cond_2
    const-string v4, "CardListAdapter.firstVisibleRow"

    invoke-virtual {p2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 487
    const-string v4, "CardListAdapter.rowPixelOffset"

    invoke-virtual {p2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 489
    const-string v4, "CardListAdapter.rowPixelHeight"

    invoke-virtual {p2, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 491
    const-string v4, "CardListAdapter.columnCount"

    iget v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mLooseItemColCount:I

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 493
    const-string v4, "CardListAdapter.itemEntriesList"

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 1765
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 1766
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Lcom/google/android/finsky/adapters/Recyclable;

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 1767
    check-cast v2, Lcom/google/android/finsky/adapters/Recyclable;

    invoke-interface {v2}, Lcom/google/android/finsky/adapters/Recyclable;->onRecycle()V

    .line 1769
    :cond_0
    instance-of v2, v1, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 1770
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .line 1771
    .local v0, "clusterView":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/play/PlayCardHeap;->recycle(Lcom/google/android/finsky/layout/play/PlayCardClusterView;)V

    .line 1773
    .end local v0    # "clusterView":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    :cond_1
    return-void
.end method

.method protected shouldHidePlainHeaderDuringInitialLoading()Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method

.method protected shouldHidePlainHeaderOnEmpty()Z
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x0

    return v0
.end method

.method public updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/ContainerList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 667
    .local p1, "containerList":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<*>;"
    invoke-super {p0, p1}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 668
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 669
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->syncItemEntries()V

    .line 670
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 671
    return-void
.end method
