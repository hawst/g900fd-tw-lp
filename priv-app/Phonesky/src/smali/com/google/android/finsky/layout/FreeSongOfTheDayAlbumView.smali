.class public Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;
.super Landroid/widget/LinearLayout;
.source "FreeSongOfTheDayAlbumView.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# instance fields
.field private mAlbumCard:Lcom/google/android/play/layout/PlayCardViewBase;

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field public mHeader:Landroid/widget/TextView;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentDoc:Lcom/google/android/finsky/api/model/Document;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method private attachToInternalRequest()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->setVisibility(I)V

    .line 107
    invoke-direct {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->prepareAndPopulateContent()V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mAlbumCard:Lcom/google/android/play/layout/PlayCardViewBase;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    goto :goto_0
.end method

.method private detachListeners()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 85
    :cond_0
    return-void
.end method

.method private prepareAndPopulateContent()V
    .locals 6

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 128
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mAlbumCard:Lcom/google/android/play/layout/PlayCardViewBase;

    iget-object v2, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mParentDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 130
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "parentDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mHeader:Landroid/widget/TextView;

    const v1, 0x7f0c0347

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 75
    iput-object p1, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mParentDoc:Lcom/google/android/finsky/api/model/Document;

    .line 76
    iput-object p2, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mUrl:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->setupView()V

    .line 79
    return-void
.end method

.method public init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 0
    .param p1, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 64
    iput-object p2, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 65
    iput-object p3, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 66
    return-void
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->prepareAndPopulateContent()V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 135
    invoke-direct {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->detachListeners()V

    .line 136
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f0a01b3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    iput-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mAlbumCard:Lcom/google/android/play/layout/PlayCardViewBase;

    .line 59
    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mHeader:Landroid/widget/TextView;

    .line 60
    return-void
.end method

.method public setupView()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->setVisibility(I)V

    .line 93
    invoke-direct {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->detachListeners()V

    .line 95
    new-instance v0, Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mUrl:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->mDetailsData:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 96
    invoke-direct {p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->attachToInternalRequest()V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->setVisibility(I)V

    goto :goto_0
.end method
