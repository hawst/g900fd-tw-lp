.class public final Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ResponseMessages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResponseMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerCommands"
.end annotation


# instance fields
.field public clearCache:Z

.field public displayErrorMessage:Ljava/lang/String;

.field public hasClearCache:Z

.field public hasDisplayErrorMessage:Z

.field public hasLogErrorStacktrace:Z

.field public logErrorStacktrace:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clear()Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 39
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    .line 43
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasClearCache:Z

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    .line 45
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasDisplayErrorMessage:Z

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    .line 47
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasLogErrorStacktrace:Z

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->cachedSize:I

    .line 49
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 70
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasClearCache:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    if-eqz v1, :cond_1

    .line 71
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasDisplayErrorMessage:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 75
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasLogErrorStacktrace:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 79
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 90
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 91
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 95
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    :sswitch_0
    return-object p0

    .line 101
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    .line 102
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasClearCache:Z

    goto :goto_0

    .line 106
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    .line 107
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasDisplayErrorMessage:Z

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    .line 112
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasLogErrorStacktrace:Z

    goto :goto_0

    .line 91
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasClearCache:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    if-eqz v0, :cond_1

    .line 56
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 58
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasDisplayErrorMessage:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 59
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 61
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasLogErrorStacktrace:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 62
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 64
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 65
    return-void
.end method
