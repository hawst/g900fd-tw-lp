.class public final Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoSnippet"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;


# instance fields
.field public description:Ljava/lang/String;

.field public hasDescription:Z

.field public hasTitle:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5466
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5467
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->clear()Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 5468
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 2

    .prologue
    .line 5444
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v0, :cond_1

    .line 5445
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 5447
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v0, :cond_0

    .line 5448
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 5450
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5452
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    return-object v0

    .line 5450
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5471
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 5472
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    .line 5473
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    .line 5474
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    .line 5475
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    .line 5476
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->cachedSize:I

    .line 5477
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 5502
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 5503
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 5504
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 5505
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 5506
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_0

    .line 5507
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5504
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5512
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 5513
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5516
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 5517
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5520
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 5528
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 5529
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 5533
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5534
    :sswitch_0
    return-object p0

    .line 5539
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5541
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_2

    move v1, v4

    .line 5542
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Image;

    .line 5544
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_1

    .line 5545
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5547
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 5548
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 5549
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 5550
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5547
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5541
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v5

    goto :goto_1

    .line 5553
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 5554
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 5555
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto :goto_0

    .line 5559
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    .line 5560
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    goto :goto_0

    .line 5564
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    .line 5565
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    goto :goto_0

    .line 5529
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5438
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5483
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 5484
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 5485
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 5486
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_0

    .line 5487
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5484
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5491
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 5492
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5494
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 5495
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5497
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5498
    return-void
.end method
