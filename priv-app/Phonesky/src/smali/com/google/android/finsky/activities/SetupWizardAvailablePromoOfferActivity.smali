.class public Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "SetupWizardAvailablePromoOfferActivity.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

.field private mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->handleOfferContinued()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->handleOfferSkipped(Ljava/lang/String;)V

    return-void
.end method

.method public static createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;)Landroid/content/Intent;
    .locals 4
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "availablePromoOffer"    # Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .prologue
    .line 70
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 71
    .local v1, "internalParams":Landroid/os/Bundle;
    const-string v2, "available_offer"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 73
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const-class v3, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 76
    const-string v2, "authAccount"

    iget-object v3, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const-string v2, "internal_parameters"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 78
    return-object v0
.end method

.method private handleOfferContinued()V
    .locals 4

    .prologue
    .line 195
    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0xfc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 196
    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    iget-object v1, v1, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;

    move-result-object v0

    .line 199
    .local v0, "addFopIntent":Landroid/content/Intent;
    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 200
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 202
    .end local v0    # "addFopIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private handleOfferSkipped(Ljava/lang/String;)V
    .locals 6
    .param p1, "noActionMessage"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 205
    iget-object v3, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v4, 0xfd

    invoke-virtual {v3, v4, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 206
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 209
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v3, "no_action_message"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 221
    .end local v1    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :goto_0
    return-void

    .line 212
    .restart local v1    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_0
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 213
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0c02a0

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v3

    const/16 v4, 0x65

    invoke-virtual {v3, v5, v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 216
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v2

    .line 217
    .local v2, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v3, "no_action_message"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 219
    .end local v0    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v1    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v2    # "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    goto :goto_0
.end method

.method private replaceUrlsWithHandlers(Ljava/lang/CharSequence;)V
    .locals 11
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v10, 0x0

    .line 237
    instance-of v8, p1, Landroid/text/Spannable;

    if-nez v8, :cond_1

    .line 253
    :cond_0
    return-void

    :cond_1
    move-object v5, p1

    .line 240
    check-cast v5, Landroid/text/Spannable;

    .line 241
    .local v5, "spannable":Landroid/text/Spannable;
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v8

    const-class v9, Landroid/text/style/URLSpan;

    invoke-interface {v5, v10, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/text/style/URLSpan;

    .line 242
    .local v6, "spans":[Landroid/text/style/URLSpan;
    move-object v0, v6

    .local v0, "arr$":[Landroid/text/style/URLSpan;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 243
    .local v4, "span":Landroid/text/style/URLSpan;
    invoke-interface {v5, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 244
    .local v7, "start":I
    invoke-interface {v5, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 245
    .local v1, "end":I
    invoke-interface {v5, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 246
    new-instance v8, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$4;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$4;-><init>(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Ljava/lang/String;)V

    invoke-interface {v5, v8, v7, v1, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 242
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setResultAndFinish()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResult(I)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->finish()V

    .line 289
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 293
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->finish()V

    .line 294
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 295
    return-void
.end method

.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020088

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 226
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 227
    return-object v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 314
    const/16 v0, 0x37a

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 272
    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    .line 274
    if-eqz p3, :cond_1

    const-string v1, "redeemed_offer_message_html"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 276
    .local v0, "redeemed":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x1f4

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logOperationSuccessBackgroundEvent(IZ)V

    .line 280
    const/4 v1, -0x1

    invoke-virtual {p0, v1, p3}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResult(ILandroid/content/Intent;)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->finish()V

    .line 283
    .end local v0    # "redeemed":Z
    :cond_0
    return-void

    .line 274
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 86
    .local v7, "intent":Landroid/content/Intent;
    const-string v0, "internal_parameters"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 87
    .local v8, "internalParameters":Landroid/os/Bundle;
    if-nez v8, :cond_0

    .line 88
    const-string v0, "No internal parameters passed."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    .line 130
    :goto_0
    return-void

    .line 92
    :cond_0
    const-string v0, "available_offer"

    invoke-static {v8, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    iput-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-nez v0, :cond_1

    .line 95
    const-string v0, "No available offer passed."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    iget-object v0, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    if-nez v0, :cond_2

    .line 100
    const-string v0, "Unsupported offer."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    goto :goto_0

    .line 105
    :cond_2
    const-string v0, "account"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAccount:Landroid/accounts/Account;

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_3

    .line 107
    const-string v0, "No account passed."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    goto :goto_0

    .line 112
    :cond_3
    new-instance v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-direct {v0, v7}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_4

    const v9, 0x7f0d01bf

    .line 117
    .local v9, "themeId":I
    :goto_1
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setTheme(I)V

    .line 119
    const v0, 0x7f0401a5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setContentView(I)V

    .line 120
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 121
    .local v6, "contentFrame":Landroid/view/ViewGroup;
    invoke-virtual {v6}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04019d

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    goto :goto_0

    .line 115
    .end local v6    # "contentFrame":Landroid/view/ViewGroup;
    .end local v9    # "themeId":I
    :cond_4
    const v9, 0x7f0d01be

    goto :goto_1
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 308
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 301
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 302
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->setResultAndFinish()V

    .line 304
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 15

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onResume()V

    .line 140
    iget-object v13, p0, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->mAvailablePromoOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    iget-object v0, v13, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    .line 142
    .local v0, "addCreditCardOffer":Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    iget-object v11, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    .line 143
    .local v11, "title":Ljava/lang/String;
    const v13, 0x7f0a009c

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 144
    .local v12, "titleView":Landroid/widget/TextView;
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v13, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 147
    .local v4, "intro":Ljava/lang/CharSequence;
    iget-object v13, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 148
    .local v3, "description":Ljava/lang/CharSequence;
    iget-object v7, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 149
    .local v7, "promoImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v13, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    .line 150
    .local v10, "terms":Ljava/lang/CharSequence;
    invoke-direct {p0, v10}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->replaceUrlsWithHandlers(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v5, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    .line 153
    .local v5, "noActionMessage":Ljava/lang/String;
    const v13, 0x7f0a037b

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;

    .line 155
    .local v1, "content":Lcom/google/android/finsky/layout/AvailablePromoOfferContent;
    invoke-static {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v9

    .line 156
    .local v9, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v9, :cond_0

    .line 157
    const/4 v13, 0x0

    invoke-virtual {v1, v4, v13, v3, v10}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->configure(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 158
    const v13, 0x7f0a0380

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioGroup;

    .line 159
    .local v8, "radioGroup":Landroid/widget/RadioGroup;
    invoke-virtual {v9}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v13

    new-instance v14, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;

    invoke-direct {v14, p0, v8, v5}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$1;-><init>(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Landroid/widget/RadioGroup;Ljava/lang/String;)V

    invoke-virtual {v13, v14}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    .end local v8    # "radioGroup":Landroid/widget/RadioGroup;
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {v1, v4, v7, v3, v10}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->configure(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 171
    const v13, 0x7f0a0231

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;

    .line 173
    .local v2, "continueButton":Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0201a2

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    const v13, 0x7f0c00bb

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setText(Ljava/lang/String;)V

    .line 175
    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setEnabled(Z)V

    .line 176
    new-instance v13, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$2;

    invoke-direct {v13, p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$2;-><init>(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;)V

    invoke-virtual {v2, v13}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    const v13, 0x7f0a031b

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 183
    .local v6, "notNowButton":Landroid/widget/TextView;
    const v13, 0x7f0c011d

    invoke-virtual {p0, v13}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    const/4 v13, 0x0

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    new-instance v13, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$3;

    invoke-direct {v13, p0, v5}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity$3;-><init>(Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public showUrlWebView(Ljava/lang/String;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 258
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v4, "policy_terms"

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 268
    :goto_0
    return-void

    .line 261
    :cond_0
    new-instance v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 262
    .local v1, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v4, 0x7f04002f

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c02a1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 263
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 264
    .local v0, "argumentsBundle":Landroid/os/Bundle;
    const-string v4, "url_key"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 266
    invoke-virtual {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v3

    .line 267
    .local v3, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "policy_terms"

    invoke-virtual {v3, v2, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
