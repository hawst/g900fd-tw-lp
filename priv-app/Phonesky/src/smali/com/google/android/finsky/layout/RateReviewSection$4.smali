.class Lcom/google/android/finsky/layout/RateReviewSection$4;
.super Ljava/lang/Object;
.source "RateReviewSection.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/RateReviewSection;->refresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/RateReviewSection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/RateReviewSection;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/finsky/layout/RateReviewSection$4;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V
    .locals 6
    .param p1, "response"    # Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .prologue
    const/16 v3, 0x8

    .line 233
    iget-object v1, p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 234
    .local v1, "plusDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_0

    .line 235
    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection$4;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mMyDisplayName:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/layout/RateReviewSection;->access$500(Lcom/google/android/finsky/layout/RateReviewSection;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    .line 238
    .local v0, "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection$4;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mMyAvatar:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v2}, Lcom/google/android/finsky/layout/RateReviewSection;->access$600(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 245
    .end local v0    # "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection$4;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mMyAvatar:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v2}, Lcom/google/android/finsky/layout/RateReviewSection;->access$600(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 243
    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection$4;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mMyDisplayName:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/layout/RateReviewSection;->access$500(Lcom/google/android/finsky/layout/RateReviewSection;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 230
    check-cast p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/RateReviewSection$4;->onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V

    return-void
.end method
