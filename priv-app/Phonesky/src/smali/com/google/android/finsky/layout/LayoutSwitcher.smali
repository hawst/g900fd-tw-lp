.class public Lcom/google/android/finsky/layout/LayoutSwitcher;
.super Ljava/lang/Object;
.source "LayoutSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
    }
.end annotation


# instance fields
.field protected final mContentLayout:Landroid/view/View;

.field protected mDataLayoutId:I

.field private final mErrorLayoutId:I

.field private final mHandler:Landroid/os/Handler;

.field private final mLoadingLayoutId:I

.field private mMode:I

.field private volatile mPendingLoad:Z

.field private final mRetryListener:Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;

.field private final retryClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V
    .locals 1
    .param p1, "pageLayout"    # Landroid/view/View;
    .param p2, "dataLayoutId"    # I
    .param p3, "errorLayoutId"    # I
    .param p4, "loadingLayoutId"    # I
    .param p5, "listener"    # Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
    .param p6, "initialState"    # I

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mHandler:Landroid/os/Handler;

    .line 60
    new-instance v0, Lcom/google/android/finsky/layout/LayoutSwitcher$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/LayoutSwitcher$1;-><init>(Lcom/google/android/finsky/layout/LayoutSwitcher;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->retryClickListener:Landroid/view/View$OnClickListener;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mPendingLoad:Z

    .line 103
    iput p2, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mDataLayoutId:I

    .line 104
    iput p3, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mErrorLayoutId:I

    .line 105
    iput p4, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mLoadingLayoutId:I

    .line 106
    iput-object p1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mContentLayout:Landroid/view/View;

    .line 107
    iput-object p5, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mRetryListener:Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;

    .line 108
    iput p6, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/view/View;ILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;)V
    .locals 1
    .param p1, "pageLayout"    # Landroid/view/View;
    .param p2, "dataLayoutId"    # I
    .param p3, "listener"    # Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mHandler:Landroid/os/Handler;

    .line 60
    new-instance v0, Lcom/google/android/finsky/layout/LayoutSwitcher$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/LayoutSwitcher$1;-><init>(Lcom/google/android/finsky/layout/LayoutSwitcher;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->retryClickListener:Landroid/view/View$OnClickListener;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mPendingLoad:Z

    .line 123
    iput p2, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mDataLayoutId:I

    .line 124
    const v0, 0x7f0a01cf

    iput v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mErrorLayoutId:I

    .line 125
    const v0, 0x7f0a0109

    iput v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mLoadingLayoutId:I

    .line 126
    iput-object p1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mContentLayout:Landroid/view/View;

    .line 127
    iput-object p3, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mRetryListener:Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;

    .line 129
    invoke-direct {p0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->resetMode()V

    .line 130
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/LayoutSwitcher;)Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/LayoutSwitcher;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mRetryListener:Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/LayoutSwitcher;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/LayoutSwitcher;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mPendingLoad:Z

    return v0
.end method

.method private performSwitch(ILjava/lang/String;)V
    .locals 3
    .param p1, "newMode"    # I
    .param p2, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mPendingLoad:Z

    .line 205
    iget v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    if-ne v0, p1, :cond_0

    .line 239
    :goto_0
    return-void

    .line 209
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 221
    :goto_1
    packed-switch p1, :pswitch_data_1

    .line 234
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "should be LOADING_MODE, ERROR_MODE, DATA_MODE, or BLANK_MODE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setLoadingVisible(Z)V

    goto :goto_1

    .line 214
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setErrorVisible(ZLjava/lang/String;)V

    goto :goto_1

    .line 217
    :pswitch_2
    if-nez p1, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {p0, v2, v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setDataVisible(ZZ)V

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    .line 223
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setLoadingVisible(Z)V

    .line 238
    :goto_3
    :pswitch_4
    iput p1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    goto :goto_0

    .line 226
    :pswitch_5
    invoke-direct {p0, v1, p2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setErrorVisible(ZLjava/lang/String;)V

    goto :goto_3

    .line 229
    :pswitch_6
    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setDataVisible(ZZ)V

    goto :goto_3

    .line 209
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 221
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method private resetMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    .line 134
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setLoadingVisible(Z)V

    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setErrorVisible(ZLjava/lang/String;)V

    .line 136
    invoke-virtual {p0, v1, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->setDataVisible(ZZ)V

    .line 137
    return-void
.end method

.method private setErrorVisible(ZLjava/lang/String;)V
    .locals 5
    .param p1, "show"    # Z
    .param p2, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 257
    iget-object v3, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mContentLayout:Landroid/view/View;

    iget v4, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mErrorLayoutId:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 258
    .local v0, "errorIndicator":Landroid/view/View;
    if-eqz p1, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 260
    if-eqz p1, :cond_0

    .line 261
    const v3, 0x7f0a00f4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 262
    .local v2, "textView":Landroid/widget/TextView;
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    .end local v2    # "textView":Landroid/widget/TextView;
    :cond_0
    const v3, 0x7f0a01ce

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 266
    .local v1, "retryButton":Landroid/widget/Button;
    if-eqz p1, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->retryClickListener:Landroid/view/View$OnClickListener;

    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    return-void

    .line 258
    .end local v1    # "retryButton":Landroid/widget/Button;
    :cond_1
    const/16 v3, 0x8

    goto :goto_0

    .line 266
    .restart local v1    # "retryButton":Landroid/widget/Button;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setLoadingVisible(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 247
    iget-object v1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mContentLayout:Landroid/view/View;

    iget v2, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mLoadingLayoutId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 248
    .local v0, "loadingIndicators":Landroid/view/View;
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    return-void

    .line 248
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method


# virtual methods
.method public isDataMode()Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setDataVisible(ZZ)V
    .locals 3
    .param p1, "show"    # Z
    .param p2, "goingToLoadingMode"    # Z

    .prologue
    .line 275
    iget v1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mDataLayoutId:I

    if-gtz v1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mContentLayout:Landroid/view/View;

    iget v2, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mDataLayoutId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 279
    .local v0, "dataView":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 280
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public switchToBlankMode()V
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->performSwitch(ILjava/lang/String;)V

    .line 152
    return-void
.end method

.method public switchToDataMode()V
    .locals 2

    .prologue
    .line 187
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->performSwitch(ILjava/lang/String;)V

    .line 188
    return-void
.end method

.method public switchToDataMode(I)V
    .locals 0
    .param p1, "layoutId"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mDataLayoutId:I

    .line 192
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 193
    return-void
.end method

.method public switchToErrorMode(Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 199
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->performSwitch(ILjava/lang/String;)V

    .line 200
    return-void
.end method

.method public switchToLoadingDelayed(I)V
    .locals 4
    .param p1, "delayMillis"    # I

    .prologue
    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mPendingLoad:Z

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/layout/LayoutSwitcher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/layout/LayoutSwitcher$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/LayoutSwitcher$2;-><init>(Lcom/google/android/finsky/layout/LayoutSwitcher;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 181
    return-void
.end method

.method public switchToLoadingMode()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->performSwitch(ILjava/lang/String;)V

    .line 159
    return-void
.end method
