.class public Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;
.super Lcom/google/android/finsky/layout/IdentifiableFrameLayout;
.source "PeopleDetailsProfileInfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;


# instance fields
.field private mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

.field private mContentHorizontalPadding:I

.field private mDisplayName:Landroid/widget/TextView;

.field private final mIdealCoverHeight:I

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mPlusDoc:Lcom/google/android/finsky/api/model/Document;

.field private mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

.field private mProfileCoverImageFrameOverlay:Landroid/view/View;

.field private mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

.field private mViewProfileText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0166

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mIdealCoverHeight:I

    .line 68
    return-void
.end method

.method private launchGPlusProfileView(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "obfusGaiaId"    # Ljava/lang/String;

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppViewItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 258
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showAppNeededDialog(I)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;I)V
    .locals 11
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p3, "contentHorizontalPadding"    # I

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 96
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    .line 97
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 98
    iput p3, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mContentHorizontalPadding:I

    .line 100
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v1

    .line 102
    .local v1, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    const/16 v7, 0xf

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Common$Image;

    .line 103
    .local v2, "coverImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    .line 105
    .local v0, "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    if-nez v2, :cond_1

    .line 106
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6, v10}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 113
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v7, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v8, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v6, v7, v8, v1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 115
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "title":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, p2}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 124
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getPersonDetails()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    move-result-object v3

    .line 125
    .local v3, "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    if-eqz v3, :cond_0

    .line 126
    iget-boolean v6, v3, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v6, :cond_2

    .line 127
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v6, v10}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setVisibility(I)V

    .line 132
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 133
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfileText:Landroid/widget/TextView;

    const v7, 0x7f0c0394

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    invoke-virtual {v6, p0}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    const v7, 0x7f0c03b3

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v9

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->hideSeparator()V

    .line 139
    return-void

    .line 108
    .end local v3    # "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    .end local v4    # "res":Landroid/content/res/Resources;
    .end local v5    # "title":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6, v9}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 109
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v7, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v8, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v6, v7, v8, v1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_0

    .line 129
    .restart local v3    # "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    .restart local v5    # "title":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v6, v9}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public getOverlayTransparencyHeightFromTop()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public getOverlayableImageHeight()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/HeroImageFrame;->getHeight()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 236
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/finsky/activities/MainActivity;

    if-nez v1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/MainActivity;

    .line 241
    .local v0, "mainActivity":Lcom/google/android/finsky/activities/MainActivity;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    if-ne p1, v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    .line 243
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x119

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 246
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->launchGPlusProfileView(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;->onFinishInflate()V

    .line 74
    const v4, 0x7f0a02ac

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/HeroImageFrame;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    .line 75
    const v4, 0x7f0a02ad

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/FifeImageView;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 76
    const v4, 0x7f0a02ae

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrameOverlay:Landroid/view/View;

    .line 77
    const v4, 0x7f0a0141

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/FifeImageView;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    .line 78
    const v4, 0x7f0a02af

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    .line 79
    const v4, 0x7f0a02b0

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    .line 80
    const v4, 0x7f0a02b1

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    .line 81
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    const v5, 0x7f0a02b2

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfileText:Landroid/widget/TextView;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 85
    .local v0, "res":Landroid/content/res/Resources;
    const v4, 0x7f0b016d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 86
    .local v3, "textShadowOffset":I
    const v4, 0x7f0b016c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 87
    .local v1, "textShadowAmount":I
    const v4, 0x7f09009f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 88
    .local v2, "textShadowColor":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfileText:Landroid/widget/TextView;

    int-to-float v5, v1

    const/4 v6, 0x0

    int-to-float v7, v3

    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 89
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 23
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->getWidth()I

    move-result v18

    .line 200
    .local v18, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/HeroImageFrame;->getMeasuredHeight()I

    move-result v13

    .line 201
    .local v13, "coverHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3, v13}, Lcom/google/android/finsky/layout/HeroImageFrame;->layout(IIII)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrameOverlay:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrameOverlay:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v18

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v5

    .line 206
    .local v5, "avatarHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v8

    .line 207
    .local v8, "avatarWidth":I
    int-to-float v0, v5

    move/from16 v19, v0

    const v20, 0x3f333333    # 0.7f

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    sub-int v7, v13, v19

    .line 208
    .local v7, "avatarTop":I
    sub-int v19, v18, v8

    div-int/lit8 v6, v19, 0x2

    .line 209
    .local v6, "avatarLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    add-int v20, v6, v8

    add-int v21, v7, v5

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v7, v1, v2}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/image/FifeImageView;->getBottom()I

    move-result v14

    .line 213
    .local v14, "displayNameTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v21

    add-int v21, v21, v14

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    move/from16 v3, v21

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getVisibility()I

    move-result v19

    if-nez v19, :cond_0

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 219
    .local v9, "circleStatusButtonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getMeasuredWidth()I

    move-result v12

    .line 220
    .local v12, "circleStatusWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getBottom()I

    move-result v19

    iget v0, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v20, v0

    add-int v11, v19, v20

    .line 221
    .local v11, "circleStatusTop":I
    sub-int v19, v18, v12

    div-int/lit8 v10, v19, 0x2

    .line 222
    .local v10, "circleStatusLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    move-object/from16 v19, v0

    add-int v20, v10, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getMeasuredHeight()I

    move-result v21

    add-int v21, v21, v11

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v10, v11, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->layout(IIII)V

    .line 227
    .end local v9    # "circleStatusButtonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v10    # "circleStatusLeft":I
    .end local v11    # "circleStatusTop":I
    .end local v12    # "circleStatusWidth":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->getMeasuredWidth()I

    move-result v17

    .line 228
    .local v17, "viewProfilePackWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->getMeasuredHeight()I

    move-result v16

    .line 229
    .local v16, "viewProfilePackHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mContentHorizontalPadding:I

    move/from16 v19, v0

    sub-int v19, v18, v19

    sub-int v15, v19, v17

    .line 230
    .local v15, "profilePackLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    move-object/from16 v19, v0

    sub-int v20, v13, v16

    add-int v21, v15, v17

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v15, v1, v2, v13}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->layout(IIII)V

    .line 232
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 158
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 159
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v3, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 160
    .local v3, "screenHeight":I
    iget v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mIdealCoverHeight:I

    div-int/lit8 v8, v3, 0x3

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 162
    .local v2, "coverHeight":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, p1, v8}, Lcom/google/android/finsky/layout/HeroImageFrame;->measure(II)V

    .line 164
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrameOverlay:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrameOverlay:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, p1, v8}, Landroid/view/View;->measure(II)V

    .line 167
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 168
    .local v0, "avatarLp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    iget v8, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 171
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v10}, Landroid/widget/TextView;->measure(II)V

    .line 172
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v7, v10, v10}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->measure(II)V

    .line 176
    iget v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mContentHorizontalPadding:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v6, v7

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v5, v7, 0x2

    .line 178
    .local v5, "viewProfilePackMaxWidth":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mViewProfilePack:Lcom/google/android/finsky/layout/AccessibleLinearLayout;

    const/high16 v8, -0x80000000

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, v10}, Lcom/google/android/finsky/layout/AccessibleLinearLayout;->measure(II)V

    .line 182
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 185
    .local v1, "circleStatusButtonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3e99999a    # 0.3f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    add-int/2addr v7, v2

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    add-int v4, v7, v8

    .line 188
    .local v4, "totalHeight":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getVisibility()I

    move-result v7

    if-nez v7, :cond_0

    .line 189
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getMeasuredHeight()I

    move-result v7

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v7, v8

    add-int/2addr v4, v7

    .line 193
    :cond_0
    invoke-virtual {p0, v6, v4}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->setMeasuredDimension(II)V

    .line 194
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 273
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 274
    return-void
.end method

.method public setOverlayableImageTopPadding(I)V
    .locals 2
    .param p1, "topPadding"    # I

    .prologue
    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->mProfileCoverImageFrame:Lcom/google/android/finsky/layout/HeroImageFrame;

    invoke-virtual {v0, v1, p1, v1, v1}, Lcom/google/android/finsky/layout/HeroImageFrame;->setPadding(IIII)V

    .line 154
    return-void
.end method
