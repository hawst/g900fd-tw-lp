.class Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;
.super Ljava/lang/Object;
.source "MyWishlistFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->rebindAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

.field final synthetic val$isRestoring:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;Z)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    iput-boolean p2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->val$isRestoring:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$000(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$100(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$000(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$100(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 279
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->val$isRestoring:Z

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$100(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$000(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$200(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$200(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$000(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    # getter for: Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->access$300(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f0a026e

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setEmptyView(Landroid/view/View;)V

    .line 290
    :cond_1
    return-void
.end method
