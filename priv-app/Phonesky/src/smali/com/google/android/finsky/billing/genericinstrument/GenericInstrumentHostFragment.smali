.class public Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
.super Landroid/support/v4/app/Fragment;
.source "GenericInstrumentHostFragment.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;
    }
.end annotation


# instance fields
.field private mLastStateInstance:I

.field private mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

.field private mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    return-void
.end method

.method private getListener()Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;

    return-object v0
.end method

.method private handleError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorHtml"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "accountName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getSubstate()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 161
    .local v1, "isTerminalError":Z
    :goto_0
    invoke-static {v0, p1, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 163
    return-void

    .line 159
    .end local v1    # "isTerminalError":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleFlowState(Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;)V
    .locals 3
    .param p1, "flowState"    # Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .prologue
    .line 167
    iget-object v1, p1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    if-eqz v1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 175
    return-void

    .line 173
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unsupported form"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private handleRunning()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/ProgressSpinnerFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mProgressSpinnerFragment:Lcom/google/android/finsky/billing/ProgressSpinnerFragment;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 151
    return-void
.end method

.method private handleSuccess(Ljava/lang/String;)V
    .locals 1
    .param p1, "instrumentId"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getListener()Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;->onFinished(Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "genericInstrument"    # Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .prologue
    .line 62
    new-instance v1, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;-><init>()V

    .line 63
    .local v1, "result":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v2, "GenericInstrumentFlowFragment.generic_instrument"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method

.method private showFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 180
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 181
    const/16 v1, 0x1003

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 182
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 183
    return-void
.end method


# virtual methods
.method public acknowledgeTerminalError()V
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getListener()Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment$Listener;->onFinished(Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method public createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;)V

    .line 196
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const-string v0, "GenericInstrumentFlowFragment.last_state_instance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mLastStateInstance:I

    .line 76
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    const v0, 0x7f040032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 114
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 108
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    const-string v0, "GenericInstrumentFlowFragment.last_state_instance"

    iget v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mLastStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    return-void
.end method

.method public onStart()V
    .locals 6

    .prologue
    .line 92
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "generic_instrument_sidecar"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    iput-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    .line 94
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    if-nez v3, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 96
    .local v1, "args":Landroid/os/Bundle;
    const-string v3, "authAccount"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "accountName":Ljava/lang/String;
    const-string v3, "GenericInstrumentFlowFragment.generic_instrument"

    invoke-static {v1, v3}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 99
    .local v2, "genericInstrument":Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    invoke-static {v0, v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const-string v5, "generic_instrument_sidecar"

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 102
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v2    # "genericInstrument":Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;
    :cond_0
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v3, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getStateInstance()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mLastStateInstance:I

    if-gt v0, v1, :cond_0

    .line 119
    const-string v0, "Already received state instance %d, ignore."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mLastStateInstance:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getStateInstance()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mLastStateInstance:I

    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->requestInitialInstrumentFlowState()V

    goto :goto_0

    .line 129
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->handleRunning()V

    goto :goto_0

    .line 132
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentFlowState()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->handleFlowState(Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;)V

    goto :goto_0

    .line 135
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getErrorHtml()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->handleError(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->handleSuccess(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public retryAfterError()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->mSidecar:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->retryAfterError()V

    .line 210
    return-void
.end method
