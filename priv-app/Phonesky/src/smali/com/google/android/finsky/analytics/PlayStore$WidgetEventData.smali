.class public final Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WidgetEventData"
.end annotation


# instance fields
.field public backendId:I

.field public classId:I

.field public hasBackendId:Z

.field public hasClassId:Z

.field public hasIntentActionId:Z

.field public hasMaxHeight:Z

.field public hasMaxWidth:Z

.field public hasMinHeight:Z

.field public hasMinWidth:Z

.field public hasNumWidgets:Z

.field public intentActionId:I

.field public maxHeight:I

.field public maxWidth:I

.field public minHeight:I

.field public minWidth:I

.field public numWidgets:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4699
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4700
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->clear()Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    .line 4701
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4704
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    .line 4705
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasClassId:Z

    .line 4706
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    .line 4707
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasIntentActionId:Z

    .line 4708
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    .line 4709
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasNumWidgets:Z

    .line 4710
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    .line 4711
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasBackendId:Z

    .line 4712
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    .line 4713
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinWidth:Z

    .line 4714
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    .line 4715
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinHeight:Z

    .line 4716
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    .line 4717
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxWidth:Z

    .line 4718
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    .line 4719
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxHeight:Z

    .line 4720
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->cachedSize:I

    .line 4721
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4756
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4757
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasClassId:Z

    if-eqz v1, :cond_1

    .line 4758
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4761
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasIntentActionId:Z

    if-eqz v1, :cond_3

    .line 4762
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4765
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasNumWidgets:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    if-eqz v1, :cond_5

    .line 4766
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4769
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasBackendId:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    if-eqz v1, :cond_7

    .line 4770
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4773
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinWidth:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    if-eqz v1, :cond_9

    .line 4774
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4777
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinHeight:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    if-eqz v1, :cond_b

    .line 4778
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4781
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxWidth:Z

    if-nez v1, :cond_c

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    if-eqz v1, :cond_d

    .line 4782
    :cond_c
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4785
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxHeight:Z

    if-nez v1, :cond_e

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    if-eqz v1, :cond_f

    .line 4786
    :cond_e
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4789
    :cond_f
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 4797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4798
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4802
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4803
    :sswitch_0
    return-object p0

    .line 4808
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4809
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 4814
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    .line 4815
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasClassId:Z

    goto :goto_0

    .line 4821
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4822
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 4828
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    .line 4829
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasIntentActionId:Z

    goto :goto_0

    .line 4835
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    .line 4836
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasNumWidgets:Z

    goto :goto_0

    .line 4840
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    .line 4841
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasBackendId:Z

    goto :goto_0

    .line 4845
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    .line 4846
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinWidth:Z

    goto :goto_0

    .line 4850
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    .line 4851
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinHeight:Z

    goto :goto_0

    .line 4855
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    .line 4856
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxWidth:Z

    goto :goto_0

    .line 4860
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    .line 4861
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxHeight:Z

    goto :goto_0

    .line 4798
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    .line 4809
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 4822
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4637
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4727
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasClassId:Z

    if-eqz v0, :cond_1

    .line 4728
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->classId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4730
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasIntentActionId:Z

    if-eqz v0, :cond_3

    .line 4731
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->intentActionId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4733
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasNumWidgets:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    if-eqz v0, :cond_5

    .line 4734
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->numWidgets:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4736
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasBackendId:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    if-eqz v0, :cond_7

    .line 4737
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->backendId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4739
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinWidth:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    if-eqz v0, :cond_9

    .line 4740
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minWidth:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4742
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMinHeight:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    if-eqz v0, :cond_b

    .line 4743
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->minHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4745
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxWidth:Z

    if-nez v0, :cond_c

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    if-eqz v0, :cond_d

    .line 4746
    :cond_c
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxWidth:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4748
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->hasMaxHeight:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    if-eqz v0, :cond_f

    .line 4749
    :cond_e
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;->maxHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4751
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4752
    return-void
.end method
