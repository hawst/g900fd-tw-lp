.class public Lcom/google/android/finsky/layout/DiscoveryBar;
.super Landroid/widget/LinearLayout;
.source "DiscoveryBar.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mBadgeContainer:Landroid/view/ViewGroup;

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mDiscoveryBadges:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DiscoveryBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/16 v0, 0x708

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 57
    return-void
.end method

.method private populateView()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 97
    invoke-virtual {p0, v12}, Lcom/google/android/finsky/layout/DiscoveryBar;->setVisibility(I)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DiscoveryBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 101
    .local v10, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 103
    iget-object v8, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDiscoveryBadges:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .local v8, "arr$":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    array-length v11, v8

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v11, :cond_4

    aget-object v1, v8, v9

    .line 105
    .local v1, "badge":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    iget-boolean v2, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasAggregateRating:Z

    if-eqz v2, :cond_0

    .line 106
    const v2, 0x7f040086

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;

    .line 122
    .local v0, "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 124
    iget-object v2, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 103
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    :cond_0
    iget-boolean v2, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->isPlusOne:Z

    if-eqz v2, :cond_1

    .line 109
    const v2, 0x7f040087

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;

    .restart local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    goto :goto_1

    .line 111
    .end local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    :cond_1
    iget-boolean v2, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasUserStarRating:Z

    if-eqz v2, :cond_2

    .line 112
    const v2, 0x7f040088

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialRating;

    .restart local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    goto :goto_1

    .line 114
    .end local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    :cond_2
    iget-boolean v2, v1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->hasDownloadCount:Z

    if-eqz v2, :cond_3

    .line 115
    const v2, 0x7f040084

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeDowloadCount;

    .restart local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    goto :goto_1

    .line 119
    .end local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    :cond_3
    const v2, 0x7f040085

    iget-object v3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v3, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;

    .restart local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    goto :goto_1

    .line 126
    .end local v0    # "badgeView":Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
    .end local v1    # "badge":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    :cond_4
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 140
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 141
    return-void
.end method

.method public configure(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "discoveryBadges"    # [Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "hasDetailsLoaded"    # Z
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 68
    if-eqz p7, :cond_0

    if-eqz p4, :cond_0

    array-length v0, p4

    if-nez v0, :cond_1

    .line 71
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DiscoveryBar;->setVisibility(I)V

    .line 88
    :goto_0
    return-void

    .line 75
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DiscoveryBar;->setVisibility(I)V

    .line 77
    iput-object p3, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 78
    iput-object p4, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDiscoveryBadges:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 79
    iput-object p2, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 80
    iput-object p1, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 81
    iput-object p5, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 82
    iput-object p6, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 83
    iput-object p8, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 87
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DiscoveryBar;->populateView()V

    goto :goto_0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 93
    const v0, 0x7f0a0113

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DiscoveryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DiscoveryBar;->mBadgeContainer:Landroid/view/ViewGroup;

    .line 94
    return-void
.end method
