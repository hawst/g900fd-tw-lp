.class public final Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ModifyCommentRequestProto"
.end annotation


# instance fields
.field public assetId:Ljava/lang/String;

.field public flagMessage:Ljava/lang/String;

.field public flagType:I

.field public hasAssetId:Z

.field public hasFlagMessage:Z

.field public hasFlagType:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 693
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    .line 694
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 697
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    .line 698
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasAssetId:Z

    .line 699
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    .line 700
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagType:Z

    .line 701
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    .line 702
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagMessage:Z

    .line 703
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->cachedSize:I

    .line 704
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 724
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 725
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasAssetId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 726
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 729
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagType:Z

    if-eqz v1, :cond_3

    .line 730
    :cond_2
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 733
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagMessage:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 734
    :cond_4
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 737
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 745
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 746
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 750
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 751
    :sswitch_0
    return-object p0

    .line 756
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    .line 757
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasAssetId:Z

    goto :goto_0

    .line 761
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 762
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 769
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    .line 770
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagType:Z

    goto :goto_0

    .line 776
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    .line 777
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagMessage:Z

    goto :goto_0

    .line 746
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch

    .line 762
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 655
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 710
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasAssetId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 711
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 713
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    if-ne v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagType:Z

    if-eqz v0, :cond_3

    .line 714
    :cond_2
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 716
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagMessage:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 717
    :cond_4
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 719
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 720
    return-void
.end method
