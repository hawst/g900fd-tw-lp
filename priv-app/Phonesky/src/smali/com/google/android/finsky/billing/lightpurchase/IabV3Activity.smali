.class public Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;
.super Lcom/google/android/finsky/billing/lightpurchase/IabActivity;
.source "IabV3Activity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/IabActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "params"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-static {p0, p1, v1, v1}, Lcom/google/android/finsky/billing/lightpurchase/IabActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 20
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 21
    return-object v0
.end method


# virtual methods
.method protected getResponseCodeForAlreadyOwned()Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ITEM_ALREADY_OWNED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    return-object v0
.end method

.method protected handleAccessRestriction()V
    .locals 4

    .prologue
    .line 51
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "RESPONSE_CODE"

    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_USER_CANCELED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 53
    .local v0, "data":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;->setResult(ILandroid/content/Intent;)V

    .line 54
    return-void
.end method

.method protected onFinish(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V
    .locals 7
    .param p1, "purchaseFragment"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 32
    .local v0, "data":Landroid/content/Intent;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;->getResponseCode(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v3

    .line 34
    .local v3, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getExtraPurchaseData()Landroid/os/Bundle;

    move-result-object v1

    .line 35
    .local v1, "extraPurchaseData":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 36
    const-string v5, "inapp_signed_purchase_data"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "purchaseData":Ljava/lang/String;
    const-string v5, "inapp_purchase_data_signature"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "signature":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    .line 41
    const-string v5, "INAPP_PURCHASE_DATA"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v5, "INAPP_DATA_SIGNATURE"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    .end local v2    # "purchaseData":Ljava/lang/String;
    .end local v4    # "signature":Ljava/lang/String;
    :cond_0
    const-string v5, "RESPONSE_CODE"

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->ordinal()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 46
    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-ne v3, v5, :cond_1

    const/4 v5, -0x1

    :goto_0
    invoke-virtual {p0, v5, v0}, Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;->setResult(ILandroid/content/Intent;)V

    .line 47
    return-void

    .line 46
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
