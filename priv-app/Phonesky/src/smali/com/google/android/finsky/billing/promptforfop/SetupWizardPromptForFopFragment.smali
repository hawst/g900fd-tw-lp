.class public Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;
.super Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;
.source "SetupWizardPromptForFopFragment.java"


# instance fields
.field private mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/accounts/Account;[BLcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "serverLogsCookie"    # [B
    .param p2, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .prologue
    .line 37
    new-instance v1, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;-><init>()V

    .line 38
    .local v1, "result":Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;
    invoke-static {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->buildArgumentsBundle(Landroid/accounts/Account;[B)Landroid/os/Bundle;

    move-result-object v0

    .line 39
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "setup_wizard_params"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 40
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method protected addCreditCard()V
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;

    move-result-object v0

    .line 106
    .local v0, "addCreditCardIntent":Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 108
    return-void
.end method

.method protected addDcb3(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)V
    .locals 3
    .param p1, "instrumentStatus"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-static {v1, p1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;

    move-result-object v0

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 124
    return-void
.end method

.method protected configureContinueButtonStyle(Landroid/view/View;)V
    .locals 3
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 58
    instance-of v1, p1, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 59
    check-cast v0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;

    .line 60
    .local v0, "iconButton":Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    const v1, 0x7f0c020d

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->setText(Ljava/lang/String;)V

    .line 63
    .end local v0    # "iconButton":Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;
    :cond_0
    return-void
.end method

.method protected determineUiMode()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 82
    invoke-static {}, Lcom/google/android/finsky/utils/SetupWizardUtils;->shouldUseMaterialTheme()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    const/4 v1, 0x6

    .line 98
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    .line 87
    .local v0, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    const-string v2, "cl:billing.setupwizard_prompt_for_fop_ui_mode_radio_button"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 90
    const-string v2, "cl:billing.setupwizard_prompt_for_fop_ui_mode_billing_profile"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 92
    const/4 v1, 0x2

    goto :goto_0

    .line 93
    :cond_2
    const-string v2, "cl:billing.setupwizard_prompt_for_fop_ui_mode_billing_profile_more_details"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    const/4 v1, 0x4

    goto :goto_0

    .line 97
    :cond_3
    const-string v2, "No UI mode specified, defaulting to UI_MODE_RADIO_BUTTONS"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getActionEntryLayout()I
    .locals 1

    .prologue
    .line 72
    const v0, 0x7f0401a3

    return v0
.end method

.method protected getBillingProfileRequestEnum()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x3

    return v0
.end method

.method protected getCreditCardEventType()I
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x168

    return v0
.end method

.method protected getDcb2Action(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;)Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .locals 2
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .prologue
    .line 130
    const-string v0, "Dcb entry not shown because the api version is dcb2."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDcbEventType()I
    .locals 1

    .prologue
    .line 146
    const/16 v0, 0x169

    return v0
.end method

.method protected getMainLayout()I
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f0401a6

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 77
    const/16 v0, 0x37d

    return v0
.end method

.method protected getPrimerStringId()I
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f0c00a2

    return v0
.end method

.method protected getRedeemEventType()I
    .locals 1

    .prologue
    .line 151
    const/16 v0, 0x16a

    return v0
.end method

.method protected getTopupEventType()I
    .locals 1

    .prologue
    .line 156
    const/16 v0, 0x16b

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "setup_wizard_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 49
    return-void
.end method

.method protected redeemCheckoutCode()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;

    move-result-object v0

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 116
    return-void
.end method

.method protected supportsGenericInstruments()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method protected topup(Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)V
    .locals 2
    .param p1, "topupInfo"    # Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .prologue
    .line 136
    const-string v0, "Top-up is not supported for Setup Wizard."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    return-void
.end method
