.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "SmsCodeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;
    }
.end annotation


# instance fields
.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

.field private mCodeEntry:Landroid/widget/EditText;

.field private mMainView:Landroid/view/View;

.field private mResendButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->syncSubmitButtonState()V

    return-void
.end method

.method private getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    .line 171
    :goto_0
    return-object v0

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    if-eqz v0, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    goto :goto_0

    .line 173
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener registered."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .prologue
    .line 64
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v2, "SmsCodeFragment.backend"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v2, "SmsCodeFragment.challenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 68
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;-><init>()V

    .line 69
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 70
    return-object v1
.end method

.method private syncSubmitButtonState()V
    .locals 2

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 161
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 178
    const/16 v0, 0x57b

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mResendButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v0, :cond_1

    .line 184
    const/16 v0, 0x57d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->logClickEvent(I)V

    .line 185
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;->onResendSmsCode(Ljava/lang/String;)V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v0, :cond_0

    .line 187
    const/16 v0, 0x580

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->logClickEvent(I)V

    .line 188
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$Listener;->onVerifySmsCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SmsCodeFragment.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .line 77
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x8

    .line 82
    const v4, 0x7f040021

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    .line 85
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a009c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 86
    .local v3, "title":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 87
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a00bb

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 93
    .local v1, "description":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 94
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a00c5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    .line 100
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v4, :cond_5

    .line 101
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 102
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 104
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 105
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mCodeEntry:Landroid/widget/EditText;

    new-instance v5, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 119
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a00c6

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 120
    .local v2, "errorMessageView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 121
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "SmsCodeFragment.backend"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 130
    .local v0, "backend":I
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a00f8

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 131
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 132
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    const v5, 0x7f0a0115

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mResendButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 137
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 139
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mResendButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {v4, v0, v5, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 144
    :goto_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->syncSubmitButtonState()V

    .line 146
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    return-object v4

    .line 89
    .end local v0    # "backend":I
    .end local v1    # "description":Landroid/widget/TextView;
    .end local v2    # "errorMessageView":Landroid/widget/TextView;
    :cond_2
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "No title returned"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 96
    .restart local v1    # "description":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 123
    .restart local v2    # "errorMessageView":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 126
    .end local v2    # "errorMessageView":Landroid/widget/TextView;
    :cond_5
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "No SMS code field returned."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 134
    .restart local v0    # "backend":I
    .restart local v2    # "errorMessageView":Landroid/widget/TextView;
    :cond_6
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "No submit button returned."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 141
    :cond_7
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mResendButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v4, v6}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;->onResume()V

    .line 155
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 156
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/SmsCodeFragment;->mMainView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 157
    return-void
.end method
