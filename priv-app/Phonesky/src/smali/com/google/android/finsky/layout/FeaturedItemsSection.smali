.class public Lcom/google/android/finsky/layout/FeaturedItemsSection;
.super Lcom/google/android/finsky/layout/DetailsPackSection;
.source "FeaturedItemsSection.java"


# instance fields
.field private mHasDetailsLoaded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/FeaturedItemsSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsPackSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mHasDetailsLoaded:Z

    .line 28
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "subheader"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "moreUrl"    # Ljava/lang/String;
    .param p6, "maxItemsPerRow"    # I
    .param p7, "maxRowCount"    # I
    .param p8, "hasDetailsLoaded"    # Z
    .param p9, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 34
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-super/range {v1 .. v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 35
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mHasDetailsLoaded:Z

    .line 36
    return-void
.end method

.method protected populateContent()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 48
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 50
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 52
    .local v9, "itemCount":I
    if-nez v9, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FeaturedItemsSection;->handleEmptyData()V

    .line 105
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mListingContent:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FeaturedItemsSection;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 61
    .local v8, "inflater":Landroid/view/LayoutInflater;
    packed-switch v9, :pswitch_data_0

    .line 70
    const v7, 0x7f04011f

    .line 73
    .local v7, "clusterLayoutId":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v7, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 75
    .local v11, "layout":Landroid/view/ViewGroup;
    const/4 v10, 0x0

    .local v10, "itemIndex":I
    :goto_2
    if-ge v10, v9, :cond_2

    .line 77
    packed-switch v10, :pswitch_data_1

    .line 88
    const v6, 0x7f0a02c4

    .line 91
    .local v6, "cardLayoutId":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2, v10}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 92
    .local v1, "data":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v11, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;

    .line 94
    .local v0, "childCard":Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
    if-eqz v1, :cond_1

    .line 95
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getInitialUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/FeaturedItemsSection;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {v1, v2, v0, p0, v3}, Lcom/google/android/finsky/utils/PlayCardUtils;->updatePrimaryActionButton(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 99
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mHasDetailsLoaded:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Z)V

    .line 75
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 63
    .end local v0    # "childCard":Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
    .end local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "cardLayoutId":I
    .end local v7    # "clusterLayoutId":I
    .end local v10    # "itemIndex":I
    .end local v11    # "layout":Landroid/view/ViewGroup;
    :pswitch_0
    const v7, 0x7f04011d

    .line 64
    .restart local v7    # "clusterLayoutId":I
    goto :goto_1

    .line 66
    .end local v7    # "clusterLayoutId":I
    :pswitch_1
    const v7, 0x7f04011e

    .line 67
    .restart local v7    # "clusterLayoutId":I
    goto :goto_1

    .line 79
    .restart local v10    # "itemIndex":I
    .restart local v11    # "layout":Landroid/view/ViewGroup;
    :pswitch_2
    const v6, 0x7f0a02c4

    .line 80
    .restart local v6    # "cardLayoutId":I
    goto :goto_3

    .line 82
    .end local v6    # "cardLayoutId":I
    :pswitch_3
    const v6, 0x7f0a02c5

    .line 83
    .restart local v6    # "cardLayoutId":I
    goto :goto_3

    .line 85
    .end local v6    # "cardLayoutId":I
    :pswitch_4
    const v6, 0x7f0a02c6

    .line 86
    .restart local v6    # "cardLayoutId":I
    goto :goto_3

    .line 101
    .restart local v0    # "childCard":Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
    .restart local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->clearCardState()V

    goto :goto_4

    .line 104
    .end local v0    # "childCard":Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
    .end local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "cardLayoutId":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/FeaturedItemsSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 77
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
