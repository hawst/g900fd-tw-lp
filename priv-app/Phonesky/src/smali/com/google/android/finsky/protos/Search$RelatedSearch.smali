.class public final Lcom/google/android/finsky/protos/Search$RelatedSearch;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Search.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Search;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RelatedSearch"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Search$RelatedSearch;


# instance fields
.field public backendId:I

.field public current:Z

.field public docType:I

.field public hasBackendId:Z

.field public hasCurrent:Z

.field public hasDocType:Z

.field public hasHeader:Z

.field public hasSearchUrl:Z

.field public header:Ljava/lang/String;

.field public searchUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 328
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Search$RelatedSearch;->clear()Lcom/google/android/finsky/protos/Search$RelatedSearch;

    .line 329
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .locals 2

    .prologue
    .line 296
    sget-object v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->_emptyArray:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    if-nez v0, :cond_1

    .line 297
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->_emptyArray:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    if-nez v0, :cond_0

    .line 300
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Search$RelatedSearch;

    sput-object v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->_emptyArray:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    .line 302
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->_emptyArray:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    return-object v0

    .line 302
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 332
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    .line 333
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasSearchUrl:Z

    .line 334
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    .line 335
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasHeader:Z

    .line 336
    iput v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    .line 337
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasBackendId:Z

    .line 338
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    .line 339
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasDocType:Z

    .line 340
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    .line 341
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasCurrent:Z

    .line 342
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->cachedSize:I

    .line 343
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 369
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 370
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasSearchUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 371
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 374
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasHeader:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 375
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 378
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasBackendId:Z

    if-eqz v1, :cond_5

    .line 379
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    :cond_5
    iget v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasDocType:Z

    if-eqz v1, :cond_7

    .line 383
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 386
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasCurrent:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    if-eqz v1, :cond_9

    .line 387
    :cond_8
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 390
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 399
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 403
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 404
    :sswitch_0
    return-object p0

    .line 409
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    .line 410
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasSearchUrl:Z

    goto :goto_0

    .line 414
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    .line 415
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasHeader:Z

    goto :goto_0

    .line 419
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 420
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 432
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    .line 433
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasBackendId:Z

    goto :goto_0

    .line 439
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 440
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 473
    :pswitch_2
    iput v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    .line 474
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasDocType:Z

    goto :goto_0

    .line 480
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    .line 481
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasCurrent:Z

    goto :goto_0

    .line 399
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 420
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 440
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Search$RelatedSearch;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Search$RelatedSearch;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 349
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasSearchUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->searchUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 352
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasHeader:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 353
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->header:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 355
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasBackendId:Z

    if-eqz v0, :cond_5

    .line 356
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->backendId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 358
    :cond_5
    iget v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    if-ne v0, v2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasDocType:Z

    if-eqz v0, :cond_7

    .line 359
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->docType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 361
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->hasCurrent:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    if-eqz v0, :cond_9

    .line 362
    :cond_8
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Search$RelatedSearch;->current:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 364
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 365
    return-void
.end method
