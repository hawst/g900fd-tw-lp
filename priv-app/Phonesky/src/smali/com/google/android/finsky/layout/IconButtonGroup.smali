.class public Lcom/google/android/finsky/layout/IconButtonGroup;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "IconButtonGroup.java"


# instance fields
.field private mAllowIcon:Z

.field private mContinueButtonIcon:Landroid/widget/ImageView;

.field private mContinueButtonLabel:Landroid/widget/TextView;

.field private mLabelBackendId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/IconButtonGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mAllowIcon:Z

    .line 29
    iput v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mLabelBackendId:I

    .line 43
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setWillNotDraw(Z)V

    .line 44
    return-void
.end method

.method private updateIconVisibility()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mAllowIcon:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 107
    .local v0, "visible":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    invoke-direct {p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->updateLabelBackground()V

    .line 109
    return-void

    .end local v0    # "visible":Z
    :cond_0
    move v0, v1

    .line 106
    goto :goto_0

    .line 107
    .restart local v0    # "visible":Z
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private updateLabelBackground()V
    .locals 4

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 116
    .local v1, "ctx":Landroid/content/Context;
    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mLabelBackendId:I

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPlayActionButtonBaseBackgroundDrawable(Landroid/content/Context;I)I

    move-result v0

    .line 119
    .local v0, "backgroundResourceId":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 120
    return-void

    .line 116
    .end local v0    # "backgroundResourceId":I
    :cond_0
    iget v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mLabelBackendId:I

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPlayActionButtonStartBackgroundDrawable(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 57
    .local v0, "result":Z
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 58
    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 50
    const v0, 0x7f0a0233

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/IconButtonGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0a0232

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/IconButtonGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    .line 52
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "x":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 149
    iget-object v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 151
    iget-object v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 156
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 124
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 125
    .local v1, "iconLp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 127
    .local v2, "labelLp":Landroid/view/ViewGroup$LayoutParams;
    const/4 v3, 0x0

    .line 128
    .local v3, "width":I
    const/4 v0, 0x0

    .line 129
    .local v0, "height":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 130
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 133
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    .line 134
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    .line 136
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    const/4 v5, 0x0

    iget v6, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 139
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 140
    iget-object v4, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 142
    invoke-virtual {p0, v3, v0}, Lcom/google/android/finsky/layout/IconButtonGroup;->setMeasuredDimension(II)V

    .line 143
    return-void
.end method

.method public setAllowIcon(Z)V
    .locals 0
    .param p1, "allowIcon"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mAllowIcon:Z

    .line 102
    invoke-direct {p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->updateIconVisibility()V

    .line 103
    return-void
.end method

.method public setBackendForLabel(I)V
    .locals 0
    .param p1, "backend"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mLabelBackendId:I

    .line 85
    invoke-direct {p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->updateLabelBackground()V

    .line 86
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setEnabled(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 67
    iget-object v1, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 69
    return-void

    .line 67
    :cond_0
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    invoke-direct {p0}, Lcom/google/android/finsky/layout/IconButtonGroup;->updateIconVisibility()V

    .line 94
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/IconButtonGroup;->mContinueButtonLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method
