.class public final Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextFormField"
.end annotation


# instance fields
.field public contentType:I

.field public defaultValue:Ljava/lang/String;

.field public errorMessage:Ljava/lang/String;

.field public hasContentType:Z

.field public hasDefaultValue:Z

.field public hasErrorMessage:Z

.field public hasLabel:Z

.field public hasUseBestGuess:Z

.field public label:Ljava/lang/String;

.field public regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

.field public responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

.field public useBestGuess:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1294
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1295
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->clear()Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .line 1296
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1299
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    .line 1300
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasLabel:Z

    .line 1301
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    .line 1302
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasContentType:Z

    .line 1303
    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    .line 1304
    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    .line 1305
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    .line 1306
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasErrorMessage:Z

    .line 1307
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    .line 1308
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasDefaultValue:Z

    .line 1309
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    .line 1310
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasUseBestGuess:Z

    .line 1311
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->cachedSize:I

    .line 1312
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1344
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1345
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasLabel:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1346
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1349
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasContentType:Z

    if-eqz v1, :cond_3

    .line 1350
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1353
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    if-eqz v1, :cond_4

    .line 1354
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1357
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    if-eqz v1, :cond_5

    .line 1358
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1361
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasErrorMessage:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1362
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1365
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasDefaultValue:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1366
    :cond_8
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1369
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasUseBestGuess:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    if-eqz v1, :cond_b

    .line 1370
    :cond_a
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1373
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1381
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1382
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1386
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1387
    :sswitch_0
    return-object p0

    .line 1392
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    .line 1393
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasLabel:Z

    goto :goto_0

    .line 1397
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1398
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1408
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    .line 1409
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasContentType:Z

    goto :goto_0

    .line 1415
    .end local v1    # "value":I
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    if-nez v2, :cond_1

    .line 1416
    new-instance v2, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    .line 1418
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1422
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    if-nez v2, :cond_2

    .line 1423
    new-instance v2, Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    .line 1425
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1429
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    .line 1430
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasErrorMessage:Z

    goto :goto_0

    .line 1434
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    .line 1435
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasDefaultValue:Z

    goto :goto_0

    .line 1439
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    .line 1440
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasUseBestGuess:Z

    goto :goto_0

    .line 1382
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    .line 1398
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1240
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1318
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasLabel:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1319
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->label:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1321
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasContentType:Z

    if-eqz v0, :cond_3

    .line 1322
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->contentType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1324
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    if-eqz v0, :cond_4

    .line 1325
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->responseFormat:Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1327
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    if-eqz v0, :cond_5

    .line 1328
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->regexValidation:Lcom/google/android/finsky/protos/CreateInstrument$FormInputRegexValidation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1330
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasErrorMessage:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1331
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1333
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasDefaultValue:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1334
    :cond_8
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->defaultValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1336
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->hasUseBestGuess:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    if-eqz v0, :cond_b

    .line 1337
    :cond_a
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;->useBestGuess:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1339
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1340
    return-void
.end method
