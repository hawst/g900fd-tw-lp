.class public Lcom/google/android/finsky/api/model/DfeSearch;
.super Lcom/google/android/finsky/api/model/ContainerList;
.source "DfeSearch.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/ContainerList",
        "<",
        "Lcom/google/android/finsky/protos/Search$SearchResponse;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/api/model/DfeSearch;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAggregatedQuery:Ljava/lang/Boolean;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mFullPageReplaced:Z

.field private final mInitialUrl:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field private mSuggestedQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lcom/google/android/finsky/api/model/DfeSearch$1;

    invoke-direct {v0}, Lcom/google/android/finsky/api/model/DfeSearch$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/api/model/DfeSearch;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p3}, Lcom/google/android/finsky/api/model/ContainerList;-><init>(Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mFullPageReplaced:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    .line 72
    iput-object p3, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mInitialUrl:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 74
    iput-object p2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mQuery:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private constructor <init>(Ljava/util/List;ILjava/lang/String;Ljava/lang/Boolean;)V
    .locals 3
    .param p2, "currentCount"    # I
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "isAggregatedQuery"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;",
            ">;I",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "urlList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;>;"
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 58
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/finsky/api/model/ContainerList;-><init>(Ljava/util/List;IZ)V

    .line 40
    iput-boolean v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mFullPageReplaced:Z

    .line 46
    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    .line 59
    iput-object p3, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mQuery:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    .line 61
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    iget-object v0, v0, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mInitialUrl:Ljava/lang/String;

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;ILjava/lang/String;Ljava/lang/Boolean;Lcom/google/android/finsky/api/model/DfeSearch$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/Boolean;
    .param p5, "x4"    # Lcom/google/android/finsky/api/model/DfeSearch$1;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/api/model/DfeSearch;-><init>(Ljava/util/List;ILjava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method protected clearDiskCache()V
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public getBackendId()I
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeSearch;->isAggregateResult()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v0

    goto :goto_0
.end method

.method protected getItemsFromResponse(Lcom/google/android/finsky/protos/Search$SearchResponse;)[Lcom/google/android/finsky/api/model/Document;
    .locals 2
    .param p1, "response"    # Lcom/google/android/finsky/protos/Search$SearchResponse;

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 92
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->aggregateQuery:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    .line 94
    :cond_0
    iget-object v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 95
    iget-object v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->suggestedQuery:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mSuggestedQuery:Ljava/lang/String;

    .line 96
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->fullPageReplaced:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mFullPageReplaced:Z

    .line 99
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v0, v0

    if-nez v0, :cond_3

    .line 100
    :cond_2
    new-array v0, v1, [Lcom/google/android/finsky/api/model/Document;

    .line 104
    :goto_0
    return-object v0

    :cond_3
    iget-object v0, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/model/DfeSearch;->updateContainerAndGetItems(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)[Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic getItemsFromResponse(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/finsky/protos/Search$SearchResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeSearch;->getItemsFromResponse(Lcom/google/android/finsky/protos/Search$SearchResponse;)[Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getNextPageUrl(Lcom/google/android/finsky/protos/Search$SearchResponse;)Ljava/lang/String;
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/Search$SearchResponse;

    .prologue
    .line 166
    const/4 v1, 0x0

    .line 169
    .local v1, "nextPageUrl":Ljava/lang/String;
    iget-object v2, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 171
    iget-object v2, p1, Lcom/google/android/finsky/protos/Search$SearchResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 172
    .local v0, "dfeDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v2, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-eqz v2, :cond_0

    .line 173
    iget-object v2, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    iget-object v1, v2, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl:Ljava/lang/String;

    .line 176
    .end local v0    # "dfeDoc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic getNextPageUrl(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/finsky/protos/Search$SearchResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeSearch;->getNextPageUrl(Lcom/google/android/finsky/protos/Search$SearchResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getRelatedSearches()[Lcom/google/android/finsky/protos/Search$RelatedSearch;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mLastResponse:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/protos/Search$SearchResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Search$SearchResponse;->relatedSearch:[Lcom/google/android/finsky/protos/Search$RelatedSearch;

    return-object v0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mLastResponse:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mLastResponse:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/protos/Search$SearchResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    array-length v0, v0

    if-nez v0, :cond_1

    .line 184
    :cond_0
    const/4 v0, 0x0

    .line 186
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mLastResponse:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/protos/Search$SearchResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Search$SearchResponse;->serverLogsCookie:[B

    goto :goto_0
.end method

.method public getSuggestedQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mSuggestedQuery:Ljava/lang/String;

    return-object v0
.end method

.method public hasBackendId()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAggregateResult()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isFullPageReplaced()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mFullPageReplaced:Z

    return v0
.end method

.method protected makeRequest(Ljava/lang/String;)Lcom/android/volley/Request;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v0, p1, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->search(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 196
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 197
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mUrlOffsetList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;

    .line 198
    .local v1, "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    iget v2, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->offset:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget-object v2, v1, Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;->url:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    .end local v1    # "wrapper":Lcom/google/android/finsky/api/model/PaginatedList$UrlOffsetPair;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeSearch;->getCount()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 204
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    :goto_1
    return-void

    .line 206
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/api/model/DfeSearch;->mAggregatedQuery:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method
