.class public Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
.source "RedeemCodeFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mBillingUiMode:I

.field private mCodeScreenSkipped:Z

.field private mDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field private mLastStateInstance:I

.field private mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

.field private mRedemptionContext:I

.field private mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;-><init>()V

    .line 603
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    return-void
.end method

.method private finish()V
    .locals 3

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getListener()Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;

    move-result-object v0

    .line 282
    .local v0, "listener":Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;
    if-nez v0, :cond_0

    .line 283
    const-string v1, "No listener."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-interface {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;->onFinished()V

    goto :goto_0
.end method

.method private getListener()Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;
    .locals 1

    .prologue
    .line 574
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;

    if-eqz v0, :cond_0

    .line 575
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;

    .line 579
    :goto_0
    return-object v0

    .line 576
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;

    if-eqz v0, :cond_1

    .line 577
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;

    goto :goto_0

    .line 579
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private installAppIfNecessary(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)Z
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "appToInstall"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 364
    iget v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1}, Lcom/google/android/finsky/utils/DocUtils;->isInAppDocid(Lcom/google/android/finsky/protos/Common$Docid;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v2, v0

    .line 401
    :goto_0
    return v2

    .line 371
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v1

    iget-object v7, v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 372
    .local v7, "packageName":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v1

    iget v1, v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-static {v7, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->isAppCurrentVersion(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 374
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 376
    .local v6, "intent":Landroid/content/Intent;
    if-nez v6, :cond_1

    .line 378
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;

    move-result-object v6

    .line 380
    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    move v2, v0

    .line 381
    goto :goto_0

    .line 386
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_2
    iget v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v1, v2, :cond_3

    .line 390
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getRedeemedOfferHtml()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getStoredValueInstrumentId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getExtraPurchaseData()Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {v1, v3, v4, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;-><init>(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V

    iput-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    move v2, v0

    .line 393
    goto :goto_0

    :cond_3
    move-object v0, p1

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    .line 397
    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;[BLjava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 399
    .restart local v6    # "intent":Landroid/content/Intent;
    const/4 v0, 0x2

    invoke-virtual {p0, v6, v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private static isAppCurrentVersion(Ljava/lang/String;I)Z
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "currentVersionCode"    # I

    .prologue
    .line 405
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v0

    .line 406
    .local v0, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    if-lt v1, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;IIILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "redemptionContext"    # I
    .param p2, "backend"    # I
    .param p3, "mode"    # I
    .param p4, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p5, "offerType"    # I
    .param p6, "prefillCode"    # Ljava/lang/String;
    .param p7, "partnerPayload"    # Ljava/lang/String;

    .prologue
    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v2, "RedeemCodeFragment.redemption_context"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 155
    const-string v2, "RedeemCodeFragment.backend"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 157
    const-string v2, "RedeemCodeFragment.docid"

    invoke-static {p4}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 158
    const-string v2, "RedeemCodeFragment.offer_type"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 159
    const-string v2, "RedeemCodeFragment.prefill_code"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v2, "RedeemCodeFragment.partner_payload"

    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;-><init>()V

    .line 162
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 163
    return-object v1
.end method

.method private performSuccessAction(Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;)Z
    .locals 13
    .param p1, "postSuccessAction"    # Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 297
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 298
    .local v1, "account":Landroid/accounts/Account;
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    if-eqz v3, :cond_0

    .line 299
    new-instance v6, Lcom/google/android/finsky/api/model/Document;

    iget-object v0, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v6, v0}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 302
    .local v6, "appToInstall":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v1, v6}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->installAppIfNecessary(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    .line 351
    .end local v6    # "appToInstall":Lcom/google/android/finsky/api/model/Document;
    :goto_0
    return v0

    .line 303
    :cond_0
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    if-eqz v3, :cond_1

    .line 304
    new-instance v2, Lcom/google/android/finsky/api/model/Document;

    iget-object v0, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v2, v0}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 308
    .local v2, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const/4 v5, 0x1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/ConsumptionUtils;->openItem(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;I)Z

    move-result v0

    goto :goto_0

    .line 310
    .end local v2    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    if-eqz v3, :cond_2

    .line 313
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/IntentUtils;->createAggregatedHomeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v8

    .line 314
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 315
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_2
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    if-eqz v3, :cond_4

    .line 317
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedeemedItemDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v12

    .line 318
    .local v12, "voucherDoc":Lcom/google/android/finsky/api/model/Document;
    new-instance v10, Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v10, v3}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 319
    .local v10, "purchaseDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 321
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Apps are unsupported"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    iget v4, v4, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setVoucherId(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v9

    .line 328
    .local v9, "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v9, v5, v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    .line 330
    .restart local v8    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 331
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .end local v10    # "purchaseDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v12    # "voucherDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_4
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    if-eqz v3, :cond_7

    .line 333
    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    iget-object v11, v3, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 334
    .local v11, "resolvedLink":Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    if-eqz v11, :cond_6

    .line 335
    iget-object v7, v11, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    .line 336
    .local v7, "browseUrl":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 339
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget v4, v11, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    invoke-static {v3, v7, v5, v4, v0}, Lcom/google/android/finsky/utils/IntentUtils;->createBrowseIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v8

    .line 341
    .restart local v8    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 343
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v3, "Unexpected missing browseUrl"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 346
    .end local v7    # "browseUrl":Ljava/lang/String;
    :cond_6
    const-string v3, "Unexpected missing resolvedLink"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 349
    .end local v11    # "resolvedLink":Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    :cond_7
    const-string v3, "Unsupported PostSuccessAction."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private succeed(Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "redemptionSuccess"    # Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;
    .param p3, "storedValueInstrumentId"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x3

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 540
    const/4 v1, 0x0

    .line 541
    .local v1, "extraPurchaseData":Landroid/os/Bundle;
    iget v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    if-ne v7, v3, :cond_0

    .line 544
    iget-object v7, p1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    iget-object v8, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v7, v8}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->extractExtraPurchaseData(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/google/android/finsky/protos/Common$Docid;)Landroid/os/Bundle;

    move-result-object v1

    .line 547
    :cond_0
    new-instance v7, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    invoke-direct {v7, p2, p3, v6, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;-><init>(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V

    iput-object v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .line 554
    iget v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    if-ne v7, v9, :cond_1

    .line 555
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v7}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedeemedItemDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v5

    .line 556
    .local v5, "redeemedItem":Lcom/google/android/finsky/api/model/Document;
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v7}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getConsumptionAppDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    .line 557
    .local v0, "consumptionApp":Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/finsky/utils/DocUtils;->isInAppDocid(Lcom/google/android/finsky/protos/Common$Docid;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v0, :cond_1

    iget v7, v0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-ne v7, v9, :cond_1

    .line 561
    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 562
    .local v4, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v7

    invoke-interface {v7, v4}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 564
    .local v3, "isInstalled":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 565
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.android.vending.billing.PURCHASES_UPDATED"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 566
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/support/v4/app/FragmentActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 571
    .end local v0    # "consumptionApp":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "isInstalled":Z
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "redeemedItem":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    return-void

    .restart local v0    # "consumptionApp":Lcom/google/android/finsky/protos/Common$Docid;
    .restart local v4    # "packageName":Ljava/lang/String;
    .restart local v5    # "redeemedItem":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    move v3, v6

    .line 562
    goto :goto_0
.end method


# virtual methods
.method public addressChallenge(Lcom/google/android/finsky/protos/BillingAddress$Address;[Ljava/lang/String;)V
    .locals 1
    .param p1, "address"    # Lcom/google/android/finsky/protos/BillingAddress$Address;
    .param p2, "checkboxIds"    # [Ljava/lang/String;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->respondWithAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;[Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method public confirm()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->respondWithConfirmation()V

    .line 246
    return-void
.end method

.method public getRedeemCodeResult()Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    return-object v0
.end method

.method public hideCancelButton()V
    .locals 3

    .prologue
    .line 532
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0a0390

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 533
    .local v0, "cancelButton":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 534
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 536
    :cond_0
    return-void
.end method

.method public instrumentManagerFinished(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 253
    if-eqz p1, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->respondInstrumentManagerSucceeded()V

    .line 259
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 411
    packed-switch p1, :pswitch_data_0

    .line 426
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 413
    :pswitch_0
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    .line 418
    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedemptionSuccess()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    move-result-object v2

    .line 419
    .local v2, "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    new-instance v0, Lcom/google/android/finsky/api/model/Document;

    iget-object v3, v2, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v0, v3}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 420
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;

    move-result-object v1

    .line 421
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    .line 422
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    goto :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    .line 170
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ui_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    .line 171
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RedeemCodeFragment.redemption_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RedeemCodeFragment.docid"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Docid;

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 173
    iget v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null docid in purchase context."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    if-eqz p1, :cond_1

    .line 178
    const-string v0, "RedeemCodeFragment.last_state_instance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mLastStateInstance:I

    .line 179
    const-string v0, "RedeemCodeFragment.redeem_code_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .line 180
    const-string v0, "RedeemCodeFragment.code_screen_skipped"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCodeScreenSkipped:Z

    .line 182
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    iget v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    if-nez v1, :cond_0

    const v0, 0x7f0400bc

    .line 197
    .local v0, "layoutId":I
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 195
    .end local v0    # "layoutId":I
    :cond_0
    const v0, 0x7f0401a8

    goto :goto_0
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 598
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 599
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    .line 601
    :cond_0
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 586
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 589
    const-string v2, "dialog_details_url"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 590
    .local v0, "detailsUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 591
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->startActivity(Landroid/content/Intent;)V

    .line 592
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    .line 594
    .end local v0    # "detailsUrl":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    const-string v0, "RedeemCodeFragment.last_state_instance"

    iget v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mLastStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 188
    const-string v0, "RedeemCodeFragment.redeem_code_result"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 189
    const-string v0, "RedeemCodeFragment.code_screen_skipped"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCodeScreenSkipped:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 190
    return-void
.end method

.method public onStart()V
    .locals 6

    .prologue
    .line 221
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onStart()V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "RedeemCodeFragment.sidecar"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    iput-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    .line 223
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    if-nez v1, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 226
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedemptionContext:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    const-string v4, "RedeemCodeFragment.offer_type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "RedeemCodeFragment.partner_payload"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;)Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    .line 228
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const-string v3, "RedeemCodeFragment.sidecar"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 230
    .end local v0    # "arguments":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 231
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 9
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 440
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    if-eq p1, v4, :cond_0

    .line 441
    const-string v4, "Received state change for unknown fragment: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 524
    :goto_0
    return-void

    .line 444
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getStateInstance()I

    move-result v4

    iget v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mLastStateInstance:I

    if-gt v4, v5, :cond_1

    .line 445
    const-string v4, "Already received state instance %d, ignore."

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mLastStateInstance:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 448
    :cond_1
    sget-boolean v4, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 449
    const-string v4, "State changed: %d"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v6}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getStateInstance()I

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mLastStateInstance:I

    .line 454
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 521
    const-string v4, "Unknown sidecar state: %d"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v6}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getState()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 456
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RedeemCodeFragment.prefill_code"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 457
    .local v1, "prefillCode":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 460
    iput-boolean v7, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCodeScreenSkipped:Z

    .line 461
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->redeem(Ljava/lang/String;)V

    goto :goto_0

    .line 464
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    invoke-static {v4, v1, v5, v6}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto :goto_0

    .line 470
    .end local v1    # "prefillCode":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showLoading()V

    goto :goto_0

    .line 473
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getUserConfirmationChallenge()Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    move-result-object v4

    iget v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    iget-boolean v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCodeScreenSkipped:Z

    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->newInstance(Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;IZ)Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto/16 :goto_0

    .line 477
    :pswitch_3
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getAddressChallenge()Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    move-result-object v5

    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;I)Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto/16 :goto_0

    .line 481
    :pswitch_4
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getInstrumentManagerTokens()Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/billing/giftcard/steps/InstrumentManagerRedeemStep;->newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;)Lcom/google/android/finsky/billing/giftcard/steps/InstrumentManagerRedeemStep;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto/16 :goto_0

    .line 485
    :pswitch_5
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedemptionSuccess()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    move-result-object v2

    .line 486
    .local v2, "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedeemedOfferHtml()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getStoredValueInstrumentId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->succeed(Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-boolean v4, v2, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasButtonLabel:Z

    if-eqz v4, :cond_4

    .line 490
    iget v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    invoke-static {v2, v4}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->newInstance(Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;I)Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;

    move-result-object v3

    .line 492
    .local v3, "successStep":Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto/16 :goto_0

    .line 495
    .end local v3    # "successStep":Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->performSuccessActionAndFinish()V

    goto/16 :goto_0

    .line 500
    .end local v2    # "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    :pswitch_6
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getSubstate()I

    move-result v4

    if-ne v4, v7, :cond_5

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 502
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    .line 506
    .local v0, "errorMessageHtml":Ljava/lang/String;
    :goto_1
    const-string v4, "Redemption error: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 509
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    instance-of v4, v4, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    if-eqz v4, :cond_6

    .line 510
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->hideLoading()V

    .line 511
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    check-cast v4, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->showError(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 504
    .end local v0    # "errorMessageHtml":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getErrorHtml()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "errorMessageHtml":Ljava/lang/String;
    goto :goto_1

    .line 515
    :cond_6
    iput-boolean v8, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mCodeScreenSkipped:Z

    .line 516
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getLastRedeemCode()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    invoke-static {v4, v5, v0, v6}, Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/billing/giftcard/steps/RedeemScreenStep;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto/16 :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 236
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onStop()V

    .line 237
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 203
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mContinueButton:Landroid/view/View;

    instance-of v2, v2, Lcom/google/android/finsky/layout/IconButtonGroup;

    if-eqz v2, :cond_0

    .line 204
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "RedeemCodeFragment.backend"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 205
    .local v0, "backend":I
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mContinueButton:Landroid/view/View;

    check-cast v2, Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/IconButtonGroup;->setBackendForLabel(I)V

    .line 207
    .end local v0    # "backend":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mMainView:Landroid/view/View;

    const v3, 0x7f0a0390

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 208
    .local v1, "cancelButton":Landroid/widget/Button;
    if-eqz v1, :cond_1

    .line 209
    const v2, 0x7f0c0134

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 210
    new-instance v2, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$1;-><init>(Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    :cond_1
    return-void
.end method

.method public performSuccessActionAndFinish()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 263
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mRedeemCodeResult:Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    if-eqz v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->getRedemptionSuccess()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    move-result-object v0

    .line 265
    .local v0, "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    iget-object v1, v0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    if-eqz v1, :cond_0

    .line 266
    iget v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mBillingUiMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 267
    const-string v1, "Cannot perform success action during Setup Wizard"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    .end local v0    # "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->finish()V

    .line 278
    :goto_0
    return-void

    .line 268
    .restart local v0    # "redemptionSuccess":Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    :cond_1
    iget-object v1, v0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->performSuccessAction(Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "Dialog shown, waiting for user input."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public redeem(Ljava/lang/String;)V
    .locals 1
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->mSidecar:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->redeem(Ljava/lang/String;)V

    .line 242
    return-void
.end method
