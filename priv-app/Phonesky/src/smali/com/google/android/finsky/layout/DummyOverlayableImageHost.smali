.class public Lcom/google/android/finsky/layout/DummyOverlayableImageHost;
.super Landroid/view/View;
.source "DummyOverlayableImageHost.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;
.implements Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DummyOverlayableImageHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getOverlayTransparencyHeightFromTop()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getOverlayableImageHeight()I
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DummyOverlayableImageHost;->getHeight()I

    move-result v0

    return v0
.end method

.method public setOverlayableImageTopPadding(I)V
    .locals 0
    .param p1, "topPadding"    # I

    .prologue
    .line 40
    return-void
.end method
