.class public Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
.super Lcom/google/android/finsky/billing/InstrumentFlowFragment;
.source "AddCreditCardFlowFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;
.implements Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$6;,
        Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

.field private mBillingUiMode:I

.field private mCancelButton:Landroid/widget/Button;

.field private mCardholderName:Ljava/lang/String;

.field private mCcCvcField:Landroid/widget/TextView;

.field private mCcExpMonthField:Landroid/widget/TextView;

.field private mCcExpYearField:Landroid/widget/TextView;

.field private mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

.field private mCountries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;"
        }
    .end annotation
.end field

.field private final mElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

.field private mLastErrorInstance:I

.field private mMainView:Landroid/view/ViewGroup;

.field private mSaveButton:Landroid/widget/Button;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

.field private mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

.field private mUiEnabled:Z

.field private mWidgetsInitialized:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;-><init>()V

    .line 115
    const/16 v0, 0x35d

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 741
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)Lcom/google/android/finsky/layout/BillingAddress;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->onBillingCountriesLoaded()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private clearErrorMessages()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 343
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/BillingAddress;->clearErrorMessage()V

    .line 344
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setError(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpMonthField:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpYearField:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcCvcField:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 348
    return-void
.end method

.method private creditCardInputErrorsToInputValidationErrors(Ljava/util/Set;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "errors":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;>;"
    .local p2, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 536
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;

    .line 537
    .local v0, "error":Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;
    sget-object v2, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$6;->$SwitchMap$com$google$android$finsky$billing$creditcard$CreditCardValidator$InputField:[I

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 555
    const-string v2, "Unhandled credit card input field error for: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 539
    :pswitch_0
    const v2, 0x7f0c00bc

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->makeInputValidationError(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 543
    :pswitch_1
    const v2, 0x7f0c00bf

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->makeInputValidationError(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 547
    :pswitch_2
    const/4 v2, 0x3

    const v3, 0x7f0c00bd

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->makeInputValidationError(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 551
    :pswitch_3
    const/4 v2, 0x2

    const v3, 0x7f0c00be

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->makeInputValidationError(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 558
    .end local v0    # "error":Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;
    :cond_0
    return-void

    .line 537
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    .locals 4
    .param p1, "error"    # Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .prologue
    .line 450
    iget-object v0, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 451
    .local v0, "errorMessage":Ljava/lang/String;
    const/4 v1, 0x0

    .line 452
    .local v1, "textView":Landroid/widget/TextView;
    const/4 v2, -0x1

    .line 453
    .local v2, "textViewNameId":I
    iget v3, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    packed-switch v3, :pswitch_data_0

    .line 473
    :goto_0
    if-eqz v1, :cond_0

    .line 474
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :goto_1
    return-object v1

    .line 455
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .line 456
    const v2, 0x7f0c00a9

    .line 457
    goto :goto_0

    .line 459
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpMonthField:Landroid/widget/TextView;

    .line 460
    const v2, 0x7f0c00ac

    .line 461
    goto :goto_0

    .line 463
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpYearField:Landroid/widget/TextView;

    .line 464
    const v2, 0x7f0c00ad

    .line 465
    goto :goto_0

    .line 467
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcCvcField:Landroid/widget/TextView;

    .line 468
    const v2, 0x7f0c00af

    .line 469
    goto :goto_0

    .line 476
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/layout/BillingAddress;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v1

    goto :goto_1

    .line 453
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 3
    .param p1, "country"    # Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .prologue
    const/4 v2, 0x1

    .line 301
    new-instance v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;-><init>()V

    .line 302
    .local v0, "spec":Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->allowsReducedBillingAddress:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    :goto_0
    iput v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    .line 304
    iput-boolean v2, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->hasBillingAddressType:Z

    .line 305
    return-object v0

    :cond_0
    move v1, v2

    .line 302
    goto :goto_0
.end method

.method private getCreditCardOrShowErrors()Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;
    .locals 18

    .prologue
    .line 490
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/billing/creditcard/CreditCardNumberFilter;->getNumbers(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 491
    .local v2, "cardNumber":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpMonthField:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 492
    .local v4, "expMonth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpYearField:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 493
    .local v5, "expYear":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcCvcField:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 495
    .local v3, "cvc":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v17

    .line 498
    .local v17, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    .line 499
    .local v7, "creditCardErrors":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/finsky/billing/creditcard/CreditCardValidator$InputField;>;"
    const/16 v6, 0x7d0

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/billing/creditcard/CreditCardValidator;->validate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/Set;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v16

    .line 501
    .local v16, "type":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v7, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->creditCardInputErrorsToInputValidationErrors(Ljava/util/Set;Ljava/util/List;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/BillingAddress;->getAddressValidationErrors()Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 507
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayErrors(Ljava/util/List;)V

    .line 508
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_0

    .line 509
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/BillingAddress;->getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v14

    .line 512
    .local v14, "billingAddress":Lcom/google/android/finsky/protos/BillingAddress$Address;
    new-instance v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-direct {v15}, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;-><init>()V

    .line 514
    .local v15, "creditCard":Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    .line 515
    const/4 v6, 0x1

    iput-boolean v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationMonth:Z

    .line 517
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit16 v6, v6, 0x7d0

    iput v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    .line 518
    const/4 v6, 0x1

    iput-boolean v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationYear:Z

    .line 520
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    .line 521
    const/4 v6, 0x1

    iput-boolean v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasLastDigits:Z

    .line 523
    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->protobufType:I

    iput v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    .line 524
    const/4 v6, 0x1

    iput-boolean v6, v15, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasType:Z

    .line 526
    new-instance v12, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v12}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    .line 527
    .local v12, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iput-object v14, v12, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 528
    iput-object v15, v12, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    .line 529
    new-instance v8, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;

    const/4 v13, 0x0

    move-object/from16 v9, p0

    move-object v10, v2

    move-object v11, v3

    invoke-direct/range {v8 .. v13}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$Instrument;Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$1;)V

    .line 531
    .end local v12    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v14    # "billingAddress":Lcom/google/android/finsky/protos/BillingAddress$Address;
    .end local v15    # "creditCard":Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
    :goto_0
    return-object v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private handleError()V
    .locals 14

    .prologue
    const/4 v3, -0x1

    const-wide/16 v12, 0x0

    const/16 v5, 0x360

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 585
    iget v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mLastErrorInstance:I

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getStateInstance()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 586
    const-string v0, "Already handled error %d, ignoring."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mLastErrorInstance:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 633
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getStateInstance()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mLastErrorInstance:I

    .line 590
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getSubstate()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 631
    const-string v0, "Unknown error code: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getSubstate()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 592
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getResponse()Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-static {v0}, Lcom/google/android/finsky/utils/ArrayUtils;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayErrors(Ljava/util/List;)V

    goto :goto_0

    .line 595
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0c01e2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 598
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getErrorHtml()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1, v4}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    .line 600
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, v12, v13, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 604
    :pswitch_3
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 606
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getErrorHtml()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01b4

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01b3

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x361

    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mAccountName:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 613
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v11

    .line 614
    .local v11, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "error_with_choice"

    invoke-virtual {v11, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 617
    .end local v6    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v11    # "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v7

    .line 618
    .local v7, "error":Lcom/android/volley/VolleyError;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v10

    .line 619
    .local v10, "htmlMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v2, v10, v4}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    .line 620
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, v12, v13, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0

    .line 624
    .end local v7    # "error":Lcom/android/volley/VolleyError;
    .end local v10    # "htmlMessage":Ljava/lang/String;
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v8

    .line 625
    .local v8, "escrowError":Lcom/android/volley/VolleyError;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v9

    .line 626
    .local v9, "escrowHtmlMessage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v2, v9, v4}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    .line 627
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, v12, v13, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_0

    .line 590
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private loadBillingCountries()V
    .locals 3

    .prologue
    .line 309
    new-instance v0, Lcom/google/android/finsky/billing/GetBillingCountriesAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;-><init>()V

    .line 310
    .local v0, "getBillingCountriesAction":Lcom/google/android/finsky/billing/GetBillingCountriesAction;
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mAccountName:Ljava/lang/String;

    new-instance v2, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$4;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;->run(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 316
    return-void
.end method

.method private static makeInputValidationError(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 2
    .param p0, "field"    # I
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 636
    new-instance v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    .line 637
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iput p0, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    .line 638
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    .line 640
    iput-object p1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 641
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    .line 642
    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "cardholderName"    # Ljava/lang/String;
    .param p2, "mode"    # I
    .param p3, "isLightTheme"    # Z

    .prologue
    .line 125
    new-instance v1, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;-><init>()V

    .line 126
    .local v1, "result":Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v2, "cardholder_name"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 130
    const-string v2, "is_light_theme"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->setArguments(Landroid/os/Bundle;)V

    .line 132
    return-object v1
.end method

.method private onBillingCountriesLoaded()V
    .locals 5

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 337
    :goto_0
    return-void

    .line 322
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getBillingCountries()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCountries:Ljava/util/List;

    .line 323
    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCountries:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCountries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 324
    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->setupBillingCountriesWidgets(Landroid/os/Bundle;)V

    .line 325
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->updateSaveButtonState()V

    goto :goto_0

    .line 328
    :cond_1
    const-string v2, "BillingCountries not loaded."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 330
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v2, 0x7f0c00ea

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c01d7

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c0134

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 334
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 335
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupBillingCountriesWidgets(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    if-nez v0, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getDefaultCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCountries:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getBillingCountries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountries(Ljava/util/List;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)V

    .line 370
    if-eqz p1, :cond_1

    .line 371
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 374
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mWidgetsInitialized:Z

    .line 375
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V

    .line 378
    :cond_2
    return-void
.end method

.method private updateSaveButtonState()V
    .locals 2

    .prologue
    .line 676
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mUiEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->areAllFieldsVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 677
    return-void

    .line 676
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public back()V
    .locals 0

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->cancel()V

    .line 703
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 718
    invoke-super {p0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->cancel()V

    .line 719
    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 735
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unwanted children."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public displayErrors(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    .local p1, "inputValidationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->clearErrorMessages()V

    .line 409
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 444
    :goto_0
    return-void

    .line 414
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v4, 0x362

    invoke-virtual {v1, v2, v3, v4, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 416
    new-instance v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Ljava/util/List;)V

    .line 439
    .local v0, "displayErrorsRunnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddCreditCardFields;->expandFields()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/finsky/layout/AddCreditCardFields;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 442
    :cond_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public enableUi(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 649
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mWidgetsInitialized:Z

    if-nez v0, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-void

    .line 653
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mUiEnabled:Z

    .line 654
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->setEnabled(Z)V

    .line 656
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setEnabled(Z)V

    .line 657
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcCvcField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 658
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpMonthField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 659
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpYearField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 660
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->updateSaveButtonState()V

    .line 661
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 709
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V

    .line 710
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onAllFieldsVisible()V
    .locals 0

    .prologue
    .line 667
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->updateSaveButtonState()V

    .line 668
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 137
    instance-of v0, p1, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement PlayStoreUiElementNode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onAttach(Landroid/app/Activity;)V

    .line 141
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 382
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 384
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x35e

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 385
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getCreditCardOrShowErrors()Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;

    move-result-object v0

    .line 386
    .local v0, "instrumentAndCredentials":Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;
    if-eqz v0, :cond_0

    .line 389
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->creditCardNumber:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->access$300(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->cvc:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->access$400(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;->access$500(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->saveCreditCard(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V

    goto :goto_0

    .line 393
    .end local v0    # "instrumentAndCredentials":Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$InstrumentAndCredentials;
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x35f

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 395
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->cancel()V

    goto :goto_0

    .line 382
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a00f8 -> :sswitch_0
        0x7f0a0115 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onCreate(Landroid/os/Bundle;)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 147
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mAccountName:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "cardholder_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCardholderName:Ljava/lang/String;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ui_mode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingUiMode:I

    .line 150
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 151
    if-eqz p1, :cond_0

    .line 152
    const-string v1, "last_error_instance"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mLastErrorInstance:I

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f0c0134

    const v8, 0x7f0c00c5

    const v7, 0x7f0c00b3

    const/4 v6, 0x0

    .line 161
    if-nez p2, :cond_0

    .line 162
    const/4 v4, 0x0

    .line 265
    :goto_0
    return-object v4

    .line 164
    :cond_0
    iput-object p3, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 165
    iget v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingUiMode:I

    if-nez v4, :cond_2

    const v1, 0x7f04002b

    .line 168
    .local v1, "mainViewResource":I
    :goto_1
    invoke-virtual {p1, v1, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    .line 170
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e6

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/BillingAddress;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    .line 173
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v4, v7}, Lcom/google/android/finsky/layout/BillingAddress;->setNameInputHint(I)V

    .line 174
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    new-instance v5, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$1;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountryChangeListener(Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;)V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v3

    .line 186
    .local v3, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v3, :cond_3

    .line 187
    invoke-virtual {v3}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    .line 188
    invoke-virtual {v3}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    .line 193
    :goto_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    instance-of v4, v4, Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v4, :cond_4

    .line 194
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v8, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 200
    :goto_3
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->updateSaveButtonState()V

    .line 201
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    instance-of v4, v4, Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v4, :cond_5

    .line 202
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v4, v6, v9, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 209
    :goto_4
    iget v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingUiMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 211
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00d7

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 213
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/AddCreditCardFields;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    .line 214
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "is_light_theme"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/AddCreditCardFields;->setIsLightTheme(Z)V

    .line 215
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mFields:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-virtual {v4, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->setOnAllFieldsVisibleListener(Lcom/google/android/finsky/layout/AddCreditCardFields$OnAllFieldsVisibleListener;)V

    .line 217
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00de

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .line 218
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpMonthField:Landroid/widget/TextView;

    .line 219
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e1

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcExpYearField:Landroid/widget/TextView;

    .line 220
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e3

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcCvcField:Landroid/widget/TextView;

    .line 223
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 224
    .local v0, "cvcImage":Landroid/widget/ImageView;
    new-instance v4, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$2;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00e7

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 241
    .local v2, "privacyFooter":Landroid/widget/TextView;
    new-instance v4, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$3;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v5, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCardholderName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/BillingAddress;->setDefaultName(Ljava/lang/String;)V

    .line 261
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v4, v7}, Lcom/google/android/finsky/layout/BillingAddress;->setNameInputHint(I)V

    .line 262
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/billing/BillingUtils;->getLine1Number(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/BillingAddress;->setPhoneNumber(Ljava/lang/String;)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->loadBillingCountries()V

    .line 265
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    goto/16 :goto_0

    .line 165
    .end local v0    # "cvcImage":Landroid/widget/ImageView;
    .end local v1    # "mainViewResource":I
    .end local v2    # "privacyFooter":Landroid/widget/TextView;
    .end local v3    # "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    :cond_2
    const v1, 0x7f04019b

    goto/16 :goto_1

    .line 190
    .restart local v1    # "mainViewResource":I
    .restart local v3    # "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a00f8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    .line 191
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;

    const v5, 0x7f0a0115

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    goto/16 :goto_2

    .line 197
    :cond_4
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_3

    .line 205
    :cond_5
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setText(I)V

    .line 206
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->cancel()V

    .line 691
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onPause()V

    .line 296
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 297
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 683
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 684
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->loadBillingCountries()V

    .line 686
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onResume()V

    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    const v1, 0x7f0c00ba

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->setHostTitle(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 291
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 352
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->saveInstanceState(Landroid/os/Bundle;)V

    .line 356
    :cond_0
    const-string v0, "last_error_instance"

    iget v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mLastErrorInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 357
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onStart()V

    .line 279
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "credit_card_sidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    .line 280
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    .line 282
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    const-string v2, "credit_card_sidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 284
    :cond_0
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 4
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v1, 0x1

    .line 562
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 582
    :goto_0
    return-void

    .line 564
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->hideProgress()V

    .line 565
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->enableUi(Z)V

    goto :goto_0

    .line 568
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    const v1, 0x7f0c00c6

    invoke-interface {v0, v1}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->showProgress(I)V

    .line 569
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0xd5

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 570
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->enableUi(Z)V

    goto :goto_0

    .line 573
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->hideProgress()V

    .line 574
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->enableUi(Z)V

    .line 575
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->handleError()V

    goto :goto_0

    .line 578
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->hideProgress()V

    .line 579
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mSidecar:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getResponse()Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->finishWithUpdateInstrumentResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V

    goto :goto_0

    .line 562
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 270
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/InstrumentFlowFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 271
    if-nez p2, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mCcNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 274
    :cond_0
    return-void
.end method
