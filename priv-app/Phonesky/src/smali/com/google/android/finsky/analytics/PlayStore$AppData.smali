.class public final Lcom/google/android/finsky/analytics/PlayStore$AppData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppData"
.end annotation


# instance fields
.field public downloadExternal:Z

.field public hasDownloadExternal:Z

.field public hasOldVersion:Z

.field public hasSystemApp:Z

.field public hasVersion:Z

.field public oldVersion:I

.field public systemApp:Z

.field public version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4529
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4530
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$AppData;->clear()Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 4531
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4534
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 4535
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 4536
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 4537
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 4538
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 4539
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 4540
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    .line 4541
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    .line 4542
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->cachedSize:I

    .line 4543
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4566
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4567
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    if-eqz v1, :cond_1

    .line 4568
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4571
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    if-eqz v1, :cond_3

    .line 4572
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4575
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    if-eqz v1, :cond_5

    .line 4576
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4579
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    if-eqz v1, :cond_7

    .line 4580
    :cond_6
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4583
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4592
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4596
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4597
    :sswitch_0
    return-object p0

    .line 4602
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 4603
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    goto :goto_0

    .line 4607
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 4608
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    goto :goto_0

    .line 4612
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 4613
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    goto :goto_0

    .line 4617
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    .line 4618
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    goto :goto_0

    .line 4592
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4496
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$AppData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4549
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    if-eqz v0, :cond_1

    .line 4550
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4552
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    if-eqz v0, :cond_3

    .line 4553
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4555
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    if-eqz v0, :cond_5

    .line 4556
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4558
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    if-eqz v0, :cond_7

    .line 4559
    :cond_6
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4561
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4562
    return-void
.end method
