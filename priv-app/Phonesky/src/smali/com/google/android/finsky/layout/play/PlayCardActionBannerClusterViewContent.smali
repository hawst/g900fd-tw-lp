.class public Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.source "PlayCardActionBannerClusterViewContent.java"


# instance fields
.field private mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mExploreButton:Landroid/widget/TextView;

.field private mHasTallCover:Z

.field private mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImageFrame:Landroid/widget/FrameLayout;

.field private mProfileInfoBlock:Landroid/view/View;

.field private mProfileSubtitle:Landroid/widget/TextView;

.field private mProfileTitle:Landroid/widget/TextView;

.field private final mShouldLayoutVertically:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 65
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mShouldLayoutVertically:Z

    .line 67
    return-void
.end method


# virtual methods
.method public configureExtraContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/view/View$OnClickListener;)V
    .locals 5
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "primaryPerson"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p5, "secondaryPersons"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p6, "buttonText"    # Ljava/lang/String;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p8, "exploreClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v4, 0x0

    .line 115
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 117
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v1, p4, p5, p1, p7}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->setData(Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    .line 119
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v1, v2, v3, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v1, p8}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v1, p6}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileTitle:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileSubtitle:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    if-eqz p6, :cond_0

    .line 126
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mExploreButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mExploreButton:Landroid/widget/TextView;

    invoke-virtual {v1, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mExploreButton:Landroid/widget/TextView;

    invoke-virtual {v1, p8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mExploreButton:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected getIndexOfFirstCard()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x2

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onFinishInflate()V

    .line 73
    const v0, 0x7f0a02b8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    .line 74
    const v0, 0x7f0a02b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 76
    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileTitle:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileSubtitle:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mExploreButton:Landroid/widget/TextView;

    .line 81
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 228
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onLayout(ZIIII)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getWidth()I

    move-result v5

    .line 232
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getMeasuredHeight()I

    move-result v0

    .line 233
    .local v0, "avatarPackHeight":I
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingTop:I

    int-to-float v7, v0

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    add-int v1, v6, v7

    .line 235
    .local v1, "coverImageTop":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v6, v4, v1, v5, v7}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 238
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 239
    .local v3, "infoBlockWidth":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 240
    .local v2, "infoBlockHeight":I
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mShouldLayoutVertically:Z

    if-eqz v6, :cond_0

    .line 241
    .local v4, "infoBlockX":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingTop:I

    add-int v8, v4, v3

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingTop:I

    add-int/2addr v9, v2

    invoke-virtual {v6, v4, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 244
    return-void

    .line 240
    .end local v4    # "infoBlockX":I
    :cond_0
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentHorizontalPadding:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 136
    invoke-super/range {p0 .. p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onMeasure(II)V

    .line 138
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    .line 139
    .local v13, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getMeasuredHeight()I

    move-result v8

    .line 140
    .local v8, "measuredHeight":I
    if-nez v8, :cond_1

    const/4 v2, 0x0

    .line 142
    .local v2, "cardContentHeight":I
    :goto_0
    move v12, v2

    .line 148
    .local v12, "totalHeight":I
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 150
    .local v5, "fullWidthSpec":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mShouldLayoutVertically:Z

    if-eqz v14, :cond_2

    .line 151
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v5, v15}, Landroid/view/View;->measure(II)V

    .line 153
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 158
    .local v11, "profileInfoBlockHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v14}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getMeasuredHeight()I

    move-result v14

    int-to-float v14, v14

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    sub-int v14, v11, v14

    int-to-float v15, v2

    const v16, 0x3f59999a    # 0.85f

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v10, v14, v15

    .line 161
    .local v10, "profileImageHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v5, v15}, Landroid/widget/FrameLayout;->measure(II)V

    .line 164
    add-int/2addr v12, v11

    .line 220
    .end local v11    # "profileInfoBlockHeight":I
    :goto_1
    if-lez v2, :cond_0

    .line 221
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingTop:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingBottom:I

    add-int/2addr v14, v15

    add-int/2addr v12, v14

    .line 223
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->setMeasuredDimension(II)V

    .line 224
    return-void

    .line 140
    .end local v2    # "cardContentHeight":I
    .end local v5    # "fullWidthSpec":I
    .end local v10    # "profileImageHeight":I
    .end local v12    # "totalHeight":I
    :cond_1
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingTop:I

    sub-int v14, v8, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mCardContentPaddingBottom:I

    sub-int v2, v14, v15

    goto :goto_0

    .line 170
    .restart local v2    # "cardContentHeight":I
    .restart local v5    # "fullWidthSpec":I
    .restart local v12    # "totalHeight":I
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getLeadingGap(I)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getExtraColumnOffset()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getCellSize(I)F

    move-result v16

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v4, v14, v15

    .line 172
    .local v4, "cardContentLeadingGap":I
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 174
    .local v7, "infoBlockWidthSpec":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v7, v15}, Landroid/view/View;->measure(II)V

    .line 176
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v14}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->getMeasuredHeight()I

    move-result v1

    .line 181
    .local v1, "avatarPackHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    int-to-float v15, v1

    const/high16 v16, 0x3f000000    # 0.5f

    mul-float v15, v15, v16

    float-to-int v15, v15

    sub-int v9, v14, v15

    .line 185
    .local v9, "profileContentHeightUnderProfileImage":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mHasTallCover:Z

    if-eqz v14, :cond_3

    .line 190
    int-to-float v14, v2

    const v15, 0x3f59999a    # 0.85f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    int-to-float v15, v1

    const/high16 v16, 0x3f000000    # 0.5f

    mul-float v15, v15, v16

    float-to-int v15, v15

    sub-int v3, v14, v15

    .line 203
    .local v3, "cardContentHeightOverProfileImage":I
    :goto_2
    invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 205
    .restart local v10    # "profileImageHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v5, v15}, Landroid/widget/FrameLayout;->measure(II)V

    .line 211
    int-to-float v14, v1

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    add-int v6, v10, v14

    .line 213
    .local v6, "infoBlockFinalHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v6, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v7, v15}, Landroid/view/View;->measure(II)V

    .line 216
    int-to-float v14, v2

    const v15, 0x3e199998    # 0.14999998f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    add-int v12, v6, v14

    goto/16 :goto_1

    .line 198
    .end local v3    # "cardContentHeightOverProfileImage":I
    .end local v6    # "infoBlockFinalHeight":I
    .end local v10    # "profileImageHeight":I
    :cond_3
    int-to-float v14, v1

    const/4 v15, 0x0

    mul-float/2addr v14, v15

    float-to-int v14, v14

    int-to-float v15, v2

    const v16, 0x3f59999a    # 0.85f

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v3, v14, v15

    .restart local v3    # "cardContentHeightOverProfileImage":I
    goto :goto_2
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onRecycle()V

    .line 249
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mAvatarPack:Lcom/google/android/finsky/layout/play/PlayAvatarPack;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayAvatarPack;->onRecycle()V

    .line 250
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 251
    return-void
.end method

.method public setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V
    .locals 5
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V

    .line 90
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getDocCount()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 91
    .local v2, "visibleCardCount":I
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mHasTallCover:Z

    .line 92
    const/4 v1, 0x0

    .local v1, "tileIndex":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 93
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getDoc(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 94
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v3

    const v4, 0x3fb872b0    # 1.441f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 96
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->mHasTallCover:Z

    .line 100
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    return-void

    .line 92
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
