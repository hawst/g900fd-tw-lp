.class public Lcom/google/android/finsky/activities/PlayGamesInstallHelper;
.super Ljava/lang/Object;
.source "PlayGamesInstallHelper.java"


# direct methods
.method public static addGameIntentExtras(Lcom/google/android/finsky/api/model/Document;Landroid/content/Intent;)V
    .locals 4
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 47
    if-nez p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 51
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-eqz v0, :cond_0

    .line 54
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    .line 55
    .local v1, "appType":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "game"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    const-string v2, "is_game"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasBadgeContainer()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "has_game_features"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static handlePositiveClick(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 2
    .param p0, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 155
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/finsky/activities/PlayGamesInstallHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/PlayGamesInstallHelper$1;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 163
    return-void
.end method

.method public static isGameIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 67
    const-string v1, "is_game"

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 68
    .local v0, "isGame":Z
    if-nez v0, :cond_0

    move v1, v2

    .line 74
    :goto_0
    return v1

    .line 71
    :cond_0
    sget-object v1, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionOnlyWhenGameFeatures:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    const-string v1, "has_game_features"

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0

    .line 74
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static launchPlayGamesInstallDialog(Landroid/support/v4/app/FragmentManager;ILjava/lang/String;)V
    .locals 9
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "requestCodeForResponse"    # I
    .param p2, "tagToUse"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 130
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 131
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v0, 0x7f040148

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01e6

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c00a5

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2, p1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x13e

    const/16 v3, 0x112

    const/16 v4, 0x113

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 139
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v7

    .line 140
    .local v7, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {v7, p0, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 143
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v8, v0, 0x1

    .line 144
    .local v8, "newCount":I
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 146
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionLastTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 147
    return-void
.end method

.method public static shouldSuggestPlayGames()Z
    .locals 16

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 81
    sget-object v8, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionMaxShownCount:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 82
    .local v3, "maxTimesShown":I
    if-nez v3, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v10

    .line 86
    :cond_1
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 87
    .local v6, "numTimesShown":I
    if-ge v6, v3, :cond_0

    .line 91
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v7

    .line 93
    .local v7, "packageStateRepository":Lcom/google/android/finsky/appstate/PackageStateRepository;
    const-string v8, "com.google.android.gms"

    invoke-interface {v7, v8}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v2

    .line 94
    .local v2, "gmsPackageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v2, :cond_0

    .line 97
    iget v11, v2, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    sget-object v8, Lcom/google/android/finsky/config/G;->playGamesGmsCoreMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-lt v11, v8, :cond_0

    .line 101
    sget-object v8, Lcom/google/android/finsky/config/G;->playGamesDocId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v8

    if-nez v8, :cond_0

    .line 106
    packed-switch v6, :pswitch_data_0

    .line 114
    sget-object v8, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionSubsequentBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 118
    .local v0, "backoffMs":J
    :goto_1
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->playGamesInstallSuggestionLastTimeShown:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 119
    .local v4, "lastTimestamp":J
    add-long v12, v4, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    cmp-long v8, v12, v14

    if-gez v8, :cond_2

    move v8, v9

    :goto_2
    move v10, v8

    goto :goto_0

    .end local v0    # "backoffMs":J
    .end local v4    # "lastTimestamp":J
    :pswitch_0
    move v10, v9

    .line 109
    goto :goto_0

    .line 111
    :pswitch_1
    sget-object v8, Lcom/google/android/finsky/config/G;->playGamesInstallSuggestionFirstBackoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 112
    .restart local v0    # "backoffMs":J
    goto :goto_1

    .restart local v4    # "lastTimestamp":J
    :cond_2
    move v8, v10

    .line 119
    goto :goto_2

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
