.class Lcom/google/android/finsky/services/DetailsService$1;
.super Lcom/android/vending/details/IDetailsService$Stub;
.source "DetailsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/DetailsService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/DetailsService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/DetailsService;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/finsky/services/DetailsService$1;->this$0:Lcom/google/android/finsky/services/DetailsService;

    invoke-direct {p0}, Lcom/android/vending/details/IDetailsService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppDetails(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 29
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v28

    .line 73
    .local v28, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v17

    .line 74
    .local v17, "currentAccount":Landroid/accounts/Account;
    if-nez v17, :cond_0

    .line 76
    const/4 v5, 0x0

    .line 137
    .end local v17    # "currentAccount":Landroid/accounts/Account;
    :goto_0
    return-object v5

    .line 78
    .restart local v17    # "currentAccount":Landroid/accounts/Account;
    :cond_0
    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    .line 80
    .local v2, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    sget-object v5, Lcom/google/android/finsky/config/G;->enableDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    .line 81
    const-string v5, "API access is blocked for all apps"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    const/16 v3, 0x200

    const-string v5, "all-access-blocked"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v4, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v5, 0x0

    goto :goto_0

    .line 87
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/services/DetailsService$1;->this$0:Lcom/google/android/finsky/services/DetailsService;

    sget-object v6, Lcom/google/android/finsky/config/G;->enableThirdPartyDetailsApi:Lcom/google/android/play/utils/config/GservicesValue;

    const/16 v7, 0x200

    move-object/from16 v0, p1

    invoke-static {v5, v0, v6, v2, v7}, Lcom/google/android/finsky/utils/SignatureUtils;->getCallingAppIfAuthorized(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/play/utils/config/GservicesValue;Lcom/google/android/finsky/analytics/FinskyEventLog;I)Ljava/lang/String;

    move-result-object v13

    .line 89
    .local v13, "callingApp":Ljava/lang/String;
    if-nez v13, :cond_2

    .line 91
    const/4 v5, 0x0

    goto :goto_0

    .line 93
    :cond_2
    const-string v5, "Received app details request for %s from %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object v13, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, "detailsUrl":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    .line 99
    .local v3, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v8

    .line 100
    .local v8, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/Details$DetailsResponse;>;"
    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v9, v8

    invoke-interface/range {v3 .. v9}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/lang/String;ZZLjava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 104
    :try_start_0
    invoke-virtual {v8}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/google/android/finsky/protos/Details$DetailsResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    .local v25, "detailsResponse":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object/from16 v26, v0

    .line 121
    .local v26, "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-nez v26, :cond_4

    .line 122
    const-string v5, "No doc in details response for %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    const/16 v16, 0x200

    const-string v18, "empty-details-response"

    const/16 v20, 0x0

    move-object v15, v2

    move-object/from16 v17, p1

    move-object/from16 v19, v13

    invoke-static/range {v15 .. v20}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .end local v17    # "currentAccount":Landroid/accounts/Account;
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 105
    .end local v25    # "detailsResponse":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .end local v26    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .restart local v17    # "currentAccount":Landroid/accounts/Account;
    :catch_0
    move-exception v27

    .line 106
    .local v27, "e":Ljava/lang/InterruptedException;
    const-string v5, "Interrupted while trying to retrieve app details"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    const/16 v10, 0x200

    const-string v12, "fetch-interrupted"

    const/4 v14, 0x0

    move-object v9, v2

    move-object/from16 v11, p1

    invoke-static/range {v9 .. v14}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 110
    .end local v27    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v27

    .line 111
    .local v27, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual/range {v27 .. v27}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v24

    .line 112
    .local v24, "cause":Ljava/lang/Throwable;
    if-nez v24, :cond_3

    const/4 v14, 0x0

    .line 114
    .local v14, "exceptionType":Ljava/lang/String;
    :goto_1
    const-string v5, "Unable to retrieve app details: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v14, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    const/16 v10, 0x200

    const-string v12, "fetch-error"

    move-object v9, v2

    move-object/from16 v11, p1

    invoke-static/range {v9 .. v14}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 112
    .end local v14    # "exceptionType":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v14

    goto :goto_1

    .line 128
    .end local v24    # "cause":Ljava/lang/Throwable;
    .end local v27    # "e":Ljava/util/concurrent/ExecutionException;
    .restart local v25    # "detailsResponse":Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .restart local v26    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_4
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v18

    .line 129
    .local v18, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v19

    .line 131
    .local v19, "installer":Lcom/google/android/finsky/receivers/Installer;
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v20

    .line 132
    .local v20, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/AppStates;->blockingLoad()V

    .line 134
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v21

    .line 135
    .local v21, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/library/Libraries;->blockingLoad()V

    .line 137
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/services/DetailsService$1;->this$0:Lcom/google/android/finsky/services/DetailsService;

    new-instance v16, Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    move-object/from16 v22, v13

    move-object/from16 v23, v2

    invoke-static/range {v15 .. v23}, Lcom/google/android/finsky/services/DetailsService;->getBundle(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;Ljava/lang/String;Lcom/google/android/finsky/analytics/FinskyEventLog;)Landroid/os/Bundle;

    move-result-object v5

    goto/16 :goto_0
.end method
