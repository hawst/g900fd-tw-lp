.class public abstract Lcom/google/android/libraries/bind/card/CardGroup;
.super Ljava/lang/Object;
.source "CardGroup.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;


# instance fields
.field private final groupList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/CardGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method


# virtual methods
.method public startEditing(Landroid/view/View;I)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 617
    sget-object v4, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "startEditing position: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 620
    .local v2, "parent":Landroid/view/ViewParent;
    const/4 v1, 0x0

    .line 621
    .local v1, "editableListView":Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;
    :goto_0
    if-eqz v2, :cond_0

    .line 622
    instance-of v4, v2, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    if-eqz v4, :cond_2

    move-object v1, v2

    .line 623
    check-cast v1, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    .line 628
    :cond_0
    if-nez v1, :cond_3

    .line 636
    :cond_1
    :goto_1
    return v3

    .line 626
    :cond_2
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_0

    .line 632
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, p2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 633
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 636
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p2}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, p1, p0, p2, v3}, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->startEditing(Landroid/view/View;Lcom/google/android/libraries/bind/card/CardGroup;ILjava/lang/Object;)Z

    move-result v3

    goto :goto_1
.end method
