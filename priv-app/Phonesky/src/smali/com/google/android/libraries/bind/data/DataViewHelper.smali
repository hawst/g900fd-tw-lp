.class public Lcom/google/android/libraries/bind/data/DataViewHelper;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "DataViewHelper.java"


# instance fields
.field private attached:Z

.field private clearDataOnDetach:Z

.field private dataRow:Lcom/google/android/libraries/bind/data/DataList;

.field private registered:Z

.field private temporarilyDetached:Z

.field private final view:Lcom/google/android/libraries/bind/data/DataView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataView;)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/libraries/bind/data/DataView;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach:Z

    .line 26
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->view:Lcom/google/android/libraries/bind/data/DataView;

    .line 27
    return-void
.end method

.method private register()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    .line 136
    return-void
.end method

.method private unregister()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    .line 141
    return-void
.end method

.method private updateRegistrationIfNeeded()V
    .locals 2

    .prologue
    .line 121
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->attached:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach:Z

    if-nez v1, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 122
    .local v0, "shouldBeRegistered":Z
    :goto_0
    if-eqz v0, :cond_4

    .line 123
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_2

    .line 124
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->register()V

    .line 131
    :cond_2
    :goto_1
    return-void

    .line 121
    .end local v0    # "shouldBeRegistered":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    .restart local v0    # "shouldBeRegistered":Z
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    if-eqz v1, :cond_2

    .line 128
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->unregister()V

    goto :goto_1
.end method


# virtual methods
.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTemporarilyDetached()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->attached:Z

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    .line 85
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->updateRegistrationIfNeeded()V

    .line 86
    return-void
.end method

.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    const/4 v0, 0x1

    .line 67
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    if-gt v1, v0, :cond_1

    :cond_0
    :goto_0
    const-string v1, "Passed DataList with more than one row."

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->view:Lcom/google/android/libraries/bind/data/DataView;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/bind/data/DataView;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 70
    return-void

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->attached:Z

    .line 91
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    .line 92
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->updateRegistrationIfNeeded()V

    .line 93
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    .line 106
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->updateRegistrationIfNeeded()V

    .line 107
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->isTemporarilyDetached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onFinishTemporaryDetach()V

    .line 118
    :cond_0
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    .line 101
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->updateRegistrationIfNeeded()V

    .line 102
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    if-ne p1, v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    if-eqz v0, :cond_1

    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->unregister()V

    .line 39
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    .line 40
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->updateRegistrationIfNeeded()V

    .line 41
    sget-object v0, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 54
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "View type: %s, hasData: %b, registered: %b, attached: %b,temporarilyDetached: %b, clearDataOnDetach: %b"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->view:Lcom/google/android/libraries/bind/data/DataView;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->dataRow:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v5, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->registered:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->attached:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->temporarilyDetached:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
