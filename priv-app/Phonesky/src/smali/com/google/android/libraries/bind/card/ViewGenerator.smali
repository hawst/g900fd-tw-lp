.class public interface abstract Lcom/google/android/libraries/bind/card/ViewGenerator;
.super Ljava/lang/Object;
.source "ViewGenerator.java"


# virtual methods
.method public abstract getCardIds()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract makeView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
.end method
