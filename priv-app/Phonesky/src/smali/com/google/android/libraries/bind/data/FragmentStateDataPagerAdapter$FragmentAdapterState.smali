.class Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
.super Ljava/lang/Object;
.source "FragmentStateDataPagerAdapter.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FragmentAdapterState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final key:Ljava/lang/Object;

.field public final state:Landroid/support/v4/app/Fragment$SavedState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Landroid/support/v4/app/Fragment$SavedState;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "state"    # Landroid/support/v4/app/Fragment$SavedState;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    .line 97
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    .line 98
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 102
    instance-of v2, p1, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 103
    check-cast v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 104
    .local v0, "adapterState":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    iget-object v3, v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    invoke-static {v2, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 107
    .end local v0    # "adapterState":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 112
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    const-string v0, "key: %s, fragmentState: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/util/ParcelUtil;->writeObjectToParcel(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 129
    return-void
.end method
