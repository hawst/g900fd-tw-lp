.class public interface abstract Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;
.super Ljava/lang/Object;
.source "EditableAdapterView.java"


# static fields
.field public static final DK_IS_EDITABLE:I

.field public static final DK_IS_REMOVABLE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget v0, Lcom/google/android/libraries/bind/R$id;->EditableAdapterView_isEditable:I

    sput v0, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    .line 17
    sget v0, Lcom/google/android/libraries/bind/R$id;->EditableAdapterView_isRemovable:I

    sput v0, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_REMOVABLE:I

    return-void
.end method


# virtual methods
.method public abstract startEditing(Landroid/view/View;Lcom/google/android/libraries/bind/card/CardGroup;ILjava/lang/Object;)Z
.end method
