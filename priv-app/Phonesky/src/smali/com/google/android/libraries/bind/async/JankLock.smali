.class public final Lcom/google/android/libraries/bind/async/JankLock;
.super Ljava/lang/Object;
.source "JankLock.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field private static final DISABLED:Z

.field public static final global:Lcom/google/android/libraries/bind/async/JankLock;


# instance fields
.field private isPaused:Z

.field private final pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final resumeRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

.field private final unpaused:Ljava/util/concurrent/locks/Condition;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    .line 20
    new-instance v0, Lcom/google/android/libraries/bind/async/JankLock;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/async/JankLock;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->unpaused:Ljava/util/concurrent/locks/Condition;

    .line 25
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/bind/async/JankLock$1;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/bind/async/JankLock$1;-><init>(Lcom/google/android/libraries/bind/async/JankLock;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->resumeRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 34
    return-void
.end method


# virtual methods
.method public blockUntilJankPermitted()V
    .locals 2

    .prologue
    .line 41
    sget-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 46
    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->isPaused:Z

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->unpaused:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->awaitUninterruptibly()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 50
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 163
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/async/JankLock;->executeOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;Z)V

    .line 164
    return-void
.end method

.method public executeOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;Z)V
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "executeOnSameThreadIfUnpaused"    # Z

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

    .line 123
    .local v0, "blockingQueue":Ljava/util/concurrent/Executor;
    new-instance v1, Lcom/google/android/libraries/bind/async/JankLock$2;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/android/libraries/bind/async/JankLock$2;-><init>(Lcom/google/android/libraries/bind/async/JankLock;Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 147
    .local v1, "blockingRunnable":Ljava/lang/Runnable;
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/async/JankLock;->isPaused()Z

    move-result v2

    if-nez v2, :cond_1

    .line 148
    if-eqz p3, :cond_0

    .line 149
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 156
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 154
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 2

    .prologue
    .line 88
    sget-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 93
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->isPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 58
    sget-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    if-eqz v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 64
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->isPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public pauseTemporarily(J)V
    .locals 3
    .param p1, "milliseconds"    # J

    .prologue
    .line 103
    sget-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/async/JankLock;->pause()V

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->resumeRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    goto :goto_0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 74
    sget-boolean v0, Lcom/google/android/libraries/bind/async/JankLock;->DISABLED:Z

    if-eqz v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 80
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->isPaused:Z

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->unpaused:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/JankLock;->pauseLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
