.class public Lcom/google/android/libraries/bind/data/FilteredDataList;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "FilteredDataList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;
    }
.end annotation


# instance fields
.field private currentSourceDataVersion:I

.field private final equalityFields:[I

.field protected final filter:Lcom/google/android/libraries/bind/data/Filter;

.field private final refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field protected final sourceList:Lcom/google/android/libraries/bind/data/DataList;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Filter;[IIZ)V
    .locals 1
    .param p1, "sourceList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "filter"    # Lcom/google/android/libraries/bind/data/Filter;
    .param p3, "equalityFields"    # [I
    .param p4, "primaryKey"    # I
    .param p5, "clearOnInvalidation"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p4}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    .line 40
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 41
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->filter:Lcom/google/android/libraries/bind/data/Filter;

    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    .line 43
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->equalityFields:[I

    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;

    invoke-direct {v0, p0, p5}, Lcom/google/android/libraries/bind/data/FilteredDataList$1;-><init>(Lcom/google/android/libraries/bind/data/FilteredDataList;Z)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 53
    if-eqz p2, :cond_0

    .line 54
    invoke-interface {p2, p0}, Lcom/google/android/libraries/bind/data/Filter;->onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 56
    :cond_0
    return-void

    .line 40
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected equalityFields()[I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->equalityFields:[I

    return-object v0
.end method

.method public isDirty()Z
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->dataVersion()I

    move-result v0

    iget v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->currentSourceDataVersion:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 4

    .prologue
    .line 110
    new-instance v1, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->filter:Lcom/google/android/libraries/bind/data/Filter;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/Filter;Lcom/google/android/libraries/bind/data/DataList;)V

    return-object v1

    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

    goto :goto_0
.end method

.method protected onRegisterForInvalidation()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 93
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->refreshObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 98
    return-void
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->refresh()V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_1
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/FilteredDataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n\nParent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
