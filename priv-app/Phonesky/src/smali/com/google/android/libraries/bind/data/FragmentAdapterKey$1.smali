.class final Lcom/google/android/libraries/bind/data/FragmentAdapterKey$1;
.super Ljava/lang/Object;
.source "FragmentAdapterKey.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/libraries/bind/data/FragmentAdapterKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 53
    const-class v1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/libraries/bind/util/ParcelUtil;->readObjectFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    .line 55
    .local v0, "key":Ljava/lang/Object;
    new-instance v1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentAdapterKey$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 60
    new-array v0, p1, [Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentAdapterKey$1;->newArray(I)[Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    move-result-object v0

    return-object v0
.end method
