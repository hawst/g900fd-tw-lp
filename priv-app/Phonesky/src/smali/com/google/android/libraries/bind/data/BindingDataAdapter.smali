.class public Lcom/google/android/libraries/bind/data/BindingDataAdapter;
.super Lcom/google/android/libraries/bind/data/DataAdapter;
.source "BindingDataAdapter.java"


# static fields
.field private static viewResIdMap:Landroid/util/SparseIntArray;

.field private static final viewResIds:[I


# instance fields
.field private final a11yCardCountKey:I

.field private final equalityFieldsKey:I

.field private originalDataList:Lcom/google/android/libraries/bind/data/DataList;

.field private final viewGeneratorKey:I

.field private final viewResourceIdKey:I

.field private final viewTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/libraries/bind/R$layout;->bind__card_edit_placeholder:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/libraries/bind/R$layout;->bind__card_list_padding:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIds:[I

    return-void
.end method


# virtual methods
.method public findRowWithCardId(Ljava/lang/Object;)I
    .locals 7
    .param p1, "cardId"    # Ljava/lang/Object;

    .prologue
    .line 159
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getCount()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 160
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 161
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_2

    .line 162
    iget v6, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 163
    iget-object v6, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v6, v3}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 177
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "i":I
    :goto_1
    return v3

    .line 167
    .restart local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v3    # "i":I
    :cond_0
    iget v6, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 168
    .local v5, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-interface {v5}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardIds()Ljava/util/List;

    move-result-object v2

    .line 169
    .local v2, "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 170
    .local v1, "generatorCardId":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    .line 159
    .end local v1    # "generatorCardId":Ljava/lang/Object;
    .end local v2    # "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public getA11yRowCount(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 198
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 200
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v1, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v2

    .line 203
    :cond_1
    iget v3, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->a11yCardCountKey:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 204
    .local v0, "count":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getRowFirstCardId(I)Ljava/lang/Object;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 181
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 182
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-object v3

    .line 185
    :cond_1
    iget v4, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 186
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 188
    :cond_2
    iget v4, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 189
    .local v2, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-interface {v2}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardIds()Ljava/util/List;

    move-result-object v1

    .line 190
    .local v1, "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 191
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 89
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 90
    .local v4, "viewResId":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 91
    .local v2, "view":Landroid/view/View;
    if-nez v4, :cond_1

    .line 93
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 96
    .local v3, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    if-eqz v3, :cond_0

    const/4 v5, 0x1

    :goto_0
    const-string v6, "Missing both view resource ID and view generator"

    invoke-static {v5, v6}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 98
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v3, p2, p3, v5}, Lcom/google/android/libraries/bind/card/ViewGenerator;->makeView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v2

    .line 110
    .end local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :goto_1
    return-object v2

    .line 96
    .restart local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 101
    .end local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_1
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->makeLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v5, v6, p2, v7}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    .line 102
    .local v0, "bindingLayout":Landroid/view/View;
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->equalityFieldsKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 103
    .local v1, "equalityFields":[I
    instance-of v5, v0, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v5, :cond_2

    move-object v5, v0

    .line 104
    check-cast v5, Lcom/google/android/libraries/bind/data/DataView;

    iget-object v6, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v7, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v7, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget v10, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-direct {v8, v9, v10}, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;-><init>(II)V

    invoke-virtual {v6, v7, v8, v1}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/libraries/bind/data/DataView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 108
    :cond_2
    move-object v2, v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    return v0
.end method
