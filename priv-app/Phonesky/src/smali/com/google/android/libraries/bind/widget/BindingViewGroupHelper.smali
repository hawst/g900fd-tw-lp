.class public Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;
.super Lcom/google/android/libraries/bind/data/DataViewHelper;
.source "BindingViewGroupHelper.java"


# static fields
.field private static final blendPaint:Landroid/graphics/Paint;


# instance fields
.field private blendBitmapDurationMs:J

.field private blendBitmapStartTimeMs:J

.field private blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

.field private blendedBitmap:Landroid/graphics/Bitmap;

.field private blendedBitmapDstComputed:Z

.field private final blendedBitmapDstRect:Landroid/graphics/Rect;

.field private final blendedBitmapSrcRect:Landroid/graphics/Rect;

.field private boundData:Lcom/google/android/libraries/bind/data/Data;

.field public capturing:Z

.field private cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

.field private cardGroupPosition:I

.field private isOwnedByParent:Z

.field private supportsAnimationCapture:Z

.field private final viewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataView;)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/libraries/bind/data/DataView;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroupPosition:I

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    .line 53
    instance-of v0, p1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 54
    instance-of v0, p1, Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 55
    check-cast p1, Landroid/view/ViewGroup;

    .end local p1    # "view":Lcom/google/android/libraries/bind/data/DataView;
    iput-object p1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    .line 56
    return-void
.end method

.method private clearBlendedBitmap()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 202
    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    .line 203
    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .line 204
    iput-wide v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapStartTimeMs:J

    .line 205
    iput-wide v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapDurationMs:J

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstComputed:Z

    .line 207
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    .line 209
    :cond_0
    return-void
.end method

.method public static markDescendantsAsOwned(Landroid/view/ViewGroup;)V
    .locals 4
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 272
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 273
    .local v0, "childView":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 274
    instance-of v2, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v2, :cond_0

    move-object v2, v0

    .line 275
    check-cast v2, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setOwnedByParent(Z)V

    .line 277
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "childView":Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->markDescendantsAsOwned(Landroid/view/ViewGroup;)V

    .line 271
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 280
    :cond_2
    return-void
.end method


# virtual methods
.method public blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "bitmapRect"    # Landroid/graphics/Rect;
    .param p3, "animationDuration"    # J
    .param p5, "blendMode"    # Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .prologue
    const/4 v1, 0x0

    .line 188
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 189
    iput-object p5, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .line 190
    iput-object p1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapStartTimeMs:J

    .line 194
    iput-wide p3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapDurationMs:J

    .line 195
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 198
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 188
    goto :goto_0
.end method

.method public captureToBitmap(Landroid/graphics/Bitmap;FF)Z
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # F
    .param p3, "top"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    iget-boolean v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->supportsAnimationCapture:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    if-lez v3, :cond_0

    .line 172
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 173
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 175
    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    invoke-direct {v3, v2, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 176
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 178
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->capturing:Z

    .line 179
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 180
    iput-boolean v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->capturing:Z

    .line 183
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 212
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    sget-object v2, Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;->SHOW_SOURCE_HIDE_DESTINATION:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    if-eq v1, v2, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    instance-of v1, v1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    check-cast v1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->superDrawProxy(Landroid/graphics/Canvas;)V

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->capturing:Z

    if-nez v1, :cond_1

    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapStartTimeMs:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget-wide v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendBitmapDurationMs:J

    long-to-float v2, v2

    div-float v0, v1, v2

    .line 220
    .local v0, "blendFraction":F
    cmpl-float v1, v0, v7

    if-ltz v1, :cond_2

    .line 221
    invoke-direct {p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->clearBlendedBitmap()V

    .line 261
    .end local v0    # "blendFraction":F
    :cond_1
    :goto_0
    return-void

    .line 223
    .restart local v0    # "blendFraction":F
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstComputed:Z

    if-nez v1, :cond_7

    .line 224
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->left:I

    .line 225
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 226
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 227
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 230
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-gez v1, :cond_3

    .line 231
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    neg-int v3, v3

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 234
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-gez v1, :cond_4

    .line 235
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 238
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_5

    .line 239
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 242
    :cond_5
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_6

    .line 243
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 246
    :cond_6
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 247
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 248
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 250
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 252
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstComputed:Z

    .line 254
    :cond_7
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendMode:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    sget-object v2, Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;->FADE_SOURCE_ONLY:Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    if-ne v1, v2, :cond_8

    .line 255
    sget-object v1, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x437f0000    # 255.0f

    sub-float v3, v7, v0

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 257
    :cond_8
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapSrcRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendedBitmapDstRect:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 258
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->invalidate()V

    goto/16 :goto_0
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->boundData:Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0
.end method

.method public isOwnedByParent()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isOwnedByParent:Z

    return v0
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->updateBoundDataProxy(Lcom/google/android/libraries/bind/data/Data;)V

    .line 137
    iput-object p1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->boundData:Lcom/google/android/libraries/bind/data/Data;

    .line 138
    invoke-direct {p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->clearBlendedBitmap()V

    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->sendDataToChildrenWhoWantIt(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)V

    .line 141
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->markDescendantsAsOwned(Landroid/view/ViewGroup;)V

    .line 125
    return-void
.end method

.method public prepareForRecycling()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 60
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 61
    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroupPosition:I

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setLongClickable(Z)V

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 71
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setScaleX(F)V

    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setScaleY(F)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setRotation(F)V

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v0, v4, v4}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setMeasuredDimensionProxy(II)V

    .line 82
    :cond_2
    return-void
.end method

.method protected sendDataToChildrenWhoWantIt(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 144
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 145
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 146
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 148
    .local v0, "child":Landroid/view/View;
    instance-of v3, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v3, :cond_2

    move-object v3, v0

    .line 150
    check-cast v3, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v3}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->isOwnedByParent()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 152
    check-cast v3, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v3, p2}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 159
    :cond_0
    :goto_1
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    instance-of v3, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-nez v3, :cond_1

    .line 160
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    invoke-virtual {p0, v0, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->sendDataToChildrenWhoWantIt(Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)V

    .line 145
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 154
    .restart local v0    # "child":Landroid/view/View;
    :cond_2
    instance-of v3, v0, Lcom/google/android/libraries/bind/data/Bound;

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 156
    check-cast v3, Lcom/google/android/libraries/bind/data/Bound;

    invoke-interface {v3, p2}, Lcom/google/android/libraries/bind/data/Bound;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_1

    .line 163
    .end local v0    # "child":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->boundData:Lcom/google/android/libraries/bind/data/Data;

    .line 115
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 116
    return-void
.end method

.method public setOwnedByParent(Z)V
    .locals 0
    .param p1, "isOwnedByParent"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isOwnedByParent:Z

    .line 86
    return-void
.end method

.method public setSupportsAnimationCapture(Z)V
    .locals 0
    .param p1, "supportsAnimationCapture"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->supportsAnimationCapture:Z

    .line 94
    return-void
.end method

.method public startEditingIfPossible()Z
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->viewGroup:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->cardGroupPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/card/CardGroup;->startEditing(Landroid/view/View;I)Z

    move-result v0

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
