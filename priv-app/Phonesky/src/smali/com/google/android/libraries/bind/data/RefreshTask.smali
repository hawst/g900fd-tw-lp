.class public abstract Lcom/google/android/libraries/bind/data/RefreshTask;
.super Ljava/lang/Object;
.source "RefreshTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final cancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final dataList:Lcom/google/android/libraries/bind/data/DataList;

.field private executed:Z

.field private final executor:Ljava/util/concurrent/Executor;

.field protected final previousSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->cancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 22
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->executor:Ljava/util/concurrent/Executor;

    .line 23
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->previousSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 24
    return-void
.end method

.method private hasChanged(Ljava/util/List;[Lcom/google/android/libraries/bind/data/DataChange;)Z
    .locals 12
    .param p2, "outChange"    # [Lcom/google/android/libraries/bind/data/DataChange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;[",
            "Lcom/google/android/libraries/bind/data/DataChange;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 92
    iget-object v8, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->previousSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    invoke-virtual {v8}, Lcom/google/android/libraries/bind/data/Snapshot;->isInvalid()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 93
    sget-object v8, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    aput-object v8, p2, v10

    move v8, v9

    .line 124
    :goto_0
    return v8

    .line 96
    :cond_0
    iget-object v8, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->previousSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v5, v8, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 98
    .local v5, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    if-eq v8, v11, :cond_1

    .line 99
    sget-object v8, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    aput-object v8, p2, v10

    move v8, v9

    .line 100
    goto :goto_0

    .line 103
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    .line 104
    .local v7, "rowCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v7, :cond_3

    .line 105
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/bind/data/Data;

    iget-object v11, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->previousSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget v11, v11, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    invoke-virtual {v8, v11}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 106
    .local v6, "oldPrimaryValue":Ljava/lang/Object;
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/bind/data/Data;

    iget-object v11, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget v11, v11, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    invoke-virtual {v8, v11}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 107
    .local v3, "newPrimaryValue":Ljava/lang/Object;
    invoke-static {v6, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 108
    sget-object v8, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    aput-object v8, p2, v10

    move v8, v9

    .line 109
    goto :goto_0

    .line 104
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 112
    .end local v3    # "newPrimaryValue":Ljava/lang/Object;
    .end local v6    # "oldPrimaryValue":Ljava/lang/Object;
    :cond_3
    sget-object v8, Lcom/google/android/libraries/bind/data/DataChange;->DOESNT_AFFECT_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    aput-object v8, p2, v10

    .line 116
    iget-object v8, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v8}, Lcom/google/android/libraries/bind/data/DataList;->equalityFields()[I

    move-result-object v0

    .line 117
    .local v0, "equalityFields":[I
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v7, :cond_5

    .line 118
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/bind/data/Data;

    .line 119
    .local v4, "oldData":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 120
    .local v2, "newData":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v4, v2, v0}, Lcom/google/android/libraries/bind/data/RefreshTask;->isDataEqual(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;[I)Z

    move-result v8

    if-nez v8, :cond_4

    move v8, v9

    .line 121
    goto :goto_0

    .line 117
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v2    # "newData":Lcom/google/android/libraries/bind/data/Data;
    .end local v4    # "oldData":Lcom/google/android/libraries/bind/data/Data;
    :cond_5
    move v8, v10

    .line 124
    goto :goto_0
.end method

.method private isDataEqual(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/Data;[I)Z
    .locals 5
    .param p1, "oldData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "newData"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "equalityFields"    # [I

    .prologue
    .line 130
    if-nez p3, :cond_0

    .line 131
    invoke-virtual {p1, p2}, Lcom/google/android/libraries/bind/data/Data;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 138
    :goto_0
    return v4

    .line 133
    :cond_0
    move-object v0, p3

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget v1, v0, v2

    .line 134
    .local v1, "field":I
    invoke-virtual {p1, p2, v1}, Lcom/google/android/libraries/bind/data/Data;->equalsField(Lcom/google/android/libraries/bind/data/Data;I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 135
    const/4 v4, 0x0

    goto :goto_0

    .line 133
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 138
    .end local v1    # "field":I
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->cancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 43
    return-void
.end method

.method public execute()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->executed:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->onPreExecute()V

    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 30
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->executed:Z

    .line 31
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getFreshData()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->cancelled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method protected postRefresh(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "snapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/libraries/bind/data/DataList;->postRefresh(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 79
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    const/4 v3, 0x0

    .line 57
    .local v3, "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->getFreshData()Ljava/util/List;

    move-result-object v2

    .line 58
    .local v2, "freshData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 61
    const/4 v6, 0x1

    new-array v5, v6, [Lcom/google/android/libraries/bind/data/DataChange;

    .line 62
    .local v5, "outDataChanged":[Lcom/google/android/libraries/bind/data/DataChange;
    invoke-direct {p0, v2, v5}, Lcom/google/android/libraries/bind/data/RefreshTask;->hasChanged(Ljava/util/List;[Lcom/google/android/libraries/bind/data/DataChange;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 65
    new-instance v4, Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v6, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget v6, v6, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    invoke-direct {v4, v6, v2}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;)V
    :try_end_0
    .catch Lcom/google/android/libraries/bind/data/DataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .end local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .local v4, "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    const/4 v6, 0x0

    :try_start_1
    aget-object v0, v5, v6
    :try_end_1
    .catch Lcom/google/android/libraries/bind/data/DataException; {:try_start_1 .. :try_end_1} :catch_1

    .local v0, "change":Lcom/google/android/libraries/bind/data/DataChange;
    move-object v3, v4

    .line 71
    .end local v2    # "freshData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .end local v4    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .end local v5    # "outDataChanged":[Lcom/google/android/libraries/bind/data/DataChange;
    .restart local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 74
    invoke-virtual {p0, v3, v0}, Lcom/google/android/libraries/bind/data/RefreshTask;->postRefresh(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_0

    .line 67
    .end local v0    # "change":Lcom/google/android/libraries/bind/data/DataChange;
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Lcom/google/android/libraries/bind/data/DataException;
    :goto_2
    new-instance v3, Lcom/google/android/libraries/bind/data/Snapshot;

    .end local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    iget-object v6, p0, Lcom/google/android/libraries/bind/data/RefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget v6, v6, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    invoke-direct {v3, v6, v1}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILcom/google/android/libraries/bind/data/DataException;)V

    .line 69
    .restart local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    new-instance v0, Lcom/google/android/libraries/bind/data/DataChange;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/data/DataChange;-><init>(Lcom/google/android/libraries/bind/data/DataException;)V

    .restart local v0    # "change":Lcom/google/android/libraries/bind/data/DataChange;
    goto :goto_1

    .line 67
    .end local v0    # "change":Lcom/google/android/libraries/bind/data/DataChange;
    .end local v1    # "e":Lcom/google/android/libraries/bind/data/DataException;
    .end local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .restart local v2    # "freshData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .restart local v4    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .restart local v5    # "outDataChanged":[Lcom/google/android/libraries/bind/data/DataChange;
    :catch_1
    move-exception v1

    move-object v3, v4

    .end local v4    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    .restart local v3    # "newSnapshot":Lcom/google/android/libraries/bind/data/Snapshot;
    goto :goto_2
.end method
