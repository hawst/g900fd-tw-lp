.class public Lcom/google/android/libraries/bind/widget/BoundTextView;
.super Landroid/widget/TextView;
.source "BoundTextView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private bindDrawableEndKey:Ljava/lang/Integer;

.field private bindDrawableStartKey:Ljava/lang/Integer;

.field private bindTextColorKey:Ljava/lang/Integer;

.field private bindTextKey:Ljava/lang/Integer;

.field private final boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundTextView;->makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 45
    sget-object v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView:[I

    invoke-virtual {p1, p2, v2, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 47
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView_bind__editModeText:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    .local v1, "editModeText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    .end local v1    # "editModeText":Ljava/lang/String;
    :cond_0
    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView_bindText:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextKey:Ljava/lang/Integer;

    .line 55
    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView_bindTextColor:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextColorKey:Ljava/lang/Integer;

    .line 56
    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView_bindDrawableStart:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableStartKey:Ljava/lang/Integer;

    .line 57
    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BoundTextView_bindDrawableEnd:I

    invoke-static {v0, v2}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableEndKey:Ljava/lang/Integer;

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private updateBoundDrawableIfSpecified([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V
    .locals 5
    .param p1, "drawables"    # [Landroid/graphics/drawable/Drawable;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "bindDrawableKey"    # Ljava/lang/Integer;
    .param p4, "isStart"    # Z

    .prologue
    const/4 v3, 0x0

    .line 111
    if-eqz p3, :cond_0

    .line 112
    if-nez p2, :cond_1

    move-object v2, v3

    .line 114
    .local v2, "optDrawableId":Ljava/lang/Integer;
    :goto_0
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v4

    if-nez v4, :cond_2

    if-eqz p4, :cond_2

    const/4 v1, 0x0

    .line 116
    .local v1, "index":I
    :goto_1
    if-nez v2, :cond_3

    move-object v0, v3

    .line 118
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    aput-object v0, p1, v1

    .line 120
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "index":I
    .end local v2    # "optDrawableId":Ljava/lang/Integer;
    :cond_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p2, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 114
    .restart local v2    # "optDrawableId":Ljava/lang/Integer;
    :cond_2
    const/4 v1, 0x2

    goto :goto_1

    .line 116
    .restart local v1    # "index":I
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 7
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 68
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v3, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 70
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextKey:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 71
    if-nez p1, :cond_4

    .line 72
    invoke-virtual {p0, v4}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextColorKey:Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 86
    if-nez p1, :cond_7

    move-object v2, v4

    .line 87
    .local v2, "textColor":Ljava/lang/Object;
    :goto_1
    if-nez v2, :cond_8

    .line 88
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(I)V

    .line 100
    .end local v2    # "textColor":Ljava/lang/Object;
    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableStartKey:Ljava/lang/Integer;

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableEndKey:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 102
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableStartKey:Ljava/lang/Integer;

    invoke-direct {p0, v0, p1, v3, v6}, Lcom/google/android/libraries/bind/widget/BoundTextView;->updateBoundDrawableIfSpecified([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V

    .line 103
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindDrawableEndKey:Ljava/lang/Integer;

    invoke-direct {p0, v0, p1, v3, v5}, Lcom/google/android/libraries/bind/widget/BoundTextView;->updateBoundDrawableIfSpecified([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V

    .line 104
    aget-object v3, v0, v5

    aget-object v4, v0, v6

    const/4 v5, 0x2

    aget-object v5, v0, v5

    const/4 v6, 0x3

    aget-object v6, v0, v6

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 107
    .end local v0    # "drawables":[Landroid/graphics/drawable/Drawable;
    :cond_3
    return-void

    .line 74
    :cond_4
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextKey:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 77
    .local v1, "text":Ljava/lang/Object;
    instance-of v3, v1, Landroid/text/SpannableString;

    if-eqz v3, :cond_5

    .line 78
    check-cast v1, Landroid/text/SpannableString;

    .end local v1    # "text":Ljava/lang/Object;
    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v1, v3}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_0

    .line 80
    .restart local v1    # "text":Ljava/lang/Object;
    :cond_5
    if-nez v1, :cond_6

    move-object v3, v4

    :goto_3
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 86
    .end local v1    # "text":Ljava/lang/Object;
    :cond_7
    iget-object v3, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->bindTextColorKey:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto :goto_1

    .line 89
    .restart local v2    # "textColor":Ljava/lang/Object;
    :cond_8
    instance-of v3, v2, Landroid/content/res/ColorStateList;

    if-eqz v3, :cond_9

    .line 90
    check-cast v2, Landroid/content/res/ColorStateList;

    .end local v2    # "textColor":Ljava/lang/Object;
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_2

    .line 92
    .restart local v2    # "textColor":Ljava/lang/Object;
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "textColor":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(I)V

    goto :goto_2
.end method
