.class public Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;
.super Ljava/lang/Object;
.source "PriorityDataSetObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;
    }
.end annotation


# instance fields
.field private final observerEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public add(Landroid/database/DataSetObserver;I)Z
    .locals 4
    .param p1, "observer"    # Landroid/database/DataSetObserver;
    .param p2, "priority"    # I

    .prologue
    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Observer is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 40
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    .line 41
    .local v1, "wasEmpty":Z
    new-instance v0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;

    invoke-direct {v0, p1, p2}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;-><init>(Landroid/database/DataSetObserver;I)V

    .line 42
    .local v0, "entry":Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 44
    return v1
.end method

.method public remove(Landroid/database/DataSetObserver;)Z
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Observer is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    const/4 v1, 0x0

    .line 65
    :goto_0
    return v1

    .line 59
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 60
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->observer:Landroid/database/DataSetObserver;

    if-ne v1, p1, :cond_3

    .line 61
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 65
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    goto :goto_0

    .line 59
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "  observers count: %d\n"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;

    .line 93
    .local v1, "entry":Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;
    const-string v3, "    "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 97
    .end local v1    # "entry":Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
