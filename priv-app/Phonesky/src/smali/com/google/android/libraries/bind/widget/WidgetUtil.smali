.class public Lcom/google/android/libraries/bind/widget/WidgetUtil;
.super Ljava/lang/Object;
.source "WidgetUtil.java"


# static fields
.field private static tempLocation:[I

.field private static tempRect:Landroid/graphics/Rect;

.field private static tempRect2:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    .line 12
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    return-void
.end method

.method public static isVisibleOnScreen(Landroid/view/View;)Z
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method
