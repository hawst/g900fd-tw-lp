.class public Lcom/google/android/libraries/bind/data/Snapshot;
.super Ljava/lang/Object;
.source "Snapshot.java"


# static fields
.field private static final INVALID_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private static final INVALID_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final exception:Lcom/google/android/libraries/bind/data/DataException;

.field public final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field public final primaryKey:I

.field private final primaryKeyIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_LIST:Ljava/util/List;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_MAP:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "primaryKey"    # I

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    .line 78
    sget-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_MAP:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKeyIndex:Ljava/util/Map;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->exception:Lcom/google/android/libraries/bind/data/DataException;

    .line 81
    return-void
.end method

.method public constructor <init>(ILcom/google/android/libraries/bind/data/DataException;)V
    .locals 1
    .param p1, "primaryKey"    # I
    .param p2, "exception"    # Lcom/google/android/libraries/bind/data/DataException;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    .line 70
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/Snapshot;->exception:Lcom/google/android/libraries/bind/data/DataException;

    .line 71
    sget-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 72
    sget-object v0, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_MAP:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKeyIndex:Ljava/util/Map;

    .line 73
    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {p2, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->indexByPrimaryKey(Ljava/util/List;I)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/Snapshot;-><init>(ILjava/util/List;Ljava/util/Map;)V

    .line 41
    return-void
.end method

.method public constructor <init>(ILjava/util/List;Ljava/util/Map;)V
    .locals 3
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p3, "primaryKeyIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    .line 54
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 55
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKeyIndex:Ljava/util/Map;

    .line 56
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/libraries/bind/data/Snapshot;->exception:Lcom/google/android/libraries/bind/data/DataException;

    .line 57
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 58
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Data;->freeze()V

    goto :goto_0

    .line 60
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-void
.end method

.method public static indexByKey(Ljava/util/List;I)Ljava/util/Map;
    .locals 13
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 88
    if-nez p0, :cond_1

    .line 89
    sget-object v2, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_MAP:Ljava/util/Map;

    .line 117
    :cond_0
    :goto_0
    return-object v2

    .line 91
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 92
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    goto :goto_0

    .line 94
    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 95
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 96
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 97
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_3

    .line 98
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Entry %d has no data"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 100
    :cond_3
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 101
    .local v4, "primaryValue":Ljava/lang/Object;
    if-nez v4, :cond_4

    .line 102
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Entry %d has an empty primary key %s - %s"

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-static {p1}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v10

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v5}, Lcom/google/android/libraries/bind/data/Data;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 105
    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 106
    .local v3, "previous":Ljava/lang/Object;
    if-eqz v3, :cond_5

    .line 107
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Duplicate entries for primary key %s, value %s (class %s), positions %s and %s"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v4, v7, v10

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    aput-object v3, v7, v12

    const/4 v8, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 95
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method public static indexByPrimaryKey(Ljava/util/List;I)Ljava/util/Map;
    .locals 1
    .param p1, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->indexByKey(Ljava/util/List;I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cloneList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 236
    .local v0, "clonedList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    .line 237
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    .end local v1    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-object v0
.end method

.method public findPositionForPrimaryValue(Ljava/lang/Object;)I
    .locals 2
    .param p1, "id"    # Ljava/lang/Object;

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKeyIndex:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 194
    .local v0, "position":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getData(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->isInvalidPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0
.end method

.method public getException()Lcom/google/android/libraries/bind/data/DataException;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->exception:Lcom/google/android/libraries/bind/data/DataException;

    return-object v0
.end method

.method public getItemId(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/Snapshot;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const/4 v1, 0x0

    .line 181
    :goto_0
    return-object v1

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 181
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    iget v1, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public hasException()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->exception:Lcom/google/android/libraries/bind/data/DataException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isInvalid()Z
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    sget-object v1, Lcom/google/android/libraries/bind/data/Snapshot;->INVALID_LIST:Ljava/util/List;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInvalidPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 155
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 122
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s - primaryKey: %s, size: %d, exception: %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget v4, p0, Lcom/google/android/libraries/bind/data/Snapshot;->primaryKey:I

    invoke-static {v4}, Lcom/google/android/libraries/bind/data/Data;->keyName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/Snapshot;->getCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/Snapshot;->hasException()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/Snapshot;->getException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataException;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method
