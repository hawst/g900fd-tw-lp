.class public abstract Lcom/google/android/vending/verifier/api/BaseVerificationRequest;
.super Lcom/android/volley/Request;
.source "BaseVerificationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/android/volley/Request",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final mRequest:Lcom/google/protobuf/nano/MessageNano;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TU;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;Lcom/google/protobuf/nano/MessageNano;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$ErrorListener;",
            "TU;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequest;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequest<TT;TU;>;"
    .local p3, "request":Lcom/google/protobuf/nano/MessageNano;, "TU;"
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 28
    iput-object p3, p0, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->mRequest:Lcom/google/protobuf/nano/MessageNano;

    .line 29
    return-void
.end method

.method private makeUserAgentString()Ljava/lang/String;
    .locals 13

    .prologue
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequest;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequest<TT;TU;>;"
    const/4 v12, 0x0

    .line 50
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    .line 51
    .local v2, "context":Landroid/content/Context;
    sget-object v8, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "device":Ljava/lang/String;
    sget-object v8, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 53
    .local v5, "hardware":Ljava/lang/String;
    sget-object v8, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 54
    .local v7, "product":Ljava/lang/String;
    sget-object v8, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "buildType":Ljava/lang/String;
    sget-object v8, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "buildId":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 63
    .local v6, "pi":Landroid/content/pm/PackageInfo;
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Android-Finsky/%s (versionCode=%d,sdk=%d,device=%s,hardware=%s,product=%s,build=%s:%s)"

    const/16 v10, 0x8

    new-array v10, v10, [Ljava/lang/Object;

    iget-object v11, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v11, v10, v12

    const/4 v11, 0x1

    iget v12, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    aput-object v3, v10, v11

    const/4 v11, 0x4

    aput-object v5, v10, v11

    const/4 v11, 0x5

    aput-object v7, v10, v11

    const/4 v11, 0x6

    aput-object v0, v10, v11

    const/4 v11, 0x7

    aput-object v1, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8

    .line 59
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v4

    .line 61
    .local v4, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8
.end method

.method private static sanitizeHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "("

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ")"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBody()[B
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequest;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequest<TT;TU;>;"
    iget-object v0, p0, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->mRequest:Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method

.method public getBodyContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequest;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequest<TT;TU;>;"
    const-string v0, "application/x-protobuffer"

    return-object v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequest;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequest<TT;TU;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 34
    .local v0, "headerMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "User-Agent"

    invoke-direct {p0}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;->makeUserAgentString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string v1, "Connection"

    const-string v2, "close"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-object v0
.end method
