.class Lcom/google/android/vending/verifier/PackageVerificationService$4;
.super Ljava/lang/Object;
.source "PackageVerificationService.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerifyAllAppsRequest(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<[",
        "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

.field final synthetic val$requests:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1167
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iput-object p2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->val$requests:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1167
    check-cast p1, [Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$4;->onResponse([Lcom/google/android/vending/verifier/api/PackageVerificationResult;)V

    return-void
.end method

.method public onResponse([Lcom/google/android/vending/verifier/api/PackageVerificationResult;)V
    .locals 9
    .param p1, "responses"    # [Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    .prologue
    .line 1172
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->val$requests:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 1173
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->val$requests:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/verifier/PackageVerificationData;

    .line 1174
    .local v0, "appData":Lcom/google/android/vending/verifier/PackageVerificationData;
    aget-object v4, p1, v1

    .line 1179
    .local v4, "response":Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    if-nez v4, :cond_1

    const/4 v5, 0x0

    .line 1181
    .local v5, "verdict":I
    :goto_1
    packed-switch v5, :pswitch_data_0

    .line 1172
    :cond_0
    :goto_2
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1179
    .end local v5    # "verdict":I
    :cond_1
    iget v5, v4, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->verdict:I

    goto :goto_1

    .line 1183
    .restart local v5    # "verdict":I
    :pswitch_1
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->isAlreadyInstalled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1184
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1185
    .local v2, "label":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 1186
    iget-object v2, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    .line 1188
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v3

    .line 1189
    .local v3, "notifier":Lcom/google/android/finsky/utils/Notifier;
    iget-object v6, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->description:Ljava/lang/String;

    invoke-interface {v3, v2, v6, v7}, Lcom/google/android/finsky/utils/Notifier;->showHarmfulAppRemovedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->stopApplication(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1600(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/lang/String;)V

    .line 1195
    invoke-static {}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-result-object v6

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->uninstallPackage(Ljava/lang/String;)V

    goto :goto_2

    .line 1200
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    :pswitch_2
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->isAlreadyInstalled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-boolean v6, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mSuppressUserWarning:Z

    if-nez v6, :cond_0

    .line 1202
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->stopApplication(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1600(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/lang/String;)V

    .line 1203
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1204
    .restart local v2    # "label":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 1205
    iget-object v2, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    .line 1207
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v3

    .line 1208
    .restart local v3    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    iget-object v6, v0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->description:Ljava/lang/String;

    iget-object v8, v4, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->token:[B

    invoke-interface {v3, v2, v6, v7, v8}, Lcom/google/android/finsky/utils/Notifier;->showHarmfulAppRemoveRequestMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_2

    .line 1213
    .end local v0    # "appData":Lcom/google/android/vending/verifier/PackageVerificationData;
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    .end local v4    # "response":Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    .end local v5    # "verdict":I
    :cond_4
    iget-object v6, p0, Lcom/google/android/vending/verifier/PackageVerificationService$4;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    const/4 v7, 0x1

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->cleanupVerifyInstalledPackages(Z)V
    invoke-static {v6, v7}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1000(Lcom/google/android/vending/verifier/PackageVerificationService;Z)V

    .line 1214
    return-void

    .line 1181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
