.class public Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
.super Ljava/lang/Object;
.source "PackageVerificationApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/api/PackageVerificationApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileInfo"
.end annotation


# instance fields
.field public final digest:[B

.field public final name:Ljava/lang/String;

.field public final verificationErrors:I


# direct methods
.method public constructor <init>(Ljava/lang/String;[BI)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "digest"    # [B
    .param p3, "verificationErrors"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->name:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->digest:[B

    .line 51
    iput p3, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->verificationErrors:I

    .line 52
    return-void
.end method
