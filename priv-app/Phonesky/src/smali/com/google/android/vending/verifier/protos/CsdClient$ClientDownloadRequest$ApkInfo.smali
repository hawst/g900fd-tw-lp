.class public final Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CsdClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApkInfo"
.end annotation


# instance fields
.field public dontWarnAgain:Z

.field public files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

.field public forwardLocked:Z

.field public hasDontWarnAgain:Z

.field public hasForwardLocked:Z

.field public hasInStoppedState:Z

.field public hasInstalledByPlay:Z

.field public hasPackageName:Z

.field public hasVersionCode:Z

.field public inStoppedState:Z

.field public installedByPlay:Z

.field public packageName:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 897
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 898
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    .line 899
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 902
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    .line 903
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasPackageName:Z

    .line 904
    iput v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    .line 905
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasVersionCode:Z

    .line 906
    invoke-static {}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->emptyArray()[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    .line 907
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    .line 908
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInstalledByPlay:Z

    .line 909
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    .line 910
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasForwardLocked:Z

    .line 911
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    .line 912
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInStoppedState:Z

    .line 913
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    .line 914
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasDontWarnAgain:Z

    .line 915
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->cachedSize:I

    .line 916
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 953
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 954
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasPackageName:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 955
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 958
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasVersionCode:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    if-eqz v3, :cond_3

    .line 959
    :cond_2
    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 962
    :cond_3
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 963
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 964
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    aget-object v0, v3, v1

    .line 965
    .local v0, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    if-eqz v0, :cond_4

    .line 966
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 963
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 971
    .end local v0    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInstalledByPlay:Z

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    if-eqz v3, :cond_7

    .line 972
    :cond_6
    const/4 v3, 0x4

    iget-boolean v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 975
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasForwardLocked:Z

    if-nez v3, :cond_8

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    if-eqz v3, :cond_9

    .line 976
    :cond_8
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 979
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInStoppedState:Z

    if-nez v3, :cond_a

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    if-eqz v3, :cond_b

    .line 980
    :cond_a
    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 983
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasDontWarnAgain:Z

    if-nez v3, :cond_c

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    if-eqz v3, :cond_d

    .line 984
    :cond_c
    const/4 v3, 0x7

    iget-boolean v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 987
    :cond_d
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 995
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 996
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1000
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1001
    :sswitch_0
    return-object p0

    .line 1006
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    .line 1007
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasPackageName:Z

    goto :goto_0

    .line 1011
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    .line 1012
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasVersionCode:Z

    goto :goto_0

    .line 1016
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1018
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 1019
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    .line 1021
    .local v2, "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    if-eqz v1, :cond_1

    .line 1022
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1024
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1025
    new-instance v5, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    invoke-direct {v5}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;-><init>()V

    aput-object v5, v2, v1

    .line 1026
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1027
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1024
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1018
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    array-length v1, v5

    goto :goto_1

    .line 1030
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    :cond_3
    new-instance v5, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    invoke-direct {v5}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;-><init>()V

    aput-object v5, v2, v1

    .line 1031
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1032
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    goto :goto_0

    .line 1036
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    .line 1037
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInstalledByPlay:Z

    goto :goto_0

    .line 1041
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    .line 1042
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasForwardLocked:Z

    goto :goto_0

    .line 1046
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    .line 1047
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInStoppedState:Z

    goto :goto_0

    .line 1051
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    .line 1052
    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasDontWarnAgain:Z

    goto/16 :goto_0

    .line 996
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 922
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasPackageName:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 923
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 925
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasVersionCode:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    if-eqz v2, :cond_3

    .line 926
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 928
    :cond_3
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 929
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 930
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    aget-object v0, v2, v1

    .line 931
    .local v0, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    if-eqz v0, :cond_4

    .line 932
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 929
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 936
    .end local v0    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInstalledByPlay:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    if-eqz v2, :cond_7

    .line 937
    :cond_6
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 939
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasForwardLocked:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    if-eqz v2, :cond_9

    .line 940
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 942
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInStoppedState:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    if-eqz v2, :cond_b

    .line 943
    :cond_a
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 945
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasDontWarnAgain:Z

    if-nez v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    if-eqz v2, :cond_d

    .line 946
    :cond_c
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 948
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 949
    return-void
.end method
