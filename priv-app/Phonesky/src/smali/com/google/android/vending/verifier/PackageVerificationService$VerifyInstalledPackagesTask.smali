.class Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;
.super Landroid/os/AsyncTask;
.source "PackageVerificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/PackageVerificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VerifyInstalledPackagesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/vending/verifier/PackageVerificationData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationService;


# direct methods
.method public constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationService;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 437
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->mContext:Landroid/content/Context;

    .line 438
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 431
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->doInBackground([Ljava/lang/Void;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/Map;
    .locals 30
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/vending/verifier/PackageVerificationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v21

    .line 449
    .local v21, "installedPackagesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-virtual {v10}, Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/16 v27, 0x40

    move/from16 v0, v27

    invoke-virtual {v10, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v20

    .line 451
    .local v20, "installedPackages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    new-instance v22, Lcom/google/android/vending/verifier/PackageVerificationDataStore;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v0, v10}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;-><init>(Landroid/content/Context;)V

    .line 452
    .local v22, "mDataStore":Lcom/google/android/vending/verifier/PackageVerificationDataStore;
    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v10}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 454
    .local v11, "acceptedAntiMalwareConsent":Z
    sget-object v10, Lcom/google/android/finsky/config/G;->verifyInstalledPlayPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v10}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v26

    .line 457
    .local v26, "verifyPlayApps":Z
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->getAll()Ljava/util/Map;

    move-result-object v14

    .line 458
    .local v14, "dataStoreEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    new-instance v25, Ljava/util/HashSet;

    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    move-object/from16 v0, v25

    invoke-direct {v0, v10}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 461
    .local v25, "staleEntriesToRemoveFromDataStore":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/pm/PackageInfo;

    .line 462
    .local v23, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->isCancelled()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 463
    const/16 v21, 0x0

    .line 548
    .end local v21    # "installedPackagesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    .end local v23    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    return-object v21

    .line 466
    .restart local v21    # "installedPackagesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    .restart local v23    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    move-object/from16 v0, v23

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 469
    .local v3, "packageName":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 471
    move-object/from16 v0, v23

    iget-object v13, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 473
    .local v13, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v27, 0x9

    move/from16 v0, v27

    if-lt v10, v0, :cond_8

    .line 474
    move-object/from16 v0, v23

    iget-wide v4, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    .line 480
    .local v4, "packageFingerprint":J
    :goto_1
    if-eqz v13, :cond_3

    iget v10, v13, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v10, v10, 0x1

    if-nez v10, :cond_0

    .line 485
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v10

    invoke-virtual {v10, v3}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    .line 486
    .local v12, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_9

    const/16 v19, 0x1

    .line 487
    .local v19, "installedByPlay":Z
    :goto_2
    if-nez v19, :cond_4

    if-eqz v11, :cond_0

    .line 489
    :cond_4
    if-eqz v19, :cond_5

    if-eqz v26, :cond_0

    .line 494
    :cond_5
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->get(Ljava/lang/String;)Lcom/google/android/vending/verifier/PackageVerificationData;

    move-result-object v2

    .line 497
    .local v2, "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    if-eqz v2, :cond_6

    iget-wide v0, v2, Lcom/google/android/vending/verifier/PackageVerificationData;->mCacheFingerprint:J

    move-wide/from16 v28, v0

    cmp-long v10, v4, v28

    if-eqz v10, :cond_7

    .line 499
    :cond_6
    const-wide/16 v7, 0x0

    .line 500
    .local v7, "length":J
    const/4 v6, 0x0

    .line 501
    .local v6, "digest":[B
    iget-object v0, v13, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 502
    .local v24, "path":Ljava/lang/String;
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 504
    .local v17, "file":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_a

    .line 505
    const-string v10, "Cannot find file for %s"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v24, v27, v28

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 521
    :goto_3
    if-eqz v6, :cond_0

    .line 525
    iget-object v10, v13, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    iget-object v0, v13, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_c

    const/4 v9, 0x1

    .line 528
    .local v9, "forwardLocked":Z
    :goto_4
    new-instance v2, Lcom/google/android/vending/verifier/PackageVerificationData;

    .end local v2    # "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/vending/verifier/PackageVerificationData;-><init>(Ljava/lang/String;J[BJZZ)V

    .line 530
    .restart local v2    # "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->put(Lcom/google/android/vending/verifier/PackageVerificationData;)V

    .line 533
    .end local v6    # "digest":[B
    .end local v7    # "length":J
    .end local v9    # "forwardLocked":Z
    .end local v17    # "file":Ljava/io/File;
    .end local v24    # "path":Ljava/lang/String;
    :cond_7
    move/from16 v0, v19

    iput-boolean v0, v2, Lcom/google/android/vending/verifier/PackageVerificationData;->mInstalledByPlay:Z

    .line 534
    move-object/from16 v0, v23

    iget-object v10, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v10, v10, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v27, 0x200000

    and-int v10, v10, v27

    if-eqz v10, :cond_d

    const/4 v10, 0x1

    :goto_5
    iput-boolean v10, v2, Lcom/google/android/vending/verifier/PackageVerificationData;->mInStoppedState:Z

    .line 536
    move-object/from16 v0, v23

    iget-object v10, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->getCertificateFingerprints([Landroid/content/pm/Signature;)[[B
    invoke-static {v10}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$800([Landroid/content/pm/Signature;)[[B

    move-result-object v10

    iput-object v10, v2, Lcom/google/android/vending/verifier/PackageVerificationData;->mCertFingerprints:[[B

    .line 539
    iget-object v10, v2, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-interface {v0, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    const-string v10, "Adding %s for verification"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v3, v27, v28

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 476
    .end local v2    # "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    .end local v4    # "packageFingerprint":J
    .end local v12    # "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .end local v19    # "installedByPlay":Z
    :cond_8
    move-object/from16 v0, v23

    iget v10, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v4, v10

    .restart local v4    # "packageFingerprint":J
    goto/16 :goto_1

    .line 486
    .restart local v12    # "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 506
    .restart local v2    # "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    .restart local v6    # "digest":[B
    .restart local v7    # "length":J
    .restart local v17    # "file":Ljava/io/File;
    .restart local v19    # "installedByPlay":Z
    .restart local v24    # "path":Ljava/lang/String;
    :cond_a
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->canRead()Z

    move-result v10

    if-nez v10, :cond_b

    .line 507
    const-string v10, "Cannot read file for %s"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v24, v27, v28

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 509
    :cond_b
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v7

    .line 511
    :try_start_0
    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->getSha256Hash(Ljava/io/File;)[B
    invoke-static/range {v17 .. v17}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$700(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    goto/16 :goto_3

    .line 512
    :catch_0
    move-exception v16

    .line 513
    .local v16, "ex":Ljava/io/IOException;
    const-string v10, "Error while calculating sha256 for file=%s, error=%s"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v24, v27, v28

    const/16 v28, 0x1

    aput-object v16, v27, v28

    move-object/from16 v0, v27

    invoke-static {v10, v0}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 515
    .end local v16    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v16

    .line 516
    .local v16, "ex":Ljava/security/NoSuchAlgorithmException;
    new-instance v10, Ljava/lang/RuntimeException;

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 525
    .end local v16    # "ex":Ljava/security/NoSuchAlgorithmException;
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 534
    .end local v6    # "digest":[B
    .end local v7    # "length":J
    .end local v17    # "file":Ljava/io/File;
    .end local v24    # "path":Ljava/lang/String;
    :cond_d
    const/4 v10, 0x0

    goto :goto_5

    .line 544
    .end local v2    # "verificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "packageFingerprint":J
    .end local v12    # "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .end local v13    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v19    # "installedByPlay":Z
    .end local v23    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_e
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 545
    .local v15, "entry":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->remove(Ljava/lang/String;)V

    goto :goto_6
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 431
    check-cast p1, Ljava/util/Map;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->onPostExecute(Ljava/util/Map;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/vending/verifier/PackageVerificationData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 553
    .local p1, "installedPackagesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 554
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerifyAllAppsRequest(Ljava/util/Map;)V
    invoke-static {v0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$900(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/util/Map;)V

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->cleanupVerifyInstalledPackages(Z)V
    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1000(Lcom/google/android/vending/verifier/PackageVerificationService;Z)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 442
    const-string v0, "Verifying installed apps"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 443
    return-void
.end method
