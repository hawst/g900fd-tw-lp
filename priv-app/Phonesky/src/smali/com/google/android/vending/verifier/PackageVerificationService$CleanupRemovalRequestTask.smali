.class Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;
.super Landroid/os/AsyncTask;
.source "PackageVerificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/PackageVerificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupRemovalRequestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDontWarn:Z

.field private final mPackageName:Ljava/lang/String;

.field private final mResponseToken:[B

.field private final mUninstall:Z

.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationService;


# direct methods
.method public constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/lang/String;ZZ[B)V
    .locals 0
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uninstall"    # Z
    .param p4, "dontWarn"    # Z
    .param p5, "responseToken"    # [B

    .prologue
    .line 576
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 577
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mContext:Landroid/content/Context;

    .line 578
    iput-object p2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    .line 579
    iput-boolean p3, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mUninstall:Z

    .line 580
    iput-boolean p4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mDontWarn:Z

    .line 581
    iput-object p5, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mResponseToken:[B

    .line 582
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 567
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 586
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;-><init>(Landroid/content/Context;)V

    .line 588
    .local v0, "datastore":Lcom/google/android/vending/verifier/PackageVerificationDataStore;
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mUninstall:Z

    if-eqz v1, :cond_0

    .line 589
    const-string v1, "Uninstalling %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 590
    invoke-static {}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->uninstallPackage(Ljava/lang/String;)V

    .line 591
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->remove(Ljava/lang/String;)V

    .line 595
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 593
    :cond_0
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mDontWarn:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->setSuppressUserWarning(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 567
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "param"    # Ljava/lang/Void;

    .prologue
    .line 603
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mUninstall:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 605
    .local v0, "decision":I
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mDontWarn:Z

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mResponseToken:[B

    new-instance v3, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask$1;

    invoke-direct {v3, p0}, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask$1;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;)V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->reportUserDecision(IZ[BLcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 613
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    # getter for: Lcom/google/android/vending/verifier/PackageVerificationService;->mRemovalRequests:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1200(Lcom/google/android/vending/verifier/PackageVerificationService;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 614
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->stopServiceIfIdle()V
    invoke-static {v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1300(Lcom/google/android/vending/verifier/PackageVerificationService;)V

    .line 615
    return-void

    .line 603
    .end local v0    # "decision":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
