.class public Lcom/google/android/vending/verifier/api/PackageVerificationResult;
.super Ljava/lang/Object;
.source "PackageVerificationResult.java"


# instance fields
.field public final description:Ljava/lang/String;

.field public final moreInfoUri:Landroid/net/Uri;

.field public final token:[B

.field public final uploadApk:Z

.field public final verdict:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/net/Uri;[BZ)V
    .locals 0
    .param p1, "verdict"    # I
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "moreInfoUri"    # Landroid/net/Uri;
    .param p4, "token"    # [B
    .param p5, "uploadApk"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->verdict:I

    .line 43
    iput-object p2, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->description:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->moreInfoUri:Landroid/net/Uri;

    .line 45
    iput-object p4, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->token:[B

    .line 46
    iput-boolean p5, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->uploadApk:Z

    .line 47
    return-void
.end method

.method public static fromResponse(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;)Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    .locals 7
    .param p0, "response"    # Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    .prologue
    .line 51
    const/4 v2, 0x0

    .line 52
    .local v2, "description":Ljava/lang/String;
    const/4 v3, 0x0

    .line 53
    .local v3, "uri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    .line 54
    .local v6, "moreInfo":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;
    if-eqz v6, :cond_0

    .line 55
    iget-object v2, v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->description:Ljava/lang/String;

    .line 56
    iget-object v0, v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->url:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 60
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    .line 61
    .local v5, "uploadApk":Z
    new-instance v0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    iget v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    iget-object v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    invoke-direct/range {v0 .. v5}, Lcom/google/android/vending/verifier/api/PackageVerificationResult;-><init>(ILjava/lang/String;Landroid/net/Uri;[BZ)V

    return-object v0
.end method
