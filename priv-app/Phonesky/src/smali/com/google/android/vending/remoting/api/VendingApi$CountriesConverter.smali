.class Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;
.super Ljava/lang/Object;
.source "VendingApi.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/remoting/api/VendingApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CountriesConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;",
        ">;"
    }
.end annotation


# instance fields
.field private final mListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<[",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/volley/Response$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<[",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object p1, p0, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;->mListener:Lcom/android/volley/Response$Listener;

    .line 247
    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;)V
    .locals 2
    .param p1, "response"    # Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    .prologue
    .line 251
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;->mListener:Lcom/android/volley/Response$Listener;

    iget-object v1, p1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    iget-object v1, v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-interface {v0, v1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 256
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;->mListener:Lcom/android/volley/Response$Listener;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-interface {v0, v1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 242
    check-cast p1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;->onResponse(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;)V

    return-void
.end method
