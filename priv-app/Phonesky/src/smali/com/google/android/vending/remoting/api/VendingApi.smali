.class public Lcom/google/android/vending/remoting/api/VendingApi;
.super Ljava/lang/Object;
.source "VendingApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;
    }
.end annotation


# static fields
.field private static final SEND_AD_ID_FOR_CONTENT_SYNC:Z


# instance fields
.field private final mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

.field private final mRequestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->sendAdIdInRequestsForRads:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/vending/remoting/api/VendingApi;->SEND_AD_ID_FOR_CONTENT_SYNC:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/RequestQueue;Lcom/google/android/vending/remoting/api/VendingApiContext;)V
    .locals 0
    .param p1, "requestQueue"    # Lcom/android/volley/RequestQueue;
    .param p2, "apiContext"    # Lcom/google/android/vending/remoting/api/VendingApiContext;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 64
    iput-object p2, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    .line 65
    return-void
.end method


# virtual methods
.method public ackNotifications(Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 163
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 166
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    invoke-virtual {v7}, Lcom/google/android/vending/remoting/api/VendingRequest;->setAvoidBulkCancel()V

    .line 167
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 168
    return-void
.end method

.method public checkForPendingNotifications(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;>;"
    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;-><init>()V

    .line 146
    .local v2, "request":Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v4, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 150
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 151
    return-void
.end method

.method public checkLicense(Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "licenseRequest"    # Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 106
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 107
    return-void
.end method

.method public flagAsset(Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "assetId"    # Ljava/lang/String;
    .param p2, "flagType"    # I
    .param p3, "flagMessage"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;>;"
    const/4 v1, 0x1

    .line 80
    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;-><init>()V

    .line 81
    .local v2, "request":Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;
    iput-object p1, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->assetId:Ljava/lang/String;

    .line 82
    iput-boolean v1, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasAssetId:Z

    .line 83
    iput p2, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagType:I

    .line 84
    iput-boolean v1, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagType:Z

    .line 85
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iput-object p3, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->flagMessage:Ljava/lang/String;

    .line 87
    iput-boolean v1, v2, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;->hasFlagMessage:Z

    .line 89
    :cond_0
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 92
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 93
    return-void
.end method

.method public getApiContext()Lcom/google/android/vending/remoting/api/VendingApiContext;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    return-object v0
.end method

.method public getBillingCountries(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p2, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Response$Listener",
            "<[",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    new-instance v2, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataRequestProto;-><init>()V

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    new-instance v4, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;

    invoke-direct {v4, p1}, Lcom/google/android/vending/remoting/api/VendingApi$CountriesConverter;-><init>(Lcom/android/volley/Response$Listener;)V

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 238
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 239
    return-void
.end method

.method public getInAppPurchaseInformation(Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 121
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 122
    return-void
.end method

.method public recordBillingEvent(Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 221
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$BillingEventRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 224
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    invoke-virtual {v7}, Lcom/google/android/vending/remoting/api/VendingRequest;->setAvoidBulkCancel()V

    .line 225
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 226
    return-void
.end method

.method public restoreInAppTransactions(Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 136
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 137
    return-void
.end method

.method public syncContent(Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;Lcom/google/android/gms/ads/identifier/AdIdProvider;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
    .param p2, "adIdProvider"    # Lcom/google/android/gms/ads/identifier/AdIdProvider;
    .param p3, "accountOrdinal"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;",
            "Lcom/google/android/gms/ads/identifier/AdIdProvider;",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 181
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;>;"
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-class v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;

    const-class v3, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    iget-object v5, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-object v2, p1

    move-object v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/remoting/api/VendingRequest;->make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;

    move-result-object v7

    .line 185
    .local v7, "vendingRequest":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<**>;"
    if-eqz p2, :cond_2

    .line 186
    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->getPublicAndroidId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const-string v0, "X-Public-Android-Id"

    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->getPublicAndroidId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/vending/remoting/api/VendingRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    sget-boolean v0, Lcom/google/android/vending/remoting/api/VendingApi;->SEND_AD_ID_FOR_CONTENT_SYNC:Z

    if-eqz v0, :cond_2

    .line 192
    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->getAdId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    const-string v0, "X-Ad-Id"

    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->getAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/vending/remoting/api/VendingRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->isLimitAdTrackingEnabled()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 197
    const-string v0, "X-Limit-Ad-Tracking-Enabled"

    invoke-interface {p2}, Lcom/google/android/gms/ads/identifier/AdIdProvider;->isLimitAdTrackingEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/vending/remoting/api/VendingRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 204
    const-string v0, "X-Account-Ordinal"

    invoke-virtual {v7, v0, p3}, Lcom/google/android/vending/remoting/api/VendingRequest;->addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_3
    invoke-virtual {v7}, Lcom/google/android/vending/remoting/api/VendingRequest;->setAvoidBulkCancel()V

    .line 207
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingApi;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, v7}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 208
    return-void
.end method
