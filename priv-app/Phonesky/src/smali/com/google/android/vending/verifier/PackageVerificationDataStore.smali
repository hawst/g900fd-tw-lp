.class public Lcom/google/android/vending/verifier/PackageVerificationDataStore;
.super Ljava/lang/Object;
.source "PackageVerificationDataStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;
    }
.end annotation


# static fields
.field private static final FULL_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private final mHelper:Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "cache_fingerprint"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sha256_digest"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "length"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "forward_locked"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suppress_user_warning"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;-><init>(Lcom/google/android/vending/verifier/PackageVerificationDataStore;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mHelper:Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;

    .line 91
    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 101
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 102
    return-void
.end method

.method private packageVerificationDataFromCursor(Landroid/database/Cursor;)Lcom/google/android/vending/verifier/PackageVerificationData;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x1

    .line 190
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "packageName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 192
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    .line 194
    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 195
    .local v2, "fingerprint":J
    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 196
    .local v4, "sha256Digest":[B
    const/4 v10, 0x3

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 197
    .local v5, "length":J
    const/4 v10, 0x4

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-ne v10, v0, :cond_1

    move v7, v0

    .line 198
    .local v7, "forwardLocked":Z
    :goto_1
    const/4 v10, 0x5

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-ne v10, v0, :cond_2

    move v8, v0

    .line 199
    .local v8, "suppressUserWarning":Z
    :goto_2
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationData;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/vending/verifier/PackageVerificationData;-><init>(Ljava/lang/String;J[BJZZ)V

    goto :goto_0

    .end local v7    # "forwardLocked":Z
    .end local v8    # "suppressUserWarning":Z
    :cond_1
    move v7, v9

    .line 197
    goto :goto_1

    .restart local v7    # "forwardLocked":Z
    :cond_2
    move v8, v9

    .line 198
    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized get(Ljava/lang/String;)Lcom/google/android/vending/verifier/PackageVerificationData;
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 110
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 111
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "verification_cache"

    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->FULL_PROJECTION:[Ljava/lang/String;

    const-string v3, "package_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 114
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eq v0, v10, :cond_0

    .line 121
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 122
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v9

    :goto_0
    monitor-exit p0

    return-object v0

    .line 117
    :cond_0
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    .line 118
    invoke-direct {p0, v8}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->packageVerificationDataFromCursor(Landroid/database/Cursor;)Lcom/google/android/vending/verifier/PackageVerificationData;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 121
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 122
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 110
    .end local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 121
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 122
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized getAll()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/vending/verifier/PackageVerificationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 132
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "verification_cache"

    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->FULL_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 134
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 135
    .local v10, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-direct {p0, v8}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->packageVerificationDataFromCursor(Landroid/database/Cursor;)Lcom/google/android/vending/verifier/PackageVerificationData;

    move-result-object v9

    .line 138
    .local v9, "packageVerificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    iget-object v0, v9, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-interface {v10, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "packageVerificationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    .end local v10    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    :cond_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 141
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    monitor-exit p0

    return-object v10
.end method

.method public declared-synchronized put(Lcom/google/android/vending/verifier/PackageVerificationData;)V
    .locals 6
    .param p1, "packageVerificationData"    # Lcom/google/android/vending/verifier/PackageVerificationData;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 151
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 152
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 153
    .local v0, "values":Landroid/content/ContentValues;
    const-string v3, "package_name"

    iget-object v4, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v3, "cache_fingerprint"

    iget-wide v4, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mCacheFingerprint:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 155
    const-string v3, "sha256_digest"

    iget-object v4, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mSha256Digest:[B

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 156
    const-string v3, "length"

    iget-wide v4, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mApkLength:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 157
    const-string v4, "forward_locked"

    iget-boolean v3, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mForwardLocked:Z

    if-eqz v3, :cond_0

    move v3, v1

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    const-string v3, "suppress_user_warning"

    iget-boolean v4, p1, Lcom/google/android/vending/verifier/PackageVerificationData;->mSuppressUserWarning:Z

    if-eqz v4, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "verification_cache"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 161
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    monitor-exit p0

    return-void

    :cond_0
    move v3, v2

    .line 157
    goto :goto_0

    :cond_1
    move v1, v2

    .line 158
    goto :goto_1

    .line 151
    .end local v0    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized remove(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 170
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "verification_cache"

    const-string v2, "package_name=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 171
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method reopen()V
    .locals 1

    .prologue
    .line 95
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 96
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mHelper:Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;

    invoke-virtual {v0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 97
    return-void
.end method

.method public declared-synchronized reset()V
    .locals 4

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 209
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "verification_cache"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 210
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSuppressUserWarning(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "suppressUserWarning"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 181
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->reopen()V

    .line 182
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 183
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v3, "suppress_user_warning"

    if-eqz p2, :cond_0

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "verification_cache"

    const-string v3, "package_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 186
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationDataStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    monitor-exit p0

    return-void

    :cond_0
    move v1, v2

    .line 183
    goto :goto_0

    .line 181
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
