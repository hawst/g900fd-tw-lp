.class public final Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CsdClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/protos/CsdClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientDownloadRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;,
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;,
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;,
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;,
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;,
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;


# instance fields
.field public androidId:J

.field public apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

.field public clientAsn:[Ljava/lang/String;

.field public digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

.field public downloadType:I

.field public fileBasename:Ljava/lang/String;

.field public hasAndroidId:Z

.field public hasDownloadType:Z

.field public hasFileBasename:Z

.field public hasLength:Z

.field public hasLocale:Z

.field public hasRequestId:Z

.field public hasUrl:Z

.field public hasUserInitiated:Z

.field public length:J

.field public locale:Ljava/lang/String;

.field public originatingPackages:[Ljava/lang/String;

.field public originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

.field public requestId:[B

.field public resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

.field public signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

.field public url:Ljava/lang/String;

.field public userInitiated:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1138
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1139
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    .line 1140
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    .locals 2

    .prologue
    .line 1074
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    if-nez v0, :cond_1

    .line 1075
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1077
    :try_start_0
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    if-nez v0, :cond_0

    .line 1078
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    sput-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    .line 1080
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1082
    :cond_1
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    return-object v0

    .line 1080
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1143
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    .line 1144
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    .line 1145
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    .line 1146
    iput-wide v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    .line 1147
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLength:Z

    .line 1148
    invoke-static {}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->emptyArray()[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    .line 1149
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 1150
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    .line 1151
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUserInitiated:Z

    .line 1152
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    .line 1153
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    .line 1154
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasFileBasename:Z

    .line 1155
    iput v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    .line 1156
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasDownloadType:Z

    .line 1157
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    .line 1158
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLocale:Z

    .line 1159
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    .line 1160
    iput-wide v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    .line 1161
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasAndroidId:Z

    .line 1162
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    .line 1163
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    .line 1164
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasRequestId:Z

    .line 1165
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 1166
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->cachedSize:I

    .line 1167
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 1238
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1239
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1240
    :cond_0
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1243
    :cond_1
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    if-eqz v5, :cond_2

    .line 1244
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1247
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLength:Z

    if-nez v5, :cond_3

    iget-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    .line 1248
    :cond_3
    const/4 v5, 0x3

    iget-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1251
    :cond_4
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 1252
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    array-length v5, v5

    if-ge v3, v5, :cond_6

    .line 1253
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    aget-object v2, v5, v3

    .line 1254
    .local v2, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    if-eqz v2, :cond_5

    .line 1255
    const/4 v5, 0x4

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1252
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1260
    .end local v2    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    .end local v3    # "i":I
    :cond_6
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-eqz v5, :cond_7

    .line 1261
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1264
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUserInitiated:Z

    if-nez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    if-eqz v5, :cond_9

    .line 1265
    :cond_8
    const/4 v5, 0x6

    iget-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1268
    :cond_9
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_c

    .line 1269
    const/4 v0, 0x0

    .line 1270
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1271
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_b

    .line 1272
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1273
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_a

    .line 1274
    add-int/lit8 v0, v0, 0x1

    .line 1275
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1271
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1279
    .end local v2    # "element":Ljava/lang/String;
    :cond_b
    add-int/2addr v4, v1

    .line 1280
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1282
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_c
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasFileBasename:Z

    if-nez v5, :cond_d

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1283
    :cond_d
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1286
    :cond_e
    iget v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    if-nez v5, :cond_f

    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasDownloadType:Z

    if-eqz v5, :cond_10

    .line 1287
    :cond_f
    const/16 v5, 0xa

    iget v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1290
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLocale:Z

    if-nez v5, :cond_11

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 1291
    :cond_11
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1294
    :cond_12
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    if-eqz v5, :cond_13

    .line 1295
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1298
    :cond_13
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasAndroidId:Z

    if-nez v5, :cond_14

    iget-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_15

    .line 1299
    :cond_14
    const/16 v5, 0xd

    iget-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1302
    :cond_15
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    if-eqz v5, :cond_18

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_18

    .line 1303
    const/4 v0, 0x0

    .line 1304
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 1305
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 1306
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1307
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_16

    .line 1308
    add-int/lit8 v0, v0, 0x1

    .line 1309
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1305
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1313
    .end local v2    # "element":Ljava/lang/String;
    :cond_17
    add-int/2addr v4, v1

    .line 1314
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1316
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_18
    iget-boolean v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasRequestId:Z

    if-nez v5, :cond_19

    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    sget-object v6, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 1317
    :cond_19
    const/16 v5, 0x10

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v5

    add-int/2addr v4, v5

    .line 1320
    :cond_1a
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-eqz v5, :cond_1b

    .line 1321
    const/16 v5, 0x11

    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1324
    :cond_1b
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1332
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1333
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1337
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1338
    :sswitch_0
    return-object p0

    .line 1343
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    .line 1344
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    goto :goto_0

    .line 1348
    :sswitch_2
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    if-nez v6, :cond_1

    .line 1349
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;-><init>()V

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    .line 1351
    :cond_1
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1355
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    .line 1356
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLength:Z

    goto :goto_0

    .line 1360
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1362
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    if-nez v6, :cond_3

    move v1, v5

    .line 1363
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    .line 1365
    .local v2, "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    if-eqz v1, :cond_2

    .line 1366
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1368
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 1369
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;-><init>()V

    aput-object v6, v2, v1

    .line 1370
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1371
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1368
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1362
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    :cond_3
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    array-length v1, v6

    goto :goto_1

    .line 1374
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    :cond_4
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;-><init>()V

    aput-object v6, v2, v1

    .line 1375
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1376
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    goto :goto_0

    .line 1380
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    :sswitch_5
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-nez v6, :cond_5

    .line 1381
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 1383
    :cond_5
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1387
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    .line 1388
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUserInitiated:Z

    goto/16 :goto_0

    .line 1392
    :sswitch_7
    const/16 v6, 0x42

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1394
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    if-nez v6, :cond_7

    move v1, v5

    .line 1395
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1396
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 1397
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1399
    :cond_6
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_8

    .line 1400
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1401
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1399
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1394
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_7
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_3

    .line 1404
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1405
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1409
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    .line 1410
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasFileBasename:Z

    goto/16 :goto_0

    .line 1414
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1415
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1418
    :pswitch_1
    iput v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    .line 1419
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasDownloadType:Z

    goto/16 :goto_0

    .line 1425
    .end local v4    # "value":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    .line 1426
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLocale:Z

    goto/16 :goto_0

    .line 1430
    :sswitch_b
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    if-nez v6, :cond_9

    .line 1431
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    .line 1433
    :cond_9
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1437
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    .line 1438
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasAndroidId:Z

    goto/16 :goto_0

    .line 1442
    :sswitch_d
    const/16 v6, 0x7a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1444
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    if-nez v6, :cond_b

    move v1, v5

    .line 1445
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1446
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_a

    .line 1447
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1449
    :cond_a
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 1450
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1451
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1449
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1444
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_b
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_5

    .line 1454
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1455
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1459
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    .line 1460
    iput-boolean v8, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasRequestId:Z

    goto/16 :goto_0

    .line 1464
    :sswitch_f
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-nez v6, :cond_d

    .line 1465
    new-instance v6, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-direct {v6}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 1467
    :cond_d
    iget-object v6, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1333
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x69 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
    .end sparse-switch

    .line 1415
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 1173
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1174
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1176
    :cond_1
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    if-eqz v2, :cond_2

    .line 1177
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1179
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLength:Z

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_4

    .line 1180
    :cond_3
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1182
    :cond_4
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1183
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1184
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    aget-object v0, v2, v1

    .line 1185
    .local v0, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    if-eqz v0, :cond_5

    .line 1186
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1183
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1190
    .end local v0    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-eqz v2, :cond_7

    .line 1191
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1193
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUserInitiated:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    if-eqz v2, :cond_9

    .line 1194
    :cond_8
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->userInitiated:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1196
    :cond_9
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 1197
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 1198
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->clientAsn:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1199
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 1200
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1197
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1204
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasFileBasename:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1205
    :cond_c
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->fileBasename:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1207
    :cond_d
    iget v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    if-nez v2, :cond_e

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasDownloadType:Z

    if-eqz v2, :cond_f

    .line 1208
    :cond_e
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1210
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLocale:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 1211
    :cond_10
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1213
    :cond_11
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    if-eqz v2, :cond_12

    .line 1214
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1216
    :cond_12
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasAndroidId:Z

    if-nez v2, :cond_13

    iget-wide v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_14

    .line 1217
    :cond_13
    const/16 v2, 0xd

    iget-wide v4, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 1219
    :cond_14
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_16

    .line 1220
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_16

    .line 1221
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1222
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_15

    .line 1223
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1220
    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1227
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_16
    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasRequestId:Z

    if-nez v2, :cond_17

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_18

    .line 1228
    :cond_17
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1230
    :cond_18
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    if-eqz v2, :cond_19

    .line 1231
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1233
    :cond_19
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1234
    return-void
.end method
