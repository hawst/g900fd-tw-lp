.class public abstract Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;
.super Lcom/google/android/vending/verifier/api/BaseVerificationRequest;
.source "BaseVerificationRequestWithResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/google/android/vending/verifier/api/BaseVerificationRequest",
        "<TT;TU;>;"
    }
.end annotation


# instance fields
.field private final mListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/protobuf/nano/MessageNano;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            "TU;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult<TT;TU;>;"
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    .local p4, "request":Lcom/google/protobuf/nano/MessageNano;, "TU;"
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/vending/verifier/api/BaseVerificationRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;Lcom/google/protobuf/nano/MessageNano;)V

    .line 18
    iput-object p2, p0, Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;->mListener:Lcom/android/volley/Response$Listener;

    .line 19
    return-void
.end method


# virtual methods
.method protected deliverResponse(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;, "Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult<TT;TU;>;"
    .local p1, "response":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;->mListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v0, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 24
    return-void
.end method
