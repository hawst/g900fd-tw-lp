.class public Lcom/google/android/vending/verifier/PackageVerificationService;
.super Landroid/app/Service;
.source "PackageVerificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;,
        Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;,
        Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;,
        Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;


# instance fields
.field private mLastStartId:I

.field private mRemovalRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVerifications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;",
            ">;"
        }
    .end annotation
.end field

.field private mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    .line 137
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    .line 142
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mRemovalRequests:Ljava/util/ArrayList;

    .line 567
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageInfo(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100([Landroid/content/pm/Signature;)[[B
    .locals 1
    .param p0, "x0"    # [Landroid/content/pm/Signature;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getRawSignatures([Landroid/content/pm/Signature;)[[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/vending/verifier/PackageVerificationService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->cleanupVerifyInstalledPackages(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/vending/verifier/PackageVerificationService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mRemovalRequests:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/vending/verifier/PackageVerificationService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopServiceIfIdle()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendUploadApkIntent(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/vending/verifier/PackageVerificationService;Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    .param p3, "x3"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportResult(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopApplication(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/vending/verifier/PackageVerificationService;[[B[[B)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # [[B
    .param p2, "x2"    # [[B

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/vending/verifier/PackageVerificationService;->rawSignaturesMatch([[B[[B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->resolveHosts(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerificationRequest(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/vending/verifier/PackageVerificationService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/vending/verifier/PackageVerificationService;->extendTimeout(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/vending/verifier/PackageVerificationService;Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportAndCleanup(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    return-void
.end method

.method static synthetic access$700(Ljava/io/File;)[B
    .locals 1
    .param p0, "x0"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getSha256Hash(Ljava/io/File;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800([Landroid/content/pm/Signature;)[[B
    .locals 1
    .param p0, "x0"    # [Landroid/content/pm/Signature;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getCertificateFingerprints([Landroid/content/pm/Signature;)[[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/vending/verifier/PackageVerificationService;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerifyAllAppsRequest(Ljava/util/Map;)V

    return-void
.end method

.method private cancelDialog(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 740
    iget-object v0, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mDialog:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 741
    iget-object v0, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mDialog:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 742
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mDialog:Landroid/app/Activity;

    .line 744
    :cond_0
    return-void
.end method

.method private cancelVerificationIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "broadcast"    # Landroid/content/Intent;

    .prologue
    .line 253
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 254
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "android.content.pm.extra.VERIFICATION_ID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 255
    .local v1, "id":I
    invoke-direct {p0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->findVerification(I)Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    move-result-object v2

    .line 256
    .local v2, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 257
    const-string v3, "Cancel active verification id=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    invoke-direct {p0, v2}, Lcom/google/android/vending/verifier/PackageVerificationService;->cancelDialog(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    .line 259
    iget-object v3, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 261
    :cond_0
    return-void
.end method

.method private cleanupVerifyInstalledPackages(Z)V
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 645
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 646
    if-eqz p1, :cond_0

    .line 647
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->verifyInstalledPackagesLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 649
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    .line 650
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopServiceIfIdle()V

    .line 651
    return-void
.end method

.method private destroyAllVerifications()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 721
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 722
    :goto_0
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 723
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .line 724
    .local v0, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    const-string v1, "Destroying orphaned verification id=%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 726
    invoke-direct {p0, p0, v0, v4}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportResult(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;I)V

    .line 727
    invoke-direct {p0, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->cancelDialog(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    goto :goto_0

    .line 729
    .end local v0    # "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    :cond_0
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    if-eqz v1, :cond_1

    .line 730
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    invoke-virtual {v1, v4}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;->cancel(Z)Z

    .line 731
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    .line 733
    :cond_1
    return-void
.end method

.method private extendTimeout(II)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "code"    # I

    .prologue
    .line 713
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareDialogDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, p1, p2, v2, v3}, Landroid/content/pm/PackageManager;->extendVerificationTimeout(IIJ)V

    .line 715
    return-void
.end method

.method private findVerification(I)Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 750
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .line 751
    .local v1, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    iget v2, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    if-ne v2, p1, :cond_0

    .line 755
    .end local v1    # "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getCertificateFingerprints([Landroid/content/pm/Signature;)[[B
    .locals 5
    .param p0, "signatures"    # [Landroid/content/pm/Signature;

    .prologue
    .line 1032
    :try_start_0
    const-string v4, "SHA1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1037
    .local v1, "digester":Ljava/security/MessageDigest;
    if-eqz p0, :cond_0

    array-length v4, p0

    if-lez v4, :cond_0

    .line 1038
    array-length v4, p0

    new-array v0, v4, [[B

    .line 1039
    .local v0, "certFingerprints":[[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, p0

    if-ge v3, v4, :cond_1

    .line 1040
    aget-object v4, p0, v3

    invoke-virtual {v4}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    aput-object v4, v0, v3

    .line 1039
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1033
    .end local v0    # "certFingerprints":[[B
    .end local v1    # "digester":Ljava/security/MessageDigest;
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 1034
    .local v2, "ex":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1046
    .end local v2    # "ex":Ljava/security/NoSuchAlgorithmException;
    .restart local v1    # "digester":Ljava/security/MessageDigest;
    :cond_0
    const/4 v4, 0x0

    check-cast v4, [[B

    move-object v0, v4

    :cond_1
    return-object v0
.end method

.method private getPackageInfo(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)Z
    .locals 14
    .param p1, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    const/4 v13, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 888
    iget-object v0, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->dataUri:Landroid/net/Uri;

    .line 889
    .local v0, "dataUri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 890
    const-string v10, "Null data for request id=%d"

    new-array v9, v9, [Ljava/lang/Object;

    iget v11, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v8

    invoke-static {v10, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 973
    :goto_0
    return v8

    .line 893
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    .line 894
    .local v7, "scheme":Ljava/lang/String;
    const-string v10, "file"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 895
    const-string v10, "Unsupported scheme for %s in request id=%d"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    iget v12, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 898
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 899
    .local v6, "path":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 900
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 901
    const-string v10, "Cannot find file for %s in request id=%d"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    iget v12, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 904
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v10

    if-nez v10, :cond_3

    .line 905
    const-string v10, "Cannot read file for %s in request id=%d"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    iget v12, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 908
    :cond_3
    const/4 v4, 0x0

    .line 911
    .local v4, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/16 v11, 0x40

    invoke-virtual {v10, v6, v11}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 916
    if-nez v4, :cond_4

    .line 917
    const-string v10, "Cannot read archive for %s in request id=%d"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    iget v12, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 912
    :catch_0
    move-exception v1

    .line 913
    .local v1, "e":Ljava/lang/Exception;
    const-string v10, "Exception reading %s in request id=%d %s"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v0, v11, v8

    iget v12, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v9

    aput-object v1, v11, v13

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 923
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v10, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mPackageName:Ljava/lang/String;

    .line 924
    iget v10, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mVersion:Ljava/lang/Integer;

    .line 926
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    iput-wide v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mLength:J

    .line 929
    :try_start_1
    invoke-static {v3}, Lcom/google/android/vending/verifier/PackageVerificationService;->getSha256Hash(Ljava/io/File;)[B

    move-result-object v10

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSha256:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    .line 938
    iget-object v10, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v10, :cond_6

    .line 939
    iget-object v10, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    invoke-static {v10}, Lcom/google/android/vending/verifier/PackageVerificationService;->getRawSignatures([Landroid/content/pm/Signature;)[[B

    move-result-object v10

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSignatures:[[B

    .line 948
    :goto_1
    :try_start_2
    invoke-static {v3}, Lcom/google/android/vending/verifier/ZipAnalyzer;->analyzeZipFile(Ljava/io/File;)[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;

    move-result-object v10

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mFileInfos:[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_5

    .line 967
    :goto_2
    iget-object v8, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v6, v8, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 968
    iget-object v8, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 969
    .local v5, "label":Ljava/lang/CharSequence;
    if-eqz v5, :cond_5

    .line 970
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mLabel:Ljava/lang/String;

    :cond_5
    move v8, v9

    .line 973
    goto/16 :goto_0

    .line 930
    .end local v5    # "label":Ljava/lang/CharSequence;
    :catch_1
    move-exception v2

    .line 931
    .local v2, "ex":Ljava/io/IOException;
    const-string v10, "Error while calculating sha256 for file=%s, error=%s"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    aput-object v2, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 934
    .end local v2    # "ex":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 935
    .local v2, "ex":Ljava/security/NoSuchAlgorithmException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 943
    .end local v2    # "ex":Ljava/security/NoSuchAlgorithmException;
    :cond_6
    invoke-static {v6}, Lcom/google/android/vending/verifier/CertificateUtils;->collectCertificates(Ljava/lang/String;)[[B

    move-result-object v10

    iput-object v10, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSignatures:[[B

    goto :goto_1

    .line 949
    :catch_3
    move-exception v2

    .line 952
    .local v2, "ex":Ljava/io/IOException;
    const-string v10, "Error while getting information about apk contents for file=%s, error=%s"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    aput-object v2, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 954
    .end local v2    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 958
    .local v2, "ex":Ljava/lang/RuntimeException;
    const-string v10, "Error while getting information about apk contents for file=%s, error=%s"

    new-array v11, v13, [Ljava/lang/Object;

    aput-object v0, v11, v8

    aput-object v2, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 960
    .end local v2    # "ex":Ljava/lang/RuntimeException;
    :catch_5
    move-exception v2

    .line 961
    .local v2, "ex":Ljava/security/NoSuchAlgorithmException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8
.end method

.method private static getRawSignatures([Landroid/content/pm/Signature;)[[B
    .locals 3
    .param p0, "signatures"    # [Landroid/content/pm/Signature;

    .prologue
    .line 1004
    if-eqz p0, :cond_0

    array-length v2, p0

    if-lez v2, :cond_0

    .line 1005
    array-length v2, p0

    new-array v1, v2, [[B

    .line 1006
    .local v1, "rawSignatures":[[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 1007
    aget-object v2, p0, v0

    invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v2

    aput-object v2, v1, v0

    .line 1006
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1011
    .end local v0    # "i":I
    .end local v1    # "rawSignatures":[[B
    :cond_0
    const/4 v2, 0x0

    check-cast v2, [[B

    move-object v1, v2

    :cond_1
    return-object v1
.end method

.method private static getSha256Hash(Ljava/io/File;)[B
    .locals 2
    .param p0, "apkFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 980
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 982
    .local v0, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getSha256Hash(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 984
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method static getSha256Hash(Ljava/io/InputStream;)[B
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 993
    const-string v3, "SHA256"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 996
    .local v2, "digester":Ljava/security/MessageDigest;
    const/16 v3, 0x4000

    new-array v0, v3, [B

    .line 997
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 998
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 1000
    :cond_0
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    return-object v3
.end method

.method private handleRemovalRequestResponse(Landroid/content/Intent;)V
    .locals 7
    .param p1, "broadcast"    # Landroid/content/Intent;

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 220
    .local v6, "extras":Landroid/os/Bundle;
    const-string v1, "android.content.pm.extra.VERIFICATION_PACKAGE_NAME"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 222
    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mRemovalRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;

    const-string v1, "com.google.android.vending.verifier.intent.extra.UNINSTALL"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v1, "com.google.android.vending.verifier.intent.extra.DONT_WARN"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v1, "com.google.android.vending.verifier.intent.extra.RESPONSE_TOKEN"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/lang/String;ZZ[B)V

    .line 226
    .local v0, "removalRequestTask":Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 228
    .end local v0    # "removalRequestTask":Lcom/google/android/vending/verifier/PackageVerificationService$CleanupRemovalRequestTask;
    :cond_0
    return-void
.end method

.method private handleVerificationIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "broadcast"    # Landroid/content/Intent;

    .prologue
    .line 196
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    invoke-direct {v0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;-><init>(Landroid/content/Intent;)V

    .line 197
    .local v0, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v1, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;

    invoke-direct {v1, p0, v0}, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    .line 199
    .local v1, "task":Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 200
    return-void
.end method

.method public static migrateAntiMalwareConsent(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 851
    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_1

    .line 879
    :cond_0
    :goto_0
    return-void

    .line 855
    :cond_1
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x13

    if-lt v7, v9, :cond_3

    .line 856
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 857
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const-string v7, "package_verifier_user_consent"

    invoke-static {v2, v7, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v8, :cond_2

    move v1, v8

    .line 859
    .local v1, "consentAlreadyMigratedToSettings":Z
    :cond_2
    if-nez v1, :cond_0

    .line 862
    const-string v7, "package_verifier_user_consent"

    invoke-static {v2, v7, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 863
    new-instance v6, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    const-string v8, "shared_prefs"

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    .local v6, "sharedPreferencesDir":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v7, "package_verifer_public_preferences.xml"

    invoke-direct {v4, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 866
    .local v4, "oldConsentFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 869
    .end local v1    # "consentAlreadyMigratedToSettings":Z
    .end local v2    # "contentResolver":Landroid/content/ContentResolver;
    .end local v4    # "oldConsentFile":Ljava/io/File;
    .end local v6    # "sharedPreferencesDir":Ljava/io/File;
    :cond_3
    const-string v7, "package_verifer_public_preferences"

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 871
    .local v5, "preferences":Landroid/content/SharedPreferences;
    const-string v7, "accepted-anti-malware-consent"

    invoke-interface {v5, v7, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 872
    .local v0, "consentAlreadyMigratedToFile":Z
    if-nez v0, :cond_0

    .line 875
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 876
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "accepted-anti-malware-consent"

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 877
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private rawSignaturesMatch([[B[[B)Z
    .locals 7
    .param p1, "signature1"    # [[B
    .param p2, "signature2"    # [[B

    .prologue
    .line 1015
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1016
    :cond_0
    const/4 v6, 0x0

    .line 1026
    :goto_0
    return v6

    .line 1018
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v3

    .line 1019
    .local v3, "set1":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    move-object v0, p1

    .local v0, "arr$":[[B
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v5, v0, v1

    .line 1020
    .local v5, "sig":[B
    new-instance v6, Landroid/content/pm/Signature;

    invoke-direct {v6, v5}, Landroid/content/pm/Signature;-><init>([B)V

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1019
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1022
    .end local v5    # "sig":[B
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 1023
    .local v4, "set2":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/content/pm/Signature;>;"
    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_3

    aget-object v5, v0, v1

    .line 1024
    .restart local v5    # "sig":[B
    new-instance v6, Landroid/content/pm/Signature;

    invoke-direct {v6, v5}, Landroid/content/pm/Signature;-><init>([B)V

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1023
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1026
    .end local v5    # "sig":[B
    :cond_3
    invoke-virtual {v3, v4}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_0
.end method

.method public static registerDialog(ILandroid/app/Activity;)Z
    .locals 3
    .param p0, "id"    # I
    .param p1, "a"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 764
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 765
    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    if-nez v2, :cond_1

    .line 773
    :cond_0
    :goto_0
    return v1

    .line 768
    :cond_1
    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v2, p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->findVerification(I)Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    move-result-object v0

    .line 769
    .local v0, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    if-eqz v0, :cond_0

    .line 770
    iput-object p1, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mDialog:Landroid/app/Activity;

    .line 771
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private reportAndCleanup(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 657
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 659
    iget v0, p2, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportResult(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;I)V

    .line 661
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 662
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopServiceIfIdle()V

    .line 663
    return-void
.end method

.method public static reportConsentDialog(IZ)V
    .locals 6
    .param p0, "id"    # I
    .param p1, "granted"    # Z

    .prologue
    const/4 v5, 0x1

    .line 814
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 815
    const-string v1, "User consent %b for id=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 816
    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    if-nez v1, :cond_1

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v1, p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->findVerification(I)Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    move-result-object v0

    .line 820
    .local v0, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    if-eqz v0, :cond_0

    .line 821
    if-eqz p1, :cond_2

    .line 822
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 823
    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-static {v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->migrateAntiMalwareConsent(Landroid/content/Context;)V

    .line 824
    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v1, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerificationRequest(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    goto :goto_0

    .line 826
    :cond_2
    iput v5, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    .line 827
    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v1, v2, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportAndCleanup(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    goto :goto_0
.end method

.method private reportResult(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    .param p3, "result"    # I

    .prologue
    .line 681
    iget-boolean v1, p2, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->fromVerificationActivity:Z

    if-eqz v1, :cond_1

    .line 682
    const/4 v1, 0x1

    if-ne p3, v1, :cond_0

    .line 683
    iget-object v0, p2, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originalIntent:Landroid/content/Intent;

    .line 684
    .local v0, "installIntent":Landroid/content/Intent;
    const-string v1, "com.android.packageinstaller"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.packageinstaller"

    const-string v3, "com.android.packageinstaller.PackageInstallerActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 688
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 689
    invoke-virtual {p0, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->startActivity(Landroid/content/Intent;)V

    .line 694
    .end local v0    # "installIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    iget v1, p2, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportVerificationResult(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method public static reportUserChoice(II)V
    .locals 6
    .param p0, "id"    # I
    .param p1, "result"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 781
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 782
    const-string v3, "User selected %d for id=%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 783
    sget-object v3, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    if-nez v3, :cond_1

    .line 808
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    sget-object v3, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v3, p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->findVerification(I)Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    move-result-object v1

    .line 787
    .local v1, "state":Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;
    if-eqz v1, :cond_0

    .line 791
    iget v3, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 792
    if-ne p1, v0, :cond_2

    move v0, v2

    .line 796
    .local v0, "decision":I
    :cond_2
    iget-object v3, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mToken:[B

    new-instance v4, Lcom/google/android/vending/verifier/PackageVerificationService$1;

    invoke-direct {v4, v1}, Lcom/google/android/vending/verifier/PackageVerificationService$1;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->reportUserDecision(IZ[BLcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 805
    .end local v0    # "decision":I
    :cond_3
    iput p1, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    .line 806
    sget-object v2, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    sget-object v3, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v2, v3, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportAndCleanup(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    goto :goto_0
.end method

.method private reportVerificationResult(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "id"    # I
    .param p3, "result"    # I

    .prologue
    .line 703
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/content/pm/PackageManager;->verifyPendingInstall(II)V

    .line 704
    return-void
.end method

.method private static resolveHosts(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 6
    .param p0, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 623
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 624
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 626
    .local v1, "host":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingIp:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 631
    .end local v1    # "host":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->referrerUri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 632
    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->referrerUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 634
    .restart local v1    # "host":Ljava/lang/String;
    :try_start_1
    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->referrerIp:Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .line 639
    .end local v1    # "host":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 627
    .restart local v1    # "host":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 628
    .local v0, "ex":Ljava/net/UnknownHostException;
    const-string v2, "Could not resolve host %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 635
    .end local v0    # "ex":Ljava/net/UnknownHostException;
    :catch_1
    move-exception v0

    .line 636
    .restart local v0    # "ex":Ljava/net/UnknownHostException;
    const-string v2, "Could not resolve host %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static sendRemovalResponse(Landroid/content/Context;Ljava/lang/String;ZZ[B)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uninstall"    # Z
    .param p3, "dontWarn"    # Z
    .param p4, "responseToken"    # [B

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.vending.verifier.intent.action.REMOVAL_REQUEST_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 241
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.content.pm.extra.VERIFICATION_PACKAGE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v1, "com.google.android.vending.verifier.intent.extra.UNINSTALL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 243
    const-string v1, "com.google.android.vending.verifier.intent.extra.DONT_WARN"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 244
    const-string v1, "com.google.android.vending.verifier.intent.extra.RESPONSE_TOKEN"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 245
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 246
    return-void
.end method

.method private sendUploadApkIntent(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 7
    .param p1, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    const/4 v6, 0x0

    .line 1139
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.security.verifyapps.UPLOAD_APK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1140
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "package_name"

    iget-object v3, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1141
    const-string v2, "digest"

    iget-object v3, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSha256:[B

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1142
    const-string v2, "version_code"

    iget-object v3, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mVersion:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1143
    const-string v2, "length"

    iget-wide v4, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mLength:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1144
    const-string v2, "token"

    iget-object v3, p1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mToken:[B

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1145
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.gms"

    const-string v4, "com.google.android.gms.security.verifier.ApkUploadService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1147
    invoke-virtual {p0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1155
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1148
    :catch_0
    move-exception v0

    .line 1151
    .local v0, "ex":Ljava/lang/SecurityException;
    const-string v2, "Could not access ApkUploadService"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1152
    .end local v0    # "ex":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 1153
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v2, "Error occured while sending UPLOAD_APK intent"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private sendVerificationRequest(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 21
    .param p1, "state"    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    .prologue
    .line 1082
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1083
    .local v16, "currentLocale":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    .line 1085
    .local v17, "androidId":J
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSha256:[B

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mLength:J

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mPackageName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mVersion:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mSignatures:[[B

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mFileInfos:[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingUri:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->referrerUri:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingIp:Ljava/net/InetAddress;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->referrerIp:Ljava/net/InetAddress;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingPackageNames:[Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingSignatures:[[B

    new-instance v19, Lcom/google/android/vending/verifier/PackageVerificationService$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/vending/verifier/PackageVerificationService$2;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    new-instance v20, Lcom/google/android/vending/verifier/PackageVerificationService$3;

    invoke-direct/range {v20 .. v22}, Lcom/google/android/vending/verifier/PackageVerificationService$3;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    invoke-static/range {v3 .. v20}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->verifyApp([BJLjava/lang/String;Ljava/lang/Integer;[[B[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;[[BLjava/lang/String;JLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1135
    return-void
.end method

.method private sendVerifyAllAppsRequest(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/vending/verifier/PackageVerificationData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1159
    .local p1, "installedPackagesData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1160
    .local v1, "currentLocale":Ljava/lang/String;
    sget-object v2, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1161
    .local v6, "androidId":J
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 1163
    .local v3, "userConsented":Z
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1167
    .local v0, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    new-instance v4, Lcom/google/android/vending/verifier/PackageVerificationService$4;

    invoke-direct {v4, p0, v0}, Lcom/google/android/vending/verifier/PackageVerificationService$4;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;Ljava/util/List;)V

    .line 1217
    .local v4, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<[Lcom/google/android/vending/verifier/api/PackageVerificationResult;>;"
    new-instance v5, Lcom/google/android/vending/verifier/PackageVerificationService$5;

    invoke-direct {v5, p0}, Lcom/google/android/vending/verifier/PackageVerificationService$5;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;)V

    .line 1225
    .local v5, "errorListener":Lcom/android/volley/Response$ErrorListener;
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->verifyInstalledApps(Ljava/util/Collection;Ljava/lang/String;Ljava/lang/Long;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1227
    return-void
.end method

.method public static start(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fromBroadcast"    # Landroid/content/Intent;

    .prologue
    .line 150
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "broadcast_intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 152
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 153
    return-void
.end method

.method private stopApplication(Ljava/lang/String;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 1057
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x14

    if-ge v3, v4, :cond_0

    .line 1077
    :goto_0
    return-void

    .line 1060
    :cond_0
    const-string v3, "Attempting to stop application: %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1061
    sget-object v3, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Lcom/google/android/vending/verifier/PackageVerificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1065
    .local v0, "activityManager":Landroid/app/ActivityManager;
    :try_start_0
    const-class v3, Landroid/app/ActivityManager;

    const-string v4, "forceStopPackage"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 1067
    .local v2, "forceStopPackage":Ljava/lang/reflect/Method;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 1068
    .end local v2    # "forceStopPackage":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 1069
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "Cannot stop applications on this platform"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1070
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 1071
    .local v1, "e":Ljava/lang/SecurityException;
    const-string v3, "Cannot stop application due to security exception"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1072
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v1

    .line 1073
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "Cannot stop application due to reflection access exception"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1074
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 1075
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "Cannot stop application due to reflection invocation exception"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private stopServiceIfIdle()V
    .locals 1

    .prologue
    .line 669
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 670
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifications:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mRemovalRequests:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    if-nez v0, :cond_0

    .line 672
    iget v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mLastStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopSelf(I)V

    .line 674
    :cond_0
    return-void
.end method

.method private verifyInstalledPackages()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    invoke-direct {v0, p0}, Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;-><init>(Lcom/google/android/vending/verifier/PackageVerificationService;)V

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    .line 208
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mVerifyInstalledPackagesTask:Lcom/google/android/vending/verifier/PackageVerificationService$VerifyInstalledPackagesTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    const-string v0, "Verify installed packages is already running."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getApplicationName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1243
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1244
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    invoke-virtual {v3, p1, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1246
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v0, :cond_1

    .line 1256
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-object v4

    .line 1249
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_1
    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1250
    .local v1, "appLabel":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 1253
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 1254
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "appLabel":Ljava/lang/CharSequence;
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 1256
    .local v2, "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public isAlreadyInstalled(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1234
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 1237
    :cond_0
    :goto_0
    return v1

    .line 1235
    :catch_0
    move-exception v0

    .line 1237
    .local v0, "nnfe":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 170
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 158
    sput-object p0, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    .line 159
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->destroyAllVerifications()V

    .line 164
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/vending/verifier/PackageVerificationService;->sInstance:Lcom/google/android/vending/verifier/PackageVerificationService;

    .line 165
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 166
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 176
    const-string v2, "broadcast_intent"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 177
    .local v1, "broadcast":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.INSTALL_PACKAGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 180
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->handleVerificationIntent(Landroid/content/Intent;)V

    .line 189
    :cond_1
    :goto_0
    iput p3, p0, Lcom/google/android/vending/verifier/PackageVerificationService;->mLastStartId:I

    .line 191
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->stopServiceIfIdle()V

    .line 192
    const/4 v2, 0x3

    return v2

    .line 181
    :cond_2
    const-string v2, "com.google.android.vending.verifier.intent.action.VERIFY_INSTALLED_PACKAGES"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 182
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->verifyInstalledPackages()V

    goto :goto_0

    .line 183
    :cond_3
    const-string v2, "com.google.android.vending.verifier.intent.action.REMOVAL_REQUEST_RESPONSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 184
    invoke-direct {p0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->handleRemovalRequestResponse(Landroid/content/Intent;)V

    goto :goto_0

    .line 185
    :cond_4
    const-string v2, "android.intent.action.PACKAGE_VERIFIED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 186
    invoke-direct {p0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->cancelVerificationIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method
