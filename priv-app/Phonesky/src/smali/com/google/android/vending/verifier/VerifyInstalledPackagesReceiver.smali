.class public Lcom/google/android/vending/verifier/VerifyInstalledPackagesReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VerifyInstalledPackagesReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private checkPrerequisites(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 66
    sget-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const-string v0, "Skipping verification because disabled"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 80
    :goto_0
    return v0

    .line 71
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/installer/InstallPolicies;->hasNetwork()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    const-string v0, "Skipping verification because network inactive"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 73
    goto :goto_0

    .line 76
    :cond_1
    invoke-static {p1}, Lcom/google/android/vending/verifier/VerifyInstalledPackagesReceiver;->isPackageVerifierEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 77
    const-string v0, "Skipping verification because verify apps is not enabled"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isPackageVerifierEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 90
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_0

    .line 93
    const-string v3, "package_verifier_enable"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 99
    .local v1, "value":I
    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return v2

    .line 96
    .end local v1    # "value":I
    :cond_0
    const-string v3, "package_verifier_enable"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .restart local v1    # "value":I
    goto :goto_0

    .line 99
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static verifyInstalledPackages(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.vending.verifier.intent.action.VERIFY_INSTALLED_PACKAGES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 58
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.google.android.vending.verifier.intent.action.VERIFY_INSTALLED_PACKAGES"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/VerifyInstalledPackagesReceiver;->checkPrerequisites(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const-string v1, "Verify installed apps requested"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    invoke-static {p1, p2}, Lcom/google/android/vending/verifier/PackageVerificationService;->start(Landroid/content/Context;Landroid/content/Intent;)V

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const-string v1, "com.google.android.vending.verifier.intent.action.REMOVAL_REQUEST_RESPONSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 44
    const-string v1, "Handle removal request responses requested"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-static {p1, p2}, Lcom/google/android/vending/verifier/PackageVerificationService;->start(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 47
    :cond_2
    const-string v1, "Unexpected action %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
