.class public Lcom/google/android/vending/verifier/api/PackageVerificationMultiRequest;
.super Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;
.source "PackageVerificationMultiRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult",
        "<[",
        "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
        "Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .param p4, "request"    # Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<[",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<[Lcom/google/android/vending/verifier/api/PackageVerificationResult;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/protobuf/nano/MessageNano;)V

    .line 29
    new-instance v1, Lcom/android/volley/DefaultRetryPolicy;

    sget-object v0, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v0, v2, v3}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    invoke-virtual {p0, v1}, Lcom/google/android/vending/verifier/api/PackageVerificationMultiRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 30
    return-void
.end method


# virtual methods
.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 12
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<[",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 39
    :try_start_0
    iget-object v8, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-static {v8}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->parseFrom([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;

    move-result-object v4

    .line 40
    .local v4, "multiResponse":Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    iget-object v6, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .local v6, "responses":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    iget-object v8, p0, Lcom/google/android/vending/verifier/api/PackageVerificationMultiRequest;->mRequest:Lcom/google/protobuf/nano/MessageNano;

    check-cast v8, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;

    iget-object v8, v8, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;->requests:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    array-length v8, v8

    new-array v7, v8, [Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    .line 52
    .local v7, "results":[Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    const/4 v3, 0x0

    .line 53
    .local v3, "invalidRequestIds":I
    const/4 v0, 0x0

    .line 55
    .local v0, "blankRequestIds":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, v6

    if-ge v2, v8, :cond_1

    .line 56
    aget-object v8, v6, v2

    iget-boolean v8, v8, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasRequestId:Z

    if-eqz v8, :cond_0

    .line 64
    :try_start_1
    new-instance v8, Ljava/lang/String;

    aget-object v9, v6, v2

    iget-object v9, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    const-string v10, "UTF-8"

    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v5

    .line 74
    .local v5, "requestIndex":I
    aget-object v8, v6, v2

    invoke-static {v8}, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->fromResponse(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;)Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    move-result-object v8

    aput-object v8, v7, v5

    .line 55
    .end local v5    # "requestIndex":I
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    .end local v0    # "blankRequestIds":I
    .end local v2    # "i":I
    .end local v3    # "invalidRequestIds":I
    .end local v4    # "multiResponse":Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    .end local v6    # "responses":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .end local v7    # "results":[Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    :catch_0
    move-exception v1

    .line 42
    .local v1, "ex":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v8, Lcom/android/volley/VolleyError;

    invoke-direct {v8, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v8}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v8

    .line 88
    .end local v1    # "ex":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :goto_2
    return-object v8

    .line 66
    .restart local v0    # "blankRequestIds":I
    .restart local v2    # "i":I
    .restart local v3    # "invalidRequestIds":I
    .restart local v4    # "multiResponse":Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    .restart local v6    # "responses":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .restart local v7    # "results":[Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    :catch_1
    move-exception v1

    .line 67
    .local v1, "ex":Ljava/lang/NumberFormatException;
    add-int/lit8 v3, v3, 0x1

    .line 68
    goto :goto_1

    .line 69
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v1

    .line 71
    .local v1, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 77
    .end local v1    # "ex":Ljava/io/UnsupportedEncodingException;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 81
    :cond_1
    if-lez v3, :cond_2

    .line 82
    const-string v8, "Got %d responses with an invalid request id"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :cond_2
    if-lez v0, :cond_3

    .line 85
    const-string v8, "Got %d responses with a blank request id"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    :cond_3
    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v8

    goto :goto_2
.end method
