.class public Lcom/google/android/vending/verifier/api/PackageVerificationRequest;
.super Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;
.source "PackageVerificationRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult",
        "<",
        "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
        "Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .param p4, "request"    # Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            "Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    .local p2, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/vending/verifier/api/PackageVerificationResult;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/vending/verifier/api/BaseVerificationRequestWithResult;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/protobuf/nano/MessageNano;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 4
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    :try_start_0
    iget-object v3, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-static {v3}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->parseFrom([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    .local v0, "clientDownloadResponse":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    invoke-static {v0}, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->fromResponse(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;)Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    move-result-object v2

    .line 41
    .local v2, "result":Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v3

    .end local v0    # "clientDownloadResponse":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .end local v2    # "result":Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    :goto_0
    return-object v3

    .line 31
    :catch_0
    move-exception v1

    .line 32
    .local v1, "ex":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v3, Lcom/android/volley/VolleyError;

    invoke-direct {v3, v1}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0
.end method
