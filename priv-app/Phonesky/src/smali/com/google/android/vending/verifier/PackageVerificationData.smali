.class public Lcom/google/android/vending/verifier/PackageVerificationData;
.super Ljava/lang/Object;
.source "PackageVerificationData.java"


# instance fields
.field public final mApkLength:J

.field public final mCacheFingerprint:J

.field public mCertFingerprints:[[B

.field public final mForwardLocked:Z

.field public mInStoppedState:Z

.field public mInstalledByPlay:Z

.field public final mPackageName:Ljava/lang/String;

.field public final mSha256Digest:[B

.field public final mSuppressUserWarning:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;J[BJZZ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "cacheFingerprint"    # J
    .param p4, "sha256Digest"    # [B
    .param p5, "length"    # J
    .param p7, "forwardLocked"    # Z
    .param p8, "suppressUserWarning"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    .line 25
    iput-wide p2, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mCacheFingerprint:J

    .line 26
    invoke-virtual {p4}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mSha256Digest:[B

    .line 27
    iput-wide p5, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mApkLength:J

    .line 28
    iput-boolean p7, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mForwardLocked:Z

    .line 29
    iput-boolean p8, p0, Lcom/google/android/vending/verifier/PackageVerificationData;->mSuppressUserWarning:Z

    .line 30
    return-void
.end method
