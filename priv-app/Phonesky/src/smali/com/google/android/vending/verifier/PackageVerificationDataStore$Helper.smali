.class Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PackageVerificationDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/PackageVerificationDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Helper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationDataStore;


# direct methods
.method public constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationDataStore;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;->this$0:Lcom/google/android/vending/verifier/PackageVerificationDataStore;

    .line 66
    const-string v0, "package_verification.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 67
    return-void
.end method

.method private recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 76
    :try_start_0
    const-string v0, "DROP TABLE verification_cache"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 80
    return-void

    .line 77
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 71
    const-string v0, "CREATE TABLE verification_cache (package_name STRING PRIMARY KEY, cache_fingerprint INTEGER, sha256_digest BLOB, length INTEGER, forward_locked INTEGER, suppress_user_warning INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationDataStore$Helper;->recreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 86
    return-void
.end method
