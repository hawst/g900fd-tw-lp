.class public final Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CsdClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/protos/CsdClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientMultiDownloadResponse"
.end annotation


# instance fields
.field public responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2043
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2044
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;

    .line 2045
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 2123
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    .locals 1

    .prologue
    .line 2048
    invoke-static {}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->emptyArray()[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    .line 2049
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->cachedSize:I

    .line 2050
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 2069
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2070
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 2071
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 2072
    iget-object v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    aget-object v0, v3, v1

    .line 2073
    .local v0, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    if-eqz v0, :cond_0

    .line 2074
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2071
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2079
    .end local v0    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2087
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2088
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2092
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2093
    :sswitch_0
    return-object p0

    .line 2098
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2100
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    if-nez v5, :cond_2

    move v1, v4

    .line 2101
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    .line 2103
    .local v2, "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    if-eqz v1, :cond_1

    .line 2104
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2106
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2107
    new-instance v5, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    invoke-direct {v5}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;-><init>()V

    aput-object v5, v2, v1

    .line 2108
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2109
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2106
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2100
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    :cond_2
    iget-object v5, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    array-length v1, v5

    goto :goto_1

    .line 2112
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    :cond_3
    new-instance v5, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    invoke-direct {v5}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;-><init>()V

    aput-object v5, v2, v1

    .line 2113
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2114
    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    goto :goto_0

    .line 2088
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2023
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2056
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2057
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 2058
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadResponse;->responses:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    aget-object v0, v2, v1

    .line 2059
    .local v0, "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    if-eqz v0, :cond_0

    .line 2060
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2057
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2064
    .end local v0    # "element":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2065
    return-void
.end method
