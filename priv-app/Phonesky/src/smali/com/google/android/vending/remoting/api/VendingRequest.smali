.class public Lcom/google/android/vending/remoting/api/VendingRequest;
.super Lcom/android/volley/Request;
.source "VendingRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        "U:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/android/volley/Request",
        "<",
        "Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

.field private mAvoidBulkCancel:Z

.field private mExtraHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<TU;>;"
        }
    .end annotation
.end field

.field private final mRequest:Lcom/google/protobuf/nano/MessageNano;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mRequestClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mResponseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TU;>;"
        }
    .end annotation
.end field

.field private final mUseSecureAuthToken:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p6, "apiContext"    # Lcom/google/android/vending/remoting/api/VendingApiContext;
    .param p7, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Ljava/lang/Class",
            "<TU;>;",
            "Lcom/android/volley/Response$Listener",
            "<TU;>;",
            "Lcom/google/android/vending/remoting/api/VendingApiContext;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    .local p2, "requestClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "request":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    .local p4, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TU;>;"
    .local p5, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TU;>;"
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p7}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mAvoidBulkCancel:Z

    .line 73
    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mUseSecureAuthToken:Z

    .line 74
    iput-object p3, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mRequest:Lcom/google/protobuf/nano/MessageNano;

    .line 75
    iput-object p2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mRequestClass:Ljava/lang/Class;

    .line 76
    iput-object p4, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mResponseClass:Ljava/lang/Class;

    .line 77
    iput-object p5, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mListener:Lcom/android/volley/Response$Listener;

    .line 78
    iput-object p6, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    .line 79
    new-instance v0, Lcom/google/android/vending/remoting/api/VendingRetryPolicy;

    iget-object v1, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    iget-boolean v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mUseSecureAuthToken:Z

    invoke-direct {v0, v1, v2}, Lcom/google/android/vending/remoting/api/VendingRetryPolicy;-><init>(Lcom/google/android/vending/remoting/api/VendingApiContext;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/vending/remoting/api/VendingRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 80
    return-void
.end method

.method public static make(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/vending/remoting/api/VendingRequest;
    .locals 8
    .param p0, "url"    # Ljava/lang/String;
    .param p5, "apiContext"    # Lcom/google/android/vending/remoting/api/VendingApiContext;
    .param p6, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            "U:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Ljava/lang/Class",
            "<TU;>;",
            "Lcom/android/volley/Response$Listener",
            "<TU;>;",
            "Lcom/google/android/vending/remoting/api/VendingApiContext;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/google/android/vending/remoting/api/VendingRequest",
            "<TT;TU;>;"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "requestClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "request":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    .local p3, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TU;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TU;>;"
    new-instance v0, Lcom/google/android/vending/remoting/api/VendingRequest;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/vending/remoting/api/VendingRequest;-><init>(Ljava/lang/String;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/google/android/vending/remoting/api/VendingApiContext;Lcom/android/volley/Response$ErrorListener;)V

    return-object v0
.end method


# virtual methods
.method public addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 102
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    return-void
.end method

.method public deliverError(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 192
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    iget-boolean v1, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mUseSecureAuthToken:Z

    invoke-virtual {v0, v1}, Lcom/google/android/vending/remoting/api/VendingApiContext;->invalidateAuthToken(Z)V

    .line 195
    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    .line 196
    return-void
.end method

.method protected deliverResponse(Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    .prologue
    .line 184
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    iget-object v2, p1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    const/4 v3, 0x0

    aget-object v1, v2, v3

    .line 185
    .local v1, "theResponse":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    const-class v2, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    iget-object v3, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mResponseClass:Ljava/lang/Class;

    invoke-static {v1, v2, v3}, Lcom/google/android/play/dfe/utils/NanoProtoHelper;->getParsedResponseFromWrapper(Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    .line 187
    .local v0, "parsedResponse":Lcom/google/protobuf/nano/MessageNano;, "TU;"
    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v2, v0}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 188
    return-void
.end method

.method protected bridge synthetic deliverResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    check-cast p1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/vending/remoting/api/VendingRequest;->deliverResponse(Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;)V

    return-void
.end method

.method public getAvoidBulkCancel()Z
    .locals 1

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    iget-boolean v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mAvoidBulkCancel:Z

    return v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    invoke-virtual {v2}, Lcom/google/android/vending/remoting/api/VendingApiContext;->getHeaders()Ljava/util/Map;

    move-result-object v1

    .line 160
    .local v1, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 162
    .local v0, "combinedHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 163
    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mExtraHeaders:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 164
    move-object v1, v0

    .line 166
    .end local v0    # "combinedHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-object v1
.end method

.method public getParams()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 174
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    .line 175
    .local v0, "parameters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "request"

    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mRequest:Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {p0, v2}, Lcom/google/android/vending/remoting/api/VendingRequest;->serializeRequestProto(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v1, "version"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    return-object v0
.end method

.method protected handlePendingNotifications(Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;Z)Z
    .locals 5
    .param p1, "response"    # Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .param p2, "allowCancellation"    # Z

    .prologue
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    const/4 v2, 0x0

    .line 146
    iget-object v3, p1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    if-eqz v3, :cond_0

    .line 147
    iget-object v1, p1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    .line 148
    .local v1, "pendingNotifications":Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPendingNotificationsHandler()Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;

    move-result-object v0

    .line 149
    .local v0, "handler":Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    invoke-virtual {v4}, Lcom/google/android/vending/remoting/api/VendingApiContext;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v3, v4, v1, p2}, Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;->handlePendingNotifications(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 153
    .end local v0    # "handler":Lcom/google/android/vending/remoting/api/PendingNotificationsHandler;
    .end local v1    # "pendingNotifications":Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;
    :cond_0
    return v2
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 7
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 112
    :try_start_0
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    array-length v4, v4

    invoke-direct {v2, v3, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 115
    .local v2, "unzippedContent":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->readBytes(Ljava/io/InputStream;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->parseFrom([B)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    move-result-object v1

    .line 118
    .local v1, "proto":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    iget-object v3, v1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v3, v3

    if-eq v3, v6, :cond_0

    .line 119
    new-instance v3, Lcom/android/volley/ServerError;

    invoke-direct {v3}, Lcom/android/volley/ServerError;-><init>()V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    .line 133
    .end local v1    # "proto":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .end local v2    # "unzippedContent":Ljava/io/InputStream;
    :goto_0
    return-object v3

    .line 123
    .restart local v1    # "proto":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .restart local v2    # "unzippedContent":Ljava/io/InputStream;
    :cond_0
    iget-object v3, v1, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    iget v3, v3, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    if-eqz v3, :cond_1

    .line 124
    new-instance v3, Lcom/android/volley/ServerError;

    invoke-direct {v3}, Lcom/android/volley/ServerError;-><init>()V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0

    .line 128
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/vending/remoting/api/VendingRequest;->handlePendingNotifications(Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;Z)Z

    .line 130
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 131
    .end local v1    # "proto":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .end local v2    # "unzippedContent":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot parse Vending ResponseProto: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    new-instance v3, Lcom/android/volley/VolleyError;

    invoke-direct {v3}, Lcom/android/volley/VolleyError;-><init>()V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0
.end method

.method serializeRequestProto(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 202
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    .local p1, "request":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;-><init>()V

    .line 203
    .local v1, "requestWrapper":Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;
    const-class v2, Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    iget-object v3, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mRequestClass:Ljava/lang/Class;

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/play/dfe/utils/NanoProtoHelper;->setRequestInWrapper(Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;)V

    .line 205
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$RequestProto;-><init>()V

    .line 206
    .local v0, "batchRequest":Lcom/google/android/finsky/protos/VendingProtos$RequestProto;
    iget-object v2, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mApiContext:Lcom/google/android/vending/remoting/api/VendingApiContext;

    iget-boolean v3, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mUseSecureAuthToken:Z

    invoke-virtual {v2, v3}, Lcom/google/android/vending/remoting/api/VendingApiContext;->getRequestProperties(Z)Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto;->requestProperties:Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;

    .line 207
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/android/finsky/protos/VendingProtos$RequestProto;->request:[Lcom/google/android/finsky/protos/VendingProtos$RequestProto$Request;

    .line 208
    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public setAvoidBulkCancel()V
    .locals 1

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mAvoidBulkCancel:Z

    .line 88
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 220
    .local p0, "this":Lcom/google/android/vending/remoting/api/VendingRequest;, "Lcom/google/android/vending/remoting/api/VendingRequest<TT;TU;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/android/volley/Request;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/vending/remoting/api/VendingRequest;->mRequestClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
