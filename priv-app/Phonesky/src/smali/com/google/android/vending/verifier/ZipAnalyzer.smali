.class public Lcom/google/android/vending/verifier/ZipAnalyzer;
.super Ljava/lang/Object;
.source "ZipAnalyzer.java"


# static fields
.field private static final IMPORTANT_FILES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "classes.dex"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AndroidManifest.xml"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "resources.arsc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "META-INF/MANIFEST.MF"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/vending/verifier/ZipAnalyzer;->IMPORTANT_FILES:[Ljava/lang/String;

    return-void
.end method

.method static analyzeZipFile(Ljava/io/File;)[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    .locals 15
    .param p0, "apkFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 83
    .local v6, "importantEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/instrumentedzip/ZipEntry;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 85
    .local v8, "verificationFailedEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/instrumentedzip/ZipEntry;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 87
    .local v7, "otherEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/instrumentedzip/ZipEntry;>;"
    new-instance v10, Lcom/google/android/instrumentedzip/ZipFile;

    invoke-direct {v10, p0}, Lcom/google/android/instrumentedzip/ZipFile;-><init>(Ljava/io/File;)V

    .line 91
    .local v10, "zipFile":Lcom/google/android/instrumentedzip/ZipFile;
    :try_start_0
    invoke-virtual {v10}, Lcom/google/android/instrumentedzip/ZipFile;->allEntries()Ljava/util/List;

    move-result-object v2

    .line 92
    .local v2, "entries":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/instrumentedzip/ZipEntry;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    if-ge v5, v12, :cond_2

    .line 93
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/instrumentedzip/ZipEntry;

    .line 96
    .local v3, "entry":Lcom/google/android/instrumentedzip/ZipEntry;
    invoke-virtual {v10, v3}, Lcom/google/android/instrumentedzip/ZipFile;->verifyEntry(Lcom/google/android/instrumentedzip/ZipEntry;)V

    .line 98
    invoke-virtual {v3}, Lcom/google/android/instrumentedzip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/google/android/vending/verifier/ZipAnalyzer;->isImportantFile(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 99
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 100
    :cond_0
    iget v12, v3, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    if-eqz v12, :cond_1

    .line 101
    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 139
    .end local v2    # "entries":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/instrumentedzip/ZipEntry;>;"
    .end local v3    # "entry":Lcom/google/android/instrumentedzip/ZipEntry;
    .end local v5    # "i":I
    :catchall_0
    move-exception v12

    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v12

    .line 103
    .restart local v2    # "entries":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/instrumentedzip/ZipEntry;>;"
    .restart local v3    # "entry":Lcom/google/android/instrumentedzip/ZipEntry;
    .restart local v5    # "i":I
    :cond_1
    :try_start_1
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    .end local v3    # "entry":Lcom/google/android/instrumentedzip/ZipEntry;
    :cond_2
    const/16 v12, 0x14

    invoke-static {v6, v12}, Lcom/google/android/vending/verifier/ZipAnalyzer;->shuffleAndChoose(Ljava/util/List;I)Ljava/util/List;

    move-result-object v6

    .line 110
    const/16 v12, 0xa

    invoke-static {v8, v12}, Lcom/google/android/vending/verifier/ZipAnalyzer;->shuffleAndChoose(Ljava/util/List;I)Ljava/util/List;

    move-result-object v8

    .line 112
    const/16 v12, 0xa

    invoke-static {v7, v12}, Lcom/google/android/vending/verifier/ZipAnalyzer;->shuffleAndChoose(Ljava/util/List;I)Ljava/util/List;

    move-result-object v7

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v12

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v13

    add-int/2addr v12, v13

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 117
    .local v0, "chosenEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/instrumentedzip/ZipEntry;>;"
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 118
    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 119
    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 122
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    new-array v4, v12, [Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;

    .line 125
    .local v4, "fileInfos":[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    const/4 v5, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    if-ge v5, v12, :cond_3

    .line 126
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/instrumentedzip/ZipEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    .local v9, "zipEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    const/4 v11, 0x0

    .line 129
    .local v11, "zipInputStream":Ljava/io/InputStream;
    :try_start_2
    invoke-virtual {v10, v9}, Lcom/google/android/instrumentedzip/ZipFile;->getInputStream(Lcom/google/android/instrumentedzip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v11

    .line 130
    invoke-static {v11}, Lcom/google/android/vending/verifier/PackageVerificationService;->getSha256Hash(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 131
    .local v1, "digest":[B
    new-instance v12, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;

    invoke-virtual {v9}, Lcom/google/android/instrumentedzip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v13

    iget v14, v9, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    invoke-direct {v12, v13, v1, v14}, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;-><init>(Ljava/lang/String;[BI)V

    aput-object v12, v4, v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 134
    :try_start_3
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 125
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 134
    .end local v1    # "digest":[B
    :catchall_1
    move-exception v12

    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    .end local v9    # "zipEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    .end local v11    # "zipInputStream":Ljava/io/InputStream;
    :cond_3
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    return-object v4
.end method

.method private static isImportantFile(Ljava/lang/String;)Z
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 144
    sget-object v1, Lcom/google/android/vending/verifier/ZipAnalyzer;->IMPORTANT_FILES:[Ljava/lang/String;

    .line 147
    .local v1, "importantFiles":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 148
    aget-object v2, v1, v0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    const/4 v2, 0x1

    .line 152
    :goto_1
    return v2

    .line 147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static final shuffleAndChoose(Ljava/util/List;I)Ljava/util/List;
    .locals 1
    .param p1, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 172
    .end local p0    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :goto_0
    return-object p0

    .line 171
    .restart local p0    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    invoke-static {p0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 172
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p0

    goto :goto_0
.end method
