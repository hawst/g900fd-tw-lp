.class Lcom/google/android/vending/verifier/PackageVerificationService$5;
.super Ljava/lang/Object;
.source "PackageVerificationService.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerifyAllAppsRequest(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationService;


# direct methods
.method constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationService;)V
    .locals 0

    .prologue
    .line 1217
    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$5;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x0

    .line 1220
    const-string v0, "Multi verification error response %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$5;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->cleanupVerifyInstalledPackages(Z)V
    invoke-static {v0, v2}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$1000(Lcom/google/android/vending/verifier/PackageVerificationService;Z)V

    .line 1222
    return-void
.end method
