.class public Lcom/google/android/vending/verifier/PackageWarningDialog;
.super Landroid/app/Activity;
.source "PackageWarningDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# static fields
.field private static KEY_ACTION:Ljava/lang/String;

.field private static KEY_APP_NAME:Ljava/lang/String;

.field private static KEY_MESSAGE:Ljava/lang/String;

.field private static KEY_PACKAGE_NAME:Ljava/lang/String;

.field private static KEY_RESPONSE_TOKEN:Ljava/lang/String;

.field private static KEY_VERIFICATION_ID:Ljava/lang/String;

.field public static NO_ID:I


# instance fields
.field private mAction:I

.field private mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mId:I

.field private mPackageName:Ljava/lang/String;

.field private mResponseToken:[B

.field private mUserChoiceWasReported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, -0x1

    sput v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->NO_ID:I

    .line 36
    const-string v0, "verification_id"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_VERIFICATION_ID:Ljava/lang/String;

    .line 37
    const-string v0, "action"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_ACTION:Ljava/lang/String;

    .line 38
    const-string v0, "app_name"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_APP_NAME:Ljava/lang/String;

    .line 39
    const-string v0, "message"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_MESSAGE:Ljava/lang/String;

    .line 40
    const-string v0, "package_name"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_PACKAGE_NAME:Ljava/lang/String;

    .line 41
    const-string v0, "response_token"

    sput-object v0, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_RESPONSE_TOKEN:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mUserChoiceWasReported:Z

    return-void
.end method

.method private clearFinishOnTouchOutside()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->setFinishOnTouchOutside(Z)V

    .line 165
    return-void
.end method

.method public static createIntent(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "action"    # I
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "message"    # Ljava/lang/String;
    .param p6, "responseToken"    # [B

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/vending/verifier/PackageWarningDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x50000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 62
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_VERIFICATION_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_ACTION:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_APP_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    sget-object v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_RESPONSE_TOKEN:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 68
    return-object v0
.end method

.method public static show(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "action"    # I
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 73
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/verifier/PackageWarningDialog;->createIntent(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v7

    .line 74
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 75
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 194
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 197
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v13, 0x7f0c02a0

    const v12, 0x7f02013e

    const v11, 0x7f02013d

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    const v7, 0x7f040104

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->setContentView(I)V

    .line 83
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xb

    if-lt v7, v8, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->clearFinishOnTouchOutside()V

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 89
    .local v4, "intent":Landroid/content/Intent;
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_VERIFICATION_ID:Ljava/lang/String;

    sget v8, Lcom/google/android/vending/verifier/PackageWarningDialog;->NO_ID:I

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    .line 90
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_ACTION:Ljava/lang/String;

    invoke-virtual {v4, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    .line 91
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_APP_NAME:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "appName":Ljava/lang/String;
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_MESSAGE:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 93
    .local v5, "message":Ljava/lang/String;
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mPackageName:Ljava/lang/String;

    .line 94
    sget-object v7, Lcom/google/android/vending/verifier/PackageWarningDialog;->KEY_RESPONSE_TOKEN:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mResponseToken:[B

    .line 98
    iget v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    sget v8, Lcom/google/android/vending/verifier/PackageWarningDialog;->NO_ID:I

    if-eq v7, v8, :cond_1

    iget v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    invoke-static {v7, p0}, Lcom/google/android/vending/verifier/PackageVerificationService;->registerDialog(ILandroid/app/Activity;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->finish()V

    .line 155
    :goto_0
    return-void

    .line 104
    :cond_1
    const v7, 0x7f0a0264

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 105
    .local v2, "badgeView":Landroid/widget/ImageView;
    const v7, 0x7f0a02a0

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 106
    .local v3, "bannerView":Landroid/widget/TextView;
    const v7, 0x7f0a00c3

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 107
    .local v6, "messageView":Landroid/widget/TextView;
    const v7, 0x7f0a02a1

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 108
    .local v1, "appNameView":Landroid/widget/TextView;
    const v7, 0x7f0a02a2

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    .line 110
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 111
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 115
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    :cond_3
    const v7, 0x7f0a0114

    invoke-virtual {p0, v7}, Lcom/google/android/vending/verifier/PackageWarningDialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/ButtonBar;

    iput-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    .line 119
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 121
    iget v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 130
    :pswitch_0
    const v7, 0x7f0c0365

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 131
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    const v8, 0x7f0c0369

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setText(I)V

    .line 133
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 134
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v8, 0x7f0c036d

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 135
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v8, 0x7f0c036c

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    .line 136
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, v9}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    goto/16 :goto_0

    .line 123
    :pswitch_1
    const v7, 0x7f0c0366

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 124
    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 126
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, v9}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonVisible(Z)V

    .line 127
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, v13}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    goto/16 :goto_0

    .line 139
    :pswitch_2
    const v7, 0x7f0c0367

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 140
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 141
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    const v8, 0x7f0c036a

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setText(I)V

    .line 142
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 143
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v8, 0x7f0c036f

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 144
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v8, 0x7f0c036e

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    .line 145
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mPackageName:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/finsky/utils/Notifier;->hidePackageRemoveRequestMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 148
    :pswitch_3
    const v7, 0x7f0c0368

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 149
    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 150
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 151
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, v9}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonVisible(Z)V

    .line 152
    iget-object v7, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v7, v13}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    goto/16 :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mUserChoiceWasReported:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    if-nez v0, :cond_1

    .line 179
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportUserChoice(II)V

    .line 186
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->registerDialog(ILandroid/app/Activity;)Z

    .line 187
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 188
    return-void

    .line 181
    :cond_1
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mPackageName:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mResponseToken:[B

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendRemovalResponse(Landroid/content/Context;Ljava/lang/String;ZZ[B)V

    goto :goto_0
.end method

.method public onNegativeButtonClick()V
    .locals 4

    .prologue
    .line 216
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    if-nez v0, :cond_1

    .line 217
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportUserChoice(II)V

    .line 218
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->registerDialog(ILandroid/app/Activity;)Z

    .line 223
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mUserChoiceWasReported:Z

    .line 224
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->finish()V

    .line 225
    return-void

    .line 219
    :cond_1
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mPackageName:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mResponseToken:[B

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendRemovalResponse(Landroid/content/Context;Ljava/lang/String;ZZ[B)V

    goto :goto_0
.end method

.method public onPositiveButtonClick()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 203
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    if-nez v0, :cond_1

    .line 204
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    invoke-static {v0, v3}, Lcom/google/android/vending/verifier/PackageVerificationService;->reportUserChoice(II)V

    .line 205
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mId:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->registerDialog(ILandroid/app/Activity;)Z

    .line 210
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mUserChoiceWasReported:Z

    .line 211
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/PackageWarningDialog;->finish()V

    .line 212
    return-void

    .line 206
    :cond_1
    iget v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mAction:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageWarningDialog;->mResponseToken:[B

    invoke-static {p0, v0, v3, v1, v2}, Lcom/google/android/vending/verifier/PackageVerificationService;->sendRemovalResponse(Landroid/content/Context;Ljava/lang/String;ZZ[B)V

    goto :goto_0
.end method
