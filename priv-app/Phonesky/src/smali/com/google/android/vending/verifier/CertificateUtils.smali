.class public Lcom/google/android/vending/verifier/CertificateUtils;
.super Ljava/lang/Object;
.source "CertificateUtils.java"


# direct methods
.method public static collectCertificates(Ljava/lang/String;)[[B
    .locals 10
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 18
    const/4 v5, 0x0

    .line 20
    .local v5, "jarFile":Ljava/util/jar/JarFile;
    :try_start_0
    new-instance v6, Ljava/util/jar/JarFile;

    invoke-direct {v6, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    .end local v5    # "jarFile":Ljava/util/jar/JarFile;
    .local v6, "jarFile":Ljava/util/jar/JarFile;
    :try_start_1
    const-string v7, "AndroidManifest.xml"

    invoke-virtual {v6, v7}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    move-result-object v4

    .line 24
    .local v4, "jarEntry":Ljava/util/jar/JarEntry;
    invoke-static {v6, v4}, Lcom/google/android/vending/verifier/CertificateUtils;->loadCertificates(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;)[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 25
    .local v0, "certs":[Ljava/security/cert/Certificate;
    if-eqz v0, :cond_0

    array-length v7, v0

    if-nez v7, :cond_3

    .line 26
    :cond_0
    invoke-virtual {v6}, Ljava/util/jar/JarFile;->close()V

    .line 27
    const/4 v7, 0x0

    check-cast v7, [[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 41
    if-eqz v6, :cond_1

    .line 43
    :try_start_2
    invoke-virtual {v6}, Ljava/util/jar/JarFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    move-object v5, v6

    .line 46
    .end local v0    # "certs":[Ljava/security/cert/Certificate;
    .end local v4    # "jarEntry":Ljava/util/jar/JarEntry;
    .end local v6    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v5    # "jarFile":Ljava/util/jar/JarFile;
    :cond_2
    :goto_1
    return-object v7

    .line 44
    .end local v5    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v0    # "certs":[Ljava/security/cert/Certificate;
    .restart local v4    # "jarEntry":Ljava/util/jar/JarEntry;
    .restart local v6    # "jarFile":Ljava/util/jar/JarFile;
    :catch_0
    move-exception v2

    .line 45
    .local v2, "ex":Ljava/io/IOException;
    const-string v8, "Error closing apk file while collecting certificates"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2, v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 29
    .end local v2    # "ex":Ljava/io/IOException;
    :cond_3
    :try_start_3
    invoke-virtual {v6}, Ljava/util/jar/JarFile;->close()V

    .line 31
    array-length v7, v0

    new-array v1, v7, [[B

    .line 32
    .local v1, "encodedCerts":[[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    array-length v7, v0

    if-ge v3, v7, :cond_4

    .line 33
    aget-object v7, v0, v3

    invoke-virtual {v7}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v7

    aput-object v7, v1, v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 32
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 41
    :cond_4
    if-eqz v6, :cond_5

    .line 43
    :try_start_4
    invoke-virtual {v6}, Ljava/util/jar/JarFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_5
    :goto_3
    move-object v5, v6

    .end local v6    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v5    # "jarFile":Ljava/util/jar/JarFile;
    move-object v7, v1

    .line 46
    goto :goto_1

    .line 44
    .end local v5    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v6    # "jarFile":Ljava/util/jar/JarFile;
    :catch_1
    move-exception v2

    .line 45
    .restart local v2    # "ex":Ljava/io/IOException;
    const-string v7, "Error closing apk file while collecting certificates"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v2, v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 37
    .end local v0    # "certs":[Ljava/security/cert/Certificate;
    .end local v1    # "encodedCerts":[[B
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v3    # "i":I
    .end local v4    # "jarEntry":Ljava/util/jar/JarEntry;
    .end local v6    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v5    # "jarFile":Ljava/util/jar/JarFile;
    :catch_2
    move-exception v2

    .line 38
    .local v2, "ex":Ljava/lang/Exception;
    :goto_4
    :try_start_5
    const-string v7, "Error while collecting certificates"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2, v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    const/4 v7, 0x0

    check-cast v7, [[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 41
    if-eqz v5, :cond_2

    .line 43
    :try_start_6
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 44
    :catch_3
    move-exception v2

    .line 45
    .local v2, "ex":Ljava/io/IOException;
    const-string v8, "Error closing apk file while collecting certificates"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2, v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 41
    .end local v2    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_5
    if-eqz v5, :cond_6

    .line 43
    :try_start_7
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 46
    :cond_6
    :goto_6
    throw v7

    .line 44
    :catch_4
    move-exception v2

    .line 45
    .restart local v2    # "ex":Ljava/io/IOException;
    const-string v8, "Error closing apk file while collecting certificates"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v2, v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 41
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v5    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v6    # "jarFile":Ljava/util/jar/JarFile;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v5    # "jarFile":Ljava/util/jar/JarFile;
    goto :goto_5

    .line 37
    .end local v5    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v6    # "jarFile":Ljava/util/jar/JarFile;
    :catch_5
    move-exception v2

    move-object v5, v6

    .end local v6    # "jarFile":Ljava/util/jar/JarFile;
    .restart local v5    # "jarFile":Ljava/util/jar/JarFile;
    goto :goto_4
.end method

.method private static loadCertificates(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;)[Ljava/security/cert/Certificate;
    .locals 5
    .param p0, "jarFile"    # Ljava/util/jar/JarFile;
    .param p1, "entry"    # Ljava/util/jar/JarEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const/16 v3, 0x2000

    new-array v2, v3, [B

    .line 55
    .local v2, "readBuffer":[B
    const/4 v0, 0x0

    .line 57
    .local v0, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-virtual {p0, p1}, Ljava/util/jar/JarFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    .end local v0    # "inputStream":Ljava/io/InputStream;
    .local v1, "inputStream":Ljava/io/InputStream;
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    array-length v4, v2

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 63
    invoke-static {v1}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 65
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/jar/JarEntry;->getCertificates()[Ljava/security/cert/Certificate;

    move-result-object v3

    :goto_0
    return-object v3

    .line 63
    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "inputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    :goto_1
    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v3

    .line 65
    .end local v0    # "inputStream":Ljava/io/InputStream;
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 63
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "inputStream":Ljava/io/InputStream;
    goto :goto_1
.end method
