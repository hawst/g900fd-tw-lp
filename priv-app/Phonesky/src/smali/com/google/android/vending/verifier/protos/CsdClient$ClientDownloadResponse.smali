.class public final Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CsdClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/protos/CsdClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientDownloadResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;


# instance fields
.field public hasRequestId:Z

.field public hasToken:Z

.field public hasUploadApk:Z

.field public hasVerdict:Z

.field public moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

.field public requestId:[B

.field public token:[B

.field public uploadApk:Z

.field public verdict:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1632
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1633
    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    .line 1634
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .locals 2

    .prologue
    .line 1602
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    if-nez v0, :cond_1

    .line 1603
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1605
    :try_start_0
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    if-nez v0, :cond_0

    .line 1606
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    sput-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    .line 1608
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1610
    :cond_1
    sget-object v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->_emptyArray:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    return-object v0

    .line 1608
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1752
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1637
    iput v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    .line 1638
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasVerdict:Z

    .line 1639
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    .line 1640
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    .line 1641
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasToken:Z

    .line 1642
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    .line 1643
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasRequestId:Z

    .line 1644
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    .line 1645
    iput-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasUploadApk:Z

    .line 1646
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->cachedSize:I

    .line 1647
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1673
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1674
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasVerdict:Z

    if-eqz v1, :cond_1

    .line 1675
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1678
    :cond_1
    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    if-eqz v1, :cond_2

    .line 1679
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1682
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasToken:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1683
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1686
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasRequestId:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1687
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1690
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasUploadApk:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    if-eqz v1, :cond_8

    .line 1691
    :cond_7
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1694
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1702
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1703
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1707
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1708
    :sswitch_0
    return-object p0

    .line 1713
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1714
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1718
    :pswitch_1
    iput v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    .line 1719
    iput-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasVerdict:Z

    goto :goto_0

    .line 1725
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    if-nez v2, :cond_1

    .line 1726
    new-instance v2, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    invoke-direct {v2}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    .line 1728
    :cond_1
    iget-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1732
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    .line 1733
    iput-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasToken:Z

    goto :goto_0

    .line 1737
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    .line 1738
    iput-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasRequestId:Z

    goto :goto_0

    .line 1742
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    .line 1743
    iput-boolean v3, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasUploadApk:Z

    goto :goto_0

    .line 1703
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 1714
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1486
    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1653
    iget v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasVerdict:Z

    if-eqz v0, :cond_1

    .line 1654
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->verdict:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1656
    :cond_1
    iget-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    if-eqz v0, :cond_2

    .line 1657
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->moreInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1659
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasToken:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1660
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->token:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1662
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasRequestId:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1663
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->requestId:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1665
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasUploadApk:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    if-eqz v0, :cond_8

    .line 1666
    :cond_7
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->uploadApk:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1668
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1669
    return-void
.end method
