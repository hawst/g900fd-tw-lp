.class public Lcom/google/android/vending/verifier/api/PackageVerificationApi;
.super Ljava/lang/Object;
.source "PackageVerificationApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    }
.end annotation


# direct methods
.method private static buildRequestForApp([BJLjava/lang/String;Ljava/lang/Integer;[[B[[B[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;[[BLjava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;ZZZZ)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    .locals 13
    .param p0, "sha256"    # [B
    .param p1, "length"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "versionCode"    # Ljava/lang/Integer;
    .param p5, "signatures"    # [[B
    .param p6, "certFingerprints"    # [[B
    .param p7, "apkFiles"    # [Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    .param p8, "originatingUri"    # Landroid/net/Uri;
    .param p9, "referrerUri"    # Landroid/net/Uri;
    .param p10, "originatingIp"    # Ljava/net/InetAddress;
    .param p11, "referrerIp"    # Ljava/net/InetAddress;
    .param p12, "originatingPackageNames"    # [Ljava/lang/String;
    .param p13, "originatingSignatures"    # [[B
    .param p14, "locale"    # Ljava/lang/String;
    .param p15, "androidId"    # Ljava/lang/Long;
    .param p16, "requestIndex"    # Ljava/lang/Integer;
    .param p17, "installedByPlay"    # Z
    .param p18, "forwardLocked"    # Z
    .param p19, "inStoppedState"    # Z
    .param p20, "dontWarnAgain"    # Z

    .prologue
    .line 159
    new-instance v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    invoke-direct {v9}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;-><init>()V

    .line 161
    .local v9, "requestProto":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    if-nez p3, :cond_0

    if-eqz p4, :cond_8

    .line 162
    :cond_0
    new-instance v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    invoke-direct {v4}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;-><init>()V

    .line 164
    .local v4, "apkInfo":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;
    if-eqz p3, :cond_1

    .line 165
    move-object/from16 v0, p3

    iput-object v0, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->packageName:Ljava/lang/String;

    .line 166
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasPackageName:Z

    .line 168
    :cond_1
    if-eqz p4, :cond_2

    .line 169
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->versionCode:I

    .line 170
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasVersionCode:Z

    .line 172
    :cond_2
    if-eqz p7, :cond_3

    .line 173
    move-object/from16 v0, p7

    array-length v10, v0

    new-array v10, v10, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    iput-object v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    .line 174
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p7

    array-length v10, v0

    if-ge v6, v10, :cond_3

    .line 175
    iget-object v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->files:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    aget-object v11, p7, v6

    invoke-static {v11}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createFileInfo(Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    move-result-object v11

    aput-object v11, v10, v6

    .line 174
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 178
    .end local v6    # "i":I
    :cond_3
    if-eqz p17, :cond_4

    .line 179
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->installedByPlay:Z

    .line 180
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInstalledByPlay:Z

    .line 182
    :cond_4
    if-eqz p18, :cond_5

    .line 183
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->forwardLocked:Z

    .line 184
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasForwardLocked:Z

    .line 186
    :cond_5
    if-eqz p19, :cond_6

    .line 187
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->inStoppedState:Z

    .line 188
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasInStoppedState:Z

    .line 190
    :cond_6
    if-eqz p20, :cond_7

    .line 191
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->dontWarnAgain:Z

    .line 192
    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;->hasDontWarnAgain:Z

    .line 194
    :cond_7
    iput-object v4, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->apkInfo:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;

    .line 197
    .end local v4    # "apkInfo":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$ApkInfo;
    :cond_8
    if-eqz p5, :cond_10

    move-object/from16 v0, p5

    array-length v10, v0

    if-lez v10, :cond_10

    .line 198
    invoke-static/range {p5 .. p5}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createSignatureInfo([[B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 203
    :cond_9
    :goto_1
    if-eqz p8, :cond_12

    .line 204
    invoke-virtual/range {p8 .. p8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    .line 205
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    .line 207
    if-eqz p9, :cond_11

    .line 208
    const/4 v10, 0x2

    new-array v10, v10, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    .line 213
    :goto_2
    const/4 v10, 0x0

    move-object/from16 v0, p8

    move-object/from16 v1, p10

    move-object/from16 v2, p9

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    move-result-object v7

    .line 216
    .local v7, "originatingResource":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    iget-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    .line 218
    if-eqz p9, :cond_a

    .line 219
    const/4 v10, 0x0

    const/4 v11, 0x2

    move-object/from16 v0, p9

    move-object/from16 v1, p11

    invoke-static {v0, v1, v10, v11}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    move-result-object v8

    .line 222
    .local v8, "referrerResource":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    iget-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    const/4 v11, 0x1

    aput-object v8, v10, v11

    .line 230
    .end local v7    # "originatingResource":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    .end local v8    # "referrerResource":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    :cond_a
    :goto_3
    if-eqz p12, :cond_b

    .line 231
    move-object/from16 v0, p12

    iput-object v0, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingPackages:[Ljava/lang/String;

    .line 234
    :cond_b
    if-eqz p13, :cond_c

    move-object/from16 v0, p13

    array-length v10, v0

    if-lez v10, :cond_c

    .line 235
    invoke-static/range {p13 .. p13}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createSignatureInfo([[B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->originatingSignature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    .line 238
    :cond_c
    iput-wide p1, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->length:J

    .line 239
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLength:Z

    .line 241
    invoke-static {p0}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->buildSha256Digest([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    .line 242
    const/4 v10, 0x2

    iput v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->downloadType:I

    .line 243
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasDownloadType:Z

    .line 245
    if-eqz p14, :cond_d

    .line 246
    move-object/from16 v0, p14

    iput-object v0, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->locale:Ljava/lang/String;

    .line 247
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasLocale:Z

    .line 250
    :cond_d
    if-eqz p15, :cond_e

    .line 251
    invoke-virtual/range {p15 .. p15}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iput-wide v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->androidId:J

    .line 252
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasAndroidId:Z

    .line 255
    :cond_e
    if-eqz p16, :cond_f

    .line 259
    :try_start_0
    invoke-virtual/range {p16 .. p16}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "UTF-8"

    invoke-virtual {v10, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->requestId:[B

    .line 261
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasRequestId:Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :cond_f
    return-object v9

    .line 199
    :cond_10
    if-eqz p6, :cond_9

    move-object/from16 v0, p6

    array-length v10, v0

    if-lez v10, :cond_9

    .line 200
    invoke-static/range {p6 .. p6}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->createFingerprintSignatureInfo([[B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->signature:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    goto/16 :goto_1

    .line 210
    :cond_11
    const/4 v10, 0x1

    new-array v10, v10, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->resources:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    goto/16 :goto_2

    .line 226
    :cond_12
    const-string v10, ""

    iput-object v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->url:Ljava/lang/String;

    .line 227
    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;->hasUrl:Z

    goto :goto_3

    .line 262
    :catch_0
    move-exception v5

    .line 264
    .local v5, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10
.end method

.method private static buildSha256Digest([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;
    .locals 2
    .param p0, "sha256hash"    # [B

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;-><init>()V

    .line 324
    .local v0, "digests":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;
    iput-object p0, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;->sha256:[B

    .line 325
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;->hasSha256:Z

    .line 326
    return-object v0
.end method

.method private static createFileInfo(Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    .locals 3
    .param p0, "fileInfo"    # Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;

    .prologue
    const/4 v2, 0x1

    .line 365
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;-><init>()V

    .line 368
    .local v0, "fileInfoProto":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;
    iget-object v1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->name:Ljava/lang/String;

    .line 369
    iput-boolean v2, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->hasName:Z

    .line 370
    iget-object v1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->digest:[B

    invoke-static {v1}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->buildSha256Digest([B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->digests:Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Digests;

    .line 371
    iget v1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;->verificationErrors:I

    iput v1, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->verificationErrors:I

    .line 372
    iput-boolean v2, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$FileInfo;->hasVerificationErrors:Z

    .line 373
    return-object v0
.end method

.method private static createFingerprintSignatureInfo([[B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;
    .locals 7
    .param p0, "certFingerprints"    # [[B

    .prologue
    const/4 v6, 0x1

    .line 346
    new-instance v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-direct {v3}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;-><init>()V

    .line 347
    .local v3, "sigInfo":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;
    array-length v4, p0

    new-array v4, v4, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    iput-object v4, v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;->certificateChain:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    .line 349
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_0

    .line 350
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;-><init>()V

    .line 351
    .local v0, "certChain":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;
    new-instance v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    invoke-direct {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;-><init>()V

    .line 354
    .local v1, "certElement":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;
    aget-object v4, p0, v2

    iput-object v4, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;->fingerprint:[B

    .line 355
    iput-boolean v6, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;->hasFingerprint:Z

    .line 357
    new-array v4, v6, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    iput-object v4, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;->element:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    .line 358
    iget-object v4, v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;->certificateChain:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    aput-object v0, v4, v2

    .line 349
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 361
    .end local v0    # "certChain":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;
    .end local v1    # "certElement":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;
    :cond_0
    return-object v3
.end method

.method private static createResource(Landroid/net/Uri;Ljava/net/InetAddress;Landroid/net/Uri;I)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "ip"    # Ljava/net/InetAddress;
    .param p2, "referrerUri"    # Landroid/net/Uri;
    .param p3, "type"    # I

    .prologue
    const/4 v3, 0x1

    .line 299
    new-instance v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;

    invoke-direct {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;-><init>()V

    .line 301
    .local v1, "resource":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->url:Ljava/lang/String;

    .line 302
    iput-boolean v3, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->hasUrl:Z

    .line 303
    iput p3, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->type:I

    .line 304
    iput-boolean v3, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->hasType:Z

    .line 305
    if-eqz p2, :cond_0

    .line 306
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->referrer:Ljava/lang/String;

    .line 307
    iput-boolean v3, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->hasReferrer:Z

    .line 310
    :cond_0
    if-eqz p1, :cond_1

    .line 312
    :try_start_0
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->remoteIp:[B

    .line 313
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$Resource;->hasRemoteIp:Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    :cond_1
    return-object v1

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static createSignatureInfo([[B)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;
    .locals 7
    .param p0, "signatures"    # [[B

    .prologue
    const/4 v6, 0x1

    .line 330
    new-instance v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;

    invoke-direct {v3}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;-><init>()V

    .line 332
    .local v3, "sigInfo":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;
    array-length v4, p0

    new-array v4, v4, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    iput-object v4, v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;->certificateChain:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    .line 334
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_0

    .line 335
    new-instance v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    invoke-direct {v0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;-><init>()V

    .line 336
    .local v0, "certChain":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;
    new-instance v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    invoke-direct {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;-><init>()V

    .line 337
    .local v1, "certElement":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;
    aget-object v4, p0, v2

    iput-object v4, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;->certificate:[B

    .line 338
    iput-boolean v6, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;->hasCertificate:Z

    .line 339
    new-array v4, v6, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    iput-object v4, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;->element:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;

    .line 340
    iget-object v4, v3, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$SignatureInfo;->certificateChain:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;

    aput-object v0, v4, v2

    .line 334
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 342
    .end local v0    # "certChain":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain;
    .end local v1    # "certElement":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest$CertificateChain$Element;
    :cond_0
    return-object v3
.end method

.method public static reportUserDecision(IZ[BLcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 3
    .param p0, "userDecision"    # I
    .param p1, "dontWarn"    # Z
    .param p2, "requestToken"    # [B
    .param p3, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ[B",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 273
    new-instance v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;

    invoke-direct {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;-><init>()V

    .line 276
    .local v1, "requestProto":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;
    if-eqz p2, :cond_0

    .line 277
    iput-object p2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->token:[B

    .line 278
    iput-boolean v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->hasToken:Z

    .line 280
    :cond_0
    iput p0, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->userDecision:I

    .line 281
    iput-boolean v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->hasUserDecision:Z

    .line 283
    if-eqz p1, :cond_1

    .line 284
    iput-boolean v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->dontWarnAgain:Z

    .line 285
    iput-boolean v2, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;->hasDontWarnAgain:Z

    .line 292
    :cond_1
    new-instance v0, Lcom/google/android/vending/verifier/api/PackageVerificationStatsRequest;

    const-string v2, "https://safebrowsing.google.com/safebrowsing/clientreport/download-stat"

    invoke-direct {v0, v2, p3, v1}, Lcom/google/android/vending/verifier/api/PackageVerificationStatsRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadStatsRequest;)V

    .line 294
    .local v0, "request":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v2

    return-object v2
.end method

.method public static verifyApp([BJLjava/lang/String;Ljava/lang/Integer;[[B[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;[[BLjava/lang/String;JLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 27
    .param p0, "sha256"    # [B
    .param p1, "length"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "versionCode"    # Ljava/lang/Integer;
    .param p5, "signatures"    # [[B
    .param p6, "apkFiles"    # [Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;
    .param p7, "originatingUri"    # Landroid/net/Uri;
    .param p8, "referrerUri"    # Landroid/net/Uri;
    .param p9, "originatingIp"    # Ljava/net/InetAddress;
    .param p10, "referrerIp"    # Ljava/net/InetAddress;
    .param p11, "originatingPackageNames"    # [Ljava/lang/String;
    .param p12, "originatingSignatures"    # [[B
    .param p13, "locale"    # Ljava/lang/String;
    .param p14, "androidId"    # J
    .param p17, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BJ",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "[[B[",
            "Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Ljava/net/InetAddress;",
            "Ljava/net/InetAddress;",
            "[",
            "Ljava/lang/String;",
            "[[B",
            "Ljava/lang/String;",
            "J",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 84
    .local p16, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/vending/verifier/api/PackageVerificationResult;>;"
    const/4 v11, 0x0

    check-cast v11, [[B

    invoke-static/range {p14 .. p15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v16, p10

    move-object/from16 v17, p11

    move-object/from16 v18, p12

    move-object/from16 v19, p13

    invoke-static/range {v5 .. v25}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->buildRequestForApp([BJLjava/lang/String;Ljava/lang/Integer;[[B[[B[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;[[BLjava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;ZZZZ)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    move-result-object v26

    .line 93
    .local v26, "requestProto":Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;
    new-instance v4, Lcom/google/android/vending/verifier/api/PackageVerificationRequest;

    const-string v5, "https://safebrowsing.google.com/safebrowsing/clientreport/download"

    move-object/from16 v0, p16

    move-object/from16 v1, p17

    move-object/from16 v2, v26

    invoke-direct {v4, v5, v0, v1, v2}, Lcom/google/android/vending/verifier/api/PackageVerificationRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;)V

    .line 96
    .local v4, "request":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v5

    return-object v5
.end method

.method public static verifyInstalledApps(Ljava/util/Collection;Ljava/lang/String;Ljava/lang/Long;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 31
    .param p1, "locale"    # Ljava/lang/String;
    .param p2, "androidId"    # Ljava/lang/Long;
    .param p3, "userConsented"    # Z
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/vending/verifier/PackageVerificationData;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Z",
            "Lcom/android/volley/Response$Listener",
            "<[",
            "Lcom/google/android/vending/verifier/api/PackageVerificationResult;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "applicationsData":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/vending/verifier/PackageVerificationData;>;"
    .local p4, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<[Lcom/google/android/vending/verifier/api/PackageVerificationResult;>;"
    new-instance v28, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;

    invoke-direct/range {v28 .. v28}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;-><init>()V

    .line 120
    .local v28, "multiRequestProto":Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;
    invoke-interface/range {p0 .. p0}, Ljava/util/Collection;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    move-object/from16 v0, v28

    iput-object v5, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;->requests:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    .line 122
    move/from16 v0, p3

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;->userConsented:Z

    .line 123
    const/4 v5, 0x1

    move-object/from16 v0, v28

    iput-boolean v5, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;->hasUserConsented:Z

    .line 125
    const/16 v27, 0x0

    .line 126
    .local v27, "index":I
    invoke-interface/range {p0 .. p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/vending/verifier/PackageVerificationData;

    .line 127
    .local v4, "applicationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;->requests:[Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    move-object/from16 v30, v0

    iget-object v5, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mSha256Digest:[B

    iget-wide v6, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mApkLength:J

    iget-object v8, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mPackageName:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    check-cast v10, [[B

    iget-object v11, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mCertFingerprints:[[B

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    check-cast v18, [[B

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    iget-boolean v0, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mInstalledByPlay:Z

    move/from16 v22, v0

    iget-boolean v0, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mForwardLocked:Z

    move/from16 v23, v0

    iget-boolean v0, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mInStoppedState:Z

    move/from16 v24, v0

    iget-boolean v0, v4, Lcom/google/android/vending/verifier/PackageVerificationData;->mSuppressUserWarning:Z

    move/from16 v25, v0

    move-object/from16 v19, p1

    move-object/from16 v20, p2

    invoke-static/range {v5 .. v25}, Lcom/google/android/vending/verifier/api/PackageVerificationApi;->buildRequestForApp([BJLjava/lang/String;Ljava/lang/Integer;[[B[[B[Lcom/google/android/vending/verifier/api/PackageVerificationApi$FileInfo;Landroid/net/Uri;Landroid/net/Uri;Ljava/net/InetAddress;Ljava/net/InetAddress;[Ljava/lang/String;[[BLjava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;ZZZZ)Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadRequest;

    move-result-object v5

    aput-object v5, v30, v27

    .line 136
    const/16 p1, 0x0

    .line 137
    const/16 p2, 0x0

    .line 139
    add-int/lit8 v27, v27, 0x1

    .line 140
    goto :goto_0

    .line 146
    .end local v4    # "applicationData":Lcom/google/android/vending/verifier/PackageVerificationData;
    :cond_0
    new-instance v29, Lcom/google/android/vending/verifier/api/PackageVerificationMultiRequest;

    const-string v5, "https://safebrowsing.google.com/safebrowsing/clientreport/download-multi"

    move-object/from16 v0, v29

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, v28

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/vending/verifier/api/PackageVerificationMultiRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/google/android/vending/verifier/protos/CsdClient$ClientMultiDownloadRequest;)V

    .line 149
    .local v29, "request":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    move-result-object v5

    return-object v5
.end method
