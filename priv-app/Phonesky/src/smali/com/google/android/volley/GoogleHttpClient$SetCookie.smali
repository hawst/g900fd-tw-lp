.class final Lcom/google/android/volley/GoogleHttpClient$SetCookie;
.super Ljava/lang/Object;
.source "GoogleHttpClient.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/volley/GoogleHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SetCookie"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

.field private final mHandler:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field final synthetic this$0:Lcom/google/android/volley/GoogleHttpClient;


# direct methods
.method private constructor <init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)V
    .locals 0
    .param p3, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p4, "cookieSource"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/ResponseHandler",
            "<TT;>;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;",
            ")V"
        }
    .end annotation

    .prologue
    .line 528
    .local p0, "this":Lcom/google/android/volley/GoogleHttpClient$SetCookie;, "Lcom/google/android/volley/GoogleHttpClient$SetCookie<TT;>;"
    .local p2, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<TT;>;"
    iput-object p1, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->this$0:Lcom/google/android/volley/GoogleHttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    iput-object p2, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mHandler:Lorg/apache/http/client/ResponseHandler;

    .line 530
    iput-object p3, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 531
    iput-object p4, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    .line 532
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;Lcom/google/android/volley/GoogleHttpClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/volley/GoogleHttpClient;
    .param p2, "x1"    # Lorg/apache/http/client/ResponseHandler;
    .param p3, "x2"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p4, "x3"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
    .param p5, "x4"    # Lcom/google/android/volley/GoogleHttpClient$1;

    .prologue
    .line 522
    .local p0, "this":Lcom/google/android/volley/GoogleHttpClient$SetCookie;, "Lcom/google/android/volley/GoogleHttpClient$SetCookie<TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/volley/GoogleHttpClient$SetCookie;-><init>(Lcom/google/android/volley/GoogleHttpClient;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)V

    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 3
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 537
    .local p0, "this":Lcom/google/android/volley/GoogleHttpClient$SetCookie;, "Lcom/google/android/volley/GoogleHttpClient$SetCookie<TT;>;"
    iget-object v0, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mHandler:Lorg/apache/http/client/ResponseHandler;

    iget-object v1, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v2, p0, Lcom/google/android/volley/GoogleHttpClient$SetCookie;->mCookieSource:Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    invoke-static {v1, p1, v2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->updateFromResponseCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/http/client/ResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
