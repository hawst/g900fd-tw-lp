.class public Lcom/google/android/instrumentedzip/ZipFile;
.super Ljava/lang/Object;
.source "ZipFile.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/instrumentedzip/ZipFile$ZipInflaterInputStream;,
        Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    }
.end annotation


# instance fields
.field private final entryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/instrumentedzip/ZipEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final entryMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/instrumentedzip/ZipEntry;",
            ">;"
        }
    .end annotation
.end field

.field private raf:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->entryMap:Ljava/util/LinkedHashMap;

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->entryList:Ljava/util/List;

    .line 99
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    .line 103
    invoke-direct {p0}, Lcom/google/android/instrumentedzip/ZipFile;->readCentralDir()V

    .line 104
    return-void
.end method

.method private checkNotClosed()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Zip file closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    return-void
.end method

.method private readCentralDir()V
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v24

    const-wide/16 v26, 0x16

    sub-long v20, v24, v26

    .line 293
    .local v20, "scanOffset":J
    const-wide/16 v24, 0x0

    cmp-long v24, v20, v24

    if-gez v24, :cond_0

    .line 294
    new-instance v24, Ljava/util/zip/ZipException;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "File too short to be a zip file: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 297
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    const-wide/16 v26, 0x0

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v12

    .line 299
    .local v12, "headerMagic":I
    int-to-long v0, v12

    move-wide/from16 v24, v0

    const-wide/32 v26, 0x4034b50

    cmp-long v24, v24, v26

    if-eqz v24, :cond_1

    .line 300
    new-instance v24, Ljava/util/zip/ZipException;

    const-string v25, "Not a zip archive"

    invoke-direct/range {v24 .. v25}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 303
    :cond_1
    const-wide/32 v24, 0x10000

    sub-long v22, v20, v24

    .line 304
    .local v22, "stopOffset":J
    const-wide/16 v24, 0x0

    cmp-long v24, v22, v24

    if-gez v24, :cond_2

    .line 305
    const-wide/16 v22, 0x0

    .line 309
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/32 v26, 0x6054b50

    cmp-long v24, v24, v26

    if-nez v24, :cond_4

    .line 322
    const/16 v24, 0x12

    move/from16 v0, v24

    new-array v10, v0, [B

    .line 323
    .local v10, "eocd":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 326
    new-instance v14, Lcom/google/android/instrumentedzip/HeapBufferIterator;

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-direct {v14, v10, v0}, Lcom/google/android/instrumentedzip/HeapBufferIterator;-><init>([BI)V

    .line 327
    .local v14, "it":Lcom/google/android/instrumentedzip/HeapBufferIterator;
    invoke-virtual {v14}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v24

    const v25, 0xffff

    and-int v5, v24, v25

    .line 328
    .local v5, "diskNumber":I
    invoke-virtual {v14}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v24

    const v25, 0xffff

    and-int v8, v24, v25

    .line 329
    .local v8, "diskWithCentralDir":I
    invoke-virtual {v14}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v24

    const v25, 0xffff

    and-int v16, v24, v25

    .line 330
    .local v16, "numEntries":I
    invoke-virtual {v14}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v24

    const v25, 0xffff

    and-int v19, v24, v25

    .line 331
    .local v19, "totalNumEntries":I
    const/16 v24, 0x4

    move/from16 v0, v24

    invoke-virtual {v14, v0}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->skip(I)V

    .line 332
    invoke-virtual {v14}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide v26, 0xffffffffL

    and-long v6, v24, v26

    .line 336
    .local v6, "centralDirOffset":J
    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    if-nez v5, :cond_3

    if-eqz v8, :cond_5

    .line 337
    :cond_3
    new-instance v24, Ljava/util/zip/ZipException;

    const-string v25, "Spanned archives not supported"

    invoke-direct/range {v24 .. v25}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 314
    .end local v5    # "diskNumber":I
    .end local v6    # "centralDirOffset":J
    .end local v8    # "diskWithCentralDir":I
    .end local v10    # "eocd":[B
    .end local v14    # "it":Lcom/google/android/instrumentedzip/HeapBufferIterator;
    .end local v16    # "numEntries":I
    .end local v19    # "totalNumEntries":I
    :cond_4
    const-wide/16 v24, 0x1

    sub-long v20, v20, v24

    .line 315
    cmp-long v24, v20, v22

    if-gez v24, :cond_2

    .line 316
    new-instance v24, Ljava/util/zip/ZipException;

    const-string v25, "End Of Central Directory signature not found"

    invoke-direct/range {v24 .. v25}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 344
    .restart local v5    # "diskNumber":I
    .restart local v6    # "centralDirOffset":J
    .restart local v8    # "diskWithCentralDir":I
    .restart local v10    # "eocd":[B
    .restart local v14    # "it":Lcom/google/android/instrumentedzip/HeapBufferIterator;
    .restart local v16    # "numEntries":I
    .restart local v19    # "totalNumEntries":I
    :cond_5
    new-instance v18, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    move-object/from16 v24, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;-><init>(Ljava/io/RandomAccessFile;J)V

    .line 345
    .local v18, "rafStream":Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    new-instance v4, Ljava/io/BufferedInputStream;

    const/16 v24, 0x1000

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-direct {v4, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 346
    .local v4, "bufferedStream":Ljava/io/BufferedInputStream;
    const/16 v24, 0x2e

    move/from16 v0, v24

    new-array v11, v0, [B

    .line 347
    .local v11, "hdrBuf":[B
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v13, v0, :cond_8

    .line 348
    new-instance v15, Lcom/google/android/instrumentedzip/ZipEntry;

    invoke-direct {v15, v11, v4}, Lcom/google/android/instrumentedzip/ZipEntry;-><init>([BLjava/io/InputStream;)V

    .line 349
    .local v15, "newEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    iget-wide v0, v15, Lcom/google/android/instrumentedzip/ZipEntry;->localHeaderRelOffset:J

    move-wide/from16 v24, v0

    cmp-long v24, v24, v6

    if-ltz v24, :cond_6

    .line 350
    new-instance v24, Ljava/util/zip/ZipException;

    const-string v25, "Local file header offset is after central directory"

    invoke-direct/range {v24 .. v25}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 352
    :cond_6
    invoke-virtual {v15}, Lcom/google/android/instrumentedzip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    .line 353
    .local v9, "entryName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->entryMap:Ljava/util/LinkedHashMap;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9, v15}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/instrumentedzip/ZipEntry;

    .line 354
    .local v17, "oldEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    if-eqz v17, :cond_7

    .line 355
    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    move/from16 v24, v0

    or-int/lit8 v24, v24, 0x20

    move/from16 v0, v24

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 356
    iget v0, v15, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    move/from16 v24, v0

    or-int/lit8 v24, v24, 0x20

    move/from16 v0, v24

    iput v0, v15, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 358
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/instrumentedzip/ZipFile;->entryList:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 360
    .end local v9    # "entryName":Ljava/lang/String;
    .end local v15    # "newEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    .end local v17    # "oldEntry":Lcom/google/android/instrumentedzip/ZipEntry;
    :cond_8
    return-void
.end method

.method static throwZipException(Ljava/lang/String;I)V
    .locals 6
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "magic"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/ZipException;
        }
    .end annotation

    .prologue
    .line 363
    new-instance v0, Ljava/util/zip/ZipException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " signature not found; was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%08x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private verifyAndGetInputStream(Lcom/google/android/instrumentedzip/ZipEntry;Z)Ljava/io/InputStream;
    .locals 14
    .param p1, "entry"    # Lcom/google/android/instrumentedzip/ZipEntry;
    .param p2, "onlyVerify"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p1}, Lcom/google/android/instrumentedzip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/instrumentedzip/ZipFile;->getEntry(Ljava/lang/String;)Lcom/google/android/instrumentedzip/ZipEntry;

    move-result-object p1

    .line 214
    if-nez p1, :cond_0

    .line 215
    const/4 v7, 0x0

    .line 267
    :goto_0
    return-object v7

    .line 219
    :cond_0
    iget-object v6, p0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    .line 220
    .local v6, "localRaf":Ljava/io/RandomAccessFile;
    monitor-enter v6

    .line 224
    :try_start_0
    new-instance v7, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;

    iget-wide v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->localHeaderRelOffset:J

    invoke-direct {v7, v6, v8, v9}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;-><init>(Ljava/io/RandomAccessFile;J)V

    .line 225
    .local v7, "rafStream":Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, v7}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 227
    .local v4, "is":Ljava/io/DataInputStream;
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v5

    .line 228
    .local v5, "localMagic":I
    int-to-long v8, v5

    const-wide/32 v10, 0x4034b50

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    .line 229
    const-string v8, "Local File Header"

    invoke-static {v8, v5}, Lcom/google/android/instrumentedzip/ZipFile;->throwZipException(Ljava/lang/String;I)V

    .line 232
    :cond_1
    const/4 v8, 0x2

    invoke-virtual {v4, v8}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 235
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    invoke-static {v8}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v8

    const v9, 0xffff

    and-int v3, v8, v9

    .line 236
    .local v3, "gpbf":I
    and-int/lit8 v8, v3, 0x1

    if-eqz v8, :cond_2

    .line 237
    new-instance v8, Ljava/util/zip/ZipException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid General Purpose Bit Flag: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 271
    .end local v3    # "gpbf":I
    .end local v4    # "is":Ljava/io/DataInputStream;
    .end local v5    # "localMagic":I
    .end local v7    # "rafStream":Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    :catchall_0
    move-exception v8

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 242
    .restart local v3    # "gpbf":I
    .restart local v4    # "is":Ljava/io/DataInputStream;
    .restart local v5    # "localMagic":I
    .restart local v7    # "rafStream":Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    :cond_2
    const/16 v8, 0x12

    :try_start_1
    invoke-virtual {v4, v8}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 243
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    invoke-static {v8}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v8

    const v9, 0xffff

    and-int v2, v8, v9

    .line 244
    .local v2, "fileNameLength":I
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    invoke-static {v8}, Ljava/lang/Short;->reverseBytes(S)S

    move-result v8

    const v9, 0xffff

    and-int v1, v8, v9

    .line 245
    .local v1, "extraFieldLength":I
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 247
    iget v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->nameLength:I

    if-eq v2, v8, :cond_3

    .line 248
    iget v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    or-int/lit8 v8, v8, 0x8

    iput v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 251
    :cond_3
    const v8, 0x8000

    if-lt v1, v8, :cond_4

    .line 252
    iget v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    or-int/lit8 v8, v8, 0x4

    iput v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 255
    :cond_4
    if-eqz p2, :cond_5

    .line 256
    const/4 v7, 0x0

    monitor-exit v6

    goto/16 :goto_0

    .line 259
    :cond_5
    add-int v8, v2, v1

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->skip(J)J

    .line 261
    iget v8, p1, Lcom/google/android/instrumentedzip/ZipEntry;->compressionMethod:I

    if-nez v8, :cond_6

    .line 262
    # getter for: Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J
    invoke-static {v7}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->access$200(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;)J

    move-result-wide v8

    iget-wide v10, p1, Lcom/google/android/instrumentedzip/ZipEntry;->size:J

    add-long/2addr v8, v10

    # setter for: Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J
    invoke-static {v7, v8, v9}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->access$102(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;J)J

    .line 263
    monitor-exit v6

    goto/16 :goto_0

    .line 265
    :cond_6
    # getter for: Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J
    invoke-static {v7}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->access$200(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;)J

    move-result-wide v8

    iget-wide v10, p1, Lcom/google/android/instrumentedzip/ZipEntry;->compressedSize:J

    add-long/2addr v8, v10

    # setter for: Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J
    invoke-static {v7, v8, v9}, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->access$102(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;J)J

    .line 266
    const/16 v8, 0x400

    invoke-virtual {p1}, Lcom/google/android/instrumentedzip/ZipEntry;->getSize()J

    move-result-wide v10

    const-wide/16 v12, 0x1000

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    long-to-int v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 267
    .local v0, "bufSize":I
    new-instance v8, Lcom/google/android/instrumentedzip/ZipFile$ZipInflaterInputStream;

    new-instance v9, Ljava/util/zip/Inflater;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Ljava/util/zip/Inflater;-><init>(Z)V

    invoke-direct {v8, v7, v9, v0, p1}, Lcom/google/android/instrumentedzip/ZipFile$ZipInflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;ILcom/google/android/instrumentedzip/ZipEntry;)V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v7, v8

    goto/16 :goto_0
.end method


# virtual methods
.method public allEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/instrumentedzip/ZipEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipFile;->entryList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    .line 115
    .local v0, "localRaf":Ljava/io/RandomAccessFile;
    if-eqz v0, :cond_0

    .line 116
    monitor-enter v0

    .line 117
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->raf:Ljava/io/RandomAccessFile;

    .line 118
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 119
    monitor-exit v0

    .line 121
    :cond_0
    return-void

    .line 119
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getEntry(Ljava/lang/String;)Lcom/google/android/instrumentedzip/ZipEntry;
    .locals 4
    .param p1, "entryName"    # Ljava/lang/String;

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/instrumentedzip/ZipFile;->checkNotClosed()V

    .line 172
    if-nez p1, :cond_0

    .line 173
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "entryName == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->entryMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/instrumentedzip/ZipEntry;

    .line 177
    .local v0, "ze":Lcom/google/android/instrumentedzip/ZipEntry;
    if-nez v0, :cond_1

    .line 178
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile;->entryMap:Ljava/util/LinkedHashMap;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ze":Lcom/google/android/instrumentedzip/ZipEntry;
    check-cast v0, Lcom/google/android/instrumentedzip/ZipEntry;

    .line 180
    .restart local v0    # "ze":Lcom/google/android/instrumentedzip/ZipEntry;
    :cond_1
    return-object v0
.end method

.method public getInputStream(Lcom/google/android/instrumentedzip/ZipEntry;)Ljava/io/InputStream;
    .locals 1
    .param p1, "entry"    # Lcom/google/android/instrumentedzip/ZipEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/instrumentedzip/ZipFile;->verifyAndGetInputStream(Lcom/google/android/instrumentedzip/ZipEntry;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public verifyEntry(Lcom/google/android/instrumentedzip/ZipEntry;)V
    .locals 1
    .param p1, "entry"    # Lcom/google/android/instrumentedzip/ZipEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/instrumentedzip/ZipFile;->verifyAndGetInputStream(Lcom/google/android/instrumentedzip/ZipEntry;Z)Ljava/io/InputStream;

    .line 194
    return-void
.end method
