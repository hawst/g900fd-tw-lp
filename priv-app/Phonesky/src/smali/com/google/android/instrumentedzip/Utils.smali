.class public Lcom/google/android/instrumentedzip/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public static checkOffsetAndCount(III)V
    .locals 1
    .param p0, "arrayLength"    # I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    .line 37
    or-int v0, p1, p2

    if-ltz v0, :cond_0

    if-gt p1, p0, :cond_0

    sub-int v0, p0, p1

    if-ge v0, p2, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 40
    :cond_1
    return-void
.end method

.method public static readFully(Ljava/io/InputStream;[BII)V
    .locals 3
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "dst"    # [B
    .param p2, "offset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    if-nez p3, :cond_1

    .line 87
    :cond_0
    return-void

    .line 72
    :cond_1
    if-nez p0, :cond_2

    .line 73
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "in == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 75
    :cond_2
    if-nez p1, :cond_3

    .line 76
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "dst == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_3
    array-length v1, p1

    invoke-static {v1, p2, p3}, Lcom/google/android/instrumentedzip/Utils;->checkOffsetAndCount(III)V

    .line 79
    :goto_0
    if-lez p3, :cond_0

    .line 80
    invoke-virtual {p0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 81
    .local v0, "bytesRead":I
    if-gez v0, :cond_4

    .line 82
    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1

    .line 84
    :cond_4
    add-int/2addr p2, v0

    .line 85
    sub-int/2addr p3, v0

    .line 86
    goto :goto_0
.end method

.method public static skipFully(Ljava/io/InputStream;I)V
    .locals 4
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    if-nez p1, :cond_2

    .line 103
    :cond_0
    return-void

    .line 101
    .local v0, "bytesSkipped":J
    :cond_1
    int-to-long v2, p1

    sub-long/2addr v2, v0

    long-to-int p1, v2

    .line 96
    .end local v0    # "bytesSkipped":J
    :cond_2
    if-lez p1, :cond_0

    .line 97
    int-to-long v2, p1

    invoke-virtual {p0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 98
    .restart local v0    # "bytesSkipped":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 99
    new-instance v2, Ljava/io/EOFException;

    invoke-direct {v2}, Ljava/io/EOFException;-><init>()V

    throw v2
.end method
