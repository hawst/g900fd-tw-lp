.class Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
.super Ljava/io/InputStream;
.source "ZipFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/instrumentedzip/ZipFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RAFStream"
.end annotation


# instance fields
.field private endOffset:J

.field private offset:J

.field private final sharedRaf:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;J)V
    .locals 2
    .param p1, "raf"    # Ljava/io/RandomAccessFile;
    .param p2, "initialOffset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 380
    iput-object p1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    .line 381
    iput-wide p2, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    .line 382
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    .line 383
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/instrumentedzip/ZipFile$RAFStream;
    .param p1, "x1"    # J

    .prologue
    .line 374
    iput-wide p1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/google/android/instrumentedzip/ZipFile$RAFStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/instrumentedzip/ZipFile$RAFStream;

    .prologue
    .line 374
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    return-wide v0
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    iget-wide v2, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 390
    iget-object v2, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    monitor-enter v2

    .line 391
    :try_start_0
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    iget-wide v4, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    invoke-virtual {v1, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 392
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->read()I

    move-result v0

    .line 393
    .local v0, "value":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 394
    iget-wide v4, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    .line 396
    :cond_0
    monitor-exit v2

    return v0

    .line 397
    .end local v0    # "value":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public read([BII)I
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "byteOffset"    # I
    .param p3, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    iget-object v4, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    monitor-enter v4

    .line 402
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    iget-wide v8, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    sub-long v2, v6, v8

    .line 403
    .local v2, "length":J
    int-to-long v6, p3

    cmp-long v1, v6, v2

    if-lez v1, :cond_0

    .line 404
    long-to-int p3, v2

    .line 406
    :cond_0
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    iget-wide v6, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 407
    iget-object v1, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->sharedRaf:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    .line 408
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 409
    iget-wide v6, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    int-to-long v8, v0

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    .line 410
    monitor-exit v4

    .line 412
    .end local v0    # "count":I
    :goto_0
    return v0

    .restart local v0    # "count":I
    :cond_1
    const/4 v0, -0x1

    monitor-exit v4

    goto :goto_0

    .line 414
    .end local v0    # "count":I
    .end local v2    # "length":J
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public skip(J)J
    .locals 5
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    iget-wide v2, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 419
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->endOffset:J

    iget-wide v2, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    sub-long p1, v0, v2

    .line 421
    :cond_0
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/instrumentedzip/ZipFile$RAFStream;->offset:J

    .line 422
    return-wide p1
.end method
