.class public Lcom/google/android/instrumentedzip/ZipEntry;
.super Ljava/lang/Object;
.source "ZipEntry.java"


# instance fields
.field compressedSize:J

.field compressionMethod:I

.field localHeaderRelOffset:J

.field name:Ljava/lang/String;

.field nameLength:I

.field size:J

.field public verificationErrors:I


# direct methods
.method constructor <init>([BLjava/io/InputStream;)V
    .locals 12
    .param p1, "cdeHdrBuf"    # [B
    .param p2, "cdStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->compressedSize:J

    .line 38
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->size:J

    .line 40
    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->compressionMethod:I

    .line 42
    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->nameLength:I

    .line 43
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->localHeaderRelOffset:J

    .line 139
    const/4 v7, 0x0

    array-length v8, p1

    invoke-static {p2, p1, v7, v8}, Lcom/google/android/instrumentedzip/Utils;->readFully(Ljava/io/InputStream;[BII)V

    .line 141
    new-instance v4, Lcom/google/android/instrumentedzip/HeapBufferIterator;

    const/4 v7, 0x0

    invoke-direct {v4, p1, v7}, Lcom/google/android/instrumentedzip/HeapBufferIterator;-><init>([BI)V

    .line 143
    .local v4, "it":Lcom/google/android/instrumentedzip/HeapBufferIterator;
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    move-result v6

    .line 144
    .local v6, "sig":I
    int-to-long v8, v6

    const-wide/32 v10, 0x2014b50    # 1.6619997E-316

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 145
    const-string v7, "Central Directory Entry"

    invoke-static {v7, v6}, Lcom/google/android/instrumentedzip/ZipFile;->throwZipException(Ljava/lang/String;I)V

    .line 148
    :cond_0
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->seek(I)V

    .line 149
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v7

    const v8, 0xffff

    and-int v3, v7, v8

    .line 151
    .local v3, "gpbf":I
    and-int/lit8 v7, v3, 0x1

    if-eqz v7, :cond_1

    .line 152
    new-instance v7, Ljava/util/zip/ZipException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid General Purpose Bit Flag: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 155
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v7

    const v8, 0xffff

    and-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->compressionMethod:I

    .line 156
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    .line 157
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    .line 159
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    .line 161
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    move-result v7

    int-to-long v8, v7

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->compressedSize:J

    .line 162
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    move-result v7

    int-to-long v8, v7

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->size:J

    .line 164
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v7

    const v8, 0xffff

    and-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->nameLength:I

    .line 165
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v7

    const v8, 0xffff

    and-int v2, v7, v8

    .line 166
    .local v2, "extraLength":I
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readShort()S

    move-result v7

    const v8, 0xffff

    and-int v0, v7, v8

    .line 168
    .local v0, "commentByteCount":I
    const v7, 0x8000

    if-lt v2, v7, :cond_2

    .line 169
    iget v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 172
    :cond_2
    const v7, 0x8000

    if-lt v0, v7, :cond_3

    .line 173
    iget v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    or-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 177
    :cond_3
    const/16 v7, 0x2a

    invoke-virtual {v4, v7}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->seek(I)V

    .line 178
    invoke-virtual {v4}, Lcom/google/android/instrumentedzip/HeapBufferIterator;->readInt()I

    move-result v7

    int-to-long v8, v7

    const-wide v10, 0xffffffffL

    and-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/android/instrumentedzip/ZipEntry;->localHeaderRelOffset:J

    .line 180
    iget v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->nameLength:I

    new-array v5, v7, [B

    .line 181
    .local v5, "nameBytes":[B
    const/4 v7, 0x0

    array-length v8, v5

    invoke-static {p2, v5, v7, v8}, Lcom/google/android/instrumentedzip/Utils;->readFully(Ljava/io/InputStream;[BII)V

    .line 182
    invoke-static {v5}, Lcom/google/android/instrumentedzip/ZipEntry;->containsNulByte([B)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 183
    iget v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    or-int/lit8 v7, v7, 0x10

    iput v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->verificationErrors:I

    .line 186
    :cond_4
    :try_start_0
    new-instance v7, Ljava/lang/String;

    const/4 v8, 0x0

    array-length v9, v5

    const-string v10, "UTF-8"

    invoke-direct {v7, v5, v8, v9, v10}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iput-object v7, p0, Lcom/google/android/instrumentedzip/ZipEntry;->name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    if-lez v2, :cond_5

    .line 193
    invoke-static {p2, v2}, Lcom/google/android/instrumentedzip/Utils;->skipFully(Ljava/io/InputStream;I)V

    .line 198
    :cond_5
    if-lez v0, :cond_6

    .line 199
    invoke-static {p2, v0}, Lcom/google/android/instrumentedzip/Utils;->skipFully(Ljava/io/InputStream;I)V

    .line 201
    :cond_6
    return-void

    .line 187
    :catch_0
    move-exception v1

    .line 189
    .local v1, "ex":Ljava/io/UnsupportedEncodingException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7
.end method

.method private static containsNulByte([B)Z
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    .line 204
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-byte v1, v0, v2

    .line 205
    .local v1, "b":B
    if-nez v1, :cond_0

    .line 206
    const/4 v4, 0x1

    .line 209
    .end local v1    # "b":B
    :goto_1
    return v4

    .line 204
    .restart local v1    # "b":B
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 209
    .end local v1    # "b":B
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipEntry;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/google/android/instrumentedzip/ZipEntry;->size:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipEntry;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/instrumentedzip/ZipEntry;->name:Ljava/lang/String;

    return-object v0
.end method
