.class public final Lcom/google/android/instrumentedzip/HeapBufferIterator;
.super Ljava/lang/Object;
.source "HeapBufferIterator.java"


# instance fields
.field private final buffer:[B

.field private final offset:I

.field private position:I


# direct methods
.method constructor <init>([BI)V
    .locals 0
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    .line 30
    iput p2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->offset:I

    .line 31
    return-void
.end method


# virtual methods
.method public readInt()I
    .locals 4

    .prologue
    .line 42
    iget v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->offset:I

    iget v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    add-int v0, v2, v3

    .line 43
    .local v0, "readOffset":I
    iget v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    .line 44
    iget-object v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "readOffset":I
    .local v1, "readOffset":I
    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x0

    iget-object v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "readOffset":I
    .restart local v0    # "readOffset":I
    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "readOffset":I
    .restart local v1    # "readOffset":I
    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    or-int/2addr v2, v3

    return v2
.end method

.method public readShort()S
    .locals 4

    .prologue
    .line 51
    iget v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->offset:I

    iget v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    add-int v0, v2, v3

    .line 52
    .local v0, "readOffset":I
    iget v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    .line 53
    iget-object v2, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "readOffset":I
    .local v1, "readOffset":I
    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x0

    iget-object v3, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->buffer:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    int-to-short v2, v2

    return v2
.end method

.method public seek(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    .line 35
    return-void
.end method

.method public skip(I)V
    .locals 1
    .param p1, "byteCount"    # I

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/instrumentedzip/HeapBufferIterator;->position:I

    .line 39
    return-void
.end method
