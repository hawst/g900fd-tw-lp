.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalities()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

.field final synthetic val$adminArea:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;)V
    .locals 0

    .prologue
    .line 1789
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->val$adminArea:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1789
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "adminAreaData"    # Lorg/json/JSONObject;

    .prologue
    .line 1792
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->val$adminArea:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    iput-object p1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mData:Lorg/json/JSONObject;

    .line 1793
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;
    invoke-static {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$802(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 1794
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalityInput()V
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$900(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    .line 1796
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$1002(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;)Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    .line 1797
    return-void
.end method
