.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "AddCreditCardFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.implements Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;
    }
.end annotation


# instance fields
.field mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

.field mAddressEntryFragmentHolder:Landroid/view/View;

.field mAnimatedChildren:[Landroid/view/View;

.field private mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

.field private mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

.field mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

.field mCreditCardNumberConcealedText:Landroid/widget/TextView;

.field mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

.field private mCreditCardNumberTextWatcher:Landroid/text/TextWatcher;

.field mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

.field mCvcHintImage:Landroid/widget/ImageView;

.field mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field mExpCvcLayout:Landroid/view/View;

.field private mExpDateTextWatcher:Landroid/text/TextWatcher;

.field mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

.field mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

.field mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field mExpandCreditCardIcon:Landroid/view/View;

.field private mLaunchOcrIntent:Landroid/content/Intent;

.field private mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

.field mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

.field mOcrIcon:Landroid/view/View;

.field private mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

.field mRoot:Landroid/widget/RelativeLayout;

.field mSelectedRegionCode:I

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

.field private mViewState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    .line 140
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x672

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mSelectedRegionCode:I

    .line 1231
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    return v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getStateAfterEnteringCardNumberCompleted()I

    move-result v0

    return v0
.end method

.method private static getStateAfterEnteringCardNumberCompleted()I
    .locals 2

    .prologue
    .line 374
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private isOcrEnabled()Z
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLaunchOcrIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
    .locals 4
    .param p0, "creditCardForm"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 198
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;-><init>()V

    .line 200
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 201
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "creditCardForm"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 202
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setArguments(Landroid/os/Bundle;)V

    .line 204
    return-object v1
.end method

.method private static ocrResultCodeToExitReason(I)I
    .locals 1
    .param p0, "resultCode"    # I

    .prologue
    .line 1208
    sparse-switch p0, :sswitch_data_0

    .line 1223
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1210
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1213
    :sswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 1216
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1221
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1208
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x2711 -> :sswitch_2
        0x2713 -> :sswitch_3
        0x2714 -> :sswitch_2
        0x2715 -> :sswitch_3
        0x2716 -> :sswitch_3
        0x2717 -> :sswitch_1
        0x2719 -> :sswitch_3
    .end sparse-switch
.end method

.method private sendCreditCardEntryActionBackgroundEvent()V
    .locals 3

    .prologue
    .line 1199
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1200
    .local v0, "eventDetails":Landroid/os/Bundle;
    const-string v1, "FormEventListener.EXTRA_BACKGROUND_EVENT_TYPE"

    const/16 v2, 0x302

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1202
    const-string v1, "FormEventListener.EXTRA_BACKGROUND_EVENT_DATA"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1204
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 1205
    return-void
.end method

.method private setAnimationDelay(J)V
    .locals 5
    .param p1, "startDelay"    # J

    .prologue
    .line 709
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    .line 710
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAnimatedChildren:[Landroid/view/View;

    array-length v1, v2

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 711
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAnimatedChildren:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 710
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 714
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_0
    return-void
.end method

.method private switchToExpandedState(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    const/16 v5, 0x8

    const/4 v9, 0x3

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 628
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_0

    .line 629
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can not switch ui to expanded state for api level: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 633
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->updateRelativeLayoutParamsToShowExpandedCardView()V

    .line 634
    if-eqz p1, :cond_4

    .line 635
    const-wide/16 v4, 0x96

    invoke-direct {p0, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setAnimationDelay(J)V

    .line 637
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    invoke-static {v3, v6, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    .line 638
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    invoke-static {v3, v6, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    .line 639
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-static {v3, v6, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 641
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 642
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-static {v3, v6, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 661
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 663
    .local v2, "expCvcLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 664
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getId()I

    move-result v3

    invoke-virtual {v2, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 665
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->getViewHeightWithMargins(Landroid/view/View;)I

    move-result v3

    neg-int v1, v3

    .line 666
    .local v1, "deltaY":I
    if-eqz p1, :cond_5

    .line 667
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-static {v3, v1, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 678
    :goto_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 680
    .local v0, "addressEntryFragmentHolderLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v0, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 682
    if-eqz p1, :cond_3

    .line 683
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 684
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 685
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setTranslationY(F)V

    .line 686
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 687
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v3, :cond_2

    .line 688
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const-wide/16 v4, 0x96

    invoke-interface {v3, v1, v6, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->animateViewsBelow(IIJ)V

    .line 693
    :cond_2
    const-wide/16 v4, 0x0

    invoke-direct {p0, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setAnimationDelay(J)V

    .line 695
    :cond_3
    return-void

    .line 646
    .end local v0    # "addressEntryFragmentHolderLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "deltaY":I
    .end local v2    # "expCvcLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 647
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 648
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3, v6}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setVisibility(I)V

    .line 650
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3, v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setTranslationY(F)V

    .line 651
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3, v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setAlpha(F)V

    .line 652
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 653
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 655
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 656
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_0

    .line 670
    .restart local v1    # "deltaY":I
    .restart local v2    # "expCvcLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 672
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 673
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_1
.end method

.method private switchToShowingAddress(Z)V
    .locals 12
    .param p1, "animate"    # Z

    .prologue
    .line 523
    if-nez p1, :cond_2

    .line 524
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 525
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 526
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v1, :cond_0

    .line 527
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    .line 532
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 533
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 534
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setVisibility(I)V

    .line 535
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 536
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 537
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->switchToOneCardMode()V

    .line 538
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 615
    :cond_1
    :goto_0
    return-void

    .line 545
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_3

    .line 546
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 548
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 550
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v1, :cond_1

    .line 551
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    goto :goto_0

    .line 556
    :cond_3
    const-wide/16 v2, 0x96

    invoke-direct {p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setAnimationDelay(J)V

    .line 558
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->getViewHeightWithMargins(Landroid/view/View;)I

    move-result v8

    .line 560
    .local v8, "creditCardImagesHeight":I
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->getViewHeightWithMargins(Landroid/view/View;)I

    move-result v9

    .line 562
    .local v9, "creditCardNumberHeight":I
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->getViewHeightWithMargins(Landroid/view/View;)I

    move-result v10

    .line 564
    .local v10, "expCvcHeight":I
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->switchToOneCardMode()V

    .line 568
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    const/4 v2, 0x0

    neg-int v3, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    .line 570
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 571
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/4 v2, 0x0

    neg-int v3, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    .line 578
    :cond_4
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-static {v1, v8, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 581
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v1, v8, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 586
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 588
    .local v11, "expCvcLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 589
    const/16 v1, 0xa

    invoke-virtual {v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 590
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    add-int v2, v8, v9

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    .line 595
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 597
    .local v0, "addressEntryFragmentHolderLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 599
    add-int v4, v9, v10

    .line 600
    .local v4, "addressAndLegalStartDeltaY":I
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v1, v4, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 603
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v2, 0x0

    invoke-static {v1, v4, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 605
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v1, :cond_5

    .line 606
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v5, 0x0

    const-wide/16 v6, 0x96

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    .line 613
    :cond_5
    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setAnimationDelay(J)V

    goto/16 :goto_0
.end method

.method private transitionToState(IZ)V
    .locals 8
    .param p1, "newState"    # I
    .param p2, "allowAnimation"    # Z

    .prologue
    .line 931
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    if-gt p1, v0, :cond_0

    .line 1053
    :goto_0
    return-void

    .line 937
    :cond_0
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    if-nez v0, :cond_7

    .line 938
    packed-switch p1, :pswitch_data_0

    .line 1045
    :cond_1
    :goto_1
    const/4 v0, 0x4

    if-lt p1, v0, :cond_2

    .line 1046
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_recipient:I

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setNextFocusDownId(I)V

    .line 1049
    :cond_2
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    .line 1052
    const/4 v0, 0x1

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 940
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 941
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 944
    :cond_3
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 947
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setNextFocusDownId(I)V

    .line 948
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$4;

    invoke-direct {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$4;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setAutoAdvancedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;)V

    .line 956
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v0, :cond_1

    .line 957
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    goto :goto_1

    .line 963
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 964
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 966
    :cond_4
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 967
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 968
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v0, :cond_1

    .line 969
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    goto :goto_1

    .line 975
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 976
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 978
    :cond_5
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v0, :cond_1

    .line 979
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    goto/16 :goto_1

    .line 987
    :pswitch_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 989
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 991
    :cond_6
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->switchToShowingAddress(Z)V

    goto/16 :goto_1

    .line 994
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 995
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 996
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->switchToExpandedState(Z)V

    .line 997
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    if-eqz v0, :cond_1

    .line 998
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;->showViewsBelow(ZZIIJ)V

    goto/16 :goto_1

    .line 1003
    :cond_7
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    const/4 v0, 0x3

    if-eq p1, v0, :cond_8

    const/4 v0, 0x2

    if-ne p1, v0, :cond_b

    .line 1009
    :cond_8
    const/4 v0, 0x2

    if-ne p1, v0, :cond_9

    .line 1010
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1012
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1014
    :cond_9
    if-eqz p2, :cond_a

    .line 1015
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getHeight()I

    move-result v1

    neg-int v1, v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 1021
    :goto_2
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_month:I

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setNextFocusDownId(I)V

    goto/16 :goto_1

    .line 1019
    :cond_a
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 1022
    :cond_b
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 1026
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    .line 1027
    invoke-direct {p0, p2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->switchToExpandedState(Z)V

    goto/16 :goto_1

    .line 1029
    :cond_c
    const/4 v0, 0x4

    if-ne p1, v0, :cond_d

    .line 1031
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getConcealedCardNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1032
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->switchToShowingAddress(Z)V

    goto/16 :goto_1

    .line 1033
    :cond_d
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 1036
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1037
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    goto/16 :goto_1

    .line 938
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateRelativeLayoutParamsToShowExpandedCardView()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 722
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 724
    .local v0, "cardNumberParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->add_credit_card_title:I

    invoke-virtual {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 726
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_credit_card_number_collapsed_left_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 728
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 730
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 731
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 733
    .local v1, "ocrParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->add_credit_card_title:I

    invoke-virtual {v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 734
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 736
    .end local v1    # "ocrParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private validate(Z)Z
    .locals 11
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v10, 0x5

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 237
    new-array v2, v10, [Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    aput-object v8, v2, v7

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    aput-object v8, v2, v6

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    aput-object v9, v2, v8

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    aput-object v9, v2, v8

    const/4 v8, 0x4

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    aput-object v9, v2, v8

    .line 244
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    const/4 v5, 0x1

    .line 245
    .local v5, "valid":Z
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 246
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    if-eqz p1, :cond_2

    .line 247
    invoke-interface {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v5, :cond_1

    move v5, v6

    .line 245
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v5, v7

    .line 247
    goto :goto_1

    .line 248
    :cond_2
    invoke-interface {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v8

    if-nez v8, :cond_0

    .line 260
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    :goto_2
    return v7

    .line 254
    :cond_3
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xe

    if-lt v6, v8, :cond_4

    if-eqz p1, :cond_4

    if-nez v5, :cond_4

    .line 258
    invoke-direct {p0, v10, v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    :cond_4
    move v7, v5

    .line 260
    goto :goto_2
.end method


# virtual methods
.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 3
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    const/4 v0, 0x1

    .line 284
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 285
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    packed-switch v1, :pswitch_data_0

    .line 311
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown FormFieldMessage fieldId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setError(Ljava/lang/CharSequence;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget-boolean v1, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    if-nez v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v0, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 290
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    .line 315
    :cond_0
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 318
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    .line 322
    :cond_1
    :goto_1
    return v0

    .line 294
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 297
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget-boolean v1, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    if-nez v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v0, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 300
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    goto :goto_0

    .line 304
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget-boolean v1, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    if-nez v1, :cond_0

    .line 306
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v0, v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 307
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    goto :goto_0

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z

    move-result v0

    goto :goto_1

    .line 285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public doEnableUi()V
    .locals 2

    .prologue
    .line 335
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    if-eqz v1, :cond_0

    .line 336
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isUiEnabled()Z

    move-result v0

    .line 337
    .local v0, "uiEnabled":Z
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setEnabled(Z)V

    .line 338
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 339
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 340
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 341
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 342
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 343
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->enableUi(Z)V

    .line 344
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 345
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setEnabled(Z)V

    .line 347
    .end local v0    # "uiEnabled":Z
    :cond_0
    return-void
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 266
    const/4 v6, 0x4

    new-array v2, v6, [Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    aput-object v7, v2, v6

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v6, v2, v5

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v6

    .line 273
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 274
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 275
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    .line 279
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_1
    return v5

    .line 273
    .restart local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 279
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_1
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->focusOnFirstInvalidFormField()Z

    move-result v5

    goto :goto_1
.end method

.method public getButtonBarExpandButtonText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getExpandLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChildren()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1155
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;>;"
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1156
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;

    const/16 v2, 0x674

    invoke-direct {v1, v2, p0}, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;-><init>(ILcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1159
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v1, :cond_1

    .line 1160
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1163
    :cond_1
    return-object v0
.end method

.method public getCreditCardFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
    .locals 10

    .prologue
    .line 1079
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCardNumber()Ljava/lang/String;

    move-result-object v1

    .line 1080
    .local v1, "ccNumber":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->removeNonNumericDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1081
    .local v3, "cvc":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x4

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1082
    .local v7, "lastFourDigits":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCardType()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v0

    .line 1085
    .local v0, "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 1091
    .local v5, "expMonth":I
    :goto_0
    :try_start_1
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v8

    add-int/lit16 v6, v8, 0x7d0

    .line 1096
    .local v6, "expYear":I
    :goto_1
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;-><init>()V

    .line 1097
    .local v2, "creditCard":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
    iput v5, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationMonth:I

    .line 1098
    iput v6, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->expirationYear:I

    .line 1099
    if-eqz v0, :cond_0

    .line 1100
    iget-object v8, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    iput-object v8, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->typeToken:[B

    .line 1102
    :cond_0
    iput-object v7, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->lastFourDigits:Ljava/lang/String;

    .line 1104
    iput-object v1, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    .line 1105
    iput-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    .line 1106
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    move-result-object v8

    iput-object v8, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    .line 1108
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v8, :cond_1

    .line 1109
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    iget-object v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->opaqueData:Ljava/lang/String;

    iput-object v8, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->legalDocData:Ljava/lang/String;

    .line 1111
    :cond_1
    return-object v2

    .line 1086
    .end local v2    # "creditCard":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;
    .end local v5    # "expMonth":I
    .end local v6    # "expYear":I
    :catch_0
    move-exception v4

    .line 1087
    .local v4, "ex":Ljava/lang/NumberFormatException;
    const/4 v5, 0x0

    .restart local v5    # "expMonth":I
    goto :goto_0

    .line 1092
    .end local v4    # "ex":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v4

    .line 1093
    .restart local v4    # "ex":Ljava/lang/NumberFormatException;
    const/4 v6, 0x0

    .restart local v6    # "expYear":I
    goto :goto_1
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public handleErrorMessageDismissed(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "errorType"    # I

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->handleErrorMessageDismissed(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isReadyToSubmit()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 351
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isReadyToSubmit()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    packed-switch v1, :pswitch_data_0

    .line 361
    :cond_0
    :goto_0
    return v0

    .line 356
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 353
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 793
    const/16 v8, 0x1f4

    if-eq p1, v8, :cond_0

    .line 794
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 869
    :goto_0
    return-void

    .line 797
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->enableUi(Z)V

    .line 799
    invoke-static {p3}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->fromIntent(Landroid/content/Intent;)Lcom/google/android/gms/ocr/CreditCardOcrResult;

    move-result-object v4

    .line 800
    .local v4, "ocrResult":Lcom/google/android/gms/ocr/CreditCardOcrResult;
    const/4 v5, 0x0

    .line 801
    .local v5, "validCcNumber":Z
    const/4 v6, 0x0

    .line 802
    .local v6, "validExpMonth":Z
    const/4 v7, 0x0

    .line 803
    .local v7, "validExpYear":Z
    if-eqz v4, :cond_3

    .line 805
    const/4 v3, -0x1

    .line 806
    .local v3, "newState":I
    invoke-virtual {v4}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->getCardNumber()Ljava/lang/String;

    move-result-object v0

    .line 807
    .local v0, "ccNumber":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->getExpMonth()I

    move-result v1

    .line 809
    .local v1, "expMonth":I
    invoke-virtual {v4}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->getExpYear()I

    move-result v8

    rem-int/lit8 v2, v8, 0x64

    .line 810
    .local v2, "expYear":I
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    if-eqz v8, :cond_7

    const/4 v5, 0x1

    .line 811
    :goto_1
    if-lez v1, :cond_8

    const/16 v8, 0xd

    if-ge v1, v8, :cond_8

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v8, :cond_8

    const/4 v6, 0x1

    .line 812
    :goto_2
    if-ltz v2, :cond_9

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v8, :cond_9

    const/4 v7, 0x1

    .line 813
    :goto_3
    if-eqz v5, :cond_3

    .line 814
    iget v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 815
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getStateAfterEnteringCardNumberCompleted()I

    move-result v3

    .line 819
    :cond_1
    if-eqz v6, :cond_2

    if-eqz v7, :cond_2

    .line 820
    const/4 v3, 0x3

    .line 825
    :cond_2
    const/4 v8, 0x0

    invoke-direct {p0, v3, v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    .line 828
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->requestFocus()Z

    .line 830
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 831
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v8, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setText(Ljava/lang/CharSequence;)V

    .line 832
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 834
    if-eqz v6, :cond_3

    if-eqz v7, :cond_3

    .line 837
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 838
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 839
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 841
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->requestFocus()Z

    .line 842
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 843
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 844
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 849
    .end local v0    # "ccNumber":Ljava/lang/String;
    .end local v1    # "expMonth":I
    .end local v2    # "expYear":I
    .end local v3    # "newState":I
    :cond_3
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget v8, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    if-gez v8, :cond_a

    .line 850
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    const/4 v9, 0x1

    iput v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    .line 854
    :goto_4
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->ocrResultCodeToExitReason(I)I

    move-result v9

    iput v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->ocrExitReason:I

    .line 855
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v5, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panRecognizedByOcr:Z

    .line 856
    if-eqz v5, :cond_4

    .line 857
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    const/4 v9, 0x2

    iput v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panEntryType:I

    .line 859
    :cond_4
    if-eqz p3, :cond_5

    .line 860
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    const-string v9, "com.google.android.gms.ocr.EXP_DATE_RECOGNITION_ENABLED"

    const/4 v10, 0x0

    invoke-virtual {p3, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateOcrEnabled:Z

    .line 863
    :cond_5
    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    if-eqz v6, :cond_b

    if-eqz v7, :cond_b

    const/4 v8, 0x1

    :goto_5
    iput-boolean v8, v9, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateRecognizedByOcr:Z

    .line 864
    if-eqz v6, :cond_6

    if-eqz v7, :cond_6

    .line 865
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    const/4 v9, 0x2

    iput v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    .line 868
    :cond_6
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    goto/16 :goto_0

    .line 810
    .restart local v0    # "ccNumber":Ljava/lang/String;
    .restart local v1    # "expMonth":I
    .restart local v2    # "expYear":I
    .restart local v3    # "newState":I
    :cond_7
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 811
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 812
    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 852
    .end local v0    # "ccNumber":Ljava/lang/String;
    .end local v1    # "expMonth":I
    .end local v2    # "expYear":I
    .end local v3    # "newState":I
    :cond_a
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    goto :goto_4

    .line 863
    :cond_b
    const/4 v8, 0x0

    goto :goto_5
.end method

.method public onButtonBarExpandButtonClicked()V
    .locals 2

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->expand(Z)V

    .line 1174
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 749
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->expand_icon:I

    if-ne v2, v3, :cond_1

    .line 750
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 751
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->ocr_icon:I

    if-ne v2, v3, :cond_3

    .line 752
    const/16 v2, 0x674

    invoke-static {p0, v2}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 760
    move-object v0, p0

    .line 761
    .local v0, "current":Landroid/support/v4/app/Fragment;
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 762
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_1

    .line 765
    :cond_2
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;

    const/16 v3, 0x675

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;-><init>(ILcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 769
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLaunchOcrIntent:Landroid/content/Intent;

    const/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 770
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->enableUi(Z)V

    goto :goto_0

    .line 771
    .end local v0    # "current":Landroid/support/v4/app/Fragment;
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->cvc_hint:I

    if-ne v2, v3, :cond_0

    .line 773
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "tagCvcInfoDialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 776
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getThemeResourceId()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->newInstance(I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;

    move-result-object v1

    .line 778
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "tagCvcInfoDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 1183
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-ne p1, v1, :cond_0

    .line 1185
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return-void

    .line 1188
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {p2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    move-result-object v0

    .line 1190
    .local v0, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 380
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 382
    if-eqz p1, :cond_0

    .line 383
    const-string v3, "selectedRegionCode"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mSelectedRegionCode:I

    .line 386
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 387
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "creditCardForm"

    invoke-static {v0, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    check-cast v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    .line 390
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-boolean v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowCameraInput:Z

    if-eqz v3, :cond_1

    .line 391
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [I

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$attr;->ccOcrTheme:I

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/view/ContextThemeWrapper;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 393
    .local v1, "attrs":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 394
    .local v2, "ccOcrTheme":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 397
    new-instance v3, Lcom/google/android/gms/ocr/CreditCardOcrIntentBuilder;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/ocr/CreditCardOcrIntentBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/ocr/CreditCardOcrIntentBuilder;->setTheme(I)Lcom/google/android/gms/ocr/CreditCardOcrIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/ocr/CreditCardOcrIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLaunchOcrIntent:Landroid/content/Intent;

    .line 402
    .end local v1    # "attrs":Landroid/content/res/TypedArray;
    .end local v2    # "ccOcrTheme":I
    :cond_1
    if-eqz p1, :cond_2

    .line 403
    const-string v3, "creditCardEntryAction"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 406
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    if-nez v3, :cond_3

    .line 407
    new-instance v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    invoke-direct {v3}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;-><init>()V

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 408
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->isOcrEnabled()Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panOcrEnabled:Z

    .line 410
    :cond_3
    return-void
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 415
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_add_credit_card:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 417
    .local v9, "content":Landroid/view/View;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->credit_card_root:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mRoot:Landroid/widget/RelativeLayout;

    .line 419
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mRoot:Landroid/widget/RelativeLayout;

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->add_credit_card_title:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 421
    .local v11, "titleText":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->title:Ljava/lang/String;

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 425
    .end local v11    # "titleText":Landroid/widget/TextView;
    :cond_0
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->credit_card_images:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    .line 427
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->setCardTypes([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 428
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->card_number_concealed:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    .line 430
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->card_number:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .line 431
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_date_and_cvc:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    .line 432
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_month:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 433
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_year:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 434
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->cvc:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 435
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 437
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->cvc_hint:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    .line 438
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcHintImage:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->ocr_icon:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    .line 441
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->expand_icon:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    .line 444
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->add_card_legal_message_text:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    .line 448
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setParentUiNode(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 449
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setUrlClickListener(Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    .line 451
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    const/4 v5, 0x4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;-><init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;I)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    .line 453
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationMonth:I

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->minExpirationYear:I

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget v6, v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationMonth:I

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget v7, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->maxExpirationYear:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;-><init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;IIII)V

    .line 457
    .local v0, "expDateChecker":Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    .line 458
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {v1, v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    .line 460
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 461
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 462
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 463
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 466
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 467
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 468
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 470
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 472
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    new-instance v2, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setAutoAdvancedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;)V

    .line 480
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setOnCreditCardTypeChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;)V

    .line 481
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->allowedCardType:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setAllowedCardTypes([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 482
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->invalidBin:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setInvalidBins([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;)V

    .line 484
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_fragment_holder:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    .line 487
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_fragment_holder:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .line 489
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    if-nez v1, :cond_1

    .line 490
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v8, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->billingAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 492
    .local v8, "addressForm":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v8, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;I)Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .line 495
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_fragment_holder:I

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 498
    .end local v8    # "addressForm":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setOnRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 499
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragment:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setOnHeightOffsetChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;)V

    .line 501
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->doEnableUi()V

    .line 503
    const/16 v1, 0x9

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mRoot:Landroid/widget/RelativeLayout;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberConcealedText:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpandCreditCardIcon:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOcrIcon:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpCvcLayout:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAnimatedChildren:[Landroid/view/View;

    .line 507
    const/4 v10, 0x1

    .line 508
    .local v10, "state":I
    if-eqz p3, :cond_2

    .line 510
    const-string v1, "viewState"

    const/4 v2, 0x3

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 512
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v10, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    .line 513
    return-object v9

    .line 490
    .end local v10    # "state":I
    :cond_3
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public onCreditCardTypeChanged(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 5
    .param p1, "newType"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 1061
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardImagesView:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;

    invoke-virtual {v1, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->setCreditCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 1063
    if-eqz p1, :cond_1

    iget v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    .line 1067
    .local v0, "cvcLength":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v4, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1068
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1069
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->validate()Z

    .line 1071
    :cond_0
    return-void

    .line 1063
    .end local v0    # "cvcLength":I
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 786
    if-eqz p2, :cond_0

    .line 787
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->transitionToState(IZ)V

    .line 789
    :cond_0
    return-void
.end method

.method public onHeightOffsetChanged(F)V
    .locals 2
    .param p1, "heightOffset"    # F

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mAddressEntryFragmentHolder:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    add-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setTranslationY(F)V

    .line 1144
    return-void
.end method

.method public onRegionCodeSelected(II)V
    .locals 2
    .param p1, "regionCode"    # I
    .param p2, "senderId"    # I

    .prologue
    .line 1116
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mSelectedRegionCode:I

    if-eq v0, p1, :cond_0

    .line 1117
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mSelectedRegionCode:I

    .line 1118
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->findLegalMessageByCountry(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    .line 1120
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V

    .line 1123
    const/4 v0, 0x6

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 1126
    :cond_0
    return-void

    .line 1120
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->messageText:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 740
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 742
    const-string v0, "viewState"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mViewState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 743
    const-string v0, "selectedRegionCode"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mSelectedRegionCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 744
    const-string v0, "creditCardEntryAction"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 745
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 873
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 875
    const/4 v0, 0x6

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 880
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberTextWatcher:Landroid/text/TextWatcher;

    .line 896
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 897
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    .line 914
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 915
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpDateTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 916
    return-void
.end method

.method public setOnStateChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    .prologue
    .line 1195
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mOnStateChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;

    .line 1196
    return-void
.end method

.method public shouldShowButtonBarExpandButton()Z
    .locals 1

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validate()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 209
    invoke-direct {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->validate(Z)Z

    move-result v0

    .line 211
    .local v0, "result":Z
    const/4 v1, 0x0

    .line 212
    .local v1, "shouldSendCreditCardEvent":Z
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget-boolean v2, v2, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v3, v2, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 215
    const/4 v1, 0x1

    .line 217
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iget-boolean v2, v2, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 220
    :cond_1
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    iput-boolean v3, v2, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 221
    const/4 v1, 0x1

    .line 223
    :cond_2
    if-eqz v1, :cond_3

    .line 224
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V

    .line 227
    :cond_3
    return v0
.end method
