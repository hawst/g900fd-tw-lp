.class final Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult$1;
.super Ljava/lang/Object;
.source "AddressSourceResult.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;)I
    .locals 5
    .param p1, "lhs"    # Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    .param p2, "rhs"    # Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .prologue
    .line 31
    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 33
    .local v1, "matchingTerm":I
    if-eqz v1, :cond_0

    .line 45
    .end local v1    # "matchingTerm":I
    :goto_0
    return v1

    .line 37
    .restart local v1    # "matchingTerm":I
    :cond_0
    iget-object v2, p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    iget-object v2, p2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    .line 38
    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    iget-object v3, p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 40
    .local v0, "description":I
    if-eqz v0, :cond_1

    move v1, v0

    .line 41
    goto :goto_0

    .line 45
    .end local v0    # "description":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult$1;->compare(Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;)I

    move-result v0

    return v0
.end method
