.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;
.super Landroid/widget/Filter;
.source "AddressSourceResultAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AddressSourceResultFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method private containsAllRemainingRequiredFields(Lcom/google/location/country/Postaladdress$PostalAddress;)Z
    .locals 7
    .param p1, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    const/4 v5, 0x1

    .line 304
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C
    invoke-static {v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)[C

    move-result-object v6

    if-nez v6, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v5

    .line 307
    :cond_1
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C
    invoke-static {v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    .line 309
    .local v1, "field":C
    const/16 v6, 0x41

    if-ne v1, v6, :cond_2

    .line 310
    const/16 v1, 0x31

    .line 312
    :cond_2
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iget-object v6, v6, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRemainingFields:[C

    invoke-static {v6, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([CC)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 313
    invoke-static {p1, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v4

    .line 314
    .local v4, "value":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 315
    const/4 v5, 0x0

    goto :goto_0

    .line 307
    .end local v4    # "value":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private isBadAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)Z
    .locals 8
    .param p1, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    const/4 v6, 0x0

    .line 275
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)[C

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v7, p1, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v7, v7

    if-nez v7, :cond_2

    :cond_0
    move v4, v6

    .line 299
    :cond_1
    :goto_0
    return v4

    .line 278
    :cond_2
    const/4 v4, 0x0

    .line 279
    .local v4, "requireFieldOtherThanAddressLineAndRecipient":Z
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget-char v1, v0, v2

    .line 280
    .local v1, "field":C
    sparse-switch v1, :sswitch_data_0

    .line 288
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v7

    if-nez v7, :cond_4

    .line 279
    :cond_3
    :sswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 291
    :cond_4
    const/4 v4, 0x1

    .line 292
    invoke-static {p1, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v5

    .line 293
    .local v5, "value":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    move v4, v6

    .line 294
    goto :goto_0

    .line 280
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x41 -> :sswitch_0
        0x4e -> :sswitch_0
    .end sparse-switch
.end method

.method private processAddressSourceResults(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p2, "sourceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "unprocessedResults":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    const/4 v10, -0x1

    .line 200
    if-nez p1, :cond_1

    .line 201
    const/4 v6, 0x0

    .line 260
    :cond_0
    return-object v6

    .line 204
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v6, "processedResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 209
    .local v3, "matchingTermToPosition":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .line 211
    .local v7, "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    if-eqz v8, :cond_3

    .line 212
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {p0, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->isBadAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 218
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {p0, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->containsAllRemainingRequiredFields(Lcom/google/location/country/Postaladdress$PostalAddress;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 219
    iget-object v2, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    .line 220
    .local v2, "matchingTerm":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 222
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v2, v8}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    .end local v2    # "matchingTerm":Ljava/lang/String;
    :cond_3
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 234
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 238
    iget-object v8, v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 243
    .end local v7    # "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    :cond_4
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 245
    const/4 v4, 0x0

    .line 246
    .local v4, "offset":I
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 247
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 248
    .local v5, "position":I
    if-eq v5, v10, :cond_5

    .line 251
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 254
    .restart local v2    # "matchingTerm":Ljava/lang/String;
    new-instance v7, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    invoke-direct {v7, v2, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .restart local v7    # "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    add-int v8, v5, v4

    invoke-virtual {v6, v8, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 256
    add-int/lit8 v4, v4, 0x1

    .line 257
    goto :goto_1
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "resultValue"    # Ljava/lang/Object;

    .prologue
    .line 357
    instance-of v0, p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    if-eqz v0, :cond_0

    .line 358
    check-cast p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .end local p1    # "resultValue":Ljava/lang/Object;
    iget-object v0, p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    .line 360
    :goto_0
    return-object v0

    .restart local p1    # "resultValue":Ljava/lang/Object;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->performFilteringForValues(Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v0

    .line 329
    .local v0, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 330
    .local v1, "results":Landroid/widget/Filter$FilterResults;
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 331
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 332
    return-object v1
.end method

.method performFilteringForValues(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    if-eqz p1, :cond_1

    .line 171
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressSources:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;

    .line 174
    .local v0, "source":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressField:C
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)C

    move-result v2

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iget-object v3, v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRemainingFields:[C

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRegionCode:I
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)I

    move-result v4

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mLanguageCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->access$300(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;->getAddresses(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 181
    .local v9, "unprocessedResults":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v9, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->processAddressSourceResults(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 183
    .local v8, "processedResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    .end local v0    # "source":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "processedResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    .end local v9    # "unprocessedResults":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    :goto_1
    return-object v8

    .line 177
    .restart local v0    # "source":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
    .restart local v7    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v6

    .line 178
    .local v6, "e":Ljava/lang/Throwable;
    const-string v1, "AddressSourceResultAdap"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not fetch addresses from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 188
    .end local v0    # "source":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
    .end local v6    # "e":Ljava/lang/Throwable;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 338
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->publishResults(Ljava/lang/CharSequence;Ljava/util/ArrayList;)V

    .line 339
    return-void
.end method

.method publishResults(Ljava/lang/CharSequence;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 343
    .local p2, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iput-object p1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mLastPublishedConstraint:Ljava/lang/CharSequence;

    .line 344
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iput-object p2, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    .line 345
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->notifyDataSetChanged()V

    .line 350
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
