.class public Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
.super Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;
.source "InstrumentManagerSidecar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$RefreshPageResponseListener;,
        Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;
    }
.end annotation


# instance fields
.field private mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

.field private mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;-><init>()V

    .line 192
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
    .param p1, "x1"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
    .param p1, "x1"    # Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
    .param p2, "x2"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;
    .param p3, "x3"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->updateStateAndSendAnalyticsEvent(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
    .param p1, "x1"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    return-object p1
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
    .locals 2
    .param p0, "androidConfig"    # Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    .prologue
    .line 40
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;-><init>()V

    .line 41
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->createArgs(Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Landroid/os/Bundle;

    move-result-object v0

    .line 42
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v1
.end method

.method private updateStateAndSendAnalyticsEvent(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;)V
    .locals 9
    .param p2, "responseContext"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;
    .param p3, "error"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest",
            "<*>;",
            "Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;",
            "Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "request":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<*>;"
    const/4 v3, 0x3

    .line 153
    if-nez p3, :cond_0

    .line 154
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setState(II)V

    .line 155
    const/4 v2, 0x0

    .line 173
    .local v2, "analyticsResultCode":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->getBackgroundEventReceivedType()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->getClientLatencyMs()J

    move-result-wide v4

    iget-wide v6, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->responseTimeMillis:J

    iget-object v8, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-static/range {v1 .. v8}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendResponseReceivedBackgroundEvent(IILjava/lang/String;JJ[B)V

    .line 180
    return-void

    .line 158
    .end local v2    # "analyticsResultCode":I
    :cond_0
    iget-object v0, p3, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    const-string v0, "InstrumentManagerSidecar"

    iget-object v1, p3, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->internalDetails:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_1
    iget-object v0, p3, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    const/4 v0, 0x5

    invoke-virtual {p0, v3, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setState(II)V

    .line 163
    const/4 v2, 0x3

    .restart local v2    # "analyticsResultCode":I
    goto :goto_0

    .line 164
    .end local v2    # "analyticsResultCode":I
    :cond_2
    iget v0, p3, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p3, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 166
    const/4 v0, 0x4

    invoke-virtual {p0, v3, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setState(II)V

    .line 167
    const/4 v2, 0x2

    .restart local v2    # "analyticsResultCode":I
    goto :goto_0

    .line 170
    .end local v2    # "analyticsResultCode":I
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No error found in error response"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected clearPreviousResponses()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 146
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .line 147
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    .line 148
    return-void
.end method

.method public getRefreshPageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    return-object v0
.end method

.method public getSavePageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "savePageResponse"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    if-eqz v0, :cond_1

    .line 109
    const-string v0, "refreshPageResponse"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    :cond_1
    return-void
.end method

.method public refreshPage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;)V
    .locals 6
    .param p1, "request"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    .param p2, "previousResponseContext"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .prologue
    .line 82
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v1, :cond_0

    .line 83
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "RefreshPageRequest\'s RequestContext should not be set by the caller"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_0
    new-instance v5, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;

    iget-object v1, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-direct {v5, p0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;-><init>(Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;[B)V

    .line 88
    .local v5, "errorListener":Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureRefreshPageRequest;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    iget-object v3, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$RefreshPageResponseListener;

    invoke-direct {v4, p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$RefreshPageResponseListener;-><init>(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/api/http/SecureRefreshPageRequest;-><init>(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;[BLcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;Lcom/android/volley/Response$ErrorListener;)V

    .line 93
    .local v0, "networkRequest":Lcom/google/android/wallet/instrumentmanager/api/http/SecureRefreshPageRequest;
    invoke-virtual {v5, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->setRequest(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;)V

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->sendRequest(Lcom/android/volley/Request;)V

    .line 95
    const/16 v1, 0x2d2

    iget-object v2, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendRequestSentBackgroundEvent(I[B)V

    .line 98
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 118
    const-string v0, "savePageResponse"

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .line 120
    const-string v0, "refreshPageResponse"

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mRefreshPageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    .line 122
    return-void
.end method

.method public savePage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;)V
    .locals 6
    .param p1, "request"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    .param p2, "previousResponseContext"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .prologue
    .line 55
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    if-eqz v1, :cond_0

    .line 56
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "SavePageRequest\'s RequestContext should not be set by the caller"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 59
    :cond_0
    new-instance v5, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;

    iget-object v1, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-direct {v5, p0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;-><init>(Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;[B)V

    .line 61
    .local v5, "errorListener":Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    iget-object v3, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->sessionData:[B

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;

    invoke-direct {v4, p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;-><init>(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;-><init>(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;[BLcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;Lcom/android/volley/Response$ErrorListener;)V

    .line 66
    .local v0, "networkRequest":Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;
    invoke-virtual {v5, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->setRequest(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;)V

    .line 67
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->sendRequest(Lcom/android/volley/Request;)V

    .line 68
    const/16 v1, 0x2d0

    iget-object v2, p2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendRequestSentBackgroundEvent(I[B)V

    .line 71
    return-void
.end method
