.class public final Lcom/google/android/wallet/instrumentmanager/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final fragment_add_credit_card:I = 0x7f04009b

.field public static final fragment_address_entry:I = 0x7f04009c

.field public static final fragment_credit_card_expiration_date:I = 0x7f04009d

.field public static final fragment_customer:I = 0x7f04009e

.field public static final fragment_instrument_manager:I = 0x7f04009f

.field public static final fragment_tax_info_entry:I = 0x7f0400a0

.field public static final fragment_username_password:I = 0x7f0400a1

.field public static final view_cvc_information:I = 0x7f0401c3

.field public static final view_default_spinner:I = 0x7f0401c4

.field public static final view_form_edit_text:I = 0x7f0401c6

.field public static final view_form_non_editable_text:I = 0x7f0401c7

.field public static final view_row_address_hint_spinner:I = 0x7f0401ca

.field public static final view_row_spinner:I = 0x7f0401cb

.field public static final view_spinner_dropdown:I = 0x7f0401cc

.field public static final view_web_view:I = 0x7f0401ce
