.class Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RegionCodeSelectorSpinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RegionCodeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mDropDownResource:I

.field private final mFieldId:I

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mResource:I


# direct methods
.method public constructor <init>(Landroid/view/ContextThemeWrapper;II[Ljava/lang/Integer;)V
    .locals 1
    .param p1, "context"    # Landroid/view/ContextThemeWrapper;
    .param p2, "resource"    # I
    .param p3, "textViewResourceId"    # I
    .param p4, "objects"    # [Ljava/lang/Integer;

    .prologue
    .line 135
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 136
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mResource:I

    .line 137
    iput p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mFieldId:I

    .line 138
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 139
    return-void
.end method

.method private createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "resource"    # I

    .prologue
    .line 159
    if-nez p2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    invoke-virtual {v2, p4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 162
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 163
    .local v1, "regionCode":Ljava/lang/Integer;
    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mFieldId:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 164
    .local v0, "description":Landroid/widget/TextView;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDisplayCountryForDefaultLocale(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    :goto_0
    return-object p2

    .line 167
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mDropDownResource:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mResource:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setDropDownViewResource(I)V
    .locals 0
    .param p1, "resource"    # I

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 144
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->mDropDownResource:I

    .line 145
    return-void
.end method
