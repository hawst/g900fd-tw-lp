.class public abstract Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;
.super Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
.source "SecurePageRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RequestT:",
        "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest",
        "<TRequestT;TResponseT;>;ResponseT:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest",
        "<",
        "Landroid/util/Pair",
        "<TRequestT;TResponseT;>;>;"
    }
.end annotation


# instance fields
.field protected final mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

.field private final mEesParams:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialized:Z

.field private final mResponseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TResponseT;>;"
        }
    .end annotation
.end field

.field public final mSessionData:[B

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;[BLjava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "apiContext"    # Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;
    .param p2, "sessionData"    # [B
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;",
            "[B",
            "Ljava/lang/Class",
            "<TResponseT;>;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Landroid/util/Pair",
            "<TRequestT;TResponseT;>;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    .local p3, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TResponseT;>;"
    .local p4, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Landroid/util/Pair<TRequestT;TResponseT;>;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p4, p5}, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 62
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    .line 93
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    .line 94
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mSessionData:[B

    .line 95
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mResponseClass:Ljava/lang/Class;

    .line 96
    return-void
.end method

.method private eesEncodeCreditCardExpirationDateFormValue(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;)V
    .locals 3
    .param p1, "value"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    .prologue
    .line 214
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    const-string v1, "cvc"

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;->cvc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const-string v0, "__param:cvc"

    iput-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;->cvc:Ljava/lang/String;

    .line 216
    return-void
.end method

.method private eesEncodeCreditCardFormValue(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;)V
    .locals 3
    .param p1, "value"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .prologue
    .line 206
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    const-string v1, "credit_card_number"

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    const-string v0, "__param:credit_card_number"

    iput-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cardNumber:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    const-string v1, "cvc"

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const-string v0, "__param:cvc"

    iput-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;->cvc:Ljava/lang/String;

    .line 210
    return-void
.end method

.method private initialize()V
    .locals 6

    .prologue
    .line 171
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mInitialized:Z

    if-eqz v2, :cond_0

    .line 203
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->getPageValueToEncode()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v1

    .line 178
    .local v1, "pageValue":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    const/4 v0, 0x0

    .line 179
    .local v0, "eesEncodingRequired":Z
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->eesEncodeCreditCardFormValue(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;)V

    .line 181
    const/4 v0, 0x1

    .line 190
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 191
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "SecurePageRequest should only be used for creating / updating credit card instruments / customer information"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 182
    :cond_2
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    if-eqz v2, :cond_3

    .line 183
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->eesEncodeCreditCardExpirationDateFormValue(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;)V

    .line 184
    const/4 v0, 0x1

    goto :goto_1

    .line 185
    :cond_3
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    if-eqz v2, :cond_1

    .line 187
    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->eesEncodeCreditCardFormValue(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;)V

    .line 188
    const/4 v0, 0x1

    goto :goto_1

    .line 196
    :cond_4
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    iget-object v2, v2, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->eesBaseUrl:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->getPageActionUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "s7e"

    const-string v4, ";"

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mUrl:Ljava/lang/String;

    .line 202
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mInitialized:Z

    goto :goto_0
.end method


# virtual methods
.method public getHeaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->initialize()V

    .line 113
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->getHeaders()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getPageActionUrl()Ljava/lang/String;
.end method

.method protected abstract getPageValueToEncode()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
.end method

.method public getParams()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->initialize()V

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 103
    .local v0, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mEesParams:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 104
    const-string v1, "requestContentType"

    const-string v2, "application/protobuf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v1, "request"

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->getProtoRequestAsByteArray()[B

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-object v0
.end method

.method protected abstract getProtoRequestAsByteArray()[B
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->initialize()V

    .line 119
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 6
    .param p1, "networkResponse"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Landroid/util/Pair",
            "<TRequestT;TResponseT;>;>;"
        }
    .end annotation

    .prologue
    .line 155
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest<TRequestT;TResponseT;>;"
    :try_start_0
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mResponseClass:Ljava/lang/Class;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->mResponseClass:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/nano/MessageNano;

    iget-object v5, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-static {v3, v5}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 165
    .local v2, "response":Lcom/google/protobuf/nano/MessageNano;, "TResponseT;"
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->logResponse(Lcom/google/protobuf/nano/MessageNano;Ljava/lang/String;)V

    .line 166
    invoke-static {p1}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v0

    .line 167
    .local v0, "cacheEntry":Lcom/android/volley/Cache$Entry;
    invoke-static {p0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v3

    .end local v0    # "cacheEntry":Lcom/android/volley/Cache$Entry;
    .end local v2    # "response":Lcom/google/protobuf/nano/MessageNano;, "TResponseT;"
    :goto_0
    return-object v3

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/lang/InstantiationException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to create proto object."

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 159
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/IllegalAccessException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to create proto object."

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 161
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 162
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "SecurePageRequest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t parse proto response for url="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v3, Lcom/android/volley/ParseError;

    invoke-direct {v3, p1}, Lcom/android/volley/ParseError;-><init>(Lcom/android/volley/NetworkResponse;)V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_0
.end method
