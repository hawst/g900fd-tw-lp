.class public Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "AddressEntryFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;,
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;,
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;,
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;,
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;
    }
.end annotation


# static fields
.field private static final ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

.field private static final ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

.field private static final CASE_INSENSITIVE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

.field private static final POSTAL_CODE_ONLY_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;


# instance fields
.field mAddressFields:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

.field private mAddressHints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation
.end field

.field mAddressSources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;",
            ">;"
        }
    .end annotation
.end field

.field private mAdminAreaData:Lorg/json/JSONObject;

.field private mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

.field private mContainer:Landroid/widget/LinearLayout;

.field private mCountryData:Lorg/json/JSONObject;

.field mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

.field mHideAddressCheckbox:Landroid/widget/CheckBox;

.field mLanguageCode:Ljava/lang/String;

.field private mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

.field private mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

.field private mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

.field private mPendingRequestCounter:I

.field mPhoneNumberText:Landroid/widget/TextView;

.field private mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

.field mRecipientNameText:Landroid/widget/TextView;

.field mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

.field mRegionCodes:[I

.field mSelectedCountry:I

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xb

    const/16 v6, 0xa

    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 119
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0, v5}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    .line 121
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x53

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 122
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x52

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 123
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x43

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 124
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x31

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 125
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x32

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 126
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 127
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x58

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 134
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0, v4}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->POSTAL_CODE_ONLY_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    .line 137
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->POSTAL_CODE_ONLY_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x52

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 138
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->POSTAL_CODE_ONLY_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 143
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v7}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    .line 144
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_country:I

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 145
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_recipient:I

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 146
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_1:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 148
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_2:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 150
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_locality:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 151
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_admin_area:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 152
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_postal_code:I

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 154
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x9

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_sorting_code:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 156
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_dependent_locality:I

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 158
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_organization:I

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 160
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_phone_number:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 166
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v6}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    .line 167
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x52

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 168
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x4e

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 169
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x31

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 171
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x32

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 173
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x43

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 174
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x53

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 175
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 177
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x58

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 179
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x44

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 181
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0x4f

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseIntArray;->put(II)V

    .line 189
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$1;

    invoke-direct {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$1;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->CASE_INSENSITIVE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    .line 238
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x683

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 240
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    .line 2103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/location/country/Postaladdress$PostalAddress;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFieldValuesForPrefilling()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;)Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressLanguageCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 1
    .param p0, "x0"    # Landroid/util/SparseArray;

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getPostalAddressFromFieldValues(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lorg/json/JSONObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onReceivedCountryData(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onSelectedAdminChange()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fillAddressFieldsAndUpdateFocus(Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->focusToNextFieldOrHideKeyboard(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalityInput()V

    return-void
.end method

.method private advanceFocusOutOfFragmentOrHideKeyboard()V
    .locals 1

    .prologue
    .line 1670
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->focusOutOfFragment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675
    :goto_0
    return-void

    .line 1673
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->hideKeyboard()Z

    goto :goto_0
.end method

.method private areFormFieldsHidden()Z
    .locals 1

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cancelPendingAdminAreaMetadataRetrievalRequest()V
    .locals 1

    .prologue
    .line 1768
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    if-eqz v0, :cond_0

    .line 1769
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;->cancel()V

    .line 1770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    .line 1772
    :cond_0
    return-void
.end method

.method private configureAddressFormEditTextAutocomplete(C[CLjava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
    .locals 9
    .param p1, "field"    # C
    .param p2, "remainingFields"    # [C
    .param p3, "requiredFields"    # Ljava/lang/String;
    .param p4, "editText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .prologue
    .line 1520
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isFieldAutoCompletable(C)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->shouldAutoCompleteBeEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1522
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_row_address_hint_spinner:I

    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressLanguageCode()Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;-><init>(Landroid/view/ContextThemeWrapper;IILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V

    .line 1526
    .local v0, "adapter":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;
    invoke-virtual {p4, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1527
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setThreshold(I)V

    .line 1528
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;

    invoke-direct {v1, p0, p4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    invoke-virtual {p4, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1549
    .end local v0    # "adapter":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;
    :cond_0
    return-void
.end method

.method private constructAddressesToFetchAdminAreaFor()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1971
    .local v0, "postalAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1972
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressHints:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1973
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressHints:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1975
    :cond_0
    return-object v0
.end method

.method private createAndConfigureAddressFields()V
    .locals 11

    .prologue
    const/16 v10, 0x4e

    .line 1367
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActiveCountryDataAddressFields()Ljava/lang/String;

    move-result-object v0

    .line 1368
    .local v0, "addressFields":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v9, "require"

    invoke-static {v8, v9}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1370
    .local v7, "requiredFields":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 1371
    .local v1, "dynamicViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    .line 1372
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "length":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 1373
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1374
    .local v2, "field":C
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 1375
    .local v6, "remainingFields":[C
    if-ne v2, v10, :cond_1

    .line 1376
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    instance-of v8, v8, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v8, :cond_0

    .line 1377
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    check-cast v8, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {p0, v10, v6, v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->configureAddressFormEditTextAutocomplete(C[CLjava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    .line 1380
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isUiEnabled()Z

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1382
    :cond_0
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1372
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1384
    :cond_1
    invoke-direct {p0, v2, v6, v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->newAddressFieldInput(C[CLjava/lang/String;)Landroid/view/View;

    move-result-object v4

    .line 1385
    .local v4, "input":Landroid/view/View;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1386
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1389
    .end local v2    # "field":C
    .end local v4    # "input":Landroid/view/View;
    .end local v6    # "remainingFields":[C
    :cond_2
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v8, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->setFields(Ljava/util/ArrayList;)V

    .line 1390
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updatePostalCodeValidation()V

    .line 1391
    return-void
.end method

.method private displayRegionCodeViewWithSelectedCountry()V
    .locals 2

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodes:[I

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setRegionCodes([I)V

    .line 1037
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$5;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 1047
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onSelectedCountryChange()V

    .line 1048
    return-void
.end method

.method private fillAddressFieldsAndUpdateFocus(Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V
    .locals 13
    .param p1, "autoCompleteInput"    # Landroid/view/View;
    .param p2, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 1575
    const/4 v3, 0x0

    .line 1576
    .local v3, "foundInput":Z
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1577
    .local v10, "viewIndicesToValidate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    iget-object v11, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v8

    .local v8, "numInputs":I
    :goto_0
    if-ge v4, v8, :cond_3

    .line 1578
    iget-object v11, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 1580
    .local v5, "input":Landroid/view/View;
    if-nez v3, :cond_0

    if-ne v5, p1, :cond_0

    .line 1581
    const/4 v3, 0x1

    .line 1584
    :cond_0
    if-eqz v3, :cond_1

    .line 1585
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Character;

    invoke-virtual {v11}, Ljava/lang/Character;->charValue()C

    move-result v1

    .line 1586
    .local v1, "field":C
    invoke-static {p2, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v9

    .line 1589
    .local v9, "value":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-static {v1, v11}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isAddressFieldRequired(CLorg/json/JSONObject;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-static {v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getInputValue(Landroid/view/View;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1577
    .end local v1    # "field":C
    .end local v9    # "value":Ljava/lang/String;
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1594
    .restart local v1    # "field":C
    .restart local v9    # "value":Ljava/lang/String;
    :cond_2
    invoke-static {v5, v9}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setInputValue(Landroid/view/View;Ljava/lang/String;)V

    .line 1595
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1600
    .end local v1    # "field":C
    .end local v5    # "input":Landroid/view/View;
    .end local v9    # "value":Ljava/lang/String;
    :cond_3
    if-nez v3, :cond_5

    .line 1625
    :cond_4
    :goto_2
    return-void

    .line 1606
    :cond_5
    const/4 v2, 0x0

    .line 1607
    .local v2, "firstInvalidOrEmptyInput":Landroid/view/View;
    const/4 v4, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    .local v7, "length":I
    :goto_3
    if-ge v4, v7, :cond_a

    .line 1608
    iget-object v12, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 1609
    .restart local v5    # "input":Landroid/view/View;
    instance-of v11, v5, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v11, :cond_6

    move-object v11, v5

    check-cast v11, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v11}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v11

    if-eqz v11, :cond_9

    :cond_6
    const/4 v6, 0x1

    .line 1610
    .local v6, "isValid":Z
    :goto_4
    if-nez v2, :cond_8

    if-eqz v6, :cond_7

    invoke-static {v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getInputValue(Landroid/view/View;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1612
    :cond_7
    move-object v2, v5

    .line 1607
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1609
    .end local v6    # "isValid":Z
    :cond_9
    const/4 v6, 0x0

    goto :goto_4

    .line 1616
    .end local v5    # "input":Landroid/view/View;
    :cond_a
    if-nez v2, :cond_b

    .line 1617
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->advanceFocusOutOfFragmentOrHideKeyboard()V

    goto :goto_2

    .line 1619
    :cond_b
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 1620
    instance-of v11, v2, Landroid/widget/EditText;

    if-eqz v11, :cond_4

    move-object v0, v2

    .line 1621
    check-cast v0, Landroid/widget/EditText;

    .line 1622
    .local v0, "editText":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-interface {v11}, Landroid/text/Editable;->length()I

    move-result v11

    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_2
.end method

.method private findInputIndexWithAddressField(C)I
    .locals 5
    .param p1, "addressField"    # C

    .prologue
    .line 1959
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "length":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1960
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1961
    .local v2, "input":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 1962
    .local v0, "field":Ljava/lang/Character;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v4

    if-ne v4, p1, :cond_0

    .line 1966
    .end local v0    # "field":Ljava/lang/Character;
    .end local v1    # "i":I
    .end local v2    # "input":Landroid/view/View;
    :goto_1
    return v1

    .line 1959
    .restart local v0    # "field":Ljava/lang/Character;
    .restart local v1    # "i":I
    .restart local v2    # "input":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1966
    .end local v0    # "field":Ljava/lang/Character;
    .end local v2    # "input":Landroid/view/View;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "regionCode"    # I
    .param p2, "desiredLanguageCode"    # Ljava/lang/String;
    .param p3, "languageCodeForRequest"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 580
    .local p4, "addressesToPotentiallyFillAdminArea":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    .line 581
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->cancelPendingAdminAreaMetadataRetrievalRequest()V

    .line 583
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;

    invoke-direct {v4, p0, p4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Ljava/util/ArrayList;)V

    new-instance v5, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;

    invoke-direct {v5, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;-><init>(Lcom/android/volley/RequestQueue;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 663
    .local v0, "task":Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->indicatePendingRequest(Z)V

    .line 664
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->switchToShowingProgressBar()V

    .line 665
    invoke-virtual {v0, p3}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->run(Ljava/lang/String;)V

    .line 666
    return-void
.end method

.method private focusOutOfFragment()Z
    .locals 3

    .prologue
    .line 1641
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 1642
    .local v0, "nextView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1643
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1644
    const/4 v1, 0x1

    .line 1646
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private focusToNextField(Landroid/view/View;)Z
    .locals 3
    .param p1, "currentFocus"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 1628
    if-nez p1, :cond_1

    .line 1636
    :cond_0
    :goto_0
    return v1

    .line 1631
    :cond_1
    const/16 v2, 0x82

    invoke-virtual {p1, v2}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 1632
    .local v0, "nextView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1633
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1634
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private focusToNextFieldOrHideKeyboard(Landroid/view/View;)V
    .locals 1
    .param p1, "currentFocus"    # Landroid/view/View;

    .prologue
    .line 1662
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->focusToNextField(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667
    :goto_0
    return-void

    .line 1665
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->hideKeyboard()Z

    goto :goto_0
.end method

.method private getActiveCountryDataAddressFields()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1189
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getCountryDataAddressFields()[C

    move-result-object v2

    .line 1190
    .local v2, "allAddressFields":[C
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v7, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->postalCodeOnlyCountryCode:[Ljava/lang/String;

    iget v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-static {v8}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    .line 1192
    .local v5, "inPostalCodeOnlyMode":Z
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v7, v2

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1195
    .local v0, "activeAddressFields":Ljava/lang/StringBuilder;
    const/16 v7, 0x4e

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1196
    move-object v3, v2

    .local v3, "arr$":[C
    array-length v6, v3

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_3

    aget-char v1, v3, v4

    .line 1197
    .local v1, "addressField":C
    sget-object v7, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ALL_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1196
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1201
    :cond_1
    if-eqz v5, :cond_2

    sget-object v7, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->POSTAL_CODE_ONLY_DYNAMIC_ADDRESS_FIELDS:Landroid/util/SparseBooleanArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1206
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1208
    .end local v1    # "addressField":C
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private getAddressFieldValues()Landroid/util/SparseArray;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 839
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-nez v6, :cond_1

    .line 840
    const/4 v1, 0x0

    .line 857
    :cond_0
    :goto_0
    return-object v1

    .line 843
    :cond_1
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 844
    .local v4, "numInputs":I
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v4}, Landroid/util/SparseArray;-><init>(I)V

    .line 845
    .local v1, "fieldValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 846
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 847
    .local v3, "input":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 848
    .local v0, "field":Ljava/lang/Character;
    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getInputValue(Landroid/view/View;)Ljava/lang/String;

    move-result-object v5

    .line 849
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v6

    invoke-virtual {v1, v6, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 845
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 853
    .end local v0    # "field":Ljava/lang/Character;
    .end local v3    # "input":Landroid/view/View;
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    iget v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eqz v6, :cond_0

    .line 854
    const/16 v6, 0x52

    iget v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private getAddressFieldValuesForPrefilling()Landroid/util/SparseArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1154
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFieldValues()Landroid/util/SparseArray;

    move-result-object v1

    .line 1156
    .local v1, "fieldValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 1157
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    int-to-char v0, v3

    .line 1158
    .local v0, "field":C
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputWithAddressField(C)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/Spinner;

    if-eqz v3, :cond_0

    .line 1159
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 1156
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1162
    .end local v0    # "field":C
    :cond_1
    return-object v1
.end method

.method private getAddressLanguageCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1887
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    .line 1889
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v1, "lang"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getAdminAreaLocalities()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1317
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    .line 1319
    .local v1, "latin":Z
    const/4 v4, 0x0

    .line 1320
    .local v4, "valuesToStore":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1323
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    const-string v6, "sub_lnames"

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1326
    :cond_0
    if-nez v4, :cond_1

    .line 1327
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    const-string v6, "sub_keys"

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1330
    :cond_1
    if-eqz v4, :cond_2

    array-length v5, v4

    if-nez v5, :cond_4

    .line 1331
    :cond_2
    const/4 v2, 0x0

    .line 1358
    :cond_3
    :goto_0
    return-object v2

    .line 1335
    :cond_4
    if-eqz v1, :cond_7

    .line 1336
    move-object v3, v4

    .line 1341
    .local v3, "valuesToDisplay":[Ljava/lang/String;
    :goto_1
    if-eqz v3, :cond_5

    array-length v5, v3

    array-length v6, v4

    if-eq v5, v6, :cond_6

    .line 1344
    :cond_5
    move-object v3, v4

    .line 1347
    :cond_6
    new-instance v2, Ljava/util/ArrayList;

    array-length v5, v4

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1348
    .local v2, "localities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v5, v4

    if-ge v0, v5, :cond_8

    .line 1349
    new-instance v5, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;

    aget-object v6, v4, v0

    aget-object v7, v3, v0

    invoke-direct {v5, v6, v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1348
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1338
    .end local v0    # "i":I
    .end local v2    # "localities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    .end local v3    # "valuesToDisplay":[Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    const-string v6, "sub_names"

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "valuesToDisplay":[Ljava/lang/String;
    goto :goto_1

    .line 1354
    .restart local v0    # "i":I
    .restart local v2    # "localities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    :cond_8
    if-eqz v1, :cond_3

    .line 1355
    sget-object v5, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->CASE_INSENSITIVE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v2, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private getCountryAdminAreas()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v14, 0x0

    .line 1226
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v9

    .line 1230
    .local v9, "latin":Z
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "sub_keys"

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1231
    .local v8, "i18nKeys":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "sub_mores"

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 1233
    .local v10, "localityPresenceIndicators":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 1234
    .local v12, "valuesToStore":[Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 1237
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "sub_lnames"

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1240
    :cond_0
    if-nez v12, :cond_1

    .line 1241
    move-object v12, v8

    .line 1243
    :cond_1
    if-eqz v12, :cond_2

    array-length v1, v12

    if-nez v1, :cond_4

    :cond_2
    move-object v6, v14

    .line 1300
    :cond_3
    :goto_0
    return-object v6

    .line 1248
    :cond_4
    if-eqz v9, :cond_a

    .line 1249
    move-object v11, v12

    .line 1254
    .local v11, "valuesToDisplay":[Ljava/lang/String;
    :goto_1
    if-eqz v11, :cond_5

    array-length v1, v11

    array-length v3, v12

    if-eq v1, v3, :cond_6

    .line 1257
    :cond_5
    move-object v11, v12

    .line 1260
    :cond_6
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "sub_zips"

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 1261
    .local v13, "zipCodes":[Ljava/lang/String;
    if-eqz v13, :cond_7

    array-length v1, v13

    array-length v3, v12

    if-eq v1, v3, :cond_7

    .line 1263
    const/4 v13, 0x0

    .line 1266
    :cond_7
    if-eqz v8, :cond_8

    array-length v1, v8

    array-length v3, v12

    if-eq v1, v3, :cond_8

    .line 1269
    const/4 v8, 0x0

    .line 1272
    const/4 v10, 0x0

    .line 1275
    :cond_8
    if-eqz v10, :cond_9

    array-length v1, v10

    array-length v3, v12

    if-eq v1, v3, :cond_9

    .line 1279
    const/4 v10, 0x0

    .line 1282
    :cond_9
    new-instance v6, Ljava/util/ArrayList;

    array-length v1, v12

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1283
    .local v6, "adminAreas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    array-length v1, v12

    if-ge v7, v1, :cond_e

    .line 1284
    if-eqz v10, :cond_b

    aget-object v1, v10, v7

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v2, 0x1

    .line 1286
    .local v2, "hasLocality":Z
    :goto_3
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    if-eqz v8, :cond_c

    aget-object v1, v8, v7

    :goto_4
    aget-object v3, v12, v7

    aget-object v4, v11, v7

    if-eqz v13, :cond_d

    aget-object v5, v13, v7

    :goto_5
    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    .local v0, "adminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1283
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1251
    .end local v0    # "adminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    .end local v2    # "hasLocality":Z
    .end local v6    # "adminAreas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    .end local v7    # "i":I
    .end local v11    # "valuesToDisplay":[Ljava/lang/String;
    .end local v13    # "zipCodes":[Ljava/lang/String;
    :cond_a
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "sub_names"

    invoke-static {v1, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .restart local v11    # "valuesToDisplay":[Ljava/lang/String;
    goto :goto_1

    .line 1284
    .restart local v6    # "adminAreas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    .restart local v7    # "i":I
    .restart local v13    # "zipCodes":[Ljava/lang/String;
    :cond_b
    const/4 v2, 0x0

    goto :goto_3

    .restart local v2    # "hasLocality":Z
    :cond_c
    move-object v1, v14

    .line 1286
    goto :goto_4

    :cond_d
    move-object v5, v14

    goto :goto_5

    .line 1296
    .end local v2    # "hasLocality":Z
    :cond_e
    if-eqz v9, :cond_3

    .line 1297
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->CASE_INSENSITIVE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v6, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private getCountryDataAddressFields()[C
    .locals 4

    .prologue
    .line 1177
    const/4 v0, 0x0

    .line 1178
    .local v0, "format":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    .line 1179
    .local v1, "latin":Z
    if-eqz v1, :cond_0

    .line 1180
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "lfmt"

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1182
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1183
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v3, "fmt"

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1185
    :cond_1
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->determineAddressFieldsToDisplay(Ljava/lang/String;)[C

    move-result-object v2

    return-object v2
.end method

.method private getFieldLabel(C)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # C

    .prologue
    .line 1552
    packed-switch p1, :pswitch_data_0

    .line 1556
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-static {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->makeAddressFieldLabel(Landroid/content/Context;CLorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1554
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    goto :goto_0

    .line 1552
    nop

    :pswitch_data_0
    .packed-switch 0x4e
        :pswitch_0
    .end packed-switch
.end method

.method static getInputValue(Landroid/view/View;)Ljava/lang/String;
    .locals 4
    .param p0, "input"    # Landroid/view/View;

    .prologue
    .line 279
    if-nez p0, :cond_0

    .line 280
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Input must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :cond_0
    instance-of v1, p0, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 282
    check-cast p0, Landroid/widget/TextView;

    .end local p0    # "input":Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    .local v0, "selectedItem":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 283
    .end local v0    # "selectedItem":Ljava/lang/Object;
    .restart local p0    # "input":Landroid/view/View;
    :cond_1
    instance-of v1, p0, Landroid/widget/Spinner;

    if-eqz v1, :cond_4

    .line 284
    check-cast p0, Landroid/widget/Spinner;

    .end local p0    # "input":Landroid/view/View;
    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    .line 285
    .restart local v0    # "selectedItem":Ljava/lang/Object;
    instance-of v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;

    if-eqz v1, :cond_2

    .line 286
    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;

    .end local v0    # "selectedItem":Ljava/lang/Object;
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 287
    .restart local v0    # "selectedItem":Ljava/lang/Object;
    :cond_2
    if-eqz v0, :cond_3

    .line 288
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 290
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 293
    .end local v0    # "selectedItem":Ljava/lang/Object;
    .restart local p0    # "input":Landroid/view/View;
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown input type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static getPostalAddressFromFieldValues(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/location/country/Postaladdress$PostalAddress;"
        }
    .end annotation

    .prologue
    .line 737
    .local p0, "fieldValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    new-instance v3, Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {v3}, Lcom/google/location/country/Postaladdress$PostalAddress;-><init>()V

    .line 738
    .local v3, "postalAddress":Lcom/google/location/country/Postaladdress$PostalAddress;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 739
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    int-to-char v0, v5

    .line 740
    .local v0, "field":C
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 744
    .local v4, "value":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 738
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 747
    :cond_0
    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    .line 753
    :sswitch_0
    iget-object v5, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->appendToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    iput-object v5, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    goto :goto_1

    .line 749
    :sswitch_1
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    goto :goto_1

    .line 757
    :sswitch_2
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    goto :goto_1

    .line 760
    :sswitch_3
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    goto :goto_1

    .line 763
    :sswitch_4
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 764
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    goto :goto_1

    .line 767
    :sswitch_5
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    goto :goto_1

    .line 770
    :sswitch_6
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    goto :goto_1

    .line 773
    :sswitch_7
    iput-object v4, v3, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    goto :goto_1

    .line 777
    .end local v0    # "field":C
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    return-object v3

    .line 747
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x43 -> :sswitch_3
        0x4e -> :sswitch_6
        0x4f -> :sswitch_7
        0x52 -> :sswitch_1
        0x53 -> :sswitch_2
        0x58 -> :sswitch_5
        0x5a -> :sswitch_4
    .end sparse-switch
.end method

.method private getSelectedAdmin()Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1870
    const/16 v2, 0x53

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputWithAddressField(C)Landroid/view/View;

    move-result-object v0

    .line 1871
    .local v0, "adminAreaInput":Landroid/view/View;
    if-nez v0, :cond_1

    .line 1877
    .end local v0    # "adminAreaInput":Landroid/view/View;
    :cond_0
    :goto_0
    return-object v1

    .line 1874
    .restart local v0    # "adminAreaInput":Landroid/view/View;
    :cond_1
    instance-of v2, v0, Landroid/widget/Spinner;

    if-eqz v2, :cond_0

    .line 1875
    check-cast v0, Landroid/widget/Spinner;

    .end local v0    # "adminAreaInput":Landroid/view/View;
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    goto :goto_0
.end method

.method private hideKeyboard()Z
    .locals 2

    .prologue
    .line 1652
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1653
    .local v0, "input":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1654
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->hideSoftKeyboard(Landroid/content/Context;Landroid/view/View;)Z

    .line 1655
    const/4 v1, 0x1

    .line 1657
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isFieldAutoCompletable(C)Z
    .locals 2
    .param p1, "field"    # C

    .prologue
    const/4 v0, 0x0

    .line 1394
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1403
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 1397
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 1403
    const/4 v0, 0x1

    goto :goto_0

    .line 1397
    nop

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x58 -> :sswitch_0
    .end sparse-switch
.end method

.method private newAddressFieldInput(C[CLjava/lang/String;)Landroid/view/View;
    .locals 19
    .param p1, "field"    # C
    .param p2, "remainingFields"    # [C
    .param p3, "requiredFields"    # Ljava/lang/String;

    .prologue
    .line 1410
    const/16 v16, 0x4e

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_0

    .line 1411
    new-instance v16, Ljava/lang/IllegalArgumentException;

    const-string v17, "Recipient name should not be created dynamically"

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 1413
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    move-object/from16 v16, v0

    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isAddressFieldRequired(CLorg/json/JSONObject;)Z

    move-result v13

    .line 1414
    .local v13, "required":Z
    invoke-direct/range {p0 .. p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getFieldLabel(C)Ljava/lang/String;

    move-result-object v9

    .line 1415
    .local v9, "fieldLabel":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getThemedLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v15

    .line 1416
    .local v15, "themedInflater":Landroid/view/LayoutInflater;
    const/4 v10, 0x0

    .line 1417
    .local v10, "input":Landroid/view/View;
    sget-object v16, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->I18N_FIELD_ID_TO_ADDRESS_FORM_FIELD_ID:Landroid/util/SparseIntArray;

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    .line 1419
    .local v5, "addressFormFieldId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 1420
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_non_editable_text:I

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 1465
    :cond_1
    :goto_0
    if-nez v10, :cond_2

    .line 1466
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_edit_text:I

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 1468
    .local v8, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->configureAddressFormEditTextAutocomplete(C[CLjava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    .line 1470
    invoke-virtual {v8, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setRequired(Z)V

    .line 1471
    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1472
    const/4 v11, 0x1

    .line 1473
    .local v11, "inputType":I
    sparse-switch p1, :sswitch_data_0

    .line 1502
    :goto_1
    invoke-virtual {v8, v11}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setInputType(I)V

    .line 1503
    move-object v10, v8

    .line 1507
    .end local v8    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .end local v11    # "inputType":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 1508
    const/16 v16, 0x8

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1511
    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressFieldViewId(C)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/view/View;->setId(I)V

    .line 1512
    invoke-static/range {p1 .. p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1513
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isUiEnabled()Z

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1515
    return-object v10

    .line 1421
    :cond_4
    const/16 v16, 0x53

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_5

    .line 1422
    invoke-direct/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getCountryAdminAreas()Ljava/util/ArrayList;

    move-result-object v7

    .line 1423
    .local v7, "adminAreas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_1

    .line 1426
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_default_spinner:I

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    .line 1428
    .local v14, "spinner":Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    invoke-virtual {v14, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setRequired(Z)V

    .line 1429
    invoke-virtual {v14, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 1430
    invoke-virtual {v14, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setLabel(Ljava/lang/String;)V

    .line 1431
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v16

    sget v17, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_row_spinner:I

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v6, v0, v1, v7, v9}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;-><init>(Landroid/view/ContextThemeWrapper;ILjava/util/List;Ljava/lang/String;)V

    .line 1433
    .local v6, "adminAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_spinner_dropdown:I

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1434
    invoke-virtual {v14, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1435
    new-instance v16, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$6;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$6;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1449
    move-object v10, v14

    goto/16 :goto_0

    .line 1451
    .end local v6    # "adminAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    .end local v7    # "adminAreas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    .end local v14    # "spinner":Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    :cond_5
    const/16 v16, 0x43

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_1

    .line 1452
    invoke-direct/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAdminAreaLocalities()Ljava/util/ArrayList;

    move-result-object v12

    .line 1453
    .local v12, "localities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_1

    .line 1454
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v16

    sget v17, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_row_spinner:I

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v4, v0, v1, v12}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1456
    .local v4, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_spinner_dropdown:I

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1457
    sget v16, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_default_spinner:I

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    .line 1459
    .restart local v14    # "spinner":Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    invoke-virtual {v14, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setRequired(Z)V

    .line 1460
    invoke-virtual {v14, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 1461
    invoke-virtual {v14, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1462
    move-object v10, v14

    goto/16 :goto_0

    .line 1475
    .end local v4    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    .end local v12    # "localities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;>;"
    .end local v14    # "spinner":Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    .restart local v8    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .restart local v11    # "inputType":I
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->doesCountryUseNumericPostalCode(Lorg/json/JSONObject;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 1476
    or-int/lit8 v11, v11, 0x2

    goto/16 :goto_1

    .line 1481
    :cond_6
    or-int/lit16 v11, v11, 0x1000

    .line 1482
    const/high16 v16, 0x80000

    move/from16 v0, v16

    or-int/lit16 v11, v0, 0x1001

    .line 1484
    goto/16 :goto_1

    .line 1489
    :sswitch_1
    or-int/lit8 v11, v11, 0x70

    .line 1490
    or-int/lit16 v11, v11, 0x2000

    .line 1491
    goto/16 :goto_1

    .line 1493
    :sswitch_2
    or-int/lit16 v11, v11, 0x2000

    .line 1494
    goto/16 :goto_1

    .line 1496
    :sswitch_3
    or-int/lit16 v11, v11, 0x2000

    .line 1497
    goto/16 :goto_1

    .line 1473
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_1
        0x32 -> :sswitch_1
        0x33 -> :sswitch_1
        0x41 -> :sswitch_1
        0x43 -> :sswitch_3
        0x53 -> :sswitch_2
        0x5a -> :sswitch_0
    .end sparse-switch
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;I)Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    .locals 4
    .param p0, "addressForm"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 260
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;-><init>()V

    .line 262
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 263
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "addressForm"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 264
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 266
    return-object v1
.end method

.method private onReceivedCountryData(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 1114
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getRegionCodeFromAddressData(Lorg/json/JSONObject;)I

    move-result v0

    .line 1115
    .local v0, "country":I
    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eq v0, v2, :cond_0

    .line 1146
    :goto_0
    return-void

    .line 1121
    :cond_0
    const/4 v1, 0x0

    .line 1122
    .local v1, "oldFieldValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    if-nez v2, :cond_1

    .line 1123
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFieldValuesForPrefilling()Landroid/util/SparseArray;

    move-result-object v1

    .line 1126
    :cond_1
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    .line 1128
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateAddressSources()V

    .line 1131
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->createAndConfigureAddressFields()V

    .line 1133
    invoke-direct {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setAddressFieldValues(Landroid/util/SparseArray;)V

    .line 1136
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    if-eqz v2, :cond_2

    .line 1137
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setPostalAddressValues(Lcom/google/location/country/Postaladdress$PostalAddress;)V

    .line 1138
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 1141
    :cond_2
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->selectFirstAdminAreaIfHasLocalities()V

    .line 1143
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->switchToShowingFields()V

    .line 1145
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updatePostalCodeValidation()V

    goto :goto_0
.end method

.method private onSelectedAdminChange()V
    .locals 3

    .prologue
    .line 1736
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-nez v1, :cond_0

    .line 1764
    :goto_0
    return-void

    .line 1747
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updatePostalCodeValidation()V

    .line 1750
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getSelectedAdmin()Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    move-result-object v0

    .line 1751
    .local v0, "adminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    const-string v2, "key"

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mI18nKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1757
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    iput-object v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mData:Lorg/json/JSONObject;

    goto :goto_0

    .line 1760
    :cond_1
    iget-object v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mData:Lorg/json/JSONObject;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    .line 1761
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->cancelPendingAdminAreaMetadataRetrievalRequest()V

    .line 1763
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalities()V

    goto :goto_0
.end method

.method private onSelectedCountryChange()V
    .locals 3

    .prologue
    .line 1053
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    iget-object v2, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1055
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1056
    .local v0, "eventDetails":Landroid/os/Bundle;
    const-string v1, "FormEventListener.EXTRA_FORM_ID"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    const-string v1, "FormEventListener.EXTRA_FIELD_ID"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1060
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;

    const/4 v2, 0x3

    invoke-interface {v1, v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 1065
    .end local v0    # "eventDetails":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setSelectedRegionCode(I)V

    .line 1067
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateCountryData()V

    .line 1068
    return-void
.end method

.method private requestAlternativeCountryDataIfAvailable()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1083
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    if-nez v5, :cond_1

    .line 1109
    :cond_0
    :goto_0
    return v3

    .line 1086
    :cond_1
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAlternativeLanguageCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1088
    .local v0, "alternativeLang":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1090
    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->constructAddressesToFetchAdminAreaFor()Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {p0, v3, v5, v0, v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move v3, v4

    .line 1092
    goto :goto_0

    .line 1094
    :cond_2
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v6, "id"

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1095
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1100
    const-string v5, "--"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1101
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v6, "lang"

    invoke-static {v5, v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1102
    .local v2, "lang":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1104
    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->constructAddressesToFetchAdminAreaFor()Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {p0, v3, v5, v6, v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    move v3, v4

    .line 1106
    goto :goto_0
.end method

.method private resetPendingRequestStatus()V
    .locals 1

    .prologue
    .line 1943
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    .line 1944
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->doEnableUi()V

    .line 1945
    return-void
.end method

.method private selectFirstAdminAreaIfHasLocalities()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1854
    const/16 v3, 0x53

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputWithAddressField(C)Landroid/view/View;

    move-result-object v2

    .line 1855
    .local v2, "input":Landroid/view/View;
    instance-of v3, v2, Landroid/widget/Spinner;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 1856
    check-cast v0, Landroid/widget/Spinner;

    .line 1860
    .local v0, "adminAreaSpinner":Landroid/widget/Spinner;
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    if-nez v3, :cond_0

    .line 1861
    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    .line 1862
    .local v1, "firstAdminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    iget-boolean v3, v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mHasLocalities:Z

    if-eqz v3, :cond_0

    .line 1863
    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1867
    .end local v0    # "adminAreaSpinner":Landroid/widget/Spinner;
    .end local v1    # "firstAdminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    :cond_0
    return-void
.end method

.method private setAddressFieldValues(Landroid/util/SparseArray;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 861
    .local p1, "fieldValues":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    .line 874
    :cond_0
    return-void

    .line 865
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "length":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 866
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 867
    .local v2, "input":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 868
    .local v0, "field":Ljava/lang/Character;
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v5

    invoke-virtual {p1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 870
    .local v4, "value":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 871
    invoke-static {v2, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setInputValue(Landroid/view/View;Ljava/lang/String;)V

    .line 865
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static setInputValue(Landroid/view/View;Ljava/lang/String;)V
    .locals 9
    .param p0, "input"    # Landroid/view/View;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 307
    if-nez p0, :cond_0

    .line 308
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Input must not be null"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 309
    :cond_0
    instance-of v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v6, :cond_2

    .line 310
    check-cast p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .end local p0    # "input":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->replaceTextAndPreventFilter(Ljava/lang/CharSequence;)V

    .line 344
    :cond_1
    :goto_0
    return-void

    .line 311
    .restart local p0    # "input":Landroid/view/View;
    :cond_2
    instance-of v6, p0, Landroid/widget/TextView;

    if-eqz v6, :cond_3

    .line 312
    check-cast p0, Landroid/widget/TextView;

    .end local p0    # "input":Landroid/view/View;
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 313
    .restart local p0    # "input":Landroid/view/View;
    :cond_3
    instance-of v6, p0, Landroid/widget/Spinner;

    if-eqz v6, :cond_9

    move-object v5, p0

    .line 314
    check-cast v5, Landroid/widget/Spinner;

    .line 315
    .local v5, "spinner":Landroid/widget/Spinner;
    if-nez p1, :cond_4

    .line 317
    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 318
    :cond_4
    invoke-virtual {v5}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v6

    instance-of v6, v6, Landroid/widget/ArrayAdapter;

    if-eqz v6, :cond_1

    .line 319
    invoke-virtual {v5}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 320
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<*>;"
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 321
    const/4 v1, 0x0

    .line 322
    .local v1, "found":Z
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v4

    .local v4, "length":I
    :goto_1
    if-ge v2, v4, :cond_6

    .line 323
    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    .line 324
    .local v3, "item":Ljava/lang/Object;
    instance-of v6, v3, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;

    if-eqz v6, :cond_7

    move-object v6, v3

    check-cast v6, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;

    invoke-interface {v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 326
    const/4 v1, 0x1

    .line 330
    :cond_5
    :goto_2
    if-eqz v1, :cond_8

    .line 331
    invoke-virtual {v5, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 336
    .end local v3    # "item":Ljava/lang/Object;
    :cond_6
    if-nez v1, :cond_1

    .line 337
    invoke-virtual {v5, v7}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 327
    .restart local v3    # "item":Ljava/lang/Object;
    :cond_7
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 328
    const/4 v1, 0x1

    goto :goto_2

    .line 322
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 342
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<*>;"
    .end local v1    # "found":Z
    .end local v2    # "i":I
    .end local v3    # "item":Ljava/lang/Object;
    .end local v4    # "length":I
    .end local v5    # "spinner":Landroid/widget/Spinner;
    :cond_9
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown input type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private updateAddressSources()V
    .locals 3

    .prologue
    .line 1166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    .line 1167
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressHints:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressHints:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/common/address/LocalAddressSource;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressHints:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/LocalAddressSource;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1170
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1171
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->isCountrySupported(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1174
    :cond_1
    return-void
.end method

.method private updateCountryData()V
    .locals 5

    .prologue
    .line 1071
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getRegionCodeFromAddressData(Lorg/json/JSONObject;)I

    move-result v0

    .line 1073
    .local v0, "existingCountryCodeRegionCode":I
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-ne v0, v1, :cond_0

    .line 1074
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-direct {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onReceivedCountryData(Lorg/json/JSONObject;)V

    .line 1080
    :goto_0
    return-void

    .line 1077
    :cond_0
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->constructAddressesToFetchAdminAreaFor()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private updateLocalities()V
    .locals 6

    .prologue
    .line 1776
    const/16 v1, 0x43

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputWithAddressField(C)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1813
    :goto_0
    return-void

    .line 1779
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getSelectedAdmin()Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    move-result-object v0

    .line 1780
    .local v0, "adminArea":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    iget-boolean v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mHasLocalities:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mData:Lorg/json/JSONObject;

    if-eqz v1, :cond_2

    .line 1783
    :cond_1
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalityInput()V

    goto :goto_0

    .line 1786
    :cond_2
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    iget-object v3, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mI18nKey:Ljava/lang/String;

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;

    invoke-direct {v4, p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$8;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;)V

    new-instance v5, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;

    invoke-direct {v5, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    .line 1811
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    invoke-virtual {v1, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_0
.end method

.method private updateLocalityInput()V
    .locals 10

    .prologue
    const/16 v9, 0x43

    .line 1822
    invoke-direct {p0, v9}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputIndexWithAddressField(C)I

    move-result v0

    .line 1823
    .local v0, "addressFieldIndex":I
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1825
    .local v3, "localityInput":Landroid/view/View;
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActiveCountryDataAddressFields()Ljava/lang/String;

    move-result-object v1

    .line 1826
    .local v1, "addressFields":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 1827
    .local v5, "remainingFields":[C
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    const-string v8, "require"

    invoke-static {v7, v8}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1828
    .local v6, "requiredFields":Ljava/lang/String;
    invoke-direct {p0, v9, v5, v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->newAddressFieldInput(C[CLjava/lang/String;)Landroid/view/View;

    move-result-object v4

    .line 1831
    .local v4, "newInput":Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getInputValue(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 1833
    .local v2, "inputValue":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v7, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->replaceView(Landroid/view/View;Landroid/view/View;)V

    .line 1834
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v7, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1836
    invoke-static {v4, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setInputValue(Landroid/view/View;Ljava/lang/String;)V

    .line 1837
    return-void
.end method

.method private updatePostalCodeValidation()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1681
    const/16 v2, 0x5a

    .line 1683
    .local v2, "field":C
    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputWithAddressField(C)Landroid/view/View;

    move-result-object v3

    .line 1685
    .local v3, "input":Landroid/view/View;
    instance-of v7, v3, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-nez v7, :cond_1

    .line 1686
    iput-object v10, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 1732
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v4, v3

    .line 1690
    check-cast v4, Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;

    .line 1691
    .local v4, "postalCodeInput":Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    if-eqz v7, :cond_2

    .line 1692
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-interface {v4, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;->removeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 1693
    iput-object v10, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 1696
    :cond_2
    new-instance v7, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v8, v11, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 1697
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_address_field_invalid:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getFieldLabel(C)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1699
    .local v1, "errorMessage":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getPostalCodeRegexpPattern(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 1700
    .local v5, "postalCodeRegexpPattern":Ljava/util/regex/Pattern;
    if-eqz v5, :cond_3

    .line 1701
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    new-instance v8, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    invoke-direct {v8, v1, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 1703
    :cond_3
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getSelectedAdmin()Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    move-result-object v0

    .line 1704
    .local v0, "admin":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
    if-eqz v0, :cond_4

    .line 1705
    iget-object v7, v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mZipStartsWithRegEx:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getPostalCodeRegexpPatternForAdminArea(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 1707
    if-eqz v5, :cond_4

    .line 1708
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    new-instance v8, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    invoke-direct {v8, v1, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 1714
    :cond_4
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1715
    iput-object v10, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    goto :goto_0

    .line 1719
    :cond_5
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPostalCodeValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-interface {v4, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 1722
    instance-of v7, v4, Landroid/widget/TextView;

    if-eqz v7, :cond_7

    move-object v6, v4

    .line 1723
    check-cast v6, Landroid/widget/TextView;

    .line 1724
    .local v6, "postalCodeText":Landroid/widget/TextView;
    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v6}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1726
    :cond_6
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;->validate()Z

    goto :goto_0

    .line 1730
    .end local v6    # "postalCodeText":Landroid/widget/TextView;
    :cond_7
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;->validate()Z

    goto/16 :goto_0
.end method

.method private validate(Z)Z
    .locals 8
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 943
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isHidden()Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v5

    .line 976
    :cond_0
    :goto_0
    return v6

    .line 945
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isUiEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 947
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->hasPendingRequests()Z

    move-result v7

    if-nez v7, :cond_0

    .line 949
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    .line 951
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->areFormFieldsHidden()Z

    move-result v7

    if-eqz v7, :cond_2

    move v6, v5

    .line 952
    goto :goto_0

    .line 953
    :cond_2
    iget v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eqz v7, :cond_0

    .line 957
    const/4 v3, 0x1

    .line 958
    .local v3, "valid":Z
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "length":I
    :goto_1
    if-ge v1, v2, :cond_6

    .line 959
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 960
    .local v0, "child":Landroid/view/View;
    instance-of v7, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v7, :cond_3

    .line 961
    if-eqz p1, :cond_5

    .line 962
    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v3, :cond_4

    move v3, v5

    .line 958
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v3, v6

    .line 962
    goto :goto_2

    .line 963
    .restart local v0    # "child":Landroid/view/View;
    :cond_5
    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v7

    if-nez v7, :cond_3

    goto :goto_0

    .line 968
    :cond_6
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    instance-of v7, v7, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v7, :cond_7

    .line 969
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    check-cast v4, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 970
    .local v4, "validatablePhoneNumber":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    if-eqz p1, :cond_9

    .line 971
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v7

    if-eqz v7, :cond_8

    if-eqz v3, :cond_8

    move v3, v5

    .end local v4    # "validatablePhoneNumber":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    :cond_7
    :goto_3
    move v6, v3

    .line 976
    goto :goto_0

    .restart local v4    # "validatablePhoneNumber":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    :cond_8
    move v3, v6

    .line 971
    goto :goto_3

    .line 972
    :cond_9
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v5

    if-nez v5, :cond_7

    goto :goto_0
.end method


# virtual methods
.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 5
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    .line 891
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 892
    const/4 v1, 0x0

    .line 907
    :goto_0
    return v1

    .line 894
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    sget-object v2, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->ADDRESS_FORM_FIELD_ID_TO_VIEW_ID:Landroid/util/SparseIntArray;

    iget-object v3, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 897
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    .line 898
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FormFieldMessage received for invalid field: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " view: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 901
    :cond_2
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_3

    .line 902
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "view":Landroid/view/View;
    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 907
    const/4 v1, 0x1

    goto :goto_0

    .line 904
    .restart local v0    # "view":Landroid/view/View;
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FormFieldMessage received for non-EditText field: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected doEnableUi()V
    .locals 4

    .prologue
    .line 1896
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    .line 1910
    :cond_0
    :goto_0
    return-void

    .line 1901
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->isUiEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->hasPendingRequests()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v0, 0x1

    .line 1902
    .local v0, "enabled":Z
    :goto_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1903
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setEnabled(Z)V

    .line 1904
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "length":I
    :goto_2
    if-ge v1, v2, :cond_3

    .line 1905
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1904
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1901
    .end local v0    # "enabled":Z
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1907
    .restart local v0    # "enabled":Z
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 1908
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method findInputWithAddressField(C)Landroid/view/View;
    .locals 2
    .param p1, "addressField"    # C

    .prologue
    .line 1949
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->findInputIndexWithAddressField(C)I

    move-result v0

    .line 1950
    .local v0, "addressFieldIndex":I
    if-ltz v0, :cond_0

    .line 1951
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1953
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 981
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 982
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 983
    .local v0, "child":Landroid/view/View;
    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 986
    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 987
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    move v3, v4

    .line 1003
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v3

    .line 992
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    instance-of v3, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v3, :cond_1

    move-object v3, v0

    .line 993
    check-cast v3, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v3

    if-nez v3, :cond_1

    .line 994
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    move v3, v4

    .line 995
    goto :goto_1

    .line 981
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 999
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1000
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    move v3, v4

    .line 1001
    goto :goto_1

    .line 1003
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getAddressFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;
    .locals 3

    .prologue
    .line 719
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;-><init>()V

    .line 720
    .local v0, "addressFormValue":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->areFormFieldsHidden()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 721
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->isHidden:Z

    .line 733
    :cond_0
    :goto_0
    return-object v0

    .line 724
    :cond_1
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFieldValues()Landroid/util/SparseArray;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getPostalAddressFromFieldValues(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v2

    iput-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 725
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressLanguageCode()Ljava/lang/String;

    move-result-object v1

    .line 727
    .local v1, "addressLanguageCode":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 728
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    iput-object v1, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    .line 730
    :cond_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 731
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->phoneNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2184
    const/4 v0, 0x0

    return-object v0
.end method

.method getRequestQueue()Lcom/android/volley/RequestQueue;
    .locals 1

    .prologue
    .line 1980
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->getApiRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v0

    return-object v0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 2179
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public handleErrorMessageDismissed(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "errorType"    # I

    .prologue
    .line 912
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 913
    const/4 v1, 0x0

    .line 928
    :goto_0
    return v1

    .line 916
    :cond_0
    const/16 v1, 0x3e8

    if-ne p2, v1, :cond_2

    .line 918
    const/4 v0, 0x0

    .line 919
    .local v0, "languageCodeForRequest":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    if-eqz v1, :cond_1

    .line 920
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAlternativeLanguageCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 923
    :cond_1
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->constructAddressesToFetchAdminAreaFor()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 928
    const/4 v1, 0x1

    goto :goto_0

    .line 926
    .end local v0    # "languageCodeForRequest":Ljava/lang/String;
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized errorType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method hasPendingRequests()Z
    .locals 1

    .prologue
    .line 1927
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method indicatePendingRequest(Z)V
    .locals 4
    .param p1, "pendingRequest"    # Z

    .prologue
    const/4 v1, 0x1

    .line 1932
    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    if-eqz p1, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    .line 1936
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingRequestCounter:I

    if-nez v0, :cond_2

    .line 1938
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->doEnableUi()V

    .line 1940
    :cond_2
    return-void

    .line 1932
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isReadyToSubmit()Z
    .locals 1

    .prologue
    .line 1922
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 938
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 479
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 481
    if-eqz p1, :cond_4

    .line 482
    const-string v4, "pendingAddress"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 483
    const-string v4, "pendingAddress"

    invoke-static {p1, v4}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 485
    .local v2, "pendingAddress":Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)V

    .line 489
    .end local v2    # "pendingAddress":Lcom/google/location/country/Postaladdress$PostalAddress;
    :cond_0
    iget v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-nez v4, :cond_1

    .line 490
    const-string v4, "selectedCountry"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 492
    :cond_1
    const-string v4, "countryData"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 494
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    const-string v5, "countryData"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getRegionCodeFromAddressData(Lorg/json/JSONObject;)I

    move-result v0

    .line 506
    .local v0, "country":I
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v4

    if-eq v0, v4, :cond_2

    iget v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eq v0, v4, :cond_2

    .line 508
    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 509
    .local v3, "trulySelectedCountry":I
    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 510
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-direct {p0, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onReceivedCountryData(Lorg/json/JSONObject;)V

    .line 511
    iput v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 514
    .end local v0    # "country":I
    .end local v3    # "trulySelectedCountry":I
    :cond_2
    const-string v4, "languageCode"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 515
    const-string v4, "languageCode"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    .line 517
    :cond_3
    const-string v4, "adminAreaData"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 519
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    const-string v5, "adminAreaData"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 530
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->doEnableUi()V

    .line 532
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->displayRegionCodeViewWithSelectedCountry()V

    .line 535
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v4

    if-nez v4, :cond_5

    .line 536
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 541
    :cond_5
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    if-eqz v4, :cond_6

    iget v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eqz v4, :cond_6

    .line 542
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    iget v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getId()I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;->onRegionCodeSelected(II)V

    .line 544
    :cond_6
    return-void

    .line 495
    :catch_0
    move-exception v1

    .line 497
    .local v1, "e":Lorg/json/JSONException;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Could not construct JSONObject from KEY_COUNTRY_DATA json string"

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 521
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 523
    .restart local v1    # "e":Lorg/json/JSONException;
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Could not construct JSONObject from KEY_ADMIN_AREA_DATA json string"

    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 2168
    if-eqz p2, :cond_1

    const/16 v0, 0x8

    .line 2169
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2170
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setVisibility(I)V

    .line 2171
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->setVisibility(I)V

    .line 2172
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 2173
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2175
    :cond_0
    return-void

    .line 2168
    .end local v0    # "visibility":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 348
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "addressForm"

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    .line 352
    if-nez p1, :cond_1

    .line 354
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialCountryI18NDataJson:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)V

    .line 363
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->allowedCountryCode:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->stringArrayToRegionCodeArray([Ljava/lang/String;)[I

    move-result-object v1

    .line 367
    .local v1, "regionCodes":[I
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->scrubAndSortRegionCodes([I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodes:[I

    .line 368
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodes:[I

    array-length v2, v2

    if-gtz v2, :cond_0

    .line 369
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Array length of allowedCountryCodes must be > 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 355
    .end local v1    # "regionCodes":[I
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not construct JSONObject from mAddressForm.initialCountryI18NDataJson"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 372
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "regionCodes":[I
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->recipientLabel:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 373
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Recipient field hint must be specified!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 376
    .end local v1    # "regionCodes":[I
    :cond_1
    const-string v2, "regionCodes"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodes:[I

    .line 378
    :cond_2
    return-void
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x4e

    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 385
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_address_entry:I

    invoke-virtual {p1, v2, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    .line 388
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 389
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->address_title:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 390
    .local v1, "titleText":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 393
    .end local v1    # "titleText":Landroid/widget/TextView;
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->hide_address_checkbox:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    .line 394
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 395
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hideFormFieldsToggleLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 397
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 399
    :cond_1
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    invoke-static {v2, v8}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 400
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_non_editable_text:I

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    .line 410
    :goto_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    invoke-static {v2, v8}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 411
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 413
    :cond_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 414
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_recipient:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 415
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mHideAddressCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 417
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->region_code_view:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    .line 418
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 419
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v2, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setVisibility(I)V

    .line 421
    :cond_3
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->dynamic_address_fields_layout:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    .line 423
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-boolean v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->phoneNumberForm:Z

    if-nez v2, :cond_6

    .line 461
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->setOnHeightOffsetChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;)V

    .line 463
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    return-object v2

    .line 403
    :cond_5
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_edit_text:I

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    .line 405
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    invoke-direct {p0, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getFieldLabel(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRecipientNameText:Landroid/widget/TextView;

    const/16 v3, 0x2061

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setInputType(I)V

    goto/16 :goto_0

    .line 426
    :cond_6
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    invoke-static {v2, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 427
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_non_editable_text:I

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    .line 435
    :goto_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_phone_number:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 436
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_7

    .line 437
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v8, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 439
    :cond_7
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->hiddenField:[I

    invoke-static {v2, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 440
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 442
    :cond_8
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mDynamicAddressFieldsLayout:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 444
    if-nez p3, :cond_4

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 449
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->phoneNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 450
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->initialValue:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setPhoneNumber(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 430
    :cond_9
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_edit_text:I

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    .line 432
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_phone_number:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(I)V

    .line 433
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setInputType(I)V

    goto :goto_2

    .line 452
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 455
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_4

    .line 456
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setPhoneNumber(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 688
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onDestroyView()V

    .line 690
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->resetPendingRequestStatus()V

    .line 691
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 695
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onDetach()V

    .line 697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .line 700
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->cancelPendingAdminAreaMetadataRetrievalRequest()V

    .line 701
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$4;

    invoke-direct {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$4;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/RequestQueue;->cancelAll(Lcom/android/volley/RequestQueue$RequestFilter;)V

    .line 707
    return-void
.end method

.method public onHeightOffsetChanged(F)V
    .locals 1
    .param p1, "heightOffset"    # F

    .prologue
    .line 2157
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2158
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 2160
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    if-eqz v0, :cond_1

    .line 2161
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    invoke-interface {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;->onHeightOffsetChanged(F)V

    .line 2163
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 670
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 672
    const-string v0, "selectedCountry"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 673
    const-string v0, "regionCodes"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mRegionCodes:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 674
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    if-eqz v0, :cond_0

    .line 675
    const-string v0, "pendingAddress"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    if-eqz v0, :cond_1

    .line 678
    const-string v0, "countryData"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mCountryData:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_1
    const-string v0, "languageCode"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 682
    const-string v0, "adminAreaData"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    :cond_2
    return-void
.end method

.method public setAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)V
    .locals 3
    .param p1, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    const/4 v2, 0x0

    .line 792
    if-nez p1, :cond_2

    .line 795
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    .line 796
    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 797
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 798
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 802
    :cond_1
    new-instance p1, Lcom/google/location/country/Postaladdress$PostalAddress;

    .end local p1    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-direct {p1}, Lcom/google/location/country/Postaladdress$PostalAddress;-><init>()V

    .line 806
    .restart local p1    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    :cond_2
    iget-object v1, p1, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toRegionCode(Ljava/lang/String;)I

    move-result v0

    .line 808
    .local v0, "country":I
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 809
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    iget-object v1, v1, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 810
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    iget-object v1, v1, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mLanguageCode:Ljava/lang/String;

    .line 813
    :cond_3
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-nez v1, :cond_4

    .line 815
    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    goto :goto_0

    .line 818
    :cond_4
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-ne v0, v1, :cond_5

    .line 819
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->hasPendingRequests()Z

    move-result v1

    if-nez v1, :cond_0

    .line 821
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->requestAlternativeCountryDataIfAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 825
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setPostalAddressValues(Lcom/google/location/country/Postaladdress$PostalAddress;)V

    .line 826
    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;

    goto :goto_0

    .line 828
    :cond_5
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-nez v1, :cond_6

    .line 830
    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    goto :goto_0

    .line 833
    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setSelectedCountry(I)V

    goto :goto_0
.end method

.method public setOnHeightOffsetChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    .prologue
    .line 1029
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    .line 1030
    return-void
.end method

.method public setOnRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .prologue
    .line 1025
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .line 1026
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 1
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 471
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPhoneNumberText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    return-void
.end method

.method setPostalAddressValues(Lcom/google/location/country/Postaladdress$PostalAddress;)V
    .locals 5
    .param p1, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 878
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mContainer:Landroid/widget/LinearLayout;

    if-nez v4, :cond_1

    .line 887
    :cond_0
    return-void

    .line 882
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "length":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 883
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressFields:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 884
    .local v2, "input":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Character;

    invoke-virtual {v4}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 885
    .local v0, "field":C
    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setInputValue(Landroid/view/View;Ljava/lang/String;)V

    .line 882
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setSelectedCountry(I)V
    .locals 2
    .param p1, "selectedCountry"    # I

    .prologue
    .line 1013
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    if-eq p1, v0, :cond_0

    .line 1014
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mSelectedCountry:I

    .line 1015
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onSelectedCountryChange()V

    .line 1018
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getId()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;->onRegionCodeSelected(II)V

    .line 1022
    :cond_0
    return-void
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 933
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->validate(Z)Z

    move-result v0

    return v0
.end method
