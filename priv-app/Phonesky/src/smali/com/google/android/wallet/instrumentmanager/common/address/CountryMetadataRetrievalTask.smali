.class public Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;
.super Ljava/lang/Object;
.source "CountryMetadataRetrievalTask.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;,
        Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# static fields
.field private static final PUBLIC_ADDRESS_DATA_SERVER:Landroid/net/Uri;


# instance fields
.field private final mDesiredLanguageCode:Ljava/lang/String;

.field private final mErrorListener:Lcom/android/volley/Response$ErrorListener;

.field private final mRegionCode:I

.field private final mRequestQueue:Lcom/android/volley/RequestQueue;

.field private final mResponseListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "https://i18napis.appspot.com/address"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->PUBLIC_ADDRESS_DATA_SERVER:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/RequestQueue;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 0
    .param p1, "requestQueue"    # Lcom/android/volley/RequestQueue;
    .param p2, "regionCode"    # I
    .param p3, "desiredLanguageCode"    # Ljava/lang/String;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/RequestQueue;",
            "I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    .local p4, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lorg/json/JSONObject;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 80
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mRegionCode:I

    .line 81
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mDesiredLanguageCode:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mResponseListener:Lcom/android/volley/Response$Listener;

    .line 83
    iput-object p5, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mErrorListener:Lcom/android/volley/Response$ErrorListener;

    .line 84
    return-void
.end method

.method static synthetic access$000(ILjava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->buildCountryUri(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(ILjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->buildCountryId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildCountryId(ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "regionCode"    # I
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "data"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    .local v0, "id":Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    const-string v1, "--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static buildCountryUri(ILjava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "regionCode"    # I
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->PUBLIC_ADDRESS_DATA_SERVER:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->buildCountryId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 49
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mDesiredLanguageCode:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAlternativeLanguageCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "alternativeLang":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->run(Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mResponseListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v1, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public run(Ljava/lang/String;)V
    .locals 3
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mRegionCode:I

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mErrorListener:Lcom/android/volley/Response$ErrorListener;

    invoke-direct {v0, v1, p1, p0, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;-><init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 97
    .local v0, "request":Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 98
    return-void
.end method
