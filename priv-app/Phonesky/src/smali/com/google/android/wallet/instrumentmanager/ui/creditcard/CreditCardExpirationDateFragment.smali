.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "CreditCardExpirationDateFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;


# instance fields
.field private mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

.field mCreditCardLabel:Landroid/widget/TextView;

.field mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

.field mCvcHintImage:Landroid/view/View;

.field mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

.field public mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

.field mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x673

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-void
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;
    .locals 4
    .param p0, "creditCardExpirationDateForm"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 48
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;-><init>()V

    .line 50
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 51
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "creditCardExpDateForm"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 53
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v1
.end method

.method private validate(Z)Z
    .locals 10
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 94
    const/4 v8, 0x3

    new-array v2, v8, [Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    aput-object v8, v2, v7

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    aput-object v8, v2, v6

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    aput-object v9, v2, v8

    .line 99
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    const/4 v5, 0x1

    .line 100
    .local v5, "valid":Z
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 101
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    if-eqz p1, :cond_2

    .line 102
    invoke-interface {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v5, :cond_1

    move v5, v6

    .line 100
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v5, v7

    .line 102
    goto :goto_1

    .line 103
    :cond_2
    invoke-interface {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v8

    if-nez v8, :cond_0

    .line 107
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    :goto_2
    return v7

    :cond_3
    move v7, v5

    goto :goto_2
.end method


# virtual methods
.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 3
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    .line 130
    iget-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 147
    :goto_0
    return v0

    .line 133
    :cond_0
    iget-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    packed-switch v0, :pswitch_data_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown FormFieldMessage fieldId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 147
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 138
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 141
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected doEnableUi()V
    .locals 2

    .prologue
    .line 154
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->isUiEnabled()Z

    move-result v0

    .line 156
    .local v0, "uiEnabled":Z
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 157
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 158
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 159
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcHintImage:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 161
    .end local v0    # "uiEnabled":Z
    :cond_0
    return-void
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 113
    const/4 v7, 0x3

    new-array v2, v7, [Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v6

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v5

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v8, v2, v7

    .line 119
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 120
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 121
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    .line 125
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_1
    return v5

    .line 119
    .restart local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_1
    move v5, v6

    .line 125
    goto :goto_1
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCreditCardExpirationDateFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;
    .locals 6

    .prologue
    .line 240
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->removeNonNumericDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "cvc":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 249
    .local v3, "expMonth":I
    :goto_0
    :try_start_1
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    add-int/lit16 v4, v5, 0x7d0

    .line 254
    .local v4, "expYear":I
    :goto_1
    new-instance v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    invoke-direct {v2}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;-><init>()V

    .line 256
    .local v2, "expDateFormValue":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;
    iput v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;->newMonth:I

    .line 257
    iput v4, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;->newYear:I

    .line 259
    iput-object v0, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;->cvc:Ljava/lang/String;

    .line 260
    return-object v2

    .line 244
    .end local v2    # "expDateFormValue":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;
    .end local v3    # "expMonth":I
    .end local v4    # "expYear":I
    :catch_0
    move-exception v1

    .line 245
    .local v1, "ex":Ljava/lang/NumberFormatException;
    const/4 v3, 0x0

    .restart local v3    # "expMonth":I
    goto :goto_0

    .line 250
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v1

    .line 251
    .restart local v1    # "ex":Ljava/lang/NumberFormatException;
    const/4 v4, 0x0

    .restart local v4    # "expYear":I
    goto :goto_1
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public isReadyToSubmit()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 265
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcHintImage:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "CvcInfoDialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->newInstance(I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;

    move-result-object v0

    .line 272
    .local v0, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "CvcInfoDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 171
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "creditCardExpDateForm"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    .line 175
    return-void
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 180
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_credit_card_expiration_date:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 182
    .local v9, "content":Landroid/view/View;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->credit_card_label:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardLabel:Landroid/widget/TextView;

    .line 183
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardLabel:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cardLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->card_logo:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    .line 186
    .local v8, "cardImage":Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->isEmbeddedImageUri(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->embeddedImageUriToDrawableResourceId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setDefaultImageResId(I)V

    .line 197
    :goto_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->altText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->altText:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    :cond_0
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_month:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 203
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->exp_year:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 204
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->cvc:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 205
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    new-array v2, v11, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v10

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 208
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->cvc_hint:I

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcHintImage:Landroid/view/View;

    .line 209
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcHintImage:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->cvcLength:I

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;-><init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    .line 212
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minMonth:I

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->minYear:I

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v6, v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxMonth:I

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget v7, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->maxYear:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;-><init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;IIII)V

    .line 216
    .local v0, "expDateChecker":Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    .line 217
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {v1, v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    .line 219
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {v1, v2, v3, v11}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 220
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    invoke-virtual {v1, v2, v3, v10}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 221
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    invoke-virtual {v1, v2, v3, v11}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 223
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCvcChecker:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 224
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 225
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpYearChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V

    .line 227
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mExpMonthChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->doEnableUi()V

    .line 231
    return-object v9

    .line 190
    .end local v0    # "expDateChecker":Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->mCreditCardExpDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v2, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->getImageLoader(Landroid/content/Context;)Lcom/android/volley/toolbox/ImageLoader;

    move-result-object v3

    sget-object v1, Lcom/google/android/wallet/instrumentmanager/config/G$images;->useWebPForFife:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v8, v2, v3, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setFifeImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;Z)V

    .line 195
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_general:I

    invoke-virtual {v8, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setDefaultImageResId(I)V

    goto/16 :goto_0
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->validate(Z)Z

    move-result v0

    return v0
.end method
