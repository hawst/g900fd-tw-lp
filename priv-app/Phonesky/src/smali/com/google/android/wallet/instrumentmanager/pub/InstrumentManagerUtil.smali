.class public final Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerUtil;
.super Ljava/lang/Object;
.source "InstrumentManagerUtil.java"


# direct methods
.method public static createClientToken(Landroid/content/Context;)[B
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;-><init>()V

    .line 27
    .local v0, "clientToken":Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->createRequestContext(Landroid/content/Context;[B)Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ClientToken;->requestContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 28
    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    return-object v1
.end method
