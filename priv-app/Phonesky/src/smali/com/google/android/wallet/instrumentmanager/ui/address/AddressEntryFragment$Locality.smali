.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Locality"
.end annotation


# instance fields
.field final mLabel:Ljava/lang/String;

.field final mValue:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 2027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2028
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;->mValue:Ljava/lang/String;

    .line 2029
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;->mLabel:Ljava/lang/String;

    .line 2030
    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$Locality;->mLabel:Ljava/lang/String;

    return-object v0
.end method
