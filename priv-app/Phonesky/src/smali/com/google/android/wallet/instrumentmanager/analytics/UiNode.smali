.class public interface abstract Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.super Ljava/lang/Object;
.source "UiNode.java"


# virtual methods
.method public abstract getChildren()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.end method

.method public abstract getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
.end method
