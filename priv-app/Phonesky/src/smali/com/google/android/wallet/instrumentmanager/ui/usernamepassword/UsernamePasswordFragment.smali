.class public Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "UsernamePasswordFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;


# instance fields
.field mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

.field mLoginHelpText:Landroid/widget/TextView;

.field mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

.field private mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

.field mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 64
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x690

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-void
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;I)Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;
    .locals 4
    .param p0, "usernamePasswordForm"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 77
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;-><init>()V

    .line 78
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 79
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "usernamePasswordForm"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 81
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->setArguments(Landroid/os/Bundle;)V

    .line 82
    return-object v1
.end method

.method private validate(Z)Z
    .locals 9
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 209
    const/4 v8, 0x2

    new-array v2, v8, [Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v8, v2, v7

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v8, v2, v6

    .line 213
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    const/4 v5, 0x1

    .line 214
    .local v5, "valid":Z
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 215
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    if-eqz p1, :cond_2

    .line 216
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v5, :cond_1

    move v5, v6

    .line 214
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v5, v7

    .line 216
    goto :goto_1

    .line 217
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v8

    if-nez v8, :cond_0

    .line 221
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_2
    return v7

    :cond_3
    move v7, v5

    goto :goto_2
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 322
    const/4 v0, 0x1

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 323
    return-void
.end method

.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 3
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    .line 165
    iget-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    const/4 v0, 0x0

    .line 179
    :goto_0
    return v0

    .line 168
    :cond_0
    iget-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    packed-switch v0, :pswitch_data_0

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown FormFieldMessage fieldId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 179
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 313
    return-void
.end method

.method public doEnableUi()V
    .locals 2

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v1, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->isUiEnabled()Z

    move-result v0

    .line 186
    .local v0, "uiEnabled":Z
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 187
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setEnabled(Z)V

    .line 189
    .end local v0    # "uiEnabled":Z
    :cond_0
    return-void
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 149
    const/4 v7, 0x2

    new-array v2, v7, [Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v6

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    aput-object v7, v2, v5

    .line 154
    .local v2, "fieldsToValidate":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    move-object v0, v2

    .local v0, "arr$":[Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 155
    .local v1, "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 156
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    .line 160
    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_1
    return v5

    .line 154
    .restart local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .end local v1    # "fieldToValidate":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_1
    move v5, v6

    .line 160
    goto :goto_1
.end method

.method public getButtonBarExpandButtonText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getExpandLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->legalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public getUsernamePasswordFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;
    .locals 6

    .prologue
    .line 274
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;-><init>()V

    .line 275
    .local v1, "formValue":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;
    new-instance v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-direct {v3}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;-><init>()V

    iput-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 276
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->usernameField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->name:Ljava/lang/String;

    .line 277
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->username:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->stringValue:Ljava/lang/String;

    .line 278
    new-instance v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-direct {v3}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;-><init>()V

    iput-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 279
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->passwordField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->name:Ljava/lang/String;

    .line 280
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->encryptionType:I

    packed-switch v3, :pswitch_data_0

    .line 300
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported encryption type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->encryptionType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 282
    :pswitch_0
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->vendorSpecificSalt:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 285
    .local v2, "paypalPasswordEncryptor":Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;
    :try_start_0
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->credentialsEncryptionKey:[B

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;->encryptPassword([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->stringValue:Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;->getHashedDeviceId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->hashedDeviceId:Ljava/lang/String;

    .line 303
    .end local v2    # "paypalPasswordEncryptor":Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;
    :goto_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->encryptionType:I

    iput v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->encryptionType:I

    .line 304
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->legalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v3, :cond_0

    .line 305
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->legalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->opaqueData:Ljava/lang/String;

    iput-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->legalDocData:Ljava/lang/String;

    .line 307
    :cond_0
    return-object v1

    .line 288
    .restart local v2    # "paypalPasswordEncryptor":Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/security/cert/CertificateException;
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Unable to encrypt user PayPal credentials"

    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 297
    .end local v0    # "e":Ljava/security/cert/CertificateException;
    .end local v2    # "paypalPasswordEncryptor":Lcom/google/android/wallet/instrumentmanager/common/util/PaypalPasswordEncryptor;
    :pswitch_1
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;->password:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->stringValue:Ljava/lang/String;

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isReadyToSubmit()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onButtonBarExpandButtonClicked()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->expand(Z)V

    .line 245
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 268
    const/16 v0, 0x691

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 271
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-ne p1, v1, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {p2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    move-result-object v0

    .line 261
    .local v0, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "usernamePasswordForm"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    .line 90
    return-void
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "themedInflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 95
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_username_password:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 98
    .local v0, "content":Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->username_password_title:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 100
    .local v1, "titleText":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    .end local v1    # "titleText":Landroid/widget/TextView;
    :cond_0
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->username:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 105
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->usernameField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->applyUiFieldSpecificationToFormEditText(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    .line 107
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernameText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->password:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 110
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->passwordField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->applyUiFieldSpecificationToFormEditText(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mPasswordText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 114
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->login_help_text:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    .line 115
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->loginHelpHtml:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    :goto_0
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->legal_message_text:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    .line 127
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setParentUiNode(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 128
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v2, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setUrlClickListener(Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    .line 129
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->legalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->legalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->messageText:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-virtual {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V

    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->doEnableUi()V

    .line 134
    const/4 v2, 0x1

    sget-object v3, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 135
    return-object v0

    .line 121
    :cond_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mUsernamePasswordForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;->loginHelpHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 123
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLoginHelpText:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 316
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 142
    const/4 v0, 0x6

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 144
    return-void
.end method

.method public shouldShowButtonBarExpandButton()Z
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->validate(Z)Z

    move-result v0

    return v0
.end method
