.class public interface abstract Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;
.super Ljava/lang/Object;
.source "InstrumentManagerAnalyticsEventListener.java"


# virtual methods
.method public abstract onBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V
.end method

.method public abstract onClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)V
.end method

.method public abstract onImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)V
.end method
