.class public Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
.super Ljava/lang/Object;
.source "AddressSourceResult.java"


# static fields
.field public static final DEFAULT_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXCLUDED_ADDRESS_FIELDS:[C

.field public static final NEW_LINE_REPLACEMENT_SEPARATOR:Ljava/lang/String;


# instance fields
.field public final address:Lcom/google/location/country/Postaladdress$PostalAddress;

.field public final description:Ljava/lang/CharSequence;

.field public final matchingTerm:Ljava/lang/String;

.field public final reference:Ljava/lang/String;

.field public final sourceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x52

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->EXCLUDED_ADDRESS_FIELDS:[C

    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->NEW_LINE_REPLACEMENT_SEPARATOR:Ljava/lang/String;

    .line 26
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult$1;

    invoke-direct {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult$1;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->DEFAULT_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2
    .param p1, "matchingTerm"    # Ljava/lang/String;
    .param p2, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p3, "description"    # Ljava/lang/CharSequence;
    .param p4, "sourceName"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source name should not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 80
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    .line 81
    iput-object p4, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->sourceName:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->reference:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "matchingTerm"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/CharSequence;
    .param p3, "sourceName"    # Ljava/lang/String;
    .param p4, "reference"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source name should not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->matchingTerm:Ljava/lang/String;

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 100
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    .line 101
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->sourceName:Ljava/lang/String;

    .line 102
    iput-object p4, p0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->reference:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "matchingTermAndDescription"    # Ljava/lang/String;
    .param p2, "sourceName"    # Ljava/lang/String;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p1, p2, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method
