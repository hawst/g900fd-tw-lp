.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->configureAddressFormEditTextAutocomplete(C[CLjava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

.field final synthetic val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
    .locals 0

    .prologue
    .line 1528
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1532
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .line 1534
    .local v1, "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    iget-object v0, v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->address:Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 1535
    .local v0, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    if-eqz v0, :cond_1

    .line 1536
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fillAddressFieldsAndUpdateFocus(Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V
    invoke-static {v3, v4, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$600(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V

    .line 1546
    :cond_0
    :goto_0
    return-void

    .line 1537
    :cond_1
    iget-object v3, v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->reference:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1538
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;)V

    .line 1539
    .local v2, "task":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1542
    .end local v2    # "task":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1543
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$7;->val$editText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->focusToNextFieldOrHideKeyboard(Landroid/view/View;)V
    invoke-static {v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$700(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;)V

    goto :goto_0
.end method
