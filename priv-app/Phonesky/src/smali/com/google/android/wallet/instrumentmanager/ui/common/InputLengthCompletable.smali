.class public Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;
.super Ljava/lang/Object;
.source "InputLengthCompletable.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;


# instance fields
.field protected final mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mLength:I


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V
    .locals 0
    .param p1, "formEditText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p2, "length"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 16
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;->mLength:I

    .line 17
    return-void
.end method


# virtual methods
.method public isComplete()Z
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;->mLength:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
