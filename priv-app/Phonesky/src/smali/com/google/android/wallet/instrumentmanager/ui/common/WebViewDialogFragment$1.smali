.class Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;
.super Landroid/webkit/WebViewClient;
.source "WebViewDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

.field final synthetic val$initialUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->val$initialUrl:Ljava/lang/String;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x8

    .line 117
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;Z)Z

    .line 122
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v2, 0x8

    .line 107
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->val$initialUrl:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 113
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    invoke-virtual {p1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;Z)Z

    .line 132
    return-void
.end method
