.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$SelectableItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AdminArea"
.end annotation


# instance fields
.field mData:Lorg/json/JSONObject;

.field final mHasLocalities:Z

.field final mI18nKey:Ljava/lang/String;

.field final mLabel:Ljava/lang/String;

.field final mValue:Ljava/lang/String;

.field final mZipStartsWithRegEx:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "i18nKey"    # Ljava/lang/String;
    .param p2, "hasLocalities"    # Z
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "zipStartsWithRegEx"    # Ljava/lang/String;

    .prologue
    .line 2002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2003
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mI18nKey:Ljava/lang/String;

    .line 2004
    iput-boolean p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mHasLocalities:Z

    .line 2005
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mValue:Ljava/lang/String;

    .line 2006
    iput-object p4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mLabel:Ljava/lang/String;

    .line 2007
    iput-object p5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mZipStartsWithRegEx:Ljava/lang/String;

    .line 2008
    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2012
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2017
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;->mLabel:Ljava/lang/String;

    return-object v0
.end method
