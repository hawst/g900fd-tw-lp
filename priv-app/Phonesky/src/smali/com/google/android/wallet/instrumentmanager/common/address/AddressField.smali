.class public final Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;
.super Ljava/lang/Object;
.source "AddressField.java"


# static fields
.field private static final ALL_ADDRESS_FIELDS:[C

.field private static final sFields:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 43
    const/16 v4, 0x11

    new-array v4, v4, [C

    fill-array-data v4, :array_0

    sput-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    .line 66
    new-instance v4, Ljava/util/HashSet;

    sget-object v5, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    array-length v5, v5

    invoke-direct {v4, v5}, Ljava/util/HashSet;-><init>(I)V

    sput-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->sFields:Ljava/util/HashSet;

    .line 67
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    .local v1, "arr$":[C
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-char v0, v1, v2

    .line 68
    .local v0, "addressField":C
    sget-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->sFields:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "addressField":C
    :cond_0
    return-void

    .line 43
    :array_0
    .array-data 2
        0x53s
        0x43s
        0x4es
        0x4fs
        0x31s
        0x32s
        0x33s
        0x44s
        0x5as
        0x58s
        0x41s
        0x55s
        0x46s
        0x50s
        0x54s
        0x42s
        0x52s
    .end array-data
.end method

.method public static count()I
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    array-length v0, v0

    return v0
.end method

.method public static exists(C)Z
    .locals 2
    .param p0, "field"    # C

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->sFields:Ljava/util/HashSet;

    invoke-static {p0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static values()[C
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    sget-object v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->ALL_ADDRESS_FIELDS:[C

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([CI)[C

    move-result-object v0

    return-object v0
.end method
