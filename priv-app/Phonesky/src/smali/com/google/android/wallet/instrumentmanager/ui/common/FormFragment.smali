.class public abstract Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;
.source "FormFragment.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;


# instance fields
.field private mUiEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->mUiEnabled:Z

    return-void
.end method


# virtual methods
.method protected abstract doEnableUi()V
.end method

.method public final enableUi(Z)V
    .locals 0
    .param p1, "enableUi"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->mUiEnabled:Z

    .line 65
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->doEnableUi()V

    .line 66
    return-void
.end method

.method public getButtonBarExpandButtonText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    return-object v0
.end method

.method public handleErrorMessageDismissed(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "errorType"    # I

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public final isUiEnabled()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->mUiEnabled:Z

    return v0
.end method

.method public notifyFormEvent(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "eventType"    # I
    .param p2, "eventDetails"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 44
    return-void
.end method

.method public onButtonBarExpandButtonClicked()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    if-eqz p1, :cond_0

    .line 26
    const-string v0, "uiEnabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->mUiEnabled:Z

    .line 28
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onResume()V

    .line 54
    const/4 v0, 0x4

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 55
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 33
    const-string v0, "uiEnabled"

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->mUiEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 34
    return-void
.end method

.method public shouldShowButtonBarExpandButton()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method
