.class public Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
.super Ljava/lang/Object;
.source "CreditCardEntryAction.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public expDateEntryType:I

.field public expDateOcrEnabled:Z

.field public expDateRecognizedByOcr:Z

.field public expDateValidationErrorOccurred:Z

.field public numOcrAttempts:I

.field public ocrExitReason:I

.field public panEntryType:I

.field public panOcrEnabled:Z

.field public panRecognizedByOcr:Z

.field public panValidationErrorOccurred:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction$1;

    invoke-direct {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction$1;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    .line 48
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panOcrEnabled:Z

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panEntryType:I

    .line 50
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panRecognizedByOcr:Z

    .line 51
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 52
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateOcrEnabled:Z

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    .line 54
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateRecognizedByOcr:Z

    .line 55
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->readBooleanFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->ocrExitReason:I

    .line 58
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private static readBooleanFromParcel(Landroid/os/Parcel;)Z
    .locals 2
    .param p0, "parcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 100
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static writeBooleanToParcel(Landroid/os/Parcel;Z)V
    .locals 1
    .param p0, "parcel"    # Landroid/os/Parcel;
    .param p1, "b"    # Z

    .prologue
    .line 96
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 82
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "panOcrEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panOcrEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\npanEntryType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panEntryType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\npanRecognizedByOcr: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panRecognizedByOcr:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\npanValidationErrorOccurred: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nexpDateOcrEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateOcrEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nexpDateEntryType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nexpDateRecognizedByOcr: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateRecognizedByOcr:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nexpDateValidationErrorOccurred: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nnumOcrAttempts: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nocrExitReason"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->ocrExitReason:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panOcrEnabled:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 68
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panEntryType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panRecognizedByOcr:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 70
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->panValidationErrorOccurred:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 71
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateOcrEnabled:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 72
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateRecognizedByOcr:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 74
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->writeBooleanToParcel(Landroid/os/Parcel;Z)V

    .line 75
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->numOcrAttempts:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->ocrExitReason:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    return-void
.end method
