.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;
.super Landroid/os/AsyncTask;
.source "AddressEntryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FetchAddressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
        "Ljava/lang/Void;",
        "Lcom/google/location/country/Postaladdress$PostalAddress;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCopyOfAddressSources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;",
            ">;"
        }
    .end annotation
.end field

.field private final mCurrentLanguageCode:Ljava/lang/String;

.field private final mInput:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;)V
    .locals 2
    .param p2, "input"    # Landroid/view/View;

    .prologue
    .line 2110
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2111
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mInput:Landroid/view/View;

    .line 2112
    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressLanguageCode()Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$1100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mCurrentLanguageCode:Ljava/lang/String;

    .line 2113
    iget-object v0, p1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressSources:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mCopyOfAddressSources:Ljava/util/ArrayList;

    .line 2116
    return-void

    .line 2113
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 7
    .param p1, "results"    # [Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .prologue
    const/4 v4, 0x0

    .line 2120
    const/4 v5, 0x0

    aget-object v2, p1, v5

    .line 2121
    .local v2, "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    if-eqz v2, :cond_0

    iget-object v5, v2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->sourceName:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 2130
    :cond_0
    :goto_0
    return-object v4

    .line 2124
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mCopyOfAddressSources:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 2125
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mCopyOfAddressSources:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;

    .line 2126
    .local v3, "source":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
    iget-object v5, v2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->sourceName:Ljava/lang/String;

    invoke-interface {v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2127
    iget-object v4, v2, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->reference:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mCurrentLanguageCode:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;->getAddress(Ljava/lang/String;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v4

    goto :goto_0

    .line 2124
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2103
    check-cast p1, [Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->doInBackground([Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/location/country/Postaladdress$PostalAddress;)V
    .locals 2
    .param p1, "result"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 2135
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2143
    :cond_0
    :goto_0
    return-void

    .line 2138
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2140
    if-eqz p1, :cond_0

    .line 2141
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->mInput:Landroid/view/View;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fillAddressFieldsAndUpdateFocus(Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$600(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Landroid/view/View;Lcom/google/location/country/Postaladdress$PostalAddress;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2103
    check-cast p1, Lcom/google/location/country/Postaladdress$PostalAddress;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$FetchAddressTask;->onPostExecute(Lcom/google/location/country/Postaladdress$PostalAddress;)V

    return-void
.end method
