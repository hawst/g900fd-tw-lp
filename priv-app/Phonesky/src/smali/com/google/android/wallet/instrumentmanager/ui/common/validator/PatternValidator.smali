.class public Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.source "PatternValidator.java"


# instance fields
.field private final mPattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V
    .locals 2
    .param p1, "customErrorMessage"    # Ljava/lang/CharSequence;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>(Ljava/lang/CharSequence;)V

    .line 19
    if-nez p2, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pattern must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;->mPattern:Ljava/util/regex/Pattern;

    .line 23
    return-void
.end method


# virtual methods
.method public isValid(Landroid/widget/TextView;)Z
    .locals 2
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 27
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
