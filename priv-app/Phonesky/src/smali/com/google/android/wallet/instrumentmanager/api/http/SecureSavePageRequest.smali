.class public Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;
.super Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;
.source "SecureSavePageRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;",
        "Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public final mRequest:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;[BLcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 6
    .param p1, "apiContext"    # Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;
    .param p2, "request"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    .param p3, "sessionData"    # [B
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;",
            "Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;",
            "[B",
            "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;",
            "Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p4, "responseListener":Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;, "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener<Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;>;"
    const-class v3, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/api/http/SecurePageRequest;-><init>(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;[BLjava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 47
    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->copyFrom(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mRequest:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    .line 48
    return-void
.end method


# virtual methods
.method public getBackgroundEventReceivedType()I
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x2d1

    return v0
.end method

.method protected getPageActionUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "InstrumentManager/SavePage"

    return-object v0
.end method

.method protected getPageValueToEncode()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mRequest:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    return-object v0
.end method

.method public getProtoRequestAsByteArray()[B
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mRequest:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    iget-object v1, v1, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->applicationContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mSessionData:[B

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->createRequestContext(Landroid/content/Context;[B)Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    .line 61
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;->mRequest:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    return-object v0
.end method
