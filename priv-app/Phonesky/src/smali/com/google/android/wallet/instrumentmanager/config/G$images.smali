.class public Lcom/google/android/wallet/instrumentmanager/config/G$images;
.super Ljava/lang/Object;
.source "G.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/config/G;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "images"
.end annotation


# static fields
.field public static final diskCacheSizeBytes:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final inMemoryCacheSizeDp:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final useWebPForFife:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 103
    const-string v0, "wallet.images.use_webp_for_fife"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Z)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->useWebPForFife:Lcom/google/android/gsf/GservicesValue;

    .line 109
    const-string v0, "wallet.images.disk_cache_size_bytes"

    const/high16 v1, 0x200000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->diskCacheSizeBytes:Lcom/google/android/gsf/GservicesValue;

    .line 117
    const-string v0, "wallet.images.in_memory_cache_size_dp"

    const/16 v1, 0x2580

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->inMemoryCacheSizeDp:Lcom/google/android/gsf/GservicesValue;

    return-void
.end method
