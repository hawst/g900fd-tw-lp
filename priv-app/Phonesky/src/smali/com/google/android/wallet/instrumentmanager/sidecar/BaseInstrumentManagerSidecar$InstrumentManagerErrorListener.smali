.class public final Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;
.super Ljava/lang/Object;
.source "BaseInstrumentManagerSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "InstrumentManagerErrorListener"
.end annotation


# instance fields
.field private final mPreviousResponseLogToken:[B

.field private mRequest:Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;[B)V
    .locals 0
    .param p2, "previousResponseLogToken"    # [B

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mPreviousResponseLogToken:[B

    .line 171
    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 10
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x3

    .line 180
    instance-of v1, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    iget-boolean v1, v1, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mAttemptedToHandleAuth:Z

    if-nez v1, :cond_1

    move-object v0, p1

    .line 181
    check-cast v0, Lcom/android/volley/AuthFailureError;

    .line 182
    .local v0, "authFailureError":Lcom/android/volley/AuthFailureError;
    invoke-virtual {v0}, Lcom/android/volley/AuthFailureError;->getResolutionIntent()Landroid/content/Intent;

    move-result-object v9

    .line 183
    .local v9, "resolutionIntent":Landroid/content/Intent;
    if-eqz v9, :cond_0

    .line 186
    invoke-virtual {v9}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v3, -0x10000001

    and-int/2addr v1, v3

    invoke-virtual {v9, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 188
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    const/16 v3, 0x64

    invoke-virtual {v1, v9, v3}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->startActivityForResult(Landroid/content/Intent;I)V

    .line 189
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-virtual {v1, v2, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    .line 192
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mRequest:Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->sendRequest(Lcom/android/volley/Request;)V

    .line 218
    .end local v0    # "authFailureError":Lcom/android/volley/AuthFailureError;
    .end local v9    # "resolutionIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 196
    .restart local v0    # "authFailureError":Lcom/android/volley/AuthFailureError;
    .restart local v9    # "resolutionIntent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    .line 211
    .end local v0    # "authFailureError":Lcom/android/volley/AuthFailureError;
    .end local v9    # "resolutionIntent":Landroid/content/Intent;
    :goto_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mRequest:Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->getBackgroundEventReceivedType()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mRequest:Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;

    invoke-virtual {v4}, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->getClientLatencyMs()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mPreviousResponseLogToken:[B

    invoke-static/range {v1 .. v8}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendResponseReceivedBackgroundEvent(IILjava/lang/String;JJ[B)V

    goto :goto_0

    .line 199
    :cond_1
    instance-of v1, p1, Lcom/android/volley/ServerError;

    if-eqz v1, :cond_2

    .line 200
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-virtual {v1, v4, v4}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_1

    .line 201
    :cond_2
    instance-of v1, p1, Lcom/android/volley/NetworkError;

    if-nez v1, :cond_3

    instance-of v1, p1, Lcom/android/volley/TimeoutError;

    if-eqz v1, :cond_4

    .line 202
    :cond_3
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    const/4 v3, 0x2

    invoke-virtual {v1, v4, v3}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_1

    .line 203
    :cond_4
    instance-of v1, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v1, :cond_5

    .line 205
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_1

    .line 207
    :cond_5
    const-string v1, "BaseInstrumentManagerSidecar"

    const-string v3, "Unexpected error returned from Volley"

    invoke-static {v1, v3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;

    invoke-virtual {v1, v4, v4}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_1
.end method

.method public setRequest(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "request":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<*>;"
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;->mRequest:Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;

    .line 175
    return-void
.end method
