.class public Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
.super Landroid/widget/AutoCompleteTextView;
.source "FormEditText.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ValidatableComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;
    }
.end annotation


# static fields
.field private static final PATTERN_DIGITS_ONLY:Ljava/util/regex/Pattern;


# instance fields
.field private mAutoAdvanceCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

.field private mAutoAdvanceValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

.field private mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

.field private mBlockCompletion:Z

.field private mCachedThreshold:I

.field private final mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

.field private mMaxFieldLength:I

.field private final mOnTextChangeValidationTextWatcher:Landroid/text/TextWatcher;

.field private mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

.field private mRequired:Z

.field private mRequiredError:Ljava/lang/String;

.field private mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

.field private mSavedError:Ljava/lang/CharSequence;

.field private final mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

.field mTextWatcherList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mTextWatcherWrapper:Landroid/text/TextWatcher;

.field private mValidateWhenNotVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "\\d*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->PATTERN_DIGITS_ONLY:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 87
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    .line 71
    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mValidateWhenNotVisible:Z

    .line 72
    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    .line 73
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mMaxFieldLength:I

    .line 464
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOnTextChangeValidationTextWatcher:Landroid/text/TextWatcher;

    .line 483
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherWrapper:Landroid/text/TextWatcher;

    .line 88
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 89
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 90
    iput-object p0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 92
    invoke-direct {p0, p1, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->readAttributesAndAddListeners(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    .line 71
    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mValidateWhenNotVisible:Z

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    .line 73
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mMaxFieldLength:I

    .line 464
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOnTextChangeValidationTextWatcher:Landroid/text/TextWatcher;

    .line 483
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherWrapper:Landroid/text/TextWatcher;

    .line 97
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 98
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 99
    iput-object p0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 101
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->readAttributesAndAddListeners(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    .line 71
    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mValidateWhenNotVisible:Z

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    .line 73
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mMaxFieldLength:I

    .line 464
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOnTextChangeValidationTextWatcher:Landroid/text/TextWatcher;

    .line 483
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherWrapper:Landroid/text/TextWatcher;

    .line 106
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 107
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v1, v2, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    .line 108
    iput-object p0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->readAttributesAndAddListeners(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method private getTextWatcherList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/text/TextWatcher;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherList:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherList:Ljava/util/LinkedList;

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherList:Ljava/util/LinkedList;

    return-object v0
.end method

.method private onRequiredChanged(Z)V
    .locals 2
    .param p1, "replaceExistingValidator"    # Z

    .prologue
    .line 450
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->removeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    .line 454
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    if-eqz v0, :cond_3

    .line 455
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    if-nez v0, :cond_1

    .line 456
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 462
    :cond_2
    :goto_0
    return-void

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    if-eqz v0, :cond_2

    .line 460
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->removeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    goto :goto_0
.end method

.method private readAttributesAndAddListeners(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v10, 0x7fffffff

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 179
    new-array v1, v9, [I

    const v7, 0x1010160

    aput v7, v1, v8

    .line 180
    .local v1, "maxLengthAttrArray":[I
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 181
    .local v2, "maxLengthTypedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v8, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mMaxFieldLength:I

    .line 182
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 184
    sget-object v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText:[I

    invoke-virtual {p1, p2, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 186
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_required:I

    invoke-virtual {v4, v7, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    .line 187
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_validateWhenNotVisible:I

    invoke-virtual {v4, v7, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mValidateWhenNotVisible:Z

    .line 189
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_validatorErrorString:I

    invoke-virtual {v4, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 193
    .local v5, "validatorErrorString":Ljava/lang/String;
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_validatorType:I

    invoke-virtual {v4, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 195
    .local v3, "testType":I
    packed-switch v3, :pswitch_data_0

    .line 217
    const/4 v6, 0x0

    .line 221
    .end local v5    # "validatorErrorString":Ljava/lang/String;
    .local v6, "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    :goto_0
    iget-boolean v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    if-eqz v7, :cond_1

    .line 222
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_requiredErrorString:I

    invoke-virtual {v4, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    .line 224
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 225
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_field_must_not_be_empty:I

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequiredError:Ljava/lang/String;

    .line 228
    :cond_0
    invoke-direct {p0, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->onRequiredChanged(Z)V

    .line 231
    :cond_1
    if-eqz v6, :cond_2

    .line 232
    invoke-virtual {p0, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 234
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getImeOptions()I

    move-result v7

    const/high16 v8, 0x2000000

    or-int/2addr v7, v8

    const/high16 v8, 0x10000000

    or-int/2addr v7, v8

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setImeOptions(I)V

    .line 242
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextWatcherWrapper:Landroid/text/TextWatcher;

    invoke-super {p0, v7}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 243
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOnTextChangeValidationTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListenerToFront(Landroid/text/TextWatcher;)V

    .line 245
    invoke-virtual {p0, v10}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setThreshold(I)V

    .line 246
    return-void

    .line 197
    .end local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    .restart local v5    # "validatorErrorString":Ljava/lang/String;
    :pswitch_0
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    sget v7, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_only_numeric_digits_allowed:I

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .end local v5    # "validatorErrorString":Ljava/lang/String;
    :cond_3
    sget-object v7, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->PATTERN_DIGITS_ONLY:Ljava/util/regex/Pattern;

    invoke-direct {v6, v5, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    .line 200
    .restart local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setInputType(I)V

    goto :goto_0

    .line 203
    .end local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    .restart local v5    # "validatorErrorString":Ljava/lang/String;
    :pswitch_1
    sget v7, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImFormEditText_validatorRegexp:I

    invoke-virtual {v4, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "customRegexp":Ljava/lang/String;
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    .line 207
    .restart local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    goto :goto_0

    .line 209
    .end local v0    # "customRegexp":Ljava/lang/String;
    .end local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    :pswitch_2
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    sget v7, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_email_address_invalid:I

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .end local v5    # "validatorErrorString":Ljava/lang/String;
    :cond_4
    sget-object v7, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-direct {v6, v5, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    .line 212
    .restart local v6    # "xmlValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    const/16 v7, 0x21

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setInputType(I)V

    goto/16 :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private shouldShowAutocomplete()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 438
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 439
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 440
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-int v0, v3

    .line 441
    .local v0, "density":I
    if-nez v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/2addr v3, v0

    const/16 v4, 0x8c

    if-le v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addOnTextChangeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validator"    # Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 312
    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 290
    return-void
.end method

.method public addTextChangedListenerToFront(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 299
    return-void
.end method

.method public addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validator"    # Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 304
    return-void
.end method

.method announceError()V
    .locals 2

    .prologue
    .line 365
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/util/AndroidUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getErrorTextForAccessibility()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 369
    :cond_0
    return-void
.end method

.method public beginBatchEdit()V
    .locals 1

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 524
    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->beginBatchEdit()V

    .line 525
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 539
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 543
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 546
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getErrorTextForAccessibility()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 549
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V
    .locals 2
    .param p1, "completable"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;
    .param p2, "validatable"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    .param p3, "autoRetreat"    # Z

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvanceCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvanceValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-ne v1, p2, :cond_0

    .line 140
    :goto_0
    return-void

    .line 132
    :cond_0
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;-><init>(Landroid/widget/EditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 134
    .local v0, "newAutoAdvanceListener":Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    if-nez v1, :cond_1

    .line 135
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 137
    :cond_1
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    .line 138
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvanceCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

    .line 139
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvanceValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    goto :goto_0
.end method

.method public enoughToFilter()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mBlockCompletion:Z

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x0

    .line 272
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    move-result v0

    goto :goto_0
.end method

.method getErrorTextForAccessibility()Ljava/lang/String;
    .locals 5

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_accessibility_event_form_field_error:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mValidateWhenNotVisible:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->isValid(Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 529
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 530
    .local v0, "target":Landroid/view/inputmethod/InputConnection;
    if-eqz v0, :cond_0

    .line 531
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Landroid/view/inputmethod/InputConnection;Z)V

    .line 533
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 400
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    .line 403
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 404
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->announceError()V

    .line 406
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 407
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 411
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onMeasure(II)V

    .line 414
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mCachedThreshold:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setThreshold(I)V

    .line 415
    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1, "textWatcher"    # Landroid/text/TextWatcher;

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 295
    return-void
.end method

.method public removeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validator"    # Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->remove(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->remove(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 332
    return-void
.end method

.method public replaceTextAndPreventFilter(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mBlockCompletion:Z

    .line 259
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->replaceText(Ljava/lang/CharSequence;)V

    .line 264
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mBlockCompletion:Z

    .line 265
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setAutoAdvancedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->setAutoAdvancedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;)V

    .line 162
    :cond_0
    return-void
.end method

.method public setOnOutOfFocusValidatable(Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V
    .locals 0
    .param p1, "validatable"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mOutOfFocusValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 396
    return-void
.end method

.method public setRequired(Z)V
    .locals 1
    .param p1, "required"    # Z

    .prologue
    .line 376
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mRequired:Z

    .line 377
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->onRequiredChanged(Z)V

    .line 378
    return-void
.end method

.method public setThreshold(I)V
    .locals 1
    .param p1, "threshold"    # I

    .prologue
    .line 419
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mCachedThreshold:I

    .line 420
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->shouldShowAutocomplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 426
    :goto_0
    return-void

    .line 423
    :cond_0
    const v0, 0x7fffffff

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 424
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->dismissDropDown()V

    goto :goto_0
.end method

.method public validate()Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    .line 343
    .local v0, "valid":Z
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mFocusChangeAndValidateableValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 348
    :cond_0
    :goto_0
    return v0

    .line 345
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 346
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
