.class final Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$1;
.super Ljava/lang/Object;
.source "FormSpinner.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;)V
    .locals 3
    .param p1, "spinner"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    .param p2, "selectedView"    # Landroid/view/View;

    .prologue
    .line 55
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 56
    check-cast p2, Landroid/widget/TextView;

    .end local p2    # "selectedView":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 60
    :goto_0
    return-void

    .line 58
    .restart local p2    # "selectedView":Landroid/view/View;
    :cond_0
    const-string v0, "FormSpinner"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot clear error on view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getError(Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "selectedView"    # Landroid/view/View;

    .prologue
    .line 64
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 65
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "selectedView":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .restart local p1    # "selectedView":Landroid/view/View;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "spinner"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
    .param p2, "selectedView"    # Landroid/view/View;
    .param p3, "error"    # Ljava/lang/CharSequence;

    .prologue
    .line 46
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 47
    check-cast p2, Landroid/widget/TextView;

    .end local p2    # "selectedView":Landroid/view/View;
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 51
    :goto_0
    return-void

    .line 49
    .restart local p2    # "selectedView":Landroid/view/View;
    :cond_0
    const-string v0, "FormSpinner"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot set error on view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
