.class public Lcom/google/android/wallet/instrumentmanager/common/address/LocalAddressSource;
.super Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;
.source "LocalAddressSource.java"


# instance fields
.field private final mRawAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    const-string v0, "LocalAddressSource"

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/LocalAddressSource;->mRawAddresses:Ljava/util/ArrayList;

    .line 21
    return-void
.end method


# virtual methods
.method protected getAddresses()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/LocalAddressSource;->mRawAddresses:Ljava/util/ArrayList;

    return-object v0
.end method
