.class Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.source "CreditCardNumberEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->initializeViewAndListeners(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/CharSequence;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/CharSequence;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-direct {p0, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public isValid(Landroid/widget/TextView;)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Landroid/util/Pair;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->isLuhnChecksumValid()Z
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
