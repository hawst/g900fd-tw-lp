.class public Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;
.super Lcom/android/volley/DefaultRetryPolicy;
.source "AuthHandlingRetryPolicy.java"


# instance fields
.field private final mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

.field private mHadAuthException:Z


# direct methods
.method public constructor <init>(ILcom/google/android/wallet/instrumentmanager/api/http/ApiContext;)V
    .locals 2
    .param p1, "initialTimeoutMs"    # I
    .param p2, "context"    # Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    .prologue
    .line 27
    const/4 v0, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/android/volley/DefaultRetryPolicy;-><init>(IIF)V

    .line 28
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    .line 29
    return-void
.end method


# virtual methods
.method public retry(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    .prologue
    .line 33
    instance-of v1, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/android/volley/AuthFailureError;

    .line 37
    .local v0, "authError":Lcom/android/volley/AuthFailureError;
    invoke-virtual {v0}, Lcom/android/volley/AuthFailureError;->getResolutionIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;->mHadAuthException:Z

    if-eqz v1, :cond_1

    .line 38
    :cond_0
    throw p1

    .line 41
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;->mHadAuthException:Z

    .line 42
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->invalidateAuthToken()V

    .line 43
    invoke-super {p0, p1}, Lcom/android/volley/DefaultRetryPolicy;->retry(Lcom/android/volley/VolleyError;)V

    .line 47
    return-void

    .line 45
    .end local v0    # "authError":Lcom/android/volley/AuthFailureError;
    :cond_2
    throw p1
.end method
