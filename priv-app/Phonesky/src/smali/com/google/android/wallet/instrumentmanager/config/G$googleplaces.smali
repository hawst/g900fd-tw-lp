.class public Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;
.super Ljava/lang/Object;
.source "G.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/config/G;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "googleplaces"
.end annotation


# static fields
.field public static final supportedCountries:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final thresholdAddressLine1:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final thresholdDefault:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-string v0, "wallet.google_places_autocomplete_supported_countries"

    const-string v1, "CA,FR,DE,US"

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->supportedCountries:Lcom/google/android/gsf/GservicesValue;

    .line 42
    const-string v0, "wallet.google_places_autocomplete_threshold_address_line_1"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->thresholdAddressLine1:Lcom/google/android/gsf/GservicesValue;

    .line 50
    const-string v0, "wallet.google_places_autocomplete_threshold_default"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->thresholdDefault:Lcom/google/android/gsf/GservicesValue;

    return-void
.end method
