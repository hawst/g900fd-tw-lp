.class public final Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;
.super Ljava/lang/Object;
.source "InstrumentManagerBackgroundEvent.java"


# instance fields
.field public final backgroundEventType:I

.field public final clientLatencyMs:J

.field public final creditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

.field public final exceptionType:Ljava/lang/String;

.field public final integratorLogToken:[B

.field public final resultCode:I

.field public final serverLatencyMs:J


# direct methods
.method public constructor <init>(IILjava/lang/String;JJ[B)V
    .locals 2
    .param p1, "backgroundEventType"    # I
    .param p2, "resultCode"    # I
    .param p3, "exceptionType"    # Ljava/lang/String;
    .param p4, "clientLatencyMs"    # J
    .param p6, "serverLatencyMs"    # J
    .param p8, "integratorLogToken"    # [B

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->backgroundEventType:I

    .line 80
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->resultCode:I

    .line 81
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->exceptionType:Ljava/lang/String;

    .line 82
    iput-wide p4, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->clientLatencyMs:J

    .line 83
    iput-wide p6, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->serverLatencyMs:J

    .line 84
    iput-object p8, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->integratorLogToken:[B

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->creditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;[B)V
    .locals 4
    .param p1, "creditCardEntryAction"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    .param p2, "integratorLogToken"    # [B

    .prologue
    const-wide/16 v2, -0x1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/16 v0, 0x302

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->backgroundEventType:I

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->resultCode:I

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->exceptionType:Ljava/lang/String;

    .line 93
    iput-wide v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->clientLatencyMs:J

    .line 94
    iput-wide v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->serverLatencyMs:J

    .line 95
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->integratorLogToken:[B

    .line 96
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->creditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 97
    return-void
.end method
