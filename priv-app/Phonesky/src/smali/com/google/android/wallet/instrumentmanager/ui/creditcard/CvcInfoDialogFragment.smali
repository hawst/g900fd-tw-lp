.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;
.source "CvcInfoDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;
    .locals 2
    .param p0, "themeResourceId"    # I

    .prologue
    .line 26
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;-><init>()V

    .line 27
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->getThemedLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_cvc_information:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 37
    .local v0, "rootView":Landroid/view/View;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcInfoDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_close:I

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
