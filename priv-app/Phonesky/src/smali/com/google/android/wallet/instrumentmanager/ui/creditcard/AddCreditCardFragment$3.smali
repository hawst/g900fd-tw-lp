.class Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;
.super Ljava/lang/Object;
.source "AddCreditCardFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->onViewStateRestored(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V
    .locals 0

    .prologue
    .line 897
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 912
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 899
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v1, 0x1

    .line 903
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    move-result-object v0

    iget v0, v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    if-eq v0, v1, :cond_0

    .line 905
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->mCreditCardEntryAction:Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    move-result-object v0

    iput v1, v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;->expDateEntryType:I

    .line 907
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->sendCreditCardEntryActionBackgroundEvent()V
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;)V

    .line 909
    :cond_0
    return-void
.end method
