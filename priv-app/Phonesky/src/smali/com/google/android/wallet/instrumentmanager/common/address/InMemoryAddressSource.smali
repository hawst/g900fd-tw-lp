.class public abstract Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;
.super Ljava/lang/Object;
.source "InMemoryAddressSource.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;


# instance fields
.field mAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final mMergedAddressesByFields:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mName:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mMergedAddressesByFields:Ljava/util/HashMap;

    .line 33
    return-void
.end method

.method private allowMatchingTermOnly(C)Z
    .locals 1
    .param p1, "addressField"    # C

    .prologue
    .line 88
    const/16 v0, 0x4e

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getAddress(Ljava/lang/String;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 3
    .param p1, "reference"    # Ljava/lang/String;
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not use reference identifiers"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract getAddresses()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public final getAddresses(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    .locals 21
    .param p1, "prefix"    # Ljava/lang/CharSequence;
    .param p2, "addressField"    # C
    .param p3, "remainingFields"    # [C
    .param p4, "regionCode"    # I
    .param p5, "languageCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "C[CI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->initializeIfNecessary()V

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mAddresses:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_0

    .line 97
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .line 171
    :goto_0
    return-object v7

    .line 100
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v7, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->getName()Ljava/lang/String;

    move-result-object v18

    .line 102
    .local v18, "sourceName":Ljava/lang/String;
    invoke-static/range {p4 .. p4}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v9

    .line 103
    .local v9, "countryNameCode":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->getMergedAddressesByRemainingFields([C)Ljava/util/ArrayList;

    move-result-object v17

    .line 105
    .local v17, "mergedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    new-instance v3, Ljava/util/TreeMap;

    sget-object v19, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 107
    .local v3, "additionalMatchedTerms":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->allowMatchingTermOnly(C)Z

    move-result v16

    .line 108
    .local v16, "matchingTermOnlyAllowed":Z
    const/4 v13, 0x0

    .local v13, "i":I
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v15

    .local v15, "length":I
    :goto_1
    if-ge v13, v15, :cond_a

    .line 109
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 110
    .local v4, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    if-nez v4, :cond_2

    .line 108
    :cond_1
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 113
    :cond_2
    move/from16 v0, p2

    invoke-static {v4, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v12

    .line 115
    .local v12, "formattedAddressValue":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v12, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/SourceUtils;->startsWithOrContainsWordPrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 118
    const/4 v8, 0x0

    .line 119
    .local v8, "copied":Z
    if-eqz p4, :cond_5

    .line 120
    iget-object v0, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    iget-object v0, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->safeToRegionCode(Ljava/lang/String;)I

    move-result v5

    .line 123
    .local v5, "addressCountry":I
    :goto_3
    if-eqz v5, :cond_4

    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v19

    move/from16 v0, v19

    if-eq v5, v0, :cond_4

    .line 125
    move/from16 v0, p4

    if-eq v5, v0, :cond_5

    .line 126
    if-eqz v16, :cond_1

    .line 127
    invoke-virtual {v3, v12}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 128
    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v12, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 120
    .end local v5    # "addressCountry":I
    :cond_3
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v5

    goto :goto_3

    .line 133
    .restart local v5    # "addressCountry":I
    :cond_4
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v19

    move/from16 v0, p4

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 135
    if-eqz v8, :cond_6

    .line 136
    :goto_4
    iput-object v9, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    .line 137
    const/4 v8, 0x1

    .line 140
    .end local v5    # "addressCountry":I
    :cond_5
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 141
    iget-object v0, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 142
    iget-object v6, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    .line 143
    .local v6, "addressLanguageCode":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-static {v6, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 144
    if-eqz v16, :cond_1

    .line 145
    invoke-virtual {v3, v12}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 146
    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v12, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 135
    .end local v6    # "addressLanguageCode":Ljava/lang/String;
    .restart local v5    # "addressCountry":I
    :cond_6
    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->copyFrom(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v19

    check-cast v19, Lcom/google/location/country/Postaladdress$PostalAddress;

    move-object/from16 v4, v19

    goto :goto_4

    .line 153
    .end local v5    # "addressCountry":I
    :cond_7
    if-eqz v8, :cond_9

    .line 154
    :goto_5
    move-object/from16 v0, p5

    iput-object v0, v4, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    .line 155
    const/4 v8, 0x1

    .line 158
    :cond_8
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v12, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v19, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->NEW_LINE_REPLACEMENT_SEPARATOR:Ljava/lang/String;

    sget-object v20, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->EXCLUDED_ADDRESS_FIELDS:[C

    move-object/from16 v0, v19

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddress(Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v10

    .line 162
    .local v10, "description":Ljava/lang/String;
    new-instance v19, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v12, v4, v10, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;-><init>(Ljava/lang/String;Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 153
    .end local v10    # "description":Ljava/lang/String;
    :cond_9
    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->copyFrom(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v19

    check-cast v19, Lcom/google/location/country/Postaladdress$PostalAddress;

    move-object/from16 v4, v19

    goto :goto_5

    .line 165
    .end local v4    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v8    # "copied":Z
    .end local v12    # "formattedAddressValue":Ljava/lang/String;
    :cond_a
    invoke-virtual {v3}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_6
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 166
    .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_b

    .line 167
    new-instance v20, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 170
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_c
    sget-object v19, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->DEFAULT_COMPARATOR:Ljava/util/Comparator;

    move-object/from16 v0, v19

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method getMergedAddressesByRemainingFields([C)Ljava/util/ArrayList;
    .locals 8
    .param p1, "remainingFields"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([C)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    if-eqz p1, :cond_1

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    .line 60
    .local v4, "remainingFieldsKey":Ljava/lang/String;
    :goto_0
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mMergedAddressesByFields:Ljava/util/HashMap;

    monitor-enter v6

    .line 61
    :try_start_0
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mMergedAddressesByFields:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 63
    .local v3, "mergedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    if-nez v3, :cond_3

    .line 64
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mAddresses:Ljava/util/ArrayList;

    invoke-static {v5, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->mergeAddresses(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v3

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "length":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 69
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 70
    .local v0, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    iget-object v5, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v5, v5

    const/4 v7, 0x1

    if-ne v5, v7, :cond_0

    .line 71
    iget-object v5, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    const-string v7, ""

    invoke-static {v5, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->appendToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    iput-object v5, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    .end local v0    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "mergedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    .end local v4    # "remainingFieldsKey":Ljava/lang/String;
    :cond_1
    const-string v4, "*"

    goto :goto_0

    .line 74
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    .restart local v3    # "mergedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    .restart local v4    # "remainingFieldsKey":Ljava/lang/String;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mMergedAddressesByFields:Ljava/util/HashMap;

    invoke-virtual {v5, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_3
    monitor-exit v6

    return-object v3

    .line 77
    .end local v3    # "mergedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mName:Ljava/lang/String;

    return-object v0
.end method

.method declared-synchronized initializeIfNecessary()V
    .locals 4

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mAddresses:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 44
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->getAddresses()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 49
    .local v0, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :goto_0
    if-eqz v0, :cond_1

    .line 50
    const/4 v2, 0x0

    :try_start_2
    invoke-static {v0, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->mergeAddresses(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mAddresses:Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 55
    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Could not retrieve addresses"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 47
    const/4 v0, 0x0

    .restart local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    goto :goto_0

    .line 52
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;->mAddresses:Ljava/util/ArrayList;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 41
    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
