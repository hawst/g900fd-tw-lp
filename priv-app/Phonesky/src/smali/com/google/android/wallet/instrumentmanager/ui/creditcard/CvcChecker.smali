.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;
.super Ljava/lang/Object;
.source "CvcChecker.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# instance fields
.field private final mCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

.field private final mContext:Landroid/content/Context;

.field private final mCvcLength:I

.field private final mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mMaxFieldLength:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cvcText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p3, "cvcLength"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .line 40
    iput p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcLength:I

    .line 41
    iput p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mMaxFieldLength:I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cvcText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p3, "cardNumberText"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;
    .param p4, "maxFieldLength"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mContext:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 31
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcLength:I

    .line 33
    iput p4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mMaxFieldLength:I

    .line 34
    return-void
.end method

.method private isLengthCorrect()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcLength:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 49
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iget v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcLength:I

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 51
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCardNumberText:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCardType()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v0

    .line 52
    .local v0, "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    iget v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->cvcLength:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public isComplete()Z
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->isLengthCorrect()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mMaxFieldLength:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->isLengthCorrect()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validate()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 62
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    :goto_0
    return v0

    .line 66
    :cond_0
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->isLengthCorrect()Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mContext:Landroid/content/Context;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_cvc_invalid:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CvcChecker;->mCvcText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 75
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
