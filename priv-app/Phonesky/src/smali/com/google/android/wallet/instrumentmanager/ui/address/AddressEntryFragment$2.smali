.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

.field final synthetic val$addressesToPotentiallyFillAdminArea:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->val$addressesToPotentiallyFillAdminArea:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 585
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->onResponse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONObject;)V
    .locals 9
    .param p1, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 588
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->indicatePendingRequest(Z)V

    .line 591
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getRegionCodeFromAddressData(Lorg/json/JSONObject;)I

    move-result v6

    .line 593
    .local v6, "regionCode":I
    invoke-static {v6}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v2

    .line 594
    .local v2, "countryNameCode":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 597
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->val$addressesToPotentiallyFillAdminArea:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 598
    .local v0, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    if-eqz v0, :cond_0

    iget-object v7, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 603
    iget-object v5, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 604
    .local v5, "postalCode":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 605
    invoke-static {p1, v5}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAdminAreaForPostalCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 608
    .local v1, "adminArea":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 609
    iput-object v1, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    goto :goto_0

    .line 617
    .end local v0    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v1    # "adminArea":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "postalCode":Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDefaultPostalCodeForCountry(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    .line 622
    .local v3, "defaultPostalCode":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 624
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 625
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iget-object v7, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iget-object v7, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iget-object v7, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 629
    :cond_2
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iput-object v3, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 639
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->onReceivedCountryData(Lorg/json/JSONObject;)V
    invoke-static {v7, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$300(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lorg/json/JSONObject;)V

    .line 640
    return-void

    .line 632
    :cond_4
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFieldValuesForPrefilling()Landroid/util/SparseArray;
    invoke-static {v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Landroid/util/SparseArray;

    move-result-object v8

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getPostalAddressFromFieldValues(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$200(Landroid/util/SparseArray;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v8

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$002(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/location/country/Postaladdress$PostalAddress;)Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 634
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iput-object v3, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 635
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mPendingAddress:Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v7

    iput-object v2, v7, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    goto :goto_1
.end method
