.class public Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;
.super Lcom/android/volley/toolbox/NetworkImageView;
.source "FifeNetworkImageView.java"


# instance fields
.field private mFadeIn:Z

.field private mFadeInDuration:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/android/volley/toolbox/NetworkImageView;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/android/volley/toolbox/NetworkImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/android/volley/toolbox/NetworkImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method private static createFifeUrl(Ljava/lang/String;IIZ)Ljava/lang/String;
    .locals 6
    .param p0, "baseUrl"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "preferWebP"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    if-eqz p3, :cond_0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    move v0, v2

    .line 88
    .local v0, "useWebP":Z
    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "-rw"

    .line 89
    .local v1, "webPFifeArg":Ljava/lang/String;
    :goto_1
    const-string v4, "%s=w%d-h%d-e365%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x3

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .end local v0    # "useWebP":Z
    .end local v1    # "webPFifeArg":Ljava/lang/String;
    :cond_0
    move v0, v3

    .line 87
    goto :goto_0

    .line 88
    .restart local v0    # "useWebP":Z
    :cond_1
    const-string v1, ""

    goto :goto_1
.end method


# virtual methods
.method public setFadeIn(Z)V
    .locals 2
    .param p1, "fadeIn"    # Z

    .prologue
    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeIn:Z

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeIn:Z

    .line 49
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeIn:Z

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeInDuration:I

    goto :goto_0
.end method

.method public setFifeImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;Z)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "imageLoader"    # Lcom/android/volley/toolbox/ImageLoader;
    .param p3, "preferWebP"    # Z

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getWidth()I

    move-result v1

    .line 71
    .local v1, "desiredWidth":I
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getHeight()I

    move-result v0

    .line 73
    .local v0, "desiredHeight":I
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 76
    .local v2, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v2, :cond_0

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v3, :cond_0

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v3, :cond_0

    .line 77
    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 78
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 83
    .end local v2    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    invoke-static {p1, v1, v0, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->createFifeUrl(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;)V

    .line 84
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/NetworkImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    iget-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeIn:Z

    if-eqz v3, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 101
    .local v0, "fullyVisible":Z
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    .line 102
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->getAlpha()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    move v0, v1

    .line 104
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 105
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->mFadeInDuration:I

    int-to-long v4, v1

    invoke-static {p0, v2, v2, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;IIJ)V

    .line 109
    .end local v0    # "fullyVisible":Z
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 100
    goto :goto_0

    .restart local v0    # "fullyVisible":Z
    :cond_3
    move v0, v2

    .line 102
    goto :goto_1
.end method
