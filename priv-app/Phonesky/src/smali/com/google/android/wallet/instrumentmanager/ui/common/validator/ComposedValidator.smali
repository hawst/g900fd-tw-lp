.class public abstract Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.source "ComposedValidator.java"


# instance fields
.field protected final mValidators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/lang/CharSequence;[Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 2
    .param p1, "customErrorMessage"    # Ljava/lang/CharSequence;
    .param p2, "validators"    # [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>(Ljava/lang/CharSequence;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->mValidators:Ljava/util/ArrayList;

    .line 23
    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validator"    # Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 26
    if-eqz p1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->mValidators:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    :cond_0
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->mValidators:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public remove(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validator"    # Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 32
    if-eqz p1, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->mValidators:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 35
    :cond_0
    return-void
.end method
