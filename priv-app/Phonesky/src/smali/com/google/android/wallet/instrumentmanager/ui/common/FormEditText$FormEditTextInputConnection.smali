.class Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "FormEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FormEditTextInputConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 0
    .param p2, "target"    # Landroid/view/inputmethod/InputConnection;
    .param p3, "mutable"    # Z

    .prologue
    .line 554
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 555
    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 556
    return-void
.end method


# virtual methods
.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .prologue
    .line 560
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;
    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 561
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 562
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 563
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$FormEditTextInputConnection;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$100(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 565
    :cond_0
    return v0
.end method
