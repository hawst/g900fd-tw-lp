.class public Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;
.super Landroid/widget/TextView;
.source "InfoMessageTextView.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;


# instance fields
.field private mExpanded:Z

.field private mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

.field private mInlineExpandLabel:Z

.field private mParentUiNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

.field private mRequestedVisibility:I

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

.field private mUrlClickListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInlineExpandLabel:Z

    .line 42
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 47
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x65b

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 52
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getVisibility()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInlineExpandLabel:Z

    .line 42
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 47
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x65b

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getVisibility()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x1

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInlineExpandLabel:Z

    .line 42
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 47
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x65b

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getVisibility()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 65
    return-void
.end method

.method private readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 68
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImInfoMessageTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 70
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImInfoMessageTextView_inlineExpandLabel:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInlineExpandLabel:Z

    .line 73
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 74
    return-void
.end method

.method private updateContent()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 111
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-nez v1, :cond_0

    .line 112
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iput-boolean v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 134
    :goto_0
    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mRequestedVisibility:I

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 135
    return-void

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->detailedMessageHtml:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 117
    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->detailedMessageHtml:Ljava/lang/String;

    invoke-static {p0, v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->clickify(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    goto :goto_0

    .line 119
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInlineExpandLabel:Z

    if-eqz v1, :cond_2

    .line 121
    const-string v1, "%s <a href=\"%s\">%s</a>"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->messageHtml:Ljava/lang/String;

    aput-object v4, v2, v3

    const-string v3, "expandInfoText"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->showDetailedMessageLabel:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "source":Ljava/lang/String;
    invoke-static {p0, v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->clickify(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    goto :goto_0

    .line 127
    .end local v0    # "source":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->messageHtml:Ljava/lang/String;

    invoke-static {p0, v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->clickify(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    goto :goto_0

    .line 130
    :cond_3
    iput-boolean v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 131
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->messageHtml:Ljava/lang/String;

    invoke-static {p0, v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->clickify(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    goto :goto_0
.end method

.method private static verifyInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V
    .locals 2
    .param p0, "infoMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .prologue
    .line 218
    if-nez p0, :cond_1

    .line 229
    :cond_0
    return-void

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->messageHtml:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Info message must contain messageHtml."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->detailedMessageHtml:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->showDetailedMessageLabel:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Info message must either contain both detailedMessageHtml and showDetailedMessageLabel, or neither."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public expand(Z)V
    .locals 1
    .param p1, "expand"    # Z

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    if-eq v0, p1, :cond_1

    .line 166
    if-eqz p1, :cond_0

    .line 167
    const/16 v0, 0x65c

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 170
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 171
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->updateContent()V

    .line 173
    :cond_1
    return-void
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;->showDetailedMessageLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mParentUiNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    return-object v0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    return v0
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 185
    const-string v0, "expandInfoText"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->expand(Z)V

    .line 192
    :goto_0
    return-void

    .line 188
    :cond_0
    const/16 v0, 0x65d

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUrlClickListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    invoke-interface {v0, p0, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;->onClick(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 205
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 206
    check-cast v0, Landroid/os/Bundle;

    .line 207
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "infoMessage"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .line 208
    const-string v1, "expanded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 209
    const-string v1, "parentState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->updateContent()V

    .line 215
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 196
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 197
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/widget/TextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 198
    const-string v1, "infoMessage"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 199
    const-string v1, "expanded"

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 200
    return-object v0
.end method

.method public setInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V
    .locals 1
    .param p1, "infoMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .prologue
    .line 104
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->verifyInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V

    .line 105
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mExpanded:Z

    .line 107
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->updateContent()V

    .line 108
    return-void
.end method

.method public setParentUiNode(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V
    .locals 0
    .param p1, "parentUiNode"    # Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mParentUiNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .line 144
    return-void
.end method

.method public setUrlClickListener(Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mUrlClickListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    .line 157
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mRequestedVisibility:I

    .line 86
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->mInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-nez v0, :cond_0

    .line 87
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
