.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;
.super Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;
.source "CreditCardImagesAnimatorGingerbread.java"


# instance fields
.field private final mFadeInAnimation:Landroid/view/animation/Animation;

.field private final mFadeOutAnimation:Landroid/view/animation/Animation;

.field private final mVisible:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;[Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "images"    # [Landroid/widget/ImageView;

    .prologue
    .line 27
    invoke-direct {p0, p2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;-><init>([Landroid/widget/ImageView;)V

    .line 28
    array-length v0, p2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    .line 29
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 30
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$anim;->wallet_im_fade_in_from_half:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 31
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$anim;->wallet_im_fade_out_to_half:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mFadeOutAnimation:Landroid/view/animation/Animation;

    .line 32
    return-void
.end method


# virtual methods
.method public animateToType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 5
    .param p1, "type"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 36
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-static {p1, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->equals(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 37
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->findIndex(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)I

    move-result v1

    .line 38
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mImages:[Landroid/widget/ImageView;

    array-length v2, v3

    .local v2, "length":I
    :goto_0
    if-ge v0, v2, :cond_4

    .line 39
    if-eq v0, v1, :cond_0

    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    .line 40
    :cond_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    aget-boolean v3, v3, v0

    if-nez v3, :cond_1

    .line 41
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 43
    :cond_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    const/4 v4, 0x1

    aput-boolean v4, v3, v0

    .line 38
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_3

    .line 46
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mFadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 48
    :cond_3
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mVisible:[Z

    const/4 v4, 0x0

    aput-boolean v4, v3, v0

    goto :goto_1

    .line 51
    :cond_4
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 53
    .end local v0    # "i":I
    .end local v1    # "index":I
    .end local v2    # "length":I
    :cond_5
    return-void
.end method

.method public restoreCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 0
    .param p1, "type"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;->animateToType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 58
    return-void
.end method
