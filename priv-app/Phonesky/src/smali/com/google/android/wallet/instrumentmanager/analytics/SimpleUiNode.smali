.class public Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;
.super Ljava/lang/Object;
.source "SimpleUiNode.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;


# instance fields
.field private final mParentNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method public constructor <init>(ILcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V
    .locals 1
    .param p1, "elementId"    # I
    .param p2, "parentNode"    # Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {v0, p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 21
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;->mParentNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .line 22
    return-void
.end method


# virtual methods
.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;->mParentNode:Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    return-object v0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method
