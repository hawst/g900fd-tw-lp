.class public abstract Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.super Ljava/lang/Object;
.source "AbstractValidator.java"


# instance fields
.field protected mErrorMessage:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>(Ljava/lang/CharSequence;)V

    .line 24
    return-void
.end method

.method protected constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "customErrorMessage"    # Ljava/lang/CharSequence;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;->mErrorMessage:Ljava/lang/CharSequence;

    .line 15
    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;->mErrorMessage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public abstract isValid(Landroid/widget/TextView;)Z
.end method
