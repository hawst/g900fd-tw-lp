.class public final Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;
.super Ljava/lang/Object;
.source "InstrumentManagerAnalyticsEventDispatcher.java"


# static fields
.field private static sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;


# direct methods
.method public static sendBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V
    .locals 3
    .param p0, "e"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    if-eqz v0, :cond_1

    .line 44
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    invoke-interface {v0, p0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;->onBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const-string v0, "ImAnalyticsDispatcher"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "ImAnalyticsDispatcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No listener found for sending background event of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;->backgroundEventType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static sendClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)V
    .locals 4
    .param p0, "e"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    if-eqz v0, :cond_1

    .line 59
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    invoke-interface {v0, p0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;->onClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    const-string v0, "ImAnalyticsDispatcher"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const-string v1, "ImAnalyticsDispatcher"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No listener found for sending click event from the clicked element "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    iget v0, v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static sendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)V
    .locals 3
    .param p0, "e"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    if-eqz v0, :cond_1

    .line 74
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    invoke-interface {v0, p0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;->onImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const-string v0, "ImAnalyticsDispatcher"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "ImAnalyticsDispatcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No listener found for sending the following impression event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setEventListener(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;)V
    .locals 0
    .param p0, "listener"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    .prologue
    .line 33
    sput-object p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sListener:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;

    .line 34
    return-void
.end method
