.class public abstract Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;
.super Ljava/lang/Object;
.source "BackgroundEventRequestResponseListener.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RequestT:",
        "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest",
        "<*>;ResponseT:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Landroid/util/Pair",
        "<TRequestT;TResponseT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;, "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener<TRequestT;TResponseT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract handleResponse(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/protobuf/nano/MessageNano;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequestT;TResponseT;)V"
        }
    .end annotation
.end method

.method public final onResponse(Landroid/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<TRequestT;TResponseT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;, "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener<TRequestT;TResponseT;>;"
    .local p1, "volleyResponse":Landroid/util/Pair;, "Landroid/util/Pair<TRequestT;TResponseT;>;"
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;->handleResponse(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/protobuf/nano/MessageNano;)V

    .line 26
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;, "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener<TRequestT;TResponseT;>;"
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;->onResponse(Landroid/util/Pair;)V

    return-void
.end method
