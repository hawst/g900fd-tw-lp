.class public Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;
.super Landroid/widget/FrameLayout;
.source "DynamicAddressFieldsLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;
    }
.end annotation


# instance fields
.field mFieldContainer:Landroid/widget/LinearLayout;

.field mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

.field mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method


# virtual methods
.method protected addViews(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "fields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 59
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 62
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v4, v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    .line 45
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->dynamic_address_fields_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    .line 46
    return-void
.end method

.method public replaceView(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1, "oldView"    # Landroid/view/View;
    .param p2, "newView"    # Landroid/view/View;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 88
    .local v0, "oldViewIndex":I
    if-gez v0, :cond_0

    .line 89
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "oldView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not present in the fields container"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 93
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "newView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already present in the fields container"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 97
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 98
    return-void
.end method

.method public setFields(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "fields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->addViews(Ljava/util/ArrayList;)V

    .line 54
    return-void
.end method

.method public setOnHeightOffsetChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    .line 120
    return-void
.end method

.method public switchToShowingFields()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 69
    return-void
.end method

.method public switchToShowingProgressBar()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    return-void
.end method
