.class public abstract Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
.super Lcom/android/volley/Request;
.source "BackgroundEventRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/android/volley/Request",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mEndTimeMs:J

.field private final mResponseListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mStartTimeMs:J


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 2
    .param p1, "method"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<TT;>;"
    .local p3, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    invoke-direct {p0, p1, p2, p4}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 29
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mResponseListener:Lcom/android/volley/Response$Listener;

    .line 30
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mStartTimeMs:J

    .line 31
    return-void
.end method


# virtual methods
.method public deliverError(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<TT;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mEndTimeMs:J

    .line 42
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    .line 43
    return-void
.end method

.method public deliverResponse(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<TT;>;"
    .local p1, "response":Ljava/lang/Object;, "TT;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mEndTimeMs:J

    .line 36
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mResponseListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v0, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public abstract getBackgroundEventReceivedType()I
.end method

.method public getClientLatencyMs()J
    .locals 4

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;, "Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest<TT;>;"
    iget-wide v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mEndTimeMs:J

    iget-wide v2, p0, Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;->mStartTimeMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
