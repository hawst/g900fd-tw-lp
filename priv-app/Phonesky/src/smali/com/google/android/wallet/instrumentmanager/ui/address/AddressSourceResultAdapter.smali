.class public Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;
.super Landroid/widget/BaseAdapter;
.source "AddressSourceResultAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;
    }
.end annotation


# instance fields
.field private final mAddressField:C

.field private final mAddressSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;",
            ">;"
        }
    .end annotation
.end field

.field private mFilter:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLanguageCode:Ljava/lang/String;

.field mLastPublishedConstraint:Ljava/lang/CharSequence;

.field mObjects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation
.end field

.field private final mRegionCode:I

.field final mRemainingFields:[C

.field private final mRequiredFields:[C

.field private final mTextViewResourceId:I


# direct methods
.method public constructor <init>(Landroid/view/ContextThemeWrapper;IILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/view/ContextThemeWrapper;
    .param p2, "textViewResourceId"    # I
    .param p3, "regionCode"    # I
    .param p4, "languageCode"    # Ljava/lang/String;
    .param p5, "addressField"    # C
    .param p6, "remainingFields"    # [C
    .param p7, "requiredFields"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ContextThemeWrapper;",
            "II",
            "Ljava/lang/String;",
            "C[C",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p8, "addressSources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 86
    if-eqz p6, :cond_0

    array-length v0, p6

    if-nez v0, :cond_1

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "remainingFields are required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    if-eqz p8, :cond_2

    invoke-interface {p8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addressSources are required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_3
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mTextViewResourceId:I

    .line 93
    iput p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRegionCode:I

    .line 94
    iput-object p4, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mLanguageCode:Ljava/lang/String;

    .line 95
    iput-char p5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressField:C

    .line 96
    invoke-static {p6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->checkValidAddressFieldsAndCopy([C)[C

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRemainingFields:[C

    .line 97
    if-eqz p7, :cond_4

    invoke-virtual {p7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressSources:Ljava/util/List;

    .line 100
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    .line 102
    return-void

    .line 97
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressSources:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)C
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    .prologue
    .line 35
    iget-char v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mAddressField:C

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRegionCode:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mLanguageCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)[C
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mRequiredFields:[C

    return-object v0
.end method

.method private static checkValidAddressFieldsAndCopy([C)[C
    .locals 3
    .param p0, "fields"    # [C

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-char v1, p0, v0

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 63
    array-length v1, p0

    invoke-static {p0, v0, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v1

    return-object v1

    .line 65
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "fields must contain at least one valid field"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getCustomView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 150
    if-nez p2, :cond_0

    .line 151
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mTextViewResourceId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 154
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->getItem(I)Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    move-result-object v1

    .line 155
    .local v1, "item":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->description:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 156
    .local v0, "description":Landroid/widget/TextView;
    iget-object v2, v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;->description:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    return-object p2
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->getCustomView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mFilter:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mFilter:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mFilter:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter$AddressSourceResultFilter;

    return-object v0
.end method

.method public getItem(I)Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->mObjects:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->getItem(I)Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 128
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressSourceResultAdapter;->getCustomView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
