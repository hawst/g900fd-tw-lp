.class public Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;
.super Ljava/lang/Object;
.source "GooglePlacesAddressSource.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->mContext:Landroid/content/Context;

    .line 158
    return-void
.end method

.method private buildPlaceDetailsUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "reference"    # Ljava/lang/String;
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 288
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v1, "parameters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "reference"

    invoke-direct {v3, v4, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 291
    .local v0, "location":Landroid/location/Location;
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "sensor"

    if-eqz v0, :cond_1

    const-string v3, "true"

    :goto_0
    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "key"

    const-string v5, "AIzaSyCgACP5TTubzmLhxFL5ONXq6B5l2eH_EXc"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 295
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "language"

    invoke-direct {v3, v4, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_0
    const-string v3, "utf-8"

    invoke-static {v1, v3}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 298
    .local v2, "queryString":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://maps.googleapis.com/maps/api/place/details/json?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 291
    .end local v2    # "queryString":Ljava/lang/String;
    :cond_1
    const-string v3, "false"

    goto :goto_0
.end method

.method private buildPlacesAutocompleteUrl(Ljava/lang/CharSequence;CILjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "query"    # Ljava/lang/CharSequence;
    .param p2, "addressField"    # C
    .param p3, "regionCode"    # I
    .param p4, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 265
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v2, "parameters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "input"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "key"

    const-string v6, "AIzaSyCgACP5TTubzmLhxFL5ONXq6B5l2eH_EXc"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "types"

    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getRequestTypeForField(C)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    .line 270
    .local v1, "location":Landroid/location/Location;
    if-eqz v1, :cond_0

    .line 271
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "latlon":Ljava/lang/String;
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "location"

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "radius"

    const v6, 0x13880

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    .end local v0    # "latlon":Ljava/lang/String;
    :cond_0
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "sensor"

    if-eqz v1, :cond_2

    const-string v4, "true"

    :goto_0
    invoke-direct {v5, v6, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "components"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "country:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p3}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 281
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "language"

    invoke-direct {v4, v5, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_1
    const-string v4, "utf-8"

    invoke-static {v2, v4}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 284
    .local v3, "queryString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://maps.googleapis.com/maps/api/place/autocomplete/json?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 276
    .end local v3    # "queryString":Ljava/lang/String;
    :cond_2
    const-string v4, "false"

    goto :goto_0
.end method

.method private static convertJsonObjectToAddressSourceResults(Lorg/json/JSONObject;Ljava/lang/CharSequence;C)Ljava/util/ArrayList;
    .locals 25
    .param p0, "response"    # Lorg/json/JSONObject;
    .param p1, "prefix"    # Ljava/lang/CharSequence;
    .param p2, "addressField"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/CharSequence;",
            "C)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    if-nez p0, :cond_1

    .line 310
    const/16 v18, 0x0

    .line 371
    :cond_0
    :goto_0
    return-object v18

    .line 313
    :cond_1
    const-string v21, "status"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 314
    .local v20, "status":Ljava/lang/String;
    const-string v21, "OK"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_2

    .line 315
    const-string v21, "GooglePlacesAddressSour"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Response has invalid status: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/16 v18, 0x0

    goto :goto_0

    .line 321
    :cond_2
    :try_start_0
    const-string v21, "predictions"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 327
    .local v15, "predictions":Lorg/json/JSONArray;
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v18, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v13

    .local v13, "numPredictions":I
    :goto_1
    if-ge v8, v13, :cond_0

    .line 330
    :try_start_1
    invoke-virtual {v15, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    .line 332
    .local v14, "prediction":Lorg/json/JSONObject;
    const-string v21, "description"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 334
    .local v5, "description":Ljava/lang/CharSequence;
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v21

    if-nez v21, :cond_4

    .line 328
    .end local v5    # "description":Ljava/lang/CharSequence;
    .end local v14    # "prediction":Lorg/json/JSONObject;
    :cond_3
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 322
    .end local v8    # "i":I
    .end local v13    # "numPredictions":I
    .end local v15    # "predictions":Lorg/json/JSONArray;
    .end local v18    # "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    :catch_0
    move-exception v7

    .line 323
    .local v7, "e":Lorg/json/JSONException;
    const-string v21, "GooglePlacesAddressSour"

    const-string v22, "Response does not contain predictions"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const/16 v18, 0x0

    goto :goto_0

    .line 337
    .end local v7    # "e":Lorg/json/JSONException;
    .restart local v5    # "description":Ljava/lang/CharSequence;
    .restart local v8    # "i":I
    .restart local v13    # "numPredictions":I
    .restart local v14    # "prediction":Lorg/json/JSONObject;
    .restart local v15    # "predictions":Lorg/json/JSONArray;
    .restart local v18    # "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;>;"
    :cond_4
    :try_start_2
    const-string v21, "reference"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 338
    .local v16, "reference":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_3

    .line 341
    move/from16 v0, p2

    invoke-static {v14, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->hasTypeForField(Lorg/json/JSONObject;C)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 344
    invoke-static {v14}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getMatchingTerm(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v12

    .line 345
    .local v12, "matchingTerm":Ljava/lang/String;
    if-eqz v12, :cond_3

    .line 347
    invoke-static/range {p2 .. p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->shouldValidateMatchingTerm(C)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 348
    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 352
    :cond_5
    invoke-static {v14}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getMatchedSubstrings(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v11

    .line 355
    .local v11, "matchedSubstrings":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_7

    .line 356
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 357
    .end local v5    # "description":Ljava/lang/CharSequence;
    .local v6, "description":Ljava/lang/CharSequence;
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    .line 358
    .local v10, "matchedSubstring":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v19, Landroid/text/style/StyleSpan;

    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 359
    .local v19, "span":Landroid/text/style/StyleSpan;
    move-object v0, v6

    check-cast v0, Landroid/text/SpannableString;

    move-object/from16 v21, v0

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v23

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v24

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    add-int v22, v22, v24

    const/16 v24, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move/from16 v2, v23

    move/from16 v3, v22

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    .line 367
    .end local v6    # "description":Ljava/lang/CharSequence;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "matchedSubstring":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v11    # "matchedSubstrings":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    .end local v12    # "matchingTerm":Ljava/lang/String;
    .end local v14    # "prediction":Lorg/json/JSONObject;
    .end local v16    # "reference":Ljava/lang/String;
    .end local v19    # "span":Landroid/text/style/StyleSpan;
    :catch_1
    move-exception v21

    goto/16 :goto_2

    .restart local v6    # "description":Ljava/lang/CharSequence;
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v11    # "matchedSubstrings":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    .restart local v12    # "matchingTerm":Ljava/lang/String;
    .restart local v14    # "prediction":Lorg/json/JSONObject;
    .restart local v16    # "reference":Ljava/lang/String;
    :cond_6
    move-object v5, v6

    .line 364
    .end local v6    # "description":Ljava/lang/CharSequence;
    .end local v9    # "i$":Ljava/util/Iterator;
    .restart local v5    # "description":Ljava/lang/CharSequence;
    :cond_7
    new-instance v17, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;

    const-string v21, "GooglePlacesAddressSource"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v16

    invoke-direct {v0, v12, v5, v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    .local v17, "result":Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2
.end method

.method private static convertJsonObjectToPostalAddress(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 23
    .param p0, "response"    # Lorg/json/JSONObject;
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 458
    if-nez p0, :cond_1

    .line 459
    const/4 v2, 0x0

    .line 536
    :cond_0
    :goto_0
    return-object v2

    .line 461
    :cond_1
    const-string v19, "status"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 462
    .local v14, "status":Ljava/lang/String;
    const-string v19, "OK"

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 463
    const-string v19, "GooglePlacesAddressSour"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Response has invalid status: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    const/4 v2, 0x0

    goto :goto_0

    .line 469
    :cond_2
    :try_start_0
    const-string v19, "result"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 476
    .local v12, "result":Lorg/json/JSONObject;
    :try_start_1
    const-string v19, "address_components"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 481
    .local v3, "addressComponents":Lorg/json/JSONArray;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 482
    .local v8, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    .local v7, "length":I
    :goto_1
    if-ge v6, v7, :cond_7

    .line 484
    :try_start_2
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 486
    .local v4, "component":Lorg/json/JSONObject;
    const-string v19, "postal_code_prefix"

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->hasType(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 482
    .end local v4    # "component":Lorg/json/JSONObject;
    :cond_3
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 470
    .end local v3    # "addressComponents":Lorg/json/JSONArray;
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v8    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "result":Lorg/json/JSONObject;
    :catch_0
    move-exception v5

    .line 471
    .local v5, "e":Lorg/json/JSONException;
    const/4 v2, 0x0

    goto :goto_0

    .line 477
    .end local v5    # "e":Lorg/json/JSONException;
    .restart local v12    # "result":Lorg/json/JSONObject;
    :catch_1
    move-exception v5

    .line 478
    .restart local v5    # "e":Lorg/json/JSONException;
    const/4 v2, 0x0

    goto :goto_0

    .line 491
    .end local v5    # "e":Lorg/json/JSONException;
    .restart local v3    # "addressComponents":Lorg/json/JSONArray;
    .restart local v4    # "component":Lorg/json/JSONObject;
    .restart local v6    # "i":I
    .restart local v7    # "length":I
    .restart local v8    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    const-string v19, "administrative_area_level_1"

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->hasType(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_5

    const-string v19, "country"

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->hasType(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 493
    :cond_5
    const-string v10, "short_name"

    .line 497
    .local v10, "nameKey":Ljava/lang/String;
    :goto_3
    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 498
    .local v9, "name":Ljava/lang/String;
    const-string v19, "types"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v18

    .line 499
    .local v18, "types":Lorg/json/JSONArray;
    const/16 v16, 0x0

    .local v16, "t":I
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v11

    .local v11, "numTypes":I
    :goto_4
    move/from16 v0, v16

    if-ge v0, v11, :cond_3

    .line 500
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 501
    .local v17, "type":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v8, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 495
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "nameKey":Ljava/lang/String;
    .end local v11    # "numTypes":I
    .end local v16    # "t":I
    .end local v17    # "type":Ljava/lang/String;
    .end local v18    # "types":Lorg/json/JSONArray;
    :cond_6
    const-string v10, "long_name"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .restart local v10    # "nameKey":Ljava/lang/String;
    goto :goto_3

    .line 507
    .end local v4    # "component":Lorg/json/JSONObject;
    .end local v10    # "nameKey":Ljava/lang/String;
    :cond_7
    new-instance v2, Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {v2}, Lcom/google/location/country/Postaladdress$PostalAddress;-><init>()V

    .line 508
    .local v2, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    const-string v19, "street_number"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_8

    const-string v19, "route"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 509
    :cond_8
    const-string v19, "street_number"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 510
    .local v15, "streetNumber":Ljava/lang/String;
    const-string v19, "route"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 511
    .local v13, "route":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 512
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v13, v19, v20

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    .line 519
    .end local v13    # "route":Ljava/lang/String;
    .end local v15    # "streetNumber":Ljava/lang/String;
    :cond_9
    :goto_5
    const-string v19, "locality"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 520
    const-string v19, "locality"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    .line 522
    :cond_a
    const-string v19, "administrative_area_level_1"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 523
    const-string v19, "administrative_area_level_1"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    .line 527
    :cond_b
    const-string v19, "postal_code"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 528
    const-string v19, "postal_code"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 530
    :cond_c
    const-string v19, "country"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 531
    const-string v19, "country"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    .line 533
    :cond_d
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 534
    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 513
    .restart local v13    # "route":Ljava/lang/String;
    .restart local v15    # "streetNumber":Ljava/lang/String;
    :cond_e
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 514
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v15, v19, v20

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    goto/16 :goto_5

    .line 516
    :cond_f
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    goto/16 :goto_5

    .line 503
    .end local v2    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v13    # "route":Ljava/lang/String;
    .end local v15    # "streetNumber":Ljava/lang/String;
    :catch_2
    move-exception v19

    goto/16 :goto_2
.end method

.method private fetchJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 8
    .param p1, "urlSpec"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 210
    :try_start_0
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v1

    .line 211
    .local v1, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lorg/json/JSONObject;>;"
    new-instance v2, Lcom/android/volley/toolbox/JsonObjectRequest;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3, v1, v1}, Lcom/android/volley/toolbox/JsonObjectRequest;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 213
    .local v2, "request":Lcom/android/volley/toolbox/JsonObjectRequest;
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->getApiRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 214
    const-wide/16 v6, 0x1388

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v6, v7, v3}, Lcom/android/volley/toolbox/RequestFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 223
    .end local v1    # "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lorg/json/JSONObject;>;"
    .end local v2    # "request":Lcom/android/volley/toolbox/JsonObjectRequest;
    :goto_0
    return-object v3

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    const-string v3, "GooglePlacesAddressSour"

    const-string v5, "TimeoutException while retrieving addresses from GooglePlaces"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v4

    .line 217
    goto :goto_0

    .line 218
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "GooglePlacesAddressSour"

    const-string v5, "InterruptedException while retrieving addresses from GooglePlaces"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v4

    .line 220
    goto :goto_0

    .line 221
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v0

    .line 222
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "GooglePlacesAddressSour"

    const-string v5, "ExecutionException while retrieving addresses from GooglePlaces"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v4

    .line 223
    goto :goto_0
.end method

.method private getLastKnownLocation()Landroid/location/Location;
    .locals 3

    .prologue
    .line 302
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 304
    .local v0, "manager":Landroid/location/LocationManager;
    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    return-object v1
.end method

.method private static getMatchedSubstrings(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "prediction"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 375
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 377
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    :try_start_0
    const-string v7, "matched_substrings"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 378
    .local v3, "matchedSubstrings":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .local v4, "numSubstrings":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 379
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 380
    .local v2, "matchedSubstring":Lorg/json/JSONObject;
    const-string v7, "offset"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 381
    .local v5, "offset":I
    const-string v7, "length"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 382
    .local v1, "length":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 384
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "matchedSubstring":Lorg/json/JSONObject;
    .end local v3    # "matchedSubstrings":Lorg/json/JSONArray;
    .end local v4    # "numSubstrings":I
    .end local v5    # "offset":I
    :catch_0
    move-exception v7

    .line 387
    :cond_0
    return-object v6
.end method

.method private static getMatchingTerm(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 8
    .param p0, "prediction"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 392
    const-string v6, "matched_substrings"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "offset"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 394
    .local v3, "offset":I
    const-string v6, "terms"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 395
    .local v5, "terms":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v2

    .local v2, "numTerms":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 396
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 397
    .local v4, "term":Lorg/json/JSONObject;
    const-string v6, "offset"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v7, "value"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int v0, v6, v7

    .line 398
    .local v0, "endOfTerm":I
    if-ge v3, v0, :cond_0

    .line 399
    const-string v6, "value"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 402
    .end local v0    # "endOfTerm":I
    .end local v4    # "term":Lorg/json/JSONObject;
    :goto_1
    return-object v6

    .line 395
    .restart local v0    # "endOfTerm":I
    .restart local v4    # "term":Lorg/json/JSONObject;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 402
    .end local v0    # "endOfTerm":I
    .end local v4    # "term":Lorg/json/JSONObject;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method static getRequestTypeForField(C)Ljava/lang/String;
    .locals 1
    .param p0, "field"    # C

    .prologue
    .line 239
    sparse-switch p0, :sswitch_data_0

    .line 259
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 241
    :sswitch_0
    const-string v0, "geocode"

    goto :goto_0

    .line 243
    :sswitch_1
    const-string v0, "(cities)"

    goto :goto_0

    .line 245
    :sswitch_2
    const-string v0, "(regions)"

    goto :goto_0

    .line 239
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x43 -> :sswitch_1
        0x53 -> :sswitch_2
    .end sparse-switch
.end method

.method static getThresholdForField(C)I
    .locals 1
    .param p0, "field"    # C

    .prologue
    .line 229
    packed-switch p0, :pswitch_data_0

    .line 233
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->thresholdDefault:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    .line 231
    :pswitch_0
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->thresholdAddressLine1:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch
.end method

.method private static hasType(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 6
    .param p0, "object"    # Lorg/json/JSONObject;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 429
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 442
    :cond_0
    :goto_0
    return v4

    .line 434
    :cond_1
    :try_start_0
    const-string v5, "types"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 435
    .local v3, "types":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "t":I
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    .local v1, "numTypes":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 436
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    .line 437
    const/4 v4, 0x1

    goto :goto_0

    .line 435
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 441
    .end local v1    # "numTypes":I
    .end local v2    # "t":I
    .end local v3    # "types":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Lorg/json/JSONException;
    goto :goto_0
.end method

.method private static hasTypeForField(Lorg/json/JSONObject;C)Z
    .locals 2
    .param p0, "object"    # Lorg/json/JSONObject;
    .param p1, "field"    # C

    .prologue
    .line 407
    sparse-switch p1, :sswitch_data_0

    .line 422
    const/4 v0, 0x0

    .line 425
    .local v0, "responseTypeForField":Ljava/lang/String;
    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->hasType(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 409
    .end local v0    # "responseTypeForField":Ljava/lang/String;
    :sswitch_0
    const-string v0, "route"

    .line 410
    .restart local v0    # "responseTypeForField":Ljava/lang/String;
    goto :goto_0

    .line 412
    .end local v0    # "responseTypeForField":Ljava/lang/String;
    :sswitch_1
    const-string v0, "locality"

    .line 413
    .restart local v0    # "responseTypeForField":Ljava/lang/String;
    goto :goto_0

    .line 415
    .end local v0    # "responseTypeForField":Ljava/lang/String;
    :sswitch_2
    const-string v0, "administrative_area_level_1"

    .line 416
    .restart local v0    # "responseTypeForField":Ljava/lang/String;
    goto :goto_0

    .line 419
    .end local v0    # "responseTypeForField":Ljava/lang/String;
    :sswitch_3
    const-string v0, "locality"

    .line 420
    .restart local v0    # "responseTypeForField":Ljava/lang/String;
    goto :goto_0

    .line 407
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x43 -> :sswitch_1
        0x53 -> :sswitch_2
        0x5a -> :sswitch_3
    .end sparse-switch
.end method

.method public static isCountrySupported(I)Z
    .locals 2
    .param p0, "regionCode"    # I

    .prologue
    .line 161
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "countryCode":Ljava/lang/String;
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;->supportedCountries:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method private static isFieldSupported(IC)Z
    .locals 2
    .param p0, "regionCode"    # I
    .param p1, "addressField"    # C

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->isCountrySupported(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getRequestTypeForField(C)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static shouldValidateMatchingTerm(C)Z
    .locals 1
    .param p0, "field"    # C

    .prologue
    .line 447
    sparse-switch p0, :sswitch_data_0

    .line 452
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 450
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 447
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x43 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getAddress(Ljava/lang/String;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 3
    .param p1, "reference"    # Ljava/lang/String;
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    const/4 v2, 0x0

    .line 200
    :goto_0
    return-object v2

    .line 198
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->buildPlaceDetailsUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "urlSpec":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->fetchJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 200
    .local v0, "response":Lorg/json/JSONObject;
    invoke-static {v0, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->convertJsonObjectToPostalAddress(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v2

    goto :goto_0
.end method

.method public getAddresses(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "prefix"    # Ljava/lang/CharSequence;
    .param p2, "addressField"    # C
    .param p3, "remainingFields"    # [C
    .param p4, "regionCode"    # I
    .param p5, "languageCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "C[CI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 181
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->getThresholdForField(C)I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-object v2

    .line 184
    :cond_1
    invoke-static {p4, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->isFieldSupported(IC)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->buildPlacesAutocompleteUrl(Ljava/lang/CharSequence;CILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "urlSpec":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->fetchJsonObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 189
    .local v0, "response":Lorg/json/JSONObject;
    invoke-static {v0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/GooglePlacesAddressSource;->convertJsonObjectToAddressSourceResults(Lorg/json/JSONObject;Ljava/lang/CharSequence;C)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const-string v0, "GooglePlacesAddressSource"

    return-object v0
.end method
