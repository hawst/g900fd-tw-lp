.class public interface abstract Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;
.super Ljava/lang/Object;
.source "FormSpinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Invalidator"
.end annotation


# virtual methods
.method public abstract clearError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;)V
.end method

.method public abstract getError(Landroid/view/View;)Ljava/lang/CharSequence;
.end method

.method public abstract setError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;Ljava/lang/CharSequence;)V
.end method
