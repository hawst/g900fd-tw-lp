.class public Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;
.super Lcom/android/volley/toolbox/JsonObjectRequest;
.source "CountryMetadataRetrievalTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CountryMetadataRetrievalRequest"
.end annotation


# static fields
.field private static final COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    .line 139
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "upper"

    const-string v2, "C"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "zip_name_type"

    const-string v2, "postal"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "fmt"

    const-string v2, "%N%n%O%n%A%n%C"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "require"

    const-string v2, "AC"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "state_name_type"

    const-string v2, "province"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "data/ZZ"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    const-string v1, "dir"

    const-string v2, "ltr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 6
    .param p1, "regionCode"    # I
    .param p2, "languageCode"    # Ljava/lang/String;
    .param p4, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lorg/json/JSONObject;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152
    .local p3, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lorg/json/JSONObject;>;"
    const/4 v1, 0x0

    # invokes: Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->buildCountryUri(ILjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->access$000(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/volley/toolbox/JsonObjectRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 154
    # invokes: Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->buildCountryId(ILjava/lang/String;)Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask;->access$100(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->mId:Ljava/lang/String;

    .line 155
    return-void
.end method


# virtual methods
.method public getPriority()Lcom/android/volley/Request$Priority;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/android/volley/Request$Priority;->HIGH:Lcom/android/volley/Request$Priority;

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 11
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/JsonObjectRequest;->parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;

    move-result-object v7

    .line 165
    .local v7, "parsedResponse":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lorg/json/JSONObject;>;"
    invoke-virtual {v7}, Lcom/android/volley/Response;->isSuccess()Z

    move-result v8

    if-nez v8, :cond_0

    .line 219
    .end local v7    # "parsedResponse":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lorg/json/JSONObject;>;"
    :goto_0
    return-object v7

    .line 176
    .restart local v7    # "parsedResponse":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lorg/json/JSONObject;>;"
    :cond_0
    iget-object v6, v7, Lcom/android/volley/Response;->result:Ljava/lang/Object;

    check-cast v6, Lorg/json/JSONObject;

    .line 179
    .local v6, "parsedJson":Lorg/json/JSONObject;
    const-string v8, "id"

    invoke-static {v6, v8}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 181
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->mId:Ljava/lang/String;

    .line 183
    :try_start_0
    const-string v8, "id"

    invoke-virtual {v6, v8, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 190
    :cond_1
    const-string v8, "key"

    invoke-static {v6, v8}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 191
    .local v4, "key":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 192
    const-string v8, "/"

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 194
    .local v5, "keyFromId":Ljava/lang/String;
    :try_start_1
    const-string v8, "key"

    invoke-virtual {v6, v8, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 202
    .end local v5    # "keyFromId":Ljava/lang/String;
    :cond_2
    sget-object v8, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 203
    .local v3, "jsonKey":Ljava/lang/String;
    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 205
    :try_start_2
    sget-object v8, Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$CountryMetadataRetrievalRequest;->COUNTRY_DATA_DEFAULT:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v3, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Lorg/json/JSONException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error adding country default data for key="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to response"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 184
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "jsonKey":Ljava/lang/String;
    .end local v4    # "key":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 185
    .restart local v0    # "e":Lorg/json/JSONException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error adding id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to response"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 195
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v4    # "key":Ljava/lang/String;
    .restart local v5    # "keyFromId":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 196
    .restart local v0    # "e":Lorg/json/JSONException;
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error adding key="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to response"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 219
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v5    # "keyFromId":Ljava/lang/String;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v8, v7, Lcom/android/volley/Response;->cacheEntry:Lcom/android/volley/Cache$Entry;

    invoke-static {v6, v8}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v7

    goto/16 :goto_0
.end method
