.class public final Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;
.super Ljava/lang/Object;
.source "InstrumentManagerImpressionEvent.java"


# instance fields
.field public final impressionTree:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)V
    .locals 0
    .param p1, "impressionTree"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->impressionTree:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 23
    return-void
.end method

.method private static printTree(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/lang/StringBuilder;I)V
    .locals 5
    .param p0, "element"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .param p1, "treeString"    # Ljava/lang/StringBuilder;
    .param p2, "indent"    # I

    .prologue
    .line 36
    const-string v1, ""

    .line 37
    .local v1, "inc":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 38
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "| "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    const-string v3, "|-"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    iget v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    const-string v3, " tokenLen="

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    const/16 v3, 0xa

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 46
    add-int/lit8 p2, p2, 0x1

    .line 47
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "length":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 48
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-static {v3, p1, p2}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->printTree(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/lang/StringBuilder;I)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 51
    .end local v2    # "length":I
    :cond_1
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 27
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 28
    .local v1, "treeString":Ljava/lang/StringBuilder;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 29
    const/4 v0, 0x0

    .line 30
    .local v0, "indent":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->impressionTree:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-static {v2, v1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;->printTree(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/lang/StringBuilder;I)V

    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
