.class public Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;
.super Landroid/widget/FrameLayout;
.source "RegionCodeView.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;


# instance fields
.field private mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

.field private mReadOnlyMode:Z

.field private mRegionCodesSet:Z

.field private mSelectedRegionCode:I

.field public mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

.field public mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 48
    return-void
.end method


# virtual methods
.method public getSelectedRegionCode()I
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mReadOnlyMode:Z

    if-eqz v0, :cond_0

    .line 102
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSelectedRegionCode:I

    .line 107
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getSelectedRegionCode()I

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 53
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->region_code_spinner:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    .line 54
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->region_code_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mTextView:Landroid/widget/TextView;

    .line 55
    return-void
.end method

.method public onRegionCodeSelected(II)V
    .locals 2
    .param p1, "regionCode"    # I
    .param p2, "senderId"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSelectedRegionCode:I

    .line 114
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->getId()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;->onRegionCodeSelected(II)V

    .line 117
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 60
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setEnabled(Z)V

    .line 61
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 62
    return-void
.end method

.method public setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .line 98
    return-void
.end method

.method public setRegionCodes([I)V
    .locals 5
    .param p1, "regionCodes"    # [I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    array-length v0, p1

    if-ne v0, v3, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mTextView:Landroid/widget/TextView;

    aget v1, p1, v2

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDisplayCountryForDefaultLocale(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    aget v0, p1, v2

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->getId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->onRegionCodeSelected(II)V

    .line 70
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    iput-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mReadOnlyMode:Z

    .line 79
    :goto_0
    iput-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mRegionCodesSet:Z

    .line 80
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setRegionCodes([I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mReadOnlyMode:Z

    goto :goto_0
.end method

.method public setSelectedRegionCode(I)V
    .locals 2
    .param p1, "regionCode"    # I

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mRegionCodesSet:Z

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRegionCodes() must be called before setSelectedRegionCode()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mReadOnlyMode:Z

    if-nez v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->mSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setSelectedRegionCode(I)V

    .line 94
    :cond_1
    return-void
.end method
