.class public Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;
.super Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;
.source "DeviceAddressSource.java"


# static fields
.field private static final STREET_SEPARATORS:Ljava/util/regex/Pattern;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mHasReadContactsPermission:Z

.field private mQueriedReadContactsPermission:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "[\\r\\n]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->STREET_SEPARATORS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 43
    const-string v0, "DeviceAddressSource"

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/InMemoryAddressSource;-><init>(Ljava/lang/String;)V

    .line 38
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mHasReadContactsPermission:Z

    .line 39
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mQueriedReadContactsPermission:Z

    .line 44
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private static checkRemainingSizeAllowance(II)I
    .locals 2
    .param p0, "remainingSizeAllowanceBytes"    # I
    .param p1, "sizeBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 196
    sub-int/2addr p0, p1

    .line 197
    if-gez p0, :cond_0

    .line 198
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Device data exceeds allowed storage for source"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_0
    return p0
.end method

.method private static getAddressesCursor(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 212
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    .line 213
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "in_visible_group=1"

    .line 214
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 215
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "sortOrder":Ljava/lang/String;
    move-object v0, p0

    move-object v2, p1

    .line 216
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static getContactsCursor(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 204
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 205
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "in_visible_group=1 AND mimetype=?"

    .line 206
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, "vnd.android.cursor.item/name"

    aput-object v2, v4, v0

    .line 207
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v5, "contact_id"

    .local v5, "sortOrder":Ljava/lang/String;
    move-object v0, p0

    move-object v2, p1

    .line 208
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getSizeAllowanceBytes()I
    .locals 5

    .prologue
    .line 48
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 50
    .local v1, "mgr":Landroid/app/ActivityManager;
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 51
    .local v0, "memClassMegaBytes":I
    div-int/lit8 v3, v0, 0x10

    mul-int/lit16 v3, v3, 0x400

    mul-int/lit16 v2, v3, 0x400

    .line 52
    .local v2, "sizeAllowanceBytes":I
    if-nez v2, :cond_0

    .line 54
    const/high16 v2, 0x100000

    .line 56
    :cond_0
    return v2
.end method

.method private declared-synchronized hasReadContactsPermission()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 220
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mQueriedReadContactsPermission:Z

    if-nez v1, :cond_0

    .line 221
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mQueriedReadContactsPermission:Z

    .line 222
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mHasReadContactsPermission:Z

    .line 226
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mHasReadContactsPermission:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static sizeOfPostalAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)I
    .locals 2
    .param p0, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;

    .prologue
    .line 187
    if-nez p0, :cond_0

    .line 188
    const/4 v1, 0x0

    .line 191
    :goto_0
    return v1

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/google/location/country/Postaladdress$PostalAddress;->getCachedSize()I

    move-result v0

    .line 191
    .local v0, "numChars":I
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x2d

    div-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x8

    goto :goto_0
.end method

.method private static trim(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 183
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getAddresses()Ljava/util/ArrayList;
    .locals 35
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->hasReadContactsPermission()Z

    move-result v32

    if-nez v32, :cond_1

    .line 63
    const/4 v3, 0x0

    .line 179
    :cond_0
    return-object v3

    .line 66
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->getSizeAllowanceBytes()I

    move-result v26

    .line 68
    .local v26, "remainingSizeAllowanceBytes":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 73
    .local v27, "resolver":Landroid/content/ContentResolver;
    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const-string v34, "contact_id"

    aput-object v34, v32, v33

    const/16 v33, 0x1

    const-string v34, "data1"

    aput-object v34, v32, v33

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->getContactsCursor(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 76
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v8, Landroid/util/SparseArray;

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v32

    move/from16 v0, v32

    invoke-direct {v8, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 77
    .local v8, "contacts":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v32

    if-lez v32, :cond_3

    .line 78
    const-string v32, "contact_id"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 79
    .local v7, "contactIdCol":I
    const-string v32, "data1"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 81
    .local v14, "displayNamePrimaryCol":I
    :cond_2
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v32

    if-eqz v32, :cond_3

    .line 82
    invoke-interface {v12, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 83
    .local v6, "contactId":I
    invoke-interface {v12, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 84
    .local v13, "displayName":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_2

    .line 87
    invoke-virtual {v8, v6, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    .end local v6    # "contactId":I
    .end local v7    # "contactIdCol":I
    .end local v8    # "contacts":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .end local v13    # "displayName":Ljava/lang/String;
    .end local v14    # "displayNamePrimaryCol":I
    :catchall_0
    move-exception v32

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 92
    const/4 v12, 0x0

    throw v32

    .line 91
    .restart local v8    # "contacts":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 92
    const/4 v12, 0x0

    .line 98
    const/16 v32, 0x8

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const-string v34, "contact_id"

    aput-object v34, v32, v33

    const/16 v33, 0x1

    const-string v34, "data4"

    aput-object v34, v32, v33

    const/16 v33, 0x2

    const-string v34, "data7"

    aput-object v34, v32, v33

    const/16 v33, 0x3

    const-string v34, "data6"

    aput-object v34, v32, v33

    const/16 v33, 0x4

    const-string v34, "data8"

    aput-object v34, v32, v33

    const/16 v33, 0x5

    const-string v34, "data9"

    aput-object v34, v32, v33

    const/16 v33, 0x6

    const-string v34, "data10"

    aput-object v34, v32, v33

    const/16 v33, 0x7

    const-string v34, "data5"

    aput-object v34, v32, v33

    move-object/from16 v0, v27

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->getAddressesCursor(Landroid/content/ContentResolver;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 103
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v32

    move/from16 v0, v32

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 104
    .local v3, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    new-instance v9, Landroid/util/SparseBooleanArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v32

    move/from16 v0, v32

    invoke-direct {v9, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 105
    .local v9, "contactsWithAddresses":Landroid/util/SparseBooleanArray;
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v32

    if-lez v32, :cond_e

    .line 106
    const-string v32, "contact_id"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 107
    .restart local v7    # "contactIdCol":I
    const-string v32, "data4"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    .line 108
    .local v29, "streetCol":I
    const-string v32, "data7"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 109
    .local v5, "cityCol":I
    const-string v32, "data6"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 110
    .local v19, "neighborhoodCol":I
    const-string v32, "data8"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 111
    .local v25, "regionCol":I
    const-string v32, "data9"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 112
    .local v23, "postcodeCol":I
    const-string v32, "data10"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 113
    .local v11, "countryCol":I
    const-string v32, "data5"

    move-object/from16 v0, v32

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 114
    .local v21, "poboxCol":I
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v32

    if-eqz v32, :cond_e

    .line 115
    new-instance v2, Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {v2}, Lcom/google/location/country/Postaladdress$PostalAddress;-><init>()V

    .line 116
    .local v2, "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-interface {v12, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 117
    .restart local v6    # "contactId":I
    invoke-virtual {v8, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 118
    .local v17, "name":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_4

    .line 119
    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    .line 121
    :cond_4
    move/from16 v0, v29

    invoke-interface {v12, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v32

    if-nez v32, :cond_7

    .line 122
    sget-object v32, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->STREET_SEPARATORS:Ljava/util/regex/Pattern;

    move/from16 v0, v29

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v30

    .line 123
    .local v30, "streets":[Ljava/lang/String;
    new-instance v31, Ljava/util/ArrayList;

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v32, v0

    invoke-direct/range {v31 .. v32}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .local v31, "streetsWithNameDefined":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v16, v0

    .local v16, "length":I
    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    .line 125
    aget-object v32, v30, v15

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 126
    .local v28, "street":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_5

    .line 127
    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 130
    .end local v28    # "street":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v32

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [Ljava/lang/String;

    move-object/from16 v0, v32

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    .line 133
    .end local v15    # "i":I
    .end local v16    # "length":I
    .end local v30    # "streets":[Ljava/lang/String;
    .end local v31    # "streetsWithNameDefined":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_7
    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "city":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_8

    .line 135
    iput-object v4, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    .line 137
    :cond_8
    move/from16 v0, v19

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 138
    .local v18, "neighborhood":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_9

    .line 139
    move-object/from16 v0, v18

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    .line 141
    :cond_9
    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 142
    .local v24, "region":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 143
    move-object/from16 v0, v24

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    .line 145
    :cond_a
    move/from16 v0, v23

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 146
    .local v22, "postcode":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_b

    .line 147
    move-object/from16 v0, v22

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 149
    :cond_b
    invoke-interface {v12, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 150
    .local v10, "country":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_c

    .line 151
    iput-object v10, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    .line 153
    :cond_c
    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 154
    .local v20, "pobox":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_d

    .line 155
    move-object/from16 v0, v20

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    .line 157
    :cond_d
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->sizeOfPostalAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)I

    move-result v32

    move/from16 v0, v26

    move/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->checkRemainingSizeAllowance(II)I

    move-result v26

    .line 159
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    const/16 v32, 0x1

    move/from16 v0, v32

    invoke-virtual {v9, v6, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_1

    .line 164
    .end local v2    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v3    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    .end local v4    # "city":Ljava/lang/String;
    .end local v5    # "cityCol":I
    .end local v6    # "contactId":I
    .end local v7    # "contactIdCol":I
    .end local v9    # "contactsWithAddresses":Landroid/util/SparseBooleanArray;
    .end local v10    # "country":Ljava/lang/String;
    .end local v11    # "countryCol":I
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "neighborhood":Ljava/lang/String;
    .end local v19    # "neighborhoodCol":I
    .end local v20    # "pobox":Ljava/lang/String;
    .end local v21    # "poboxCol":I
    .end local v22    # "postcode":Ljava/lang/String;
    .end local v23    # "postcodeCol":I
    .end local v24    # "region":Ljava/lang/String;
    .end local v25    # "regionCol":I
    .end local v29    # "streetCol":I
    :catchall_1
    move-exception v32

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 165
    const/4 v12, 0x0

    throw v32

    .line 164
    .restart local v3    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    .restart local v9    # "contactsWithAddresses":Landroid/util/SparseBooleanArray;
    :cond_e
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 165
    const/4 v12, 0x0

    .line 167
    const/4 v15, 0x0

    .restart local v15    # "i":I
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v16

    .restart local v16    # "length":I
    :goto_3
    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    .line 168
    invoke-virtual {v8, v15}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 169
    .restart local v6    # "contactId":I
    invoke-virtual {v9, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 167
    :goto_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 172
    :cond_f
    invoke-virtual {v8, v15}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 173
    .restart local v17    # "name":Ljava/lang/String;
    new-instance v2, Lcom/google/location/country/Postaladdress$PostalAddress;

    invoke-direct {v2}, Lcom/google/location/country/Postaladdress$PostalAddress;-><init>()V

    .line 174
    .restart local v2    # "address":Lcom/google/location/country/Postaladdress$PostalAddress;
    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    .line 175
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->sizeOfPostalAddress(Lcom/google/location/country/Postaladdress$PostalAddress;)I

    move-result v32

    move/from16 v0, v26

    move/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/DeviceAddressSource;->checkRemainingSizeAllowance(II)I

    move-result v26

    .line 177
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method
