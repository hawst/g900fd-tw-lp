.class public Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "CustomerFragment.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;


# instance fields
.field private mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

.field private mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

.field mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

.field mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

.field private mRegionCodes:[I

.field mSelectedRegionCode:I

.field mSubForms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/ui/common/Form;",
            ">;"
        }
    .end annotation
.end field

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 70
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x681

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSelectedRegionCode:I

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    return-void
.end method

.method public static newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;I)Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;
    .locals 4
    .param p0, "customerForm"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->readOnlyField:[I

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->contains([II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 101
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Customer form with both a legal country selector and a legal address form containing a country selector is not supported"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :cond_0
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;-><init>()V

    .line 107
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 108
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "customerForm"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 109
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 111
    return-object v1
.end method

.method private setLegalMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;)V
    .locals 2
    .param p1, "message"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    .line 521
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V

    .line 523
    const/4 v0, 0x6

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 525
    return-void

    .line 521
    :cond_0
    iget-object v0, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->messageText:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    goto :goto_0
.end method

.method private validate(Z)Z
    .locals 6
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v4, 0x0

    .line 125
    const/4 v3, 0x1

    .line 126
    .local v3, "valid":Z
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 127
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    .line 128
    .local v0, "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    if-eqz p1, :cond_2

    .line 129
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->validate()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    .line 126
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    .line 129
    goto :goto_1

    .line 130
    :cond_2
    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->isValid()Z

    move-result v5

    if-nez v5, :cond_0

    .line 134
    .end local v0    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :goto_2
    return v4

    :cond_3
    move v4, v3

    goto :goto_2
.end method


# virtual methods
.method public animateViewsBelow(IIJ)V
    .locals 5
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I
    .param p3, "delayMs"    # J

    .prologue
    .line 497
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 498
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setTranslationY(F)V

    .line 499
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 500
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 502
    :cond_0
    return-void
.end method

.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 5
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    .line 149
    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    .line 155
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown FormFieldMessage fieldId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 159
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 160
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    invoke-interface {v2, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    const/4 v2, 0x1

    .line 165
    :goto_1
    return v2

    .line 159
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public doEnableUi()V
    .locals 4

    .prologue
    .line 182
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-nez v3, :cond_0

    .line 195
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->isUiEnabled()Z

    move-result v2

    .line 188
    .local v2, "uiEnabled":Z
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    if-eqz v3, :cond_1

    .line 189
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setEnabled(Z)V

    .line 191
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 192
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    invoke-interface {v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->enableUi(Z)V

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 194
    :cond_2
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 3

    .prologue
    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 140
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->focusOnFirstInvalidFormField()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const/4 v2, 0x1

    .line 144
    :goto_1
    return v2

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getButtonBarExpandButtonText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->getExpandLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChildren()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 443
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;>;"
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    if-eqz v4, :cond_0

    .line 444
    new-instance v4, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;

    const/16 v5, 0x684

    invoke-direct {v4, v5, p0}, Lcom/google/android/wallet/instrumentmanager/analytics/SimpleUiNode;-><init>(ILcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "length":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 449
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    .line 450
    .local v1, "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    instance-of v4, v1, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    if-eqz v4, :cond_1

    .line 451
    check-cast v1, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 455
    :cond_2
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v4, :cond_3

    .line 456
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    :cond_3
    return-object v0
.end method

.method public getCustomerFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;
    .locals 7

    .prologue
    .line 392
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;-><init>()V

    .line 393
    .local v0, "customer":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    if-eqz v4, :cond_0

    .line 394
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->getSelectedRegionCode()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalCountryCode:Ljava/lang/String;

    .line 397
    :cond_0
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    if-eqz v4, :cond_1

    .line 398
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;->opaqueData:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalDocData:Ljava/lang/String;

    .line 400
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "length":I
    :goto_0
    if-ge v2, v3, :cond_6

    .line 401
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    .line 402
    .local v1, "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    instance-of v4, v1, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;

    if-eqz v4, :cond_2

    .line 403
    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;

    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->getTaxInfoFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    move-result-object v4

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->taxInfo:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    .line 400
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 404
    .restart local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_2
    instance-of v4, v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    if-eqz v4, :cond_3

    .line 405
    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getAddressFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    move-result-object v4

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->legalAddress:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressFormValue;

    goto :goto_1

    .line 406
    .restart local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_3
    instance-of v4, v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    if-eqz v4, :cond_4

    .line 407
    new-instance v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v4}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 408
    iget-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getCreditCardFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    move-result-object v5

    iput-object v5, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    goto :goto_1

    .line 410
    .restart local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_4
    instance-of v4, v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    if-eqz v4, :cond_5

    .line 411
    new-instance v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v4}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 412
    iget-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;->instrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getUsernamePasswordFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    move-result-object v5

    iput-object v5, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    goto :goto_1

    .line 415
    .restart local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_5
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Form "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not supported as a subform"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 418
    .end local v1    # "form":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_6
    return-object v0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public handleErrorMessageDismissed(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "formId"    # Ljava/lang/String;
    .param p2, "errorType"    # I

    .prologue
    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 171
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    invoke-interface {v2, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->handleErrorMessageDismissed(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    const/4 v2, 0x1

    .line 175
    :goto_1
    return v2

    .line 170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public isReadyToSubmit()Z
    .locals 3

    .prologue
    .line 199
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 200
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;

    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Form;->isReadyToSubmit()Z

    move-result v2

    if-nez v2, :cond_0

    .line 201
    const/4 v2, 0x0

    .line 204
    :goto_1
    return v2

    .line 199
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 349
    .local v0, "instrumentFormFragment":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    if-eqz v1, :cond_0

    .line 350
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 352
    :cond_0
    return-void
.end method

.method public onButtonBarExpandButtonClicked()V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->expand(Z)V

    .line 512
    return-void
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 423
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-ne p1, v1, :cond_0

    .line 425
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {p2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    move-result-object v0

    .line 430
    .local v0, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "tagTosWebViewDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 209
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 210
    if-eqz p1, :cond_0

    .line 211
    const-string v0, "selectedRegionCode"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSelectedRegionCode:I

    .line 213
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "customerForm"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    .line 214
    return-void
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 219
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_customer:I

    invoke-virtual {p1, v4, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 221
    .local v0, "content":Landroid/view/View;
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->customer_legal_message_text:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    .line 223
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v4, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setParentUiNode(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 224
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v4, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setUrlClickListener(Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    .line 227
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-nez v4, :cond_0

    .line 228
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    if-nez v4, :cond_1

    move-object v4, v5

    :goto_0
    invoke-direct {p0, v4}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->setLegalMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;)V

    .line 231
    :cond_0
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-eqz v4, :cond_3

    .line 232
    if-nez p3, :cond_a

    .line 234
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    array-length v4, v4

    if-gtz v4, :cond_2

    .line 235
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "LegalCountrySelector\'s allowed country codes cannot be empty"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 228
    :cond_1
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;->defaultMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    goto :goto_0

    .line 238
    :cond_2
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->allowedCountryCode:[Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->stringArrayToRegionCodeArray([Ljava/lang/String;)[I

    move-result-object v3

    .line 242
    .local v3, "regionCodes":[I
    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->scrubAndSortRegionCodes([I)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodes:[I

    .line 247
    .end local v3    # "regionCodes":[I
    :goto_1
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->region_code_view:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    .line 248
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v4, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setVisibility(I)V

    .line 249
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v4, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 250
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodes:[I

    invoke-virtual {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setRegionCodes([I)V

    .line 251
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    iget-object v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toRegionCode(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->setSelectedRegionCode(I)V

    .line 255
    :cond_3
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    array-length v4, v4

    if-lez v4, :cond_5

    .line 256
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->tax_info_fragment_holder:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->tax_info_fragment_holder:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;

    .line 260
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;
    if-nez v1, :cond_4

    .line 261
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->taxInfoForm:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->initialTaxInfoForm:I

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getThemeResourceId()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->newInstance([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;II)Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;

    move-result-object v1

    .line 263
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->tax_info_fragment_holder:I

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 267
    :cond_4
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    .end local v1    # "fragment":Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;
    :cond_5
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    if-eqz v4, :cond_7

    .line 271
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->legal_address_entry_fragment_holder:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 274
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->legal_address_entry_fragment_holder:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    .line 276
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    if-nez v1, :cond_6

    .line 277
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getThemeResourceId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;I)Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    move-result-object v1

    .line 279
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->legal_address_entry_fragment_holder:I

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 283
    :cond_6
    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->setOnRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V

    .line 284
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    .end local v1    # "fragment":Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
    :cond_7
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v4, :cond_9

    .line 288
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    if-eqz v4, :cond_b

    .line 292
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    .line 294
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
    if-nez v1, :cond_8

    .line 295
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getThemeResourceId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    move-result-object v1

    .line 297
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 304
    :cond_8
    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->setOnStateChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment$OnAddCreditCardFragmentStateChangedListener;)V

    .line 305
    move-object v2, v1

    .line 326
    .end local v1    # "fragment":Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;
    .local v2, "instrumentForm":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :goto_2
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSubForms:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    .end local v2    # "instrumentForm":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->doEnableUi()V

    .line 330
    return-object v0

    .line 244
    :cond_a
    const-string v4, "regionCodes"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodes:[I

    goto/16 :goto_1

    .line 306
    :cond_b
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    if-eqz v4, :cond_d

    .line 307
    sget v4, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 310
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    .line 313
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;
    if-nez v1, :cond_c

    .line 314
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getThemeResourceId()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;I)Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    move-result-object v1

    .line 317
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->instrument_form_fragment_holder:I

    invoke-virtual {v4, v5, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 321
    :cond_c
    move-object v2, v1

    .line 322
    .restart local v2    # "instrumentForm":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    goto :goto_2

    .line 323
    .end local v1    # "fragment":Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;
    .end local v2    # "instrumentForm":Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
    :cond_d
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Instrument form did not contain a recognized subform."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public onRegionCodeSelected(II)V
    .locals 4
    .param p1, "regionCode"    # I
    .param p2, "senderId"    # I

    .prologue
    .line 372
    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSelectedRegionCode:I

    if-eq v2, p1, :cond_1

    .line 373
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSelectedRegionCode:I

    .line 374
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "country":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodeView:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeView;->getId()I

    move-result v2

    if-ne p2, v2, :cond_0

    .line 377
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalCountrySelector:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$CountrySelector;->initialCountryCode:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 378
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 379
    .local v1, "eventDetails":Landroid/os/Bundle;
    const-string v2, "FormEventListener.EXTRA_FORM_ID"

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v2, "FormEventListener.EXTRA_FIELD_ID"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 382
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 386
    .end local v1    # "eventDetails":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mCustomerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;->legalMessages:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;

    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->findLegalMessageByCountry(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->setLegalMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;)V

    .line 389
    .end local v0    # "country":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 335
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 337
    const-string v0, "regionCodes"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mRegionCodes:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 338
    const-string v0, "selectedRegionCode"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mSelectedRegionCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 356
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 358
    const/4 v0, 0x6

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 360
    return-void
.end method

.method public shouldShowButtonBarExpandButton()Z
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->isExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showViewsBelow(ZZIIJ)V
    .locals 5
    .param p1, "show"    # Z
    .param p2, "animate"    # Z
    .param p3, "startDeltaY"    # I
    .param p4, "endDeltaY"    # I
    .param p5, "delayMs"    # J

    .prologue
    const/16 v1, 0xe

    .line 470
    if-eqz p2, :cond_3

    .line 471
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 475
    :cond_0
    if-eqz p1, :cond_2

    .line 476
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-static {v0, p3, p4}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 482
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_1

    .line 483
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 488
    :cond_1
    :goto_1
    return-void

    .line 478
    :cond_2
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-static {v0, p3, p4}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToGone(Landroid/view/View;II)V

    goto :goto_0

    .line 486
    :cond_3
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->mLegalMessageText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-eqz p1, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const/16 v0, 0x8

    goto :goto_2
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->validate(Z)Z

    move-result v0

    return v0
.end method
