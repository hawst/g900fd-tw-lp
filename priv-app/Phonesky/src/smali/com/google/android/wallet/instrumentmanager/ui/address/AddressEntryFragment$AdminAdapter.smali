.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AddressEntryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AdminAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;",
        ">;"
    }
.end annotation


# instance fields
.field private mHiddenView:Landroid/view/View;

.field private mPromptView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/ContextThemeWrapper;ILjava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/view/ContextThemeWrapper;
    .param p2, "textViewResourceId"    # I
    .param p4, "hint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ContextThemeWrapper;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "adminAreas":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2053
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2055
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;

    move-object v3, v1

    move-object v4, p4

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminArea;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->insert(Ljava/lang/Object;I)V

    .line 2056
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2093
    const/4 v0, 0x0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 2077
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2078
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2079
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    .line 2080
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2081
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2083
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    .line 2088
    :goto_0
    return-object v0

    .line 2085
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mHiddenView:Landroid/view/View;

    if-ne p2, v0, :cond_2

    .line 2086
    const/4 p2, 0x0

    .line 2088
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 2060
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2061
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 2062
    const/4 v0, 0x0

    invoke-super {p0, v0, v2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    .line 2064
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2065
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2067
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    .line 2072
    :goto_0
    return-object v0

    .line 2069
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$AdminAdapter;->mPromptView:Landroid/widget/TextView;

    if-ne p2, v0, :cond_2

    .line 2070
    const/4 p2, 0x0

    .line 2072
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2098
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
