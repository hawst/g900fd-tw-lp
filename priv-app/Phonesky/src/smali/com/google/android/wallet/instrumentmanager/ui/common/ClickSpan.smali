.class public Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;
.super Landroid/text/style/URLSpan;
.source "ClickSpan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 29
    if-nez p2, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "listener should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->mListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    .line 33
    return-void
.end method

.method public static clickify(Landroid/widget/TextView;Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V
    .locals 15
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    .prologue
    .line 45
    new-instance v10, Landroid/text/SpannableString;

    invoke-static/range {p1 .. p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-direct {v10, v12}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 46
    .local v10, "text":Landroid/text/SpannableString;
    const/4 v12, 0x0

    invoke-virtual {v10}, Landroid/text/SpannableString;->length()I

    move-result v13

    const-class v14, Landroid/text/style/URLSpan;

    invoke-virtual {v10, v12, v13, v14}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/text/style/URLSpan;

    .line 47
    .local v9, "spans":[Landroid/text/style/URLSpan;
    move-object v1, v9

    .local v1, "arr$":[Landroid/text/style/URLSpan;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v1, v2

    .line 48
    .local v5, "span":Landroid/text/style/URLSpan;
    invoke-virtual {v5}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    .line 49
    .local v11, "url":Ljava/lang/String;
    invoke-virtual {v10, v5}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    .line 50
    .local v8, "spanStart":I
    invoke-virtual {v10, v5}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 51
    .local v6, "spanEnd":I
    invoke-virtual {v10, v5}, Landroid/text/SpannableString;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    .line 52
    .local v7, "spanFlags":I
    invoke-virtual {v10, v5}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 53
    new-instance v12, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;

    move-object/from16 v0, p2

    invoke-direct {v12, v11, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;-><init>(Ljava/lang/String;Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    invoke-virtual {v10, v12, v8, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    .end local v5    # "span":Landroid/text/style/URLSpan;
    .end local v6    # "spanEnd":I
    .end local v7    # "spanFlags":I
    .end local v8    # "spanStart":I
    .end local v11    # "url":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v4

    .line 57
    .local v4, "movementMethod":Landroid/text/method/MovementMethod;
    instance-of v12, v4, Landroid/text/method/LinkMovementMethod;

    if-nez v12, :cond_1

    .line 58
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v12

    invoke-virtual {p0, v12}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 60
    :cond_1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->mListener:Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;->onClick(Landroid/view/View;Ljava/lang/String;)V

    .line 38
    return-void
.end method
