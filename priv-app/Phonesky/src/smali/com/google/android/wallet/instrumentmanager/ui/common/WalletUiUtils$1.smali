.class final Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "WalletUiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;IIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$animationDurationMs:J

.field final synthetic val$endDeltaY:I

.field final synthetic val$previousDuration:J

.field final synthetic val$startDeltaY:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;JIIJ)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$view:Landroid/view/View;

    iput-wide p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$previousDuration:J

    iput p4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$startDeltaY:I

    iput p5, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$endDeltaY:I

    iput-wide p6, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$animationDurationMs:J

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$previousDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 120
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$view:Landroid/view/View;

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$startDeltaY:I

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$endDeltaY:I

    iget-wide v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;->val$animationDurationMs:J

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearingInternal(Landroid/view/View;IIJ)V
    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->access$000(Landroid/view/View;IIJ)V

    .line 122
    return-void
.end method
