.class public Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;
.super Ljava/lang/Object;
.source "EditTextAutoAdvanceListener.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

.field private final mAutoFocusHandler:Landroid/os/Handler;

.field private final mAutoRetreat:Z

.field private final mCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

.field private final mEditText:Landroid/widget/EditText;

.field private final mValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V
    .locals 1
    .param p1, "editText"    # Landroid/widget/EditText;
    .param p2, "completable"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;
    .param p3, "validatable"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;
    .param p4, "autoRetreat"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mEditText:Landroid/widget/EditText;

    .line 38
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

    .line 39
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 40
    iput-boolean p4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoRetreat:Z

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoFocusHandler:Landroid/os/Handler;

    .line 42
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 79
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 82
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 46
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-nez v2, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    if-lez p4, :cond_4

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mCompletable:Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;

    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 51
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mValidatable:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

    if-eqz v2, :cond_2

    .line 53
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;->onAutoAdvanced()V

    .line 55
    :cond_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mEditText:Landroid/widget/EditText;

    const/16 v3, 0x82

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "nextView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 57
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/AndroidUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 58
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoFocusHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;Landroid/view/View;)V

    const-wide/16 v4, 0x2ee

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 65
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 69
    .end local v0    # "nextView":Landroid/view/View;
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoRetreat:Z

    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mEditText:Landroid/widget/EditText;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->focusSearch(I)Landroid/view/View;

    move-result-object v1

    .line 72
    .local v1, "previousView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public setAutoAdvancedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/listeners/EditTextAutoAdvanceListener;->mAutoAdvancedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/AutoAdvancedListener;

    .line 86
    return-void
.end method
