.class Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;
.super Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;
.source "InstrumentManagerSidecar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SavePageResponseListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;",
        "Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BackgroundEventRequestResponseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic handleResponse(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/protobuf/nano/MessageNano;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
    .param p2, "x1"    # Lcom/google/protobuf/nano/MessageNano;

    .prologue
    .line 182
    check-cast p1, Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;

    .end local p1    # "x0":Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;
    check-cast p2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .end local p2    # "x1":Lcom/google/protobuf/nano/MessageNano;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;->handleResponse(Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;)V

    return-void
.end method

.method public handleResponse(Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/wallet/instrumentmanager/api/http/SecureSavePageRequest;
    .param p2, "response"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    # setter for: Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->mSavePageResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;
    invoke-static {v0, p2}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->access$002(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    .line 188
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar$SavePageResponseListener;->this$0:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    iget-object v1, p2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iget-object v2, p2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    # invokes: Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->updateStateAndSendAnalyticsEvent(Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->access$100(Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;Lcom/google/android/wallet/instrumentmanager/api/http/BackgroundEventRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;)V

    .line 189
    return-void
.end method
