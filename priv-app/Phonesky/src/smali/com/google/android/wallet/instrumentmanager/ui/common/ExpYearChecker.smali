.class public Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;
.source "ExpYearChecker.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# instance fields
.field private final mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V
    .locals 1
    .param p1, "expYearText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p2, "expDateChecker"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .prologue
    .line 16
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V

    .line 17
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 18
    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validate()Z
    .locals 3

    .prologue
    .line 27
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 28
    .local v0, "valid":Z
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpYearChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 31
    :cond_0
    return v0

    .line 27
    .end local v0    # "valid":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
