.class public final Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;
.super Ljava/lang/Object;
.source "InstrumentManagerClickEvent.java"


# instance fields
.field public final clickPath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "clickPath":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .local v0, "clickString":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 28
    const-string v3, " -> "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    :cond_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;->clickPath:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    iget v3, v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
