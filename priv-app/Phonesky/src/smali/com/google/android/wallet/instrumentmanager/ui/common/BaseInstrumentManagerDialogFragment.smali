.class public Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "BaseInstrumentManagerDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method protected static createArgs(I)Landroid/os/Bundle;
    .locals 2
    .param p0, "themeResourceId"    # I

    .prologue
    .line 19
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 20
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "themeResourceId"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 21
    return-object v0
.end method


# virtual methods
.method protected final getThemedContext()Landroid/view/ContextThemeWrapper;
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "themeResourceId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 29
    .local v0, "themeResourceId":I
    if-gtz v0, :cond_0

    .line 30
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid theme resource id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    :cond_0
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1
.end method

.method protected final getThemedLayoutInflater()Landroid/view/LayoutInflater;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method
