.class public Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;
.source "ExpMonthChecker.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# instance fields
.field private final mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

.field private final mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;)V
    .locals 1
    .param p1, "expMonthText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p2, "expYearText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p3, "expDateChecker"    # Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .prologue
    .line 21
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V

    .line 22
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 23
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    .line 24
    return-void
.end method


# virtual methods
.method public isComplete()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 29
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    .line 30
    .local v0, "ch":C
    const/16 v3, 0x30

    if-eq v0, v3, :cond_0

    const/16 v3, 0x31

    if-eq v0, v3, :cond_0

    .line 32
    .end local v0    # "ch":C
    :goto_0
    return v1

    .restart local v0    # "ch":C
    :cond_0
    move v1, v2

    .line 30
    goto :goto_0

    .line 32
    .end local v0    # "ch":C
    :cond_1
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;->isComplete()Z

    move-result v1

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 65
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setSelection(I)V

    .line 69
    :cond_0
    return-void
.end method

.method public validate()Z
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mFormEditText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpMonthChecker;->mExpDateChecker:Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;

    invoke-interface {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;->validate()Z

    move-result v0

    .line 60
    :goto_0
    return v0

    .line 58
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 60
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
