.class public final Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;
.super Ljava/lang/Object;
.source "InstrumentManagerRequestQueue.java"


# static fields
.field private static final MAX_IMAGE_CACHE_SIZE_BYTES:I

.field private static sImageInstance:Lcom/android/volley/RequestQueue;

.field private static sInstance:Lcom/android/volley/RequestQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->diskCacheSizeBytes:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->MAX_IMAGE_CACHE_SIZE_BYTES:I

    return-void
.end method

.method public static declared-synchronized getApiRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;
    .locals 6
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 51
    const-class v4, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sInstance:Lcom/android/volley/RequestQueue;

    if-nez v3, :cond_0

    .line 52
    new-instance v2, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v5, Lcom/google/android/volley/GoogleHttpClientStack;

    sget-object v3, Lcom/google/android/wallet/instrumentmanager/config/G;->allowPiiLogging:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v5, p0, v3}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v2, v5}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;)V

    .line 54
    .local v2, "network":Lcom/android/volley/Network;
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v5, "wallet_im_volley_api_cache"

    invoke-direct {v1, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 56
    .local v1, "cacheDir":Ljava/io/File;
    new-instance v0, Lcom/android/volley/toolbox/DiskBasedCache;

    const/high16 v3, 0x100000

    invoke-direct {v0, v1, v3}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    .line 57
    .local v0, "cache":Lcom/android/volley/toolbox/DiskBasedCache;
    new-instance v3, Lcom/android/volley/RequestQueue;

    const/4 v5, 0x2

    invoke-direct {v3, v0, v2, v5}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;I)V

    sput-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sInstance:Lcom/android/volley/RequestQueue;

    .line 58
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sInstance:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3}, Lcom/android/volley/RequestQueue;->start()V

    .line 60
    .end local v0    # "cache":Lcom/android/volley/toolbox/DiskBasedCache;
    .end local v1    # "cacheDir":Ljava/io/File;
    .end local v2    # "network":Lcom/android/volley/Network;
    :cond_0
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sInstance:Lcom/android/volley/RequestQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v4

    return-object v3

    .line 51
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static declared-synchronized getImageRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;
    .locals 6
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 82
    const-class v4, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sImageInstance:Lcom/android/volley/RequestQueue;

    if-nez v3, :cond_0

    .line 83
    new-instance v2, Lcom/android/volley/toolbox/BasicNetwork;

    new-instance v5, Lcom/google/android/volley/GoogleHttpClientStack;

    sget-object v3, Lcom/google/android/wallet/instrumentmanager/config/G;->allowPiiLogging:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v5, p0, v3}, Lcom/google/android/volley/GoogleHttpClientStack;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v2, v5}, Lcom/android/volley/toolbox/BasicNetwork;-><init>(Lcom/android/volley/toolbox/HttpStack;)V

    .line 85
    .local v2, "network":Lcom/android/volley/Network;
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v5, "wallet_im_volley_image_cache"

    invoke-direct {v1, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 87
    .local v1, "cacheDir":Ljava/io/File;
    new-instance v0, Lcom/android/volley/toolbox/DiskBasedCache;

    sget v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->MAX_IMAGE_CACHE_SIZE_BYTES:I

    invoke-direct {v0, v1, v3}, Lcom/android/volley/toolbox/DiskBasedCache;-><init>(Ljava/io/File;I)V

    .line 88
    .local v0, "cache":Lcom/android/volley/toolbox/DiskBasedCache;
    new-instance v3, Lcom/android/volley/RequestQueue;

    const/4 v5, 0x6

    invoke-direct {v3, v0, v2, v5}, Lcom/android/volley/RequestQueue;-><init>(Lcom/android/volley/Cache;Lcom/android/volley/Network;I)V

    sput-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sImageInstance:Lcom/android/volley/RequestQueue;

    .line 89
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sImageInstance:Lcom/android/volley/RequestQueue;

    invoke-virtual {v3}, Lcom/android/volley/RequestQueue;->start()V

    .line 91
    .end local v0    # "cache":Lcom/android/volley/toolbox/DiskBasedCache;
    .end local v1    # "cacheDir":Ljava/io/File;
    .end local v2    # "network":Lcom/android/volley/Network;
    :cond_0
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->sImageInstance:Lcom/android/volley/RequestQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v4

    return-object v3

    .line 82
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method
