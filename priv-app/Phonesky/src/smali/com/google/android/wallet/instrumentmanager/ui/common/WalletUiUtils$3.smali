.class final Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "WalletUiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingInternal(Landroid/view/View;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$finalViewVisibility:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$view:Landroid/view/View;

    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$finalViewVisibility:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$view:Landroid/view/View;

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$finalViewVisibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$view:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 199
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 200
    return-void
.end method
