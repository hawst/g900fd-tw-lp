.class public Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;
.super Landroid/widget/LinearLayout;
.source "TopBarView.java"


# instance fields
.field mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

.field mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "defResStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 44
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->title_separator:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "separator":Landroid/view/View;
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->title:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleTextView:Landroid/widget/TextView;

    .line 52
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->title_icon:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [I

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$attr;->imTitleSeparatorBackground:I

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v2

    .line 58
    .local v2, "typedValue":Landroid/util/TypedValue;
    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->setViewBackgroundOrHide(Landroid/view/View;Landroid/util/TypedValue;)V

    .line 59
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "titleImageFifeUrl"    # Ljava/lang/String;
    .param p3, "titleImageContentDescription"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 78
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setFadeIn(Z)V

    .line 87
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->getImageLoader(Landroid/content/Context;)Lcom/android/volley/toolbox/ImageLoader;

    move-result-object v2

    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->useWebPForFife:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, p2, v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setFifeImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    invoke-virtual {v0, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 94
    :goto_1
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->mTitleImageView:Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setVisibility(I)V

    goto :goto_1
.end method
