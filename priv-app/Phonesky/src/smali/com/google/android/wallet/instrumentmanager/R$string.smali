.class public final Lcom/google/android/wallet/instrumentmanager/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final wallet_im_accessibility_event_form_field_error:I = 0x7f0c0001

.field public static final wallet_im_address_field_address_line_1:I = 0x7f0c0002

.field public static final wallet_im_address_field_address_line_2:I = 0x7f0c0003

.field public static final wallet_im_address_field_address_line_3:I = 0x7f0c0004

.field public static final wallet_im_address_field_admin_area_area:I = 0x7f0c0005

.field public static final wallet_im_address_field_admin_area_county:I = 0x7f0c0006

.field public static final wallet_im_address_field_admin_area_department:I = 0x7f0c0007

.field public static final wallet_im_address_field_admin_area_district:I = 0x7f0c0008

.field public static final wallet_im_address_field_admin_area_do_si:I = 0x7f0c0009

.field public static final wallet_im_address_field_admin_area_emirate:I = 0x7f0c000a

.field public static final wallet_im_address_field_admin_area_island:I = 0x7f0c000b

.field public static final wallet_im_address_field_admin_area_parish:I = 0x7f0c000c

.field public static final wallet_im_address_field_admin_area_prefecture:I = 0x7f0c000d

.field public static final wallet_im_address_field_admin_area_province:I = 0x7f0c000e

.field public static final wallet_im_address_field_admin_area_region:I = 0x7f0c000f

.field public static final wallet_im_address_field_admin_area_state:I = 0x7f0c0010

.field public static final wallet_im_address_field_country:I = 0x7f0c0011

.field public static final wallet_im_address_field_dependent_locality:I = 0x7f0c0012

.field public static final wallet_im_address_field_locality:I = 0x7f0c0013

.field public static final wallet_im_address_field_organization:I = 0x7f0c0014

.field public static final wallet_im_address_field_postal_code:I = 0x7f0c0015

.field public static final wallet_im_address_field_sorting_code:I = 0x7f0c0016

.field public static final wallet_im_address_field_zip_code:I = 0x7f0c0017

.field public static final wallet_im_close:I = 0x7f0c0019

.field public static final wallet_im_error_address_field_invalid:I = 0x7f0c001f

.field public static final wallet_im_error_creditcard_number_invalid:I = 0x7f0c0020

.field public static final wallet_im_error_cvc_invalid:I = 0x7f0c0021

.field public static final wallet_im_error_email_address_invalid:I = 0x7f0c0022

.field public static final wallet_im_error_expired_credit_card:I = 0x7f0c0023

.field public static final wallet_im_error_field_must_not_be_empty:I = 0x7f0c0024

.field public static final wallet_im_error_only_numeric_digits_allowed:I = 0x7f0c0027

.field public static final wallet_im_error_title:I = 0x7f0c0028

.field public static final wallet_im_error_year_invalid:I = 0x7f0c0029

.field public static final wallet_im_network_error_message:I = 0x7f0c002d

.field public static final wallet_im_network_error_title:I = 0x7f0c002e

.field public static final wallet_im_phone_number:I = 0x7f0c002f

.field public static final wallet_im_retry:I = 0x7f0c0030

.field public static final wallet_im_unknown_error:I = 0x7f0c0032
