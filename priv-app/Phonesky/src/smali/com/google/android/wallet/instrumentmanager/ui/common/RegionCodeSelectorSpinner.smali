.class public Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;
.super Landroid/widget/Spinner;
.source "RegionCodeSelectorSpinner.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;
    }
.end annotation


# instance fields
.field private mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

.field private mThemedContext:Landroid/view/ContextThemeWrapper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setThemedContext(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setThemedContext(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setThemedContext(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;)Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    return-object v0
.end method

.method private setThemedContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    instance-of v0, p1, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_0

    .line 51
    check-cast p1, Landroid/view/ContextThemeWrapper;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mThemedContext:Landroid/view/ContextThemeWrapper;

    .line 56
    return-void

    .line 53
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RegionCodeSelectorSpinner must be used with a ContextThemeWrapper"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getRegionCodeCount()I
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getCount()I

    move-result v0

    return v0
.end method

.method public getSelectedRegionCode()I
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getRegionCodeCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getSelectedRegionCode()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRegionCodeSelectedListener(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    .line 113
    return-void
.end method

.method public setRegionCodes([I)V
    .locals 5
    .param p1, "regionCodes"    # [I

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mThemedContext:Landroid/view/ContextThemeWrapper;

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_row_spinner:I

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->description:I

    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->toWrapperArray([I)[Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;-><init>(Landroid/view/ContextThemeWrapper;II[Ljava/lang/Integer;)V

    .line 85
    .local v0, "adapter":Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_spinner_dropdown:I

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->setDropDownViewResource(I)V

    .line 86
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 87
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;

    invoke-direct {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;)V

    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 103
    return-void
.end method

.method public setSelectedRegionCode(I)V
    .locals 3
    .param p1, "regionCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Populate selector with region codes before setting the selected Region Code"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 70
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getSelectedRegionCode()I

    move-result v1

    if-eq p1, v1, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$RegionCodeAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 73
    .local v0, "index":I
    if-ltz v0, :cond_1

    .line 74
    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setSelection(I)V

    .line 77
    .end local v0    # "index":I
    :cond_1
    return-void
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->isValid()Z

    move-result v0

    return v0
.end method
