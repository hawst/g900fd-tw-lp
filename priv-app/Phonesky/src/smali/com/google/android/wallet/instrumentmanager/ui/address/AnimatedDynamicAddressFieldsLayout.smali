.class public Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;
.super Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;
.source "AnimatedDynamicAddressFieldsLayout.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private mActiveAnimator:Landroid/animation/ValueAnimator;

.field private mAnimator1:Landroid/animation/ValueAnimator;

.field private mAnimator2:Landroid/animation/ValueAnimator;

.field private mExpandCalledWhileContracting:Z

.field private mNewFields:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingReplaceNewViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingReplaceOldViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceNewViews:Ljava/util/ArrayList;

    .line 60
    return-void
.end method

.method private getViewHeightAtTransitionStart()I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method private invokeOnHeightChanged(F)V
    .locals 1
    .param p1, "heightOffset"    # F

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mOnHeightOffsetChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;

    invoke-interface {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout$OnHeightOffsetChangedListener;->onHeightOffsetChanged(F)V

    .line 237
    :cond_0
    return-void
.end method

.method private setFieldsLayerType(I)V
    .locals 4
    .param p1, "layerType"    # I

    .prologue
    .line 240
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .local v0, "childCount":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 241
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 240
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_0
    return-void
.end method

.method private switchAnimator()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    if-ne v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator2:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    goto :goto_0
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 228
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v3, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 193
    invoke-direct {p0, v6}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->setFieldsLayerType(I)V

    .line 195
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 197
    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    if-ne v2, v3, :cond_2

    .line 198
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 199
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 206
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mNewFields:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 207
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mNewFields:Ljava/util/ArrayList;

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->setFields(Ljava/util/ArrayList;)V

    .line 208
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mNewFields:Ljava/util/ArrayList;

    .line 212
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "size":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 213
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceNewViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->replaceView(Landroid/view/View;Landroid/view/View;)V

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 200
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_2
    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 201
    iput v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 202
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 215
    .restart local v0    # "i":I
    .restart local v1    # "size":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 216
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceNewViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 220
    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mExpandCalledWhileContracting:Z

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    if-ne v2, v7, :cond_4

    .line 221
    iput-boolean v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mExpandCalledWhileContracting:Z

    .line 222
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->switchAnimator()V

    .line 223
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->switchToShowingFields()V

    .line 225
    :cond_4
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 231
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 188
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->setFieldsLayerType(I)V

    .line 189
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 175
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 176
    .local v4, "t":F
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .local v1, "childCount":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 177
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 178
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 179
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v4

    invoke-virtual {v0, v5}, Landroid/view/View;->setY(F)V

    .line 176
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    sub-float v6, v7, v4

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 182
    sub-float v5, v7, v4

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->getViewHeightAtTransitionStart()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float v2, v5, v6

    .line 183
    .local v2, "heightOffset":F
    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->invokeOnHeightChanged(F)V

    .line 184
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->onFinishInflate()V

    .line 65
    new-array v0, v3, [F

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    .line 66
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 68
    new-array v0, v3, [F

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator2:Landroid/animation/ValueAnimator;

    .line 69
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mAnimator1:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    .line 72
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 155
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->onSizeChanged(IIII)V

    .line 159
    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v2, v1

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->getViewHeightAtTransitionStart()I

    move-result v2

    sub-int/2addr v2, p2

    int-to-float v2, v2

    mul-float v0, v1, v2

    .line 161
    .local v0, "translationY":F
    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->invokeOnHeightChanged(F)V

    .line 162
    return-void
.end method

.method public replaceView(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "oldView"    # Landroid/view/View;
    .param p2, "newView"    # Landroid/view/View;

    .prologue
    .line 250
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceNewViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/address/DynamicAddressFieldsLayout;->replaceView(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method public setFields(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "fields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 78
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->addViews(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 85
    :pswitch_1
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mNewFields:Ljava/util/ArrayList;

    .line 87
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceOldViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 89
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mPendingReplaceNewViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 93
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->addViews(Ljava/util/ArrayList;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public switchToShowingFields()V
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 119
    :goto_0
    :pswitch_0
    return-void

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 107
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 108
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 114
    :pswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mExpandCalledWhileContracting:Z

    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 107
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public switchToShowingProgressBar()V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 123
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    packed-switch v0, :pswitch_data_0

    .line 139
    :goto_0
    :pswitch_0
    return-void

    .line 125
    :pswitch_1
    iput v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 126
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 128
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 129
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 130
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 133
    :pswitch_2
    iput v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mState:I

    .line 134
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AnimatedDynamicAddressFieldsLayout;->mActiveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 127
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
