.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;
.super Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;
.source "CreditCardImagesAnimatorIcs.java"


# instance fields
.field private final mGeneralImage:Landroid/widget/ImageView;

.field private mInOneCardMode:Z


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "images"    # [Landroid/widget/ImageView;
    .param p2, "generalImage"    # Landroid/widget/ImageView;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;-><init>([Landroid/widget/ImageView;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    .line 26
    return-void
.end method


# virtual methods
.method public animateToType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 10
    .param p1, "type"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    const/4 v9, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 30
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-static {p1, v6}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->equals(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 31
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)I

    move-result v2

    .line 32
    .local v2, "index":I
    if-ne v2, v9, :cond_1

    iget-boolean v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v6, :cond_1

    move v1, v4

    .line 33
    .local v1, "inactiveAlpha":F
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v3, v6

    .local v3, "length":I
    :goto_1
    if-ge v0, v3, :cond_3

    .line 34
    if-ne v0, v2, :cond_2

    .line 35
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 36
    iget-boolean v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v6, :cond_0

    .line 37
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    .line 33
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    .end local v3    # "length":I
    :cond_1
    move v1, v5

    .line 32
    goto :goto_0

    .line 40
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    .restart local v3    # "length":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 41
    iget-boolean v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v6, :cond_0

    .line 42
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    .line 46
    :cond_3
    iget-boolean v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-eqz v6, :cond_4

    .line 47
    if-ne v2, v9, :cond_6

    .line 48
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 53
    :cond_4
    :goto_3
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 55
    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    .end local v2    # "index":I
    .end local v3    # "length":I
    :cond_5
    return-void

    .line 50
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    .restart local v2    # "index":I
    .restart local v3    # "length":I
    :cond_6
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_3
.end method

.method public restoreCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 9
    .param p1, "type"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)I

    move-result v2

    .line 60
    .local v2, "index":I
    const/4 v6, -0x1

    if-ne v2, v6, :cond_0

    move v1, v4

    .line 61
    .local v1, "inactiveAlpha":F
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v3, v6

    .local v3, "length":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 64
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 65
    if-ne v0, v2, :cond_1

    .line 66
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 67
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setX(F)V

    .line 61
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    .end local v3    # "length":I
    :cond_0
    move v1, v5

    .line 60
    goto :goto_0

    .line 69
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    .restart local v3    # "length":I
    :cond_1
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 70
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v6, v6, v0

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setTranslationX(F)V

    goto :goto_2

    .line 73
    :cond_2
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 74
    return-void
.end method

.method public switchToOneCardMode()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 83
    iget-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v3, :cond_2

    .line 84
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)I

    move-result v0

    .line 85
    .local v0, "currentIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v2, v3

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 86
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 87
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 88
    if-ne v1, v0, :cond_0

    .line 89
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v1

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 85
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v1

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_1

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 95
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setX(F)V

    .line 96
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    .line 98
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 103
    .end local v0    # "currentIndex":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_2
    :goto_2
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    .line 104
    return-void

    .line 100
    .restart local v0    # "currentIndex":I
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_2
.end method
