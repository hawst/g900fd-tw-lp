.class Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;
.super Ljava/lang/Object;
.source "CreditCardNumberEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getFormattedCreditCardNumber(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$500(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "formatted":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {p1, v1, v2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 200
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 192
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 182
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCardType()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v1

    .line 183
    .local v1, "oldCardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->updateCardNumber(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$300(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/String;)V

    .line 184
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCardType()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    move-result-object v0

    .line 185
    .local v0, "newCardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, v2, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    if-eqz v2, :cond_0

    invoke-static {v1, v0}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->equals(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    iget-object v2, v2, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    invoke-interface {v2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;->onCreditCardTypeChanged(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 189
    :cond_0
    return-void
.end method
