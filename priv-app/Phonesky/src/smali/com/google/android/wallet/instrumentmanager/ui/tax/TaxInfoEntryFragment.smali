.class public Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;
.source "TaxInfoEntryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private mCurrentTaxFormIndex:I

.field mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

.field private mTaxInfoForms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;",
            ">;"
        }
    .end annotation
.end field

.field mTaxInfoTextFields:Landroid/widget/LinearLayout;

.field private final mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x682

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-void
.end method

.method public static newInstance([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;II)Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;
    .locals 5
    .param p0, "taxInfoForms"    # [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    .param p1, "initialTaxInfoFormIndex"    # I
    .param p2, "themeResourceId"    # I

    .prologue
    .line 69
    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    .line 70
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "At least one tax form should be provided"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 72
    :cond_1
    if-ltz p1, :cond_2

    array-length v2, p0

    if-lt p1, v2, :cond_3

    .line 73
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Initial tax form index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is outside of tax forms valid range: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "[0,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 76
    :cond_3
    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "taxInfoForms"

    new-instance v3, Ljava/util/ArrayList;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProtoArrayList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 79
    const-string v2, "initialTaxInfoFormIndex"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;-><init>()V

    .line 81
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 82
    return-object v1
.end method

.method private showCurrentTaxFormFields()V
    .locals 7

    .prologue
    .line 142
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 143
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    iget v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    iget-object v2, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    .line 144
    .local v2, "uiFields":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    const/4 v0, 0x0

    .local v0, "i":I
    array-length v1, v2

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 145
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    aget-object v4, v2, v0

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->getThemedLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->createFormEditTextForTextUiField(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;ILandroid/view/LayoutInflater;)Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    return-void
.end method

.method private validate(Z)Z
    .locals 6
    .param p1, "showErrorIfInvalid"    # Z

    .prologue
    const/4 v4, 0x0

    .line 161
    const/4 v3, 0x1

    .line 162
    .local v3, "valid":Z
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 163
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 164
    .local v0, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    if-eqz p1, :cond_2

    .line 165
    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->validate()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    .line 162
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    .line 165
    goto :goto_1

    .line 166
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isValid()Z

    move-result v5

    if-nez v5, :cond_0

    .line 170
    .end local v0    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_2
    return v4

    :cond_3
    move v4, v3

    goto :goto_2
.end method


# virtual methods
.method public applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
    .locals 6
    .param p1, "formFieldMessage"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .prologue
    const/4 v3, 0x1

    .line 187
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    iget v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 188
    .local v0, "currentTaxInfoForm":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    iget-object v4, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 189
    const/4 v3, 0x0

    .line 204
    :goto_0
    return v3

    .line 191
    :cond_0
    iget-object v4, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    if-eq v4, v3, :cond_1

    .line 192
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TaxInfoForm does not support field with id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 195
    :cond_1
    iget-object v4, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget v2, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->repeatedFieldIndex:I

    .line 196
    .local v2, "repeatedFieldIndex":I
    if-ltz v2, :cond_2

    iget-object v4, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v4, v4

    if-lt v2, v4, :cond_3

    .line 198
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FormFieldMessage repeatedFieldIndex: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is out of range [0,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 202
    :cond_3
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 203
    .local v1, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    iget-object v4, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->message:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public doEnableUi()V
    .locals 4

    .prologue
    .line 210
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    if-nez v3, :cond_1

    .line 218
    :cond_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->isUiEnabled()Z

    move-result v2

    .line 214
    .local v2, "uiEnabled":Z
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    invoke-virtual {v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setEnabled(Z)V

    .line 215
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .local v1, "length":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 216
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public focusOnFirstInvalidFormField()Z
    .locals 4

    .prologue
    .line 175
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 176
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 177
    .local v0, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 178
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->requestFocusAndAnnounceError(Landroid/view/View;)V

    .line 179
    const/4 v3, 0x1

    .line 182
    .end local v0    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :goto_1
    return v3

    .line 175
    .restart local v0    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    .end local v0    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTaxInfoFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;
    .locals 7

    .prologue
    .line 234
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    iget v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    .line 235
    .local v2, "selectedTaxInfoForm":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;
    iget-object v5, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    array-length v4, v5

    .line 236
    .local v4, "textFieldsCount":I
    new-instance v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;

    invoke-direct {v3}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;-><init>()V

    .line 237
    .local v3, "taxInfoFormValue":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;
    iget-object v5, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->id:Ljava/lang/String;

    iput-object v5, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;->taxInfoFormId:Ljava/lang/String;

    .line 238
    new-array v5, v4, [Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    iput-object v5, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;->taxInfoValue:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    .line 239
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 240
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 241
    .local v0, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    iget-object v5, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;->taxInfoValue:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    new-instance v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    invoke-direct {v6}, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;-><init>()V

    aput-object v6, v5, v1

    .line 242
    iget-object v5, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;->taxInfoValue:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    aget-object v5, v5, v1

    iget-object v6, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->taxInfoField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->name:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->name:Ljava/lang/String;

    .line 243
    iget-object v5, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoFormValue;->taxInfoValue:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;

    aget-object v5, v5, v1

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiFieldValue;->stringValue:Ljava/lang/String;

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    .end local v0    # "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    :cond_0
    return-object v3
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mUiElement:Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    return-object v0
.end method

.method public isReadyToSubmit()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->validate(Z)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 90
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "taxInfoForms"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoArrayListFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string v1, "currentTaxFormIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    const-string v1, "initialTaxInfoFormIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    goto :goto_0
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 102
    sget v5, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_tax_info_entry:I

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "content":Landroid/view/View;
    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->tax_info_forms_spinner:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    iput-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    .line 104
    sget v5, Lcom/google/android/wallet/instrumentmanager/R$id;->tax_info_fields_container:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoTextFields:Landroid/widget/LinearLayout;

    .line 105
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->showCurrentTaxFormFields()V

    .line 106
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    .line 107
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    invoke-virtual {v5, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setVisibility(I)V

    .line 108
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 109
    .local v3, "taxFormsCount":I
    new-array v4, v3, [Ljava/lang/String;

    .line 110
    .local v4, "taxFormsLabels":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 111
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoForms:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;

    iget-object v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/tax/TaxInfoFormOuterClass$TaxInfoForm;->label:Ljava/lang/String;

    aput-object v5, v4, v2

    .line 110
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v6, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_row_spinner:I

    invoke-direct {v0, v5, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 115
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    sget v5, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_spinner_dropdown:I

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 116
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    invoke-virtual {v5, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 117
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    iget v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    invoke-virtual {v5, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setSelection(I)V

    .line 118
    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mTaxInfoFormSpinner:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    invoke-virtual {v5, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 120
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "taxFormsCount":I
    .end local v4    # "taxFormsLabels":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->doEnableUi()V

    .line 121
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    if-eq v0, p3, :cond_0

    .line 133
    iput p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    .line 134
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->showCurrentTaxFormFields()V

    .line 136
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "currentTaxFormIndex"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->mCurrentTaxFormIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    return-void
.end method

.method public validate()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/tax/TaxInfoEntryFragment;->validate(Z)Z

    move-result v0

    return v0
.end method
