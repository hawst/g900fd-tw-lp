.class public Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;
.super Landroid/support/v4/app/Fragment;
.source "SidecarFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;
    }
.end annotation


# instance fields
.field private mCreated:Z

.field private mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

.field mNotifyListenerOfStateChange:Z

.field private mState:I

.field private mSubstate:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 128
    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    .line 132
    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    .line 138
    return-void
.end method

.method private notifyListener()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;->onStateChange(Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;)V

    .line 259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    .line 261
    :cond_0
    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    return v0
.end method

.method public getSubstate()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mSubstate:I

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 160
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 161
    invoke-virtual {p0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->setRetainInstance(Z)V

    .line 162
    if-eqz p1, :cond_0

    .line 163
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    if-eqz v0, :cond_1

    .line 166
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->notifyListener()V

    .line 168
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mCreated:Z

    .line 169
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mCreated:Z

    .line 174
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 175
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 179
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 180
    const-string v0, "SidecarFragment.state"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    const-string v0, "SidecarFragment.substate"

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mSubstate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v0, "SidecarFragment.notifyListenerOfStateChange"

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 183
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 194
    const-string v0, "SidecarFragment.state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    .line 195
    const-string v0, "SidecarFragment.substate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mSubstate:I

    .line 196
    const-string v0, "SidecarFragment.notifyListenerOfStateChange"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    .line 198
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 199
    const-string v0, "SidecarFragment"

    const-string v1, "Restoring after serialization in RUNNING, resetting to INIT."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-virtual {p0, v2, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->setState(II)V

    .line 202
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    .line 216
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mListener:Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mCreated:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    if-eqz v0, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->notifyListener()V

    .line 219
    :cond_0
    return-void
.end method

.method protected setState(II)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "substate"    # I

    .prologue
    .line 245
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method must be called from the UI thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mSubstate:I

    if-eq p2, v0, :cond_2

    .line 249
    :cond_1
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mState:I

    .line 250
    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mSubstate:I

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->mNotifyListenerOfStateChange:Z

    .line 252
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->notifyListener()V

    .line 254
    :cond_2
    return-void
.end method
