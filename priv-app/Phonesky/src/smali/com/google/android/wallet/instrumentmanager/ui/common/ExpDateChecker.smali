.class public Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;
.super Ljava/lang/Object;
.source "ExpDateChecker.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

.field private final mMaxDate:Ljava/util/GregorianCalendar;

.field private final mMinDate:Ljava/util/GregorianCalendar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;IIII)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "expMonthText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p3, "expYearText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .param p4, "minMonth"    # I
    .param p5, "minYear"    # I
    .param p6, "maxMonth"    # I
    .param p7, "maxYear"    # I

    .prologue
    const/4 v2, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mContext:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 44
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 49
    new-instance v0, Ljava/util/GregorianCalendar;

    add-int/lit8 v1, p4, -0x1

    invoke-direct {v0, p5, v1, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mMinDate:Ljava/util/GregorianCalendar;

    .line 50
    new-instance v0, Ljava/util/GregorianCalendar;

    add-int/lit8 v1, p6, -0x1

    invoke-direct {v0, p7, v1, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mMaxDate:Ljava/util/GregorianCalendar;

    .line 51
    return-void
.end method

.method private checkExpDate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .locals 8
    .param p1, "monthText"    # Ljava/lang/CharSequence;
    .param p2, "yearText"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 55
    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 59
    .local v2, "month":I
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 60
    .local v3, "year":I
    const/16 v6, 0x64

    if-ge v3, v6, :cond_0

    .line 61
    add-int/lit16 v3, v3, 0x7d0

    .line 64
    :cond_0
    if-lt v2, v4, :cond_1

    const/16 v6, 0xc

    if-le v2, v6, :cond_3

    :cond_1
    move v4, v5

    .line 82
    .end local v2    # "month":I
    .end local v3    # "year":I
    :cond_2
    :goto_0
    return v4

    .line 72
    .restart local v2    # "month":I
    .restart local v3    # "year":I
    :cond_3
    new-instance v1, Ljava/util/GregorianCalendar;

    add-int/lit8 v6, v2, -0x1

    const/4 v7, 0x1

    invoke-direct {v1, v3, v6, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 75
    .local v1, "inputDate":Ljava/util/GregorianCalendar;
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mMinDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v6}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v6

    if-gez v6, :cond_4

    .line 76
    const/4 v4, -0x1

    goto :goto_0

    .line 77
    :cond_4
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mMaxDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v1, v6}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-gtz v5, :cond_2

    .line 80
    const/4 v4, 0x0

    goto :goto_0

    .line 81
    .end local v1    # "inputDate":Ljava/util/GregorianCalendar;
    .end local v2    # "month":I
    .end local v3    # "year":I
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NumberFormatException;
    move v4, v5

    .line 82
    goto :goto_0
.end method


# virtual methods
.method public isValid()Z
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->checkExpDate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public validate()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 93
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpMonthText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->checkExpDate(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mContext:Landroid/content/Context;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_expired_credit_card:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 100
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mExpYearText:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ExpDateChecker;->mContext:Landroid/content/Context;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_year_invalid:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
