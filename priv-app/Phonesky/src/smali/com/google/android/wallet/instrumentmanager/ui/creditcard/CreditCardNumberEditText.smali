.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
.source "CreditCardNumberEditText.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_DIGIT_GROUPING:[I


# instance fields
.field mAllowedCardTypes:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

.field private mCardNumber:Ljava/lang/String;

.field private mCardTypeAndBinRange:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;",
            "Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;",
            ">;"
        }
    .end annotation
.end field

.field private final mCreditCardNumberWatcher:Landroid/text/TextWatcher;

.field private mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

.field mInvalidBins:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

.field mOnCreditCardTypeChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

.field private mOriginalTextColors:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->DEFAULT_DIGIT_GROUPING:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x4
        0x4
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;-><init>(Landroid/content/Context;)V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCreditCardNumberWatcher:Landroid/text/TextWatcher;

    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->initializeViewAndListeners(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCreditCardNumberWatcher:Landroid/text/TextWatcher;

    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->initializeViewAndListeners(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$3;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCreditCardNumberWatcher:Landroid/text/TextWatcher;

    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->initializeViewAndListeners(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Landroid/util/Pair;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->isLuhnChecksumValid()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->updateCardNumber(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getFormattedCreditCardNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static findFirstMatchingBin([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .locals 9
    .param p0, "binRanges"    # [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .param p1, "cardNumber"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 243
    if-nez p0, :cond_0

    move-object v1, v7

    .line 257
    :goto_0
    return-object v1

    .line 246
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 247
    .local v2, "ccNumberLength":I
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 248
    .local v1, "binRange":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    iget-object v8, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v5

    .line 249
    .local v5, "length":I
    if-ge v2, v5, :cond_2

    .line 247
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 252
    :cond_2
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 253
    .local v6, "prefix":Ljava/lang/String;
    iget-object v8, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->start:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-ltz v8, :cond_1

    iget-object v8, v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->end:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-gtz v8, :cond_1

    goto :goto_0

    .end local v1    # "binRange":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v5    # "length":I
    .end local v6    # "prefix":Ljava/lang/String;
    :cond_3
    move-object v1, v7

    .line 257
    goto :goto_0
.end method

.method private getCompleteCardNumberLength()I
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    iget v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->cardNumberLength:I

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private getFormattedCreditCardNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 292
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    iget-object v0, v6, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->digitGrouping:[I

    .line 297
    .local v0, "digitGrouping":[I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCompleteCardNumberLength()I

    move-result v6

    array-length v7, v0

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 299
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    const/4 v1, 0x0

    .local v1, "group":I
    const/4 v2, 0x0

    .local v2, "groupElement":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .local v4, "length":I
    :goto_1
    if-ge v3, v4, :cond_2

    .line 300
    aget v6, v0, v1

    if-ne v6, v2, :cond_0

    .line 301
    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 302
    const/4 v2, 0x0

    .line 303
    add-int/lit8 v1, v1, 0x1

    .line 305
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 306
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 299
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 292
    .end local v0    # "digitGrouping":[I
    .end local v1    # "group":I
    .end local v2    # "groupElement":I
    .end local v3    # "i":I
    .end local v4    # "length":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->DEFAULT_DIGIT_GROUPING:[I

    goto :goto_0

    .line 308
    .restart local v0    # "digitGrouping":[I
    .restart local v1    # "group":I
    .restart local v2    # "groupElement":I
    .restart local v3    # "i":I
    .restart local v4    # "length":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private initializeViewAndListeners(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    const-string v0, "1234567890 "

    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setSingleLine()V

    .line 144
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setOriginalTextColors()V

    .line 147
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCreditCardNumberWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->addTextChangedListenerToFront(Landroid/text/TextWatcher;)V

    .line 154
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->addOnTextChangeValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 165
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_creditcard_number_invalid:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$2;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 172
    return-void
.end method

.method private isLuhnChecksumValid()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 265
    const/4 v4, 0x0

    .line 266
    .local v4, "sum":I
    const/4 v2, 0x0

    .line 267
    .local v2, "doubled":Z
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v3, v7, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_3

    .line 269
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 270
    .local v1, "digit":I
    if-eqz v2, :cond_1

    .line 271
    mul-int/lit8 v0, v1, 0x2

    .line 272
    .local v0, "addend":I
    const/16 v7, 0x9

    if-le v0, v7, :cond_0

    .line 273
    add-int/lit8 v0, v0, -0x9

    .line 278
    :cond_0
    :goto_1
    add-int/2addr v4, v0

    .line 279
    if-nez v2, :cond_2

    move v2, v5

    .line 267
    :goto_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 276
    .end local v0    # "addend":I
    :cond_1
    move v0, v1

    .restart local v0    # "addend":I
    goto :goto_1

    :cond_2
    move v2, v6

    .line 279
    goto :goto_2

    .line 281
    .end local v0    # "addend":I
    .end local v1    # "digit":I
    :cond_3
    rem-int/lit8 v7, v4, 0xa

    if-nez v7, :cond_4

    :goto_3
    return v5

    :cond_4
    move v5, v6

    goto :goto_3
.end method

.method private setOriginalTextColors()V
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mOriginalTextColors:Landroid/content/res/ColorStateList;

    .line 176
    return-void
.end method

.method private updateCardNumber(Ljava/lang/String;)V
    .locals 9
    .param p1, "newCardNumber"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 204
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->removeNonNumericDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 207
    .local v6, "normalizedCardNumber":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-eqz v7, :cond_1

    :cond_0
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 209
    iput-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    .line 210
    iput-object v8, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 212
    :cond_1
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mAllowedCardTypes:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    if-eqz v7, :cond_2

    .line 213
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mAllowedCardTypes:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .local v0, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v3, v0, v4

    .line 214
    .local v3, "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    iget-object v7, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->binRange:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-static {v7, v6}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->findFirstMatchingBin([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v1

    .line 215
    .local v1, "bin":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    if-eqz v1, :cond_5

    .line 216
    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    .line 221
    .end local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v1    # "bin":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v3    # "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_2
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    if-nez v7, :cond_3

    .line 222
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBins:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    invoke-static {v7, v6}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->findFirstMatchingBin([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 224
    :cond_3
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCompleteCardNumberLength()I

    move-result v2

    .line 228
    .local v2, "cardNumberLength":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v2, :cond_4

    .line 229
    const/4 v7, 0x0

    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 231
    :cond_4
    iput-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    .line 232
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->isComplete()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->isValid()Z

    move-result v7

    if-nez v7, :cond_6

    .line 233
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/wallet/instrumentmanager/R$color;->wallet_im_credit_card_invalid_text_color:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setTextColor(I)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x3

    invoke-static {v7, p0, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->playShakeAnimationIfPossible(Landroid/content/Context;Landroid/view/View;I)V

    .line 240
    :goto_1
    return-void

    .line 213
    .end local v2    # "cardNumberLength":I
    .restart local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .restart local v1    # "bin":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .restart local v3    # "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 238
    .end local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v1    # "bin":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    .end local v3    # "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .restart local v2    # "cardNumberLength":I
    :cond_6
    iget-object v7, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mOriginalTextColors:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v7}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_1
.end method


# virtual methods
.method public getCardNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getCardType()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardTypeAndBinRange:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConcealedCardNumber()Ljava/lang/String;
    .locals 6

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCompleteCardNumberLength()I

    move-result v2

    .line 108
    .local v2, "fullCardNumberLength":I
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v5, v2, -0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 110
    .local v0, "concealCount":I
    new-array v3, v0, [C

    .line 111
    .local v3, "stars":[C
    const/16 v4, 0x2022

    invoke-static {v3, v4}, Ljava/util/Arrays;->fill([CC)V

    .line 112
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 114
    .local v1, "concealed":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 115
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getFormattedCreditCardNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public isComplete()Z
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mCardNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->getCompleteCardNumberLength()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->onFinishInflate()V

    .line 132
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->setOriginalTextColors()V

    .line 133
    return-void
.end method

.method public setAllowedCardTypes([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 0
    .param p1, "cardTypes"    # [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mAllowedCardTypes:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 89
    return-void
.end method

.method public setInvalidBins([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;)V
    .locals 0
    .param p1, "invalidBins"    # [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBins:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    .line 93
    return-void
.end method

.method public setOnCreditCardTypeChangedListener(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    .line 85
    return-void
.end method
