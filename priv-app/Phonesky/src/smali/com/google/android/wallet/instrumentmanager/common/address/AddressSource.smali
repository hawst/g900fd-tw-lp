.class public interface abstract Lcom/google/android/wallet/instrumentmanager/common/address/AddressSource;
.super Ljava/lang/Object;
.source "AddressSource.java"


# virtual methods
.method public abstract getAddress(Ljava/lang/String;Ljava/lang/String;)Lcom/google/location/country/Postaladdress$PostalAddress;
.end method

.method public abstract getAddresses(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "C[CI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/address/AddressSourceResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/String;
.end method
