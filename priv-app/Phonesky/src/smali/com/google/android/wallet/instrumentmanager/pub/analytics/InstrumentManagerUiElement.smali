.class public final Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
.super Ljava/lang/Object;
.source "InstrumentManagerUiElement.java"


# instance fields
.field public children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;",
            ">;"
        }
    .end annotation
.end field

.field public final elementId:I

.field public final integratorLogToken:[B


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "elementId"    # I

    .prologue
    .line 86
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-direct {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I[B)V

    .line 87
    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 0
    .param p1, "elementId"    # I
    .param p2, "integratorLogToken"    # [B

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    .line 91
    iput-object p2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    .line 92
    return-void
.end method
