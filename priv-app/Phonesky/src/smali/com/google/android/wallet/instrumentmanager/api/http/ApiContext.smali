.class public Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;
.super Ljava/lang/Object;
.source "ApiContext.java"


# instance fields
.field public final applicationContext:Landroid/content/Context;

.field public final baseUrl:Landroid/net/Uri;

.field public final eesBaseUrl:Landroid/net/Uri;

.field private final mAuthTokenType:Ljava/lang/String;

.field private final mAuthenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

.field private mLastAuthToken:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;Lcom/android/volley/toolbox/AndroidAuthenticator;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "androidEnvironmentConfig"    # Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;
    .param p3, "authenticator"    # Lcom/android/volley/toolbox/AndroidAuthenticator;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->applicationContext:Landroid/content/Context;

    .line 49
    iget-object v0, p2, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverBasePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->baseUrl:Landroid/net/Uri;

    .line 50
    iget-object v0, p2, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->serverEesBasePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->eesBaseUrl:Landroid/net/Uri;

    .line 51
    iget-object v0, p2, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mAuthTokenType:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mAuthenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

    .line 53
    return-void
.end method

.method public static create(Landroid/content/Context;Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "androidEnvironmentConfig"    # Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    .prologue
    .line 40
    new-instance v0, Landroid/accounts/Account;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->accountName:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v0, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .local v0, "account":Landroid/accounts/Account;
    new-instance v1, Lcom/android/volley/toolbox/AndroidAuthenticator;

    iget-object v2, p1, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;->authTokenType:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v2}, Lcom/android/volley/toolbox/AndroidAuthenticator;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 43
    .local v1, "authenticator":Lcom/android/volley/toolbox/AndroidAuthenticator;
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;-><init>(Landroid/content/Context;Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;Lcom/android/volley/toolbox/AndroidAuthenticator;)V

    return-object v2
.end method


# virtual methods
.method public declared-synchronized getHeaders()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mAuthenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

    invoke-virtual {v1}, Lcom/android/volley/toolbox/AndroidAuthenticator;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 78
    .local v0, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mAuthTokenType:Ljava/lang/String;

    const-string v2, "oauth2:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :goto_0
    monitor-exit p0

    return-object v0

    .line 81
    :cond_0
    :try_start_1
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GoogleLogin auth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    .end local v0    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized invalidateAuthToken()V
    .locals 2

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mAuthenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/AndroidAuthenticator;->invalidateAuthToken(Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->mLastAuthToken:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :cond_0
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
