.class public Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;
.super Ljava/lang/Object;
.source "ParcelableProto.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
            "<",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mPayload:Lcom/google/protobuf/nano/MessageNano;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mSerialized:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;

    invoke-direct {v0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/nano/MessageNano;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    .local p1, "payload":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mSerialized:[B

    .line 29
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/protobuf/nano/MessageNano;
    .param p2, "x1"    # Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;)V

    return-void
.end method

.method public static forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(TT;)",
            "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "payload":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    invoke-direct {v0, p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;)V

    return-object v0
.end method

.method public static forProtoArrayList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "payloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    if-nez p0, :cond_1

    .line 49
    const/4 v2, 0x0

    .line 56
    :cond_0
    return-object v2

    .line 51
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 52
    .local v1, "length":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    .local v2, "parcelableProtos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 54
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getProtoArrayListFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p0, "b"    # Landroid/os/Bundle;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 96
    .local v3, "wrapperArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;>;"
    if-nez v3, :cond_1

    .line 97
    const/4 v2, 0x0

    .line 104
    :cond_0
    return-object v2

    .line 99
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 100
    .local v1, "length":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    .local v2, "protosArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 102
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    invoke-virtual {v4}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2
    .param p0, "b"    # Landroid/os/Bundle;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    .line 85
    .local v0, "wrapper":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getPayload()Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 118
    .local p0, "this":Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;, "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto<TT;>;"
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    if-nez v0, :cond_0

    .line 120
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mSerialized:[B

    if-nez v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mSerialized:[B

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mSerialized:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mSerialized:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 128
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
