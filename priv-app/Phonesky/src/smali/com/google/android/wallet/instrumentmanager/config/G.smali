.class public final Lcom/google/android/wallet/instrumentmanager/config/G;
.super Ljava/lang/Object;
.source "G.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/config/G$images;,
        Lcom/google/android/wallet/instrumentmanager/config/G$googleplaces;
    }
.end annotation


# static fields
.field public static final allowPiiLogging:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final androidId:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final minApiLevelToShowAutocompleteForAccessibility:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final pageImpressionDelayBeforeTrackingMs:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final volleyApiRequestDefaultTimeoutMs:Lcom/google/android/gsf/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 21
    const-string v0, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->androidId:Lcom/google/android/gsf/GservicesValue;

    .line 63
    const-string v0, "wallet.accessibility.min_api_level_to_show_autocomplete"

    const v1, 0x7fffffff

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->minApiLevelToShowAutocompleteForAccessibility:Lcom/google/android/gsf/GservicesValue;

    .line 73
    const-string v0, "wallet.allow_pii_logging"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Z)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->allowPiiLogging:Lcom/google/android/gsf/GservicesValue;

    .line 79
    const-string v0, "wallet.volley_api_request_default_timeout"

    const/16 v1, 0x2710

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->volleyApiRequestDefaultTimeoutMs:Lcom/google/android/gsf/GservicesValue;

    .line 91
    const-string v0, "wallet.page_impression_delay_before_tracking_ms"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gsf/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->pageImpressionDelayBeforeTrackingMs:Lcom/google/android/gsf/GservicesValue;

    return-void
.end method
