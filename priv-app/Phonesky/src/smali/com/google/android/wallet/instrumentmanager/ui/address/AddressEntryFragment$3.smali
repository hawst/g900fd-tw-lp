.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->fireCountryDataRequest(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 9
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 646
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->indicatePendingRequest(Z)V

    .line 651
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 652
    .local v0, "eventDetails":Landroid/os/Bundle;
    const-string v1, "FormEventListener.EXTRA_FORM_ID"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAddressForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$400(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;

    move-result-object v2

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/AddressFormOuterClass$AddressForm;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v6, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    const/4 v7, 0x5

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_network_error_title:I

    invoke-virtual {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    sget v4, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_network_error_message:I

    invoke-virtual {v3, v4}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$3;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    sget v8, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_retry:I

    invoke-virtual {v5, v8}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ErrorUtils;->addErrorDetailsToBundle(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v6, v7, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->notifyFormEvent(ILandroid/os/Bundle;)V

    .line 660
    return-void
.end method
