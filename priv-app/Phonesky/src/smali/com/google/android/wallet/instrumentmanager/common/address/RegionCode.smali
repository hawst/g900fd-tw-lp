.class public Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;
.super Ljava/lang/Object;
.source "RegionCode.java"


# direct methods
.method public static getUnknown()I
    .locals 1

    .prologue
    .line 711
    const/16 v0, 0x35a

    return v0
.end method

.method public static safeToRegionCode(Ljava/lang/String;)I
    .locals 2
    .param p0, "cldrCode"    # Ljava/lang/String;

    .prologue
    .line 693
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    const/4 v1, 0x0

    .line 699
    :goto_0
    return v1

    .line 697
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toRegionCode(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v1

    goto :goto_0
.end method

.method public static toCountryCode(I)Ljava/lang/String;
    .locals 7
    .param p0, "regionCode"    # I

    .prologue
    .line 677
    if-eqz p0, :cond_0

    and-int/lit16 v2, p0, -0x400

    if-eqz v2, :cond_1

    .line 678
    :cond_0
    const-string v2, "ZZ"

    .line 682
    :goto_0
    return-object v2

    .line 680
    :cond_1
    and-int/lit16 v2, p0, 0x3e0

    shr-int/lit8 v2, v2, 0x5

    add-int/lit8 v2, v2, 0x41

    add-int/lit8 v2, v2, -0x1

    int-to-char v0, v2

    .line 681
    .local v0, "firstChar":C
    and-int/lit8 v2, p0, 0x1f

    add-int/lit8 v2, v2, 0x41

    add-int/lit8 v2, v2, -0x1

    int-to-char v1, v2

    .line 682
    .local v1, "secondChar":C
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%c%c"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static toRegionCode(Ljava/lang/String;)I
    .locals 4
    .param p0, "cldrCode"    # Ljava/lang/String;

    .prologue
    .line 660
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 661
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "CountryCode must have length of 2!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 663
    :cond_0
    const-string v2, "UK"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 664
    const-string p0, "GB"

    .line 666
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    .line 667
    .local v0, "firstChar":C
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    .line 668
    .local v1, "secondChar":C
    add-int/lit8 v2, v0, -0x41

    add-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0x5

    add-int/lit8 v3, v1, -0x41

    add-int/lit8 v3, v3, 0x1

    or-int/2addr v2, v3

    return v2
.end method
