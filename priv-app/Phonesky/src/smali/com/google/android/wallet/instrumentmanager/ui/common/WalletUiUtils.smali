.class public final Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;
.super Ljava/lang/Object;
.source "WalletUiUtils.java"


# static fields
.field private static final sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method static synthetic access$000(Landroid/view/View;IIJ)V
    .locals 1
    .param p0, "x0"    # Landroid/view/View;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 43
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearingInternal(Landroid/view/View;IIJ)V

    return-void
.end method

.method public static animateViewAppearing(Landroid/view/View;II)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I

    .prologue
    .line 73
    const-wide/16 v0, -0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;IIJ)V

    .line 74
    return-void
.end method

.method public static animateViewAppearing(Landroid/view/View;IIJ)V
    .locals 11
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I
    .param p3, "animationDurationMs"    # J

    .prologue
    const/4 v4, 0x0

    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 92
    if-ge p1, p2, :cond_0

    .line 102
    invoke-virtual {p0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 103
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 107
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v2

    .line 113
    .local v2, "previousDuration":J
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;

    move-object v1, p0

    move v4, p1

    move v5, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$1;-><init>(Landroid/view/View;JIIJ)V

    invoke-virtual {v9, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 133
    .end local v2    # "previousDuration":J
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearingInternal(Landroid/view/View;IIJ)V

    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {p0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 129
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$anim;->wallet_im_fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    .line 131
    .local v8, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private static animateViewAppearingInternal(Landroid/view/View;IIJ)V
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I
    .param p3, "animationDurationMs"    # J

    .prologue
    .line 143
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 145
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    int-to-float v1, p1

    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 147
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 150
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v4, p2

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 151
    .local v0, "animation":Landroid/view/ViewPropertyAnimator;
    const-wide/16 v4, 0x0

    cmp-long v1, p3, v4

    if-ltz v1, :cond_0

    .line 152
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v2

    .line 153
    .local v2, "previousDuration":J
    invoke-virtual {v0, p3, p4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;

    invoke-direct {v4, p0, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;-><init>(Landroid/view/View;J)V

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 162
    .end local v2    # "previousDuration":J
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 163
    return-void
.end method

.method private static animateViewDisappearingInternal(Landroid/view/View;III)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I
    .param p3, "finalViewVisibility"    # I

    .prologue
    .line 189
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 190
    int-to-float v1, p1

    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 191
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v2, p2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;

    invoke-direct {v2, p0, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$3;-><init>(Landroid/view/View;I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 209
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$anim;->wallet_im_fade_out:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 206
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 207
    invoke-virtual {p0, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static animateViewDisappearingToGone(Landroid/view/View;II)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I

    .prologue
    .line 172
    const/16 v0, 0x8

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingInternal(Landroid/view/View;III)V

    .line 173
    return-void
.end method

.method public static animateViewDisappearingToInvisible(Landroid/view/View;II)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "startDeltaY"    # I
    .param p2, "endDeltaY"    # I

    .prologue
    .line 183
    const/4 v0, 0x4

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingInternal(Landroid/view/View;III)V

    .line 184
    return-void
.end method

.method public static announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 371
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/AndroidUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 388
    :goto_0
    return-void

    .line 374
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    .line 375
    invoke-virtual {p0, p1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 379
    :cond_1
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 381
    .local v1, "event":Landroid/view/accessibility/AccessibilityEvent;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 382
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 386
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public static applyUiFieldSpecificationToFormEditText(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
    .locals 12
    .param p0, "uiField"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .param p1, "editText"    # Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 419
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->label:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 420
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget-object v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->initialValue:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 421
    iget-boolean v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->isOptional:Z

    if-nez v8, :cond_1

    move v8, v9

    :goto_0
    invoke-virtual {p1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setRequired(Z)V

    .line 422
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->maxLength:I

    if-lez v8, :cond_0

    .line 423
    new-array v8, v9, [Landroid/text/InputFilter;

    new-instance v9, Landroid/text/InputFilter$LengthFilter;

    iget-object v11, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget v11, v11, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->maxLength:I

    invoke-direct {v9, v11}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v9, v8, v10

    invoke-virtual {p1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 429
    new-instance v3, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;

    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->maxLength:I

    invoke-direct {v3, p1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/InputLengthCompletable;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;I)V

    .line 431
    .local v3, "inputLengthCompletable":Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;
    invoke-virtual {p1, v3, p1, v10}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->enableAutoAdvance(Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;Z)V

    .line 434
    .end local v3    # "inputLengthCompletable":Lcom/google/android/wallet/instrumentmanager/ui/common/Completable;
    :cond_0
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->keyboardLayout:I

    packed-switch v8, :pswitch_data_0

    .line 453
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TextField.keyboardLayout "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget v10, v10, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->keyboardLayout:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is not supported"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    move v8, v10

    .line 421
    goto :goto_0

    .line 436
    :pswitch_0
    const/4 v4, 0x1

    .line 437
    .local v4, "inputType":I
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget-boolean v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->isMasked:Z

    if-eqz v8, :cond_2

    .line 438
    or-int/lit16 v4, v4, 0x80

    .line 456
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    .line 457
    .local v6, "originalTypeface":Landroid/graphics/Typeface;
    invoke-virtual {p1, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setInputType(I)V

    .line 460
    invoke-virtual {p1, v6}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 461
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget-object v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->validation:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    array-length v8, v8

    if-lez v8, :cond_4

    .line 462
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;

    new-array v8, v10, [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    invoke-direct {v1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;-><init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 463
    .local v1, "compoundRegexValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget-object v0, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->validation:[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;

    .local v0, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v5, :cond_3

    aget-object v7, v0, v2

    .line 464
    .local v7, "validation":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    new-instance v8, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;

    iget-object v9, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->errorMessage:Ljava/lang/String;

    iget-object v10, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;->regex:Ljava/lang/String;

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/PatternValidator;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v1, v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;->add(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 463
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 442
    .end local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .end local v1    # "compoundRegexValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;
    .end local v2    # "i$":I
    .end local v4    # "inputType":I
    .end local v5    # "len$":I
    .end local v6    # "originalTypeface":Landroid/graphics/Typeface;
    .end local v7    # "validation":Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    :pswitch_1
    const/4 v4, 0x2

    .line 443
    .restart local v4    # "inputType":I
    iget-object v8, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    iget-boolean v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;->isMasked:Z

    if-eqz v8, :cond_2

    .line 446
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Number passwords are not supported"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 450
    .end local v4    # "inputType":I
    :pswitch_2
    const/16 v4, 0x21

    .line 451
    .restart local v4    # "inputType":I
    goto :goto_1

    .line 467
    .restart local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .restart local v1    # "compoundRegexValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;
    .restart local v2    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "originalTypeface":Landroid/graphics/Typeface;
    :cond_3
    invoke-virtual {p1, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->addValidator(Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 469
    .end local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField$Validation;
    .end local v1    # "compoundRegexValidator":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    :cond_4
    return-void

    .line 434
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static createFormEditTextForTextUiField(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;ILandroid/view/LayoutInflater;)Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    .locals 4
    .param p0, "uiField"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;
    .param p1, "viewId"    # I
    .param p2, "themedInflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 401
    iget-object v1, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;->textField:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField$TextField;

    if-nez v1, :cond_0

    .line 402
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "UiField.textField is not defined"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 404
    :cond_0
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_form_edit_text:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .line 406
    .local v0, "editText":Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setId(I)V

    .line 407
    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->applyUiFieldSpecificationToFormEditText(Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/UiFieldOuterClass$UiField;Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V

    .line 408
    return-object v0
.end method

.method public static generateViewId()I
    .locals 4

    .prologue
    .line 477
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 478
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v1

    .line 488
    .local v0, "newValue":I
    .local v1, "result":I
    :goto_0
    return v1

    .line 481
    :cond_0
    sget-object v2, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 483
    add-int/lit8 v0, v1, 0x1

    .line 484
    const v2, 0xffffff

    if-le v0, v2, :cond_1

    .line 485
    const/4 v0, 0x1

    .line 487
    :cond_1
    sget-object v2, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method public static getViewHeightWithMargins(Landroid/view/View;)I
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 238
    const/4 v2, 0x0

    .line 239
    .local v2, "verticalMarginHeight":I
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 240
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    instance-of v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 241
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 242
    .local v0, "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v2, v3, v4

    .line 244
    .end local v0    # "marginParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v2

    return v3
.end method

.method public static hideSoftKeyboard(Landroid/content/Context;Landroid/view/View;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 354
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 356
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 358
    const/4 v1, 0x1

    .line 360
    :cond_0
    return v1
.end method

.method public static playShakeAnimationIfPossible(Landroid/content/Context;Landroid/view/View;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;
    .param p2, "shakeCount"    # I

    .prologue
    .line 217
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 232
    :goto_0
    return-void

    .line 220
    :cond_0
    const-string v2, "translationX"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 221
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_shake_animation_delta:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 223
    .local v1, "shakeDelta":F
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;

    invoke-direct {v2, p2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;-><init>(IF)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 231
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 220
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static requestFocusAndAnnounceError(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 303
    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 304
    instance-of v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    if-eqz v0, :cond_1

    .line 305
    check-cast p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    .end local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->announceError()V

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 306
    .restart local p0    # "view":Landroid/view/View;
    :cond_1
    instance-of v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    if-eqz v0, :cond_0

    .line 307
    check-cast p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    .end local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->announceError()V

    goto :goto_0

    .line 312
    .restart local p0    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    if-eqz v0, :cond_0

    .line 315
    check-cast p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;

    .end local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->announceError()V

    goto :goto_0
.end method

.method public static setViewBackgroundOrHide(Landroid/view/View;Landroid/util/TypedValue;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "typedValue"    # Landroid/util/TypedValue;

    .prologue
    .line 54
    if-eqz p1, :cond_0

    iget v0, p1, Landroid/util/TypedValue;->type:I

    if-nez v0, :cond_1

    .line 55
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 62
    :goto_0
    return-void

    .line 56
    :cond_1
    iget v0, p1, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_2

    iget v0, p1, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_2

    .line 58
    iget v0, p1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 60
    :cond_2
    iget v0, p1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private static showKeyboard(Landroid/widget/EditText;Z)Z
    .locals 6
    .param p0, "focusView"    # Landroid/widget/EditText;
    .param p1, "hasNextView"    # Z

    .prologue
    const/4 v1, 0x1

    .line 260
    invoke-virtual {p0}, Landroid/widget/EditText;->requestFocus()Z

    move-result v2

    if-nez v2, :cond_1

    .line 261
    const/4 v1, 0x0

    .line 286
    :cond_0
    :goto_0
    return v1

    .line 268
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/widget/EditText;->getImeOptions()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-nez v2, :cond_2

    .line 270
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 272
    :cond_2
    invoke-virtual {p0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 274
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 278
    new-instance v2, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$5;

    invoke-direct {v2, v0, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$5;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/widget/EditText;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {p0, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static showSoftKeyboardOnFirstEditText(Landroid/view/View;)Z
    .locals 7
    .param p0, "rootView"    # Landroid/view/View;

    .prologue
    .line 331
    const/16 v6, 0x82

    invoke-virtual {p0, v6}, Landroid/view/View;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 332
    .local v1, "focusables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .line 333
    .local v0, "focusView":Landroid/widget/EditText;
    const/4 v2, 0x0

    .line 334
    .local v2, "hasNextView":Z
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .local v4, "length":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 335
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 336
    .local v5, "view":Landroid/view/View;
    instance-of v6, v5, Landroid/widget/EditText;

    if-eqz v6, :cond_0

    .line 337
    if-nez v0, :cond_1

    move-object v0, v5

    .line 338
    check-cast v0, Landroid/widget/EditText;

    .line 334
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 340
    :cond_1
    const/4 v2, 0x1

    .line 345
    .end local v5    # "view":Landroid/view/View;
    :cond_2
    if-eqz v0, :cond_3

    invoke-static {v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->showKeyboard(Landroid/widget/EditText;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    return v6

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method
