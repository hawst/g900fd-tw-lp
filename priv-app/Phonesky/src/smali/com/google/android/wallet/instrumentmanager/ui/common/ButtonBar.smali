.class public Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;
.super Landroid/widget/RelativeLayout;
.source "ButtonBar.java"


# instance fields
.field private mCapitalizeButtonText:Z

.field public mExpandButton:Landroid/widget/Button;

.field mLogoImage:Landroid/widget/ImageView;

.field private mLogoImageDefined:Z

.field public mPositiveButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method private readAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImButtonBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$styleable;->WalletImButtonBar_capitalizeButtonText:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mCapitalizeButtonText:Z

    .line 66
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 71
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 73
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->logo:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImage:Landroid/widget/ImageView;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->getContext()Landroid/content/Context;

    move-result-object v3

    new-array v6, v4, [I

    sget v7, Lcom/google/android/wallet/instrumentmanager/R$attr;->imButtonBarIntegratorLogoDrawable:I

    aput v7, v6, v5

    invoke-virtual {v3, v6}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 76
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v2

    .line 77
    .local v2, "typedValue":Landroid/util/TypedValue;
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImage:Landroid/widget/ImageView;

    invoke-static {v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->setViewBackgroundOrHide(Landroid/view/View;Landroid/util/TypedValue;)V

    .line 80
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImageDefined:Z

    .line 82
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->expand_btn:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    .line 84
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->positive_btn:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    .line 85
    iget-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mCapitalizeButtonText:Z

    if-eqz v3, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v0, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 87
    .local v0, "locale":Ljava/util/Locale;
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 89
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_0
    return-void

    :cond_1
    move v3, v5

    .line 80
    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 102
    instance-of v1, p1, Landroid/os/Bundle;

    if-nez v1, :cond_0

    .line 103
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 111
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 106
    check-cast v0, Landroid/os/Bundle;

    .line 107
    .local v0, "savedInstanceState":Landroid/os/Bundle;
    const-string v1, "superSavedInstanceState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    const-string v2, "positiveButtonEnabled"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 110
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    const-string v2, "expandButtonEnabled"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v0, "outState":Landroid/os/Bundle;
    const-string v1, "superSavedInstanceState"

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    const-string v1, "positiveButtonEnabled"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 96
    const-string v1, "expandButtonEnabled"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    return-object v0
.end method

.method public setExpandButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 121
    return-void
.end method

.method public setExpandButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    return-void
.end method

.method public setExpandButtonText(Ljava/lang/String;)V
    .locals 1
    .param p1, "buttonText"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method public setPositiveButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method public setPositiveButtonText(Ljava/lang/String;)V
    .locals 1
    .param p1, "buttonText"    # Ljava/lang/String;

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mCapitalizeButtonText:Z

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 128
    return-void
.end method

.method public showExpandButton(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 143
    if-eqz p1, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 145
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImageDefined:Z

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mExpandButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 150
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImageDefined:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->mLogoImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
