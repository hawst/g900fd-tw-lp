.class public Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;
.super Landroid/widget/RelativeLayout;
.source "CreditCardImagesView.java"


# instance fields
.field mCardImages:[Landroid/widget/ImageView;

.field mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

.field mCurrentCardType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

.field mGeneralCardImage:Landroid/widget/ImageView;

.field mOneCardMode:Z

.field private mSuspendAnimations:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    .line 65
    return-void
.end method


# virtual methods
.method createImagesAnimator([Landroid/widget/ImageView;)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;
    .locals 2
    .param p1, "imageViews"    # [Landroid/widget/ImageView;

    .prologue
    .line 180
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 181
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mGeneralCardImage:Landroid/widget/ImageView;

    invoke-direct {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;-><init>([Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 183
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorGingerbread;-><init>(Landroid/content/Context;[Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 70
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->general_logo:I

    invoke-virtual {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mGeneralCardImage:Landroid/widget/ImageView;

    .line 71
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 158
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 159
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    .line 161
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCurrentCardType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;->restoreCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 162
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mOneCardMode:Z

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->switchToOneCardMode()V

    .line 166
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 143
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 144
    check-cast v0, Landroid/os/Bundle;

    .line 148
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "cardType"

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCurrentCardType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 149
    const-string v1, "oneCardMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mOneCardMode:Z

    .line 150
    const-string v1, "parentState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 154
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 135
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 136
    const-string v1, "cardType"

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCurrentCardType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 137
    const-string v1, "oneCardMode"

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mOneCardMode:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    return-object v0
.end method

.method public setCardTypes([Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 16
    .param p1, "cardTypes"    # [Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 75
    new-instance v9, Ljava/util/ArrayList;

    move-object/from16 v0, p1

    array-length v13, v0

    invoke-direct {v9, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 76
    .local v9, "images":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/ImageView;>;"
    const/4 v12, -0x1

    .line 77
    .local v12, "previousCardId":I
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->generateViewId()I

    move-result v6

    .line 78
    .local v6, "currentCardId":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_credit_card_icon_width:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 80
    .local v3, "cardIconWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_credit_card_icon_height:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 82
    .local v2, "cardIconHeight":I
    move-object/from16 v1, p1

    .local v1, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v10, :cond_5

    aget-object v5, v1, v8

    .line 84
    .local v5, "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    iget-object v13, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v13, v13, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->isEmbeddedImageUri(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 85
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v4, v13}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 86
    .local v4, "cardImage":Landroid/widget/ImageView;
    iget-object v13, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v13, v13, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->embeddedImageUriToDrawableResourceId(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    :goto_1
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0xb

    if-lt v13, v14, :cond_0

    .line 95
    const/4 v13, 0x2

    const/4 v14, 0x0

    invoke-virtual {v4, v13, v14}, Landroid/widget/ImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 97
    :cond_0
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setId(I)V

    .line 98
    new-instance v11, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v11, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 100
    .local v11, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v13, -0x1

    if-ne v12, v13, :cond_4

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_credit_card_logos_left_margin:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    iput v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 108
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    instance-of v13, v4, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    if-eqz v13, :cond_1

    move-object v7, v4

    .line 110
    check-cast v7, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    .line 111
    .local v7, "fifeCardImage":Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setFadeIn(Z)V

    .line 113
    iget-object v13, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v14, v13, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->getImageLoader(Landroid/content/Context;)Lcom/android/volley/toolbox/ImageLoader;

    move-result-object v15

    sget-object v13, Lcom/google/android/wallet/instrumentmanager/config/G$images;->useWebPForFife:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v13}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    invoke-virtual {v7, v14, v15, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setFifeImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;Z)V

    .line 117
    sget v13, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_general:I

    invoke-virtual {v7, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;->setErrorImageResId(I)V

    .line 119
    .end local v7    # "fifeCardImage":Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;
    :cond_1
    iget-object v13, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v13, v13, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->altText:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_2

    .line 120
    iget-object v13, v5, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->icon:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v13, v13, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->altText:Ljava/lang/String;

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 122
    :cond_2
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 123
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    move v12, v6

    .line 126
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->generateViewId()I

    move-result v6

    .line 82
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 91
    .end local v4    # "cardImage":Landroid/widget/ImageView;
    .end local v11    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    new-instance v4, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-direct {v4, v13}, Lcom/google/android/wallet/instrumentmanager/ui/common/FifeNetworkImageView;-><init>(Landroid/content/Context;)V

    .restart local v4    # "cardImage":Landroid/widget/ImageView;
    goto :goto_1

    .line 104
    .restart local v11    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/google/android/wallet/instrumentmanager/R$dimen;->wallet_im_spacing_between_credit_card_images:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    iput v13, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 106
    const/4 v13, 0x1

    invoke-virtual {v11, v13, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2

    .line 128
    .end local v4    # "cardImage":Landroid/widget/ImageView;
    .end local v5    # "cardType":Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .end local v11    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Landroid/widget/ImageView;

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImages:[Landroid/widget/ImageView;

    .line 129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImages:[Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->createImagesAnimator([Landroid/widget/ImageView;)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    .line 130
    return-void
.end method

.method public setCreditCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
    .locals 1
    .param p1, "newType"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCurrentCardType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .line 188
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    invoke-virtual {v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;->animateToType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V

    .line 191
    :cond_0
    return-void
.end method

.method public switchToOneCardMode()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    instance-of v0, v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mOneCardMode:Z

    .line 171
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mSuspendAnimations:Z

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesView;->mCardImagesAnimator:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;

    check-cast v0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimatorIcs;->switchToOneCardMode()V

    .line 175
    :cond_0
    return-void
.end method
