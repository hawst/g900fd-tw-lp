.class Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;
.super Ljava/lang/Object;
.source "FormEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 487
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextWatcher;

    .line 488
    .local v1, "watcher":Landroid/text/TextWatcher;
    invoke-interface {v1, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    goto :goto_0

    .line 490
    .end local v1    # "watcher":Landroid/text/TextWatcher;
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 494
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextWatcher;

    .line 495
    .local v1, "watcher":Landroid/text/TextWatcher;
    invoke-interface {v1, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 497
    .end local v1    # "watcher":Landroid/text/TextWatcher;
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 501
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getTextWatcherList()Ljava/util/LinkedList;
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$200(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/TextWatcher;

    .line 502
    .local v1, "watcher":Landroid/text/TextWatcher;
    invoke-interface {v1, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 505
    .end local v1    # "watcher":Landroid/text/TextWatcher;
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 506
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$2;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->announceError()V

    .line 508
    :cond_1
    return-void
.end method
