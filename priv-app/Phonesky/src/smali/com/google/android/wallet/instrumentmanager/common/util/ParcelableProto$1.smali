.class final Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;
.super Ljava/lang/Object;
.source "ParcelableProto.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
        "<",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;
    .locals 9
    .param p1, "source"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
            "<",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 136
    .local v5, "size":I
    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 138
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    invoke-direct {v6, v7, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;)V

    .line 151
    :goto_0
    return-object v6

    .line 141
    :cond_0
    new-array v3, v5, [B

    .line 142
    .local v3, "payload":[B
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readByteArray([B)V

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "className":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 147
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    invoke-virtual {v1, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v7

    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v7, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/nano/MessageNano;

    .line 150
    .local v4, "proto":Lcom/google/protobuf/nano/MessageNano;
    invoke-static {v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/CodedInputByteBufferNano;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;

    .line 151
    new-instance v6, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    const/4 v7, 0x0

    invoke-direct {v6, v4, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "proto":Lcom/google/protobuf/nano/MessageNano;
    :catch_0
    move-exception v2

    .line 154
    .local v2, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception when unmarshalling: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;
    .locals 1
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto",
            "<",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-array v0, p1, [Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto$1;->newArray(I)[Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v0

    return-object v0
.end method
