.class public interface abstract Lcom/google/android/wallet/instrumentmanager/ui/common/Form;
.super Ljava/lang/Object;
.source "Form.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# virtual methods
.method public abstract applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z
.end method

.method public abstract enableUi(Z)V
.end method

.method public abstract focusOnFirstInvalidFormField()Z
.end method

.method public abstract handleErrorMessageDismissed(Ljava/lang/String;I)Z
.end method

.method public abstract isReadyToSubmit()Z
.end method
