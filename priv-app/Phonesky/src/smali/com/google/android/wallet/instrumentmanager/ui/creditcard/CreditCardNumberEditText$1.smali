.class Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.source "CreditCardNumberEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->initializeViewAndListeners(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v0

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public isValid(Landroid/widget/TextView;)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->mInvalidBin:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardNumberEditText;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$BinRange;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
