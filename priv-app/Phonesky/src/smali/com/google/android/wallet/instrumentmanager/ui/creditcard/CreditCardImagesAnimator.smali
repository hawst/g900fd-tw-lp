.class public abstract Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;
.super Ljava/lang/Object;
.source "CreditCardImagesAnimator.java"


# instance fields
.field protected mCurrentType:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

.field protected final mImages:[Landroid/widget/ImageView;


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "images"    # [Landroid/widget/ImageView;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;->mImages:[Landroid/widget/ImageView;

    .line 21
    return-void
.end method


# virtual methods
.method public abstract animateToType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
.end method

.method protected findIndex(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)I
    .locals 4
    .param p1, "type"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    const/4 v3, -0x1

    .line 40
    if-nez p1, :cond_1

    move v0, v3

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;->mImages:[Landroid/widget/ImageView;

    array-length v1, v2

    .local v1, "length":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 44
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardImagesAnimator;->mImages:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->equals(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v3

    .line 48
    goto :goto_0
.end method

.method public abstract restoreCardType(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)V
.end method
