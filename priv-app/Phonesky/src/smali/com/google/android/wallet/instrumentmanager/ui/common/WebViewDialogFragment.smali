.class public Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;
.source "WebViewDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$WebViewWithKeyboardSupport;
    }
.end annotation


# instance fields
.field mErrorText:Landroid/widget/TextView;

.field private mPageLoaded:Z

.field mProgressBar:Landroid/widget/ProgressBar;

.field mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;-><init>()V

    .line 180
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    return p1
.end method

.method public static newInstance(Ljava/lang/String;I)Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "themeResourceId"    # I

    .prologue
    .line 53
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 54
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "url"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;-><init>()V

    .line 56
    .local v1, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, -0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 72
    if-eqz p1, :cond_0

    .line 73
    const-string v3, "pageLoaded"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->getThemedLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Lcom/google/android/wallet/instrumentmanager/R$layout;->view_web_view:I

    invoke-virtual {v3, v4, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 77
    .local v1, "rootView":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "initialUrl":Ljava/lang/String;
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->progress_bar:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 79
    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->error_msg:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mErrorText:Landroid/widget/TextView;

    .line 81
    new-instance v3, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$WebViewWithKeyboardSupport;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->getThemedContext()Landroid/view/ContextThemeWrapper;

    move-result-object v4

    invoke-direct {v3, v4, v7}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$WebViewWithKeyboardSupport;-><init>(Landroid/content/Context;Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;)V

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    .line 82
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 85
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, p0}, Landroid/webkit/WebView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 86
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xa

    if-gt v3, v4, :cond_1

    .line 87
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, p0}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 89
    :cond_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    .line 90
    .local v2, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 91
    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 92
    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 93
    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 94
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_2

    .line 98
    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 100
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object v3

    if-nez v3, :cond_4

    .line 101
    :cond_3
    iput-boolean v5, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    .line 102
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 104
    :cond_4
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;

    invoke-direct {v4, p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment$1;-><init>(Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 137
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 138
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->requestFocus()Z

    .line 140
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    sget v4, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_close:I

    invoke-virtual {v3, v4, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 149
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 151
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 63
    const-string v0, "pageLoaded"

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 64
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mPageLoaded:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 67
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 160
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 165
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
