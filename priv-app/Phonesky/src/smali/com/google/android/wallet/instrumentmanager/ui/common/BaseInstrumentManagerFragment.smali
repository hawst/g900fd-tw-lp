.class public abstract Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseInstrumentManagerFragment.java"


# instance fields
.field private mThemeResourceId:I

.field private mThemedContext:Landroid/view/ContextThemeWrapper;

.field private mThemedInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method protected static createArgs(I)Landroid/os/Bundle;
    .locals 2
    .param p0, "themeResourceId"    # I

    .prologue
    .line 25
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 26
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "themeResourceId"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 27
    return-object v0
.end method


# virtual methods
.method protected getThemeResourceId()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemeResourceId:I

    return v0
.end method

.method protected getThemedContext()Landroid/view/ContextThemeWrapper;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedContext:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method protected getThemedLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "themeResourceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemeResourceId:I

    .line 35
    iget v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemeResourceId:I

    if-gtz v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid theme resource id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemeResourceId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemeResourceId:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedContext:Landroid/view/ContextThemeWrapper;

    .line 39
    return-void
.end method

.method protected abstract onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedContext:Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedInflater:Landroid/view/LayoutInflater;

    .line 45
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->mThemedInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
