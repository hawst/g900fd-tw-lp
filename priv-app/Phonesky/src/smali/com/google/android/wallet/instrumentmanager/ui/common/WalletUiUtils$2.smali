.class final Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "WalletUiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearingInternal(Landroid/view/View;IIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$previousDuration:J

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;->val$view:Landroid/view/View;

    iput-wide p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;->val$previousDuration:J

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$2;->val$previousDuration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 159
    return-void
.end method
