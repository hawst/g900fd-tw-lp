.class public Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
.source "AndValidator.java"


# direct methods
.method public varargs constructor <init>([Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V
    .locals 1
    .param p1, "validators"    # [Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;-><init>(Ljava/lang/CharSequence;[Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;)V

    .line 14
    return-void
.end method


# virtual methods
.method public isValid(Landroid/widget/TextView;)Z
    .locals 3
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 18
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;->mValidators:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;

    .line 19
    .local v1, "v":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    invoke-virtual {v1, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;->isValid(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 20
    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AndValidator;->mErrorMessage:Ljava/lang/CharSequence;

    .line 21
    const/4 v2, 0x0

    .line 24
    .end local v1    # "v":Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
