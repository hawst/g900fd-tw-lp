.class public final Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;
.super Ljava/lang/Object;
.source "AnalyticsUtil.java"


# direct methods
.method private static addChildrenToTreeWithInjection(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/util/List;II)V
    .locals 7
    .param p0, "parent"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .param p2, "injectedElementParentId"    # I
    .param p3, "injectedElementId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;>;"
    const/4 v6, -0x1

    .line 118
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    if-eq p3, v6, :cond_1

    iget v5, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    if-eq v5, p2, :cond_2

    .line 145
    :cond_1
    :goto_0
    return-void

    .line 124
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v0, "childUiElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;>;"
    if-eq p3, v6, :cond_3

    iget v5, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    if-ne v5, p2, :cond_3

    .line 127
    new-instance v5, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {v5, p3}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_3
    if-eqz p1, :cond_4

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "length":I
    :goto_1
    if-ge v1, v2, :cond_4

    .line 132
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .line 135
    .local v4, "node":Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->cloneElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v3

    .line 136
    .local v3, "newElement":Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v4}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getChildren()Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v5, p2, p3}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->addChildrenToTreeWithInjection(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/util/List;II)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "newElement":Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .end local v4    # "node":Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    :cond_4
    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->children:Ljava/util/List;

    goto :goto_0
.end method

.method private static cloneElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 3
    .param p0, "original"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->integratorLogToken:[B

    invoke-direct {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I[B)V

    return-object v0
.end method

.method public static createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V
    .locals 4
    .param p0, "parentOfClickedElement"    # Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .param p1, "clickedElementId"    # I

    .prologue
    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v1, "clickPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;>;"
    new-instance v3, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    invoke-direct {v3, p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    move-object v2, p0

    .line 53
    .local v2, "currentElement":Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    :goto_0
    if-eqz v2, :cond_0

    .line 54
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    move-result-object v2

    goto :goto_0

    .line 57
    :cond_0
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;

    invoke-direct {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;-><init>(Ljava/util/List;)V

    .line 58
    .local v0, "clickEvent":Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sendClickEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerClickEvent;)V

    .line 59
    return-void
.end method

.method public static createAndSendCreditCardEntryActionBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;[B)V
    .locals 1
    .param p0, "creditCardEntryAction"    # Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    .param p1, "integratorLogToken"    # [B

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    invoke-direct {v0, p0, p1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;-><init>(Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;[B)V

    .line 45
    .local v0, "creditCardEntryActionEvent":Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sendBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V

    .line 46
    return-void
.end method

.method public static createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V
    .locals 1
    .param p0, "nodeShown"    # Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    .prologue
    .line 71
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 72
    return-void
.end method

.method public static createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V
    .locals 5
    .param p0, "nodeShown"    # Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .param p1, "transientElementId"    # I

    .prologue
    .line 88
    move-object v2, p0

    .line 89
    .local v2, "rootUiNode":Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    :goto_0
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 90
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;

    move-result-object v2

    goto :goto_0

    .line 94
    :cond_0
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->cloneElement(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v0

    .line 96
    .local v0, "impressionTree":Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    invoke-interface {v2}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getChildren()Ljava/util/List;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;->getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    move-result-object v4

    iget v4, v4, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;->elementId:I

    invoke-static {v0, v3, v4, p1}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->addChildrenToTreeWithInjection(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;Ljava/util/List;II)V

    .line 99
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;

    invoke-direct {v1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;-><init>(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;)V

    .line 101
    .local v1, "impresssionEvent":Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerImpressionEvent;)V

    .line 102
    return-void
.end method

.method public static createAndSendRequestSentBackgroundEvent(I[B)V
    .locals 9
    .param p0, "backgroundEventType"    # I
    .param p1, "integratorLogToken"    # [B

    .prologue
    const-wide/16 v4, -0x1

    .line 25
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v1, p0

    move-wide v6, v4

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;-><init>(IILjava/lang/String;JJ[B)V

    .line 29
    .local v0, "requestEvent":Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sendBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V

    .line 30
    return-void
.end method

.method public static createAndSendResponseReceivedBackgroundEvent(IILjava/lang/String;JJ[B)V
    .locals 9
    .param p0, "backgroundEventType"    # I
    .param p1, "resultCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;
    .param p3, "clientLatencyMs"    # J
    .param p5, "serverLatencyMs"    # J
    .param p7, "integratorLogToken"    # [B

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;

    move v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;-><init>(IILjava/lang/String;JJ[B)V

    .line 38
    .local v0, "responseEvent":Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->sendBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/events/InstrumentManagerBackgroundEvent;)V

    .line 39
    return-void
.end method
