.class public Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;
.super Landroid/widget/Spinner;
.source "FormSpinner.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/Validatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;
    }
.end annotation


# static fields
.field public static final DEFAULT_INVALIDATOR:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;


# instance fields
.field private mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

.field private mLabel:Ljava/lang/String;

.field private mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mPotentialErrorOnConfigChange:Z

.field private mRequired:Z

.field private mViewLaidOutOnceAfterAdapterSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$1;

    invoke-direct {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$1;-><init>()V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->DEFAULT_INVALIDATOR:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mRequired:Z

    .line 74
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->DEFAULT_INVALIDATOR:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mViewLaidOutOnceAfterAdapterSet:Z

    .line 80
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->initializeListener()V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mRequired:Z

    .line 74
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->DEFAULT_INVALIDATOR:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mViewLaidOutOnceAfterAdapterSet:Z

    .line 85
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->initializeListener()V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mRequired:Z

    .line 74
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->DEFAULT_INVALIDATOR:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mViewLaidOutOnceAfterAdapterSet:Z

    .line 90
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->initializeListener()V

    .line 91
    return-void
.end method

.method private initializeListener()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 95
    return-void
.end method


# virtual methods
.method public announceError()V
    .locals 1

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getErrorTextForAccessibility()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 248
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 252
    invoke-super {p0, p1}, Landroid/widget/Spinner;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 256
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;->getError(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getErrorTextForAccessibility()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method getErrorTextForAccessibility()Ljava/lang/String;
    .locals 6

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_accessibility_event_form_field_error:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mLabel:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;->getError(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isRequired()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mRequired:Z

    return v0
.end method

.method public isValid()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->isRequired()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move v2, v3

    .line 162
    :cond_1
    :goto_0
    return v2

    .line 149
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    .line 150
    .local v0, "adapter":Landroid/widget/Adapter;
    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getSelectedItemPosition()I

    move-result v1

    .line 155
    .local v1, "selectedItemPosition":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    .line 159
    instance-of v2, v0, Landroid/widget/ListAdapter;

    if-eqz v2, :cond_3

    .line 160
    check-cast v0, Landroid/widget/ListAdapter;

    .end local v0    # "adapter":Landroid/widget/Adapter;
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v2

    goto :goto_0

    .restart local v0    # "adapter":Landroid/widget/Adapter;
    :cond_3
    move v2, v3

    .line 162
    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2
    .param p1, "focused"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 229
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;->getError(Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->announceError()V

    .line 232
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/Spinner;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 233
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->validate()Z

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 213
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 187
    invoke-super/range {p0 .. p5}, Landroid/widget/Spinner;->onLayout(ZIIII)V

    .line 192
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mViewLaidOutOnceAfterAdapterSet:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mViewLaidOutOnceAfterAdapterSet:Z

    .line 194
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mPotentialErrorOnConfigChange:Z

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->validate()Z

    .line 198
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->validate()Z

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p1}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 225
    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 176
    instance-of v1, p1, Landroid/os/Bundle;

    if-nez v1, :cond_0

    .line 177
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 183
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 180
    check-cast v0, Landroid/os/Bundle;

    .line 181
    .local v0, "inState":Landroid/os/Bundle;
    const-string v1, "superSavedInstanceState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 182
    const-string v1, "potentialErrorOnConfigChange"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mPotentialErrorOnConfigChange:Z

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 168
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 169
    .local v0, "outState":Landroid/os/Bundle;
    const-string v1, "superSavedInstanceState"

    invoke-super {p0}, Landroid/widget/Spinner;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 170
    const-string v1, "potentialErrorOnConfigChange"

    iget-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mPotentialErrorOnConfigChange:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 171
    return-object v0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mLabel:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "onItemSelectedListener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 100
    return-void
.end method

.method public setRequired(Z)V
    .locals 0
    .param p1, "required"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mRequired:Z

    .line 108
    return-void
.end method

.method public validate()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 128
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/SpinnerAdapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must set non-empty adapter before validating"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mPotentialErrorOnConfigChange:Z

    .line 132
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 133
    .local v0, "selectedView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->isValid()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 134
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    invoke-interface {v2, p0, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;->clearError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;)V

    .line 139
    :goto_0
    return v1

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->mInvalidator:Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_field_must_not_be_empty:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p0, v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner$Invalidator;->setError(Lcom/google/android/wallet/instrumentmanager/ui/common/FormSpinner;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 139
    const/4 v1, 0x0

    goto :goto_0
.end method
