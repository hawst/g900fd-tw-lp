.class final Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;
.super Ljava/lang/Object;
.source "WalletUiUtils.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->playShakeAnimationIfPossible(Landroid/content/Context;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$shakeCount:I

.field final synthetic val$shakeDelta:F


# direct methods
.method constructor <init>(IF)V
    .locals 0

    .prologue
    .line 223
    iput p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;->val$shakeCount:I

    iput p2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;->val$shakeDelta:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "input"    # F

    .prologue
    .line 226
    float-to-double v0, p1

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;->val$shakeCount:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils$4;->val$shakeDelta:F

    mul-float/2addr v0, v1

    return v0
.end method
