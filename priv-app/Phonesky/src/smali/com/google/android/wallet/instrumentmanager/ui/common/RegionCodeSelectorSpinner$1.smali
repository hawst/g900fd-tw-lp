.class Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;
.super Ljava/lang/Object;
.source "RegionCodeSelectorSpinner.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->setRegionCodes([I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v1, p3}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 92
    .local v0, "regionCode":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;)Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->mOnRegionCodeSelectedListener:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;)Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelectorSpinner;->getId()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/RegionCodeSelector$OnRegionCodeSelectedListener;->onRegionCodeSelected(II)V

    .line 95
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v0, "RegionCodeSelectorSpinn"

    const-string v1, "Listener fired for onNothingSelected; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-void
.end method
