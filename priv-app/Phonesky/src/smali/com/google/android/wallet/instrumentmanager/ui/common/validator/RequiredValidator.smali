.class public Lcom/google/android/wallet/instrumentmanager/ui/common/validator/RequiredValidator;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;
.source "RequiredValidator.java"


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "customErrorMessage"    # Ljava/lang/CharSequence;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/AbstractValidator;-><init>(Ljava/lang/CharSequence;)V

    .line 15
    return-void
.end method


# virtual methods
.method public isValid(Landroid/widget/TextView;)Z
    .locals 2
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 19
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 20
    .local v0, "text":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
