.class public final Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;
.super Ljava/lang/Object;
.source "PaymentUtils.java"


# static fields
.field private static final EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

.field private static final NON_NUMERIC_PATTERN:Ljava/util/regex/Pattern;

.field private static sImageLoader:Lcom/android/volley/toolbox/ImageLoader;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const-string v0, "[^\\d]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->NON_NUMERIC_PATTERN:Ljava/util/regex/Pattern;

    .line 62
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    .line 63
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_amex:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 66
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_discover:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_jcb:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 72
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_mastercard:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 75
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_visa:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 78
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_diners:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 81
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    const/16 v1, 0xf

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$drawable;->wallet_im_card_full_elo:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 84
    return-void
.end method

.method public static createRequestContext(Landroid/content/Context;[B)Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionData"    # [B

    .prologue
    .line 138
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;-><init>()V

    .line 139
    .local v1, "nativeClientContext":Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->osVersion:Ljava/lang/String;

    .line 140
    sget-object v9, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->device:Ljava/lang/String;

    .line 142
    const-string v9, "phone"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 144
    .local v7, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v7, :cond_0

    .line 146
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v6

    .line 147
    .local v6, "simOperator":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 148
    iput-object v6, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->mccMnc:Ljava/lang/String;

    .line 152
    .end local v6    # "simOperator":Ljava/lang/String;
    :cond_0
    const-string v9, "window"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    .line 154
    .local v8, "windowManager":Landroid/view/WindowManager;
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 155
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 156
    iget v9, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenWidthPx:I

    .line 157
    iget v9, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenHeightPx:I

    .line 158
    iget v9, v0, Landroid/util/DisplayMetrics;->xdpi:F

    iput v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenXDpi:F

    .line 159
    iget v9, v0, Landroid/util/DisplayMetrics;->ydpi:F

    iput v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->screenYDpi:F

    .line 161
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 162
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 164
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 165
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageName:Ljava/lang/String;

    .line 166
    iget v9, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionCode:Ljava/lang/String;

    .line 167
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v9, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->packageVersionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    new-instance v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    invoke-direct {v5}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;-><init>()V

    .line 171
    .local v5, "riskData":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
    invoke-static {v7}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->getProperties(Landroid/telephony/TelephonyManager;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    move-result-object v9

    iput-object v9, v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->properties:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    .line 172
    invoke-static {p0, v2, v7}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->getState(Landroid/content/Context;Landroid/content/pm/PackageInfo;Landroid/telephony/TelephonyManager;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    move-result-object v9

    iput-object v9, v5, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;->state:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    .line 173
    iput-object v5, v1, Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;->riskData:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;

    .line 175
    new-instance v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;

    invoke-direct {v4}, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;-><init>()V

    .line 176
    .local v4, "requestContext":Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
    if-eqz p1, :cond_1

    .line 177
    iput-object p1, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->sessionData:[B

    .line 179
    :cond_1
    iput-object v1, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->nativeContext:Lcom/google/commerce/payments/orchestration/proto/common/ContextOuterClass$NativeClientContext;

    .line 180
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v9}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->toBcp47LanguageCode(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->languageCode:Ljava/lang/String;

    .line 182
    const/4 v9, 0x2

    iput v9, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientType:I

    .line 183
    const-wide/32 v10, 0x9bb748

    iput-wide v10, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;->clientVersion:J

    .line 185
    return-object v4

    .line 168
    .end local v4    # "requestContext":Lcom/google/commerce/payments/orchestration/proto/ui/common/RequestContextOuterClass$RequestContext;
    .end local v5    # "riskData":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed;
    :catch_0
    move-exception v9

    goto :goto_0
.end method

.method public static embeddedImageUriToDrawableResourceId(Ljava/lang/String;)I
    .locals 5
    .param p0, "embeddedImageUri"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 112
    const-string v2, "embedded:"

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->isEmbeddedImageUri(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid embedded image uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 115
    :cond_0
    const-string v2, "embedded:"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 117
    .local v1, "embeddedImageId":I
    sget-object v2, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->EMBEDDED_IMAGE_ID_TO_DRAWABLE_RESOURCE_ID:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 118
    .local v0, "drawableResourceId":I
    if-ne v0, v3, :cond_1

    .line 119
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid embedded image id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 121
    :cond_1
    return v0
.end method

.method public static equals(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;)Z
    .locals 2
    .param p0, "type1"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;
    .param p1, "type2"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;

    .prologue
    .line 125
    if-ne p0, p1, :cond_0

    .line 126
    const/4 v0, 0x1

    .line 131
    :goto_0
    return v0

    .line 128
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    iget-object v1, p1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CardType;->typeToken:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static findLegalMessageByCountry(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;Ljava/lang/String;)Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;
    .locals 3
    .param p0, "legalMessageSet"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 340
    if-nez p0, :cond_0

    .line 341
    const/4 v2, 0x0

    .line 348
    :goto_0
    return-object v2

    .line 343
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;->messageByCountry:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageByCountry;

    array-length v1, v2

    .local v1, "length":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 344
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;->messageByCountry:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageByCountry;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageByCountry;->country:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;->messageByCountry:[Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageByCountry;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageByCountry;->message:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    goto :goto_0

    .line 343
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 348
    :cond_2
    iget-object v2, p0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageSetOuterClass$LegalMessageSet;->defaultMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/legal/LegalMessageOuterClass$LegalMessage;

    goto :goto_0
.end method

.method public static declared-synchronized getImageLoader(Landroid/content/Context;)Lcom/android/volley/toolbox/ImageLoader;
    .locals 5
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 355
    const-class v1, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->sImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    if-nez v0, :cond_0

    .line 356
    new-instance v2, Lcom/android/volley/toolbox/ImageLoader;

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->getImageRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v3

    new-instance v4, Lcom/google/android/wallet/instrumentmanager/common/util/BitmapLruCache;

    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G$images;->inMemoryCacheSizeDp:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/util/BitmapLruCache;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v3, v4}, Lcom/android/volley/toolbox/ImageLoader;-><init>(Lcom/android/volley/RequestQueue;Lcom/android/volley/toolbox/ImageLoader$ImageCache;)V

    sput-object v2, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->sImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    .line 360
    :cond_0
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->sImageLoader:Lcom/android/volley/toolbox/ImageLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getProperties(Landroid/telephony/TelephonyManager;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
    .locals 6
    .param p0, "telephonyManager"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 189
    new-instance v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;

    invoke-direct {v2}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;-><init>()V

    .line 190
    .local v2, "properties":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;
    const/4 v3, 0x0

    iput v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->operatingSystem:I

    .line 191
    if-eqz p0, :cond_1

    .line 192
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "deviceId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 194
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 208
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "line1Number":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 210
    iput-object v1, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->phoneNumber:Ljava/lang/String;

    .line 213
    .end local v0    # "deviceId":Ljava/lang/String;
    .end local v1    # "line1Number":Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/config/G;->androidId:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidId:J

    .line 214
    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->deviceName:Ljava/lang/String;

    .line 215
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->productName:Ljava/lang/String;

    .line 216
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->modelName:Ljava/lang/String;

    .line 217
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->manufacturer:Ljava/lang/String;

    .line 218
    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->buildFingerprint:Ljava/lang/String;

    .line 219
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->osVersion:Ljava/lang/String;

    .line 220
    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    iput-object v3, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->androidBuildBrand:Ljava/lang/String;

    .line 221
    return-object v2

    .line 197
    .restart local v0    # "deviceId":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x8

    if-gt v3, v4, :cond_2

    .line 198
    iput-object v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->esn:Ljava/lang/String;

    goto :goto_0

    .line 200
    :cond_2
    iput-object v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->meid:Ljava/lang/String;

    goto :goto_0

    .line 204
    :pswitch_1
    iput-object v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$Properties;->imei:Ljava/lang/String;

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getState(Landroid/content/Context;Landroid/content/pm/PackageInfo;Landroid/telephony/TelephonyManager;)Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;
    .param p2, "telephonyManager"    # Landroid/telephony/TelephonyManager;

    .prologue
    .line 228
    new-instance v19, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;

    invoke-direct/range {v19 .. v19}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;-><init>()V

    .line 230
    .local v19, "state":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;
    if-eqz p1, :cond_2

    .line 231
    new-instance v16, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    invoke-direct/range {v16 .. v16}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;-><init>()V

    .line 232
    .local v16, "pi":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 233
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->name:Ljava/lang/String;

    .line 235
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->versionCode:Ljava/lang/String;

    .line 236
    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v16

    iput-wide v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->firstInstallTime:J

    .line 237
    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v16

    iput-wide v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->lastUpdateTime:J

    .line 238
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 239
    .local v4, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v4, :cond_1

    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 240
    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;->installLocation:Ljava/lang/String;

    .line 242
    :cond_1
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v16, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->installedPackages:[Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;

    .line 245
    .end local v4    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v16    # "pi":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$PackageInfo;
    :cond_2
    const-string v20, "location"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/location/LocationManager;

    .line 247
    .local v15, "manager":Landroid/location/LocationManager;
    const-string v20, "network"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v14

    .line 249
    .local v14, "location":Landroid/location/Location;
    if-eqz v14, :cond_3

    .line 250
    new-instance v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    invoke-direct {v12}, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;-><init>()V

    .line 251
    .local v12, "loc":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;
    invoke-virtual {v14}, Landroid/location/Location;->getLatitude()D

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->latitude:D

    .line 252
    invoke-virtual {v14}, Landroid/location/Location;->getLongitude()D

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->longitude:D

    .line 253
    invoke-virtual {v14}, Landroid/location/Location;->getTime()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    iput-wide v0, v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->timeInMs:D

    .line 254
    invoke-virtual {v14}, Landroid/location/Location;->getAltitude()D

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->altitude:D

    .line 255
    invoke-virtual {v14}, Landroid/location/Location;->getAccuracy()F

    move-result v20

    move/from16 v0, v20

    iput v0, v12, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;->accuracy:F

    .line 256
    move-object/from16 v0, v19

    iput-object v12, v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->lastGpsLocation:Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;

    .line 260
    .end local v12    # "loc":Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State$Location;
    :cond_3
    const/16 v20, 0x0

    new-instance v21, Landroid/content/IntentFilter;

    const-string v22, "android.intent.action.BATTERY_CHANGED"

    invoke-direct/range {v21 .. v22}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v5

    .line 262
    .local v5, "batteryData":Landroid/content/Intent;
    if-eqz v5, :cond_4

    .line 263
    const-string v20, "level"

    const/16 v21, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 264
    .local v11, "level":I
    const-string v20, "scale"

    const/16 v21, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 265
    .local v17, "scale":I
    if-lez v17, :cond_4

    .line 266
    mul-int/lit8 v20, v11, 0x64

    div-int v20, v20, v17

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->percentBattery:I

    .line 270
    .end local v11    # "level":I
    .end local v17    # "scale":I
    :cond_4
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v19

    iput-wide v0, v2, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->gmtOffsetMillis:J

    .line 272
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 273
    .local v7, "contentResolver":Landroid/content/ContentResolver;
    sget v20, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v21, 0x11

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 274
    const/16 v20, 0x1

    const-string v21, "adb_enabled"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v7, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    const/16 v20, 0x1

    :goto_0
    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    .line 275
    const/16 v20, 0x1

    const-string v21, "install_non_market_apps"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v7, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    const/16 v20, 0x1

    :goto_1
    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    .line 283
    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v13, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 284
    .local v13, "locale":Ljava/util/Locale;
    invoke-virtual {v13}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->language:Ljava/lang/String;

    .line 285
    invoke-virtual {v13}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->locale:Ljava/lang/String;

    .line 287
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/util/NetUtils;->getNonLoopbackInetAddresses()Ljava/util/ArrayList;

    move-result-object v9

    .line 288
    .local v9, "inetAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    .line 289
    const/4 v8, 0x0

    .local v8, "i":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    .local v10, "length":I
    :goto_3
    if-ge v8, v10, :cond_a

    .line 290
    move-object/from16 v0, v19

    iget-object v0, v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->ipAddr:[Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/net/InetAddress;

    invoke-virtual/range {v20 .. v20}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v21, v8

    .line 289
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 274
    .end local v8    # "i":I
    .end local v9    # "inetAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    .end local v10    # "length":I
    .end local v13    # "locale":Ljava/util/Locale;
    :cond_5
    const/16 v20, 0x0

    goto :goto_0

    .line 275
    :cond_6
    const/16 v20, 0x0

    goto :goto_1

    .line 278
    :cond_7
    const/16 v20, 0x1

    const-string v21, "adb_enabled"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v7, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    const/16 v20, 0x1

    :goto_4
    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->devModeOn:Z

    .line 279
    const/16 v20, 0x1

    const-string v21, "install_non_market_apps"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v7, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    const/16 v20, 0x1

    :goto_5
    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->nonPlayInstallAllowed:Z

    goto/16 :goto_2

    .line 278
    :cond_8
    const/16 v20, 0x0

    goto :goto_4

    .line 279
    :cond_9
    const/16 v20, 0x0

    goto :goto_5

    .line 293
    .restart local v8    # "i":I
    .restart local v9    # "inetAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    .restart local v10    # "length":I
    .restart local v13    # "locale":Ljava/util/Locale;
    :cond_a
    if-eqz p2, :cond_c

    .line 294
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v6

    .line 295
    .local v6, "cellOperator":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 296
    move-object/from16 v0, v19

    iput-object v6, v0, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->cellOperator:Ljava/lang/String;

    .line 298
    :cond_b
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v18

    .line 299
    .local v18, "simOperator":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_c

    .line 300
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    iput-object v0, v1, Lpaymentfraud/mobile/DeviceFingerprinting$Parsed$State;->simOperator:Ljava/lang/String;

    .line 303
    .end local v6    # "cellOperator":Ljava/lang/String;
    .end local v18    # "simOperator":Ljava/lang/String;
    :cond_c
    return-object v19
.end method

.method public static isEmbeddedImageUri(Ljava/lang/String;)Z
    .locals 1
    .param p0, "imageUri"    # Ljava/lang/String;

    .prologue
    .line 108
    const-string v0, "embedded:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static removeNonNumericDigits(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 92
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/util/PaymentUtils;->NON_NUMERIC_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static shouldAutoCompleteBeEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/AndroidUtils;->isTouchAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->minApiLevelToShowAutocompleteForAccessibility:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toBcp47LanguageCode(Ljava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    const/16 v4, 0x2d

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 313
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 316
    .local v1, "country":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    .line 318
    .local v2, "variant":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 319
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 322
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
