.class public abstract Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;
.super Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;
.source "BaseInstrumentManagerSidecar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar$InstrumentManagerErrorListener;
    }
.end annotation


# instance fields
.field protected mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

.field mAttemptedToHandleAuth:Z

.field mQueuedRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/volley/Request",
            "<*>;>;"
        }
    .end annotation
.end field

.field private mRequestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mAttemptedToHandleAuth:Z

    .line 158
    return-void
.end method

.method protected static createArgs(Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Landroid/os/Bundle;
    .locals 3
    .param p0, "androidEnvironmentConfig"    # Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    .prologue
    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "androidConfig"

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    return-object v0
.end method

.method private static createRetryPolicy(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;)Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;
    .locals 2
    .param p0, "apiContext"    # Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    .prologue
    .line 150
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;

    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->volleyApiRequestDefaultTimeoutMs:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0, p0}, Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;-><init>(ILcom/google/android/wallet/instrumentmanager/api/http/ApiContext;)V

    return-object v1
.end method


# virtual methods
.method protected abstract clearPreviousResponses()V
.end method

.method protected executePendingRequests()V
    .locals 3

    .prologue
    .line 140
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 141
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 142
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 143
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/volley/Request;

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->sendRequest(Lcom/android/volley/Request;)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 147
    .end local v0    # "i":I
    .end local v1    # "length":I
    :cond_1
    return-void
.end method

.method protected final isWaitingForAuth()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 133
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->getState()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->getSubstate()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 118
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 119
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 121
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mAttemptedToHandleAuth:Z

    .line 122
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->executePendingRequests()V

    .line 130
    :goto_0
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_0

    .line 128
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "androidConfig"

    invoke-static {v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    .line 78
    .local v0, "androidEnvironmentConfig":Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;->create(Landroid/content/Context;Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/api/InstrumentManagerRequestQueue;->getApiRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 81
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    const-string v0, "attemptedToHandleAuth"

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mAttemptedToHandleAuth:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 88
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 94
    const-string v0, "attemptedToHandleAuth"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mAttemptedToHandleAuth:Z

    .line 95
    return-void
.end method

.method protected sendRequest(Lcom/android/volley/Request;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Request",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "request":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->clearPreviousResponses()V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->isWaitingForAuth()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mQueuedRequests:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mApiContext:Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->createRetryPolicy(Lcom/google/android/wallet/instrumentmanager/api/http/ApiContext;)Lcom/google/android/wallet/instrumentmanager/api/http/AuthHandlingRetryPolicy;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 106
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v0, p1}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 107
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/BaseInstrumentManagerSidecar;->setState(II)V

    goto :goto_0
.end method
