.class Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;
.super Ljava/lang/Object;
.source "InstrumentManagerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->trackImpressionForPageIfNecessary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;)V
    .locals 0

    .prologue
    .line 799
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    # getter for: Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->access$000(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->access$002(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;Z)Z

    .line 804
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;->this$0:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 806
    :cond_0
    return-void
.end method
