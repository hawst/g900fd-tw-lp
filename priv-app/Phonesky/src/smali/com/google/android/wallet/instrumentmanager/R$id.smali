.class public final Lcom/google/android/wallet/instrumentmanager/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_card_legal_message_text:I = 0x7f0a01e8

.field public static final add_credit_card_title:I = 0x7f0a01df

.field public static final address_field_address_line_1:I = 0x7f0a000c

.field public static final address_field_address_line_2:I = 0x7f0a000d

.field public static final address_field_address_line_3:I = 0x7f0a000e

.field public static final address_field_admin_area:I = 0x7f0a000f

.field public static final address_field_country:I = 0x7f0a0010

.field public static final address_field_dependent_locality:I = 0x7f0a0011

.field public static final address_field_locality:I = 0x7f0a0012

.field public static final address_field_organization:I = 0x7f0a0013

.field public static final address_field_phone_number:I = 0x7f0a0014

.field public static final address_field_postal_code:I = 0x7f0a0015

.field public static final address_field_recipient:I = 0x7f0a0016

.field public static final address_field_sorting_code:I = 0x7f0a0017

.field public static final address_fragment_holder:I = 0x7f0a01e7

.field public static final address_title:I = 0x7f0a01e9

.field public static final button_bar:I = 0x7f0a0114

.field public static final card_logo:I = 0x7f0a01ed

.field public static final card_number:I = 0x7f0a01e3

.field public static final card_number_concealed:I = 0x7f0a01e1

.field public static final credit_card_images:I = 0x7f0a01e0

.field public static final credit_card_label:I = 0x7f0a01ee

.field public static final credit_card_root:I = 0x7f0a01de

.field public static final customer_legal_message_text:I = 0x7f0a01f2

.field public static final cvc:I = 0x7f0a03b5

.field public static final cvc_hint:I = 0x7f0a03b6

.field public static final description:I = 0x7f0a00bb

.field public static final dynamic_address_fields_container:I = 0x7f0a03b7

.field public static final dynamic_address_fields_layout:I = 0x7f0a01ec

.field public static final error_code:I = 0x7f0a01f7

.field public static final error_message:I = 0x7f0a01cd

.field public static final error_msg:I = 0x7f0a00f4

.field public static final exp_date_and_cvc:I = 0x7f0a01e6

.field public static final exp_month:I = 0x7f0a01e5

.field public static final exp_year:I = 0x7f0a03b3

.field public static final expand_btn:I = 0x7f0a03b1

.field public static final expand_icon:I = 0x7f0a01e2

.field public static final general_logo:I = 0x7f0a00dd

.field public static final hide_address_checkbox:I = 0x7f0a01ea

.field public static final instrument_form_fragment_holder:I = 0x7f0a01f1

.field public static final legal_address_entry_fragment_holder:I = 0x7f0a01f0

.field public static final legal_message_text:I = 0x7f0a01fe

.field public static final login_help_text:I = 0x7f0a01fd

.field public static final logo:I = 0x7f0a0223

.field public static final main_content:I = 0x7f0a01f3

.field public static final ocr_icon:I = 0x7f0a01e4

.field public static final password:I = 0x7f0a01fc

.field public static final positive_btn:I = 0x7f0a03b2

.field public static final progress_bar:I = 0x7f0a010c

.field public static final region_code_spinner:I = 0x7f0a03b8

.field public static final region_code_text:I = 0x7f0a03b9

.field public static final region_code_view:I = 0x7f0a01eb

.field public static final sub_form:I = 0x7f0a01f6

.field public static final tax_info_fields_container:I = 0x7f0a01f9

.field public static final tax_info_forms_spinner:I = 0x7f0a01f8

.field public static final tax_info_fragment_holder:I = 0x7f0a01ef

.field public static final title:I = 0x7f0a009c

.field public static final title_icon:I = 0x7f0a03ba

.field public static final title_separator:I = 0x7f0a03bb

.field public static final top_bar:I = 0x7f0a01f4

.field public static final top_info_text:I = 0x7f0a01f5

.field public static final username:I = 0x7f0a01fb

.field public static final username_password_title:I = 0x7f0a01fa
