.class Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;
.super Ljava/lang/Object;
.source "FormEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 480
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 477
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v2, 0x0

    .line 467
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->isValid(Landroid/widget/TextView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # getter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mTextChangedValidator:Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$000(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;)Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/validator/ComposedValidator;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 472
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText$1;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->mSavedError:Ljava/lang/CharSequence;
    invoke-static {v0, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;->access$102(Lcom/google/android/wallet/instrumentmanager/ui/common/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    goto :goto_0
.end method
