.class public final Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;
.super Ljava/lang/Object;
.source "AddressUtils.java"


# static fields
.field private static final ALL_ADDRESS_FIELDS:[C

.field private static final ID_SEPARATOR:Ljava/util/regex/Pattern;

.field public static LANGUAGE_CODE_SEPARATORS:Ljava/util/regex/Pattern;

.field private static final LANGUAGE_SEPARATOR:Ljava/util/regex/Pattern;

.field private static final POSTAL_CODE_NUMERIC_CHARS:Ljava/util/regex/Pattern;

.field private static final POSTAL_CODE_STATIC_VALUE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "/"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->ID_SEPARATOR:Ljava/util/regex/Pattern;

    .line 58
    const-string v0, "--"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->LANGUAGE_SEPARATOR:Ljava/util/regex/Pattern;

    .line 76
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->values()[C

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->ALL_ADDRESS_FIELDS:[C

    .line 486
    const-string v0, "(\\\\d|\\d|[^\\^\\w])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->POSTAL_CODE_NUMERIC_CHARS:Ljava/util/regex/Pattern;

    .line 535
    const-string v0, "^[\\w \\-]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->POSTAL_CODE_STATIC_VALUE:Ljava/util/regex/Pattern;

    .line 793
    const-string v0, "[_-]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->LANGUAGE_CODE_SEPARATORS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static determineAddressFieldsToDisplay(Ljava/lang/String;)[C
    .locals 9
    .param p0, "format"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/UnknownFormatConversionException;
        }
    .end annotation

    .prologue
    .line 337
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 338
    new-instance v7, Ljava/util/UnknownFormatConversionException;

    const-string v8, "Cannot convert null/empty formats"

    invoke-direct {v7, v8}, Ljava/util/UnknownFormatConversionException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 341
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 342
    .local v1, "fieldOrder":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Character;>;"
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getFormatSubStrings(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 344
    .local v6, "substring":Ljava/lang/String;
    const-string v7, "%."

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "%n"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 348
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 349
    .local v0, "field":C
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    .end local v0    # "field":C
    .end local v6    # "substring":Ljava/lang/String;
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .local v3, "finalFieldOrder":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Character;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Character;

    invoke-virtual {v7}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 355
    .restart local v0    # "field":C
    const/16 v7, 0x41

    if-ne v0, v7, :cond_3

    .line 356
    const/16 v7, 0x31

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    const/16 v7, 0x33

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 360
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 364
    .end local v0    # "field":C
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v2, v7, [C

    .line 365
    .local v2, "fields":[C
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v7, v2

    if-ge v4, v7, :cond_5

    .line 366
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Character;

    invoke-virtual {v7}, Ljava/lang/Character;->charValue()C

    move-result v7

    aput-char v7, v2, v4

    .line 365
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 368
    :cond_5
    return-object v2
.end method

.method public static doesCountryUseNumericPostalCode(Lorg/json/JSONObject;)Z
    .locals 2
    .param p0, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 498
    if-nez p0, :cond_0

    .line 499
    const/4 v1, 0x0

    .line 502
    :goto_0
    return v1

    .line 501
    :cond_0
    const-string v1, "zip"

    invoke-static {p0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 502
    .local v0, "zip":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->doesPostalCodeRegexIndicateNumericPostalCode(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method static doesPostalCodeRegexIndicateNumericPostalCode(Ljava/lang/String;)Z
    .locals 3
    .param p0, "zip"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 508
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 511
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->POSTAL_CODE_NUMERIC_CHARS:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static doesZipCodeOnlyAllowOneOption(Ljava/lang/String;)Z
    .locals 1
    .param p0, "zip"    # Ljava/lang/String;

    .prologue
    .line 540
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    const/4 v0, 0x0

    .line 543
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->POSTAL_CODE_STATIC_VALUE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method public static getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "jsonObject"    # Lorg/json/JSONObject;
    .param p1, "addressDataKey"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 106
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0, "jsonObject"    # Lorg/json/JSONObject;
    .param p1, "addressDataKey"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-static {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "raw":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 125
    const/4 v1, 0x0

    .line 128
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "~"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getAddressFieldViewId(C)I
    .locals 1
    .param p0, "addressField"    # C

    .prologue
    .line 298
    sparse-switch p0, :sswitch_data_0

    .line 324
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 300
    :sswitch_0
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_1:I

    goto :goto_0

    .line 302
    :sswitch_1
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_2:I

    goto :goto_0

    .line 304
    :sswitch_2
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_3:I

    goto :goto_0

    .line 306
    :sswitch_3
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_admin_area:I

    goto :goto_0

    .line 308
    :sswitch_4
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_country:I

    goto :goto_0

    .line 310
    :sswitch_5
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_dependent_locality:I

    goto :goto_0

    .line 312
    :sswitch_6
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_locality:I

    goto :goto_0

    .line 314
    :sswitch_7
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_organization:I

    goto :goto_0

    .line 316
    :sswitch_8
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_postal_code:I

    goto :goto_0

    .line 318
    :sswitch_9
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_recipient:I

    goto :goto_0

    .line 320
    :sswitch_a
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_sorting_code:I

    goto :goto_0

    .line 322
    :sswitch_b
    sget v0, Lcom/google/android/wallet/instrumentmanager/R$id;->address_field_address_line_1:I

    goto :goto_0

    .line 298
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x41 -> :sswitch_b
        0x43 -> :sswitch_6
        0x44 -> :sswitch_5
        0x4e -> :sswitch_9
        0x4f -> :sswitch_7
        0x52 -> :sswitch_4
        0x53 -> :sswitch_3
        0x58 -> :sswitch_a
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method public static getAdminAreaForPostalCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "countryData"    # Lorg/json/JSONObject;
    .param p1, "postalCode"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 553
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    :cond_0
    const/4 v0, 0x0

    .line 557
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAdminAreasForPostalCodes(Lorg/json/JSONObject;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public static getAdminAreasForPostalCodes(Lorg/json/JSONObject;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 14
    .param p0, "countryData"    # Lorg/json/JSONObject;
    .param p1, "postalCodes"    # [Ljava/lang/String;

    .prologue
    .line 569
    if-nez p1, :cond_1

    .line 570
    const/4 v0, 0x0

    .line 612
    :cond_0
    return-object v0

    .line 573
    :cond_1
    array-length v13, p1

    new-array v0, v13, [Ljava/lang/String;

    .line 575
    .local v0, "adminAreas":[Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getPostalCodeRegexpPattern(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 576
    .local v3, "countryZip":Ljava/util/regex/Pattern;
    if-eqz v3, :cond_0

    .line 579
    const-string v13, "sub_zips"

    invoke-static {p0, v13}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 580
    .local v12, "subZips":[Ljava/lang/String;
    if-eqz v12, :cond_0

    array-length v13, v12

    if-eqz v13, :cond_0

    .line 584
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v13, p1

    if-ge v4, v13, :cond_0

    .line 585
    aget-object v8, p1, v4

    .line 586
    .local v8, "postalCode":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v3, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 587
    const/4 v1, -0x1

    .line 588
    .local v1, "bestMatchIndex":I
    const/4 v2, 0x0

    .line 589
    .local v2, "bestMatchScore":I
    const/4 v5, 0x0

    .local v5, "j":I
    array-length v6, v12

    .local v6, "length":I
    :goto_1
    if-ge v5, v6, :cond_4

    .line 590
    aget-object v11, v12, v5

    .line 591
    .local v11, "subZip":Ljava/lang/String;
    invoke-static {v11}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getPostalCodeRegexpPatternForAdminArea(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 593
    .local v7, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 596
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v9

    .line 597
    .local v9, "score":I
    const/4 v13, -0x1

    if-eq v1, v13, :cond_2

    if-le v9, v2, :cond_3

    .line 598
    :cond_2
    move v1, v5

    .line 599
    move v2, v9

    .line 589
    .end local v9    # "score":I
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 603
    .end local v7    # "matcher":Ljava/util/regex/Matcher;
    .end local v11    # "subZip":Ljava/lang/String;
    :cond_4
    if-ltz v1, :cond_5

    .line 604
    const-string v13, "sub_keys"

    invoke-static {p0, v13}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 605
    .local v10, "subKeys":[Ljava/lang/String;
    if-eqz v10, :cond_5

    array-length v13, v10

    if-ge v1, v13, :cond_5

    .line 606
    aget-object v13, v10, v1

    aput-object v13, v0, v4

    .line 584
    .end local v1    # "bestMatchIndex":I
    .end local v2    # "bestMatchScore":I
    .end local v5    # "j":I
    .end local v6    # "length":I
    .end local v10    # "subKeys":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static getAlternativeLanguageCode(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 740
    const-string v7, "languages"

    invoke-static {p0, v7}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressDataArray(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 741
    .local v3, "languages":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v7, v3

    const/4 v8, 0x1

    if-gt v7, v8, :cond_2

    :cond_0
    move-object v5, v6

    .line 759
    :cond_1
    :goto_0
    return-object v5

    .line 745
    :cond_2
    const-string v7, "lang"

    invoke-static {p0, v7}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 746
    .local v2, "lang":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v5, v6

    .line 748
    goto :goto_0

    .line 750
    :cond_3
    invoke-static {v2, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v5, v6

    .line 752
    goto :goto_0

    .line 754
    :cond_4
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v5, v0, v1

    .line 755
    .local v5, "supportedLang":Ljava/lang/String;
    invoke-static {v5, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 754
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v5    # "supportedLang":Ljava/lang/String;
    :cond_5
    move-object v5, v6

    .line 759
    goto :goto_0
.end method

.method public static getDefaultLanguageForNonLatinRegionCode(I)Ljava/lang/String;
    .locals 1
    .param p0, "regionCode"    # I

    .prologue
    .line 849
    sparse-switch p0, :sswitch_data_0

    .line 867
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 851
    :sswitch_0
    const-string v0, "hy"

    goto :goto_0

    .line 856
    :sswitch_1
    const-string v0, "zh"

    goto :goto_0

    .line 858
    :sswitch_2
    const-string v0, "ja"

    goto :goto_0

    .line 861
    :sswitch_3
    const-string v0, "ko"

    goto :goto_0

    .line 863
    :sswitch_4
    const-string v0, "th"

    goto :goto_0

    .line 865
    :sswitch_5
    const-string v0, "vi"

    goto :goto_0

    .line 849
    nop

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_0
        0x6e -> :sswitch_1
        0x10b -> :sswitch_1
        0x150 -> :sswitch_2
        0x170 -> :sswitch_3
        0x172 -> :sswitch_3
        0x1af -> :sswitch_1
        0x288 -> :sswitch_4
        0x297 -> :sswitch_1
        0x2ce -> :sswitch_5
    .end sparse-switch
.end method

.method public static getDefaultPostalCodeForCountry(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 3
    .param p0, "countryData"    # Lorg/json/JSONObject;

    .prologue
    const/4 v1, 0x0

    .line 521
    if-nez p0, :cond_1

    move-object v0, v1

    .line 528
    :cond_0
    :goto_0
    return-object v0

    .line 524
    :cond_1
    const-string v2, "zip"

    invoke-static {p0, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 525
    .local v0, "zip":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->doesZipCodeOnlyAllowOneOption(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 528
    goto :goto_0
.end method

.method public static getDisplayCountryForDefaultLocale(I)Ljava/lang/String;
    .locals 5
    .param p0, "regionCode"    # I

    .prologue
    .line 799
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 800
    .local v0, "defaultLocale":Ljava/util/Locale;
    new-instance v1, Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->toCountryCode(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getFormatSubStrings(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "formatString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/UnknownFormatConversionException;
        }
    .end annotation

    .prologue
    .line 769
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 771
    .local v5, "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 772
    .local v2, "escaped":Z
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_4

    aget-char v1, v0, v3

    .line 773
    .local v1, "c":C
    if-eqz v2, :cond_2

    .line 774
    const/4 v2, 0x0

    .line 775
    const-string v6, "%n"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 776
    const-string v6, "%n"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 778
    :cond_0
    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v6

    if-nez v6, :cond_1

    .line 779
    new-instance v6, Ljava/util/UnknownFormatConversionException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Cannot determine AddressField for \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/UnknownFormatConversionException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 782
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 784
    :cond_2
    const/16 v6, 0x25

    if-ne v1, v6, :cond_3

    .line 785
    const/4 v2, 0x1

    goto :goto_1

    .line 787
    :cond_3
    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 790
    .end local v1    # "c":C
    :cond_4
    return-object v5
.end method

.method public static getPostalCodeRegexpPattern(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;
    .locals 5
    .param p0, "data"    # Lorg/json/JSONObject;

    .prologue
    const/4 v3, 0x0

    .line 442
    if-nez p0, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-object v3

    .line 446
    :cond_1
    const-string v4, "zip"

    invoke-static {p0, v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 447
    .local v2, "zip":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 451
    const-string v4, "id"

    invoke-static {p0, v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "id":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 456
    sget-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->ID_SEPARATOR:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v4

    array-length v1, v4

    .line 457
    .local v1, "idPieces":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 459
    :pswitch_0
    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v3

    goto :goto_0

    .line 461
    :pswitch_1
    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getPostalCodeRegexpPatternForAdminArea(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    goto :goto_0

    .line 457
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getPostalCodeRegexpPatternForAdminArea(Ljava/lang/String;)Ljava/util/regex/Pattern;
    .locals 2
    .param p0, "zipStartsWithRegEx"    # Ljava/lang/String;

    .prologue
    .line 472
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    const/4 v0, 0x0

    .line 475
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ").*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRegionCodeFromAddressData(Lorg/json/JSONObject;)I
    .locals 6
    .param p0, "data"    # Lorg/json/JSONObject;

    .prologue
    const/4 v3, 0x0

    .line 407
    if-nez p0, :cond_1

    .line 421
    :cond_0
    :goto_0
    return v3

    .line 411
    :cond_1
    const-string v4, "id"

    invoke-static {p0, v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "id":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 416
    sget-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->ID_SEPARATOR:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v1

    .line 417
    .local v1, "idPieces":[Ljava/lang/String;
    array-length v4, v1

    packed-switch v4, :pswitch_data_0

    .line 423
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid address data id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 420
    :pswitch_0
    sget-object v4, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->LANGUAGE_SEPARATOR:Ljava/util/regex/Pattern;

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    .line 421
    .local v2, "regionCodePieces":[Ljava/lang/String;
    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->safeToRegionCode(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 417
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static isAddressFieldRequired(CLorg/json/JSONObject;)Z
    .locals 3
    .param p0, "addressField"    # C
    .param p1, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 381
    const/16 v2, 0x4e

    if-ne p0, v2, :cond_0

    .line 382
    const/4 v2, 0x1

    .line 396
    :goto_0
    return v2

    .line 385
    :cond_0
    const-string v2, "require"

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "required":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 387
    const/4 v2, 0x0

    goto :goto_0

    .line 390
    :cond_1
    move v0, p0

    .line 391
    .local v0, "field":C
    const/16 v2, 0x31

    if-ne p0, v2, :cond_2

    .line 393
    const/16 v0, 0x41

    .line 396
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_0
.end method

.method public static isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "languageCode1"    # Ljava/lang/String;
    .param p1, "languageCode2"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 814
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 821
    :cond_0
    :goto_0
    return v2

    .line 817
    :cond_1
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->LANGUAGE_CODE_SEPARATORS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v3

    aget-object v0, v3, v2

    .line 819
    .local v0, "languageOnly1":Ljava/lang/String;
    sget-object v3, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->LANGUAGE_CODE_SEPARATORS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v3

    aget-object v1, v3, v2

    .line 821
    .local v1, "languageOnly2":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method

.method static isSubsetOf(Lcom/google/location/country/Postaladdress$PostalAddress;Lcom/google/location/country/Postaladdress$PostalAddress;[C)Z
    .locals 9
    .param p0, "subset"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p1, "superset"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p2, "addressFields"    # [C

    .prologue
    const/4 v7, 0x0

    .line 670
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v2, v7

    .line 690
    :cond_1
    :goto_0
    return v2

    .line 673
    :cond_2
    const/4 v2, 0x0

    .line 674
    .local v2, "foundAtLeastOneMatchingField":Z
    move-object v1, p2

    .local v1, "arr$":[C
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_1

    aget-char v0, v1, v3

    .line 675
    .local v0, "addressField":C
    if-nez v0, :cond_4

    .line 674
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 678
    :cond_4
    invoke-static {p0, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v5

    .line 680
    .local v5, "subsetValue":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 683
    invoke-static {p1, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;

    move-result-object v6

    .line 684
    .local v6, "supersetValue":Ljava/lang/String;
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 685
    const/4 v2, 0x1

    goto :goto_2

    :cond_5
    move v2, v7

    .line 687
    goto :goto_0
.end method

.method public static makeAddressFieldLabel(Landroid/content/Context;CLorg/json/JSONObject;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "addressField"    # C
    .param p2, "countryData"    # Lorg/json/JSONObject;

    .prologue
    .line 219
    sparse-switch p1, :sswitch_data_0

    .line 283
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 221
    :sswitch_0
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_address_line_1:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 223
    :sswitch_1
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_address_line_2:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 225
    :sswitch_2
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_address_line_3:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 227
    :sswitch_3
    const-string v2, "state_name_type"

    invoke-static {p2, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "stateNameType":Ljava/lang/String;
    const-string v2, "province"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_province:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 231
    :cond_0
    const-string v2, "state"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 232
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_state:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 233
    :cond_1
    const-string v2, "area"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 234
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_area:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 235
    :cond_2
    const-string v2, "county"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 236
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_county:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 237
    :cond_3
    const-string v2, "department"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 238
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_department:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 239
    :cond_4
    const-string v2, "district"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 240
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_district:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 241
    :cond_5
    const-string v2, "do_si"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 242
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_do_si:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 243
    :cond_6
    const-string v2, "emirate"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 244
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_emirate:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 245
    :cond_7
    const-string v2, "island"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 246
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_island:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 247
    :cond_8
    const-string v2, "parish"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 248
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_parish:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 249
    :cond_9
    const-string v2, "prefecture"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 250
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_prefecture:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 251
    :cond_a
    const-string v2, "region"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 252
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_region:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 255
    :cond_b
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_admin_area_province:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 258
    .end local v0    # "stateNameType":Ljava/lang/String;
    :sswitch_4
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_country:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 260
    :sswitch_5
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_dependent_locality:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 262
    :sswitch_6
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_locality:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 264
    :sswitch_7
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_organization:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 266
    :sswitch_8
    const-string v2, "zip_name_type"

    invoke-static {p2, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "zipNameType":Ljava/lang/String;
    const-string v2, "postal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 268
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_postal_code:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 269
    :cond_c
    const-string v2, "zip"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 270
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_zip_code:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 273
    :cond_d
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_postal_code:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 276
    .end local v1    # "zipNameType":Ljava/lang/String;
    :sswitch_9
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Recipient labels should be read from an AddressForm proto."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 279
    :sswitch_a
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_sorting_code:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 281
    :sswitch_b
    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_address_field_address_line_1:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 219
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x41 -> :sswitch_b
        0x43 -> :sswitch_6
        0x44 -> :sswitch_5
        0x4e -> :sswitch_9
        0x4f -> :sswitch_7
        0x52 -> :sswitch_4
        0x53 -> :sswitch_3
        0x58 -> :sswitch_a
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method public static mergeAddresses(Ljava/util/Collection;[C)Ljava/util/ArrayList;
    .locals 7
    .param p1, "addressFields"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;[C)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/location/country/Postaladdress$PostalAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626
    .local p0, "addresses":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    if-nez p0, :cond_1

    .line 627
    const/4 v3, 0x0

    .line 656
    :cond_0
    return-object v3

    .line 630
    :cond_1
    if-nez p1, :cond_4

    .line 631
    sget-object p1, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->ALL_ADDRESS_FIELDS:[C

    .line 640
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 642
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "iSubset":I
    :goto_0
    if-ltz v1, :cond_0

    .line 643
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    .local v2, "iSuperset":I
    :goto_1
    if-ltz v2, :cond_7

    .line 644
    if-ne v1, v2, :cond_6

    .line 643
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 633
    .end local v1    # "iSubset":I
    .end local v2    # "iSuperset":I
    .end local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v6, p1

    if-ge v0, v6, :cond_2

    .line 634
    aget-char v6, p1, v0

    invoke-static {v6}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v6

    if-nez v6, :cond_5

    .line 635
    const/4 v6, 0x0

    aput-char v6, p1, v0

    .line 633
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 647
    .end local v0    # "i":I
    .restart local v1    # "iSubset":I
    .restart local v2    # "iSuperset":I
    .restart local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/location/country/Postaladdress$PostalAddress;>;"
    :cond_6
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 648
    .local v4, "subset":Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 649
    .local v5, "superset":Lcom/google/location/country/Postaladdress$PostalAddress;
    invoke-static {v4, v5, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSubsetOf(Lcom/google/location/country/Postaladdress$PostalAddress;Lcom/google/location/country/Postaladdress$PostalAddress;[C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 650
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 642
    .end local v4    # "subset":Lcom/google/location/country/Postaladdress$PostalAddress;
    .end local v5    # "superset":Lcom/google/location/country/Postaladdress$PostalAddress;
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public static scrubAndSortRegionCodes([I)[I
    .locals 18
    .param p0, "regionCodes"    # [I

    .prologue
    .line 139
    const-string v1, ""

    .line 140
    .local v1, "EMPTY_LABEL":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 141
    const/4 v13, 0x0

    .line 204
    :cond_0
    return-object v13

    .line 145
    :cond_1
    new-instance v7, Landroid/util/SparseArray;

    const/16 v15, 0x112

    invoke-direct {v7, v15}, Landroid/util/SparseArray;-><init>(I)V

    .line 148
    .local v7, "labels":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v15

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 151
    new-instance v14, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    array-length v15, v0

    invoke-direct {v14, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 152
    .local v14, "validRegionCodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v2, p0

    .local v2, "arr$":[I
    array-length v9, v2

    .local v9, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v9, :cond_3

    aget v12, v2, v4

    .line 153
    .local v12, "regionCode":I
    if-eqz v12, :cond_2

    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v15

    if-eq v12, v15, :cond_2

    .line 154
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 158
    .end local v12    # "regionCode":I
    :cond_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 160
    .local v11, "numValidRegionCodes":I
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 161
    .restart local v12    # "regionCode":I
    invoke-static {v12}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDisplayCountryForDefaultLocale(I)Ljava/lang/String;

    move-result-object v6

    .line 162
    .local v6, "label":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 163
    const-string v15, "AddressUtils"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Region code \'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\' without label"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const-string v6, ""

    .line 167
    :cond_4
    invoke-virtual {v7, v12, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 171
    .end local v6    # "label":Ljava/lang/String;
    .end local v12    # "regionCode":I
    :cond_5
    new-instance v15, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils$1;

    invoke-direct {v15, v7}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils$1;-><init>(Landroid/util/SparseArray;)V

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 183
    const/4 v10, 0x0

    .line 184
    .local v10, "newLength":I
    const/4 v8, 0x0

    .line 185
    .local v8, "last":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v11, :cond_7

    .line 186
    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 187
    .restart local v12    # "regionCode":I
    if-eq v12, v8, :cond_6

    .line 188
    move v8, v12

    .line 189
    add-int/lit8 v10, v10, 0x1

    .line 185
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 193
    .end local v12    # "regionCode":I
    :cond_7
    const/4 v8, 0x0

    .line 194
    new-array v13, v10, [I

    .line 195
    .local v13, "result":[I
    const/4 v3, 0x0

    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    if-ge v3, v11, :cond_0

    .line 196
    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 197
    .restart local v12    # "regionCode":I
    if-eq v12, v8, :cond_8

    .line 198
    aput v12, v13, v5

    .line 199
    move v8, v12

    .line 200
    add-int/lit8 v5, v5, 0x1

    .line 195
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public static shouldUseLatinScript(ILjava/lang/String;)Z
    .locals 3
    .param p0, "regionCode"    # I
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 834
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 842
    :cond_0
    :goto_0
    return v1

    .line 837
    :cond_1
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDefaultLanguageForNonLatinRegionCode(I)Ljava/lang/String;

    move-result-object v0

    .line 839
    .local v0, "regionCodeLanguage":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 842
    invoke-static {v0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static shouldUseLatinScript(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 5
    .param p0, "data"    # Lorg/json/JSONObject;
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 703
    if-nez p0, :cond_1

    .line 727
    :cond_0
    :goto_0
    return v3

    .line 707
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 711
    const-string v4, "lname"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "sub_lnames"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "lfmt"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 713
    :cond_2
    const-string v4, "lang"

    invoke-static {p0, v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getAddressData(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 715
    .local v0, "addressDataLanguage":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 716
    invoke-static {p0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getRegionCodeFromAddressData(Lorg/json/JSONObject;)I

    move-result v1

    .line 717
    .local v1, "regionCode":I
    invoke-static {v1, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(ILjava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 718
    .end local v1    # "regionCode":I
    :cond_3
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v3, v2

    .line 721
    goto :goto_0

    .line 723
    :cond_4
    invoke-static {v0, p1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_5
    move v2, v3

    goto :goto_1
.end method

.method public static stringArrayToRegionCodeArray([Ljava/lang/String;)[I
    .locals 4
    .param p0, "regionCodeIds"    # [Ljava/lang/String;

    .prologue
    .line 86
    if-nez p0, :cond_1

    .line 87
    const/4 v2, 0x0

    .line 95
    :cond_0
    return-object v2

    .line 90
    :cond_1
    array-length v1, p0

    .line 91
    .local v1, "length":I
    new-array v2, v1, [I

    .line 92
    .local v2, "regionCodes":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 93
    aget-object v3, p0, v0

    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->safeToRegionCode(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
