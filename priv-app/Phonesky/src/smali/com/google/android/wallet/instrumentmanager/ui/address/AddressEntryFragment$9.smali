.class Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;
.super Ljava/lang/Object;
.source "AddressEntryFragment.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalities()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V
    .locals 0

    .prologue
    .line 1799
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v1, 0x0

    .line 1805
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaData:Lorg/json/JSONObject;
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$802(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 1806
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # invokes: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->updateLocalityInput()V
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$900(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;)V

    .line 1808
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment$9;->this$0:Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;

    # setter for: Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->mAdminAreaMetadataRetrievalRequest:Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;
    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;->access$1002(Lcom/google/android/wallet/instrumentmanager/ui/address/AddressEntryFragment;Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;)Lcom/google/android/wallet/instrumentmanager/common/address/CountryMetadataRetrievalTask$AdminAreaMetadataRetrievalRequest;

    .line 1809
    return-void
.end method
