.class public final Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;
.super Ljava/lang/Object;
.source "AddressFormatter.java"


# static fields
.field private static final FORMATTING_CHARS_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 460
    const-string v0, "^[\\-,\\s]+|[\\-,\\s]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->FORMATTING_CHARS_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static formatAddress(Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/String;[C[C)Ljava/lang/String;
    .locals 1
    .param p0, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p1, "newLineReplacementSeparator"    # Ljava/lang/String;
    .param p2, "includeAddressFields"    # [C
    .param p3, "excludeAddressFields"    # [C

    .prologue
    .line 453
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddress(Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/String;[C[CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static formatAddress(Lcom/google/location/country/Postaladdress$PostalAddress;Ljava/lang/String;[C[CLjava/lang/String;)Ljava/lang/String;
    .locals 29
    .param p0, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p1, "newLineReplacementSeparator"    # Ljava/lang/String;
    .param p2, "includeAddressFields"    # [C
    .param p3, "excludeAddressFields"    # [C
    .param p4, "format"    # Ljava/lang/String;

    .prologue
    .line 485
    if-nez p1, :cond_0

    .line 486
    const-string p1, "\n"

    .line 489
    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v26, v0

    if-lez v26, :cond_1

    .line 491
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->values()[C

    move-result-object p2

    .line 493
    :cond_1
    if-nez p4, :cond_3

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->safeToRegionCode(Ljava/lang/String;)I

    move-result v22

    .line 495
    .local v22, "regionCode":I
    if-nez v22, :cond_2

    .line 496
    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v22

    .line 498
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 499
    .local v17, "languageCode":Ljava/lang/String;
    move/from16 v0, v22

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->shouldUseLatinScript(ILjava/lang/String;)Z

    move-result v18

    .line 500
    .local v18, "latin":Z
    move/from16 v0, v22

    move/from16 v1, v18

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->getFormat(IZLjava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 503
    .end local v17    # "languageCode":Ljava/lang/String;
    .end local v18    # "latin":Z
    .end local v22    # "regionCode":I
    :cond_3
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    .local v15, "formatted":Ljava/lang/StringBuilder;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 505
    .local v20, "line":Ljava/lang/StringBuilder;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 506
    .local v23, "staticChars":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v21

    .line 509
    .local v21, "newLineReplacementSeparatorLength":I
    if-eqz p2, :cond_7

    .line 511
    new-instance v11, Landroid/util/SparseBooleanArray;

    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->count()I

    move-result v26

    add-int/lit8 v26, v26, 0x1

    move/from16 v0, v26

    invoke-direct {v11, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 512
    .local v11, "eligibleAddressFields":Landroid/util/SparseBooleanArray;
    move-object/from16 v8, p2

    .local v8, "arr$":[C
    array-length v0, v8

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_5

    aget-char v14, v8, v16

    .line 513
    .local v14, "field":C
    invoke-static {v14}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 514
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v11, v14, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 512
    :cond_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 518
    .end local v14    # "field":C
    :cond_5
    if-eqz p3, :cond_8

    .line 519
    move-object/from16 v8, p3

    array-length v0, v8

    move/from16 v19, v0

    const/16 v16, 0x0

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_8

    aget-char v14, v8, v16

    .line 520
    .restart local v14    # "field":C
    invoke-static {v14}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 521
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v11, v14, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 519
    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 527
    .end local v8    # "arr$":[C
    .end local v11    # "eligibleAddressFields":Landroid/util/SparseBooleanArray;
    .end local v14    # "field":C
    .end local v16    # "i$":I
    .end local v19    # "len$":I
    :cond_7
    const/4 v11, 0x0

    .line 530
    .restart local v11    # "eligibleAddressFields":Landroid/util/SparseBooleanArray;
    :cond_8
    const/4 v13, 0x0

    .line 531
    .local v13, "escaped":Z
    const/4 v4, 0x0

    .line 532
    .local v4, "appendedNewLineSeparator":Z
    const/4 v6, 0x0

    .line 533
    .local v6, "appendedRecipientName":Z
    const/4 v5, 0x0

    .line 534
    .local v5, "appendedOtherField":Z
    const/4 v7, 0x0

    .line 535
    .local v7, "appendedStaticLine":Z
    const/4 v12, 0x0

    .line 537
    .local v12, "encounteredEmptyValueOnLine":Z
    const-string v26, "%A"

    const-string v27, "%1%n%2%n%3"

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 539
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .restart local v8    # "arr$":[C
    array-length v0, v8

    move/from16 v19, v0

    .restart local v19    # "len$":I
    const/16 v16, 0x0

    .restart local v16    # "i$":I
    :goto_2
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_15

    aget-char v9, v8, v16

    .line 540
    .local v9, "c":C
    if-eqz v13, :cond_13

    .line 541
    const/4 v13, 0x0

    .line 542
    const/16 v26, 0x6e

    move/from16 v0, v26

    if-ne v0, v9, :cond_c

    .line 544
    const/4 v3, 0x0

    .line 545
    .local v3, "appendNewline":Z
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-lez v26, :cond_b

    .line 546
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 547
    sget-object v26, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->FORMATTING_CHARS_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v26

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    const/4 v3, 0x1

    .line 549
    const/16 v26, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 562
    :cond_9
    :goto_3
    if-eqz v3, :cond_a

    if-lez v21, :cond_a

    .line 563
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const/4 v4, 0x1

    .line 566
    :cond_a
    const/4 v12, 0x0

    .line 596
    .end local v3    # "appendNewline":Z
    :goto_4
    const/16 v26, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 539
    :goto_5
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 550
    .restart local v3    # "appendNewline":Z
    :cond_b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-lez v26, :cond_9

    if-nez v12, :cond_9

    .line 554
    sget-object v26, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->FORMATTING_CHARS_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v26

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 556
    .local v24, "staticLine":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v26

    if-lez v26, :cond_9

    .line 557
    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const/4 v3, 0x1

    .line 559
    const/4 v7, 0x1

    goto :goto_3

    .line 568
    .end local v3    # "appendNewline":Z
    .end local v24    # "staticLine":Ljava/lang/String;
    :cond_c
    invoke-static {v9}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressField;->exists(C)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 569
    if-eqz v11, :cond_d

    invoke-virtual {v11, v9}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 570
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v9, v1}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 572
    .local v25, "value":Ljava/lang/String;
    if-eqz v25, :cond_e

    .line 573
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    .line 575
    :cond_e
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 576
    const/4 v12, 0x1

    goto :goto_4

    .line 579
    :cond_f
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    const/16 v26, 0x4e

    move/from16 v0, v26

    if-ne v0, v9, :cond_10

    .line 582
    const/4 v6, 0x1

    goto :goto_4

    .line 584
    :cond_10
    const/4 v5, 0x1

    goto :goto_4

    .line 588
    .end local v25    # "value":Ljava/lang/String;
    :cond_11
    const/4 v12, 0x1

    goto :goto_4

    .line 591
    :cond_12
    const-string v26, "AddressFormatter"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Could not format AddressField."

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    const/4 v12, 0x1

    goto/16 :goto_4

    .line 597
    :cond_13
    const/16 v26, 0x25

    move/from16 v0, v26

    if-ne v9, v0, :cond_14

    .line 598
    const/4 v13, 0x1

    goto/16 :goto_5

    .line 601
    :cond_14
    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 606
    .end local v9    # "c":C
    :cond_15
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-gtz v26, :cond_16

    if-nez v12, :cond_1d

    .line 607
    :cond_16
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-nez v26, :cond_17

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-lez v26, :cond_17

    .line 608
    const/4 v7, 0x1

    .line 610
    :cond_17
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 611
    sget-object v26, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->FORMATTING_CHARS_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v26

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    :cond_18
    :goto_6
    if-eqz v11, :cond_19

    const/16 v26, 0x52

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v26

    if-eqz v26, :cond_1c

    .line 619
    :cond_19
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-eqz v26, :cond_1a

    if-eqz v6, :cond_1c

    if-nez v5, :cond_1c

    if-nez v7, :cond_1c

    .line 621
    :cond_1a
    const/16 v26, 0x52

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 623
    .local v10, "country":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_1c

    .line 624
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    if-lez v26, :cond_1b

    if-lez v21, :cond_1b

    .line 625
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    :cond_1b
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    .end local v10    # "country":Ljava/lang/String;
    :cond_1c
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    return-object v26

    .line 612
    :cond_1d
    if-eqz v4, :cond_18

    .line 613
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v26

    sub-int v26, v26, v21

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public static formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;C)Ljava/lang/String;
    .locals 1
    .param p0, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p1, "addressField"    # C

    .prologue
    .line 644
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressFormatter;->formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static formatAddressValue(Lcom/google/location/country/Postaladdress$PostalAddress;CLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "address"    # Lcom/google/location/country/Postaladdress$PostalAddress;
    .param p1, "addressField"    # C
    .param p2, "newLineReplacementSeparator"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 661
    if-nez p0, :cond_1

    .line 720
    :cond_0
    :goto_0
    return-object v1

    .line 664
    :cond_1
    if-nez p2, :cond_2

    .line 665
    const-string p2, "\n"

    .line 667
    :cond_2
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 678
    :sswitch_0
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    if-lt v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    goto :goto_0

    .line 669
    :sswitch_1
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    goto :goto_0

    .line 672
    :sswitch_2
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    goto :goto_0

    .line 674
    :sswitch_3
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_4
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    goto :goto_0

    .line 680
    :sswitch_5
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    if-lt v2, v4, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    aget-object v1, v1, v3

    goto :goto_0

    .line 682
    :sswitch_6
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    aget-object v1, v1, v4

    goto :goto_0

    .line 684
    :sswitch_7
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    goto :goto_0

    .line 687
    :sswitch_8
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    goto :goto_0

    .line 690
    :sswitch_9
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    goto :goto_0

    .line 692
    :sswitch_a
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    invoke-static {p2, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 696
    :sswitch_b
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    goto/16 :goto_0

    .line 699
    :sswitch_c
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    goto/16 :goto_0

    .line 701
    :sswitch_d
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    goto/16 :goto_0

    .line 703
    :sswitch_e
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    goto/16 :goto_0

    .line 706
    :sswitch_f
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 709
    :sswitch_10
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 710
    iget-object v1, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    goto/16 :goto_0

    .line 712
    :cond_3
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->safeToRegionCode(Ljava/lang/String;)I

    move-result v0

    .line 713
    .local v0, "regionCode":I
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/wallet/instrumentmanager/common/address/RegionCode;->getUnknown()I

    move-result v2

    if-eq v2, v0, :cond_0

    .line 714
    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->getDisplayCountryForDefaultLocale(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 667
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_5
        0x33 -> :sswitch_6
        0x41 -> :sswitch_a
        0x42 -> :sswitch_f
        0x43 -> :sswitch_2
        0x44 -> :sswitch_7
        0x46 -> :sswitch_c
        0x4e -> :sswitch_3
        0x4f -> :sswitch_4
        0x50 -> :sswitch_d
        0x52 -> :sswitch_10
        0x53 -> :sswitch_1
        0x54 -> :sswitch_e
        0x55 -> :sswitch_b
        0x58 -> :sswitch_9
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method static getFormat(IZLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "regionCode"    # I
    .param p1, "latin"    # Z
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 60
    sparse-switch p0, :sswitch_data_0

    .line 398
    const-string v1, "%N%n%O%n%A%n%C"

    .line 401
    .local v1, "rawI18nFormat":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 402
    .local v0, "i18nFormatWithCountry":Ljava/lang/StringBuilder;
    if-nez p1, :cond_8

    const-string v2, "ja"

    invoke-static {v2, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ko"

    invoke-static {v2, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "zh"

    invoke-static {v2, p2}, Lcom/google/android/wallet/instrumentmanager/common/address/AddressUtils;->isSameLanguage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 406
    :cond_0
    const-string v2, "%R%n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 62
    .end local v0    # "i18nFormatWithCountry":Ljava/lang/StringBuilder;
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_0
    if-eqz p1, :cond_1

    const-string v1, "%N%n%O%n%A%n%D%n%C%n%S, %Z"

    .line 63
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_2
    goto :goto_0

    .line 62
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_1
    const-string v1, "%Z%n%S%C%D%n%A%n%O%n%N"

    goto :goto_2

    .line 65
    :sswitch_1
    if-eqz p1, :cond_2

    const-string v1, "%N%n%O%n%A%n%D%n%C%n%S%n%Z"

    .line 66
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_3
    goto :goto_0

    .line 65
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_2
    const-string v1, "%S %C%D%n%A%n%O%n%N%n%Z"

    goto :goto_3

    .line 68
    :sswitch_2
    if-eqz p1, :cond_3

    const-string v1, "%N%n%O%n%A%n%C, %S %Z"

    .line 69
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_4
    goto :goto_0

    .line 68
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_3
    const-string v1, "%Z%n%S%C%n%A%n%O%n%N"

    goto :goto_4

    .line 71
    :sswitch_3
    if-eqz p1, :cond_4

    const-string v1, "%N%n%O%n%A"

    .line 72
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_5
    goto :goto_0

    .line 71
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_4
    const-string v1, "%A%n%O%n%N"

    goto :goto_5

    .line 74
    :sswitch_4
    if-eqz p1, :cond_5

    const-string v1, "%N%n%O%n%A%n%C, %S%n%Z"

    .line 75
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_6
    goto :goto_0

    .line 74
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_5
    const-string v1, "\u3012%Z%n%S%C%n%A%n%O%n%N"

    goto :goto_6

    .line 77
    :sswitch_5
    if-eqz p1, :cond_6

    const-string v1, "%N%n%O%n%A%n%S"

    .line 78
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_7
    goto :goto_0

    .line 77
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_6
    const-string v1, "%S%n%A%n%O%n%N"

    goto :goto_7

    .line 80
    :sswitch_6
    if-eqz p1, :cond_7

    const-string v1, "%N%n%O%n%A%n%D, %C%n%S %Z"

    .line 81
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    :goto_8
    goto :goto_0

    .line 80
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :cond_7
    const-string v1, "%N%n%O%n%A%n%D %C%n%S %Z"

    goto :goto_8

    .line 87
    :sswitch_7
    const-string v1, "%N%n%O%n%A%n%Z %C%n%S"

    .line 88
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 90
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_8
    const-string v1, "%N%n%O%n%A%n%C - %Z"

    .line 91
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 93
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_9
    const-string v1, "%N%n%O%n%A%n%C %Z, %S"

    .line 94
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 97
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_a
    const-string v1, "%N%n%O%n%A%n%C%n%S%n%Z"

    .line 98
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 100
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_b
    const-string v1, "%N%n%O%n%A%n%S%n%C"

    .line 101
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 103
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_c
    const-string v1, "%N%n%O%n%A%n%Z %C, %S"

    .line 104
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 106
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_d
    const-string v1, "%N%n%O%n%A%n%C %X%n%S"

    .line 107
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto :goto_0

    .line 109
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_e
    const-string v1, "%N%n%O%n%A%n%X%n%C%nGUERNSEY%n%Z"

    .line 110
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 112
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_f
    const-string v1, "%Z%n%S%n%C%n%A%n%O%n%N"

    .line 113
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 115
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_10
    const-string v1, "%N%n%O%n%A%n%C PR %Z"

    .line 116
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 118
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_11
    const-string v1, "%N%n%O%n%A%n%Z%n%C, %S"

    .line 119
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 121
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_12
    const-string v1, "%O%n%N%n%A%nSE-%Z %C"

    .line 122
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 124
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_13
    const-string v1, "%N%n%O%n%A%nSINGAPORE %Z"

    .line 125
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 127
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_14
    const-string v1, "%N%n%O%n%A%n%X%n%C%nJERSEY%n%Z"

    .line 128
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 130
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_15
    const-string v1, "%O%n%N%n%A%nFL-%Z %C"

    .line 131
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 133
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_16
    const-string v1, "%O%n%N%n%A%n%C-%S%n%Z"

    .line 134
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 136
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_17
    const-string v1, "%N%n%O%n%A%n%D%n%Z %C, %S"

    .line 137
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 139
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_18
    const-string v1, "%N%n%O%n%X %A %C %X"

    .line 140
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 142
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_19
    const-string v1, "%N%n%O%n%A%nHT%Z %C %X"

    .line 143
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 145
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1a
    const-string v1, "%N%n%O%n%C%n%A%n%Z"

    .line 146
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 151
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1b
    const-string v1, "%N%n%O%n%A%n%C%n%S"

    .line 152
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 157
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1c
    const-string v1, "%N%n%O%n%A%n%Z %C %S"

    .line 158
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 160
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1d
    const-string v1, "%N%n%O%n%A%n%C, %Z"

    .line 161
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 166
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1e
    const-string v1, "%N%n%O%n%A%n%C%n%Z"

    .line 167
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 169
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_1f
    const-string v1, "%N%n%O%n%A%n%C%n%S %Z"

    .line 170
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 172
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_20
    const-string v1, "%N%n%O%n%A%n%C, %S %Z"

    .line 173
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 175
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_21
    const-string v1, "%N%n%O%n%A%n%C%n%S %X"

    .line 176
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 178
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_22
    const-string v1, "%N%n%O%n%A%n%Z- %C"

    .line 179
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 183
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_23
    const-string v1, "%N%n%O%n%A%n%C, %S"

    .line 184
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 186
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_24
    const-string v1, "%N%n%O%n%A%nMD-%Z %C"

    .line 187
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 189
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_25
    const-string v1, "%N%n%O%n%A%n%C-%Z"

    .line 190
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 192
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_26
    const-string v1, "%N%n%O%n%A%n%Z %C %X"

    .line 193
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 195
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_27
    const-string v1, "%O%n%N%n%A%nAX-%Z %C%n\u00c5LAND"

    .line 196
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 198
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_28
    const-string v1, "%N%n%O%n%Z %A %C"

    .line 199
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 201
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_29
    const-string v1, "%N%n%O%n%A%n%Z %C/%S"

    .line 202
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 204
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2a
    const-string v1, "%N%n%O%n%A%n%Z%n%C%n%S"

    .line 205
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 207
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2b
    const-string v1, "%O%n%N%n%A%nCH-%Z %C"

    .line 208
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 210
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2c
    const-string v1, "%N%n%O%n%A%n%C, %S%n%Z"

    .line 211
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 213
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2d
    const-string v1, "%O%n%N%n%A%nL-%Z %C"

    .line 214
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 226
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2e
    const-string v1, "%O%n%N%n%A%n%Z %C %X"

    .line 227
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 235
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_2f
    const-string v1, "%N%n%O%n%A%n%X%n%C%n%Z"

    .line 236
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 247
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_30
    const-string v1, "%N%n%O%n%A%n%C %S %Z"

    .line 248
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 253
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_31
    const-string v1, "%O%n%N%n%A%n%Z %C"

    .line 254
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 256
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_32
    const-string v1, "%Z %C%n%A%n%O%n%N"

    .line 257
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 259
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_33
    const-string v1, "%N%n%O%n%A%nSI- %Z %C"

    .line 260
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 262
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_34
    const-string v1, "%Z %C %X%n%A%n%O%n%N"

    .line 263
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 265
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_35
    const-string v1, "%S%n%Z %C %X%n%A%n%O%n%N"

    .line 266
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 268
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_36
    const-string v1, "%N%n%O%n%A%nMC-%Z %C %X"

    .line 269
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 271
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_37
    const-string v1, "%N%n%O%n%A%nAZ %Z %C"

    .line 272
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 277
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_38
    const-string v1, "%N%n%O%n%A%n%C %X"

    .line 278
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 280
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_39
    const-string v1, "%N%n%O%n%A%nFO%Z %C"

    .line 281
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 296
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3a
    const-string v1, "%N%n%O%n%A%n%C %Z"

    .line 297
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 300
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3b
    const-string v1, "%N%n%O%n%A%n%C %Z%n%S"

    .line 301
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 305
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3c
    const-string v1, "%N%n%O%n%A%n%Z%n%C"

    .line 306
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 308
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3d
    const-string v1, "%N%n%O%n%A"

    .line 309
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 311
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3e
    const-string v1, "%N%n%O%n%A%nHR-%Z %C"

    .line 312
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 315
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_3f
    const-string v1, "%N%n%O%n%A%n%C"

    .line 316
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 318
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_40
    const-string v1, "%O%n%N%n%A%nLT-%Z %C"

    .line 319
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 321
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_41
    const-string v1, "%O%n%N%n%A%n%C, %S%n%Z"

    .line 322
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 324
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_42
    const-string v1, "%O%n%N%n%A%nFI-%Z %C"

    .line 325
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 327
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_43
    const-string v1, "%N%n%O%n%A%n%S %C-%X%n%Z"

    .line 328
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 330
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_44
    const-string v1, "%Z %C  %n%A%n%O%n%N"

    .line 331
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 337
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_45
    const-string v1, "%O%n%N%n%A%n%C %S %Z"

    .line 338
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 379
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_46
    const-string v1, "%N%n%O%n%A%n%Z %C"

    .line 380
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 383
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_47
    const-string v1, "%N%n%O%n%A%n%S"

    .line 384
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 386
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_48
    const-string v1, "%N%n%O%n%A%n%X%n%C%n%S"

    .line 387
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 389
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_49
    const-string v1, "%N%n%O%n%A%n%C %Z %S"

    .line 390
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 392
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_4a
    const-string v1, "%N%n%O%n%A%n%Z-%C%n%S"

    .line 393
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 395
    .end local v1    # "rawI18nFormat":Ljava/lang/String;
    :sswitch_4b
    const-string v1, "%N%n%O%n%A%n%Z %S"

    .line 396
    .restart local v1    # "rawI18nFormat":Ljava/lang/String;
    goto/16 :goto_0

    .line 409
    .restart local v0    # "i18nFormatWithCountry":Ljava/lang/StringBuilder;
    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v2, "%n%R"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_4b
        0x25 -> :sswitch_3f
        0x2d -> :sswitch_2a
        0x32 -> :sswitch_7
        0x33 -> :sswitch_30
        0x34 -> :sswitch_31
        0x35 -> :sswitch_45
        0x38 -> :sswitch_27
        0x3a -> :sswitch_37
        0x41 -> :sswitch_46
        0x44 -> :sswitch_8
        0x45 -> :sswitch_31
        0x46 -> :sswitch_38
        0x47 -> :sswitch_46
        0x48 -> :sswitch_3a
        0x4c -> :sswitch_2e
        0x4d -> :sswitch_3a
        0x4e -> :sswitch_3a
        0x52 -> :sswitch_16
        0x53 -> :sswitch_23
        0x59 -> :sswitch_35
        0x61 -> :sswitch_30
        0x63 -> :sswitch_45
        0x64 -> :sswitch_38
        0x68 -> :sswitch_2b
        0x69 -> :sswitch_18
        0x6b -> :sswitch_3a
        0x6c -> :sswitch_7
        0x6e -> :sswitch_0
        0x6f -> :sswitch_23
        0x72 -> :sswitch_46
        0x73 -> :sswitch_46
        0x76 -> :sswitch_7
        0x78 -> :sswitch_45
        0x79 -> :sswitch_46
        0x7a -> :sswitch_46
        0x85 -> :sswitch_46
        0x8b -> :sswitch_31
        0x8f -> :sswitch_46
        0x9a -> :sswitch_46
        0xa3 -> :sswitch_3c
        0xa5 -> :sswitch_46
        0xa7 -> :sswitch_a
        0xa8 -> :sswitch_46
        0xb3 -> :sswitch_1c
        0xb4 -> :sswitch_46
        0xc9 -> :sswitch_42
        0xcb -> :sswitch_2f
        0xcd -> :sswitch_30
        0xcf -> :sswitch_39
        0xd2 -> :sswitch_2e
        0xe2 -> :sswitch_a
        0xe5 -> :sswitch_46
        0xe6 -> :sswitch_2e
        0xe7 -> :sswitch_e
        0xe9 -> :sswitch_3d
        0xec -> :sswitch_46
        0xee -> :sswitch_28
        0xf0 -> :sswitch_2e
        0xf2 -> :sswitch_46
        0xf3 -> :sswitch_2f
        0xf4 -> :sswitch_22
        0xf5 -> :sswitch_30
        0xf7 -> :sswitch_46
        0x10b -> :sswitch_5
        0x10d -> :sswitch_45
        0x10e -> :sswitch_2c
        0x112 -> :sswitch_3e
        0x114 -> :sswitch_19
        0x115 -> :sswitch_1a
        0x124 -> :sswitch_1f
        0x125 -> :sswitch_1b
        0x12c -> :sswitch_3a
        0x12d -> :sswitch_2f
        0x12e -> :sswitch_3b
        0x12f -> :sswitch_2f
        0x131 -> :sswitch_41
        0x133 -> :sswitch_46
        0x134 -> :sswitch_1c
        0x145 -> :sswitch_14
        0x14d -> :sswitch_21
        0x14f -> :sswitch_3a
        0x150 -> :sswitch_4
        0x165 -> :sswitch_1e
        0x167 -> :sswitch_34
        0x168 -> :sswitch_3a
        0x169 -> :sswitch_b
        0x16e -> :sswitch_23
        0x172 -> :sswitch_1
        0x177 -> :sswitch_46
        0x179 -> :sswitch_47
        0x17a -> :sswitch_f
        0x181 -> :sswitch_46
        0x182 -> :sswitch_3a
        0x189 -> :sswitch_15
        0x18b -> :sswitch_1e
        0x192 -> :sswitch_26
        0x193 -> :sswitch_3a
        0x194 -> :sswitch_40
        0x195 -> :sswitch_2d
        0x196 -> :sswitch_1d
        0x1a1 -> :sswitch_46
        0x1a3 -> :sswitch_36
        0x1a4 -> :sswitch_24
        0x1a5 -> :sswitch_46
        0x1a6 -> :sswitch_2e
        0x1a7 -> :sswitch_46
        0x1a8 -> :sswitch_30
        0x1ab -> :sswitch_46
        0x1ae -> :sswitch_43
        0x1af -> :sswitch_3
        0x1b0 -> :sswitch_30
        0x1b1 -> :sswitch_2e
        0x1b4 -> :sswitch_3a
        0x1b5 -> :sswitch_3c
        0x1b6 -> :sswitch_3a
        0x1b7 -> :sswitch_38
        0x1b8 -> :sswitch_17
        0x1b9 -> :sswitch_c
        0x1ba -> :sswitch_3f
        0x1c3 -> :sswitch_2e
        0x1c5 -> :sswitch_46
        0x1c6 -> :sswitch_45
        0x1c7 -> :sswitch_3b
        0x1c9 -> :sswitch_11
        0x1cc -> :sswitch_31
        0x1cf -> :sswitch_46
        0x1d0 -> :sswitch_3a
        0x1d2 -> :sswitch_47
        0x1da -> :sswitch_3a
        0x1ed -> :sswitch_3c
        0x201 -> :sswitch_1b
        0x206 -> :sswitch_1c
        0x207 -> :sswitch_49
        0x208 -> :sswitch_7
        0x20b -> :sswitch_25
        0x20c -> :sswitch_46
        0x20d -> :sswitch_2e
        0x20e -> :sswitch_2f
        0x212 -> :sswitch_10
        0x214 -> :sswitch_46
        0x217 -> :sswitch_30
        0x219 -> :sswitch_46
        0x245 -> :sswitch_2e
        0x24f -> :sswitch_46
        0x253 -> :sswitch_46
        0x255 -> :sswitch_44
        0x261 -> :sswitch_3a
        0x263 -> :sswitch_1b
        0x265 -> :sswitch_12
        0x267 -> :sswitch_13
        0x268 -> :sswitch_2f
        0x269 -> :sswitch_33
        0x26a -> :sswitch_46
        0x26b -> :sswitch_46
        0x26d -> :sswitch_46
        0x26e -> :sswitch_46
        0x26f -> :sswitch_20
        0x272 -> :sswitch_d
        0x274 -> :sswitch_38
        0x276 -> :sswitch_4a
        0x27a -> :sswitch_1e
        0x283 -> :sswitch_2f
        0x288 -> :sswitch_6
        0x28a -> :sswitch_46
        0x28d -> :sswitch_46
        0x28e -> :sswitch_46
        0x292 -> :sswitch_29
        0x296 -> :sswitch_48
        0x297 -> :sswitch_2
        0x2a1 -> :sswitch_32
        0x2ad -> :sswitch_30
        0x2b3 -> :sswitch_30
        0x2b9 -> :sswitch_1c
        0x2ba -> :sswitch_7
        0x2c1 -> :sswitch_46
        0x2c5 -> :sswitch_9
        0x2c9 -> :sswitch_30
        0x2ce -> :sswitch_1b
        0x2e6 -> :sswitch_2e
        0x30b -> :sswitch_46
        0x334 -> :sswitch_2e
        0x341 -> :sswitch_1e
        0x34d -> :sswitch_46
    .end sparse-switch
.end method
