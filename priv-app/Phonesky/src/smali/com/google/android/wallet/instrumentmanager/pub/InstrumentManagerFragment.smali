.class public final Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
.super Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;
.source "InstrumentManagerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
.implements Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;
.implements Lcom/google/android/wallet/instrumentmanager/ui/common/FormEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;
    }
.end annotation


# instance fields
.field mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

.field private mCommonToken:Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;

.field mErrorCode:Landroid/widget/TextView;

.field mErrorMessageDetails:Landroid/os/Bundle;

.field mErrorText:Landroid/widget/TextView;

.field private mImpressionForPageTracked:Z

.field private mImpressionHandler:Landroid/os/Handler;

.field private mInitialFocusRequired:Z

.field private mInitialFocusSet:Z

.field private mInstrumentManagerParameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

.field mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

.field mLastRequest:Lcom/google/protobuf/nano/MessageNano;

.field mMainContent:Landroid/view/View;

.field private mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

.field mProgressBar:Landroid/view/View;

.field private mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

.field private mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

.field mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

.field mSubFormFragmentHolder:Landroid/view/View;

.field mTopBarView:Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;

.field mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;-><init>()V

    .line 229
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    return p1
.end method

.method private constructRefreshPageRequest(Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    .locals 2
    .param p1, "refreshTriggerField"    # Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .prologue
    .line 938
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;-><init>()V

    .line 939
    .local v0, "refreshPageRequest":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getPageValue()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 940
    invoke-static {p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->copyFrom(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->refreshTriggerField:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .line 941
    return-object v0
.end method

.method private constructSavePageRequest()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    .locals 2

    .prologue
    .line 900
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;-><init>()V

    .line 901
    .local v0, "request":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerParameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    .line 902
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getPageValue()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 905
    return-object v0
.end method

.method private getPageValue()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    .locals 3

    .prologue
    .line 912
    new-instance v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    invoke-direct {v0}, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;-><init>()V

    .line 913
    .local v0, "pageValue":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    instance-of v1, v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    if-eqz v1, :cond_0

    .line 914
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 915
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->getCreditCardFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    move-result-object v1

    iput-object v1, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardFormValue;

    .line 931
    :goto_0
    return-object v0

    .line 917
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    instance-of v1, v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;

    if-eqz v1, :cond_1

    .line 918
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->getCreditCardExpirationDateFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCreditCardExpirationDate:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateFormValue;

    goto :goto_0

    .line 921
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    instance-of v1, v1, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;

    if-eqz v1, :cond_2

    .line 922
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->getCustomerFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    move-result-object v1

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newCustomer:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerFormValue;

    goto :goto_0

    .line 924
    :cond_2
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    instance-of v1, v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    if-eqz v1, :cond_3

    .line 925
    new-instance v1, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    invoke-direct {v1}, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;-><init>()V

    iput-object v1, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    .line 926
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;->newInstrument:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->getUsernamePasswordFormValue()Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    move-result-object v1

    iput-object v1, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentFormValue;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordFormValue;

    goto :goto_0

    .line 929
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown top level form"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static newInstance([B[BI)Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
    .locals 4
    .param p0, "commonToken"    # [B
    .param p1, "actionToken"    # [B
    .param p2, "themeResourceId"    # I

    .prologue
    .line 159
    array-length v2, p1

    if-gtz v2, :cond_0

    .line 160
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Action token is a required parameter"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 163
    :cond_0
    new-instance v1, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    invoke-direct {v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;-><init>()V

    .line 164
    .local v1, "instance":Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;
    invoke-static {p2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->createArgs(I)Landroid/os/Bundle;

    move-result-object v0

    .line 165
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "commonToken"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 166
    const-string v2, "actionToken"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 167
    invoke-virtual {v1, v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 168
    return-object v1
.end method

.method private onErrorResponse()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const v5, 0x104000a

    .line 556
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getSavePageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    move-result-object v13

    .line 557
    .local v13, "savePageResponse":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getRefreshPageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    move-result-object v12

    .line 561
    .local v12, "refreshPageResponse":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    if-eqz v13, :cond_1

    .line 562
    iget-object v0, v13, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 566
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getSubstate()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 611
    :goto_1
    return-void

    .line 563
    :cond_1
    if-eqz v12, :cond_0

    .line 564
    iget-object v0, v12, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    goto :goto_0

    .line 569
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x2

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_unknown_error:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ErrorUtils;->addErrorDetailsToBundle(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->showErrorMessage(Landroid/os/Bundle;)V

    goto :goto_1

    .line 577
    :pswitch_1
    if-eqz v13, :cond_2

    .line 578
    iget-object v7, v13, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .line 582
    .local v7, "error":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    :goto_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget v1, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->action:I

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_error_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->message:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->errorCode:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ErrorUtils;->addErrorDetailsToBundle(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->showErrorMessage(Landroid/os/Bundle;)V

    goto :goto_1

    .line 580
    .end local v7    # "error":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    :cond_2
    iget-object v7, v12, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    .restart local v7    # "error":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    goto :goto_2

    .line 591
    .end local v7    # "error":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;
    :pswitch_2
    iget-object v0, v13, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->error:Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;

    iget-object v8, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$UiError;->formFieldMessage:[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;

    .line 592
    .local v8, "formFieldMessages":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    move-object v6, v8

    .local v6, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_3
    if-ge v9, v10, :cond_4

    aget-object v11, v6, v9

    .line 593
    .local v11, "message":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0, v11}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->applyFormFieldMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 594
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FormFieldMessage form not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v11, Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;->formFieldReference:Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 598
    .end local v11    # "message":Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    :cond_4
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    const/16 v1, 0x657

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 600
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->focusOnFirstInvalidFormField()Z

    goto/16 :goto_1

    .line 604
    .end local v6    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    .end local v8    # "formFieldMessages":[Lcom/google/commerce/payments/orchestration/proto/ui/common/UiErrorOuterClass$FormFieldMessage;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    :pswitch_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/16 v1, 0x1f4

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_network_error_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_network_error_message:I

    invoke-virtual {p0, v3}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v5, Lcom/google/android/wallet/instrumentmanager/R$string;->wallet_im_retry:I

    invoke-virtual {p0, v5}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/wallet/instrumentmanager/common/util/ErrorUtils;->addErrorDetailsToBundle(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->showErrorMessage(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 566
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private onSuccessfulResponse()V
    .locals 5

    .prologue
    .line 521
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getSavePageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;

    move-result-object v2

    .line 522
    .local v2, "savePageResponse":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v3}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getRefreshPageResponse()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;

    move-result-object v1

    .line 524
    .local v1, "refreshPageResponse":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;
    if-eqz v2, :cond_3

    .line 525
    iget-boolean v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->flowComplete:Z

    if-eqz v3, :cond_1

    .line 526
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 527
    .local v0, "data":Landroid/os/Bundle;
    iget-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->instrumentId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 528
    const-string v3, "com.google.android.wallet.instrumentmanager.INSTRUMENT_ID"

    iget-object v4, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->instrumentId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    const/16 v4, 0x32

    invoke-interface {v3, v4, v0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;->onInstrumentManagerResult(ILandroid/os/Bundle;)V

    .line 550
    .end local v0    # "data":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 533
    :cond_1
    iget-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    if-eqz v3, :cond_2

    .line 534
    iget-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 535
    iget-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 536
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateNonSubformPageUi()V

    .line 537
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateSubFormFragment()V

    goto :goto_0

    .line 539
    :cond_2
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "SavePageResponse flowComplete flag was not set, but no error or next page was found."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 542
    :cond_3
    if-eqz v1, :cond_4

    .line 543
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 544
    iget-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageResponse;->nextPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iput-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 545
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateNonSubformPageUi()V

    .line 546
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateSubFormFragment()V

    goto :goto_0

    .line 548
    :cond_4
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Sidecar successful but no response was found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private refreshPage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;)V
    .locals 2
    .param p1, "refreshPageRequest"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    .prologue
    .line 991
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    .line 992
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->refreshPage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;)V

    .line 993
    return-void
.end method

.method private retryLastRequest()V
    .locals 6

    .prologue
    .line 970
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    instance-of v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    if-eqz v3, :cond_0

    .line 971
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    .line 972
    .local v2, "savePageRequest":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getPageValue()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v3

    iput-object v3, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 973
    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->savePage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;)V

    .line 983
    .end local v2    # "savePageRequest":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;
    :goto_0
    return-void

    .line 974
    :cond_0
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    instance-of v3, v3, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    if-eqz v3, :cond_1

    .line 975
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    check-cast v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    .line 976
    .local v1, "refreshPageRequest":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getPageValue()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    move-result-object v3

    iput-object v3, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;->pageValue:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$PageValue;

    .line 977
    invoke-direct {p0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->refreshPage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;)V

    goto :goto_0

    .line 979
    .end local v1    # "refreshPageRequest":Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;
    :cond_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 980
    .local v0, "className":Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retryLastRequest() called with invalid last request. Unexpected request class: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 979
    .end local v0    # "className":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private savePage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;)V
    .locals 2
    .param p1, "savePageRequest"    # Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    .prologue
    .line 986
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    .line 987
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->savePage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;)V

    .line 988
    return-void
.end method

.method private showErrorMessage(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "errorMessageDetails"    # Landroid/os/Bundle;

    .prologue
    .line 823
    iput-object p1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    .line 824
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateNonSubformPageUi()V

    .line 825
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateErrorMessageStateAndVisibility()V

    .line 829
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->hideSoftKeyboard(Landroid/content/Context;Landroid/view/View;)Z

    .line 830
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->announceForAccessibility(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 832
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    const/16 v1, 0x65a

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 834
    return-void
.end method

.method private trackImpressionForPageIfNecessary()V
    .locals 6

    .prologue
    .line 790
    iget-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    if-nez v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 799
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$2;-><init>(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;)V

    sget-object v0, Lcom/google/android/wallet/instrumentmanager/config/G;->pageImpressionDelayBeforeTrackingMs:Lcom/google/android/gsf/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/gsf/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 809
    :cond_0
    return-void
.end method

.method private updateErrorMessageStateAndVisibility()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 668
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    if-nez v1, :cond_0

    .line 670
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, v4}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 671
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragmentHolder:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 673
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorCode:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 689
    :goto_0
    return-void

    .line 677
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v3, "ErrorUtils.KEY_ERROR_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 678
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v2, "ErrorUtils.KEY_ERROR_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 679
    .local v0, "errorCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 680
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorCode:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 683
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setVisibility(I)V

    .line 684
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragmentHolder:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 686
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 687
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorCode:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateExpandButton()V
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->shouldShowButtonBarExpandButton()Z

    move-result v0

    if-nez v0, :cond_1

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->showExpandButton(Z)V

    .line 775
    :goto_0
    return-void

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->showExpandButton(Z)V

    .line 773
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->getButtonBarExpandButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setExpandButtonText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateNonSubformPageUi()V
    .locals 6

    .prologue
    .line 350
    const/4 v3, 0x0

    .line 351
    .local v3, "titleImageUri":Ljava/lang/String;
    const/4 v2, 0x0

    .line 353
    .local v2, "titleImageAltText":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    if-nez v4, :cond_1

    .line 354
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v1, v4, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->title:Ljava/lang/String;

    .line 355
    .local v1, "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    if-eqz v4, :cond_0

    .line 356
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v3, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->imageUri:Ljava/lang/String;

    .line 357
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v4, v4, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->titleImage:Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;

    iget-object v2, v4, Lcom/google/commerce/payments/orchestration/proto/ui/common/generic/ImageWithCaptionOuterClass$ImageWithCaption;->altText:Ljava/lang/String;

    .line 359
    :cond_0
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v4, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->submitButtonText:Ljava/lang/String;

    .line 360
    .local v0, "positiveButtonText":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iget-object v5, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v5, v5, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    invoke-virtual {v4, v5}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setInfoMessage(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;)V

    .line 367
    :goto_0
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopBarView:Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;

    invoke-virtual {v4, v1, v3, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->setTitle(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    invoke-virtual {v4, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setPositiveButtonText(Ljava/lang/String;)V

    .line 371
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateExpandButton()V

    .line 372
    return-void

    .line 363
    .end local v0    # "positiveButtonText":Ljava/lang/String;
    .end local v1    # "title":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v5, "ErrorUtils.KEY_TITLE"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 364
    .restart local v1    # "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v5, "ErrorUtils.KEY_ERROR_BUTTON_TEXT"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "positiveButtonText":Ljava/lang/String;
    goto :goto_0
.end method

.method private updateProgressBarState(ZZ)V
    .locals 5
    .param p1, "shown"    # Z
    .param p2, "triggeredByConfigChange"    # Z

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 625
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-ne p1, v0, :cond_1

    .line 665
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 625
    goto :goto_0

    .line 629
    :cond_1
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->enableUi(Z)V

    .line 630
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->isReadyToSubmit()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 631
    iget-object v3, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    if-nez p1, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setEnabled(Z)V

    .line 632
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    if-nez p1, :cond_5

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setExpandButtonEnabled(Z)V

    .line 634
    if-eqz p2, :cond_7

    .line 635
    if-eqz p1, :cond_6

    .line 636
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v0, v2

    .line 629
    goto :goto_2

    :cond_3
    move v0, v2

    .line 630
    goto :goto_3

    :cond_4
    move v0, v2

    .line 631
    goto :goto_4

    :cond_5
    move v1, v2

    .line 632
    goto :goto_5

    .line 639
    :cond_6
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 648
    :cond_7
    if-eqz p1, :cond_8

    .line 653
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->hideSoftKeyboard(Landroid/content/Context;Landroid/view/View;)Z

    .line 654
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    invoke-static {v0, v2, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    .line 656
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-static {v0, v2, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToInvisible(Landroid/view/View;II)V

    .line 657
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    const/16 v1, 0x659

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    goto :goto_1

    .line 660
    :cond_8
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    invoke-static {v0, v2, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewDisappearingToInvisible(Landroid/view/View;II)V

    .line 661
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-static {v0, v2, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->animateViewAppearing(Landroid/view/View;II)V

    goto :goto_1
.end method

.method private updateSubFormFragment()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 389
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->expand(Z)V

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInitialFocusRequired:Z

    .line 392
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInitialFocusSet:Z

    .line 394
    iput-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    .line 395
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->creditCard:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardForm;I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/AddCreditCardFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    .line 411
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->sub_form:I

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 425
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopBarView:Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;

    new-instance v1, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$1;-><init>(Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;->post(Ljava/lang/Runnable;)Z

    .line 431
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->instrumentForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/InstrumentFormOuterClass$InstrumentForm;->usernamePassword:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/UsernamePassword$UsernamePasswordForm;I)Lcom/google/android/wallet/instrumentmanager/ui/usernamepassword/UsernamePasswordFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    goto :goto_0

    .line 401
    :cond_1
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    if-eqz v0, :cond_2

    .line 402
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->creditCardExpirationDateForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/instrument/types/CreditCard$CreditCardExpirationDateForm;I)Lcom/google/android/wallet/instrumentmanager/ui/creditcard/CreditCardExpirationDateFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    goto :goto_0

    .line 404
    :cond_2
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    if-eqz v0, :cond_3

    .line 405
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v0, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->customerForm:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;

    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;->newInstance(Lcom/google/commerce/payments/orchestration/proto/ui/common/components/customer/CustomerFormOuterClass$CustomerForm;I)Lcom/google/android/wallet/instrumentmanager/ui/customer/CustomerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    goto :goto_0

    .line 408
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No top level form specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getChildren()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 961
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 962
    .local v0, "children":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;>;"
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->topInfoMessage:Lcom/google/commerce/payments/orchestration/proto/ui/common/components/InfoMessageOuterClass$InfoMessage;

    if-eqz v1, :cond_0

    .line 963
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_0
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 966
    return-object v0
.end method

.method public getParentUiNode()Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;
    .locals 1

    .prologue
    .line 946
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUiElement()Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;
    .locals 3

    .prologue
    .line 955
    new-instance v0, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;

    const/16 v1, 0x654

    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-direct {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerUiElement;-><init>(I[B)V

    return-object v0
.end method

.method public notifyFormEvent(ILandroid/os/Bundle;)V
    .locals 11
    .param p1, "eventType"    # I
    .param p2, "eventDetails"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 693
    packed-switch p1, :pswitch_data_0

    .line 763
    :pswitch_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown formEvent: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 695
    :pswitch_1
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    iget-object v9, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->isReadyToSubmit()Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 706
    :pswitch_2
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getState()I

    move-result v8

    if-eq v8, v9, :cond_0

    .line 709
    const-string v8, "FormEventListener.EXTRA_FORM_ID"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 710
    .local v7, "triggeringFormId":Ljava/lang/String;
    const-string v8, "FormEventListener.EXTRA_FIELD_ID"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 711
    .local v6, "triggeringFieldId":I
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iget-object v0, v8, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;->refreshTriggerField:[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;

    .local v0, "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v3, v0, v4

    .line 712
    .local v3, "formFieldReference":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    iget v8, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->fieldId:I

    if-ne v6, v8, :cond_1

    iget-object v8, v3, Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;->formId:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/wallet/instrumentmanager/common/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 714
    invoke-direct {p0, v3}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->constructRefreshPageRequest(Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;)Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->refreshPage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$RefreshPageRequest;)V

    goto :goto_0

    .line 711
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 720
    .end local v0    # "arr$":[Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    .end local v3    # "formFieldReference":Lcom/google/commerce/payments/orchestration/proto/ui/common/FormFieldReferenceOuterClass$FormFieldReference;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "triggeringFieldId":I
    .end local v7    # "triggeringFormId":Ljava/lang/String;
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->trackImpressionForPageIfNecessary()V

    .line 721
    iget-boolean v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInitialFocusRequired:Z

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInitialFocusSet:Z

    if-nez v8, :cond_0

    .line 722
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    invoke-static {v8}, Lcom/google/android/wallet/instrumentmanager/ui/common/WalletUiUtils;->showSoftKeyboardOnFirstEditText(Landroid/view/View;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInitialFocusSet:Z

    goto :goto_0

    .line 726
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateExpandButton()V

    goto :goto_0

    .line 729
    :pswitch_5
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v8}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getState()I

    move-result v8

    if-eq v8, v9, :cond_0

    .line 740
    invoke-direct {p0, p2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->showErrorMessage(Landroid/os/Bundle;)V

    goto :goto_0

    .line 743
    :pswitch_6
    const-string v8, "FormEventListener.EXTRA_BACKGROUND_EVENT_TYPE"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 745
    .local v1, "backgroundEventType":I
    packed-switch v1, :pswitch_data_1

    .line 758
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown analytics background event type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 747
    :pswitch_7
    const-string v8, "FormEventListener.EXTRA_BACKGROUND_EVENT_DATA"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;

    .line 749
    .local v2, "creditCardEntryAction":Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;
    if-nez v2, :cond_2

    .line 750
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "CreditCardEntryAction background events must include a CreditCardEntryAction"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 754
    :cond_2
    iget-object v8, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iget-object v8, v8, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;->logToken:[B

    invoke-static {v2, v8}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendCreditCardEntryActionBackgroundEvent(Lcom/google/android/wallet/instrumentmanager/pub/analytics/CreditCardEntryAction;[B)V

    goto/16 :goto_0

    .line 693
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 745
    :pswitch_data_1
    .packed-switch 0x302
        :pswitch_7
    .end packed-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 435
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "InstrumentManagerFragment.sidecar"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    .line 444
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    .line 446
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 448
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    if-eqz v1, :cond_1

    .line 449
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 451
    :cond_1
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mCommonToken:Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;

    iget-object v1, v1, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;->androidEnvironmentConfig:Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->newInstance(Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$AndroidEnvironmentConfig;)Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    .line 453
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    const-string v2, "InstrumentManagerFragment.sidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 457
    .end local v0    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getState()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 459
    invoke-direct {p0, v3, v3}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateProgressBarState(ZZ)V

    .line 461
    :cond_3
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->sub_form:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 817
    .local v0, "childFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 818
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 820
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 838
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->positive_btn:I

    if-ne v2, v3, :cond_4

    .line 839
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    if-nez v2, :cond_2

    .line 843
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    const/16 v3, 0x655

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendClickEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 846
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->validate()Z

    move-result v2

    if-nez v2, :cond_1

    .line 847
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    const/16 v3, 0x657

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/analytics/util/AnalyticsUtil;->createAndSendImpressionEvent(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;I)V

    .line 849
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->focusOnFirstInvalidFormField()Z

    .line 893
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 851
    :cond_1
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->constructSavePageRequest()Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->savePage(Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$SavePageRequest;)V

    goto :goto_0

    .line 855
    :cond_2
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v3, "FormEventListener.EXTRA_FORM_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 856
    .local v1, "formId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    const-string v3, "ErrorUtils.KEY_TYPE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 858
    .local v0, "errorType":I
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    .line 859
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateErrorMessageStateAndVisibility()V

    .line 860
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateNonSubformPageUi()V

    .line 861
    if-eqz v1, :cond_3

    .line 863
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->handleErrorMessageDismissed(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 864
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Form to handle error message not found: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 869
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 881
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown errorType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 871
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    const/16 v3, 0x34

    sget-object v4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v2, v3, v4}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;->onInstrumentManagerResult(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 878
    :sswitch_2
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->retryLastRequest()V

    goto :goto_0

    .line 885
    .end local v0    # "errorType":I
    .end local v1    # "formId":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lcom/google/android/wallet/instrumentmanager/R$id;->expand_btn:I

    if-ne v2, v3, :cond_0

    .line 890
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->showExpandButton(Z)V

    .line 891
    iget-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    invoke-virtual {v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;->onButtonBarExpandButtonClicked()V

    goto/16 :goto_0

    .line 869
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x1f4 -> :sswitch_2
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "InstrumentManagerFragment.webViewDialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 384
    :goto_0
    return-void

    .line 381
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getThemeResourceId()I

    move-result v1

    invoke-static {p2, v1}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->newInstance(Ljava/lang/String;I)Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;

    move-result-object v0

    .line 383
    .local v0, "fragment":Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "InstrumentManagerFragment.webViewDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/wallet/instrumentmanager/ui/common/WebViewDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 269
    .local v1, "parentFragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    if-eqz v2, :cond_0

    .line 270
    check-cast v1, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    .end local v1    # "parentFragment":Landroid/support/v4/app/Fragment;
    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    .line 281
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gsf/GservicesValue;->init(Landroid/content/Context;)V

    .line 282
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 287
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "commonToken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    const-class v3, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->parseFrom([BLjava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mCommonToken:Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$CommonToken;

    .line 290
    if-nez p1, :cond_1

    .line 291
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "actionToken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    const-class v3, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;

    invoke-static {v2, v3}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->parseFrom([BLjava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;

    .line 293
    .local v0, "actionToken":Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;
    const-string v2, "actionToken="

    invoke-static {v0, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ProtoUtils;->log(Lcom/google/protobuf/nano/MessageNano;Ljava/lang/String;)V

    .line 294
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;->initializeResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->context:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 295
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;->initializeResponse:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;

    iget-object v2, v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InitializeResponse;->initialPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 296
    iget-object v2, v0, Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;->parameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerParameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    .line 308
    .end local v0    # "actionToken":Lcom/google/commerce/payments/orchestration/proto/api/integratorbuyflow/DataTokens$ActionToken;
    :goto_1
    return-void

    .line 272
    .restart local v1    # "parentFragment":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    goto :goto_0

    .line 298
    .end local v1    # "parentFragment":Landroid/support/v4/app/Fragment;
    :cond_1
    const-string v2, "responseContext"

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    .line 300
    const-string v2, "page"

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    .line 301
    const-string v2, "instrumentManagerParameters"

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerParameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    .line 303
    const-string v2, "impressionForPageTracked"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    .line 305
    const-string v2, "errorMessageDetails"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    .line 306
    const-string v2, "lastRequest"

    invoke-static {p1, v2}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    goto :goto_1
.end method

.method protected onCreateThemedView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 313
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$layout;->fragment_instrument_manager:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 315
    .local v0, "contentView":Landroid/view/View;
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->top_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopBarView:Lcom/google/android/wallet/instrumentmanager/ui/common/TopBarView;

    .line 316
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->progress_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mProgressBar:Landroid/view/View;

    .line 317
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->main_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mMainContent:Landroid/view/View;

    .line 318
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->button_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    .line 319
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mButtonBar:Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/ButtonBar;->setExpandButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->top_info_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    .line 324
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setParentUiNode(Lcom/google/android/wallet/instrumentmanager/analytics/UiNode;)V

    .line 325
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mTopInfoText:Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/InfoMessageTextView;->setUrlClickListener(Lcom/google/android/wallet/instrumentmanager/ui/common/ClickSpan$OnClickListener;)V

    .line 326
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->sub_form:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragmentHolder:Landroid/view/View;

    .line 329
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->error_message:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorText:Landroid/widget/TextView;

    .line 330
    sget v1, Lcom/google/android/wallet/instrumentmanager/R$id;->error_code:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorCode:Landroid/widget/TextView;

    .line 332
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateNonSubformPageUi()V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/google/android/wallet/instrumentmanager/R$id;->sub_form:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    iput-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    .line 335
    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mSubFormFragment:Lcom/google/android/wallet/instrumentmanager/ui/common/FormFragment;

    if-nez v1, :cond_0

    .line 336
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateSubFormFragment()V

    .line 339
    :cond_0
    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 342
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateErrorMessageStateAndVisibility()V

    .line 345
    :cond_1
    return-object v0
.end method

.method public onDetach()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 259
    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    .line 261
    :cond_0
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onDetach()V

    .line 262
    iput-object v2, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResultListener:Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;

    .line 263
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 471
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onPause()V

    .line 472
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setListener(Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;)V

    .line 473
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 465
    invoke-super {p0}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onResume()V

    .line 466
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->setListener(Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment$Listener;)V

    .line 467
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 477
    invoke-super {p0, p1}, Lcom/google/android/wallet/instrumentmanager/ui/common/BaseInstrumentManagerFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 478
    const-string v0, "responseContext"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mResponseContext:Lcom/google/commerce/payments/orchestration/proto/ui/common/ResponseContextOuterClass$ResponseContext;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 479
    const-string v0, "page"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mPage:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$Page;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 480
    const-string v0, "instrumentManagerParameters"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerParameters:Lcom/google/commerce/payments/orchestration/proto/ui/instrumentcreator/Api$InstrumentManagerParameters;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 482
    const-string v0, "impressionForPageTracked"

    iget-boolean v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mImpressionForPageTracked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 483
    const-string v0, "errorMessageDetails"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mErrorMessageDetails:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    if-eqz v0, :cond_0

    .line 485
    const-string v0, "lastRequest"

    iget-object v1, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mLastRequest:Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v1}, Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/wallet/instrumentmanager/common/util/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 487
    :cond_0
    return-void
.end method

.method public onStateChange(Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;)V
    .locals 2
    .param p1, "sidecar"    # Lcom/google/android/wallet/instrumentmanager/sidecar/SidecarFragment;

    .prologue
    const/4 v1, 0x0

    .line 491
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    if-eq p1, v0, :cond_0

    .line 492
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected sidecar"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->mInstrumentManagerSidecar:Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;

    invoke-virtual {v0}, Lcom/google/android/wallet/instrumentmanager/sidecar/InstrumentManagerSidecar;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 513
    :goto_0
    return-void

    .line 499
    :pswitch_0
    invoke-direct {p0, v1, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateProgressBarState(ZZ)V

    goto :goto_0

    .line 502
    :pswitch_1
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateProgressBarState(ZZ)V

    goto :goto_0

    .line 505
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->onSuccessfulResponse()V

    .line 506
    invoke-direct {p0, v1, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateProgressBarState(ZZ)V

    goto :goto_0

    .line 509
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->onErrorResponse()V

    .line 510
    invoke-direct {p0, v1, v1}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->updateProgressBarState(ZZ)V

    goto :goto_0

    .line 494
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
