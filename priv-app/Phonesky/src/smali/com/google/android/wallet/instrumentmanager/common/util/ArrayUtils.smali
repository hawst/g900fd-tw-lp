.class public final Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;
.super Ljava/lang/Object;
.source "ArrayUtils.java"


# direct methods
.method public static appendToArray([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "from":[Ljava/lang/Object;, "[TT;"
    .local p1, "itemToAppend":Ljava/lang/Object;, "TT;"
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 77
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot generate array of generic type w/o class info"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_0
    if-nez p0, :cond_1

    .line 80
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 84
    .local v0, "result":[Ljava/lang/Object;, "[TT;"
    :goto_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 85
    return-object v0

    .line 82
    .end local v0    # "result":[Ljava/lang/Object;, "[TT;"
    :cond_1
    array-length v1, p0

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .restart local v0    # "result":[Ljava/lang/Object;, "[TT;"
    goto :goto_0
.end method

.method public static contains([CC)Z
    .locals 5
    .param p0, "array"    # [C
    .param p1, "value"    # C

    .prologue
    const/4 v4, 0x0

    .line 57
    if-nez p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v4

    .line 60
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[C
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    .line 61
    .local v1, "c":C
    if-ne v1, p1, :cond_2

    .line 62
    const/4 v4, 0x1

    goto :goto_0

    .line 60
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static contains([II)Z
    .locals 5
    .param p0, "array"    # [I
    .param p1, "value"    # I

    .prologue
    const/4 v4, 0x0

    .line 45
    if-nez p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v4

    .line 48
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 49
    .local v1, "i":I
    if-ne v1, p1, :cond_2

    .line 50
    const/4 v4, 0x1

    goto :goto_0

    .line 48
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static contains([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "array":[Ljava/lang/Object;, "[TT;"
    .local p1, "searchValue":Ljava/lang/Object;, "TT;"
    invoke-static {p0, p1}, Lcom/google/android/wallet/instrumentmanager/common/util/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static indexOf([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "array":[Ljava/lang/Object;, "[TT;"
    .local p1, "searchValue":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    array-length v0, p0

    .line 23
    .local v0, "arrayLength":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 24
    aget-object v2, p0, v1

    invoke-static {v2, p1}, Lcom/google/android/wallet/instrumentmanager/common/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 28
    .end local v1    # "i":I
    :goto_2
    return v1

    .line 22
    .end local v0    # "arrayLength":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 23
    .restart local v0    # "arrayLength":I
    .restart local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 28
    :cond_2
    const/4 v1, -0x1

    goto :goto_2
.end method

.method public static toWrapperArray([I)[Ljava/lang/Integer;
    .locals 4
    .param p0, "array"    # [I

    .prologue
    .line 104
    if-nez p0, :cond_1

    .line 105
    const/4 v2, 0x0

    .line 114
    :cond_0
    return-object v2

    .line 107
    :cond_1
    array-length v1, p0

    .line 108
    .local v1, "length":I
    new-array v2, v1, [Ljava/lang/Integer;

    .line 109
    .local v2, "newArray":[Ljava/lang/Integer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 112
    aget v3, p0, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
