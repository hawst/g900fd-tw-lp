.class final Lcom/google/android/gms/car/support/b;
.super Lcom/google/android/gms/car/support/FragmentTransaction;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/support/b$a;
    }
.end annotation


# instance fields
.field final Nl:Lcom/google/android/gms/car/support/g;

.field Nm:Lcom/google/android/gms/car/support/b$a;

.field Nn:Lcom/google/android/gms/car/support/b$a;

.field mAddToBackStack:Z

.field mAllowAddToBackStack:Z

.field mBreadCrumbShortTitleRes:I

.field mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

.field mBreadCrumbTitleRes:I

.field mBreadCrumbTitleText:Ljava/lang/CharSequence;

.field mCommitted:Z

.field mEnterAnim:I

.field mExitAnim:I

.field mIndex:I

.field mName:Ljava/lang/String;

.field mNumOp:I

.field mPopEnterAnim:I

.field mPopExitAnim:I

.field mTransition:I

.field mTransitionStyle:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/support/g;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/support/FragmentTransaction;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/b;->mAllowAddToBackStack:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/b;->mIndex:I

    iput-object p1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    return-void
.end method


# virtual methods
.method a(Lcom/google/android/gms/car/support/b$a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/gms/car/support/b;->Nn:Lcom/google/android/gms/car/support/b$a;

    iput-object p1, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    :goto_0
    iget v0, p0, Lcom/google/android/gms/car/support/b;->mEnterAnim:I

    iput v0, p1, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mExitAnim:I

    iput v0, p1, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopEnterAnim:I

    iput v0, p1, Lcom/google/android/gms/car/support/b$a;->popEnterAnim:I

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopExitAnim:I

    iput v0, p1, Lcom/google/android/gms/car/support/b$a;->popExitAnim:I

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mNumOp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/support/b;->mNumOp:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nn:Lcom/google/android/gms/car/support/b$a;

    iput-object v0, p1, Lcom/google/android/gms/car/support/b$a;->Np:Lcom/google/android/gms/car/support/b$a;

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nn:Lcom/google/android/gms/car/support/b$a;

    iput-object p1, v0, Lcom/google/android/gms/car/support/b$a;->No:Lcom/google/android/gms/car/support/b$a;

    iput-object p1, p0, Lcom/google/android/gms/car/support/b;->Nn:Lcom/google/android/gms/car/support/b$a;

    goto :goto_0
.end method

.method bumpBackStackNesting(I)V
    .locals 6
    .param p1, "amt"    # I

    .prologue
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/b;->mAddToBackStack:Z

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_2

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bump nesting in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bump nesting of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v3, v3, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    iget v3, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    add-int/2addr v3, p1

    iput v3, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    sget-boolean v3, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v3, :cond_4

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bump nesting of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/car/support/b$a;->No:Lcom/google/android/gms/car/support/b$a;

    move-object v2, v0

    goto/16 :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "writer"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p3, v0}, Lcom/google/android/gms/car/support/b;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 7
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "full"    # Z

    .prologue
    const/4 v1, 0x0

    if-eqz p3, :cond_8

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->mName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mIndex:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/b;->mCommitted:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/b;->mEnterAnim:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mExitAnim:I

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mEnterAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mExitAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopEnterAnim:I

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopExitAnim:I

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mPopEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopEnterAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mPopExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mPopExitAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleRes:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleRes:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_6
    iget v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleRes:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleRes:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    if-eqz v0, :cond_10

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    move v2, v1

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_10

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->cmd:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cmd="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, v3, Lcom/google/android/gms/car/support/b$a;->cmd:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  Op #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    if-eqz p3, :cond_c

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    if-nez v0, :cond_9

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    if-eqz v0, :cond_a

    :cond_9
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "enterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " exitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_a
    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->popEnterAnim:I

    if-nez v0, :cond_b

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->popExitAnim:I

    if-eqz v0, :cond_c

    :cond_b
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "popEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->popEnterAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " popExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lcom/google/android/gms/car/support/b$a;->popExitAnim:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_c
    iget-object v0, v3, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v0, v1

    :goto_2
    iget-object v5, v3, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_f

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v3, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_d

    const-string v5, "Removed: "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_3
    iget-object v5, v3, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_0
    const-string v0, "NULL"

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "ADD"

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "REPLACE"

    goto/16 :goto_1

    :pswitch_3
    const-string v0, "REMOVE"

    goto/16 :goto_1

    :pswitch_4
    const-string v0, "HIDE"

    goto/16 :goto_1

    :pswitch_5
    const-string v0, "SHOW"

    goto/16 :goto_1

    :pswitch_6
    const-string v0, "DETACH"

    goto/16 :goto_1

    :pswitch_7
    const-string v0, "ATTACH"

    goto/16 :goto_1

    :cond_d
    if-nez v0, :cond_e

    const-string v5, "Removed:"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_3

    :cond_f
    iget-object v3, v3, Lcom/google/android/gms/car/support/b$a;->No:Lcom/google/android/gms/car/support/b$a;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_10
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public run()V
    .locals 9

    const/4 v8, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Run: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/b;->mAddToBackStack:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/b;->mIndex:I

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addToBackStack() called after commit()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, v8}, Lcom/google/android/gms/car/support/b;->bumpBackStackNesting(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nm:Lcom/google/android/gms/car/support/b$a;

    move-object v4, v0

    :goto_0
    if-eqz v4, :cond_b

    iget v0, v4, Lcom/google/android/gms/car/support/b$a;->cmd:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown cmd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v4, Lcom/google/android/gms/car/support/b$a;->cmd:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;Z)V

    :cond_2
    :goto_1
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->No:Lcom/google/android/gms/car/support/b$a;

    move-object v4, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget-object v1, v1, Lcom/google/android/gms/car/support/g;->mAdded:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    move v1, v2

    move-object v3, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget-object v0, v0, Lcom/google/android/gms/car/support/g;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget-object v0, v0, Lcom/google/android/gms/car/support/g;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    sget-boolean v5, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v5, :cond_3

    const-string v5, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OP_REPLACE: adding="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " old="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz v3, :cond_4

    iget v5, v0, Lcom/google/android/gms/car/support/Fragment;->mContainerId:I

    iget v6, v3, Lcom/google/android/gms/car/support/Fragment;->mContainerId:I

    if-ne v5, v6, :cond_5

    :cond_4
    if-ne v0, v3, :cond_6

    const/4 v3, 0x0

    iput-object v3, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    :cond_5
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    iget-object v5, v4, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    if-nez v5, :cond_7

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    :cond_7
    iget-object v5, v4, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v5, v4, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iput v5, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-boolean v5, p0, Lcom/google/android/gms/car/support/b;->mAddToBackStack:Z

    if-eqz v5, :cond_8

    iget v5, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    sget-boolean v5, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v5, :cond_8

    const-string v5, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bump nesting of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/google/android/gms/car/support/Fragment;->mBackStackNesting:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v5, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v6, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v7, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;II)V

    goto :goto_3

    :cond_9
    move-object v3, v0

    :cond_a
    if-eqz v3, :cond_2

    iget v0, v4, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iput v0, v3, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;Z)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v5, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v5, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/g;->b(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v5, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/g;->c(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    :pswitch_5
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v5, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/g;->d(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, v4, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v4, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mNextAnim:I

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v5, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/g;->e(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    iget v1, v1, Lcom/google/android/gms/car/support/g;->mCurState:I

    iget v2, p0, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v3, p0, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    invoke-virtual {v0, v1, v2, v3, v8}, Lcom/google/android/gms/car/support/g;->moveToState(IIIZ)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/b;->mAddToBackStack:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->Nl:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/support/g;->b(Lcom/google/android/gms/car/support/b;)V

    :cond_c
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/support/b;->mIndex:I

    if-ltz v1, :cond_0

    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/gms/car/support/b;->mIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->mName:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/support/b;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
