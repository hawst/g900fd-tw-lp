.class public interface abstract Lcom/google/android/gms/games/appcontent/AppContentCondition;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/Freezable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/data/Freezable",
        "<",
        "Lcom/google/android/gms/games/appcontent/AppContentCondition;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract mA()Ljava/lang/String;
.end method

.method public abstract mB()Ljava/lang/String;
.end method

.method public abstract mC()Ljava/lang/String;
.end method

.method public abstract mD()Landroid/os/Bundle;
.end method
