.class public Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private Fl:Ljava/lang/String;

.field private aGD:Z

.field private aGJ:Z

.field private aGK:Z

.field private aGL:Z

.field private aGM:Z

.field private aGN:Z

.field private aGO:Z

.field private aGP:Z

.field private aGQ:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/a;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZZZZZZZZLcom/google/android/gms/photos/autobackup/model/UserQuota;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "isEnabled"    # Z
    .param p4, "wifiOnly"    # Z
    .param p5, "roamingUpload"    # Z
    .param p6, "chargingOnly"    # Z
    .param p7, "wifiOnlyVideo"    # Z
    .param p8, "uploadFullResolution"    # Z
    .param p9, "localFoldersAutoBackup"    # Z
    .param p10, "photosStorageManaged"    # Z
    .param p11, "userQuota"    # Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->Fl:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGD:Z

    iput-boolean p4, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGJ:Z

    iput-boolean p5, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGK:Z

    iput-boolean p6, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGL:Z

    iput-boolean p7, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGM:Z

    iput-boolean p8, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGN:Z

    iput-boolean p9, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGO:Z

    iput-boolean p10, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGP:Z

    iput-object p11, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGQ:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->Fl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserQuota()Lcom/google/android/gms/photos/autobackup/model/UserQuota;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGQ:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    return-object v0
.end method

.method public isChargingOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGL:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGD:Z

    return v0
.end method

.method public isLocalFoldersAutoBackup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGO:Z

    return v0
.end method

.method public isPhotosStorageManaged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGP:Z

    return v0
.end method

.method public isRoamingUpload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGK:Z

    return v0
.end method

.method public isUploadFullResolution()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGN:Z

    return v0
.end method

.method public isWifiOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGJ:Z

    return v0
.end method

.method public isWifiOnlyVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->aGM:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/model/a;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;Landroid/os/Parcel;I)V

    return-void
.end method
