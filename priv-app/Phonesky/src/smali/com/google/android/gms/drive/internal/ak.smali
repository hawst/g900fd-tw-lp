.class public final Lcom/google/android/gms/drive/internal/ak;
.super Lcom/google/android/gms/internal/wu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/wu",
        "<",
        "Lcom/google/android/gms/drive/internal/ak;",
        ">;"
    }
.end annotation


# instance fields
.field public abc:Ljava/lang/String;

.field public abd:J

.field public abe:J

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/wu;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/ak;->jO()Lcom/google/android/gms/drive/internal/ak;

    return-void
.end method


# virtual methods
.method protected c()I
    .locals 4

    invoke-super {p0}, Lcom/google/android/gms/internal/wu;->c()I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/wt;->E(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/wt;->j(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/wt;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/wt;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/drive/internal/ak;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/gms/drive/internal/ak;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p0, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    iget v2, p1, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/ak;->abd:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/internal/ak;->abe:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/internal/ak;->a(Lcom/google/android/gms/internal/wu;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    iget v0, p0, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/ak;->vB()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public jO()Lcom/google/android/gms/drive/internal/ak;
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    iput-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/ak;->aYu:Lcom/google/android/gms/internal/ww;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/ak;->aYF:I

    return-object p0
.end method

.method public writeTo(Lcom/google/android/gms/internal/wt;)V
    .locals 4
    .param p1, "output"    # Lcom/google/android/gms/internal/wt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/drive/internal/ak;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/wt;->C(II)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/ak;->abc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/wt;->b(ILjava/lang/String;)V

    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abd:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/wt;->c(IJ)V

    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/ak;->abe:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/gms/internal/wt;->c(IJ)V

    invoke-super {p0, p1}, Lcom/google/android/gms/internal/wu;->writeTo(Lcom/google/android/gms/internal/wt;)V

    return-void
.end method
