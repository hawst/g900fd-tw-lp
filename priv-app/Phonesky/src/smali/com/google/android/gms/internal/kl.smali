.class public Lcom/google/android/gms/internal/kl;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/kl$a;,
        Lcom/google/android/gms/internal/kl$e;,
        Lcom/google/android/gms/internal/kl$b;,
        Lcom/google/android/gms/internal/kl$f;,
        Lcom/google/android/gms/internal/kl$d;,
        Lcom/google/android/gms/internal/kl$h;,
        Lcom/google/android/gms/internal/kl$c;,
        Lcom/google/android/gms/internal/kl$g;
    }
.end annotation


# static fields
.field public static final abD:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Lcom/google/android/gms/drive/DriveId;",
            ">;"
        }
    .end annotation
.end field

.field public static final abE:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abF:Lcom/google/android/gms/internal/kl$a;

.field public static final abG:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abH:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abI:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abJ:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final abK:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abL:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abM:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abN:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abO:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abP:Lcom/google/android/gms/internal/kl$b;

.field public static final abQ:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abR:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abS:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abT:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final abU:Lcom/google/android/gms/internal/kl$c;

.field public static final abV:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abW:Lcom/google/android/gms/drive/metadata/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final abX:Lcom/google/android/gms/drive/metadata/internal/m;

.field public static final abY:Lcom/google/android/gms/drive/metadata/internal/m;

.field public static final abZ:Lcom/google/android/gms/internal/kl$d;

.field public static final aca:Lcom/google/android/gms/internal/kl$e;

.field public static final acb:Lcom/google/android/gms/internal/kl$f;

.field public static final acc:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Lcom/google/android/gms/common/data/BitmapTeleporter;",
            ">;"
        }
    .end annotation
.end field

.field public static final acd:Lcom/google/android/gms/internal/kl$g;

.field public static final ace:Lcom/google/android/gms/internal/kl$h;

.field public static final acf:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final acg:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ach:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final aci:Lcom/google/android/gms/drive/metadata/internal/b;

.field public static final acj:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const v8, 0x4c4b40

    const v7, 0x432380

    const v6, 0x5b8d80

    const v5, 0x3e8fa0

    const v4, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/ko;->acp:Lcom/google/android/gms/internal/ko;

    sput-object v0, Lcom/google/android/gms/internal/kl;->abD:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abE:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kl$a;

    invoke-direct {v0, v8}, Lcom/google/android/gms/internal/kl$a;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abF:Lcom/google/android/gms/internal/kl$a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abG:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abH:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abI:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/g;

    const-string v1, "fileSize"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abJ:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abK:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abL:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abM:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abN:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isEditable"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abO:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kl$b;

    const-string v1, "isPinned"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abP:Lcom/google/android/gms/internal/kl$b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abQ:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abR:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isTrashable"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abS:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abT:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kl$c;

    const-string v1, "mimeType"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abU:Lcom/google/android/gms/internal/kl$c;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abV:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/k;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abW:Lcom/google/android/gms/drive/metadata/b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "lastModifyingUser"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abX:Lcom/google/android/gms/drive/metadata/internal/m;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "sharingUser"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abY:Lcom/google/android/gms/drive/metadata/internal/m;

    new-instance v0, Lcom/google/android/gms/internal/kl$d;

    const-string v1, "parents"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->abZ:Lcom/google/android/gms/internal/kl$d;

    new-instance v0, Lcom/google/android/gms/internal/kl$e;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/internal/kl$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->aca:Lcom/google/android/gms/internal/kl$e;

    new-instance v0, Lcom/google/android/gms/internal/kl$f;

    const-string v1, "starred"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acb:Lcom/google/android/gms/internal/kl$f;

    new-instance v0, Lcom/google/android/gms/internal/kl$1;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/google/android/gms/internal/kl$1;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acc:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kl$g;

    const-string v1, "title"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acd:Lcom/google/android/gms/internal/kl$g;

    new-instance v0, Lcom/google/android/gms/internal/kl$h;

    const-string v1, "trashed"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/internal/kl$h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->ace:Lcom/google/android/gms/internal/kl$h;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acf:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acg:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "uniqueIdentifier"

    invoke-direct {v0, v1, v8}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->ach:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "writersCanShare"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->aci:Lcom/google/android/gms/drive/metadata/internal/b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "role"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kl;->acj:Lcom/google/android/gms/drive/metadata/MetadataField;

    return-void
.end method
