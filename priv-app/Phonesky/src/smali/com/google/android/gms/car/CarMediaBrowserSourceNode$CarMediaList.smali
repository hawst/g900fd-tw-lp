.class public Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarMediaBrowserSourceNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CarMediaList"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public albumArt:[B

.field final mVersionCode:I

.field public name:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/o;

    invoke-direct {v0}, Lcom/google/android/gms/car/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[BI)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "albumArt"    # [B
    .param p5, "type"    # I

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->path:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->albumArt:[B

    iput p5, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->type:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/o;->a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;Landroid/os/Parcel;I)V

    return-void
.end method
