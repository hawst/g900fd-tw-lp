.class public abstract Lcom/google/android/gms/internal/gk;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# instance fields
.field private final my:Ljava/lang/Runnable;

.field private volatile wt:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/gk$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/gk$1;-><init>(Lcom/google/android/gms/internal/gk;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/gk;->my:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/gk;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/gk;->wt:Ljava/lang/Thread;

    return-object p1
.end method


# virtual methods
.method public abstract cv()V
.end method

.method public final start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/gk;->my:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/gms/internal/gm;->a(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
