.class public Lcom/google/android/gms/photos/autobackup/model/UserQuota;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/photos/autobackup/model/UserQuota;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private aHe:J

.field private aHf:J

.field private aHg:Z

.field private aHh:Z

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/f;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(IJJZZ)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "quotaLimit"    # J
    .param p4, "quotaUsed"    # J
    .param p6, "quotaUnlimited"    # Z
    .param p7, "disableFullResUploads"    # Z

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->mVersionCode:I

    iput-wide p2, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHe:J

    iput-wide p4, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHf:J

    iput-boolean p6, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHg:Z

    iput-boolean p7, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHh:Z

    return-void
.end method


# virtual methods
.method public areFullResUploadsDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHh:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getQuotaLimit()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHe:J

    return-wide v0
.end method

.method public getQuotaUsed()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHf:J

    return-wide v0
.end method

.method public isQuotaUnlimited()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->aHg:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/model/f;->a(Lcom/google/android/gms/photos/autobackup/model/UserQuota;Landroid/os/Parcel;I)V

    return-void
.end method
