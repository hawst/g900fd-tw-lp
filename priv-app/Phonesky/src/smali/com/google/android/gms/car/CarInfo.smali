.class public Lcom/google/android/gms/car/CarInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public headUnitProtocolMajorVersionNumber:I

.field public headUnitProtocolMinorVersionNumber:I

.field final mVersionCode:I

.field public manufacturer:Ljava/lang/String;

.field public model:Ljava/lang/String;

.field public modelYear:Ljava/lang/String;

.field public vehicleId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/g;

    invoke-direct {v0}, Lcom/google/android/gms/car/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/CarInfo;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "manufacturer"    # Ljava/lang/String;
    .param p3, "model"    # Ljava/lang/String;
    .param p4, "modelYear"    # Ljava/lang/String;
    .param p5, "vehicleId"    # Ljava/lang/String;
    .param p6, "headUnitProtocolMajorVersionNumber"    # I
    .param p7, "headUnitProtocolMinorVersionNumber"    # I

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarInfo;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarInfo;->manufacturer:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/car/CarInfo;->model:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/car/CarInfo;->modelYear:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/car/CarInfo;->vehicleId:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/gms/car/CarInfo;->headUnitProtocolMajorVersionNumber:I

    iput p7, p0, Lcom/google/android/gms/car/CarInfo;->headUnitProtocolMinorVersionNumber:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarInfo;->mVersionCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/CarInfo;->modelYear:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/CarInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/CarInfo;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/car/CarInfo;->vehicleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/g;->a(Lcom/google/android/gms/car/CarInfo;Landroid/os/Parcel;I)V

    return-void
.end method
