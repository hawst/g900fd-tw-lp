.class public final Lcom/google/android/gms/internal/ef;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ee;


# instance fields
.field public final mg:Lcom/google/android/gms/internal/ep;

.field public final sA:Landroid/content/Context;

.field public final sB:Lcom/google/android/gms/internal/eo;

.field public final sz:Lcom/google/android/gms/internal/er;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ee;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ee;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ef;->CREATOR:Lcom/google/android/gms/internal/ee;

    return-void
.end method

.method constructor <init>(ILandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ef;->versionCode:I

    invoke-static {p2}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ep;

    iput-object v0, p0, Lcom/google/android/gms/internal/ef;->mg:Lcom/google/android/gms/internal/ep;

    invoke-static {p3}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/er;

    iput-object v0, p0, Lcom/google/android/gms/internal/ef;->sz:Lcom/google/android/gms/internal/er;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/internal/ef;->sA:Landroid/content/Context;

    invoke-static {p5}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/eo;

    iput-object v0, p0, Lcom/google/android/gms/internal/ef;->sB:Lcom/google/android/gms/internal/eo;

    return-void
.end method


# virtual methods
.method cr()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ef;->sB:Lcom/google/android/gms/internal/eo;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cs()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ef;->mg:Lcom/google/android/gms/internal/ep;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method ct()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ef;->sz:Lcom/google/android/gms/internal/er;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cu()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ef;->sA:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ee;->a(Lcom/google/android/gms/internal/ef;Landroid/os/Parcel;I)V

    return-void
.end method
