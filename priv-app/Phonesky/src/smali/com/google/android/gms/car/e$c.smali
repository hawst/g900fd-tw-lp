.class Lcom/google/android/gms/car/e$c;
.super Lcom/google/android/gms/car/am$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

.field private final Km:Ljava/lang/Object;

.field private volatile Lk:Z

.field private final Ll:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/car/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/e;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/am$a;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/e$c;->Km:Ljava/lang/Object;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/car/e$c;->Ll:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Lcom/google/android/gms/car/Car$CarConnectionListener;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/e$c;->Ll:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "CAR.CLIENT"

    const-string v2, "ICarConnectionListenerImpl.notifyCarConnectionToClient"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/e;->getLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/e$c$1;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/gms/car/e$c$1;-><init>(Lcom/google/android/gms/car/e$c;Lcom/google/android/gms/car/e;Lcom/google/android/gms/car/Car$CarConnectionListener;I)V

    invoke-static {v1, v2}, Lcom/google/android/gms/car/bg;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/car/Car$CarConnectionListener;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/e$c;->Ll:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "CAR.CLIENT"

    const-string v2, "ICarConnectionListenerImpl.notifyCarDisconnectionToClient"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/e;->getLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/e$c$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/car/e$c$2;-><init>(Lcom/google/android/gms/car/e$c;Lcom/google/android/gms/car/e;Lcom/google/android/gms/car/Car$CarConnectionListener;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/car/bg;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/car/Car$CarConnectionListener;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/e$c;->Ll:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/car/e$c;->Km:Ljava/lang/Object;

    monitor-enter v3

    const/4 v4, 0x0

    :try_start_0
    iput-boolean v4, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    iput-object p1, p0, Lcom/google/android/gms/car/e$c;->Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/car/e$c;->Km:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Lcom/google/android/gms/car/CarNotConnectedException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/e;->gg()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    if-nez v4, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    :goto_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/e;->gh()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/e$c;->a(Lcom/google/android/gms/car/Car$CarConnectionListener;I)V
    :try_end_3
    .catch Lcom/google/android/gms/car/CarNotConnectedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Lcom/google/android/gms/car/CarNotConnectedException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public onConnected(I)V
    .locals 4
    .param p1, "connectionType"    # I

    .prologue
    const/4 v0, 0x1

    const-string v1, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.CLIENT"

    const-string v2, "ICarConnectionListenerImpl.onConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/e$c;->Km:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    if-nez v3, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/e$c;->Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/car/e$c;->a(Lcom/google/android/gms/car/Car$CarConnectionListener;I)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onDisconnected()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/e$c;->Ll:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "CAR.CLIENT"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "CAR.CLIENT"

    const-string v3, "ICarConnectionListenerImpl.onDisconnected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/e$c;->Km:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/car/e$c;->Lk:Z

    iget-object v3, p0, Lcom/google/android/gms/car/e$c;->Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/google/android/gms/car/e;->c(Lcom/google/android/gms/car/e;)V

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/e$c;->b(Lcom/google/android/gms/car/Car$CarConnectionListener;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
