.class public Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/audience/dialogs/CircleSelection$UpdateBuilder;


# instance fields
.field private final mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method private static g(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    instance-of v0, p0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/ArrayList;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static getAddedAudienceDelta(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getInitialAudienceMembers(Landroid/content/Intent;)Ljava/util/List;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRemovedAudienceDelta(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    const-string v0, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getSelectedAudienceMembers(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->getInitialAudienceMembers(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->getRemovedAudienceDelta(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->getAddedAudienceDelta(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public bridge synthetic setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/CircleSelection$UpdateBuilder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->setAccountName(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setClientApplicationId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
    .locals 2
    .param p1, "clientApplicationId"    # Ljava/lang/String;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public bridge synthetic setClientApplicationId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/CircleSelection$UpdateBuilder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->setClientApplicationId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setInitialAudience(Ljava/util/List;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;"
        }
    .end annotation

    .prologue
    .local p1, "audience":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    if-nez p1, :cond_0

    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-static {p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->g(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setInitialCircles(Ljava/util/List;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)",
            "Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;"
        }
    .end annotation

    .prologue
    .local p1, "circles":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->setInitialAudience(Ljava/util/List;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;

    return-object p0
.end method

.method public bridge synthetic setInitialCircles(Ljava/util/List;)Lcom/google/android/gms/common/audience/dialogs/CircleSelection$UpdateBuilder;
    .locals 1
    .param p1, "x0"    # Ljava/util/List;

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->setInitialCircles(Ljava/util/List;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setUpdatePersonId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;
    .locals 3
    .param p1, "peopleQualifiedId"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-string v0, "People qualified ID"

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ql;->K(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v1, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->forPersonWithPeopleQualifiedId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/c;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic setUpdatePersonId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/CircleSelection$UpdateBuilder;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;->setUpdatePersonId(Ljava/lang/String;)Lcom/google/android/gms/common/audience/dialogs/AudienceSelectionIntentBuilder;

    move-result-object v0

    return-object v0
.end method
