.class public Lcom/google/android/gms/reminders/model/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/reminders/model/TaskList;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/reminders/model/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final aLF:Ljava/lang/Integer;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/reminders/model/j;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/model/k;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/Integer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/reminders/model/k;->aLF:Ljava/lang/Integer;

    iput p1, p0, Lcom/google/android/gms/reminders/model/k;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/reminders/model/TaskList;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/TaskList;->getSystemListId()Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/reminders/model/k;-><init>(Ljava/lang/Integer;Z)V

    return-void
.end method

.method constructor <init>(Ljava/lang/Integer;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/reminders/model/k;-><init>(ILjava/lang/Integer;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/reminders/model/TaskList;Lcom/google/android/gms/reminders/model/TaskList;)Z
    .locals 2

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/TaskList;->getSystemListId()Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/TaskList;->getSystemListId()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    instance-of v0, p1, Lcom/google/android/gms/reminders/model/TaskList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/gms/reminders/model/TaskList;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/model/k;->a(Lcom/google/android/gms/reminders/model/TaskList;Lcom/google/android/gms/reminders/model/TaskList;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic freeze()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/k;->rZ()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v0

    return-object v0
.end method

.method public getSystemListId()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/k;->aLF:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/k;->getSystemListId()Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public rZ()Lcom/google/android/gms/reminders/model/TaskList;
    .locals 0

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/reminders/model/j;->a(Lcom/google/android/gms/reminders/model/k;Landroid/os/Parcel;I)V

    return-void
.end method
