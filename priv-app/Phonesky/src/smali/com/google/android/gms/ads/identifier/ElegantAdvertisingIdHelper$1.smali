.class Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;
.super Landroid/os/AsyncTask;
.source "ElegantAdvertisingIdHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->refreshCachedData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->this$0:Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .locals 5
    .param p1, "objects"    # [Ljava/lang/Void;

    .prologue
    .line 36
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->this$0:Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;

    # getter for: Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->access$000(Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 41
    :goto_0
    return-object v1

    .line 37
    :catch_0
    move-exception v0

    .line 39
    .local v0, "exception":Ljava/lang/Exception;
    const-string v1, "Wasn\'t able to fetch the adId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    .prologue
    .line 47
    if-nez p1, :cond_0

    .line 48
    const-string v0, "AdId result returned null."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->this$0:Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;

    monitor-enter v1

    .line 52
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sCachedAdId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    # setter for: Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sIsLimitAdTrackingEnabled:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->access$202(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 54
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->onPostExecute(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;)V

    return-void
.end method
