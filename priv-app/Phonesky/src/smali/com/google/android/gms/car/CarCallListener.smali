.class public Lcom/google/android/gms/car/CarCallListener;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchPhoneKeyEvent(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    return-void
.end method

.method public onAudioStateChanged(ZII)V
    .locals 0
    .param p1, "isMuted"    # Z
    .param p2, "route"    # I
    .param p3, "supportedRouteMask"    # I

    .prologue
    return-void
.end method

.method public onCallAdded(Lcom/google/android/gms/car/CarCall;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    return-void
.end method

.method public onCallDestroyed(Lcom/google/android/gms/car/CarCall;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    return-void
.end method

.method public onCallRemoved(Lcom/google/android/gms/car/CarCall;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    return-void
.end method

.method public onCannedTextResponsesLoaded(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "cannedTextResponses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public onChildrenChanged(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "children":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/car/CarCall;>;"
    return-void
.end method

.method public onConferenceableCallsChanged(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "conferenceableCalls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/car/CarCall;>;"
    return-void
.end method

.method public onDetailsChanged(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "details"    # Lcom/google/android/gms/car/CarCall$Details;

    .prologue
    return-void
.end method

.method public onParentChanged(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "parent"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    return-void
.end method

.method public onPostDialWait(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "remainingPostDialSequence"    # Ljava/lang/String;

    .prologue
    return-void
.end method

.method public onStateChanged(Lcom/google/android/gms/car/CarCall;I)V
    .locals 0
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "state"    # I

    .prologue
    return-void
.end method
