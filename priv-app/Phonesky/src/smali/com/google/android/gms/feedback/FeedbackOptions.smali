.class public Lcom/google/android/gms/feedback/FeedbackOptions;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/feedback/FeedbackOptions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAccountInUse:Ljava/lang/String;

.field public mApplicationErrorReport:Landroid/app/ApplicationErrorReport;

.field public mBitmapTeleporter:Lcom/google/android/gms/common/data/BitmapTeleporter;

.field public mCategoryTag:Ljava/lang/String;

.field public mDescription:Ljava/lang/String;

.field public mExcludePii:Z

.field public mFileTeleporters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/feedback/FileTeleporter;",
            ">;"
        }
    .end annotation
.end field

.field public mPackageName:Ljava/lang/String;

.field public mPrimaryThemeColor:Ljava/lang/String;

.field public mPsdBundle:Landroid/os/Bundle;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/feedback/b;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 12

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/app/ApplicationErrorReport;

    invoke-direct {v6}, Landroid/app/ApplicationErrorReport;-><init>()V

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    move v11, v1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/feedback/FeedbackOptions;-><init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "accountInUse"    # Ljava/lang/String;
    .param p3, "psdBundle"    # Landroid/os/Bundle;
    .param p4, "primaryThemeColor"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "applicationErrorReport"    # Landroid/app/ApplicationErrorReport;
    .param p7, "categoryTag"    # Ljava/lang/String;
    .param p8, "bitmapTeleporter"    # Lcom/google/android/gms/common/data/BitmapTeleporter;
    .param p9, "packageName"    # Ljava/lang/String;
    .param p11, "excludePii"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/app/ApplicationErrorReport;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/common/data/BitmapTeleporter;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/feedback/FileTeleporter;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p10, "fileTeleporters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/feedback/FileTeleporter;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mAccountInUse:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mPsdBundle:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mPrimaryThemeColor:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mDescription:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mApplicationErrorReport:Landroid/app/ApplicationErrorReport;

    iput-object p7, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mCategoryTag:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mBitmapTeleporter:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iput-object p9, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mPackageName:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mFileTeleporters:Ljava/util/ArrayList;

    iput-boolean p11, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->mExcludePii:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/feedback/b;->a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/os/Parcel;I)V

    return-void
.end method
