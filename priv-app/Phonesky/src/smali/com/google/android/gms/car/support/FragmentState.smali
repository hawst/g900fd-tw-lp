.class final Lcom/google/android/gms/car/support/FragmentState;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/support/FragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field NH:Lcom/google/android/gms/car/support/Fragment;

.field final mArguments:Landroid/os/Bundle;

.field final mClassName:Ljava/lang/String;

.field final mContainerId:I

.field final mDetached:Z

.field final mFragmentId:I

.field final mFromLayout:Z

.field final mIndex:I

.field final mRetainInstance:Z

.field mSavedFragmentState:Landroid/os/Bundle;

.field final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/support/FragmentState$1;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/FragmentState$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mClassName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mFromLayout:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mFragmentId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mContainerId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mTag:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mRetainInstance:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mDetached:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mArguments:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public instantiate(Lcom/google/android/gms/car/support/FragmentActivity;Lcom/google/android/gms/car/support/Fragment;)Lcom/google/android/gms/car/support/Fragment;
    .locals 3
    .param p1, "activity"    # Lcom/google/android/gms/car/support/FragmentActivity;
    .param p2, "parent"    # Lcom/google/android/gms/car/support/Fragment;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mArguments:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mArguments:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/FragmentActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/FragmentActivity;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mClassName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/car/support/FragmentState;->mArguments:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/support/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/FragmentActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mIndex:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/car/support/Fragment;->a(ILcom/google/android/gms/car/support/Fragment;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mFromLayout:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->mFromLayout:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->mRestored:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mFragmentId:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mFragmentId:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mContainerId:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->mContainerId:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mTag:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->mTag:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mRetainInstance:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->mRetainInstance:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/FragmentState;->mDetached:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/Fragment;->mDetached:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    iget-object v1, p1, Lcom/google/android/gms/car/support/FragmentActivity;->Ny:Lcom/google/android/gms/car/support/g;

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->Nt:Lcom/google/android/gms/car/support/g;

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Instantiated fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->NH:Lcom/google/android/gms/car/support/Fragment;

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mClassName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mFromLayout:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mFragmentId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mContainerId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mRetainInstance:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mDetached:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mArguments:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
