.class public Lcom/google/android/gms/car/CarPhoneStatus$CarCall;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarPhoneStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CarCall"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarPhoneStatus$CarCall;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public callDurationSeconds:I

.field public callerId:Ljava/lang/String;

.field public callerNumber:Ljava/lang/String;

.field public callerNumberType:Ljava/lang/String;

.field public callerThumbnail:[B

.field final mVersionCode:I

.field public state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/r;

    invoke-direct {v0}, Lcom/google/android/gms/car/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "state"    # I
    .param p3, "callDurationSeconds"    # I
    .param p4, "callerNumber"    # Ljava/lang/String;
    .param p5, "callerId"    # Ljava/lang/String;
    .param p6, "callerNumberType"    # Ljava/lang/String;
    .param p7, "callerThumbnail"    # [B

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->state:I

    iput-object p4, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->callerNumber:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->callDurationSeconds:I

    iput-object p5, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->callerId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->callerNumberType:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->callerThumbnail:[B

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/r;->a(Lcom/google/android/gms/car/CarPhoneStatus$CarCall;Landroid/os/Parcel;I)V

    return-void
.end method
