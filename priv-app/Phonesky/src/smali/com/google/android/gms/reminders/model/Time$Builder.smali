.class public Lcom/google/android/gms/reminders/model/Time$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/reminders/model/Time;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private aLG:Ljava/lang/Integer;

.field private aLH:Ljava/lang/Integer;

.field private aLI:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/reminders/model/Time;
    .locals 5

    new-instance v0, Lcom/google/android/gms/reminders/model/m;

    iget-object v1, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLG:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLH:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLI:Ljava/lang/Integer;

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/reminders/model/m;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    return-object v0
.end method

.method public setHour(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/Time$Builder;
    .locals 0
    .param p1, "hour"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLG:Ljava/lang/Integer;

    return-object p0
.end method

.method public setMinute(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/Time$Builder;
    .locals 0
    .param p1, "minute"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLH:Ljava/lang/Integer;

    return-object p0
.end method

.method public setSecond(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/Time$Builder;
    .locals 0
    .param p1, "second"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Time$Builder;->aLI:Ljava/lang/Integer;

    return-object p0
.end method
