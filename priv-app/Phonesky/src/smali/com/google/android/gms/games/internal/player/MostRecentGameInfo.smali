.class public interface abstract Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/Freezable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/data/Freezable",
        "<",
        "Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract od()Ljava/lang/String;
.end method

.method public abstract oe()Ljava/lang/String;
.end method

.method public abstract of()J
.end method

.method public abstract og()Landroid/net/Uri;
.end method

.method public abstract oh()Landroid/net/Uri;
.end method

.method public abstract oi()Landroid/net/Uri;
.end method
