.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept:I = 0x7f0c0035

.field public static final common_android_wear_update_text:I = 0x7f0c0049

.field public static final common_android_wear_update_title:I = 0x7f0c0047

.field public static final common_google_play_services_enable_button:I = 0x7f0c0045

.field public static final common_google_play_services_enable_text:I = 0x7f0c0044

.field public static final common_google_play_services_enable_title:I = 0x7f0c0043

.field public static final common_google_play_services_install_button:I = 0x7f0c0042

.field public static final common_google_play_services_install_text_phone:I = 0x7f0c0040

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0c0041

.field public static final common_google_play_services_install_title:I = 0x7f0c003f

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0c004d

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0c004c

.field public static final common_google_play_services_network_error_text:I = 0x7f0c004b

.field public static final common_google_play_services_network_error_title:I = 0x7f0c004a

.field public static final common_google_play_services_unknown_issue:I = 0x7f0c004e

.field public static final common_google_play_services_unsupported_text:I = 0x7f0c0050

.field public static final common_google_play_services_unsupported_title:I = 0x7f0c004f

.field public static final common_google_play_services_update_button:I = 0x7f0c0051

.field public static final common_google_play_services_update_text:I = 0x7f0c0048

.field public static final common_google_play_services_update_title:I = 0x7f0c0046

.field public static final create_calendar_message:I = 0x7f0c0038

.field public static final create_calendar_title:I = 0x7f0c0037

.field public static final decline:I = 0x7f0c0036

.field public static final store_picture_message:I = 0x7f0c0034

.field public static final store_picture_title:I = 0x7f0c0033
