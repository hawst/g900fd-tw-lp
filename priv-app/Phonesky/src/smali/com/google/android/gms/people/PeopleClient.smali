.class public Lcom/google/android/gms/people/PeopleClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;,
        Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;,
        Lcom/google/android/gms/people/PeopleClient$c;,
        Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;,
        Lcom/google/android/gms/people/PeopleClient$OnDataChangedListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final aBT:Lcom/google/android/gms/internal/qj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectedListener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3, "connectionFailedListener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4, "clientApplicationId"    # I

    .prologue
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/PeopleClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;ILjava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connectedListener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3, "connectionFailedListener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4, "clientApplicationId"    # I
    .param p5, "realClientPackage"    # Ljava/lang/String;

    .prologue
    new-instance v0, Lcom/google/android/gms/internal/qj;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/qj;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/PeopleClient;-><init>(Lcom/google/android/gms/internal/qj;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/internal/qj;)V
    .locals 0
    .param p1, "impl"    # Lcom/google/android/gms/internal/qj;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    return-void
.end method


# virtual methods
.method public connect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qj;->connect()V

    return-void
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qj;->disconnect()V

    return-void
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qj;->isConnected()Z

    move-result v0

    return v0
.end method

.method public isConnecting()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/qj;->isConnecting()Z

    move-result v0

    return v0
.end method

.method public loadCircles(Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8
    .param p1, "listener"    # Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;
    .param p4, "circleId"    # Ljava/lang/String;
    .param p5, "circleType"    # I
    .param p6, "circleNamePrefix"    # Ljava/lang/String;
    .param p7, "getVisibility"    # Z

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    new-instance v1, Lcom/google/android/gms/people/PeopleClient$c;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/people/PeopleClient$c;-><init>(Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;Lcom/google/android/gms/people/PeopleClient$1;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/internal/qj;->a(Lcom/google/android/gms/common/api/BaseImplementation$b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public loadPeople(Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "pageId"    # Ljava/lang/String;
    .param p4, "options"    # Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    new-instance v1, Lcom/google/android/gms/people/PeopleClient$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/people/PeopleClient$1;-><init>(Lcom/google/android/gms/people/PeopleClient;Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;)V

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/google/android/gms/internal/qj;->a(Lcom/google/android/gms/common/api/BaseImplementation$b;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;)V

    return-void
.end method

.method public registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/qj;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/qj;->registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method

.method public unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/qj;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient;->aBT:Lcom/google/android/gms/internal/qj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/qj;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    return-void
.end method
