.class public final Lcom/google/android/gms/car/Car;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/Car$b;,
        Lcom/google/android/gms/car/Car$a;,
        Lcom/google/android/gms/car/Car$CarFirstPartyApi;,
        Lcom/google/android/gms/car/Car$CarApi;,
        Lcom/google/android/gms/car/Car$CarOptions;,
        Lcom/google/android/gms/car/Car$CarActivityStartListener;,
        Lcom/google/android/gms/car/Car$CarConnectionListener;
    }
.end annotation


# static fields
.field public static final API:Lcom/google/android/gms/common/api/Api;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api",
            "<",
            "Lcom/google/android/gms/car/Car$CarOptions;",
            ">;"
        }
    .end annotation
.end field

.field private static final CLIENT_BUILDER:Lcom/google/android/gms/common/api/Api$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$b",
            "<",
            "Lcom/google/android/gms/car/e;",
            "Lcom/google/android/gms/car/Car$CarOptions;",
            ">;"
        }
    .end annotation
.end field

.field static final CLIENT_KEY:Lcom/google/android/gms/common/api/Api$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$c",
            "<",
            "Lcom/google/android/gms/car/e;",
            ">;"
        }
    .end annotation
.end field

.field public static final CarApi:Lcom/google/android/gms/car/Car$CarApi;

.field public static final CarFirstPartyApi:Lcom/google/android/gms/car/Car$CarFirstPartyApi;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/gms/common/api/Api$c;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/Api$c;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/Car;->CLIENT_KEY:Lcom/google/android/gms/common/api/Api$c;

    new-instance v0, Lcom/google/android/gms/car/Car$1;

    invoke-direct {v0}, Lcom/google/android/gms/car/Car$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/Car;->CLIENT_BUILDER:Lcom/google/android/gms/common/api/Api$b;

    new-instance v0, Lcom/google/android/gms/common/api/Api;

    sget-object v1, Lcom/google/android/gms/car/Car;->CLIENT_BUILDER:Lcom/google/android/gms/common/api/Api$b;

    sget-object v2, Lcom/google/android/gms/car/Car;->CLIENT_KEY:Lcom/google/android/gms/common/api/Api$c;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Api;-><init>(Lcom/google/android/gms/common/api/Api$b;Lcom/google/android/gms/common/api/Api$c;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/car/Car;->API:Lcom/google/android/gms/common/api/Api;

    new-instance v0, Lcom/google/android/gms/car/Car$a;

    invoke-direct {v0, v4}, Lcom/google/android/gms/car/Car$a;-><init>(Lcom/google/android/gms/car/Car$1;)V

    sput-object v0, Lcom/google/android/gms/car/Car;->CarApi:Lcom/google/android/gms/car/Car$CarApi;

    new-instance v0, Lcom/google/android/gms/car/Car$b;

    invoke-direct {v0, v4}, Lcom/google/android/gms/car/Car$b;-><init>(Lcom/google/android/gms/car/Car$1;)V

    sput-object v0, Lcom/google/android/gms/car/Car;->CarFirstPartyApi:Lcom/google/android/gms/car/Car$CarFirstPartyApi;

    return-void
.end method

.method private static b(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "GoogleApiClient is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static buildGoogleApiClientForCar(Landroid/content/Context;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;Lcom/google/android/gms/car/Car$CarConnectionListener;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connectionCallbacks"    # Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
    .param p2, "connectionFailedListener"    # Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;
    .param p3, "carConnectionListener"    # Lcom/google/android/gms/car/Car$CarConnectionListener;

    .prologue
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/car/Car;->API:Lcom/google/android/gms/common/api/Api;

    invoke-static {p3}, Lcom/google/android/gms/car/Car$CarOptions;->builder(Lcom/google/android/gms/car/Car$CarConnectionListener;)Lcom/google/android/gms/car/Car$CarOptions$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/car/Car$CarOptions$Builder;->build()Lcom/google/android/gms/car/Car$CarOptions;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 0

    invoke-static {p0}, Lcom/google/android/gms/car/Car;->b(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    return-void
.end method
