.class final Lcom/google/android/gms/car/support/j$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/support/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic NM:Lcom/google/android/gms/car/support/j;

.field mCanRemove:Z

.field mIndex:I

.field final mOffset:I

.field mSize:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/j;I)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/car/support/j$a;->NM:Lcom/google/android/gms/car/support/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/j$a;->mCanRemove:Z

    iput p2, p0, Lcom/google/android/gms/car/support/j$a;->mOffset:I

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/j;->colGetSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/j$a;->mSize:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    iget v0, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    iget v1, p0, Lcom/google/android/gms/car/support/j$a;->mSize:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/car/support/j$a;->NM:Lcom/google/android/gms/car/support/j;

    iget v1, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    iget v2, p0, Lcom/google/android/gms/car/support/j$a;->mOffset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/j;->colGetEntry(II)Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/j$a;->mCanRemove:Z

    return-object v0
.end method

.method public remove()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/j$a;->mCanRemove:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    iget v0, p0, Lcom/google/android/gms/car/support/j$a;->mSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/j$a;->mSize:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/j$a;->mCanRemove:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/j$a;->NM:Lcom/google/android/gms/car/support/j;

    iget v1, p0, Lcom/google/android/gms/car/support/j$a;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/j;->colRemoveAt(I)V

    return-void
.end method
