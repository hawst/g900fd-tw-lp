.class Lcom/google/android/gms/car/CarMediaManager$a;
.super Lcom/google/android/gms/car/ao$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarMediaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic LA:Lcom/google/android/gms/car/CarMediaManager;


# virtual methods
.method public f(Ljava/lang/String;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager$a;->LA:Lcom/google/android/gms/car/CarMediaManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarMediaManager;->b(Lcom/google/android/gms/car/CarMediaManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarMediaManager$a;->LA:Lcom/google/android/gms/car/CarMediaManager;

    invoke-static {v1}, Lcom/google/android/gms/car/CarMediaManager;->b(Lcom/google/android/gms/car/CarMediaManager;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onGetNode(Ljava/lang/String;IZ)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "albumArt"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager$a;->LA:Lcom/google/android/gms/car/CarMediaManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarMediaManager;->b(Lcom/google/android/gms/car/CarMediaManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager$a;->LA:Lcom/google/android/gms/car/CarMediaManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarMediaManager;->b(Lcom/google/android/gms/car/CarMediaManager;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, p2, v0, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
