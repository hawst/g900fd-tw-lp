.class public abstract Lcom/google/android/gms/internal/xa;
.super Ljava/lang/Object;


# instance fields
.field protected volatile aYF:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/xa;->aYF:I

    return-void
.end method

.method public static final toByteArray(Lcom/google/android/gms/internal/xa;[BII)V
    .locals 3
    .param p0, "msg"    # Lcom/google/android/gms/internal/xa;
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/internal/wt;->b([BII)Lcom/google/android/gms/internal/wt;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/xa;->writeTo(Lcom/google/android/gms/internal/wt;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/wt;->vA()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final toByteArray(Lcom/google/android/gms/internal/xa;)[B
    .locals 3
    .param p0, "msg"    # Lcom/google/android/gms/internal/xa;

    .prologue
    invoke-virtual {p0}, Lcom/google/android/gms/internal/xa;->getSerializedSize()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/internal/xa;->toByteArray(Lcom/google/android/gms/internal/xa;[BII)V

    return-object v0
.end method


# virtual methods
.method protected c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/xa;->aYF:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/xa;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/xa;->aYF:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/xa;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/xa;->aYF:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/internal/xb;->f(Lcom/google/android/gms/internal/xa;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/wt;)V
    .locals 0
    .param p1, "output"    # Lcom/google/android/gms/internal/wt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    return-void
.end method
