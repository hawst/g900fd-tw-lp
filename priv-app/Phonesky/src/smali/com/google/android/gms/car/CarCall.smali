.class public Lcom/google/android/gms/car/CarCall;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarCall$Details;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public cannedTextResponses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public details:Lcom/google/android/gms/car/CarCall$Details;

.field public hasChildren:Z

.field public final id:I

.field final mVersionCode:I

.field public parent:Lcom/google/android/gms/car/CarCall;

.field public remainingPostDialSequence:Ljava/lang/String;

.field public state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/b;

    invoke-direct {v0}, Lcom/google/android/gms/car/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILcom/google/android/gms/car/CarCall;Ljava/util/List;Ljava/lang/String;ILcom/google/android/gms/car/CarCall$Details;Z)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "id"    # I
    .param p3, "parent"    # Lcom/google/android/gms/car/CarCall;
    .param p5, "remainingPostDialSequence"    # Ljava/lang/String;
    .param p6, "state"    # I
    .param p7, "details"    # Lcom/google/android/gms/car/CarCall$Details;
    .param p8, "hasChildren"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/gms/car/CarCall$Details;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p4, "cannedTextResponses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarCall;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/car/CarCall;->id:I

    iput-object p3, p0, Lcom/google/android/gms/car/CarCall;->parent:Lcom/google/android/gms/car/CarCall;

    iput-object p4, p0, Lcom/google/android/gms/car/CarCall;->cannedTextResponses:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/gms/car/CarCall;->remainingPostDialSequence:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/gms/car/CarCall;->state:I

    iput-object p7, p0, Lcom/google/android/gms/car/CarCall;->details:Lcom/google/android/gms/car/CarCall$Details;

    iput-boolean p8, p0, Lcom/google/android/gms/car/CarCall;->hasChildren:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    instance-of v0, p1, Lcom/google/android/gms/car/CarCall;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/CarCall;->id:I

    check-cast p1, Lcom/google/android/gms/car/CarCall;

    .end local p1    # "o":Ljava/lang/Object;
    iget v1, p1, Lcom/google/android/gms/car/CarCall;->id:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarCall;->mVersionCode:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarCall;->id:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CarCall{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->parent:Lcom/google/android/gms/car/CarCall;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cannedTextResponses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->cannedTextResponses:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainingPostDialSequence=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->remainingPostDialSequence:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->details:Lcom/google/android/gms/car/CarCall$Details;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasChildren="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/CarCall;->hasChildren:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/b;->a(Lcom/google/android/gms/car/CarCall;Landroid/os/Parcel;I)V

    return-void
.end method
