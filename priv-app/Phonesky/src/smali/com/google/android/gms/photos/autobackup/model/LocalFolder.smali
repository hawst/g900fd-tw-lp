.class public Lcom/google/android/gms/photos/autobackup/model/LocalFolder;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/photos/autobackup/model/LocalFolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private aGD:Z

.field private aGZ:Ljava/lang/String;

.field private aHa:Ljava/lang/String;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/c;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "folderName"    # Ljava/lang/String;
    .param p3, "bucketId"    # Ljava/lang/String;
    .param p4, "enabled"    # Z

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGZ:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aHa:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGD:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getBucketId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aHa:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGZ:Ljava/lang/String;

    return-object v0
.end method

.method public isAutoBackupEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGD:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->aGD:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/model/c;->a(Lcom/google/android/gms/photos/autobackup/model/LocalFolder;Landroid/os/Parcel;I)V

    return-void
.end method
