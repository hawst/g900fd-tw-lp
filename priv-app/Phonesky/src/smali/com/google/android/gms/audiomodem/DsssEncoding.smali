.class public Lcom/google/android/gms/audiomodem/DsssEncoding;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/audiomodem/DsssEncoding;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final FA:I

.field private final FB:I

.field private final FC:F

.field private final FD:I

.field private final FE:F

.field private final FF:I

.field private final FG:I

.field private final FH:I

.field private final FI:I

.field private final Fx:I

.field private final Fy:Z

.field private final Fz:Z

.field private final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/audiomodem/b;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/DsssEncoding;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIZZIIFIFIIII)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "tokenLengthBytes"    # I
    .param p3, "shouldIncludeParitySymbol"    # Z
    .param p4, "shouldUseSingleSideband"    # Z
    .param p5, "numberOfTapsLfsr"    # I
    .param p6, "codeNumber"    # I
    .param p7, "coderSampleRate"    # F
    .param p8, "upsamplingFactor"    # I
    .param p9, "desiredCarrierFrequency"    # F
    .param p10, "bitsPerSymbol"    # I
    .param p11, "minCyclesPerFrame"    # I
    .param p12, "basebandDecimationFactor"    # I
    .param p13, "numCrcCheckBytes"    # I

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fx:I

    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fy:Z

    iput-boolean p4, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fz:Z

    iput p5, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FA:I

    iput p6, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FB:I

    iput p7, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FC:F

    iput p8, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FD:I

    iput p9, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FE:F

    iput p10, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FF:I

    iput p11, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FG:I

    iput p12, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FH:I

    iput p13, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FI:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/audiomodem/DsssEncoding;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->mVersionCode:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getVersionCode()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fx:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getTokenLengthBytes()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fy:Z

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->shouldIncludeParitySymbol()Z

    move-result v3

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fz:Z

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->shouldUseSingleSideband()Z

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FA:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getNumberOfTapsLfsr()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FB:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getCodeNumber()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FC:F

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getCoderSampleRate()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FD:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getUpsamplingFactor()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FE:F

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getDesiredCarrierFrequency()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FF:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getBitsPerSymbol()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FG:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getMinCyclesPerFrame()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FH:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getBasebandDecimationFactor()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FI:I

    invoke-virtual {p1}, Lcom/google/android/gms/audiomodem/DsssEncoding;->getNumCrcCheckBytes()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getBasebandDecimationFactor()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FH:I

    return v0
.end method

.method public getBitsPerSymbol()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FF:I

    return v0
.end method

.method public getCodeNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FB:I

    return v0
.end method

.method public getCoderSampleRate()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FC:F

    return v0
.end method

.method public getDesiredCarrierFrequency()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FE:F

    return v0
.end method

.method public getMinCyclesPerFrame()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FG:I

    return v0
.end method

.method public getNumCrcCheckBytes()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FI:I

    return v0
.end method

.method public getNumberOfTapsLfsr()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FA:I

    return v0
.end method

.method public getTokenLengthBytes()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fx:I

    return v0
.end method

.method public getUpsamplingFactor()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FD:I

    return v0
.end method

.method getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->mVersionCode:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->mVersionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fx:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fy:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fz:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FA:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FB:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FC:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FD:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FE:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FF:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FG:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->FI:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public shouldIncludeParitySymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fy:Z

    return v0
.end method

.method public shouldUseSingleSideband()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/DsssEncoding;->Fz:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/audiomodem/b;->a(Lcom/google/android/gms/audiomodem/DsssEncoding;Landroid/os/Parcel;I)V

    return-void
.end method
