.class public Lcom/google/android/gms/car/CarMediaBrowserSourceNode;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarMediaBrowserSourceNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public lists:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

.field final mVersionCode:I

.field public mediaSource:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

.field public start:I

.field public total:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/n;

    invoke-direct {v0}, Lcom/google/android/gms/car/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->mVersionCode:I

    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->mediaSource:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;II[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "mediaSource"    # Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;
    .param p3, "start"    # I
    .param p4, "total"    # I
    .param p5, "lists"    # [Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->mediaSource:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    iput p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->start:I

    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->total:I

    iput-object p5, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->lists:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/n;->a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode;Landroid/os/Parcel;I)V

    return-void
.end method
