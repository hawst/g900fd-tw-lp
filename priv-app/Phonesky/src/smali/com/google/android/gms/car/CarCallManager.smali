.class public Lcom/google/android/gms/car/CarCallManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarCallManager$a;,
        Lcom/google/android/gms/car/CarCallManager$b;
    }
.end annotation


# instance fields
.field private final KA:Lcom/google/android/gms/car/av;

.field private KB:Lcom/google/android/gms/car/CarCallManager$a;

.field private final KC:Lcom/google/android/gms/car/CarCallManager$b;

.field private final KD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCallListener;",
            ">;"
        }
    .end annotation
.end field

.field private final Kz:Lcom/google/android/gms/car/ak;


# direct methods
.method static synthetic a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager;->KD:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public handleCarDisconnection()V
    .locals 2

    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "handleCarDisconnection."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager;->KA:Lcom/google/android/gms/car/av;

    iget-object v1, p0, Lcom/google/android/gms/car/CarCallManager;->KC:Lcom/google/android/gms/car/CarCallManager$b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/av;->b(Lcom/google/android/gms/car/aw;)Z

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager;->Kz:Lcom/google/android/gms/car/ak;

    iget-object v1, p0, Lcom/google/android/gms/car/CarCallManager;->KB:Lcom/google/android/gms/car/CarCallManager$a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/ak;->b(Lcom/google/android/gms/car/al;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
