.class public Lcom/google/android/gms/location/copresence/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/g;


# instance fields
.field private final aum:Z

.field private final aun:Z

.field private auo:[Lcom/google/android/gms/internal/my;

.field private aup:[Lcom/google/android/gms/internal/mw;

.field private final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/copresence/g;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/f;->CREATOR:Lcom/google/android/gms/location/copresence/g;

    return-void
.end method

.method constructor <init>(IZZ[Lcom/google/android/gms/internal/my;[Lcom/google/android/gms/internal/mw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    iput-boolean p2, p0, Lcom/google/android/gms/location/copresence/f;->aum:Z

    iput-boolean p3, p0, Lcom/google/android/gms/location/copresence/f;->aun:Z

    iput-object p4, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    iput-object p5, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 7

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/gms/location/copresence/f;

    iget v1, p0, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/f;->aum:Z

    iget-boolean v3, p0, Lcom/google/android/gms/location/copresence/f;->aun:Z

    iget-object v4, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    if-nez v4, :cond_0

    move-object v4, v5

    :goto_0
    iget-object v6, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    if-nez v6, :cond_1

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/f;-><init>(IZZ[Lcom/google/android/gms/internal/my;[Lcom/google/android/gms/internal/mw;)V

    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    invoke-virtual {v4}, [Lcom/google/android/gms/internal/my;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/internal/my;

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    invoke-virtual {v5}, [Lcom/google/android/gms/internal/mw;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/android/gms/internal/mw;

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/copresence/f;->CREATOR:Lcom/google/android/gms/location/copresence/g;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/f;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p0, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/f;->aum:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/f;->aum:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/f;->aun:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/f;->aun:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/location/copresence/f;->mVersionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/f;->aun:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/f;->aum:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/f;->aum:Z

    return v0
.end method

.method public oU()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/f;->aun:Z

    return v0
.end method

.method public oV()[Lcom/google/android/gms/internal/my;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/f;->auo:[Lcom/google/android/gms/internal/my;

    invoke-virtual {v0}, [Lcom/google/android/gms/internal/my;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/my;

    goto :goto_0
.end method

.method public oW()[Lcom/google/android/gms/internal/mw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/f;->aup:[Lcom/google/android/gms/internal/mw;

    invoke-virtual {v0}, [Lcom/google/android/gms/internal/mw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/mw;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/location/copresence/f;->CREATOR:Lcom/google/android/gms/location/copresence/g;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/g;->a(Lcom/google/android/gms/location/copresence/f;Landroid/os/Parcel;I)V

    return-void
.end method
