.class public Lcom/google/android/gms/car/CarMediaBrowserSongNode;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarMediaBrowserSongNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public albumArt:[B

.field public durationSeconds:I

.field final mVersionCode:I

.field public song:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/m;

    invoke-direct {v0}, Lcom/google/android/gms/car/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->mVersionCode:I

    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->song:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;[BI)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "song"    # Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;
    .param p3, "albumArt"    # [B
    .param p4, "durationSeconds"    # I

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->song:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->albumArt:[B

    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->durationSeconds:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/m;->a(Lcom/google/android/gms/car/CarMediaBrowserSongNode;Landroid/os/Parcel;I)V

    return-void
.end method
