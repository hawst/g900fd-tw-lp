.class public Lcom/google/android/gms/car/CarMediaManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarMediaManager$b;,
        Lcom/google/android/gms/car/CarMediaManager$a;,
        Lcom/google/android/gms/car/CarMediaManager$CarMediaListener;
    }
.end annotation


# instance fields
.field private final Lv:Lcom/google/android/gms/car/an;

.field private final Lw:Lcom/google/android/gms/car/CarMediaManager$a;

.field private Lx:Lcom/google/android/gms/car/CarMediaManager$CarMediaListener;

.field private final Ly:Lcom/google/android/gms/car/ap;

.field private final Lz:Lcom/google/android/gms/car/CarMediaManager$b;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static synthetic b(Lcom/google/android/gms/car/CarMediaManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method handleCarDisconnection()V
    .locals 2

    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.MEDIA"

    const-string v1, "handleCarDisconnection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarMediaManager;->unregisterListener()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager;->Lv:Lcom/google/android/gms/car/an;

    iget-object v1, p0, Lcom/google/android/gms/car/CarMediaManager;->Lw:Lcom/google/android/gms/car/CarMediaManager$a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/an;->b(Lcom/google/android/gms/car/ao;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarMediaManager;->Ly:Lcom/google/android/gms/car/ap;

    iget-object v1, p0, Lcom/google/android/gms/car/CarMediaManager;->Lz:Lcom/google/android/gms/car/CarMediaManager$b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/ap;->b(Lcom/google/android/gms/car/aq;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public unregisterListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaManager;->Lx:Lcom/google/android/gms/car/CarMediaManager$CarMediaListener;

    return-void
.end method
