.class public Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/people/PeopleClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoadPeopleOptions"
.end annotation


# static fields
.field public static final DEFAULT:Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;


# instance fields
.field private WX:Ljava/lang/String;

.field private aBD:I

.field private aBF:I

.field private aBM:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aBN:J

.field private aBy:Z

.field private aBz:I

.field private xM:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    invoke-direct {v0}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->DEFAULT:Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x1fffff

    iput v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBz:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBD:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBF:I

    return-void
.end method


# virtual methods
.method public clone()Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 4

    new-instance v0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    invoke-direct {v0}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->WX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setCircleId(Ljava/lang/String;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBM:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setQualifiedIds(Ljava/util/Collection;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBz:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setProjection(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBy:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setPeopleOnly(Z)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBN:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setChangedSince(J)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->xM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setQuery(Ljava/lang/String;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBD:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setSearchFields(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBF:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setSortOrder(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->clone()Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    move-result-object v0

    return-object v0
.end method

.method public getChangedSince()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBN:J

    return-wide v0
.end method

.method public getCircleId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->WX:Ljava/lang/String;

    return-object v0
.end method

.method public getProjection()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBz:I

    return v0
.end method

.method public getQualifiedIds()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBM:Ljava/util/Collection;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->xM:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFields()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBD:I

    return v0
.end method

.method public getSortOrder()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBF:I

    return v0
.end method

.method public isPeopleOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBy:Z

    return v0
.end method

.method public setChangedSince(J)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 1
    .param p1, "changedSince"    # J

    .prologue
    iput-wide p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBN:J

    return-object p0
.end method

.method public setCircleId(Ljava/lang/String;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "circleId"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->WX:Ljava/lang/String;

    return-object p0
.end method

.method public setPeopleOnly(Z)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "peopleOnly"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBy:Z

    return-object p0
.end method

.method public setProjection(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "projection"    # I

    .prologue
    iput p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBz:I

    return-object p0
.end method

.method public setQualifiedIds(Ljava/util/Collection;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;"
        }
    .end annotation

    .prologue
    .local p1, "qualifiedIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBM:Ljava/util/Collection;

    return-object p0
.end method

.method public setQuery(Ljava/lang/String;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->xM:Ljava/lang/String;

    return-object p0
.end method

.method public setSearchFields(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "searchFields"    # I

    .prologue
    iput p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBD:I

    return-object p0
.end method

.method public setSortOrder(I)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    .locals 0
    .param p1, "sortOrder"    # I

    .prologue
    iput p1, p0, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->aBF:I

    return-object p0
.end method
