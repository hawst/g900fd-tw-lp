.class public Lcom/google/android/gms/internal/du;
.super Lcom/google/android/gms/internal/ec$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/du$b;,
        Lcom/google/android/gms/internal/du$c;,
        Lcom/google/android/gms/internal/du$a;
    }
.end annotation


# static fields
.field private static final rL:I


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mr:Lcom/google/android/gms/internal/gz;

.field private rM:Lcom/google/android/gms/internal/dw;

.field private rN:Lcom/google/android/gms/internal/dy;

.field private rO:Lcom/google/android/gms/internal/du$c;

.field private rP:Lcom/google/android/gms/internal/dz;

.field private rQ:Z

.field private rR:Z

.field private rS:Landroid/widget/FrameLayout;

.field private rT:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private rU:Z

.field private rV:Z

.field private rW:Z

.field private rX:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/du;->rL:I

    return-void
.end method

.method private static a(IIII)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p0, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/dw;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.ads.AdActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.ads.internal.overlay.useClientJar"

    iget-object v2, p1, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/gx;->wU:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/dw;->a(Landroid/content/Intent;Lcom/google/android/gms/internal/dw;)V

    invoke-static {}, Lcom/google/android/gms/common/util/m;->jl()Z

    move-result v1

    if-nez v1, :cond_0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_1

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public X()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rQ:Z

    return-void
.end method

.method public a(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1, v2, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->X()V

    iput-object p2, p0, Lcom/google/android/gms/internal/du;->rT:Landroid/webkit/WebChromeClient$CustomViewCallback;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rR:Z

    return-void
.end method

.method public b(IIII)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/internal/du;->a(IIII)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/dy;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public c(IIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/dy;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/dy;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gz;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/internal/du;->a(IIII)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dB()Lcom/google/android/gms/internal/ha;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/ha;->A(Z)V

    :cond_0
    return-void
.end method

.method public cc()Lcom/google/android/gms/internal/dy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    return-object v0
.end method

.method public cd()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rR:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->orientation:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/du;->setRequestedOrientation(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->X()V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v2, p0, Lcom/google/android/gms/internal/du;->rS:Landroid/widget/FrameLayout;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rT:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rT:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    iput-object v2, p0, Lcom/google/android/gms/internal/du;->rT:Landroid/webkit/WebChromeClient$CustomViewCallback;

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rR:Z

    return-void
.end method

.method public ce()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/du;->p(Z)V

    return-void
.end method

.method cf()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rV:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rV:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->ch()V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->z(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    iget-object v0, v0, Lcom/google/android/gms/internal/du$c;->sa:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    iget v2, v2, Lcom/google/android/gms/internal/du$c;->index:I

    iget-object v3, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    iget-object v3, v3, Lcom/google/android/gms/internal/du$c;->rZ:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    invoke-interface {v0}, Lcom/google/android/gms/internal/dx;->ag()V

    goto :goto_0
.end method

.method cg()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->cg()V

    return-void
.end method

.method ch()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->ch()V

    return-void
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.google.android.gms.ads.internal.overlay.hasResumed"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rU:Z

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/dw;->b(Landroid/content/Intent;)Lcom/google/android/gms/internal/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/du$a;

    const-string v1, "Could not get info for ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/du$a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/internal/du$a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/du$a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gw;->w(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ad;->ml:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rW:Z

    :goto_1
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    invoke-interface {v0}, Lcom/google/android/gms/internal/dx;->ah()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->sk:I

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    invoke-interface {v0}, Lcom/google/android/gms/internal/y;->onAdClicked()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->sk:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/gms/internal/du$a;

    const-string v1, "Could not determine ad overlay type."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/du$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rW:Z

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/du;->r(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/gms/internal/du$c;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/du$c;-><init>(Lcom/google/android/gms/internal/gz;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/du;->r(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/du;->r(Z)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rU:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v2, v2, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/dr;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dt;Lcom/google/android/gms/internal/ea;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Lcom/google/android/gms/internal/du$a; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dy;->destroy()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->cf()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rN:Lcom/google/android/gms/internal/dy;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dy;->pause()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->cd()V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rO:Lcom/google/android/gms/internal/du$c;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/gn;->a(Landroid/webkit/WebView;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->cf()V

    return-void
.end method

.method public onRestart()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget v0, v0, Lcom/google/android/gms/internal/dw;->sk:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rU:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/internal/gn;->b(Landroid/webkit/WebView;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/du;->rU:Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outBundle"    # Landroid/os/Bundle;

    .prologue
    const-string v0, "com.google.android.gms.ads.internal.overlay.hasResumed"

    iget-boolean v1, p0, Lcom/google/android/gms/internal/du;->rU:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->cf()V

    return-void
.end method

.method public p(Z)V
    .locals 4

    const/4 v3, -0x2

    if-eqz p1, :cond_0

    const/16 v0, 0x32

    :goto_0
    new-instance v1, Lcom/google/android/gms/internal/dz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/internal/dz;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz p1, :cond_1

    const/16 v0, 0xb

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/dw;->sh:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/dz;->q(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    :cond_1
    const/16 v0, 0x9

    goto :goto_1
.end method

.method public q(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rP:Lcom/google/android/gms/internal/dz;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/dz;->q(Z)V

    :cond_0
    return-void
.end method

.method r(Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/du$a;
        }
    .end annotation

    const/16 v3, 0x400

    const/4 v13, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rQ:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/du;->rW:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/ad;->mv:Z

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget v1, v1, Lcom/google/android/gms/internal/dw;->orientation:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/du;->setRequestedOrientation(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_3

    const-string v1, "Enabling hardware acceleration on the AdActivity window."

    invoke-static {v1}, Lcom/google/android/gms/internal/gw;->d(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/internal/gr;->a(Landroid/view/Window;)V

    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/du$b;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v3, v3, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/du$b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rW:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->X()V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dB()Lcom/google/android/gms/internal/ha;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ha;->dL()Z

    move-result v3

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->ac()Lcom/google/android/gms/internal/bd;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v5, v5, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/gz;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bd;ZZLcom/google/android/gms/internal/k;Lcom/google/android/gms/internal/gx;)Lcom/google/android/gms/internal/gz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dB()Lcom/google/android/gms/internal/ha;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v8, v0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v9, v0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v11, v0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dB()Lcom/google/android/gms/internal/ha;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ha;->dK()Lcom/google/android/gms/internal/aa;

    move-result-object v12

    move-object v6, v4

    move-object v7, v4

    move v10, v2

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/gms/internal/ha;->a(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/dx;Lcom/google/android/gms/internal/cg;Lcom/google/android/gms/internal/ea;ZLcom/google/android/gms/internal/cj;Lcom/google/android/gms/internal/aa;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dB()Lcom/google/android/gms/internal/ha;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/du$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/du$1;-><init>(Lcom/google/android/gms/internal/du;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ha;->a(Lcom/google/android/gms/internal/ha$a;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v1, v1, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->loadUrl(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/gz;->a(Lcom/google/android/gms/internal/du;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/internal/du;->rW:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    sget v1, Lcom/google/android/gms/internal/du;->rL:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->setBackgroundColor(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v1, v13, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;II)V

    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/internal/du;->cg()V

    :cond_6
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/du;->p(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gz;->dC()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/du;->q(Z)V

    :cond_7
    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rX:Landroid/widget/RelativeLayout;

    sget v1, Lcom/google/android/gms/internal/du;->rL:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v5, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v6, v0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v7, v0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    const-string v8, "text/html"

    const-string v9, "UTF-8"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/internal/gz;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    new-instance v0, Lcom/google/android/gms/internal/du$a;

    const-string v1, "No URL or HTML to display in ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/du$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->rM:Lcom/google/android/gms/internal/dw;

    iget-object v0, v0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    iput-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->setContext(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public setRequestedOrientation(I)V
    .locals 1
    .param p1, "requestedOrientation"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/internal/du;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method
