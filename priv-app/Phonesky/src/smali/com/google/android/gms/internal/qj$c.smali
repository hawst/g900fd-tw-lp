.class final Lcom/google/android/gms/internal/qj$c;
.super Lcom/google/android/gms/common/internal/h$b;

# interfaces
.implements Lcom/google/android/gms/people/Graph$LoadCirclesResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/qj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/h",
        "<",
        "Lcom/google/android/gms/internal/qc;",
        ">.b<",
        "Lcom/google/android/gms/common/api/BaseImplementation$b",
        "<",
        "Lcom/google/android/gms/people/Graph$LoadCirclesResult;",
        ">;>;",
        "Lcom/google/android/gms/people/Graph$LoadCirclesResult;"
    }
.end annotation


# instance fields
.field private final EU:Lcom/google/android/gms/common/api/Status;

.field final synthetic aCN:Lcom/google/android/gms/internal/qj;

.field private final aCO:Lcom/google/android/gms/people/model/CircleBuffer;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/qj;Lcom/google/android/gms/common/api/BaseImplementation$b;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/CircleBuffer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/BaseImplementation$b",
            "<",
            "Lcom/google/android/gms/people/Graph$LoadCirclesResult;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Lcom/google/android/gms/people/model/CircleBuffer;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/internal/qj$c;->aCN:Lcom/google/android/gms/internal/qj;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/h$b;-><init>(Lcom/google/android/gms/common/internal/h;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/qj$c;->EU:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lcom/google/android/gms/internal/qj$c;->aCO:Lcom/google/android/gms/people/model/CircleBuffer;

    return-void
.end method


# virtual methods
.method public getCircles()Lcom/google/android/gms/people/model/CircleBuffer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qj$c;->aCO:Lcom/google/android/gms/people/model/CircleBuffer;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qj$c;->EU:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected synthetic i(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/api/BaseImplementation$b;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/qj$c;->p(Lcom/google/android/gms/common/api/BaseImplementation$b;)V

    return-void
.end method

.method protected iJ()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/qj$c;->release()V

    return-void
.end method

.method protected p(Lcom/google/android/gms/common/api/BaseImplementation$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/BaseImplementation$b",
            "<",
            "Lcom/google/android/gms/people/Graph$LoadCirclesResult;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/BaseImplementation$b;->d(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/qj$c;->aCO:Lcom/google/android/gms/people/model/CircleBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/qj$c;->aCO:Lcom/google/android/gms/people/model/CircleBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/CircleBuffer;->close()V

    :cond_0
    return-void
.end method
