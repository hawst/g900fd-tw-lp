.class public Lcom/google/android/gms/reminders/model/Location$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/reminders/model/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private aLe:Ljava/lang/Double;

.field private aLf:Ljava/lang/Double;

.field private aLg:Ljava/lang/Integer;

.field private aLh:Ljava/lang/Integer;

.field private aLi:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/reminders/model/Location;
    .locals 8

    new-instance v0, Lcom/google/android/gms/reminders/model/d;

    iget-object v1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLe:Ljava/lang/Double;

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLf:Ljava/lang/Double;

    iget-object v3, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLg:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLh:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLi:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/reminders/model/d;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setDisplayAddress(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "displayAddress"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLi:Ljava/lang/String;

    return-object p0
.end method

.method public setLat(Ljava/lang/Double;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "lat"    # Ljava/lang/Double;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLe:Ljava/lang/Double;

    return-object p0
.end method

.method public setLng(Ljava/lang/Double;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "lng"    # Ljava/lang/Double;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLf:Ljava/lang/Double;

    return-object p0
.end method

.method public setLocationType(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "locationType"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLh:Ljava/lang/Integer;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->mName:Ljava/lang/String;

    return-object p0
.end method

.method public setRadiusMeters(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/Location$Builder;
    .locals 0
    .param p1, "radiusMeters"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/Location$Builder;->aLg:Ljava/lang/Integer;

    return-object p0
.end method
