.class final Lcom/google/android/gms/car/ExceptionParcel$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/ExceptionParcel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/car/ExceptionParcel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bA(Landroid/os/Parcel;)Lcom/google/android/gms/car/ExceptionParcel;
    .locals 2

    :try_start_0
    new-instance v1, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/ExceptionParcel;-><init>(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    new-instance v0, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ExceptionParcel;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/ExceptionParcel$1;->bA(Landroid/os/Parcel;)Lcom/google/android/gms/car/ExceptionParcel;

    move-result-object v0

    return-object v0
.end method

.method public cs(I)[Lcom/google/android/gms/car/ExceptionParcel;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/car/ExceptionParcel;

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/ExceptionParcel$1;->cs(I)[Lcom/google/android/gms/car/ExceptionParcel;

    move-result-object v0

    return-object v0
.end method
