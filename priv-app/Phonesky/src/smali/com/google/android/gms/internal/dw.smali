.class public final Lcom/google/android/gms/internal/dw;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/dv;


# instance fields
.field public final lR:Lcom/google/android/gms/internal/gx;

.field public final orientation:I

.field public final sb:Lcom/google/android/gms/internal/dt;

.field public final sc:Lcom/google/android/gms/internal/y;

.field public final sd:Lcom/google/android/gms/internal/dx;

.field public final se:Lcom/google/android/gms/internal/gz;

.field public final sf:Lcom/google/android/gms/internal/cg;

.field public final sg:Ljava/lang/String;

.field public final sh:Z

.field public final si:Ljava/lang/String;

.field public final sj:Lcom/google/android/gms/internal/ea;

.field public final sk:I

.field public final sl:Lcom/google/android/gms/internal/cj;

.field public final sm:Ljava/lang/String;

.field public final sn:Lcom/google/android/gms/internal/ad;

.field public final url:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/dv;

    invoke-direct {v0}, Lcom/google/android/gms/internal/dv;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/dw;->CREATOR:Lcom/google/android/gms/internal/dv;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/internal/dt;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/gx;Landroid/os/IBinder;Ljava/lang/String;Lcom/google/android/gms/internal/ad;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/dw;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    invoke-static {p3}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/y;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/dx;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    invoke-static {p5}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/gz;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-static {p6}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/cg;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iput-object p7, p0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/dw;->sh:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/ea;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    iput p11, p0, Lcom/google/android/gms/internal/dw;->orientation:I

    iput p12, p0, Lcom/google/android/gms/internal/dw;->sk:I

    iput-object p13, p0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    invoke-static/range {p15 .. p15}, Lcom/google/android/gms/dynamic/d$a;->bG(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/cj;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/dt;Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/dx;Lcom/google/android/gms/internal/ea;Lcom/google/android/gms/internal/gx;)V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/gms/internal/dw;->versionCode:I

    iput-object p1, p0, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    iput-object p2, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    iput-object p3, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dw;->sh:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/dw;->orientation:I

    iput v2, p0, Lcom/google/android/gms/internal/dw;->sk:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/dx;Lcom/google/android/gms/internal/cg;Lcom/google/android/gms/internal/ea;Lcom/google/android/gms/internal/gz;ZILjava/lang/String;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/cj;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/dw;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    iput-object p1, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    iput-object p2, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    iput-object p5, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    iput-object p3, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/dw;->sh:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    iput p7, p0, Lcom/google/android/gms/internal/dw;->orientation:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/dw;->sk:I

    iput-object p8, p0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    iput-object p10, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/dx;Lcom/google/android/gms/internal/cg;Lcom/google/android/gms/internal/ea;Lcom/google/android/gms/internal/gz;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/cj;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/dw;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    iput-object p1, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    iput-object p2, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    iput-object p5, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    iput-object p3, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iput-object p9, p0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/dw;->sh:Z

    iput-object p8, p0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    iput p7, p0, Lcom/google/android/gms/internal/dw;->orientation:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/dw;->sk:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    iput-object p11, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/dx;Lcom/google/android/gms/internal/ea;Lcom/google/android/gms/internal/gz;ZILcom/google/android/gms/internal/gx;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/dw;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sb:Lcom/google/android/gms/internal/dt;

    iput-object p1, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    iput-object p2, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    iput-object p4, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sg:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/dw;->sh:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->si:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    iput p6, p0, Lcom/google/android/gms/internal/dw;->orientation:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/dw;->sk:I

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->url:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/dw;->lR:Lcom/google/android/gms/internal/gx;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sm:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/internal/dw;->sn:Lcom/google/android/gms/internal/ad;

    return-void
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/gms/internal/dw;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method

.method public static b(Landroid/content/Intent;)Lcom/google/android/gms/internal/dw;
    .locals 2

    :try_start_0
    const-string v0, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/internal/dw;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/dw;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method ci()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->sc:Lcom/google/android/gms/internal/y;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cj()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->sd:Lcom/google/android/gms/internal/dx;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method ck()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->se:Lcom/google/android/gms/internal/gz;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cl()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->sf:Lcom/google/android/gms/internal/cg;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cm()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->sl:Lcom/google/android/gms/internal/cj;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method cn()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dw;->sj:Lcom/google/android/gms/internal/ea;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/dv;->a(Lcom/google/android/gms/internal/dw;Landroid/os/Parcel;I)V

    return-void
.end method
