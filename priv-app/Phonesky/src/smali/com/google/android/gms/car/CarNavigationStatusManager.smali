.class public Lcom/google/android/gms/car/CarNavigationStatusManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarNavigationStatusManager$a;,
        Lcom/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener;
    }
.end annotation


# instance fields
.field private final LI:Lcom/google/android/gms/car/at;

.field private LJ:Lcom/google/android/gms/car/CarNavigationStatusManager$a;

.field private LK:Lcom/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener;

.field private LL:I

.field private LM:I

.field private LN:I

.field private LO:I

.field private LP:I

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static synthetic a(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LL:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LM:I

    return p1
.end method

.method private b(Landroid/os/RemoteException;)V
    .locals 3

    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemoteException from car service:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LN:I

    return p1
.end method

.method static synthetic d(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LO:I

    return p1
.end method

.method static synthetic e(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LP:I

    return p1
.end method

.method static synthetic g(Lcom/google/android/gms/car/CarNavigationStatusManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method handleCarDisconnection()V
    .locals 2

    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SENSOR"

    const-string v1, "handleCarDisconnection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarNavigationStatusManager;->unregisterListener()V

    return-void
.end method

.method public unregisterListener()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LI:Lcom/google/android/gms/car/at;

    iget-object v1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LJ:Lcom/google/android/gms/car/CarNavigationStatusManager$a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/at;->b(Lcom/google/android/gms/car/au;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager;->LK:Lcom/google/android/gms/car/CarNavigationStatusManager$CarNavigationStatusListener;

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/CarNavigationStatusManager;->b(Landroid/os/RemoteException;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
