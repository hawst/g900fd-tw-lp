.class public Lcom/google/android/gms/car/CarMediaBrowserListNode;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/CarMediaBrowserListNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public list:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

.field final mVersionCode:I

.field public songs:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

.field public start:I

.field public total:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/k;

    invoke-direct {v0}, Lcom/google/android/gms/car/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->list:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->mVersionCode:I

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;II[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;)V
    .locals 1
    .param p1, "versionCode"    # I
    .param p2, "list"    # Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;
    .param p3, "start"    # I
    .param p4, "total"    # I
    .param p5, "songs"    # [Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->list:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->list:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iput p3, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->start:I

    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->total:I

    iput-object p5, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->songs:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserListNode;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/k;->a(Lcom/google/android/gms/car/CarMediaBrowserListNode;Landroid/os/Parcel;I)V

    return-void
.end method
