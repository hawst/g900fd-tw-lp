.class public Lcom/google/android/gms/reminders/model/TaskId$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/reminders/model/TaskId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private aLC:Ljava/lang/Long;

.field private aLD:Ljava/lang/String;

.field private aLE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/reminders/model/TaskId;
    .locals 5

    new-instance v0, Lcom/google/android/gms/reminders/model/i;

    iget-object v1, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLC:Ljava/lang/Long;

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLD:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLE:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/reminders/model/i;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setClientAssignedId(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/TaskId$Builder;
    .locals 0
    .param p1, "clientAssignedId"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLD:Ljava/lang/String;

    return-object p0
.end method

.method public setClientAssignedThreadId(Ljava/lang/String;)Lcom/google/android/gms/reminders/model/TaskId$Builder;
    .locals 0
    .param p1, "clientAssignedThreadId"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLE:Ljava/lang/String;

    return-object p0
.end method

.method public setServerAssignedId(Ljava/lang/Long;)Lcom/google/android/gms/reminders/model/TaskId$Builder;
    .locals 0
    .param p1, "serverAssignedId"    # Ljava/lang/Long;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/TaskId$Builder;->aLC:Ljava/lang/Long;

    return-object p0
.end method
