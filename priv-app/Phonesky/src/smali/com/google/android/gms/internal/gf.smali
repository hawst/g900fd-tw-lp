.class public Lcom/google/android/gms/internal/gf;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# static fields
.field private static final vW:Lcom/google/android/gms/internal/gf;

.field public static final vX:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mK:Ljava/lang/Object;

.field private nG:Lcom/google/android/gms/internal/ar;

.field private nH:Lcom/google/android/gms/internal/aq;

.field private nI:Lcom/google/android/gms/internal/fc;

.field private qK:Lcom/google/android/gms/internal/gx;

.field private uV:Z

.field private uW:Z

.field public final vY:Ljava/lang/String;

.field private final vZ:Lcom/google/android/gms/internal/gg;

.field private wb:Ljava/math/BigInteger;

.field private final wc:Ljava/util/HashSet;

.field private final wd:Ljava/util/HashMap;

.field private we:Z

.field private wf:Z

.field private wg:Lcom/google/android/gms/internal/as;

.field private wh:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private wi:Z

.field private wj:Landroid/os/Bundle;

.field private wk:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/gf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gf;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    sget-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    iget-object v0, v0, Lcom/google/android/gms/internal/gf;->vY:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/gf;->vX:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->mK:Ljava/lang/Object;

    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->wb:Ljava/math/BigInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->wc:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->wd:Ljava/util/HashMap;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/gf;->we:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/gf;->uV:Z

    iput-boolean v2, p0, Lcom/google/android/gms/internal/gf;->wf:Z

    iput-boolean v3, p0, Lcom/google/android/gms/internal/gf;->uW:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/gf;->nG:Lcom/google/android/gms/internal/ar;

    iput-object v1, p0, Lcom/google/android/gms/internal/gf;->wg:Lcom/google/android/gms/internal/as;

    iput-object v1, p0, Lcom/google/android/gms/internal/gf;->nH:Lcom/google/android/gms/internal/aq;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->wh:Ljava/util/LinkedList;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/gf;->wi:Z

    invoke-static {}, Lcom/google/android/gms/internal/bs;->by()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->wj:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/google/android/gms/internal/gf;->nI:Lcom/google/android/gms/internal/fc;

    invoke-static {}, Lcom/google/android/gms/internal/gn;->dv()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->vY:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/gg;

    iget-object v1, p0, Lcom/google/android/gms/internal/gf;->vY:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/gg;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/gf;->vZ:Lcom/google/android/gms/internal/gg;

    return-void
.end method

.method public static bL()Landroid/os/Bundle;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gf;->dn()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static c(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/internal/gf;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static dl()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gf;->dm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/gf;->vW:Lcom/google/android/gms/internal/gf;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/gf;->f(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public d(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/gf;->qK:Lcom/google/android/gms/internal/gx;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/gx;->wU:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/gf;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/gf;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getRemoteResource(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1
.end method

.method public dm()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/gf;->mK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/gf;->wk:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public dn()Landroid/os/Bundle;
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/gf;->mK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/gf;->wj:Landroid/os/Bundle;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f(Ljava/lang/Throwable;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/gf;->wf:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fc;

    iget-object v1, p0, Lcom/google/android/gms/internal/gf;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/gf;->qK:Lcom/google/android/gms/internal/gx;

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/internal/fc;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gx;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/fc;->b(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method
