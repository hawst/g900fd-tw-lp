.class final Lcom/google/android/gms/car/support/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/support/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mBreadCrumbShortTitleRes:I

.field final mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

.field final mBreadCrumbTitleRes:I

.field final mBreadCrumbTitleText:Ljava/lang/CharSequence;

.field final mIndex:I

.field final mName:Ljava/lang/String;

.field final mOps:[I

.field final mTransition:I

.field final mTransitionStyle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/support/c$1;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/c$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/c;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->mTransition:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->mTransitionStyle:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->mIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleRes:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleRes:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/car/support/g;)Lcom/google/android/gms/car/support/b;
    .locals 11

    const/4 v10, 0x1

    const/4 v2, 0x0

    new-instance v6, Lcom/google/android/gms/car/support/b;

    invoke-direct {v6, p1}, Lcom/google/android/gms/car/support/b;-><init>(Lcom/google/android/gms/car/support/g;)V

    move v1, v2

    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    array-length v3, v3

    if-ge v0, v3, :cond_4

    new-instance v7, Lcom/google/android/gms/car/support/b$a;

    invoke-direct {v7}, Lcom/google/android/gms/car/support/b$a;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v4, v0, 0x1

    aget v0, v3, v0

    iput v0, v7, Lcom/google/android/gms/car/support/b$a;->cmd:I

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Instantiate "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " op #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " base fragment #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    aget v5, v5, v4

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    if-ltz v0, :cond_2

    iget-object v4, p1, Lcom/google/android/gms/car/support/g;->mActive:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    iput-object v0, v7, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Lcom/google/android/gms/car/support/b$a;->enterAnim:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v3, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Lcom/google/android/gms/car/support/b$a;->exitAnim:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v4, v3, 0x1

    aget v0, v0, v3

    iput v0, v7, Lcom/google/android/gms/car/support/b$a;->popEnterAnim:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v5, v4, 0x1

    aget v0, v0, v4

    iput v0, v7, Lcom/google/android/gms/car/support/b$a;->popExitAnim:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v3, v5, 0x1

    aget v8, v0, v5

    if-lez v8, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v7, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    move v4, v2

    :goto_2
    if-ge v4, v8, :cond_3

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->DEBUG:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Instantiate "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " set remove fragment #"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    aget v9, v9, v3

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/support/g;->mActive:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    add-int/lit8 v5, v3, 0x1

    aget v3, v9, v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    iget-object v3, v7, Lcom/google/android/gms/car/support/b$a;->removed:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v3, v5

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/android/gms/car/support/b$a;->Nq:Lcom/google/android/gms/car/support/Fragment;

    goto :goto_1

    :cond_3
    invoke-virtual {v6, v7}, Lcom/google/android/gms/car/support/b;->a(Lcom/google/android/gms/car/support/b$a;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto/16 :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/c;->mTransition:I

    iput v0, v6, Lcom/google/android/gms/car/support/b;->mTransition:I

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mTransitionStyle:I

    iput v0, v6, Lcom/google/android/gms/car/support/b;->mTransitionStyle:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mName:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/car/support/b;->mName:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mIndex:I

    iput v0, v6, Lcom/google/android/gms/car/support/b;->mIndex:I

    iput-boolean v10, v6, Lcom/google/android/gms/car/support/b;->mAddToBackStack:Z

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleRes:I

    iput v0, v6, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleRes:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    iput-object v0, v6, Lcom/google/android/gms/car/support/b;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleRes:I

    iput v0, v6, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleRes:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    iput-object v0, v6, Lcom/google/android/gms/car/support/b;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    invoke-virtual {v6, v10}, Lcom/google/android/gms/car/support/b;->bumpBackStackNesting(I)V

    return-object v6
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mOps:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mTransition:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mTransitionStyle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleRes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    return-void
.end method
