.class public Lcom/google/android/gms/car/DrawingSpec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/car/DrawingSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public dpi:I

.field public height:I

.field final mVersionCode:I

.field public surface:Landroid/view/Surface;

.field public width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/x;

    invoke-direct {v0}, Lcom/google/android/gms/car/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/DrawingSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIILandroid/view/Surface;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "dpi"    # I
    .param p5, "surface"    # Landroid/view/Surface;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/car/DrawingSpec;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/car/DrawingSpec;->width:I

    iput p3, p0, Lcom/google/android/gms/car/DrawingSpec;->height:I

    iput p4, p0, Lcom/google/android/gms/car/DrawingSpec;->dpi:I

    iput-object p5, p0, Lcom/google/android/gms/car/DrawingSpec;->surface:Landroid/view/Surface;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/DrawingSpec;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/x;->a(Lcom/google/android/gms/car/DrawingSpec;Landroid/os/Parcel;I)V

    return-void
.end method
