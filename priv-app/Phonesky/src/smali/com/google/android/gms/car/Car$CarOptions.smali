.class public final Lcom/google/android/gms/car/Car$CarOptions;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/Api$ApiOptions$HasOptions;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/Car;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/Car$CarOptions$Builder;
    }
.end annotation


# instance fields
.field final Jf:Lcom/google/android/gms/car/Car$CarConnectionListener;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/Car$CarOptions$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/google/android/gms/car/Car$CarOptions$Builder;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/car/Car$CarOptions$Builder;->a(Lcom/google/android/gms/car/Car$CarOptions$Builder;)Lcom/google/android/gms/car/Car$CarConnectionListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/Car$CarOptions;->Jf:Lcom/google/android/gms/car/Car$CarConnectionListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/Car$CarOptions$Builder;Lcom/google/android/gms/car/Car$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/car/Car$CarOptions$Builder;
    .param p2, "x1"    # Lcom/google/android/gms/car/Car$1;

    .prologue
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/Car$CarOptions;-><init>(Lcom/google/android/gms/car/Car$CarOptions$Builder;)V

    return-void
.end method

.method public static builder(Lcom/google/android/gms/car/Car$CarConnectionListener;)Lcom/google/android/gms/car/Car$CarOptions$Builder;
    .locals 2
    .param p0, "carConnectionListener"    # Lcom/google/android/gms/car/Car$CarConnectionListener;

    .prologue
    new-instance v0, Lcom/google/android/gms/car/Car$CarOptions$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/Car$CarOptions$Builder;-><init>(Lcom/google/android/gms/car/Car$CarConnectionListener;Lcom/google/android/gms/car/Car$1;)V

    return-object v0
.end method
