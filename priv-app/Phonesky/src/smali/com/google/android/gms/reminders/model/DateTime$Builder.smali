.class public Lcom/google/android/gms/reminders/model/DateTime$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/reminders/model/DateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private aKX:Ljava/lang/Integer;

.field private aKY:Ljava/lang/Integer;

.field private aKZ:Ljava/lang/Integer;

.field private aLa:Lcom/google/android/gms/reminders/model/Time;

.field private aLb:Ljava/lang/Integer;

.field private aLc:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 8

    new-instance v0, Lcom/google/android/gms/reminders/model/b;

    iget-object v1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKX:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKY:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKZ:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLa:Lcom/google/android/gms/reminders/model/Time;

    iget-object v5, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLb:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLc:Ljava/lang/Long;

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/reminders/model/b;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/reminders/model/Time;Ljava/lang/Integer;Ljava/lang/Long;Z)V

    return-object v0
.end method

.method public setAbsoluteTimeMs(Ljava/lang/Long;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 0
    .param p1, "absoluteTimeMs"    # Ljava/lang/Long;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLc:Ljava/lang/Long;

    return-object p0
.end method

.method public setDay(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 0
    .param p1, "day"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKZ:Ljava/lang/Integer;

    return-object p0
.end method

.method public setMonth(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 0
    .param p1, "month"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKY:Ljava/lang/Integer;

    return-object p0
.end method

.method public setPeriod(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 0
    .param p1, "period"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLb:Ljava/lang/Integer;

    return-object p0
.end method

.method public setTime(Lcom/google/android/gms/reminders/model/Time;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 1
    .param p1, "time"    # Lcom/google/android/gms/reminders/model/Time;

    .prologue
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Time;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/reminders/model/Time;

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aLa:Lcom/google/android/gms/reminders/model/Time;

    :cond_0
    return-object p0
.end method

.method public setYear(Ljava/lang/Integer;)Lcom/google/android/gms/reminders/model/DateTime$Builder;
    .locals 0
    .param p1, "year"    # Ljava/lang/Integer;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/reminders/model/DateTime$Builder;->aKX:Ljava/lang/Integer;

    return-object p0
.end method
