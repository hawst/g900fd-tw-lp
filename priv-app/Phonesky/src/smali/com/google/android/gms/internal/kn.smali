.class public Lcom/google/android/gms/internal/kn;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/kn$e;,
        Lcom/google/android/gms/internal/kn$c;,
        Lcom/google/android/gms/internal/kn$d;,
        Lcom/google/android/gms/internal/kn$b;,
        Lcom/google/android/gms/internal/kn$a;
    }
.end annotation


# static fields
.field public static final ack:Lcom/google/android/gms/internal/kn$a;

.field public static final acl:Lcom/google/android/gms/internal/kn$b;

.field public static final acm:Lcom/google/android/gms/internal/kn$d;

.field public static final acn:Lcom/google/android/gms/internal/kn$c;

.field public static final aco:Lcom/google/android/gms/internal/kn$e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x3e8fa0

    new-instance v0, Lcom/google/android/gms/internal/kn$a;

    const-string v1, "created"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/kn$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kn;->ack:Lcom/google/android/gms/internal/kn$a;

    new-instance v0, Lcom/google/android/gms/internal/kn$b;

    const-string v1, "lastOpenedTime"

    const v2, 0x419ce0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/kn$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kn;->acl:Lcom/google/android/gms/internal/kn$b;

    new-instance v0, Lcom/google/android/gms/internal/kn$d;

    const-string v1, "modified"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/kn$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kn;->acm:Lcom/google/android/gms/internal/kn$d;

    new-instance v0, Lcom/google/android/gms/internal/kn$c;

    const-string v1, "modifiedByMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/kn$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kn;->acn:Lcom/google/android/gms/internal/kn$c;

    new-instance v0, Lcom/google/android/gms/internal/kn$e;

    const-string v1, "sharedWithMe"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/kn$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kn;->aco:Lcom/google/android/gms/internal/kn$e;

    return-void
.end method
