.class public Lcom/google/android/gms/car/CarBluetoothConnectionManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarBluetoothConnectionManager$a;
    }
.end annotation


# instance fields
.field private final Kt:Lcom/google/android/gms/car/CarBluetoothConnectionManager$a;


# virtual methods
.method handleCarDisconnection()V
    .locals 2

    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.BT"

    const-string v1, "handleCarDisconnection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarBluetoothConnectionManager;->Kt:Lcom/google/android/gms/car/CarBluetoothConnectionManager$a;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarBluetoothConnectionManager$a;->onCarDisconnected()V

    return-void
.end method
