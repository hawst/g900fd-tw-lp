.class Lcom/google/android/gms/car/CarNavigationStatusManager$a;
.super Lcom/google/android/gms/car/au$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarNavigationStatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;


# virtual methods
.method public onStart(IIIII)V
    .locals 3
    .param p1, "minIntervalMs"    # I
    .param p2, "instrumentClusterType"    # I
    .param p3, "imageHeight"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageColourDepthBits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/CarNavigationStatusManager;->a(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0, p2}, Lcom/google/android/gms/car/CarNavigationStatusManager;->b(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0, p3}, Lcom/google/android/gms/car/CarNavigationStatusManager;->c(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0, p4}, Lcom/google/android/gms/car/CarNavigationStatusManager;->d(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0, p5}, Lcom/google/android/gms/car/CarNavigationStatusManager;->e(Lcom/google/android/gms/car/CarNavigationStatusManager;I)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarNavigationStatusManager;->g(Lcom/google/android/gms/car/CarNavigationStatusManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v1}, Lcom/google/android/gms/car/CarNavigationStatusManager;->g(Lcom/google/android/gms/car/CarNavigationStatusManager;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onStop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarNavigationStatusManager;->g(Lcom/google/android/gms/car/CarNavigationStatusManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarNavigationStatusManager$a;->LQ:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-static {v1}, Lcom/google/android/gms/car/CarNavigationStatusManager;->g(Lcom/google/android/gms/car/CarNavigationStatusManager;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
