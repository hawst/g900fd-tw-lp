.class public interface abstract Lcom/google/android/gms/car/Car$CarApi;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/Car;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CarApi"
.end annotation


# virtual methods
.method public abstract isConnectedToCar(Lcom/google/android/gms/common/api/GoogleApiClient;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method
