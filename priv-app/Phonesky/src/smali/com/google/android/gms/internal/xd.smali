.class public final Lcom/google/android/gms/internal/xd;
.super Ljava/lang/Object;


# static fields
.field public static final aYG:[I

.field public static final aYH:[J

.field public static final aYI:[F

.field public static final aYJ:[D

.field public static final aYK:[Z

.field public static final aYL:[Ljava/lang/String;

.field public static final aYM:[[B

.field public static final aYN:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYG:[I

    new-array v0, v1, [J

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYH:[J

    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYI:[F

    new-array v0, v1, [D

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYJ:[D

    new-array v0, v1, [Z

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYK:[Z

    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYL:[Ljava/lang/String;

    new-array v0, v1, [[B

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYM:[[B

    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/internal/xd;->aYN:[B

    return-void
.end method

.method static H(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static mt(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method
