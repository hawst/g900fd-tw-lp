.class Lcom/google/android/gms/car/support/g$5;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic ND:Lcom/google/android/gms/car/support/g;

.field final synthetic NE:Lcom/google/android/gms/car/support/Fragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/g;Lcom/google/android/gms/car/support/Fragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/g$5;->ND:Lcom/google/android/gms/car/support/g;

    iput-object p2, p0, Lcom/google/android/gms/car/support/g$5;->NE:Lcom/google/android/gms/car/support/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g$5;->NE:Lcom/google/android/gms/car/support/Fragment;

    iget-object v0, v0, Lcom/google/android/gms/car/support/Fragment;->mAnimatingAway:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g$5;->NE:Lcom/google/android/gms/car/support/Fragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/support/Fragment;->mAnimatingAway:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g$5;->ND:Lcom/google/android/gms/car/support/g;

    iget-object v1, p0, Lcom/google/android/gms/car/support/g$5;->NE:Lcom/google/android/gms/car/support/Fragment;

    iget-object v2, p0, Lcom/google/android/gms/car/support/g$5;->NE:Lcom/google/android/gms/car/support/Fragment;

    iget v2, v2, Lcom/google/android/gms/car/support/Fragment;->mStateAfterAnimating:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/Fragment;IIIZ)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    return-void
.end method
