.class public interface abstract Lcom/google/android/gms/ads/identifier/AdIdProvider;
.super Ljava/lang/Object;
.source "AdIdProvider.java"


# virtual methods
.method public abstract getAdId()Ljava/lang/String;
.end method

.method public abstract getPublicAndroidId()Ljava/lang/String;
.end method

.method public abstract isLimitAdTrackingEnabled()Ljava/lang/Boolean;
.end method

.method public abstract refreshCachedData()V
.end method
