.class public final Lcom/google/android/gms/internal/gx;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/gy;


# instance fields
.field public final versionCode:I

.field public wR:Ljava/lang/String;

.field public wS:I

.field public wT:I

.field public wU:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/gy;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gy;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/gx;->CREATOR:Lcom/google/android/gms/internal/gy;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/gx;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/gx;->wR:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/internal/gx;->wS:I

    iput p4, p0, Lcom/google/android/gms/internal/gx;->wT:I

    iput-boolean p5, p0, Lcom/google/android/gms/internal/gx;->wU:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/gy;->a(Lcom/google/android/gms/internal/gx;Landroid/os/Parcel;I)V

    return-void
.end method
