.class public Lcom/google/android/gms/internal/dq;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/fd;
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mU:Landroid/view/WindowManager;

.field private final mr:Lcom/google/android/gms/internal/gz;

.field private rA:F

.field rB:I

.field rC:I

.field private rD:I

.field private rE:I

.field private rF:I

.field private rG:[I

.field private final ry:Lcom/google/android/gms/internal/bq;

.field rz:Landroid/util/DisplayMetrics;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gz;Landroid/content/Context;Lcom/google/android/gms/internal/bq;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rB:I

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rC:I

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rE:I

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rF:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/dq;->rG:[I

    iput-object p1, p0, Lcom/google/android/gms/internal/dq;->mr:Lcom/google/android/gms/internal/gz;

    iput-object p2, p0, Lcom/google/android/gms/internal/dq;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/dq;->ry:Lcom/google/android/gms/internal/bq;

    const-string v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/dq;->mU:Landroid/view/WindowManager;

    invoke-direct {p0}, Lcom/google/android/gms/internal/dq;->bT()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/dq;->bU()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/dq;->bV()V

    return-void
.end method

.method private bT()V
    .locals 2

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/gms/internal/dq;->mU:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/gms/internal/dq;->rA:F

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rD:I

    return-void
.end method

.method private bV()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/dq;->mr:Lcom/google/android/gms/internal/gz;

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->rG:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gz;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dq;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/gms/internal/gz;->measure(II)V

    const/high16 v0, 0x43200000    # 160.0f

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/internal/dq;->rE:I

    iget-object v1, p0, Lcom/google/android/gms/internal/dq;->mr:Lcom/google/android/gms/internal/gz;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gz;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rF:I

    return-void
.end method


# virtual methods
.method bU()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/dq;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/gn;->s(Landroid/content/Context;)I

    move-result v0

    const/high16 v1, 0x43200000    # 160.0f

    iget-object v2, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/internal/dq;->rB:I

    iget-object v2, p0, Lcom/google/android/gms/internal/dq;->rz:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dq;->rC:I

    return-void
.end method
