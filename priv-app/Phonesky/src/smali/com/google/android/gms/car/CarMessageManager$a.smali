.class Lcom/google/android/gms/car/CarMessageManager$a;
.super Lcom/google/android/gms/car/as$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarMessageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final LH:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/car/CarMessageManager;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public onIntegerMessage(III)V
    .locals 4
    .param p1, "category"    # I
    .param p2, "key"    # I
    .param p3, "value"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/car/CarMessageManager$a;->LH:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarMessageManager;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/car/CarMessageManager;->b(Lcom/google/android/gms/car/CarMessageManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/car/CarMessageManager;->b(Lcom/google/android/gms/car/CarMessageManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v2, p1, p2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onOwnershipLost(I)V
    .locals 4
    .param p1, "category"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/car/CarMessageManager$a;->LH:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarMessageManager;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/car/CarMessageManager;->b(Lcom/google/android/gms/car/CarMessageManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/car/CarMessageManager;->b(Lcom/google/android/gms/car/CarMessageManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
