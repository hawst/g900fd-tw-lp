.class public interface abstract Lcom/google/android/gms/games/internal/game/ExtendedGame;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/gms/common/data/Freezable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcom/google/android/gms/common/data/Freezable",
        "<",
        "Lcom/google/android/gms/games/internal/game/ExtendedGame;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getGame()Lcom/google/android/gms/games/Game;
.end method

.method public abstract nF()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/games/internal/game/GameBadge;",
            ">;"
        }
    .end annotation
.end method

.method public abstract nG()I
.end method

.method public abstract nH()Z
.end method

.method public abstract nI()I
.end method

.method public abstract nJ()J
.end method

.method public abstract nK()J
.end method

.method public abstract nL()Ljava/lang/String;
.end method

.method public abstract nM()J
.end method

.method public abstract nN()Ljava/lang/String;
.end method

.method public abstract nO()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
.end method
