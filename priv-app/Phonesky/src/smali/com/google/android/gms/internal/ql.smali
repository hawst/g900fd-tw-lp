.class public Lcom/google/android/gms/internal/ql;
.super Ljava/lang/Object;


# static fields
.field public static final aDi:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static aDj:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<*>;"
        }
    .end annotation
.end field

.field public static final aDk:Landroid/os/Handler;

.field public static final aDl:[Ljava/lang/String;

.field public static final aDm:Ljava/util/regex/Pattern;

.field public static final aDn:Ljava/util/regex/Pattern;

.field public static final aDo:Ljava/util/regex/Pattern;

.field public static final aDp:Ljava/util/regex/Pattern;

.field public static final aDq:Ljava/lang/String;

.field public static final aDr:Ljava/lang/String;

.field public static final aDs:Ljava/security/SecureRandom;

.field private static final aDt:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private static final aDu:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final aDv:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final aDw:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final aDx:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final aDy:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/gms/internal/ql$1;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDi:Ljava/util/Map;

    new-instance v0, Lcom/google/android/gms/internal/qa;

    invoke-direct {v0}, Lcom/google/android/gms/internal/qa;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDj:Ljava/lang/Iterable;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDk:Landroid/os/Handler;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDl:[Ljava/lang/String;

    const-string v0, "\\,"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDm:Ljava/util/regex/Pattern;

    const-string v0, "[\u2028\u2029 \u00a0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\t\u000b\u000c\u001c\u001d\u001e\u001f\n\r]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDn:Ljava/util/regex/Pattern;

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDo:Ljava/util/regex/Pattern;

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDp:Ljava/util/regex/Pattern;

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDq:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDr:Ljava/lang/String;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDs:Ljava/security/SecureRandom;

    new-instance v0, Lcom/google/android/gms/internal/ql$2;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$2;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDt:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/gms/internal/ql$3;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$3;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDu:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/gms/internal/ql$4;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$4;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDv:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/gms/internal/ql$5;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$5;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDw:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/gms/internal/ql$6;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$6;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDx:Ljava/lang/ThreadLocal;

    new-instance v0, Lcom/google/android/gms/internal/ql$7;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ql$7;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ql;->aDy:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static K(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/s;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "g:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "e:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Expecting qualified-id, not gaia-id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/s;->b(ZLjava/lang/Object;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
