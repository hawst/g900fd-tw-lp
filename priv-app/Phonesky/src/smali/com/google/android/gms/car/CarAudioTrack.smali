.class public Lcom/google/android/gms/car/CarAudioTrack;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/CarAudioTrack$a;
    }
.end annotation


# instance fields
.field private final JZ:Lcom/google/android/gms/car/CarAudioManager;

.field private final Kc:I

.field private final Kh:Lcom/google/android/gms/car/ag;

.field private final Kk:Lcom/google/android/gms/car/CarAudioTrack$a;

.field private volatile Kl:Z

.field private final mHandler:Landroid/os/Handler;

.field private mPlayState:I


# direct methods
.method static synthetic a(Lcom/google/android/gms/car/CarAudioTrack;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/CarAudioTrack;->bV(I)V

    return-void
.end method

.method private declared-synchronized bV(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/gms/car/CarAudioTrack;->mPlayState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/CarAudioTrack;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kh:Lcom/google/android/gms/car/ag;

    invoke-interface {v0}, Lcom/google/android/gms/car/ag;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kl:Z

    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->JZ:Lcom/google/android/gms/car/CarAudioManager;

    iget v1, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kc:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarAudioManager;->bT(I)V

    return-void
.end method

.method public declared-synchronized release()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kl:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->JZ:Lcom/google/android/gms/car/CarAudioManager;

    iget v1, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kc:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarAudioManager;->bT(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kh:Lcom/google/android/gms/car/ag;

    iget-object v1, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kk:Lcom/google/android/gms/car/CarAudioTrack$a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/ag;->a(Lcom/google/android/gms/car/ah;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kh:Lcom/google/android/gms/car/ag;

    invoke-interface {v0}, Lcom/google/android/gms/car/ag;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarAudioTrack;->Kl:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
