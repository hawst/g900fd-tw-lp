.class Lcom/google/android/gms/car/support/Fragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/support/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/car/support/Fragment;->instantiateChildFragmentManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic Nx:Lcom/google/android/gms/car/support/Fragment;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/Fragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/Fragment$1;->Nx:Lcom/google/android/gms/car/support/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .locals 2
    .param p1, "id"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment$1;->Nx:Lcom/google/android/gms/car/support/Fragment;

    iget-object v0, v0, Lcom/google/android/gms/car/support/Fragment;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment does not have a view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment$1;->Nx:Lcom/google/android/gms/car/support/Fragment;

    iget-object v0, v0, Lcom/google/android/gms/car/support/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
