.class public Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private Fl:Ljava/lang/String;

.field private aGR:I

.field private aGS:Ljava/lang/String;

.field private aGT:F

.field private aGU:I

.field private aGV:I

.field private aGW:I

.field private aGX:[Ljava/lang/String;

.field private aGY:Ljava/lang/String;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/b;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->mVersionCode:I

    return-void
.end method

.method constructor <init>(IILjava/lang/String;Ljava/lang/String;FIII[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "autoBackupState"    # I
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "currentItem"    # Ljava/lang/String;
    .param p5, "currentItemProgress"    # F
    .param p6, "numCompleted"    # I
    .param p7, "numPending"    # I
    .param p8, "numFailed"    # I
    .param p9, "failedItems"    # [Ljava/lang/String;
    .param p10, "enabledAccountName"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGR:I

    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->Fl:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGS:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGT:F

    iput p6, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGU:I

    iput p7, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGV:I

    iput p8, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGW:I

    iput-object p9, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGX:[Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGY:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->Fl:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoBackupState()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGR:I

    return v0
.end method

.method public getCurrentItem()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGS:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentItemProgress()F
    .locals 1

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGT:F

    return v0
.end method

.method public getEnabledAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGY:Ljava/lang/String;

    return-object v0
.end method

.method public getFailedItems()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGX:[Ljava/lang/String;

    return-object v0
.end method

.method public getNumCompleted()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGU:I

    return v0
.end method

.method public getNumFailed()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGW:I

    return v0
.end method

.method public getNumPending()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGV:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/gms/common/internal/r;->j(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->Fl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "autoBackupState"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGR:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "currentItem"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGS:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "currentItemProgress"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGT:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "numCompleted"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "numPending"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGV:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "numFailed"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGW:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "failedItems"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGX:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    const-string v1, "enabledAccountName"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->aGY:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/r$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/r$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/r$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/model/b;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;Landroid/os/Parcel;I)V

    return-void
.end method
