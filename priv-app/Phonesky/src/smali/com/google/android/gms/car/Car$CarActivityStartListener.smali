.class public interface abstract Lcom/google/android/gms/car/Car$CarActivityStartListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/Car;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CarActivityStartListener"
.end annotation


# virtual methods
.method public abstract onActivityStarted(Landroid/content/Intent;)V
.end method

.method public abstract onNewActivityRequest(Landroid/content/Intent;)V
.end method
