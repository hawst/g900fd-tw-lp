.class public abstract Lcom/google/android/gms/internal/qx;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/qx$a;,
        Lcom/google/android/gms/internal/qx$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static aEJ:Lcom/google/android/gms/internal/qx$b;

.field public static aEK:Lcom/google/android/gms/internal/qx$a;


# instance fields
.field private final aEE:C

.field private final aEF:C

.field private final aEG:Ljava/lang/String;

.field private final aEH:Ljava/lang/String;

.field private final aEI:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/qx$b;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/qx$b;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/internal/qx;->aEJ:Lcom/google/android/gms/internal/qx$b;

    new-instance v0, Lcom/google/android/gms/internal/qx$a;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/qx$a;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/internal/qx;->aEK:Lcom/google/android/gms/internal/qx$a;

    return-void
.end method

.method constructor <init>(Landroid/os/Bundle;CC)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/qx;->aEI:Landroid/os/Bundle;

    iput-char p2, p0, Lcom/google/android/gms/internal/qx;->aEE:C

    iput-char p3, p0, Lcom/google/android/gms/internal/qx;->aEF:C

    iget-char v0, p0, Lcom/google/android/gms/internal/qx;->aEE:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/qx;->aEG:Ljava/lang/String;

    iget-char v0, p0, Lcom/google/android/gms/internal/qx;->aEF:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/qx;->aEH:Ljava/lang/String;

    return-void
.end method
