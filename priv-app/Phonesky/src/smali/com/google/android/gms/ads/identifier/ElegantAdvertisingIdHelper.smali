.class public Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;
.super Ljava/lang/Object;
.source "ElegantAdvertisingIdHelper.java"

# interfaces
.implements Lcom/google/android/gms/ads/identifier/AdIdProvider;


# static fields
.field private static sCachedAdId:Ljava/lang/String;

.field private static sIsLimitAdTrackingEnabled:Ljava/lang/Boolean;

.field private static sPublicAndroidId:Ljava/lang/String;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    sput-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sCachedAdId:Ljava/lang/String;

    .line 17
    sput-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sPublicAndroidId:Ljava/lang/String;

    .line 18
    sput-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sIsLimitAdTrackingEnabled:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/Context;)V
    .locals 1
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->mContentResolver:Landroid/content/ContentResolver;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->mContext:Landroid/content/Context;

    .line 26
    const-string v0, "android_id"

    invoke-static {p1, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sPublicAndroidId:Ljava/lang/String;

    .line 28
    invoke-virtual {p0}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->refreshCachedData()V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 15
    sput-object p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sCachedAdId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 15
    sput-object p0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sIsLimitAdTrackingEnabled:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method public declared-synchronized getAdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sCachedAdId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPublicAndroidId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sPublicAndroidId:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized isLimitAdTrackingEnabled()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;->sIsLimitAdTrackingEnabled:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public refreshCachedData()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;-><init>(Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/identifier/ElegantAdvertisingIdHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 58
    return-void
.end method
