.class public Lcom/google/android/gms/reminders/model/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/reminders/model/Task;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/reminders/model/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final YH:Ljava/lang/String;

.field private final aLA:Lcom/google/android/gms/reminders/model/b;

.field private final aLB:Lcom/google/android/gms/reminders/model/d;

.field private final aLm:Ljava/lang/Long;

.field private final aLn:Ljava/lang/Long;

.field private final aLo:Ljava/lang/Boolean;

.field private final aLp:Ljava/lang/Boolean;

.field private final aLq:Ljava/lang/Boolean;

.field private final aLr:Ljava/lang/Boolean;

.field private final aLs:Ljava/lang/Long;

.field private final aLw:Ljava/lang/Long;

.field private final aLx:Lcom/google/android/gms/reminders/model/i;

.field private final aLy:Lcom/google/android/gms/reminders/model/k;

.field private final aLz:Lcom/google/android/gms/reminders/model/b;

.field public final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/reminders/model/f;

    invoke-direct {v0}, Lcom/google/android/gms/reminders/model/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/model/g;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/reminders/model/i;Lcom/google/android/gms/reminders/model/k;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/b;Lcom/google/android/gms/reminders/model/b;Lcom/google/android/gms/reminders/model/d;Ljava/lang/Long;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/reminders/model/g;->aLx:Lcom/google/android/gms/reminders/model/i;

    iput-object p3, p0, Lcom/google/android/gms/reminders/model/g;->aLy:Lcom/google/android/gms/reminders/model/k;

    iput-object p4, p0, Lcom/google/android/gms/reminders/model/g;->YH:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/reminders/model/g;->aLm:Ljava/lang/Long;

    iput-object p6, p0, Lcom/google/android/gms/reminders/model/g;->aLn:Ljava/lang/Long;

    if-nez p7, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/g;->aLo:Ljava/lang/Boolean;

    if-nez p8, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/g;->aLp:Ljava/lang/Boolean;

    if-nez p9, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/g;->aLq:Ljava/lang/Boolean;

    if-nez p10, :cond_3

    const/4 v1, 0x0

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/reminders/model/g;->aLr:Ljava/lang/Boolean;

    iput-object p11, p0, Lcom/google/android/gms/reminders/model/g;->aLs:Ljava/lang/Long;

    iput-object p12, p0, Lcom/google/android/gms/reminders/model/g;->aLz:Lcom/google/android/gms/reminders/model/b;

    iput-object p13, p0, Lcom/google/android/gms/reminders/model/g;->aLA:Lcom/google/android/gms/reminders/model/b;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLB:Lcom/google/android/gms/reminders/model/d;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLw:Ljava/lang/Long;

    iput p1, p0, Lcom/google/android/gms/reminders/model/g;->mVersionCode:I

    return-void

    :cond_0
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_1

    :cond_2
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_2

    :cond_3
    invoke-virtual {p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_3
.end method

.method public constructor <init>(Lcom/google/android/gms/reminders/model/Task;)V
    .locals 16

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getTaskId()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getTaskList()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getCreatedTimeMillis()Ljava/lang/Long;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getArchivedTimeMs()Ljava/lang/Long;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getArchived()Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getDeleted()Ljava/lang/Boolean;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getPinned()Ljava/lang/Boolean;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getSnoozed()Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getSnoozedTimeMillis()Ljava/lang/Long;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getDueDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getEventDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getLocation()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/reminders/model/Task;->getLocationSnoozedUntilMs()Ljava/lang/Long;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v15}, Lcom/google/android/gms/reminders/model/g;-><init>(Lcom/google/android/gms/reminders/model/TaskId;Lcom/google/android/gms/reminders/model/TaskList;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/Location;Ljava/lang/Long;Z)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/reminders/model/TaskId;Lcom/google/android/gms/reminders/model/TaskList;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/DateTime;Lcom/google/android/gms/reminders/model/Location;Ljava/lang/Long;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/reminders/model/g;->mVersionCode:I

    iput-object p3, p0, Lcom/google/android/gms/reminders/model/g;->YH:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/reminders/model/g;->aLm:Ljava/lang/Long;

    iput-object p5, p0, Lcom/google/android/gms/reminders/model/g;->aLn:Ljava/lang/Long;

    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLo:Ljava/lang/Boolean;

    if-nez p7, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLp:Ljava/lang/Boolean;

    if-nez p8, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLq:Ljava/lang/Boolean;

    if-nez p9, :cond_3

    const/4 v0, 0x0

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLr:Ljava/lang/Boolean;

    iput-object p10, p0, Lcom/google/android/gms/reminders/model/g;->aLs:Ljava/lang/Long;

    iput-object p14, p0, Lcom/google/android/gms/reminders/model/g;->aLw:Ljava/lang/Long;

    if-eqz p15, :cond_4

    check-cast p1, Lcom/google/android/gms/reminders/model/i;

    iput-object p1, p0, Lcom/google/android/gms/reminders/model/g;->aLx:Lcom/google/android/gms/reminders/model/i;

    check-cast p2, Lcom/google/android/gms/reminders/model/k;

    iput-object p2, p0, Lcom/google/android/gms/reminders/model/g;->aLy:Lcom/google/android/gms/reminders/model/k;

    check-cast p11, Lcom/google/android/gms/reminders/model/b;

    iput-object p11, p0, Lcom/google/android/gms/reminders/model/g;->aLz:Lcom/google/android/gms/reminders/model/b;

    check-cast p12, Lcom/google/android/gms/reminders/model/b;

    iput-object p12, p0, Lcom/google/android/gms/reminders/model/g;->aLA:Lcom/google/android/gms/reminders/model/b;

    check-cast p13, Lcom/google/android/gms/reminders/model/d;

    iput-object p13, p0, Lcom/google/android/gms/reminders/model/g;->aLB:Lcom/google/android/gms/reminders/model/d;

    :goto_4
    return-void

    :cond_0
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_2
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2

    :cond_3
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3

    :cond_4
    if-nez p1, :cond_5

    const/4 v0, 0x0

    :goto_5
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLx:Lcom/google/android/gms/reminders/model/i;

    if-nez p2, :cond_6

    const/4 v0, 0x0

    :goto_6
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLy:Lcom/google/android/gms/reminders/model/k;

    if-nez p11, :cond_7

    const/4 v0, 0x0

    :goto_7
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLz:Lcom/google/android/gms/reminders/model/b;

    if-nez p12, :cond_8

    const/4 v0, 0x0

    :goto_8
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLA:Lcom/google/android/gms/reminders/model/b;

    if-nez p13, :cond_9

    const/4 v0, 0x0

    :goto_9
    iput-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLB:Lcom/google/android/gms/reminders/model/d;

    goto :goto_4

    :cond_5
    new-instance v0, Lcom/google/android/gms/reminders/model/i;

    invoke-direct {v0, p1}, Lcom/google/android/gms/reminders/model/i;-><init>(Lcom/google/android/gms/reminders/model/TaskId;)V

    goto :goto_5

    :cond_6
    new-instance v0, Lcom/google/android/gms/reminders/model/k;

    invoke-direct {v0, p2}, Lcom/google/android/gms/reminders/model/k;-><init>(Lcom/google/android/gms/reminders/model/TaskList;)V

    goto :goto_6

    :cond_7
    new-instance v0, Lcom/google/android/gms/reminders/model/b;

    invoke-direct {v0, p11}, Lcom/google/android/gms/reminders/model/b;-><init>(Lcom/google/android/gms/reminders/model/DateTime;)V

    goto :goto_7

    :cond_8
    new-instance v0, Lcom/google/android/gms/reminders/model/b;

    invoke-direct {v0, p12}, Lcom/google/android/gms/reminders/model/b;-><init>(Lcom/google/android/gms/reminders/model/DateTime;)V

    goto :goto_8

    :cond_9
    new-instance v0, Lcom/google/android/gms/reminders/model/d;

    invoke-direct {v0, p13}, Lcom/google/android/gms/reminders/model/d;-><init>(Lcom/google/android/gms/reminders/model/Location;)V

    goto :goto_9
.end method

.method public static a(Lcom/google/android/gms/reminders/model/Task;Lcom/google/android/gms/reminders/model/Task;)Z
    .locals 2

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getTaskId()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getTaskId()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getTaskList()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getTaskList()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getCreatedTimeMillis()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getCreatedTimeMillis()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getArchivedTimeMs()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getArchivedTimeMs()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getArchived()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getArchived()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getDeleted()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getDeleted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getPinned()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getPinned()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getSnoozed()Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getSnoozed()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getSnoozedTimeMillis()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getSnoozedTimeMillis()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getDueDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getDueDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getEventDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getEventDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getLocation()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getLocation()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Task;->getLocationSnoozedUntilMs()Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->getLocationSnoozedUntilMs()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/r;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    instance-of v0, p1, Lcom/google/android/gms/reminders/model/Task;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/gms/reminders/model/Task;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/gms/reminders/model/g;->a(Lcom/google/android/gms/reminders/model/Task;Lcom/google/android/gms/reminders/model/Task;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic freeze()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->rV()Lcom/google/android/gms/reminders/model/Task;

    move-result-object v0

    return-object v0
.end method

.method public getArchived()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLo:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getArchivedTimeMs()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLn:Ljava/lang/Long;

    return-object v0
.end method

.method public getCreatedTimeMillis()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLm:Ljava/lang/Long;

    return-object v0
.end method

.method public getDeleted()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLp:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getDueDate()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLz:Lcom/google/android/gms/reminders/model/b;

    return-object v0
.end method

.method public getEventDate()Lcom/google/android/gms/reminders/model/DateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLA:Lcom/google/android/gms/reminders/model/b;

    return-object v0
.end method

.method public getLocation()Lcom/google/android/gms/reminders/model/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLB:Lcom/google/android/gms/reminders/model/d;

    return-object v0
.end method

.method public getLocationSnoozedUntilMs()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLw:Ljava/lang/Long;

    return-object v0
.end method

.method public getPinned()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLq:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSnoozed()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLr:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSnoozedTimeMillis()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLs:Ljava/lang/Long;

    return-object v0
.end method

.method public getTaskId()Lcom/google/android/gms/reminders/model/TaskId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLx:Lcom/google/android/gms/reminders/model/i;

    return-object v0
.end method

.method public getTaskList()Lcom/google/android/gms/reminders/model/TaskList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->aLy:Lcom/google/android/gms/reminders/model/k;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/reminders/model/g;->YH:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getTaskId()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getTaskList()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getCreatedTimeMillis()Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getArchivedTimeMs()Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getArchived()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getDeleted()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getPinned()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getSnoozed()Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getSnoozedTimeMillis()Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getDueDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getEventDate()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getLocation()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/g;->getLocationSnoozedUntilMs()Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/r;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public rV()Lcom/google/android/gms/reminders/model/Task;
    .locals 0

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/reminders/model/f;->a(Lcom/google/android/gms/reminders/model/g;Landroid/os/Parcel;I)V

    return-void
.end method
