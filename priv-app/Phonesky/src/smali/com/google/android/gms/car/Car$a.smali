.class final Lcom/google/android/gms/car/Car$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/Car$CarApi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/Car;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/Car$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/Car$a;-><init>()V

    return-void
.end method


# virtual methods
.method public isConnectedToCar(Lcom/google/android/gms/common/api/GoogleApiClient;)Z
    .locals 1
    .param p1, "client"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    invoke-static {p1}, Lcom/google/android/gms/car/Car;->c(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    sget-object v0, Lcom/google/android/gms/car/Car;->CLIENT_KEY:Lcom/google/android/gms/common/api/Api$c;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->a(Lcom/google/android/gms/common/api/Api$c;)Lcom/google/android/gms/common/api/Api$a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/e;->gg()Z

    move-result v0

    return v0
.end method
