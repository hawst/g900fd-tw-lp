.class public final Lcom/google/android/gms/car/Car$CarOptions$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/Car$CarOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/Car$CarConnectionListener;)V
    .locals 0
    .param p1, "carConnectionListener"    # Lcom/google/android/gms/car/Car$CarConnectionListener;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/car/Car$CarOptions$Builder;->Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/Car$CarConnectionListener;Lcom/google/android/gms/car/Car$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/car/Car$CarConnectionListener;
    .param p2, "x1"    # Lcom/google/android/gms/car/Car$1;

    .prologue
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/Car$CarOptions$Builder;-><init>(Lcom/google/android/gms/car/Car$CarConnectionListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/Car$CarOptions$Builder;)Lcom/google/android/gms/car/Car$CarConnectionListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/Car$CarOptions$Builder;->Jg:Lcom/google/android/gms/car/Car$CarConnectionListener;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/gms/car/Car$CarOptions;
    .locals 2

    new-instance v0, Lcom/google/android/gms/car/Car$CarOptions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/Car$CarOptions;-><init>(Lcom/google/android/gms/car/Car$CarOptions$Builder;Lcom/google/android/gms/car/Car$1;)V

    return-object v0
.end method
