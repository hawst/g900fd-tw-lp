.class public Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;,
        Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeRemoteException;,
        Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeInterruptedException;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/s;->k(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->mContext:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a",
            "<TR;>;)TR;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->aD(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    new-instance v1, Lcom/google/android/gms/common/a;

    invoke-direct {v1}, Lcom/google/android/gms/common/a;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v1, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/a;->hN()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w$a;->V(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/dataservice/w;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;->b(Lcom/google/android/gms/auth/firstparty/dataservice/w;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    iget-object v4, p0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v4, "GoogleAccountDataServiceClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[GoogleAccountDataServiceClient]  Interrupted when getting service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeInterruptedException;

    invoke-direct {v4, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v4, p0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_5
    const-string v4, "GoogleAccountDataServiceClient"

    const-string v5, "[GoogleAccountDataServiceClient]  RemoteException when executing call!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeRemoteException;

    invoke-direct {v4, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0
.end method

.method private static aD(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.auth.DATA_PROXY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/GoogleAuthUtil;->KEY_ANDROID_PACKAGE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public confirmCredentials(Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1
    .param p1, "request"    # Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;

    .prologue
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$5;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    return-object v0
.end method

.method public getReauthSettings(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .locals 1
    .param p1, "request"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;

    .prologue
    invoke-static {p1}, Lcom/google/android/gms/common/internal/s;->k(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$14;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$14;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    return-object v0
.end method

.method public verifyPin(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;
    .locals 1
    .param p1, "request"    # Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;

    .prologue
    invoke-static {p1}, Lcom/google/android/gms/common/internal/s;->k(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$15;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$15;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;->a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient$a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    return-object v0
.end method
