.class public Lcom/google/android/gms/internal/nf;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/internal/nf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final avd:I

.field public final ave:Lcom/google/android/gms/internal/nh;

.field public final avf:Lcom/google/android/gms/internal/nn;

.field public final avg:Lcom/google/android/gms/internal/nl;

.field public final avh:Lcom/google/android/gms/internal/np;

.field private final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ng;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ng;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/nf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILcom/google/android/gms/internal/nh;Lcom/google/android/gms/internal/nn;Lcom/google/android/gms/internal/nl;Lcom/google/android/gms/internal/np;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/nf;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/internal/nf;->avd:I

    iput-object p3, p0, Lcom/google/android/gms/internal/nf;->ave:Lcom/google/android/gms/internal/nh;

    iput-object p4, p0, Lcom/google/android/gms/internal/nf;->avf:Lcom/google/android/gms/internal/nn;

    iput-object p5, p0, Lcom/google/android/gms/internal/nf;->avg:Lcom/google/android/gms/internal/nl;

    iput-object p6, p0, Lcom/google/android/gms/internal/nf;->avh:Lcom/google/android/gms/internal/np;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/nf;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ng;->a(Lcom/google/android/gms/internal/nf;Landroid/os/Parcel;I)V

    return-void
.end method
