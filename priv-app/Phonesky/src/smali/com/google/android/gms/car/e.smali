.class public Lcom/google/android/gms/car/e;
.super Lcom/google/android/gms/common/internal/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/car/e$b;,
        Lcom/google/android/gms/car/e$c;,
        Lcom/google/android/gms/car/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/h",
        "<",
        "Lcom/google/android/gms/car/aa;",
        ">;"
    }
.end annotation


# instance fields
.field private final KU:Ljava/lang/Object;

.field private KV:Lcom/google/android/gms/car/CarAudioManager;

.field private KW:Lcom/google/android/gms/car/CarSensorManager;

.field private KX:Lcom/google/android/gms/car/CarNavigationStatusManager;

.field private KY:Lcom/google/android/gms/car/CarMediaManager;

.field private KZ:Lcom/google/android/gms/car/CarCallManager;

.field private final La:Ljava/util/HashMap;

.field private Lb:Lcom/google/android/gms/car/CarMessageManager;

.field private Lc:Lcom/google/android/gms/car/CarBluetoothConnectionManager;

.field private final Ld:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final Le:Lcom/google/android/gms/car/e$c;

.field private final Lf:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

.field private Lg:Lcom/google/android/gms/car/e$b;

.field private Lh:Landroid/os/IBinder$DeathRecipient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/Car$CarConnectionListener;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .locals 7

    const/4 v6, 0x0

    new-array v5, v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/h;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KU:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->La:Ljava/util/HashMap;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Ld:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/google/android/gms/car/e$c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/e$c;-><init>(Lcom/google/android/gms/car/e;)V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    new-instance v0, Lcom/google/android/gms/car/e$a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/e$a;-><init>(Lcom/google/android/gms/car/e;)V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Lf:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lf:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/e;->registerConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)V

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/e$c;->a(Lcom/google/android/gms/car/Car$CarConnectionListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/e;)Lcom/google/android/gms/car/e$c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    return-object v0
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/car/CarNotConnectedException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/e;->b(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/CarNotConnectedException;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarNotConnectedException;-><init>()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/e;Landroid/os/RemoteException;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/e;->b(Landroid/os/RemoteException;)V

    return-void
.end method

.method private b(Landroid/os/RemoteException;)V
    .locals 3

    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.CLIENT"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Remote exception from car service:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Ld:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.CLIENT"

    const-string v1, "Already handling a remote exception, ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    invoke-virtual {v0}, Lcom/google/android/gms/car/e$c;->onDisconnected()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->disconnect()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/e;->gv()V

    return-void
.end method

.method public static b(Ljava/lang/IllegalStateException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/car/CarNotConnectedException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CarNotConnected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/CarNotConnectedException;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarNotConnectedException;-><init>()V

    throw v0

    :cond_0
    throw p0
.end method

.method static synthetic c(Lcom/google/android/gms/car/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/e;->handleCarDisconnection()V

    return-void
.end method

.method private gu()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/car/e;->KU:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->KV:Lcom/google/android/gms/car/CarAudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/e;->KV:Lcom/google/android/gms/car/CarAudioManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarAudioManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KV:Lcom/google/android/gms/car/CarAudioManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->KW:Lcom/google/android/gms/car/CarSensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/e;->KW:Lcom/google/android/gms/car/CarSensorManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarSensorManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KW:Lcom/google/android/gms/car/CarSensorManager;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lb:Lcom/google/android/gms/car/CarMessageManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lb:Lcom/google/android/gms/car/CarMessageManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarMessageManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Lb:Lcom/google/android/gms/car/CarMessageManager;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lc:Lcom/google/android/gms/car/CarBluetoothConnectionManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lc:Lcom/google/android/gms/car/CarBluetoothConnectionManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarBluetoothConnectionManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Lc:Lcom/google/android/gms/car/CarBluetoothConnectionManager;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/e;->KX:Lcom/google/android/gms/car/CarNavigationStatusManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/e;->KX:Lcom/google/android/gms/car/CarNavigationStatusManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarNavigationStatusManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KX:Lcom/google/android/gms/car/CarNavigationStatusManager;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/e;->KY:Lcom/google/android/gms/car/CarMediaManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/e;->KY:Lcom/google/android/gms/car/CarMediaManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarMediaManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KY:Lcom/google/android/gms/car/CarMediaManager;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/e;->KZ:Lcom/google/android/gms/car/CarCallManager;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/e;->KZ:Lcom/google/android/gms/car/CarCallManager;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarCallManager;->handleCarDisconnection()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/e;->KZ:Lcom/google/android/gms/car/CarCallManager;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/e;->La:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized gv()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/e$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/e$1;-><init>(Lcom/google/android/gms/car/e;)V

    iput-object v0, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    invoke-interface {v0}, Lcom/google/android/gms/car/aa;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.CLIENT"

    const-string v1, "Unable to link death recipient to ICar."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized gw()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    invoke-interface {v0}, Lcom/google/android/gms/car/aa;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/car/e;->Lh:Landroid/os/IBinder$DeathRecipient;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "CAR.CLIENT"

    const-string v1, "Unable to unlink death recipient from ICar."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private handleCarDisconnection()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/e;->gu()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/internal/p;Lcom/google/android/gms/common/internal/h$e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    const-string v2, "car-1-0"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x645b68

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/p;->o(Lcom/google/android/gms/common/internal/o;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected aa(Landroid/os/IBinder;)Lcom/google/android/gms/car/aa;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/car/aa$a;->ab(Landroid/os/IBinder;)Lcom/google/android/gms/car/aa;

    move-result-object v0

    return-object v0
.end method

.method public connect()V
    .locals 2

    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.CLIENT"

    const-string v1, "connect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/h;->connect()V

    return-void
.end method

.method public disconnect()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/j;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.CLIENT"

    const-string v1, "disconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/e;->gu()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    iget-object v1, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/aa;->b(Lcom/google/android/gms/car/am;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/e;->Le:Lcom/google/android/gms/car/e$c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/e$c;->a(Lcom/google/android/gms/car/Car$CarConnectionListener;)V

    invoke-direct {p0}, Lcom/google/android/gms/car/e;->gw()V

    iget-object v0, p0, Lcom/google/android/gms/car/e;->Lg:Lcom/google/android/gms/car/e$b;

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    iget-object v1, p0, Lcom/google/android/gms/car/e;->Lg:Lcom/google/android/gms/car/e$b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/aa;->b(Lcom/google/android/gms/car/ab;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/car/e;->Lg:Lcom/google/android/gms/car/e$b;

    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/common/internal/h;->disconnect()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected getServiceDescriptor()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.car.ICar"

    return-object v0
.end method

.method protected getStartServiceAction()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.car.service.START"

    return-object v0
.end method

.method public gg()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    invoke-interface {v0}, Lcom/google/android/gms/car/aa;->gg()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/e;->b(Landroid/os/RemoteException;)V

    move v0, v1

    goto :goto_0
.end method

.method public gh()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/car/CarNotConnectedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->dP()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/e;->iI()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/aa;

    invoke-interface {v0}, Lcom/google/android/gms/car/aa;->gh()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/e;->a(Landroid/os/RemoteException;)V

    :goto_1
    const/4 v0, -0x1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/car/e;->b(Ljava/lang/IllegalStateException;)V

    goto :goto_1
.end method

.method protected synthetic p(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/e;->aa(Landroid/os/IBinder;)Lcom/google/android/gms/car/aa;

    move-result-object v0

    return-object v0
.end method
