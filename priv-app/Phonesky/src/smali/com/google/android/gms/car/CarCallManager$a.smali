.class Lcom/google/android/gms/car/CarCallManager$a;
.super Lcom/google/android/gms/car/al$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/car/CarCallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic KE:Lcom/google/android/gms/car/CarCallManager;


# virtual methods
.method public dispatchPhoneKeyEvent(Landroid/view/KeyEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "dispatchPhoneKeyEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/gms/car/CarCallManager$a$1;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Landroid/view/KeyEvent;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onAudioStateChanged(ZII)V
    .locals 8
    .param p1, "isMuted"    # Z
    .param p2, "route"    # I
    .param p3, "supportedRouteMask"    # I

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onAudioStateChanged isMuted=%b\troute=%d\tsupportedRoutes=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/CarCallListener;

    new-instance v0, Lcom/google/android/gms/car/CarCallManager$a$5;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarCallManager$a$5;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;ZII)V

    invoke-static {v0}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onCallAdded(Lcom/google/android/gms/car/CarCall;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCallAdded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$6;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/gms/car/CarCallManager$a$6;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onCallDestroyed(Lcom/google/android/gms/car/CarCall;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onCallDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$3;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/gms/car/CarCallManager$a$3;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onCallRemoved(Lcom/google/android/gms/car/CarCall;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCallRemoved "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$7;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/gms/car/CarCallManager$a$7;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onCannedTextResponsesLoaded(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "cannedTextResponses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onCannedTextResponsesLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$12;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$12;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onChildrenChanged(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "children":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/car/CarCall;>;"
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onChildrenChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$10;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$10;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onConferenceableCallsChanged(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/car/CarCall;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "conferenceableCalls":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/car/CarCall;>;"
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onConferenceableCallsChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$4;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$4;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onDetailsChanged(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "details"    # Lcom/google/android/gms/car/CarCall$Details;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onDetailsChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$11;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$11;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onParentChanged(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "parent"    # Lcom/google/android/gms/car/CarCall;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onParentChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$9;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$9;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onPostDialWait(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "remainingPostDialSequence"    # Ljava/lang/String;

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v1, "onPostDialWait"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$2;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$2;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onStateChanged(Lcom/google/android/gms/car/CarCall;I)V
    .locals 4
    .param p1, "call"    # Lcom/google/android/gms/car/CarCall;
    .param p2, "state"    # I

    .prologue
    const-string v0, "CAR.TEL.CarCallManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarCallManager$a;->KE:Lcom/google/android/gms/car/CarCallManager;

    invoke-static {v0}, Lcom/google/android/gms/car/CarCallManager;->a(Lcom/google/android/gms/car/CarCallManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCallListener;

    new-instance v3, Lcom/google/android/gms/car/CarCallManager$a$8;

    invoke-direct {v3, p0, v0, p1, p2}, Lcom/google/android/gms/car/CarCallManager$a$8;-><init>(Lcom/google/android/gms/car/CarCallManager$a;Lcom/google/android/gms/car/CarCallListener;Lcom/google/android/gms/car/CarCall;I)V

    invoke-static {v3}, Lcom/google/android/gms/car/bg;->c(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
