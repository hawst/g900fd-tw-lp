.class public Lcom/google/android/gms/udc/SettingState;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/udc/b;


# instance fields
.field private aRg:I

.field private aRh:I

.field private final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/udc/b;

    invoke-direct {v0}, Lcom/google/android/gms/udc/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/udc/SettingState;->mVersionCode:I

    return-void
.end method

.method constructor <init>(III)V
    .locals 0
    .param p1, "versionCode"    # I
    .param p2, "settingId"    # I
    .param p3, "settingValue"    # I

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/udc/SettingState;->mVersionCode:I

    iput p2, p0, Lcom/google/android/gms/udc/SettingState;->aRg:I

    iput p3, p0, Lcom/google/android/gms/udc/SettingState;->aRh:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    const/4 v0, 0x0

    return v0
.end method

.method public getSettingId()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->aRg:I

    return v0
.end method

.method public getSettingValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->aRh:I

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->mVersionCode:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/udc/b;->a(Lcom/google/android/gms/udc/SettingState;Landroid/os/Parcel;I)V

    return-void
.end method
