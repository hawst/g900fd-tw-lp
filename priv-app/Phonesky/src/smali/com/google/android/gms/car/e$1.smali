.class Lcom/google/android/gms/car/e$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/car/e;->gv()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic Li:Lcom/google/android/gms/car/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/e$1;->Li:Lcom/google/android/gms/car/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    const-string v0, "CAR.CLIENT"

    const-string v1, "ICar died!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/e$1;->Li:Lcom/google/android/gms/car/e;

    invoke-static {v0}, Lcom/google/android/gms/car/e;->a(Lcom/google/android/gms/car/e;)Lcom/google/android/gms/car/e$c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/e$c;->onDisconnected()V

    return-void
.end method
