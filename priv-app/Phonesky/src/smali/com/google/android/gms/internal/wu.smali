.class public abstract Lcom/google/android/gms/internal/wu;
.super Lcom/google/android/gms/internal/xa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/android/gms/internal/wu",
        "<TM;>;>",
        "Lcom/google/android/gms/internal/xa;"
    }
.end annotation


# instance fields
.field protected aYu:Lcom/google/android/gms/internal/ww;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/xa;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/internal/wu;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ww;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ww;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    iget-object v1, p1, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ww;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method protected c()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/ww;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/ww;->mq(I)Lcom/google/android/gms/internal/wx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/internal/wx;->c()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    return v1
.end method

.method protected final vB()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ww;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ww;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public writeTo(Lcom/google/android/gms/internal/wt;)V
    .locals 2
    .param p1, "output"    # Lcom/google/android/gms/internal/wt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/gms/internal/wu;, "Lcom/google/android/gms/internal/wu<TM;>;"
    iget-object v0, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/ww;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/wu;->aYu:Lcom/google/android/gms/internal/ww;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ww;->mq(I)Lcom/google/android/gms/internal/wx;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/wx;->writeTo(Lcom/google/android/gms/internal/wt;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
