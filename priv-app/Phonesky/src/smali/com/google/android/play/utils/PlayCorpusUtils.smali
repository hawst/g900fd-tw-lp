.class public Lcom/google/android/play/utils/PlayCorpusUtils;
.super Ljava/lang/Object;
.source "PlayCorpusUtils.java"


# direct methods
.method public static getPlayActionButtonBackgroundDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "channelId"    # I

    .prologue
    .line 41
    packed-switch p1, :pswitch_data_0

    .line 53
    :pswitch_0
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_multi:I

    :goto_0
    return v0

    .line 43
    :pswitch_1
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_apps:I

    goto :goto_0

    .line 45
    :pswitch_2
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_books:I

    goto :goto_0

    .line 47
    :pswitch_3
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_newsstand:I

    goto :goto_0

    .line 49
    :pswitch_4
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_movies:I

    goto :goto_0

    .line 51
    :pswitch_5
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_music:I

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPlayActionButtonBackgroundSecondaryDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "channelId"    # I

    .prologue
    .line 59
    packed-switch p1, :pswitch_data_0

    .line 71
    :pswitch_0
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_multi_secondary:I

    :goto_0
    return v0

    .line 61
    :pswitch_1
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_apps_secondary:I

    goto :goto_0

    .line 63
    :pswitch_2
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_books_secondary:I

    goto :goto_0

    .line 65
    :pswitch_3
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_newsstand_secondary:I

    goto :goto_0

    .line 67
    :pswitch_4
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_movies_secondary:I

    goto :goto_0

    .line 69
    :pswitch_5
    sget v0, Lcom/google/android/play/R$drawable;->play_action_button_music_secondary:I

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 17
    packed-switch p1, :pswitch_data_0

    .line 34
    :pswitch_0
    sget v0, Lcom/google/android/play/R$color;->play_multi_primary_text:I

    .line 37
    .local v0, "colorResourceId":I
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    return-object v1

    .line 19
    .end local v0    # "colorResourceId":I
    :pswitch_1
    sget v0, Lcom/google/android/play/R$color;->play_apps_primary_text:I

    .line 20
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 22
    .end local v0    # "colorResourceId":I
    :pswitch_2
    sget v0, Lcom/google/android/play/R$color;->play_books_primary_text:I

    .line 23
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 25
    .end local v0    # "colorResourceId":I
    :pswitch_3
    sget v0, Lcom/google/android/play/R$color;->play_newsstand_primary_text:I

    .line 26
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 28
    .end local v0    # "colorResourceId":I
    :pswitch_4
    sget v0, Lcom/google/android/play/R$color;->play_movies_primary_text:I

    .line 29
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 31
    .end local v0    # "colorResourceId":I
    :pswitch_5
    sget v0, Lcom/google/android/play/R$color;->play_music_primary_text:I

    .line 32
    .restart local v0    # "colorResourceId":I
    goto :goto_0

    .line 17
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
