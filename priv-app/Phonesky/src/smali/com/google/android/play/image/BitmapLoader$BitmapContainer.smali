.class public Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/image/BitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BitmapContainer"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapLoaded:Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

.field private final mModifiedUrl:Ljava/lang/String;

.field private final mRequestHeight:I

.field private final mRequestUrl:Ljava/lang/String;

.field private final mRequestWidth:I

.field final synthetic this$0:Lcom/google/android/play/image/BitmapLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/BitmapLoader;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)V
    .locals 0
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "requestUrl"    # Ljava/lang/String;
    .param p4, "modifiedUrl"    # Ljava/lang/String;
    .param p5, "requestWidth"    # I
    .param p6, "requestHeight"    # I
    .param p7, "handler"    # Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->this$0:Lcom/google/android/play/image/BitmapLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput-object p2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmap:Landroid/graphics/Bitmap;

    .line 411
    iput-object p3, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestUrl:Ljava/lang/String;

    .line 412
    iput-object p4, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mModifiedUrl:Ljava/lang/String;

    .line 413
    iput p5, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestWidth:I

    .line 414
    iput p6, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestHeight:I

    .line 415
    iput-object p7, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmapLoaded:Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    .line 416
    return-void
.end method

.method static synthetic access$802(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 391
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmapLoaded:Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    return-object v0
.end method


# virtual methods
.method public cancelRequest()V
    .locals 4

    .prologue
    .line 422
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmapLoaded:Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    if-nez v2, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/google/android/play/image/BitmapLoader;->access$500(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mModifiedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 427
    .local v1, "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    if-eqz v1, :cond_2

    .line 428
    invoke-virtual {v1, p0}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->removeHandlerAndCancelIfNecessary(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)Z

    move-result v0

    .line 429
    .local v0, "canceled":Z
    if-eqz v0, :cond_0

    .line 430
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/google/android/play/image/BitmapLoader;->access$500(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mModifiedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 434
    .end local v0    # "canceled":Z
    :cond_2
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/google/android/play/image/BitmapLoader;->access$600(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mModifiedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    check-cast v1, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 435
    .restart local v1    # "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    if-eqz v1, :cond_0

    .line 436
    invoke-virtual {v1, p0}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->removeHandlerAndCancelIfNecessary(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)Z

    .line 437
    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->handlers:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$700(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 438
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->this$0:Lcom/google/android/play/image/BitmapLoader;

    # getter for: Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/google/android/play/image/BitmapLoader;->access$600(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mModifiedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRequestHeight()I
    .locals 1

    .prologue
    .line 470
    iget v0, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestHeight:I

    return v0
.end method

.method public getRequestUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestWidth()I
    .locals 1

    .prologue
    .line 463
    iget v0, p0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->mRequestWidth:I

    return v0
.end method
