.class abstract Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;
.super Landroid/view/animation/Animation;
.source "PlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "FloatAnimation"
.end annotation


# instance fields
.field private final mFrom:F

.field private final mTo:F

.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method protected constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V
    .locals 0
    .param p2, "from"    # F
    .param p3, "to"    # F

    .prologue
    .line 3378
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 3379
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->mFrom:F

    .line 3380
    iput p3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->mTo:F

    .line 3381
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 3385
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 3387
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->mTo:F

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->mFrom:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->mFrom:F

    add-float v0, v1, v2

    .line 3388
    .local v0, "value":F
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;->setValue(F)V

    .line 3389
    return-void
.end method

.method protected abstract setValue(F)V
.end method
