.class public Lcom/google/android/play/image/AvatarCropTransformation;
.super Ljava/lang/Object;
.source "AvatarCropTransformation.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapTransformation;


# static fields
.field private static sInstance:Lcom/google/android/play/image/AvatarCropTransformation;


# instance fields
.field private final mCropPaint:Landroid/graphics/Paint;

.field private final mDecorationThresholdMax:I

.field private final mDecorationThresholdMin:I

.field private final mDefaultOutlinePaint:Landroid/graphics/Paint;

.field private final mDefaultRingPaint:Landroid/graphics/Paint;

.field private final mDestinationRectangle:Landroid/graphics/Rect;

.field private final mDrawRingAboveThreshold:Z

.field private final mDropShadowSizeMax:F

.field private final mDropShadowSizeMin:F

.field private final mFillToSizePaint:Landroid/graphics/Paint;

.field private final mFocusedOutlineColor:I

.field private mForceFill:Z

.field private final mHighlightOutlinePaint:Landroid/graphics/Paint;

.field private final mPressedFillPaint:Landroid/graphics/Paint;

.field private final mPressedOutlineColor:I

.field private final mRectangle:Landroid/graphics/RectF;

.field private final mResizePaint:Landroid/graphics/Paint;

.field private final mRingSizeMax:I

.field private final mRingSizeMin:I

.field private final mSourceRectangle:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Z)V
    .locals 7
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "drawRingAboveThreshold"    # Z

    .prologue
    const/4 v6, 0x2

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mForceFill:Z

    .line 105
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_decoration_threshold_min:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    .line 107
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_decoration_threshold_max:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMax:I

    .line 109
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_ring_size_min:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRingSizeMin:I

    .line 110
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_ring_size_max:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRingSizeMax:I

    .line 113
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_drop_shadow_min:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDropShadowSizeMin:F

    .line 115
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_drop_shadow_max:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDropShadowSizeMax:F

    .line 118
    sget v3, Lcom/google/android/play/R$color;->play_avatar_outline:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 119
    .local v0, "outlineColor":I
    sget v3, Lcom/google/android/play/R$color;->play_white:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 123
    .local v2, "ringColor":I
    sget v3, Lcom/google/android/play/R$dimen;->play_avatar_noring_outline:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    mul-float v1, v4, v3

    .line 125
    .local v1, "outlineStrokeWidth":F
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    .line 126
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 131
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    .line 132
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 136
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mCropPaint:Landroid/graphics/Paint;

    .line 137
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mCropPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 139
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mResizePaint:Landroid/graphics/Paint;

    .line 141
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    .line 143
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedFillPaint:Landroid/graphics/Paint;

    .line 144
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedFillPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/play/R$color;->play_avatar_pressed_fill:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 146
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedFillPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 148
    sget v3, Lcom/google/android/play/R$color;->play_avatar_pressed_outline:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedOutlineColor:I

    .line 149
    sget v3, Lcom/google/android/play/R$color;->play_avatar_focused_outline:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFocusedOutlineColor:I

    .line 151
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    .line 152
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 153
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 154
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 156
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFillToSizePaint:Landroid/graphics/Paint;

    .line 157
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFillToSizePaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/play/R$color;->play_white:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFillToSizePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mSourceRectangle:Landroid/graphics/Rect;

    .line 161
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDestinationRectangle:Landroid/graphics/Rect;

    .line 163
    iput-boolean p2, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDrawRingAboveThreshold:Z

    .line 164
    return-void
.end method

.method private drawAvatar(Landroid/graphics/Canvas;IFFF)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "sourceSize"    # I
    .param p3, "cornerRadius"    # F
    .param p4, "extraInset"    # F
    .param p5, "scale"    # F

    .prologue
    const/4 v5, 0x0

    .line 349
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v1, v4, Landroid/graphics/RectF;->left:F

    .line 350
    .local v1, "origLeft":F
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v2, v4, Landroid/graphics/RectF;->right:F

    .line 351
    .local v2, "origRight":F
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v3, v4, Landroid/graphics/RectF;->top:F

    .line 352
    .local v3, "origTop":F
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 354
    .local v0, "origBottom":F
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v5, v4, Landroid/graphics/RectF;->left:F

    .line 355
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v5, v4, Landroid/graphics/RectF;->top:F

    .line 356
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    int-to-float v5, p2

    iput v5, v4, Landroid/graphics/RectF;->right:F

    .line 357
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    int-to-float v5, p2

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    .line 359
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 362
    invoke-virtual {p1, p5, p5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 365
    invoke-virtual {p1, p4, p4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 368
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mCropPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, p3, p3, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 369
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 372
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v1, v4, Landroid/graphics/RectF;->left:F

    .line 373
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v2, v4, Landroid/graphics/RectF;->right:F

    .line 374
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v3, v4, Landroid/graphics/RectF;->top:F

    .line 375
    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    .line 376
    return-void
.end method

.method private drawOutline(Landroid/graphics/Canvas;FI)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "dropShadowRadius"    # F
    .param p3, "dropShadowColor"    # I

    .prologue
    const/high16 v9, 0x40400000    # 3.0f

    const/high16 v8, 0x40000000    # 2.0f

    .line 380
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v2, v6, Landroid/graphics/RectF;->left:F

    .line 381
    .local v2, "origLeft":F
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    .line 382
    .local v3, "origRight":F
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v4, v6, Landroid/graphics/RectF;->top:F

    .line 383
    .local v4, "origTop":F
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    .line 387
    .local v1, "origBottom":F
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    mul-float v7, v8, p2

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 388
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v5

    .line 389
    .local v5, "outline":F
    div-float v0, v5, v8

    .line 390
    .local v0, "inset":F
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->left:F

    div-float v8, p2, v9

    sub-float v8, v0, v8

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 391
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->top:F

    add-float v8, v0, p2

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 392
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->right:F

    div-float v8, p2, v9

    sub-float v8, v0, v8

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->right:F

    .line 393
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->bottom:F

    sub-float v8, v0, p2

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    .line 394
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v6, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 395
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 398
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v2, v6, Landroid/graphics/RectF;->left:F

    .line 399
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v3, v6, Landroid/graphics/RectF;->right:F

    .line 400
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v4, v6, Landroid/graphics/RectF;->top:F

    .line 401
    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    .line 402
    return-void
.end method

.method private drawRing(Landroid/graphics/Canvas;F)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "ringOutline"    # F

    .prologue
    .line 406
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v2, v5, Landroid/graphics/RectF;->left:F

    .line 407
    .local v2, "origLeft":F
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v3, v5, Landroid/graphics/RectF;->right:F

    .line 408
    .local v3, "origRight":F
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v4, v5, Landroid/graphics/RectF;->top:F

    .line 409
    .local v4, "origTop":F
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v1, v5, Landroid/graphics/RectF;->bottom:F

    .line 413
    .local v1, "origBottom":F
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 414
    const/high16 v5, 0x40000000    # 2.0f

    div-float v0, p2, v5

    .line 415
    .local v0, "inset":F
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v0

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 416
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v0

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 417
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v6, v0

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 418
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v6, v0

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 419
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDefaultRingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 422
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v2, v5, Landroid/graphics/RectF;->left:F

    .line 423
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v3, v5, Landroid/graphics/RectF;->right:F

    .line 424
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v4, v5, Landroid/graphics/RectF;->top:F

    .line 425
    iget-object v5, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    iput v1, v5, Landroid/graphics/RectF;->bottom:F

    .line 426
    return-void
.end method

.method private getDropShadowSize(II)F
    .locals 7
    .param p1, "avatarWidth"    # I
    .param p2, "avatarHeight"    # I

    .prologue
    .line 176
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 177
    .local v6, "avatarSize":I
    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    if-ge v6, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMax:I

    int-to-float v2, v0

    iget v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDropShadowSizeMin:F

    iget v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDropShadowSizeMax:F

    int-to-float v5, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/image/AvatarCropTransformation;->interpolate(FFFFF)F

    move-result v0

    goto :goto_0
.end method

.method public static declared-synchronized getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;
    .locals 3
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 98
    const-class v1, Lcom/google/android/play/image/AvatarCropTransformation;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/play/image/AvatarCropTransformation;->sInstance:Lcom/google/android/play/image/AvatarCropTransformation;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/android/play/image/AvatarCropTransformation;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v2}, Lcom/google/android/play/image/AvatarCropTransformation;-><init>(Landroid/content/res/Resources;Z)V

    sput-object v0, Lcom/google/android/play/image/AvatarCropTransformation;->sInstance:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 101
    :cond_0
    sget-object v0, Lcom/google/android/play/image/AvatarCropTransformation;->sInstance:Lcom/google/android/play/image/AvatarCropTransformation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getRingOutlineSize(II)F
    .locals 7
    .param p1, "avatarWidth"    # I
    .param p2, "avatarHeight"    # I

    .prologue
    .line 189
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 190
    .local v6, "avatarSize":I
    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMax:I

    int-to-float v2, v0

    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRingSizeMin:I

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mRingSizeMax:I

    int-to-float v4, v0

    int-to-float v5, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/image/AvatarCropTransformation;->interpolate(FFFFF)F

    move-result v0

    return v0
.end method

.method private interpolate(FFFFF)F
    .locals 2
    .param p1, "fromRangeMin"    # F
    .param p2, "fromRangeMax"    # F
    .param p3, "toRangeMin"    # F
    .param p4, "toRangeMax"    # F
    .param p5, "from"    # F

    .prologue
    .line 332
    cmpg-float v0, p5, p1

    if-gtz v0, :cond_1

    .line 342
    .end local p3    # "toRangeMin":F
    :cond_0
    :goto_0
    return p3

    .line 335
    .restart local p3    # "toRangeMin":F
    :cond_1
    cmpl-float v0, p5, p2

    if-ltz v0, :cond_2

    move p3, p4

    .line 336
    goto :goto_0

    .line 338
    :cond_2
    cmpl-float v0, p1, p2

    if-eqz v0, :cond_0

    .line 342
    sub-float v0, p5, p1

    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    add-float/2addr p3, v0

    goto :goto_0
.end method


# virtual methods
.method public drawFocusedOverlay(Landroid/graphics/Canvas;II)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 431
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/image/AvatarCropTransformation;->getDropShadowSize(II)F

    move-result v0

    .line 432
    .local v0, "offset":F
    int-to-float v3, p2

    sub-float/2addr v3, v0

    float-to-int p2, v3

    .line 433
    int-to-float v3, p3

    sub-float/2addr v3, v0

    float-to-int p3, v3

    .line 435
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 436
    div-float v3, v0, v5

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 438
    int-to-float v3, p2

    div-float/2addr v3, v5

    int-to-float v4, p3

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 439
    .local v2, "overlayRadius":F
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 440
    .local v1, "outline":F
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFocusedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 441
    div-float v3, v1, v5

    sub-float v3, v2, v3

    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 444
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 445
    return-void
.end method

.method public drawPressedOverlay(Landroid/graphics/Canvas;II)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 450
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/image/AvatarCropTransformation;->getDropShadowSize(II)F

    move-result v0

    .line 451
    .local v0, "offset":F
    int-to-float v3, p2

    sub-float/2addr v3, v0

    float-to-int p2, v3

    .line 452
    int-to-float v3, p3

    sub-float/2addr v3, v0

    float-to-int p3, v3

    .line 454
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 455
    div-float v3, v0, v5

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 457
    int-to-float v3, p2

    div-float/2addr v3, v5

    int-to-float v4, p3

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 458
    .local v2, "overlayRadius":F
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v2, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 459
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 460
    .local v1, "outline":F
    iget-object v3, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mPressedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 461
    div-float v3, v1, v5

    sub-float v3, v2, v3

    iget-object v4, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mHighlightOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 465
    return-void
.end method

.method public getTransformationInset(II)I
    .locals 3
    .param p1, "outputWidth"    # I
    .param p2, "outputHeight"    # I

    .prologue
    .line 196
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 197
    .local v0, "avatarSize":I
    iget v1, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    if-ge v0, v1, :cond_0

    .line 198
    const/4 v1, 0x0

    .line 203
    :goto_0
    return v1

    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/AvatarCropTransformation;->getRingOutlineSize(II)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/AvatarCropTransformation;->getDropShadowSize(II)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    goto :goto_0
.end method

.method public setFillToSizeColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mFillToSizePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/AvatarCropTransformation;->mForceFill:Z

    .line 470
    return-void
.end method

.method public transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 37
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "outputWidth"    # I
    .param p3, "outputHeight"    # I

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 210
    const/16 v31, 0x0

    .line 319
    :goto_0
    return-object v31

    .line 213
    :cond_0
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->max(II)I

    move-result v16

    .line 216
    .local v16, "avatarSize":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    move/from16 v0, v16

    if-lt v0, v4, :cond_4

    const/16 v23, 0x1

    .line 219
    .local v23, "drawDecorations":Z
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDrawRingAboveThreshold:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getRingOutlineSize(II)F

    move-result v30

    .line 222
    .local v30, "ringOutline":F
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getDropShadowSize(II)F

    move-result v26

    .line 224
    .local v26, "dropShadowSize":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMin:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDecorationThresholdMax:I

    int-to-float v5, v8

    const/high16 v6, 0x42400000    # 48.0f

    const/high16 v7, 0x42800000    # 64.0f

    move/from16 v0, v16

    int-to-float v8, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/play/image/AvatarCropTransformation;->interpolate(FFFFF)F

    move-result v4

    float-to-int v0, v4

    move/from16 v24, v0

    .line 227
    .local v24, "dropShadowAlpha":I
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getTransformationInset(II)I

    move-result v36

    .line 230
    .local v36, "transformationInset":I
    shl-int/lit8 v25, v24, 0x18

    .line 237
    .local v25, "dropShadowColor":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v35

    .line 238
    .local v35, "sourceWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v34

    .line 239
    .local v34, "sourceHeight":I
    move/from16 v0, v35

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v28

    .line 240
    .local v28, "largerSourceSize":I
    move/from16 v0, v35

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v33

    .line 241
    .local v33, "smallerSourceSize":I
    sub-int v4, v35, v34

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v8, 0x1

    if-gt v4, v8, :cond_1

    sub-int v4, v16, v36

    move/from16 v0, v28

    if-lt v0, v4, :cond_1

    move/from16 v0, v33

    move/from16 v1, v16

    if-gt v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mForceFill:Z

    if-eqz v4, :cond_7

    .line 244
    :cond_1
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    move/from16 v1, v16

    invoke-static {v0, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 245
    .local v29, "padded":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, v29

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 247
    .local v3, "paddedCanvas":Landroid/graphics/Canvas;
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v16

    int-to-float v6, v0

    move/from16 v0, v16

    int-to-float v7, v0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mFillToSizePaint:Landroid/graphics/Paint;

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mSourceRectangle:Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move/from16 v0, v35

    move/from16 v1, v34

    invoke-virtual {v4, v8, v10, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 254
    move/from16 v0, v16

    int-to-float v4, v0

    move/from16 v0, v35

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    int-to-float v8, v8

    div-float v32, v4, v8

    .line 256
    .local v32, "scaleFactor":F
    move/from16 v0, v35

    int-to-float v4, v0

    mul-float v4, v4, v32

    float-to-int v0, v4

    move/from16 v20, v0

    .line 257
    .local v20, "destinationWidth":I
    move/from16 v0, v34

    int-to-float v4, v0

    mul-float v4, v4, v32

    float-to-int v0, v4

    move/from16 v19, v0

    .line 258
    .local v19, "destinationHeight":I
    sub-int v4, v16, v20

    div-int/lit8 v21, v4, 0x2

    .line 259
    .local v21, "destinationX":I
    sub-int v4, v16, v19

    div-int/lit8 v22, v4, 0x2

    .line 260
    .local v22, "destinationY":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDestinationRectangle:Landroid/graphics/Rect;

    add-int v8, v21, v20

    add-int v10, v22, v19

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v4, v0, v1, v8, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 263
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mSourceRectangle:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDestinationRectangle:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mResizePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v4, v8, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 264
    move-object/from16 p1, v29

    .line 265
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 266
    .end local v35    # "sourceWidth":I
    .local v6, "sourceWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v34

    .line 269
    .end local v3    # "paddedCanvas":Landroid/graphics/Canvas;
    .end local v19    # "destinationHeight":I
    .end local v20    # "destinationWidth":I
    .end local v21    # "destinationX":I
    .end local v22    # "destinationY":I
    .end local v29    # "padded":Landroid/graphics/Bitmap;
    .end local v32    # "scaleFactor":F
    :goto_3
    new-instance v17, Landroid/graphics/BitmapShader;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v8}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 271
    .local v17, "bitmapShader":Landroid/graphics/BitmapShader;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mCropPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 273
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    move/from16 v1, v16

    invoke-static {v0, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 274
    .local v31, "round":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    move-object/from16 v0, v31

    invoke-direct {v5, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 275
    .local v5, "roundCanvas":Landroid/graphics/Canvas;
    move/from16 v0, v16

    int-to-float v4, v0

    const/high16 v8, 0x40000000    # 2.0f

    div-float v7, v4, v8

    .line 279
    .local v7, "cornerRadius":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 280
    .local v18, "boundsDelta":F
    if-eqz v23, :cond_2

    .line 286
    add-float v18, v18, v26

    .line 290
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v26, v4

    const/4 v8, 0x0

    invoke-virtual {v5, v4, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 292
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mRectangle:Landroid/graphics/RectF;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move/from16 v0, p2

    int-to-float v11, v0

    sub-float v11, v11, v18

    move/from16 v0, p2

    int-to-float v12, v0

    sub-float v12, v12, v18

    invoke-virtual {v4, v8, v10, v11, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 295
    if-eqz v23, :cond_6

    .line 297
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-direct {v0, v5, v1, v2}, Lcom/google/android/play/image/AvatarCropTransformation;->drawOutline(Landroid/graphics/Canvas;FI)V

    .line 302
    move/from16 v0, v16

    int-to-float v4, v0

    sub-float v4, v4, v26

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v10, 0x40000000    # 2.0f

    mul-float v10, v10, v30

    invoke-static {v8, v10}, Ljava/lang/Math;->max(FF)F

    move-result v8

    sub-float v27, v4, v8

    .line 303
    .local v27, "finalAvatarSize":F
    int-to-float v4, v6

    div-float v9, v27, v4

    .local v9, "scale":F
    move-object/from16 v4, p0

    move/from16 v8, v30

    .line 304
    invoke-direct/range {v4 .. v9}, Lcom/google/android/play/image/AvatarCropTransformation;->drawAvatar(Landroid/graphics/Canvas;IFFF)V

    .line 306
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mDrawRingAboveThreshold:Z

    if-eqz v4, :cond_3

    .line 307
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-direct {v0, v5, v1}, Lcom/google/android/play/image/AvatarCropTransformation;->drawRing(Landroid/graphics/Canvas;F)V

    .line 317
    .end local v9    # "scale":F
    .end local v27    # "finalAvatarSize":F
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/image/AvatarCropTransformation;->mCropPaint:Landroid/graphics/Paint;

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto/16 :goto_0

    .line 216
    .end local v5    # "roundCanvas":Landroid/graphics/Canvas;
    .end local v6    # "sourceWidth":I
    .end local v7    # "cornerRadius":F
    .end local v17    # "bitmapShader":Landroid/graphics/BitmapShader;
    .end local v18    # "boundsDelta":F
    .end local v23    # "drawDecorations":Z
    .end local v24    # "dropShadowAlpha":I
    .end local v25    # "dropShadowColor":I
    .end local v26    # "dropShadowSize":F
    .end local v28    # "largerSourceSize":I
    .end local v30    # "ringOutline":F
    .end local v31    # "round":Landroid/graphics/Bitmap;
    .end local v33    # "smallerSourceSize":I
    .end local v34    # "sourceHeight":I
    .end local v36    # "transformationInset":I
    :cond_4
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 219
    .restart local v23    # "drawDecorations":Z
    :cond_5
    const/16 v30, 0x0

    goto/16 :goto_2

    .line 311
    .restart local v5    # "roundCanvas":Landroid/graphics/Canvas;
    .restart local v6    # "sourceWidth":I
    .restart local v7    # "cornerRadius":F
    .restart local v17    # "bitmapShader":Landroid/graphics/BitmapShader;
    .restart local v18    # "boundsDelta":F
    .restart local v24    # "dropShadowAlpha":I
    .restart local v25    # "dropShadowColor":I
    .restart local v26    # "dropShadowSize":F
    .restart local v28    # "largerSourceSize":I
    .restart local v30    # "ringOutline":F
    .restart local v31    # "round":Landroid/graphics/Bitmap;
    .restart local v33    # "smallerSourceSize":I
    .restart local v34    # "sourceHeight":I
    .restart local v36    # "transformationInset":I
    :cond_6
    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    move-object/from16 v10, p0

    move-object v11, v5

    move v12, v6

    move v13, v7

    invoke-direct/range {v10 .. v15}, Lcom/google/android/play/image/AvatarCropTransformation;->drawAvatar(Landroid/graphics/Canvas;IFFF)V

    .line 313
    const/4 v4, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4, v8}, Lcom/google/android/play/image/AvatarCropTransformation;->drawOutline(Landroid/graphics/Canvas;FI)V

    goto :goto_4

    .end local v5    # "roundCanvas":Landroid/graphics/Canvas;
    .end local v6    # "sourceWidth":I
    .end local v7    # "cornerRadius":F
    .end local v17    # "bitmapShader":Landroid/graphics/BitmapShader;
    .end local v18    # "boundsDelta":F
    .end local v31    # "round":Landroid/graphics/Bitmap;
    .restart local v35    # "sourceWidth":I
    :cond_7
    move/from16 v6, v35

    .end local v35    # "sourceWidth":I
    .restart local v6    # "sourceWidth":I
    goto/16 :goto_3
.end method
